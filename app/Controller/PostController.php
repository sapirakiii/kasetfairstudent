<?php

class PostController extends AppController {

	private	$title_for_layout = 'Faculty of Agriculture, Chiang Mai University, คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่';
	private	$keywords = 'Faculty of Agriculture, Chiang Mai University';
	private	$keywordsTh = 'คณะเกษตรศาสตร์, มหาวิทยาลัยเชียงใหม่';
	private	$description = 'Faculty of Agriculture, Chiang Mai University, คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่';

	//public $layout = 'posts';
	
	public $uses = array('Info' , 'InfoDocument' , 'Typenew' ,'Event' );

	public $paginate = array(
        'limit' => 25,
        'order' => array(
        	'id' => 'DESC'
        )
	);
	private $urls = '/2017';

	public function beforeFilter() {
		date_default_timezone_set('Asia/Bangkok');
		$this->set(array('title_for_layout' => $this->title_for_layout,
						'keywords' => $this->keywords,
						'keywordsTh' => $this->keywordsTh,
						'description' => $this->description
						));

		
	
	}

	

	###################################################################################
	/////////////////////////////// fraction post /////////////////////////////////////
	###################################################################################

	public function index() {
		
		$UserName = $this->Session->read('UserName');
		$adminposts = $this->Session->read('adminposts');
	

		if(isset($UserName)){
			$this->set('UserName', $UserName);
			$this->set('adminposts', $adminposts);
			

			if($this->action == 'login')
				$this->redirect(array('action' => 'index'));
		}
		else{
			if($this->action != 'login')
			$this->redirect(array('controller' => 'mains','action' => 'login'));
		}

		// $LevelApp = $this->Session->read('LevelApp');
		

		if ($adminposts['Adminpost']['employee_status_id'] == 2 ||
			$adminposts['Adminpost']['employee_status_id'] == 3) {
			
			$groups = $this->EmployeeTypeNew->find('all', array(
				'conditions' => array(
					'employee_id' => $UserName['Employee']['id'],
					
				),
			));
			foreach ($groups as $group) {
				$news = $this->paginate('Info' ,array(
					'and' => array(
						array('typenew_id <= ' => 7),
						array('typenew_id >= ' => 1)
						),
					'language_id' => 1
				));
			}
			//$news = $this->paginate('Info');
			$this->set(compact(
				'news'
			));
		}else {
			$groups = $this->EmployeeTypeNew->find('all', array(
				'conditions' => array(
					'employee_id' => $UserName['Employee']['id'],
				),
			));
			//debug($groups);
			foreach ($groups as $group) {
				$news = $this->paginate('Info' ,array(
					'typenew_id' => $groups[0]['EmployeeTypeNew']['typenew_id']
				));
			}
			//$news = $this->paginate('Info');
			$this->set(compact(
				'news'
			));	
		}
		// }else{
			// $groups = $this->EmployeeTypeNew->find('all', array(
			// 	'conditions' => array('employee_id' => $UserName['Employee']['id'],
			// 	),
			// ));
			// foreach ($groups as $group) {
			// $news = $this->paginate('Info' ,array(
			// 	'OR' => array(

			// 		array('TypeNews' => $group['Typenew']['TypeNews']),

			// 		array('THilight' => 1)
			// 		)
			// ));
			//}
			// $news = $this->paginate('Info' ,array(
			// 	'OR' => array(

			// 		array('TypeNews' => $group_app['Typenew']['TypeNews']),

			// 		array('THilight' => 1)
			// 		)
			// ));
			// $this->set(compact('news'));
		//}
	
	}
	public function InfoEng() {
		
		$UserName = $this->Session->read('UserName');
		$adminposts = $this->Session->read('adminposts');
	

		if(isset($UserName)){
			$this->set('UserName', $UserName);
			$this->set('adminposts', $adminposts);
			

			if($this->action == 'login')
				$this->redirect(array('action' => 'index'));
		}
		else{
			if($this->action != 'login')
				$this->redirect(array('action' => 'login'));
		}

		// $LevelApp = $this->Session->read('LevelApp');
		

		if ($adminposts['Adminpost']['employee_status_id'] == 2 ||
			$adminposts['Adminpost']['employee_status_id'] == 3) {
			
			$groups = $this->EmployeeTypeNew->find('all', array(
				'conditions' => array(
					'employee_id' => $UserName['Employee']['id'],
					
				),
			));
			foreach ($groups as $group) {
				$news = $this->paginate('Info' ,array(
					'and' => array(
						array('typenew_id <= ' => 7),
						array('typenew_id >= ' => 1)
						),
					'language_id' => 2
				));
			}
			//$news = $this->paginate('Info');
			$this->set(compact(
				'news'
			));
		}else {
			$groups = $this->EmployeeTypeNew->find('all', array(
				'conditions' => array(
					'employee_id' => $UserName['Employee']['id'],
				),
			));
			//debug($groups);
			foreach ($groups as $group) {
				$news = $this->paginate('Info' ,array(
					'typenew_id' => $groups[0]['EmployeeTypeNew']['typenew_id']
				));
			}
			//$news = $this->paginate('Info');
			$this->set(compact(
				'news'
			));	
		}
	}

	###################################################################################
	//////////////////////////////// post news ///////////////////////////////////////
	###################################################################################



	public function add_post() {
		$Usernames = $this->Session->read('UserName');

		try{

		
		if($this->request->data){
			date_default_timezone_set('Asia/Bangkok');
			$this->request->data['Info']['organize_id'] = $Usernames['Employee']['organize_id'];

			$newformat = date('Y-m-d', strtotime($this->request->data['Info']['postdate']));
			$this->request->data['Info']['postdate'] = $newformat;

			$mounth =  date('m', strtotime($this->request->data['Info']['postdate']));
			$this->request->data['Info']['mounth'] = $mounth;

			$year =  date('Y', strtotime($this->request->data['Info']['postdate']));
			$this->request->data['Info']['year'] = $year;

			$newformat1 = date('Y-m-d', strtotime($this->request->data['Info']['postend']));
			$this->request->data['Info']['postend'] = $newformat1;
			////////////////// before upload ///////////////////
			$files = $this->request->data['InfoDocument']['files'];

			////////////////// cheack img upload  ///////////////////
			// if ($this->request->data['Info']['file_Picture_select'] == 99) {
		
			// 	if ($this->request->data['Info']['file_Picture']['type'] != null) {

			// 		if($this->request->data['Info']['file_Picture']['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif")){

			// 			$this->Session->setFlash('ไฟล์ข่าวเด่นอัฟโหลดไม่ถูกต้อง');
			// 			$this->Session->write('alertType','danger');
			// 			debug($this->request->data);
			// 			$this->redirect('add_post');
			// 		}
			// 	}else{

			// 		$this->Session->setFlash('คุณไม่ได้อัปโหลดไฟล์ภาพตัวอย่าง');
			// 		$this->Session->write('alertType','danger');
			// 		$this->redirect('add_post');
			// 	}
			// }

			////////////////// cheack title ///////////////////
			if ($this->request->data['Info']['Title'] == null) {
				$this->Session->setFlash('กรุณากรอกชื่อเรื่อง');
				$this->Session->write('alertType','danger');
				$this->redirect('add_post');
			}
			
			////////////////// cheack file upload ///////////////////
			if ($this->request->data['InfoDocument']['files'][0]['type'] != null) {
				foreach ($this->request->data['InfoDocument']['files'] as $check) {
					if($check['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif" or "application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document" or "application/x-rar-compressed" or "application/octet-stream" or "application/zip" or "application/octet-stream" or "application/vnd.ms-powerpoint" or "application/vnd.openxmlformats-officedocument.presentationml.presentation" or "application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")){


						foreach ($files as $file) {

							list($name, $fileType) = explode(".", $file['name']);
							if($fileType != "doc" or "docx" or "jpg" or "jpeg" or "png" or "gif" or "zip" or "rar" or "xls" or "xlsx" or "ppt" or "pptx" or "pdf"){

								$this->Session->setFlash('กรุณาแก้ไขชื่อไฟล์ก่อนอัปโหลด โดยไม่ให้มีอักขระ จุด .');
								$this->Session->write('alertType','danger');
								$this->redirect('add_post');
							}
						}


						$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
						$this->Session->write('alertType','danger');
						$this->redirect('add_post');
					}
				}
			}
			
	

			/////////////// add ID and data to table info before upload /////////////////
			$this->Info->save($this->request->data['Info']);
			// try{
			// 	$this->Info->save($this->request->data['Info']);
			// }catch(Exception $e){
			// 	var_dump($e->getMessage());
			// }
			
			//exit();

			// if($this->request->data['Info']['file_Picture']['name']){

			// 	$info_id = $this->Info->find('first' ,array(
			// 		'order' => array('Info.id' => 'DESC'),
					
			// 	));
			// 	// $info_id = $this->Info->find('all' ,array(
			// 	// 	'order' => array('created' => 'DESC'),
			// 	// 	'limit' => 1,
			// 	// ));

			// 	list($name, $fileType) = explode(".", $this->request->data['Info']['file_Picture']['name']);

			// 	$this->request->data['Info']['Picture'] = $info_id['Info']['id']."_"."pic".".".$fileType;


			// 	if(!file_exists(WWW_ROOT.'files'.DS.'Picture'.DS))
			// 		mkdir(WWW_ROOT.'files'.DS.'Picture'.DS);

			// 	move_uploaded_file($this->request->data['Info']['file_Picture']['tmp_name'],WWW_ROOT.'files'.DS.'Picture'.DS.$this->request->data['Info']['Picture']);

			// 	$this->Info->save($this->request->data);
				
			// }

			// if ($this->request->data['Info']['file_Picture_select'] < 10 and $this->request->data['Info']['file_Picture_select'] > 0){
				
			// 	$info_id = $this->Info->find('first' ,array(
			// 		'order' => array('Info.id' => 'DESC'),
					
			// 	));

			// 	$this->request->data['Info']['Picture'] = $this->request->data['Info']['file_Picture_select']."."."jpg";
			// 	$this->Info->save($this->request->data);
				
			// }


			/////////////// files upload Document /////////////////
			if($files[0]['name'] != null){
				
				$this->request->data['Info']['Document'] = "มีเอกสารประกอบ";
				$this->Info->save($this->request->data['Info']);

				$info_id = $this->Info->find('first' ,array(
					'order' => array('Info.id' => 'DESC'),
					
				));

				$i = 0;
				foreach ($files as $file) {

					list($name, $fileType) = explode(".", $file['name']);

					$this->request->data['InfoDocument']['name'] = $info_id['Info']['id']."_".$i.".".$fileType;
					$this->request->data['InfoDocument']['name_old'] = $name;
					$this->request->data['InfoDocument']['info_id'] = $info_id['Info']['id'];

					if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
						$this->request->data['InfoDocument']['class'] = "file";
					}else{
						$this->request->data['InfoDocument']['class'] = "img";
					}
					
					
					//=====
					// if(!file_exists(WWW_ROOT.'files'.DS.'Document'.DS))
					// 	mkdir(WWW_ROOT.'files'.DS.'Document'.DS);

					// move_uploaded_file($file['tmp_name'],WWW_ROOT.'files'.DS.'Document'.DS.$this->request->data['InfoDocument']['name']);

					$folder = WWW_ROOT.'files'.DS.'Document'.DS;

					if(!file_exists($folder))
						mkdir($folder);

					move_uploaded_file($file['tmp_name'],$folder.$this->request->data['InfoDocument']['name']);


					

					$this->InfoDocument->create();
					$this->InfoDocument->save($this->request->data);
						
					
					$i++;
					
				}

			}else{
				$this->request->data['Info']['Document'] = "ไม่พบเอกสารประกอบ";
			}

			$this->Session->write('alertType','success');
			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->redirect(array('action' => 'index'));

		}
	}catch(Exception $e){
		$this->Session->write('alertType','danger');
		$this->Session->setFlash('ขออภัย ระบบยังไม่รองรับ อักขระพิเศษ หรือ Emotion กรุณาใช้ข้อความเท่านั้น !!');
		$this->redirect(array('action' => 'index'));

		echo $e->getMessage();
	 }

		// debug(WWW_ROOT.'files'.DS.'Document'.DS);
		// debug(WEBROOT_DIR); 
		
			// $adminposts = $this->Session->read('adminposts');
		
			// $listorganizes = $this->Organize->find('all',array(	
			// 	'conditions' => array(
			// 		'id' => $Usernames['Employee']['organize_id'],
			// 	),		
			// 	'fields' => array('Organize.id','Organize.name'),
			// 	'group' => array('Organize.id'),
			// ));

			// if ($adminposts['Adminpost']['employee_status_id'] == 2 ||
			// 	$adminposts['Adminpost']['employee_status_id'] == 3) {
			// 		$groups = $this->Typenew->find('all', array(
			// 			'conditions' => array(							
			// 				'and' => array(
			// 					array('Typenew.id <= ' => 7),
			// 					array('Typenew.id >= ' => 1)
			// 					),
			// 			),
			// 			'order' => array('Typenew.id ASC'),
			// 		)); 
			

			// }else {
			// 	$groups = $this->EmployeeTypeNew->find('all', array(
			// 		'conditions' => array('employee_id' => $Usernames['Employee']['id'],
			// 		),
			// 	));
			// }
			
			// $output = '';
			// foreach ($groups as $group) {		
				
			// 			$output .= '
			// 				<option value="'.$group['Typenew']['id'].'">'.$group['Typenew']['TypeNews'].'</option>
			// 			';
				
			// }	
			
				$this->set(array(							
					'output' => $output,
					'listorganizes' => $listorganizes
				));
			
	}
    public function add_post2() {
		$Usernames = $this->Session->read('UserName');

		try{

		
		if($this->request->data){
			date_default_timezone_set('Asia/Bangkok');
			
			////////////////// before upload ///////////////////
			$files = $this->request->data['InfoDocument']['files'];

			

			
			////////////////// cheack file upload ///////////////////
			if ($this->request->data['InfoDocument']['files'][0]['type'] != null) {
				foreach ($this->request->data['InfoDocument']['files'] as $check) {
					if($check['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif" or "application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document" or "application/x-rar-compressed" or "application/octet-stream" or "application/zip" or "application/octet-stream" or "application/vnd.ms-powerpoint" or "application/vnd.openxmlformats-officedocument.presentationml.presentation" or "application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")){


						foreach ($files as $file) {

							list($name, $fileType) = explode(".", $file['name']);
							if($fileType != "doc" or "docx" or "jpg" or "jpeg" or "png" or "gif" or "zip" or "rar" or "xls" or "xlsx" or "ppt" or "pptx" or "pdf"){

								$this->Session->setFlash('กรุณาแก้ไขชื่อไฟล์ก่อนอัปโหลด โดยไม่ให้มีอักขระ จุด .');
								$this->Session->write('alertType','danger');
								$this->redirect('add_post');
							}
						}


						$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
						$this->Session->write('alertType','danger');
						$this->redirect('add_post');
					}
				}
			}
			
	


			/////////////// files upload Document /////////////////
			if($files[0]['name'] != null){
				
				// $this->request->data['Info']['Document'] = "มีเอกสารประกอบ";
				// $this->Info->save($this->request->data['Info']);

				$info_id = $this->InfoDocument->find('first' ,array(
					'order' => array('InfoDocument.id' => 'DESC'),
					
				));
			
				$i = 0;
				$dates = date("Ymd_His");	
				foreach ($files as $file) {
					
					list($name, $fileType) = explode(".", $file['name']);

					$this->request->data['InfoDocument']['name'] = 	$info_id['InfoDocument']['id']."_".$i.".".$fileType;
					//$this->request->data['InfoDocument']['name_old'] = $name;
					//$this->request->data['InfoDocument']['info_id'] = $info_id['Info']['id'];

					if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
						$this->request->data['InfoDocument']['class'] = "file";
					}else{
						$this->request->data['InfoDocument']['class'] = "img";
					}
					
					
					//=====
					// if(!file_exists(WWW_ROOT.'files'.DS.'Document'.DS))
					// 	mkdir(WWW_ROOT.'files'.DS.'Document'.DS);

					// move_uploaded_file($file['tmp_name'],WWW_ROOT.'files'.DS.'Document'.DS.$this->request->data['InfoDocument']['name']);

					$folder = WWW_ROOT.'files'.DS.'Document'.DS;

					if(!file_exists($folder))
						mkdir($folder);

					move_uploaded_file($file['tmp_name'],$folder.$this->request->data['InfoDocument']['name']);


					

					$this->InfoDocument->create();
					$this->InfoDocument->save($this->request->data);
						
					
					$i++;
					
				}

			}else{
				$this->request->data['Info']['Document'] = "ไม่พบเอกสารประกอบ";
			}

			$this->Session->write('alertType','success');
			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->redirect(array('action' => 'add_post2'));

		}
		}catch(Exception $e){
			$this->Session->write('alertType','danger');
			$this->Session->setFlash('ขออภัย ระบบยังไม่รองรับ อักขระพิเศษ หรือ Emotion กรุณาใช้ข้อความเท่านั้น !!');
			$this->redirect(array('action' => 'add_post2'));

			echo $e->getMessage();
		}

			// debug(WWW_ROOT.'files'.DS.'Document'.DS);
			// debug(WEBROOT_DIR); 
			
			
				
					//===========================
			$infodocs = $this->InfoDocument->find('all' ,array(
				'conditions' => array('info_id' => null,
				),					
				'order' => array('InfoDocument.id' => 'DESC')
			));	
			$this->set(array(
				'infodocs' => $infodocs
			));		
			
	}
	public function edit_post($id = null) {
		try{
		if($this->request->data){
			$newformat = date('Y-m-d', strtotime($this->request->data['Info']['postdate']));
			$this->request->data['Info']['postdate'] = $newformat;

			$newformat1 = date('Y-m-d', strtotime($this->request->data['Info']['postend']));
			$this->request->data['Info']['postend'] = $newformat1;
			// ////////////////// cheack img upload  ///////////////////
			// if ($this->request->data['Info']['file_Picture_select'] == 99) {
		
			// 	if ($this->request->data['Info']['file_Picture']['type'] != null) {

			// 		if($this->request->data['Info']['file_Picture']['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif")){

			// 			$this->Session->setFlash('ไฟล์ข่าวเด่นอัฟโหลดไม่ถูกต้อง');
			// 			$this->Session->write('alertType','danger');
			// 			debug($this->request->data);
			// 			$this->redirect('add_post');
			// 		}
			// 	}else{

			// 		$this->Session->setFlash('คุณไม่ได้อัปโหลดไฟล์ภาพตัวอย่าง');
			// 		$this->Session->write('alertType','danger');
			// 		$this->redirect('edit_post');
			// 	}
			// }

			
			// if ($this->request->data['Info']['file_Picture_select'] < 10 and $this->request->data['Info']['file_Picture_select'] > 0){
				

			// 	$info_id = $this->Info->find('all' ,array(
			// 		'order' => array('created' => 'DESC'),
			// 		'limit' => 1,
			// 	));

			// 	$this->request->data['Info']['Picture'] = $this->request->data['Info']['file_Picture_select']."."."jpg";
				
			// }

				if($this->Info->save($this->request->data)){
					$infos = $this->Info->findById($id);
					if ($infos['Info']['language_id']== 1) {
						$this->Session->write('alertType','success');
						$this->Session->setFlash('บันทึกข้อมูล เรียบร้อยแล้ว');
						$this->redirect(array('action' => 'index'));	
					}else {
						$this->Session->write('alertType','success');
						$this->Session->setFlash('บันทึกข้อมูล เรียบร้อยแล้ว');
						$this->redirect(array('action' => 'InfoEng'));
					}
				}			
			}
		}catch(Exception $e){
			$this->Session->write('alertType','danger');
			$this->Session->setFlash('ขออภัย ระบบยังไม่รองรับ อักขระพิเศษ หรือ Emotion กรุณาใช้ข้อความเท่านั้น !!');
			$this->redirect(array('action' => 'index'));

			echo $e->getMessage();
		}
			

			$infos = $this->Info->findById($id);
			$this->request->data = $infos;

			$time = strtotime($infos['Info']['postdate']);
			$newformat = date('d-M-Y', $time);	
			$this->request->data['Info']['postdate'] = $newformat;
	
			$time1 = strtotime($infos['Info']['postend']);
			$newformat1 = date('d-M-Y', $time1);	
			$this->request->data['Info']['postend'] = $newformat1;

			$TypeNew = $this->Typenew->find('list', array(
        									'fields' => array('TypeNews')
   			));
			$this->set(array(
				'TypeNew' => $TypeNew
			));
			if(!$this->request->data){
				$this->redirect(array('action' => 'index'));
			}	
		

	}

	public function delete_post($id = null) {
		$this->request->data = $this->Info->findById($id);
		if($this->request->data){

			$deleteIds = $this->InfoDocument->find('all' , array(
				'conditions' => array('info_id' => $id)
			));

			foreach ($deleteIds as $deleteId) {

				$this->InfoDocument->create();
				$this->InfoDocument->delete($deleteId['InfoDocument']['id']);
				$file = new File($deleteId['InfoDocument']['name']);
				if($file->delete());
			}

			$this->Info->delete($id);

			$this->Session->write('alertType','danger');
			$this->Session->setFlash('ลบข้อมูล เรียบร้อยแล้ว');
			$this->redirect(array('action' => 'index'));
		}
	}
	public function ActivityAll() {
		$news = $this->paginate('Info' ,array(
				'typenew_id' => 8,
				'language_id' => 1
			));
		
		//$news = $this->paginate('Info');
		$this->set(compact(
			'news'
		));	
	
	}
	public function ActivityAll2() {
		$news = $this->paginate('Info' ,array(
				'typenew_id' => 31,
				'language_id' => 1
			));
		
		//$news = $this->paginate('Info');
		$this->set(compact(
			'news'
		));	
	
	}
	public function ActivityAllEng() {
		$news = $this->paginate('Info' ,array(
				'typenew_id' => 8,
				'language_id' => 2
			));
		
		//$news = $this->paginate('Info');
		$this->set(compact(
			'news'
		));	
	
	}
	public function AddActivity($type=null) {
		$Usernames = $this->Session->read('UserName');
		try{
		if($this->request->data){
			date_default_timezone_set('Asia/Bangkok');
			$this->request->data['Info']['organize_id'] = $Usernames['Employee']['organize_id'];

			$newformat = date('Y-m-d', strtotime($this->request->data['Info']['postdate']));
			$this->request->data['Info']['postdate'] = $newformat;

			$mounth =  date('m', strtotime($this->request->data['Info']['postdate']));
			$this->request->data['Info']['mounth'] = $mounth;

			$year =  date('Y', strtotime($this->request->data['Info']['postdate']));
			$this->request->data['Info']['year'] = $year;

			// $newformat1 = date('Y-m-d', strtotime($this->request->data['Info']['postend']));
			// $this->request->data['Info']['postend'] = $newformat1;
			////////////////// before upload ///////////////////
			$files = $this->request->data['InfoDocument']['files'];

			////////////////// cheack img upload  ///////////////////
			 if ($this->request->data['Info']['file_Picture_select'] == 99) {
		
				if ($this->request->data['Info']['file_Picture']['type'] != null) {

					if($this->request->data['Info']['file_Picture']['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif")){

						$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
						$this->Session->write('alertType','danger');					
						$this->redirect('AddActivity');
					}
				}else{

					$this->Session->setFlash('คุณไม่ได้อัปโหลดไฟล์ภาพตัวอย่าง');
					$this->Session->write('alertType','danger');
					$this->redirect('AddActivity');
				}
			}

			////////////////// cheack title ///////////////////
			if ($this->request->data['Info']['Title'] == null) {
				$this->Session->setFlash('กรุณากรอกชื่อเรื่อง');
				$this->Session->write('alertType','danger');
				$this->redirect('AddActivity');
			}
			
			////////////////// cheack file upload ///////////////////
			if ($this->request->data['InfoDocument']['files'][0]['type'] != null) {
				foreach ($this->request->data['InfoDocument']['files'] as $check) {
					if($check['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif" or "application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document" or "application/x-rar-compressed" or "application/octet-stream" or "application/zip" or "application/octet-stream" or "application/vnd.ms-powerpoint" or "application/vnd.openxmlformats-officedocument.presentationml.presentation" or "application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")){


						foreach ($files as $file) {

							list($name, $fileType) = explode(".", $file['name']);
							if($fileType != "doc" or "docx" or "jpg" or "jpeg" or "png" or "gif" or "zip" or "rar" or "xls" or "xlsx" or "ppt" or "pptx" or "pdf"){

								$this->Session->setFlash('กรุณาแก้ไขชื่อไฟล์ก่อนอัปโหลด โดยไม่ให้มีอักขระ จุด .');
								$this->Session->write('alertType','danger');
								$this->redirect('AddActivity');
							}
						}


						$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
						$this->Session->write('alertType','danger');
						$this->redirect('AddActivity');
					}
				}
			}
			
			

			/////////////// add ID and data to table info before upload /////////////////
			$this->Info->save($this->request->data['Info']);
			

			if($this->request->data['Info']['file_Picture']['name']){

				$info_id = $this->Info->find('first' ,array(
					'order' => array('Info.id' => 'DESC'),
					
				));
				// $info_id = $this->Info->find('all' ,array(
				// 	'order' => array('created' => 'DESC'),
				// 	'limit' => 1,
				// ));

				list($name, $fileType) = explode(".", $this->request->data['Info']['file_Picture']['name']);

				$this->request->data['Info']['Picture'] = $info_id['Info']['id']."_"."pic".".".$fileType;


				if(!file_exists(WWW_ROOT.'files'.DS.'Activity'.DS.$info_id['Info']['id']))
					mkdir(WWW_ROOT.'files'.DS.'Activity'.DS.$info_id['Info']['id']);

				move_uploaded_file($this->request->data['Info']['file_Picture']['tmp_name'],WWW_ROOT.'files'.DS.'Activity'.DS.$info_id['Info']['id'].DS.$this->request->data['Info']['Picture']);

				$this->Info->save($this->request->data);
				
			}

			if ($this->request->data['Info']['file_Picture_select'] < 10 and $this->request->data['Info']['file_Picture_select'] > 0){
				
				$info_id = $this->Info->find('first' ,array(
					'order' => array('Info.id' => 'DESC'),
					
				));

				$this->request->data['Info']['Picture'] = $this->request->data['Info']['file_Picture_select']."."."jpg";
				$this->Info->save($this->request->data);
				
			}
			




			/////////////// files upload Document /////////////////
			try{
				
			
			if($files[0]['name'] != null){
				
				$this->request->data['Info']['Document'] = "มีเอกสารประกอบ";
				$this->Info->save($this->request->data['Info']);

				$info_id = $this->Info->find('first' ,array(
					'order' => array('Info.id' => 'DESC'),
					
				));

				$i = date("m");
				foreach ($files as $file) {
					
					list($name, $fileType) = explode(".", $file['name']);

					$this->request->data['InfoDocument']['name'] = $info_id['Info']['id']."_".$i.".".$fileType;
					$this->request->data['InfoDocument']['name_old'] = $name;
					$this->request->data['InfoDocument']['info_id'] = $info_id['Info']['id'];

					if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
						$this->request->data['InfoDocument']['class'] = "file";
					}else{
						$this->request->data['InfoDocument']['class'] = "img";
					}
					
					$folder = WWW_ROOT.'files'.DS.'Activity'.DS.$info_id['Info']['id'];

					if(!file_exists($folder))
					mkdir($folder);
				
				move_uploaded_file($file['tmp_name'],$folder.DS.$this->request->data['InfoDocument']['name']);
				
				
				
					$this->InfoDocument->create();
					$this->InfoDocument->save($this->request->data);
						
					$i++;
					
					
						}

					}else{
						$this->request->data['Info']['Document'] = "ไม่พบเอกสารประกอบ";
					}
				}catch(Exception $e){
					echo $e->getMessage();
				}
				if ($type == 1) {
					$this->Session->write('alertType','success');
					$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
					$this->redirect(array('action' => 'ActivityAll'));
				}else {
					$this->Session->write('alertType','success');
					$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
					$this->redirect(array('action' => 'ActivityAll'));
				}
					

				}
				
			}catch(Exception $e){
				$this->Session->write('alertType','danger');
				$this->Session->setFlash('ขออภัย ระบบยังไม่รองรับ อักขระพิเศษ หรือ Emotion กรุณาใช้ข้อความเท่านั้น !!');
				$this->redirect(array('action' => 'ActivityAll'));

				echo $e->getMessage();
			}	
			
			$this->set(array(							
				'type' => $type,
				
			));
			
			
        
	
	}
	public function edit_activity($id = null,$type = null) {
		$Usernames = $this->Session->read('UserName');
		try{

		if($this->request->data){
			date_default_timezone_set('Asia/Bangkok');
					$newformat = date('Y-m-d', strtotime($this->request->data['Info']['postdate']));
					$this->request->data['Info']['postdate'] = $newformat;
		
					// $newformat1 = date('Y-m-d', strtotime($this->request->data['Info']['postend']));
					// $this->request->data['Info']['postend'] = $newformat1;

					$files = $this->request->data['InfoDocument']['files'];
					
		
					$this->Info->save($this->request->data['Info']);	
					
				////////////////// check file upload ///////////////////
				if ($this->request->data['InfoDocument']['files'][0]['type'] != null) {
					foreach ($this->request->data['InfoDocument']['files'] as $check) {
						if($check['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif" or "application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document" or "application/x-rar-compressed" or "application/octet-stream" or "application/zip" or "application/octet-stream" or "application/vnd.ms-powerpoint" or "application/vnd.openxmlformats-officedocument.presentationml.presentation" or "application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")){


							foreach ($files as $file) {

								list($name, $fileType) = explode(".", $file['name']);
								if($fileType != "doc" or "docx" or "jpg" or "jpeg" or "png" or "gif" or "zip" or "rar" or "xls" or "xlsx" or "ppt" or "pptx" or "pdf"){

									$this->Session->setFlash('กรุณาแก้ไขชื่อไฟล์ก่อนอัปโหลด โดยไม่ให้มีอักขระ จุด .');
									$this->Session->write('alertType','danger');
									$this->redirect('edit_activity',$id);
								}
							}


							$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
							$this->Session->write('alertType','danger');
							$this->redirect('edit_activity',$id);
						}
					}
				}

				
				/////////////// files upload Document /////////////////
			try{
				
			
				if($files[0]['name'] != null){
					
					$this->request->data['Info']['Document'] = "มีเอกสารประกอบ";
					$this->Info->save($this->request->data['Info']);

					$info_id = $this->Info->find('first' ,array(
						'order' => array('Info.id' => 'DESC'),
						
					));

					$infodoc_id = $this->InfoDocument->find('first' ,array(
						'conditions' => array('info_id' => $id,
						),					
						'order' => array('InfoDocument.info_id' => 'DESC')
					));	

					$dates = date("Ymd_His");	
					
				
					$i = substr($infodoc_id['InfoDocument']['name'],2,3);
					foreach ($files as $file) {
						
						list($name, $fileType) = explode(".", $file['name']);

						$this->request->data['InfoDocument']['name'] = $infodoc_id['InfoDocument']['info_id']."_".$i."_".$dates.".".$fileType;
						$this->request->data['InfoDocument']['name_old'] = $name;
						$this->request->data['InfoDocument']['info_id'] = $infodoc_id['InfoDocument']['info_id'];

						if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
							$this->request->data['InfoDocument']['class'] = "file";
						}else{
							$this->request->data['InfoDocument']['class'] = "img";
						}
						
						$folder = WWW_ROOT.'files'.DS.'Activity'.DS.$infodoc_id['InfoDocument']['info_id'];

						if(!file_exists($folder))
						mkdir($folder);
					
					move_uploaded_file($file['tmp_name'],$folder.DS.$this->request->data['InfoDocument']['name']);
					
					
					
						$this->InfoDocument->create();
						$this->InfoDocument->save($this->request->data);
							
						$i++;
						
						
					}

				}else{
					$this->request->data['Info']['Document'] = "ไม่พบเอกสารประกอบ";
				}
			}catch(Exception $e){
				echo $e->getMessage();
			}
			


			$infos = $this->Info->findById($id);
						if ($type == 1) {
							$this->Session->write('alertType','success');
							$this->Session->setFlash('บันทึกข้อมูล เรียบร้อยแล้ว');
							$this->redirect(array('action' => 'ActivityAll'));	
						}else {
							$this->Session->write('alertType','success');
							$this->Session->setFlash('บันทึกข้อมูล เรียบร้อยแล้ว');
							$this->redirect(array('action' => 'ActivityAll'));
						}					
			}
	
		}catch(Exception $e){
			$this->Session->write('alertType','danger');
			$this->Session->setFlash('ขออภัย ระบบยังไม่รองรับ อักขระพิเศษ หรือ Emotion กรุณาใช้ข้อความเท่านั้น !!');
			$this->redirect(array('action' => 'ActivityAll'));	
			echo $e->getMessage();
		}
		
		$infos = $this->Info->findById($id);
		$this->request->data = $infos;
		
		$time = strtotime($infos['Info']['postdate']);
		$newformat = date('d-M-Y', $time);	
		$this->request->data['Info']['postdate'] = $newformat;
			
		$time1 = strtotime($infos['Info']['postend']);
		$newformat1 = date('d-M-Y', $time1);	
		$this->request->data['Info']['postend'] = $newformat1;
		
		$TypeNew = $this->Typenew->find('list', array(
					'fields' => array('TypeNews')
		   ));
		$TypeNews = $this->Typenew->find('all' ,array(
			'conditions' => array('id' => $infos['Info']['typenew_id'] ,
			),					
			
		));	
		//debug($TypeNews);			
		$this->set(array(
			'TypeNew' => $TypeNew,
			'TypeNews' => $TypeNews,
			'id' => $id
		));
		if(!$this->request->data){
			$this->redirect(array('action' => 'index'));
		}	
		//===========================
		$infodocs = $this->InfoDocument->find('all' ,array(
			'conditions' => array('info_id' => $id,
			),					
			'order' => array('InfoDocument.info_id' => 'DESC')
		));	
		$this->set(array(
			'infodocs' => $infodocs
		));	
		
		$this->set(array(							
			'type' => $type,
			
		));
		
	}
	public function edit_activitypage($id = null) {
		$Usernames = $this->Session->read('UserName');
		try{

		if($this->request->data){
			date_default_timezone_set('Asia/Bangkok');
					// $newformat = date('Y-m-d', strtotime($this->request->data['Info']['postdate']));
					// $this->request->data['Info']['postdate'] = $newformat;
		
					// $newformat1 = date('Y-m-d', strtotime($this->request->data['Info']['postend']));
					// $this->request->data['Info']['postend'] = $newformat1;

					$files = $this->request->data['InfoDocument']['files'];
					
		
					$this->Info->save($this->request->data['Info']);	
					
				////////////////// check file upload ///////////////////
				if ($this->request->data['InfoDocument']['files'][0]['type'] != null) {
					foreach ($this->request->data['InfoDocument']['files'] as $check) {
						if($check['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif" or "application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document" or "application/x-rar-compressed" or "application/octet-stream" or "application/zip" or "application/octet-stream" or "application/vnd.ms-powerpoint" or "application/vnd.openxmlformats-officedocument.presentationml.presentation" or "application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")){


							foreach ($files as $file) {

								list($name, $fileType) = explode(".", $file['name']);
								if($fileType != "doc" or "docx" or "jpg" or "jpeg" or "png" or "gif" or "zip" or "rar" or "xls" or "xlsx" or "ppt" or "pptx" or "pdf"){

									$this->Session->setFlash('กรุณาแก้ไขชื่อไฟล์ก่อนอัปโหลด โดยไม่ให้มีอักขระ จุด .');
									$this->Session->write('alertType','danger');
									$this->redirect('edit_activitypage',$id);
								}
							}


							$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
							$this->Session->write('alertType','danger');
							$this->redirect('edit_activitypage',$id);
						}
					}
				}

				
				////////////////// cheack file upload ///////////////////
			if ($this->request->data['InfoDocument']['files'][0]['type'] != null) {
				foreach ($this->request->data['InfoDocument']['files'] as $check) {
					if($check['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif" or "application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document" or "application/x-rar-compressed" or "application/octet-stream" or "application/zip" or "application/octet-stream" or "application/vnd.ms-powerpoint" or "application/vnd.openxmlformats-officedocument.presentationml.presentation" or "application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")){


						foreach ($files as $file) {

							list($name, $fileType) = explode(".", $file['name']);
							if($fileType != "doc" or "docx" or "jpg" or "jpeg" or "png" or "gif" or "zip" or "rar" or "xls" or "xlsx" or "ppt" or "pptx" or "pdf"){

								$this->Session->setFlash('กรุณาแก้ไขชื่อไฟล์ก่อนอัปโหลด โดยไม่ให้มีอักขระ จุด .');
								$this->Session->write('alertType','danger');
								$this->redirect('edit_activitypage',$id);
							}
						}


						$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
						$this->Session->write('alertType','danger');
						$this->redirect('edit_activitypage',$id);
					}
				}
			}
			
	

			/////////////// add ID and data to table info before upload /////////////////
			$this->Info->save($this->request->data['Info']);
			// try{
			// 	$this->Info->save($this->request->data['Info']);
			// }catch(Exception $e){
			// 	var_dump($e->getMessage());
			// }
			
			//exit();

			// if($this->request->data['Info']['file_Picture']['name']){

			// 	$info_id = $this->Info->find('first' ,array(
			// 		'order' => array('Info.id' => 'DESC'),
					
			// 	));
			// 	// $info_id = $this->Info->find('all' ,array(
			// 	// 	'order' => array('created' => 'DESC'),
			// 	// 	'limit' => 1,
			// 	// ));

			// 	list($name, $fileType) = explode(".", $this->request->data['Info']['file_Picture']['name']);

			// 	$this->request->data['Info']['Picture'] = $info_id['Info']['id']."_"."pic".".".$fileType;


			// 	if(!file_exists(WWW_ROOT.'files'.DS.'Picture'.DS))
			// 		mkdir(WWW_ROOT.'files'.DS.'Picture'.DS);

			// 	move_uploaded_file($this->request->data['Info']['file_Picture']['tmp_name'],WWW_ROOT.'files'.DS.'Picture'.DS.$this->request->data['Info']['Picture']);

			// 	$this->Info->save($this->request->data);
				
			// }

			// if ($this->request->data['Info']['file_Picture_select'] < 10 and $this->request->data['Info']['file_Picture_select'] > 0){
				
			// 	$info_id = $this->Info->find('first' ,array(
			// 		'order' => array('Info.id' => 'DESC'),
					
			// 	));

			// 	$this->request->data['Info']['Picture'] = $this->request->data['Info']['file_Picture_select']."."."jpg";
			// 	$this->Info->save($this->request->data);
				
			// }


			/////////////// files upload Document /////////////////
			if($files[0]['name'] != null){
				
				$this->request->data['Info']['Document'] = "มีเอกสารประกอบ";
				$this->Info->save($this->request->data['Info']);

				$info_id = $this->Info->find('first' ,array(
					'order' => array('Info.id' => 'DESC'),
					
				));

				$infodoc_id = $this->InfoDocument->find('first' ,array(
					'conditions' => array('info_id' => $id,
					),					
					'order' => array('InfoDocument.id' => 'DESC')
				));	

				$dates = date("Ymd_His");	
				
				if ($infodoc_id == null) {
					$num =0;
					$i = date("m");
				}else {							
					$num = $infodoc_id['InfoDocument']['order'];
					$i = substr($infodoc_id['InfoDocument']['name'],3,2); //21011
				}
				foreach ($files as $file) {

					list($name, $fileType) = explode(".", $file['name']);

					$this->request->data['InfoDocument']['name'] = $id."_".$i.".".$fileType;
					$this->request->data['InfoDocument']['name_old'] = $name;
					$this->request->data['InfoDocument']['info_id'] = $id;

					if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
						$this->request->data['InfoDocument']['class'] = "file";
					}else{
						$this->request->data['InfoDocument']['class'] = "img";
					}
					
					
					//=====
					// if(!file_exists(WWW_ROOT.'files'.DS.'Document'.DS))
					// 	mkdir(WWW_ROOT.'files'.DS.'Document'.DS);

					// move_uploaded_file($file['tmp_name'],WWW_ROOT.'files'.DS.'Document'.DS.$this->request->data['InfoDocument']['name']);

					$folder = WWW_ROOT.'files'.DS.'Document'.DS;

					if(!file_exists($folder))
						mkdir($folder);

					move_uploaded_file($file['tmp_name'],$folder.$this->request->data['InfoDocument']['name']);


					

					$this->InfoDocument->create();
					$this->InfoDocument->save($this->request->data);
						
					
					$i++;
					
				}

			}else{
				$this->request->data['Info']['Document'] = "ไม่พบเอกสารประกอบ";
			}

			$this->Session->write('alertType','success');
			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->redirect(array('controller' => 'post','action' => 'edit_activitypage',$id));

		}
	}catch(Exception $e){
		$this->Session->write('alertType','danger');
		$this->Session->setFlash('ขออภัย ระบบยังไม่รองรับ อักขระพิเศษ หรือ Emotion กรุณาใช้ข้อความเท่านั้น !!');
		$this->redirect(array('controller' => 'post','action' => 'edit_activitypage',$id));

		echo $e->getMessage();
	 }
		
		$infos = $this->Info->findById($id);
		//debug($infos['Info']['typenew_id']);
		$this->request->data = $infos;
		
		// $time = strtotime($infos['Info']['postdate']);
		// $newformat = date('d-M-Y', $time);	
		// $this->request->data['Info']['postdate'] = $newformat;
			
		// $time1 = strtotime($infos['Info']['postend']);
		// $newformat1 = date('d-M-Y', $time1);	
		// $this->request->data['Info']['postend'] = $newformat1;
		
		$TypeNew = $this->Typenew->find('list', array(
					'fields' => array('TypeNews')
		   ));
		$TypeNews = $this->Typenew->find('all' ,array(
			'conditions' => array('id' => $infos['Info']['typenew_id'] ,
			),					
			
		));	
		//debug($TypeNews);			
		$this->set(array(
			'TypeNew' => $TypeNew,
			'TypeNews' => $TypeNews,
			'id' => $id
		));
		if(!$this->request->data){
			$this->redirect(array('action' => 'index'));
		}	
		//===========================
		$infodocs = $this->InfoDocument->find('all' ,array(
			'conditions' => array('info_id' => $id,
			),					
			'order' => array('InfoDocument.id' => 'DESC')
		));	
		$this->set(array(
			'infodocs' => $infodocs
		));		
		
	}
	public function delete_picture($infoid = null,$id=null) {
		$this->request->data = $this->InfoDocument->findById($infoid);
		if($this->request->data){

			$deleteIds = $this->InfoDocument->find('all' , array(
				'conditions' => array('InfoDocument.id' => $infoid)
			));

		

				$this->InfoDocument->create();
				$this->InfoDocument->delete($deleteIds[0]['InfoDocument']['id']);
				$file = new File($deleteIds[0]['InfoDocument']['name']);
				if($file->delete());
		

			$this->Session->write('alertType','danger');
			$this->Session->setFlash('ลบข้อมูล เรียบร้อยแล้ว');
			$this->redirect('edit_activity/'.$id);
		
		}
	}
	public function delete_activity($id = null) {
		$this->request->data = $this->Info->findById($id);
		if($this->request->data){

			$deleteIds = $this->InfoDocument->find('all' , array(
				'conditions' => array('info_id' => $id)
			));

			foreach ($deleteIds as $deleteId) {

				$this->InfoDocument->create();
				$this->InfoDocument->delete($deleteId['InfoDocument']['id']);
				$file = new File($deleteId['InfoDocument']['name']);
				if($file->delete());
			}

			$this->Info->delete($id);

			$this->Session->write('alertType','danger');
			$this->Session->setFlash('ลบข้อมูล เรียบร้อยแล้ว');
			$this->redirect(array('action' => 'ActivityAll'));
		}
	}
	public function detail($id = null) {

		
		$new = $this->request->data = $this->Info->findById($id);
		$this->set(array('new' => $new));

		$CountRead = $new['Info']['CountRead'] + 1;
		$data = array('id' => $id, 'CountRead' => $CountRead);
		$this->Info->save($data);

		

		if(!$this->request->data){
			$this->redirect(array('action' => 'index'));
		}

	}

	###################################################################################
	//////////////////////////////// post news ///////////////////////////////////////
	###################################################################################

	

	public function detail_activity($id = null) {

		$new = $this->request->data = $this->Info->findById($id);
		$this->set(array('new' => $new));

		$CountRead = $new['Info']['CountRead'] + 1;
		$data = array('id' => $id, 'CountRead' => $CountRead);
		$this->Info->save($data);

		

		if(!$this->request->data){
			$this->redirect(array('action' => 'index'));
		}

	}


	###################################################################################
	/////////////////////////////////// User system ///////////////////////////////////
	###################################################################################


	public function member() {

		// $LevelApp = $this->Session->read('LevelApp');
		// if ($LevelApp['Level']['name'] != 'admin') {
		// 	$this->Session->write('alertType','danger');
		// 	$this->Session->setFlash('ไม่มีสิทธิ์เข้าถึงส่วนนี้');
		// 	$this->redirect(array('action' => 'index'));
		// }

		$Employees = $this->paginate('EmployeeTypeNew', array(
		));

		$output = '';

		foreach ($Employees as $Employee){

			$group = $this->EmployeeTypeNew->find('all', array(
				'conditions' => array('employee_id' => $Employee['Employee']['id'])
			));

			// if ($Employee['Level']['name'] == 'admin') {
				
			// }
			// $typenews = $this->Typenew->find('all', array(
			// 	'conditions' => array('id' => $Employee['EmployeeTypeNew']['typenew_id'])
			// ));
			$output .= 
				'<tr>
					<td>'.$Employee['Employee']['id'].'</td>
					<td>'.$Employee['Employee']['UserName'].'</td>
					<td style="text-align: left !important;">'.$Employee['Employee']['fname'].' '.$Employee['Employee']['lname'].'</td>					
					<td>'.$group[0]['Typenew']['TypeNews'].'</td>
					
					<td>
						<center><a href="edit_member/'.$Employee['EmployeeTypeNew']['employee_id'].'" class="btn btn-warning">
							<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
							แก้ไข
						</a>
						<a href="delete_member/'.$Employee['EmployeeTypeNew']['id'].'" class="btn btn-danger" onclick="return confirm_click();">
							<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
							ลบ
						</a>
						</center>
					</td>
				</tr>';
	  	}
	  	$this->set(array('output' => $output));
	  	//$this->set(array('group' => $group));
	  	//debug($group);
	  	#'.$group['TypeNew']['TypeNews'].'

	}

	public function add_member() {

		// $LevelApp = $this->Session->read('LevelApp');
		// if ($LevelApp['Level']['name'] != 'admin') {
		// 	$this->Session->write('alertType','danger');
		// 	$this->Session->setFlash('ไม่มีสิทธิ์เข้าถึงส่วนนี้');
		// 	$this->redirect(array('action' => 'index'));
		// }

		if($this->request->data){

			$this->request->data['EmployeeTypeNew']['employee_id'] = $this->request->data['Employee']['id'];
			// $this->request->data['LevelApp']['employee_id'] = $this->request->data['EmployeeTypeNew']['employee_id'];
			$this->EmployeeTypeNew->save($this->request->data);
			// $this->LevelApp->save($this->request->data);

			//debug($this->request->data);

			$this->Session->write('alertType','success');
			$this->Session->setFlash('บันทึกข้อมูล เรียบร้อยแล้ว');
			$this->redirect(array('action' => 'member'));
		}
		

			$Employees = $this->Employee->find('all', array(
				'fields' => array('id','fname','lname')
			));

			$output = '';
			foreach ($Employees as $Employee) {

				// $LevelApps = $this->LevelApp->find('first',array(
				// 	'conditions' => array(
				// 		'employee_id' => $Employee['Employee']['id'],'Application_id' => 1,
				// 	)
				// )); 
				// if (count($LevelApps) == 0) {
					$output .= '
						<option value="'.$Employee['Employee']['id'].' selected="selected">'.$Employee['Employee']['fname'].' '.$Employee['Employee']['lname'].'</option>
					';
				// }
			}
			
			$this->set(array('Employees' => $Employees)); 
			$this->set(array('output' => $output)); 

			// $levels = $this->Level->find('list', array(
			// 	'fields' => array('name')
			// ));	
			// $this->set('levels',$levels);

			$TypeNew = $this->Typenew->find('list', array(
    										'fields' => array('TypeNews')
			));
			$this->set(array('TypeNew' => $TypeNew));

			if(!$this->request->data){
				//debug($this->request->data);
				//$this->redirect(array('action' => 'member'));
			}	
		
	
	}

	public function edit_member($id = null)  {

		// $LevelApp = $this->Session->read('LevelApp');
		// if ($LevelApp['Level']['name'] != 'admin') {
		// 	$this->Session->write('alertType','danger');
		// 	$this->Session->setFlash('ไม่มีสิทธิ์เข้าถึงส่วนนี้');
		// 	$this->redirect(array('action' => 'index'));
		// }

		if($this->request->data){

			// $this->request->data['EmployeeTypeNew']['EmployeeId'] = $this->student['LevelApp']['EmployeeId'];
			$this->EmployeeTypeNew->save($this->request->data);
			

			$this->Session->write('alertType','success');
			$this->Session->setFlash('บันทึกข้อมูล เรียบร้อยแล้ว');
			$this->redirect(array('action' => 'member'));			
		}
	

			
			$EmployeeTypeNew = $this->EmployeeTypeNew->findByEmployeeId($id);
		
			$this->request->data = $EmployeeTypeNew;
			


			// $level = $this->Level->find('list', array(
			// 	'fields' => array('name')
			// ));	
			// $this->set('level',$level);

			$TypeNew = $this->Typenew->find('list', array(
    			'fields' => array('TypeNews')
			));
			$this->set(array(
				'TypeNew' => $TypeNew,
				'EmployeeTypeNew' => $EmployeeTypeNew
			));

			// $Application = $this->Application->find('list', array(
			// 								'conditions' => array('Application.Name' => 'Post'),
    		// 								'fields' => array('Name')
			// ));
			// $this->set(array('Application' => $Application));

			
		

	}

	public function delete_member($id=null) {
	
		$this->request->data = $this->EmployeeTypeNew->findById($id);
		if($this->request->data){

			if ($this->EmployeeTypeNew->delete($id)) {
								
				$this->Session->write('alertType','danger');
				$this->Session->setFlash('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>ลบข้อมูล เรียบร้อยแล้ว');
				$this->redirect(array('action' => 'member'));	
			} 
	}
		

	}
	public function AddMemberStatus(){
		if($this->request->data){
		
					//$this->request->data['EmployeeTypeNew']['employee_id'] = $this->request->data['Employee']['id'];
					// $this->request->data['LevelApp']['employee_id'] = $this->request->data['EmployeeTypeNew']['employee_id'];
					$this->Adminpost->save($this->request->data);
					// $this->LevelApp->save($this->request->data);
		
					//debug($this->request->data);
		
					$this->Session->write('alertType','success');
					$this->Session->setFlash('บันทึกข้อมูล เรียบร้อยแล้ว');
					$this->redirect(array('action' => 'AddMemberStatus'));
		}


		$Employees = $this->paginate('Adminpost', array(
		));

		$output = '';

		foreach ($Employees as $Employee){

			$group = $this->Adminpost->find('all', array(
				'conditions' => array(
					'employee_id' => $Employee['Employee']['id']
					)
			));

		// 	<a href="edit_member/'.$Employee['Adminpost']['employee_id'].'" class="btn btn-warning">
		// 	<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
		// 	แก้ไข
		// </a>
			$output .= 
				'<tr>
					<td>'.$Employee['Employee']['id'].'</td>
					<td>'.$Employee['Employee']['UserName'].'</td>
					<td style="text-align: left !important;">'.$Employee['Employee']['fname'].' '.$Employee['Employee']['lname'].'</td>					
					<td>'.$Employee['Adminpost']['employee_status_id'].'</td>
					
					<td>
						<center>
						<a href="delete_member_status/'.$Employee['Adminpost']['id'].'" class="btn btn-danger" onclick="return confirm_click();">
							<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
							ลบ
						</a>
						</center>
					</td>
				</tr>';
		}
		$this->set(array('output' => $output));

		$listEmployees = $this->Employee->find('all', array(
			'fields' => array('id','fname','lname')
		));

		$outputEmployee = '';
		foreach ($listEmployees as $listEmployee) {
				$outputEmployee .= '
					<option value="'.$listEmployee['Employee']['id'].' selected="selected">'.$listEmployee['Employee']['id'].' '.$listEmployee['Employee']['fname'].' '.$listEmployee['Employee']['lname'].'</option>
				';
			
		}
		
		$this->set(array(
			'outputEmployee' => $outputEmployee
		)); 
	}

	public function delete_member_status($id=null) {
	
		$this->request->data = $this->Adminpost->findById($id);
		if($this->request->data){

			if ($this->Adminpost->delete($id)) {
								
				$this->Session->write('alertType','danger');
				$this->Session->setFlash('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>ลบข้อมูล เรียบร้อยแล้ว');
				$this->redirect(array('action' => 'member'));	
			} 
		}
	}
	###################################################################################
	///////////////////////////////////// banner  /////////////////////////////////////
	###################################################################################



	public function banner($order = null) {
		if($this->request->data){

			if($this->request->data['Banner']['activity'] == 'plus'){

				$this->request->data['Banner']['order'] --;
				$edit = $this->Banner->findByOrder($this->request->data['Banner']['order']);

				$this->Banner->save($this->request->data);

				$edit['Banner']['order'] ++;
				$this->request->data['Banner']['id'] = $edit['Banner']['id'];
				$this->request->data['Banner']['order'] = $edit['Banner']['order'];

				$this->Banner->save($this->request->data);
				$this->redirect(array('action' => 'banner'));

			}elseif ($this->request->data['Banner']['activity'] == 'bate') {

				$this->request->data['Banner']['order']++;
				$edit = $this->Banner->findByOrder($this->request->data['Banner']['order']);

				$this->Banner->save($this->request->data);

				$edit['Banner']['order']--;
				$this->request->data['Banner']['id'] = $edit['Banner']['id'];
				$this->request->data['Banner']['order'] = $edit['Banner']['order'];

				$this->Banner->save($this->request->data);
				$this->redirect(array('action' => 'banner'));

			}elseif ($this->request->data['Banner']['activity'] == 'delete') {

				// $querys = $this->Banner->find('all' ,array(
				// 	'conditions' => array('order >' => $this->request->data['Banner']['order'],'type' => 'full'),
				// 	'order' => array('order' => 'ASC')
				// ));

				// $file = new File(WWW_ROOT.'img'.DS.'banner'.DS.$this->request->data['Banner']['image_name']);
				// if($file->delete());
				$this->request->data['Banner']['status'] = 0;
				$this->Banner->save($this->request->data);

				// foreach ($querys as $query) {

				// 	$this->request->data['Banner']['id'] = $query['Banner']['id'];
				// 	$query['Banner']['order']--;
				// 	$this->request->data['Banner']['order'] = $query['Banner']['order'];				
				// 	$this->Banner->save($this->request->data);
				// }
				$this->redirect(array('action' => 'banner'));

			}elseif ($this->request->data['Banner']['activity'] == 'show') {
	
				$edit = $this->Banner->findById($this->request->data['Banner']['id']);
				if($edit['Banner']['status'] == 1){
					$edit['Banner']['status'] = 0;
				}else{
					$edit['Banner']['status'] = 1;
				}
				$this->Banner->save($edit);
				$this->redirect(array('action' => 'banner'));

			}

			$this->Session->write('alertType','success');
			$this->Session->setFlash('บันทึกข้อมูล เรียบร้อยแล้ว');
			//$this->redirect(array('action' => 'banner'));

		}
		// $LevelApp = $this->Session->read('LevelApp');
		// if ($LevelApp['Level']['name'] != 'admin') {
		// 	$this->Session->write('alertType','danger');
		// 	$this->Session->setFlash('ไม่มีสิทธิ์เข้าถึงส่วนนี้');
		// 	$this->redirect(array('action' => 'index'));
		// }

		$Banners = $this->Banner->find('all' , array(
			'conditions' => array(
				'type' => 'full',
				'status' => 1
			),
			'order' => array('order' => 'DESC')
		));
		
		$this->set(array('Banners' => $Banners));

	}

	public function add_banner() {
	
		if($this->request->data){

			if($this->request->data['Banner']['image_file']['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif")){
				$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
				$this->Session->write('alertType','danger');
				$this->redirect('add_banner');
			}else{
				$this->request->data['Banner']['image_name'] = "";
				$this->request->data['Banner']['type'] = "full";
				$this->Banner->save($this->request->data);

				$nameId = $this->Banner->find('all' ,array(
					'order' => array('id' => 'DESC'),
					'limit' => 1
				));

				if($this->request->data['Banner']['image_file']){
				
					if(!file_exists(WWW_ROOT.'img'.DS.'banner'.DS))
						mkdir(WWW_ROOT.'img'.DS.'banner'.DS);

					list($name, $fileType) = explode(".", $this->request->data['Banner']['image_file']['name']);
					$this->request->data['Banner']['id'] = $nameId[0]['Banner']['id'];
					$this->request->data['Banner']['image_name'] = $nameId[0]['Banner']['id'].".".$fileType;


					move_uploaded_file($this->request->data['Banner']['image_file']['tmp_name'],WWW_ROOT.'img'.DS.'banner'.DS.$this->request->data['Banner']['image_name']);


					$this->Banner->save($this->request->data);
					$this->Session->write('alertType','success');
					$this->Session->setFlash('บันทึกข้อมูล เรียบร้อยแล้ว');
					$this->redirect(array('action' => 'banner',));

				}
			}
		}else{

			// $LevelApp = $this->Session->read('LevelApp');
			// if ($LevelApp['Level']['name'] != 'admin') {
			// 	$this->Session->write('alertType','danger');
			// 	$this->Session->setFlash('ไม่มีสิทธิ์เข้าถึงส่วนนี้');
			// 	$this->redirect(array('action' => 'index'));
			// }
			$last_order = $this->Banner->find('all', array(
				'fields' => array('order'),
				'conditions' => array('type' => 'full'),
				'order' => array('order' => 'DESC'),
				'limit' => 1
			));
			$this->set('last_order', $last_order);

		}
	
	}


	###################################################################################
	//////////////////////////////  Banner  mini  /////////////////////////////////////
	###################################################################################



	public function banner_mini($order = null) {

		if($this->request->data){

			if($this->request->data['Banner']['activity'] == 'plus'){

				$this->request->data['Banner']['order'] --;
				$edit = $this->Banner->find('all',array(
					'conditions' => array('order' => $this->request->data['Banner']['order'],'type' => 'mini')
				));
				debug($edit);

				$this->Banner->save($this->request->data);

				$edit['Banner']['order'] ++;
				$this->request->data['Banner']['id'] = $edit['Banner']['id'];
				$this->request->data['Banner']['order'] = $edit['Banner']['order'];

				$this->Banner->save($this->request->data);
				debug($this->request);

			}elseif ($this->request->data['Banner']['activity'] == 'bate') {

				$this->request->data['Banner']['order']++;
				$edit = $this->Banner->find('all',array(
					'conditions' => array('order' => $this->request->data['Banner']['order'],'type' => 'mini')
				));

				$this->Banner->save($this->request->data);

				$edit['Banner']['order']--;
				$this->request->data['Banner']['id'] = $edit['Banner']['id'];
				$this->request->data['Banner']['order'] = $edit['Banner']['order'];

				$this->Banner->save($this->request->data);

			}elseif ($this->request->data['Banner']['activity'] == 'delete') {

				
				$querys = $this->Banner->find('all' ,array(
					'conditions' => array('order >' => $this->request->data['Banner']['order'],'type' => 'mini'),
					'order' => array('order' => 'ASC')
				));

				$file = new File(WWW_ROOT.'img'.DS.'banner'.DS.$this->request->data['Banner']['image_name']);
				if($file->delete());
				
				$this->Banner->delete($this->request->data['Banner']['id']);

				foreach ($querys as $query) {

					$this->request->data['Banner']['id'] = $query['Banner']['id'];
					$query['Banner']['order']--;
					$this->request->data['Banner']['order'] = $query['Banner']['order'];				
					$this->Banner->save($this->request->data);
				}

			}elseif ($this->request->data['Banner']['activity'] == 'show') {
	
				$edit = $this->Banner->find('all',array(
					'conditions' => array('id' => $this->request->data['Banner']['id'],'type' => 'mini')
				));
				if($edit['Banner']['status'] == 1){
					$edit['Banner']['status'] = 0;
				}else{
					$edit['Banner']['status'] = 1;
				}
				$this->Banner->save($edit);

			}

			$this->Session->write('alertType','success');
			$this->Session->setFlash('บันทึกข้อมูล เรียบร้อยแล้ว');
			//$this->redirect(array('action' => 'banner_mini'));

		}
		// $LevelApp = $this->Session->read('LevelApp');
		// if ($LevelApp['Level']['name'] != 'admin') {
		// 	$this->Session->write('alertType','danger');
		// 	$this->Session->setFlash('ไม่มีสิทธิ์เข้าถึงส่วนนี้');
		// 	$this->redirect(array('action' => 'index'));
		// }

		$Banners = $this->Banner->find('all' , array(
			'conditions' => array('type' => 'mini'),
			'order' => array('order' => 'ASC')
		));
		$this->set(array('Banners' => $Banners));

	}

	public function add_banner_mini() {
	
		if($this->request->data){

			if($this->request->data['Banner']['image_file']['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif")){

				$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
				$this->Session->write('alertType','danger');
				$this->redirect('add_banner');
			}else{

				$this->request->data['Banner']['image_name'] = "";
				$this->request->data['Banner']['type'] = "mini";
				$this->Banner->save($this->request->data);

				$nameId = $this->Banner->find('all' ,array(
					'order' => array('id' => 'DESC'),
					'limit' => 1
				));

				if($this->request->data['Banner']['image_file']){
				
					if(!file_exists(WWW_ROOT.'img'.DS.'banner'.DS))
						mkdir(WWW_ROOT.'img'.DS.'banner'.DS);

					list($name, $fileType) = explode(".", $this->request->data['Banner']['image_file']['name']);
					$this->request->data['Banner']['id'] = $nameId[0]['Banner']['id'];
					$this->request->data['Banner']['image_name'] = $nameId[0]['Banner']['id'].".".$fileType;


					move_uploaded_file($this->request->data['Banner']['image_file']['tmp_name'],WWW_ROOT.'img'.DS.'banner'.DS.$this->request->data['Banner']['image_name']);


					$this->Banner->save($this->request->data);
					$this->Session->write('alertType','success');
					$this->Session->setFlash('บันทึกข้อมูล เรียบร้อยแล้ว');
					$this->redirect(array('action' => 'banner_mini',));

				}
			}
		}else{

			// $LevelApp = $this->Session->read('LevelApp');
			// if ($LevelApp['Level']['name'] != 'admin') {
			// 	$this->Session->write('alertType','danger');
			// 	$this->Session->setFlash('ไม่มีสิทธิ์เข้าถึงส่วนนี้');
			// 	$this->redirect(array('action' => 'index'));
			// }
			$last_order = $this->Banner->find('all', array(
				'fields' => array('order'),
				'conditions' => array('type' => 'mini'),
				'order' => array('order' => 'DESC'),
				'limit' => 1
			));
			$this->set('last_order', $last_order);

		}
	
	}
	###################################################################################
	//////////////////////////////  Youtube  /////////////////////////////////////
	###################################################################################
	public function YoutubeAll(){
		$Projectvdos = $this->paginate('Projectvdo' ,array(			
			'language_id' => 1
		));
		$this->set(array(		
			'Projectvdos' => $Projectvdos	
		));
	}
	public function AddProjectvdo() {
		$Usernames = $this->Session->read('UserName');
		if($this->request->data){
			date_default_timezone_set('Asia/Bangkok');
			$this->request->data['Projectvdo']['organize_id'] = $Usernames['Employee']['organize_id'];

			$newformat = date('Y-m-d', strtotime($this->request->data['Projectvdo']['postdate']));
			$this->request->data['Projectvdo']['postdate'] = $newformat;

			$mounth =  date('m', strtotime($this->request->data['Projectvdo']['postdate']));
			$this->request->data['Projectvdo']['mounth'] = $mounth;

			$year =  date('Y', strtotime($this->request->data['Projectvdo']['postdate']));
			$this->request->data['Projectvdo']['year'] = $year;

		
		

			$this->Projectvdo->save($this->request->data['Projectvdo']);

			$this->Session->write('alertType','success');
			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->redirect(array('action' => 'YoutubeAll'));

			}
			$listorganizes = $this->Organize->find('all',array(	
				'conditions' => array(
					'id' => $Usernames['Employee']['organize_id'],
				),		
				'fields' => array('Organize.id','Organize.name'),
				'group' => array('Organize.id'),
			));
			$this->set(array(							
				
					'listorganizes' => $listorganizes,
					
				));

	
	}
	public function EditProjectvdo($id = null) {
		$Usernames = $this->Session->read('UserName');
		if($this->request->data){
			date_default_timezone_set('Asia/Bangkok');
			$this->request->data['Projectvdo']['organize_id'] = $Usernames['Employee']['organize_id'];

			$newformat = date('Y-m-d', strtotime($this->request->data['Projectvdo']['postdate']));
			$this->request->data['Projectvdo']['postdate'] = $newformat;

			$mounth =  date('m', strtotime($this->request->data['Projectvdo']['postdate']));
			$this->request->data['Projectvdo']['mounth'] = $mounth;

			$year =  date('Y', strtotime($this->request->data['Projectvdo']['postdate']));
			$this->request->data['Projectvdo']['year'] = $year;

		
		

			$this->Projectvdo->save($this->request->data['Projectvdo']);

			$this->Session->write('alertType','success');
			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->redirect(array('action' => 'YoutubeAll'));
		}	
		
					$Projectvdos = $this->Projectvdo->findById($id);
					$this->request->data = $Projectvdos;
		
					$time = strtotime($Projectvdos['Projectvdo']['postdate']);
					$newformat = date('d-M-Y', $time);	
					$this->request->data['Projectvdo']['postdate'] = $newformat;
			
				
		
					
					
				
		
	}
	public function DeleteProjectvdo($id = null) {
		$this->request->data = $this->Projectvdo->findById($id);
		if($this->request->data){

			$this->Projectvdo->delete($id);

			$this->Session->write('alertType','danger');
			$this->Session->setFlash('ลบข้อมูล เรียบร้อยแล้ว');
			$this->redirect(array('action' => 'YoutubeAll'));
		}
	}
	###################################################################################
	//////////////////////////////  Events  /////////////////////////////////////
	###################################################################################
	public function EventAll(){
		
		$this->paginate = array(
			// 'conditions' => array(
			// 	'Info.typenew_id' => $type,
			// 	'language_id' => 1
			// ),
			// 'limit' => 10,			
			'order' => array('postdate,timestart,timeend' => 'ASC')
		);
		$events = $this->paginate('Event');
		
		$this->set(array(		
			'events' => $events	
		));
	}
	public function AddEvent() {
		$Usernames = $this->Session->read('UserName');
		if($this->request->data){
			date_default_timezone_set('Asia/Bangkok');
			//$this->request->data['Event']['organize_id'] = $Usernames['Employee']['organize_id'];

			$newformat = date('Y-m-d', strtotime($this->request->data['Event']['postdate']));
			$this->request->data['Event']['postdate'] = $newformat;
			$this->Event->save($this->request->data['Event']);

			$this->Session->write('alertType','success');
			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->redirect(array('action' => 'EventAll'));

			}
			// $listorganizes = $this->Organize->find('all',array(	
			// 	'conditions' => array(
			// 		'id' => $Usernames['Employee']['organize_id'],
			// 	),		
			// 	'fields' => array('Organize.id','Organize.name'),
			// 	'group' => array('Organize.id'),
			// ));
			// $this->set(array(							
				
			// 		'listorganizes' => $listorganizes,
					
			// 	));

	}
	public function EditEvent($id = null) {
		$Usernames = $this->Session->read('UserName');
		if($this->request->data){
			date_default_timezone_set('Asia/Bangkok');
			$this->request->data['Event']['organize_id'] = $Usernames['Employee']['organize_id'];

			$newformat = date('Y-m-d', strtotime($this->request->data['Event']['postdate']));
			$this->request->data['Event']['postdate'] = $newformat;

			
		
		

			$this->Event->save($this->request->data['Event']);

			$this->Session->write('alertType','success');
			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->redirect(array('action' => 'EventAll'));
		}	
		
			$Events = $this->Event->findById($id);
			$this->request->data = $Events;
		
			$time = strtotime($Events['Event']['postdate']);
			$newformat = date('d-M-Y', $time);	
			$this->request->data['Event']['postdate'] = $newformat;

			
			
			//debug($time2);
			$this->request->data['Event']['timestart'] = $Events['Event']['timestart'];
			$this->request->data['Event']['timeend'] = $Events['Event']['timeend'];
		
	}
	public function DeleteEvent($id = null) {
		$this->request->data = $this->Event->findById($id);
		if($this->request->data){

			$this->Event->delete($id);

			$this->Session->write('alertType','danger');
			$this->Session->setFlash('ลบข้อมูล เรียบร้อยแล้ว');
			$this->redirect(array('action' => 'EventAll'));
		}
	}
}
