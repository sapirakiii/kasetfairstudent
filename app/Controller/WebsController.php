<?php

App::uses('CakeEmail', 'Network/Email');
class WebsController extends AppController {
	
	private	$title_for_layout = 'Faculty of Agriculture, Chiang Mai University, คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่';
	private	$keywords = 'Vision :: Smart Agriculture towards Sustainable Development ';
	private	$keywordsTh = 'คณะเกษตรศาสตร์, มหาวิทยาลัยเชียงใหม่';
	private	$description = 'Vision :: Smart Agriculture towards Sustainable Development ';
	
	public $layout = 'web';
	 
	private $urls = '/smart_academic';
	private $urles = '/2017';
	public $vision = "Smart Agriculture towards Sustainable Development";
	public $paginate = array(
        'limit' => 25,
        'order' => array(
        	'id' => 'DESC'
        )
	);
    public $no_graduate = 58;
 
	public $opensystem = '2024-05-28 09:00:00'; // กำหนดวันเปิดระบบ
	public $closesystem = '2024-05-29 16:30:00';  // กำหนดวันปิดระบบ
 

	public $uses = array('Info','InfoDocument','Banner','Projectvdo','Event','Manager','Typemanager',
	'Employee','Prefix','Organize','Duty','Expert','Boss','Typeboard','Board','Division','Typedivision','Position',
	'Download','Typedocument','Infopage' ,'InfoDocumentpage','Typepage','Typeactivity','Mainmenu' ,'MainInfoDocumentpage','Typemenu',
	'Contentmenu' ,'ContentInfoDocumentpage','Question','Answer','Student','Major','Degree',
	'Admission','Studentgroup','Advisor','Studentterm','Yearterm','Complaint','ComplaintInfopage','Typeunitcomplaint',
	'Typeomplaint','AnswerComplaint','UnitType','Department','Typestatuscomplaint','Payment','Paymenttype','PersonPaymenttype','Status',
	'Student','Studentterm','StudenttermDate','StudenttermDailyDate','GraduateSeat','AdmissionCourse','VoteEmployee','MisEmployee','VoteEmployeeFinal',
	'Graduategoodcert','CurriculumInfo','CurriculumMisEmployee','Show','StudentDirectAdmission','StudentDirectAdmissionType',
	'District','Amphur','Province','Approve','StudentDirectAdmissionCopy');
	
	public function beforeFilter() {
		
		$this->set(array(
						'title_for_layout' => $this->title_for_layout,
						'keywords' => $this->keywords,
						'keywordsTh' => $this->keywordsTh,
						'description' => $this->description,
						'no_graduate' => $this->no_graduate,
						'vision' => $this->vision
						));
		
		
	}
	public function  DateDiff($strDate1,$strDate2){                             
		return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
	}
	public function logout() {
		$this->Session->delete('UserName');
		$this->Session->delete('id');
		$this->Session->delete('Students');
		$this->Session->delete('admins');
		$this->Session->delete('keycode');
		
		$this->redirect(array('controller' => 'webs','action' => 'index'));
	}
	public function logoutcomplaint() {		
		$this->Session->delete('keycode');
		
		$this->redirect(array('controller' => 'webs','action' => 'add_complaint'));
	}
	
	 
	
	public function logoutgraduate() {
	 
		$this->Session->delete('students');
		$this->Session->delete('profiles');
		$this->Session->delete('graduateSeats');
	 
		 
		$this->redirect(array('controller' => 'webs','action' => 'indexgraduate'));
	}
	public function logins() {		
		$this->layout = 'login';

		$students = $this->Session->read('students');
		$studentLevelApp = $this->Session->read('studentLevelApp');
		 
		if(isset($students)){
			$this->set('students', $students);
			 

			if($this->action == 'logins')
					$this->redirect(array('action' => 'list_all_graduate'));
		}
		else{
			if($this->action != 'logins')				
				$this->redirect(array('controller' => 'webs','action' => 'logoutgraduate'));
		}	
		if($this->request->is('post')){
			$students = $this->Student->find('first', array(
				'conditions' => array(
					'Student.id' => $this->request->data['id'],
				 
					)
			));			
		 
			if (count($students) > 0) {
				$Yearterms = $this->Yearterm->find('first',array(
					'conditions' => array(				
						 'level' => 1
					),
					'order' => array('Yearterm.id' => 'DESC')
				));
				$profiles = $this->Student->find('first', array(
					'conditions' => array(
						'Student.id' => $students['Student']['id'],	
							
					)
				));
				$graduateSeats = $this->GraduateSeat->find('first',array(
					'conditions' => array(				
						'GraduateSeat.student_id' => $profiles['Student']['id'],
						'GraduateSeat.no_graduate' => $this->no_graduate,		
					),
					'order' => array('GraduateSeat.id' => 'ASC'),					
		
				));
				 
							 
				$this->Session->write('students', $students);
				$this->Session->write('profiles', $profiles);
				 
				$this->Session->write('graduateSeats', $graduateSeats);
				
				$this->redirect(array('controller' => 'webs','action' => 'list_all_graduate'));
					 
			}else{
				$this->Session->setFlash('ขออภัย สำหรับบัณฑิตที่เข้ารับพระราชทานปริญญาบัตร ครั้งที่ 55 เท่านั้น!!!');
				$this->Session->write('alertType','danger');
				$this->redirect(array('controller' => 'webs','action' => 'logoutgraduate'));
			}
		}
	}
	public function access_denied(){
		$this->layout = '';
	}
	public function list_all_graduate(){ 
		$this->layout = 'web4';
		$students = $this->Session->read('students');
		$profiles = $this->Session->read('profiles');
		$graduateSeats = $this->Session->read('graduateSeats');
		// $Graduategoodcerts = $this->Session->read('Graduategoodcerts');
		$Yearterms = $this->Yearterm->find('first',array(
			'conditions' => array(				
				 'level' => 1
			),
			'order' => array('Yearterm.id' => 'DESC')
		));
		if ($this->request->data) {
			//*************Graduategoodcert**************** */
			$listgoodcerts = $this->Graduategoodcert->find('first' ,array(
				'conditions' => array(
					'Graduategoodcert.student_id' => $students['Student']['id'],	
					'yearterm_id' => $Yearterms['Yearterm']['id'],												
				),						
			));
			//debug($listgoodcerts);
			date_default_timezone_set('Asia/Bangkok');

			$student_mobile = $this->request->data['Graduategoodcert']['student_mobile'];
			 
			$data = array(
			   'id' => $listgoodcerts['Graduategoodcert']['id'],
			   'student_mobile' => $student_mobile,
			   'show_id' => $this->request->data['Graduategoodcert']['show_id'],
			);
			$this->Graduategoodcert->save($data);
			 
			 


			$this->Session->setFlash('<i class="glyphicon glyphicon-ok-sign"></i> ยืนยันข้อมูลบัณฑิตเรียบร้อยแล้ว');
			$this->Session->write('alertType','success');	
		 
	
		}
	 
		$goodcerts = $this->Graduategoodcert->find('first', array(
			'conditions' => array(
				'Graduategoodcert.student_id' => $students['Student']['id'],
				'Graduategoodcert.yearterm_id' => $Yearterms['Yearterm']['id'], 
						
			),
		));	
		$infopages = $this->Infopage->findById(225);

		$CountRead = $infopages['Infopage']['CountRead'] + 1;
		$data = array('id' => 225 ,'CountRead' => $CountRead);
		$this->Infopage->save($data);

		$listinfopage  = $this->Infopage->find('first',array(
			'conditions' => array(				
				'Infopage.id' => 225
			),
		));
		$shows = $this->Show->find('list',array(
			'conditions' => array(
				'level' => 1	
			),		
			// 'fields' => array('Show.id,Show.name'),
		));	
		$this->set(array(
			'listinfopage' => $listinfopage ,
			'goodcerts' => $goodcerts,
			'shows' => $shows
		));
		//==================================== 
	}
	public function information($lang = 'th'){ $this->set(array('lang' => $lang)); }
	public function contactus($lang = 'th'){ $this->set(array('lang' => $lang)); }	
	public function history($lang = 'th') { $this->set(array('lang' => $lang)); }
	public function organization($lang = 'th'){ $this->set(array('lang' => $lang)); }
 
	public function underconstruction($lang = 'th'){$this->layout ="";  $this->set(array('lang' => $lang)); }
	
	//********************************* */.
	public function index3(){ $this->layout = 'web2'; }
	public function index4(){ $this->layout = 'web2'; }
	public function indexfreshy(){
		 $this->layout = 'web4';
		 $infopages = $this->Infopage->findById(264);

		 $CountRead = $infopages['Infopage']['CountRead'] + 1;
		 $data = array('id' => 264 ,'CountRead' => $CountRead);
		 $this->Infopage->save($data);
 
		 $listinfopage  = $this->Infopage->find('first',array(
			 'conditions' => array(				
				 'Infopage.id' => 264
			 ),
		 ));
		 
		 $this->set(array(
			 'listinfopage' => $listinfopage ,
		 ));
	}
	public function graduate(){
		$this->layout = 'webgraduate';
		 
	}
	public function form_poll(){ 
		$this->layout = ''; 
	}
	 
	 
	
	
} // CLASS
