<?php
class PollController extends AppController {
	public $uses = array('Gender','Major','Question','Rate'); 
	private $urls = '/kasetfairstudent';

	public function index() {

	}
    public function poll1() {
		$genders = $this->Gender->find('list');
		$majors = $this->Major->find('list');
		$questions1 = $this->Question->find('all', array(
			'conditions' => array( 'Question.order_n' => 1),
			'order' => array('Question.order_n'),
		));
		$questions2 = $this->Question->find('all', array(
			'conditions' => array( 'Question.order_n' => 2),
			'order' => array('Question.order_n'),
		));
		$rates = $this->Rate->find('list');

		$this->set(array(
			'genders' => $genders,
			'majors' => $majors,
			'questions1' => $questions1,
			'questions2' => $questions2,
			'rates' => $rates,
		));
	} 
}