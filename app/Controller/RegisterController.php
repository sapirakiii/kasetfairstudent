<?php
class RegisterController extends AppController {
	public $components = array('Cookie', 'Session');
	public $paginate = array(
        'limit' => 25,
        'order' => array(
        	'id' => 'DESC'
        )
	);
	public $uses = array('Project','Member','Prefix','Source','MemberSource','Status','Info','InfoDocument' 
							,'ProjectFlowerGroup','MemberFlowerGroup','Dateguide','Guide'
							,'ProjectAquaticGroup','MemberAquaticGroup','Event','MemberDocument','GuideSource'
							,'ComputerEquipment','TeachingAid','EquipmentTeaching','ComputerEquipmentDocument'
							,'ComType','ComUse','MisEmployee','MisPosition','MisOrganize'
							,'Barista','BaristaDocument','Infopage'); 
	private $urls = '/kasetfairstudent';	
	public $layout = 'registers';	 
	public $max_barista = 74;
	public function beforeFilter() {
		date_default_timezone_set("Asia/Bangkok");
		$this->Session->write('max_barista', $this->max_barista);
	 
		 
	}
 
	//------------------------Barista----------------------------
	public function barista_list(){
		$UserName = $this->Session->read('MisEmployee');
		$max_barista = $this->Session->read('max_barista');
		$this->set('UserName', $UserName);

		$baristas = $this->Barista->find('all', array(
			// 'conditions' => array('Guide.dateguide_id' => $Id),
			'order' => array('Barista.id' => 'ASC')
		));	
		
		$countbaristas = $this->Barista->find('count', array(
			// 'conditions' => array('Guide.dateguide_id' => $Id),
			'order' => array('Barista.id' => 'ASC')
		));	

		$this->set(array(
			'baristas' => $baristas,
			'countbaristas' => $countbaristas,
			'max_barista' => $max_barista
		));
	}

	public function barista_register(){
		$max_barista = $this->Session->read('max_barista');
		if($this->request->data){
			$this->Barista->save($this->request->data);

			$Baristas = $this->Barista->find('first',array(
				'conditions' => array(
				// 'CurriculumRequest.mis_employee_id' => $UserName['MisEmployee']['id'] ,
				),
				'order' => array('Barista.id' => 'DESC')
			));
				
			
			$files = $this->request->data['BaristaDocument']['files'];	
			if ($this->request->data['BaristaDocument']['files'][0]['type'] != null) {
				foreach ($this->request->data['BaristaDocument']['files'] as $check) {
					if($check['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or 
										"image/gif" or "application/msword" or 
										"application/vnd.openxmlformats-officedocument.wordprocessingml.document" or 
										"application/x-rar-compressed" or "application/octet-stream" or "application/zip" or 
										"application/octet-stream" or "application/vnd.ms-powerpoint" or 
										"application/vnd.openxmlformats-officedocument.presentationml.presentation" or 
										"application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")){
							foreach ($files as $file) {
								$fileType = pathinfo($file['name'],PATHINFO_EXTENSION);
								if($fileType != "doc" or "docx" or "jpg" or "jpeg" or "png" or "gif" or "zip" or "rar" or "xls" or 
												"xlsx" or "ppt" or "pptx" or "pdf"){
									$this->Session->setFlash('กรุณาแก้ไขชื่อไฟล์ก่อนอัปโหลด โดยไม่ให้มีอักขระ จุด .');
									$this->Session->write('alertType','danger');
											
									$this->redirect(array('controller' => 'curriculumRequests','action' => 'add_proposals'));
								}
							}
								$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
								$this->Session->write('alertType','danger');		
					}
				}
			}
		 
			if($files[0]['name'] != null){
				$i = 0;
				foreach ($files as $file) { 
					$fileType = pathinfo($file['name'],PATHINFO_EXTENSION);
					$this->request->data['BaristaDocument']['name'] = date('Y-m-d His')."_".$file['name'];
					$this->request->data['BaristaDocument']['name_old'] = $file['name'];	
					$this->request->data['BaristaDocument']['post_date'] = date('Y-m-d H:i:00');		
					$this->request->data['BaristaDocument']['barista_id'] = $Baristas['Barista']['id'];
		
					$filethai = iconv("UTF-8", "TIS-620",  date('Y-m-d His')."_".$file['name']);
					if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and 
						$fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and 
						$fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and 
						$fileType != 'WebP' and $fileType != 'TIFF') {
						$this->request->data['BaristaDocument']['class'] = "file";
					}else{
						$this->request->data['BaristaDocument']['class'] = "img";
					}
									
					$folder = WWW_ROOT.'files'.DS.'barista'.DS.$Baristas['Barista']['id'].DS;
				
					if(!file_exists($folder))
						mkdir($folder);
										
						move_uploaded_file($file['tmp_name'],$folder.$filethai);
						// $files[0]['tmp_name']
									
					$this->BaristaDocument->create();					
					$this->BaristaDocument->save($this->request->data);	
							
				}

			} 
			$this->Session->write('alertType','success');
			$this->Session->setFlash('บันทึกการลงทะเบียนแข่งขัน เรียบร้อยแล้ว');
			$this->redirect(array('action' => 'barista_list'));
		}
		$prefixes = $this->Prefix->find('list', array(
			'conditions' => array(
				// 'and' => array(
				// 	array('Prefix.level <=' => 2),
				// 	array('Prefix.level >=' => 1),
				// 	),
				'level' => 1
			),
			'order' => array('id' => 'ASC')
			
		));	
		$countbaristas = $this->Barista->find('count', array(
			// 'conditions' => array('Guide.dateguide_id' => $Id),
			'order' => array('Barista.id' => 'ASC')
		));	
		$this->set(array(
			'prefixes' => $prefixes,
			'countbaristas' => $countbaristas,
			'max_barista' => $max_barista
		));
	}
 
	public function barista_detail($baristaId = null){
		$this->set(array('baristaId' => $baristaId));
		$barista = $this->Barista->findById($baristaId);

		$baristadocument = $this->BaristaDocument->find('all',array(
			'conditions' => array(
				'BaristaDocument.barista_id' => $barista['Barista']['id'] ,
			),
			'order' => array('BaristaDocument.id' => 'DESC')
		));

		// debug($checkId);

		$this->set(array(
			'barista' => $barista,
			'baristadocument' => $baristadocument,
		));
	}
	public function training_list(){
		 
	}
}