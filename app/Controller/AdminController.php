<?php
class AdminController extends AppController {
	
	public $uses; 
	
	private $admin;
	
	public function beforeFilter() {		
		date_default_timezone_set("Asia/Bangkok");
		
		$this->admin = $this->Session->read('admin');
				
		if(!isset($this->admin)){		
			if($this->action != 'login'){					
				$this->redirect(array('action' => 'login'));
			}				
		}			
	}
	
	public function index() {
			
	}
	
	public function login() {
		$this->Session->delete('admin');
		
		if($this->request->data){			

			if($this->request->data['Admin']['username'] == 'admin' && $this->request->data['Admin']['password'] == '1234'){
				$this->Session->write('admin',true);
				$this->redirect(array('action' => 'index'));
			}
			else{

				$alert = 'Username หรือ Password ไม่ถูกต้อง, กรุณาลองใหม่อีกครั้ง';
								
				$this->Session->setFlash($alert);
			}

		}
		
	}	
	
	public function logout() {		
		$this->redirect(array('action' => 'login'));
	}

}
