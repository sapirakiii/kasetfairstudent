<?php
class GuestController extends AppController
{
	public $components = array('Cookie', 'Session');
	public $paginate = array(
		'limit' => 25,
		'order' => array(
			'id' => 'DESC'
		)
	);
	public $uses = array(
		'CurriculumInterInfo',
		'LifeLongCurriculum',
		'Project',
		'Member',
		'LifeLongInfo',
		'StudentTraining',
		'CurriculumInterInfo',
		'Satisfaction',
		'Employment',
		'Prefix',
		'Source',
		'MemberSource',
		'Status',
		'Info',
		'InfoDocument',
		'ProjectFlowerGroup',
		'MemberFlowerGroup',
		'Dateguide',
		'Guide',
		'Guidet',
		'ProjectAquaticGroup',
		'MemberAquaticGroup',
		'Event',
		'MemberDocument',
		'GuideSource',
		'ComputerEquipment',
		'TeachingAid',
		'EquipmentTeaching',
		'ComputerEquipmentDocument',
		'ComType',
		'ComUse',
		'MisEmployee',
		'MisPosition',
		'MisOrganize',
		'Barista',
		'BaristaDocument',
		'Major',
		'Majordegree',
		'Paper',
		'MemberStudent',
		'MemberStudentDocument',
		'GuidePeople',
		'MemberProject',
		'Organize',
		'Advisor',
		'Yearterm',
		'StudentGraduatePlan',
		'CaptchaRegister',
		'StudentAssistantReceive',
		'StudentCommonEuropean',
		'StudentCitizentation',
		'StudentSmartAgri',
		'StudentProject',
		'StudentTravel',
		'StudentDna',
		'StudentManufacture',
		'Notification',
		'CurriculumInfo',
		'Guest'
	);

	private $urls = '/kasetfairstudent';

	public function beforeFilter()
	{
		date_default_timezone_set("Asia/Bangkok");
	}

	public function InfoDetail($id = null)
	{

		$news = $this->request->data = $this->Info->findById($id);
		$this->set(array('news' => $news));

		$CountRead = $news['Info']['CountRead'] + 1;
		$data = array('id' => $id, 'CountRead' => $CountRead);
		$this->Info->save($data);
		//==================================	
		$new = $this->Info->find('first', array(
			'conditions' => array(
				'Info.id' => $id,
			),

		));
		$this->set(array('new' => $new));


		if (!$this->request->data) {
			$this->redirect(array('action' => 'index'));
		}

		//$read = $this->Info->find('fist' ,array('conditions' => array('id' => $id)))


	}
	public function login()
	{
		// $this->layout = 'login';
		$date = date("Y-m-d");
		$time = date("H:i:s");
		$this->redirect(array('action' => 'index'));

		//$UserName = $this->Session->read('MisEmployee');



		// if(isset($UserName)){
		// 	$this->set('UserName', $UserName);

		// }
		// else{
		// 	if($this->action != 'login')
		// 		//$this->redirect(array('controller' => 'mains','action' => 'login'));

		// 	$misPositions = $this->MisPosition->find('first' , array(
		// 		'conditions' => array(
		// 			'MisPosition.id' => $UserName['MisEmployee']['mis_position_id'],	
		// 		),				
		// 		'order' => array('MisPosition.id' => 'DESC')
		// 	));
		// }



		// if (count($UserName) > 0) {



		// 	$this->redirect(array('action' => 'index'));


		// }else{
		// 	$this->Session->setFlash('ชื่อผู้ดูแลระบบ หรือ รหัสผ่านผู้ดูแลระบบ ไม่ถูกต้อง!!!');
		// }




	}
	public function logout()
	{
		$this->Session->delete('studentId');
		$this->Session->delete('students'); // from main session
		$this->Session->delete('UserName');
		$this->Session->delete('id');
		$this->Session->delete('Students');
		$this->Session->delete('MisEmployee');
		$this->Session->delete('cmuusers');
		// $this->Session->write('alertType','success');
		// $this->Session->setFlash('Logout Complete');
		$this->redirect('https://agri.cmu.ac.th/2017/webs/index/th/1');
	}
	public function index()
	{
		// $this->redirect(array('action' => 'program'));


		$UserName = $this->Session->read('MisEmployee');
		$this->set('UserName', $UserName);

		$projects = $this->Project->find('all', array(
			'conditions' => array(
				'Project.type_id' => 3,
			),
			'order' => array('Project.order' => 'ASC')
		));
		// "select * from project where type_id = 3 "    ----- all
		//

		$countProjects = $this->Project->find('count', array(
			'conditions' => array(
				'Project.type_id' => 3
			),
			'order' => array('Project.id' => 'ASC')
		));
		// count
		// list
		$this->set(array('projects' => $projects));

		//******************* */
		$countMembersList = array();


		if ($projects == null) {
			# code...
		} else {
			foreach ($projects as $project) {
				$countMembers = $this->Member->find('count', array(
					'conditions' => array(
						'Member.project_id' => $project['Project']['id'],
					)
				));

				array_push($countMembersList, $countMembers);
			}

			$this->set(array(
				'countMembersList' => $countMembersList
			));
		}
		$countMembersList2 = array();

		$MemberProjects = $this->MemberProject->find('all', array(
			'conditions' => array(),
			'order' => array('MemberProject.order' => 'ASC')
		));

		// debug($MemberProjects);
		if ($MemberProjects == null) {
			# code...
		} else {

			$countMember2s = $this->MemberStudent->find('count', array(
				'conditions' => array()
			));
		}

		// array_push($countMembersList2,$countMember2s); 

		$i = 1;
		while ($i <= 6) {
			if ($i == 6) {
				${'teamCount' . $i . '1s'} = $this->Member->query("select count(distinct created) from kasetfairstudent.members 
				where project_id=" . $i . " and right(level,1) between 1 and 3");
				$this->set(array(('teamCount' . $i . '1s') => ${'teamCount' . $i . '1s'}));
				${'teamCount' . $i . '2s'} = $this->Member->query("select count(distinct created) from kasetfairstudent.members 
				where project_id=" . $i . " and right(level,1) between 4 and 6");
				$this->set(array(('teamCount' . $i . '2s') => ${'teamCount' . $i . '2s'}));
			} else {
				${'teamCount' . $i . 's'} = $this->Member->query("select count(distinct created) from kasetfairstudent.members where project_id=" . $i);
				$this->set(array(('teamCount' . $i . 's') => ${'teamCount' . $i . 's'}));
			}

			$i++;
		}



		$this->set(array(
			'MemberProjects' => $MemberProjects,
			'countMember2s' => $countMember2s

		));
	}
	public function student_assistant()
	{
		$student_assistant_receives = $this->StudentAssistantReceive->find('all', array(
			'conditions' => array(),
		));
		$this->set(array(
			'student_assistant_receives' => $student_assistant_receives
		));
	}
	public function captcha()
	{
		$secret_key = "6LdFnBMqAAAAAL3la12qnokvISo8it_983G2syay";
		if (isset($_POST['g-recaptcha-response'])) {
			$recaptcha = $_POST['g-recaptcha-response'];
			$verify = file_get_contents('https://google.com/recaptcha/api/siteverify?secret=' . $secret_key . '&response=' . $recaptcha);
			$recapt_chasuccess = json_decode($verify);
			if (!$recaptcha) {
				$this->Session->setFlash('คุณไม่ได้ป้อน reCAPTCHA อย่างถูกต้อง');
				$this->Session->write('alertType', 'danger');
				$this->redirect('captcha');
			}
		} {
			$captcha = $this->CaptchaRegister->find('all', array(
				'conditions' => array(),
			));
			$this->set(array(
				'CaptchaRegister' => $captcha
			));
			if ($this->request->data && $recapt_chasuccess->success) {
				$this->CaptchaRegister->save($this->request->data);
				$this->Session->setFlash('success');
				$this->Session->write('alertType', 'success');
				$this->redirect('captcha');
			}
		}
	}

	public function list_life_long($yearList = null) //1.1.2 & 1.1.3
	{
		$UserName = $this->Session->read('MisEmployee');
		$admins = $this->Session->read('adminStudents');
		$this->set(array(
			'UserName' => $UserName,
			'admins' => $admins,
		));
		$dropdowns = $this->Yearterm->find('all', array(
			'conditions' => array(
				'Yearterm.level' => 1,
			),
			'group' => array('Yearterm.year'),
			'order' => array('Yearterm.id' => 'DESC')
		));
		if ($yearList == null) {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('id' => 'DESC')
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		} else {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
					'Yearterm.level' => 1,
				)
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		}
		$this->set(array(
			'years' => $years,
			'dropdowns' => $dropdowns,
			'lastyearterms' => $lastyearterms
		));
		$lifelonginfos1 = $this->LifeLongInfo->find('all', array(
			'conditions' => array(
				'and' => array(
					array('LifeLongInfo.month <=' => 12),
					array('LifeLongInfo.month >=' => 10),
				),
				'LifeLongInfo.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('LifeLongInfo.year' => 'ASC'),
		));
		$lifelonginfos2 = $this->LifeLongInfo->find('all', array(
			'conditions' => array(
				'and' => array(
					array('LifeLongInfo.month <=' => 3),
					array('LifeLongInfo.month >=' => 1),
				),
				'LifeLongInfo.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('LifeLongInfo.year' => 'ASC'),
		));
		$lifelonginfos3 = $this->LifeLongInfo->find('all', array(
			'conditions' => array(
				'and' => array(
					array('LifeLongInfo.month <=' => 6),
					array('LifeLongInfo.month >=' => 4),
				),
				'LifeLongInfo.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('LifeLongInfo.year' => 'ASC'),
		));
		$lifelonginfos4 = $this->LifeLongInfo->find('all', array(
			'conditions' => array(
				'and' => array(
					array('LifeLongInfo.month <=' => 9),
					array('LifeLongInfo.month >=' => 7),
				),
				'LifeLongInfo.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('LifeLongInfo.year' => 'ASC'),
		));

		$this->set(array(
			'listlifelongs1' => $lifelonginfos1,
			'listlifelongs2' => $lifelonginfos2,
			'listlifelongs3' => $lifelonginfos3,
			'listlifelongs4' => $lifelonginfos4,
			'lastyearterms' => $lastyearterms
		));
	}
	public function add_life_long()
	{
		if ($this->request->data) {

			$this->LifeLongInfo->save($this->request->data);

			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('list_life_long');
		}
	}
	public function edit_life_long($id = null)
	{
		if ($this->request->data) {

			$this->LifeLongInfo->save($this->request->data);

			$this->Session->setFlash('แก้ไขข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('list_life_long');
		}

		$lifeLongInfos = $this->LifeLongInfo->find('first', array(
			'conditions' => array(
				'LifeLongInfo.id' => $id,
			),

		));


		$this->request->data = $lifeLongInfos;


		$this->set(array(
			'lifeLongInfos' => $lifeLongInfos,

		));
	}
	public function remove_life_long($id = null)
	{
		$this->LifeLongInfo->delete($id);
		$this->Session->write('alertType', 'danger');
		$this->Session->setFlash('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>ลบข้อมูล เรียบร้อยแล้ว');

		$this->redirect('list_life_long');
	}
	public function edit_life_long_learning($id = null)
	{
		if ($this->request->data) {

			$this->LifeLongCurriculum->save($this->request->data);

			$this->Session->setFlash('แก้ไขข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('life_long_learning');
		}

		$LifeLongCurriculums = $this->LifeLongCurriculum->find('first', array(
			'conditions' => array(
				'LifeLongCurriculum.id' => $id,
			),

		));


		$this->request->data = $LifeLongCurriculums;


		$this->set(array(
			'LifeLongCurriculums' => $LifeLongCurriculums,

		));
	}
	public function life_long_learning($yearList = null) //1.1.10
	{
		$UserName = $this->Session->read('MisEmployee');
		$admins = $this->Session->read('adminStudents');
		$this->set(array(
			'UserName' => $UserName,
			'admins' => $admins,
		));
		$dropdowns = $this->Yearterm->find('all', array(
			'conditions' => array(
				'Yearterm.level' => 1,
			),
			'group' => array('Yearterm.year'),
			'order' => array('Yearterm.id' => 'DESC')
		));
		if ($yearList == null) {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('id' => 'DESC')
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		} else {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
					'Yearterm.level' => 1,
				)
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		}
		$this->set(array(
			'years' => $years,
			'dropdowns' => $dropdowns,
			'lastyearterms' => $lastyearterms
		));
		$life_long_learnings1 = $this->LifeLongCurriculum->find('all', array(
			'conditions' => array(
				'and' => array(
					array('LifeLongCurriculum.month <=' => 12),
					array('LifeLongCurriculum.month >=' => 10),
				),
				'LifeLongCurriculum.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('LifeLongCurriculum.year' => 'ASC'),
		));
		$life_long_learnings2 = $this->LifeLongCurriculum->find('all', array(
			'conditions' => array(
				'and' => array(
					array('LifeLongCurriculum.month <=' => 3),
					array('LifeLongCurriculum.month >=' => 1),
				),
				'LifeLongCurriculum.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('LifeLongCurriculum.year' => 'ASC'),
		));
		$life_long_learnings3 = $this->LifeLongCurriculum->find('all', array(
			'conditions' => array(
				'and' => array(
					array('LifeLongCurriculum.month <=' => 6),
					array('LifeLongCurriculum.month >=' => 4),
				),
				'LifeLongCurriculum.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('LifeLongCurriculum.year' => 'ASC'),
		));
		$life_long_learnings4 = $this->LifeLongCurriculum->find('all', array(
			'conditions' => array(
				'and' => array(
					array('LifeLongCurriculum.month <=' => 9),
					array('LifeLongCurriculum.month >=' => 7),
				),
				'LifeLongCurriculum.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('LifeLongCurriculum.year' => 'ASC'),
		));

		$this->set(array(
			'life_long_learnings1' => $life_long_learnings1,
			'life_long_learnings2' => $life_long_learnings2,
			'life_long_learnings3' => $life_long_learnings3,
			'life_long_learnings4' => $life_long_learnings4,
			'lastyearterms' => $lastyearterms,
			'years' => $years,
			'dropdowns' => $dropdowns,
		));
	}
	public function add_life_long_learning()
	{
		if ($this->request->data) {
			$this->LifeLongCurriculum->save($this->request->data);

			$this->redirect('life_long_learning');
			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
		}
	}
	public function remove_life_long_learning($id = null)
	{
		$this->LifeLongCurriculum->delete($id);
		$this->Session->write('alertType', 'danger');
		$this->Session->setFlash('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>ลบข้อมูล เรียบร้อยแล้ว');
		$this->redirect('life_long_learning');
	}
	public function employment_data($yearList = null, $year = null)
	{
		// $UserName = $this->Session->read('MisEmployee');  
		// $admins = $this->Session->read('adminStudents');
		// $this->set(array(
		// 'UserName' => $UserName,
		// 'admins' => $admins,
		// ));
		$dropdowns = $this->Yearterm->find('all', array(
			'conditions' => array(
				'Yearterm.level' => 1,
			),
			'group' => array('Yearterm.year'),
			'order' => array('Yearterm.id' => 'DESC')
		));
		if ($yearList == null) {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('id' => 'DESC')
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		} else {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
					'Yearterm.level' => 1,
				)
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		}
		$this->set(array(
			'years' => $years,
			'dropdowns' => $dropdowns,
			'lastyearterms' => $lastyearterms
		));

		if ($year == null) {
			$employments = $this->Employment->find('all', array(
				'conditions' => array(
					'Employment.year' => $lastyearterms['Yearterm']['year'],
					'Employment.degree_name' => 'ปริญญาตรี',
				),
				'order' => array('Employment.degree_name' => 'ASC'),
			));
		} else {
			$employments = $this->Employment->find('all', array(
				'conditions' => array(
					'Employment.year' => $year,
					// 'Employment.year' => $lastyearterms['Yearterm']['year'],
					'Employment.degree_name' => 'ปริญญาตรี',
				),
				'order' => array('Employment.degree_name' => 'ASC'),
			));
		}

		$this->set(array(
			'employment_datas' => $employments,
			'year' => $year
		));
	}
	// public function add_employment_data()
	// {
	// 	if ($this->request->data)
	// 	{
	// 		$this->Employment->save($this->request->data);
	// 		$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
	// 		$this->Session->write('alertType','success');
	// 		$this->redirect('employment_data');
	// 	}
	// }
	// public function edit_employment_data($id = null)
	// {
	// 	if ($this->request->data)
	// 	{
	// 		$this->Employment->save($this->request->data);
	// 		$this->Session->setFlash('แก้ไขข้อมูล เรียบร้อยแล้ว');
	// 		$this->Session->write('alertType','success');
	// 		$this->redirect('employment_data');
	// 	}

	// 	$Employment_datas = $this->Employment->find('first',array(
	// 		'conditions' => array(
	// 			'Employment.id'=> $id,
	// 		),
	// 	));

	// 	$this->request->data = $Employment_datas;
	// 	$this->set(array(
	// 		'Employment' => $Employment_datas
	// 	));

	// }
	// public function remove_employment_data($id = null)
	// {
	// 	$this->Employment->delete($id);
	// 	$this->Session->write('alertType','danger');
	// 	$this->Session->setFlash('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>ลบข้อมูล เรียบร้อยแล้ว');
	// 	$this->redirect('employment_data');
	// }
	public function satisfaction($yearList = null) //1.1.9
	{
		$UserName = $this->Session->read('MisEmployee');
		$admins = $this->Session->read('adminStudents');
		$this->set(array(
			'UserName' => $UserName,
			'admins' => $admins,
		));
		$dropdowns = $this->Yearterm->find('all', array(
			'conditions' => array(
				'Yearterm.level' => 1,
			),
			'group' => array('Yearterm.year'),
			'order' => array('Yearterm.id' => 'DESC'),
		));
		if ($yearList == null) {
			$years = $this->Yearterm->find('first', array(
				'condtions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('id' => 'DESC')
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('Yearterm.id' => 'DESC'),
			));
		} else {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
					'Yearterm.level' => 1,
				)
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
				),
				'order' => array('Yearterm.id' => 'DESC')
			));
		}
		$this->set(array(
			'years' => $years,
			'lastyearterms' => $lastyearterms,
			'dropdowns' => $dropdowns,
		));
		$satisfactions1 = $this->Satisfaction->find('all', array(
			'conditions' => array(
				'and' => array(
					array('Satisfaction.month <=' => 12),
					array('Satisfaction.month >=' => 10),
				),
				'Satisfaction.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('Satisfaction.year' => 'ASC'),
		));
		$satisfactions2 = $this->Satisfaction->find('all', array(
			'conditions' => array(
				'and' => array(
					array('Satisfaction.month <=' => 3),
					array('Satisfaction.month >=' => 1),
				),
				'Satisfaction.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('Satisfaction.year' => 'ASC'),
		));
		$satisfactions3 = $this->Satisfaction->find('all', array(
			'conditions' => array(
				'and' => array(
					array('Satisfaction.month <=' => 6),
					array('Satisfaction.month >=' => 4),
				),
				'Satisfaction.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('Satisfaction.year' => 'ASC'),
		));
		$satisfactions4 = $this->Satisfaction->find('all', array(
			'conditions' => array(
				'and' => array(
					array('Satisfaction.month <=' => 9),
					array('Satisfaction.month >=' => 7),
				),
				'Satisfaction.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('Satisfaction.year' => 'ASC'),
		));

		$this->set(array(
			'satisfactions1' => $satisfactions1,
			'satisfactions2' => $satisfactions2,
			'satisfactions3' => $satisfactions3,
			'satisfactions4' => $satisfactions4,
		));
	}
	public function add_satisfaction()
	{
		if ($this->request->data) {
			$this->Satisfaction->save($this->request->data);
			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('satisfaction');
		}
	}
	public function edit_satisfaction($id = NULL)
	{
		if ($this->request->data) {
			$this->Satisfaction->save($this->request->data);
			$this->Session->setFlash('แก้ไขข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('satisfaction');
		}
		$Satisfactions = $this->Satisfaction->find('first', array(
			'conditions' => array(
				'Satisfaction.id' => $id,
			),
		));
		$this->request->data = $Satisfactions;
		$this->set(array(
			'Satisfactions' => $Satisfactions
		));
	}
	public function remove_satisfaction($id = NULL)
	{
		$this->Satisfaction->delete($id);
		$this->Session->write('alertType', 'danger');
		$this->Session->setFlash('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>ลบข้อมูล เรียบร้อยแล้ว');
		$this->redirect('satisfaction');
	}
	public function student_training($yearList = null) //1.1.6
	{
		$UserName = $this->Session->read('MisEmployee');
		$admins = $this->Session->read('adminStudents');
		$this->set(array(
			'UserName' => $UserName,
			'admins' => $admins,
		));
		$dropdowns = $this->Yearterm->find('all', array(
			'conditions' => array(
				'Yearterm.level' => 1,
			),
			'group' => array('Yearterm.year'),
			'order' => array('Yearterm.id' => 'DESC'),
		));
		if ($yearList == null) {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('id' => 'DESC')
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		} else {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
					'Yearterm.level' => 1,
				)
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		}
		$this->set(array(
			'lastyearterms' => $lastyearterms,
			'dropdowns' => $dropdowns,
			'years' => $years,
		));
		$student_trainings1 = $this->StudentTraining->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentTraining.month <=' => 12),
					array('StudentTraining.month >=' => 10),
				),
				'StudentTraining.year' => $lastyearterms['Yearterm']['year'],
			),
		));
		$student_trainings2 = $this->StudentTraining->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentTraining.month <=' => 3),
					array('StudentTraining.month >=' => 1),
				),
				'StudentTraining.year' => $lastyearterms['Yearterm']['year'],
			),
		));
		$student_trainings3 = $this->StudentTraining->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentTraining.month <=' => 6),
					array('StudentTraining.month >=' => 4),
				),
				'StudentTraining.year' => $lastyearterms['Yearterm']['year'],
			),
		));
		$student_trainings4 = $this->StudentTraining->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentTraining.month <=' => 9),
					array('StudentTraining.month >=' => 7),
				),
				'StudentTraining.year' => $lastyearterms['Yearterm']['year'],
			),
		));
		$this->set(array(
			'student_trainings1' => $student_trainings1,
			'student_trainings2' => $student_trainings2,
			'student_trainings3' => $student_trainings3,
			'student_trainings4' => $student_trainings4,
		));
	}
	public function add_student_training()
	{
		if ($this->request->data) {
			$this->StudentTraining->save($this->request->data);
			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('student_training');
		}
	}
	public function edit_student_training($id = NULL)
	{
		if ($this->request->data) {
			$this->StudentTraining->save($this->request->data);
			$this->Session->setFlash('แก้ไขข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('student_training');
		}
		$StudentTrainings = $this->StudentTraining->find('first', array(
			'conditions' => array(
				'StudentTraining.id' => $id,
			),
		));
		$this->request->data = $StudentTrainings;
		$this->set(array(
			'StudentTraining' => $StudentTrainings,
		));
	}
	public function remove_student_training($id = NULL)
	{
		$this->StudentTraining->delete($id);
		$this->Session->write('alertType', 'danger');
		$this->Session->setFlash('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>ลบข้อมูล เรียบร้อยแล้ว');
		$this->redirect('student_training');
	}
	public function curriculum_inter_info($yearList = null) //1.1.1
	{
		$UserName = $this->Session->read('MisEmployee');
		$admins = $this->Session->read('adminStudents');
		$this->set(array(
			'UserName' => $UserName,
			'admins' => $admins,
		));
		$dropdowns = $this->Yearterm->find('all', array(
			'conditions' => array(
				'Yearterm.level' => 1,
			),
			'group' => array('Yearterm.year'),
			'order' => array('Yearterm.id' => 'DESC')
		));
		if ($yearList == null) {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('id' => 'DESC')
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		} else {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
					'Yearterm.level' => 1,
				)
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		}



		$this->set(array(
			'years' => $years,
			'dropdowns' => $dropdowns,
			'lastyearterms' => $lastyearterms
		));


		$curriculum_inter_infos1 = $this->CurriculumInterInfo->find('all', array(
			'conditions' => array(
				'and' => array(
					array('CurriculumInterInfo.month <=' => 12),
					array('CurriculumInterInfo.month >=' => 10),
				),
				'CurriculumInterInfo.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('CurriculumInterInfo.id' => 'desc')
		));

		$curriculum_inter_infos2 = $this->CurriculumInterInfo->find('all', array(
			'conditions' => array(
				'and' => array(
					array('CurriculumInterInfo.month <=' => 3),
					array('CurriculumInterInfo.month >=' => 1),
				),
				'CurriculumInterInfo.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('CurriculumInterInfo.id' => 'desc')
		));

		$curriculum_inter_infos3 = $this->CurriculumInterInfo->find('all', array(
			'conditions' => array(
				'and' => array(
					array('CurriculumInterInfo.month <=' => 6),
					array('CurriculumInterInfo.month >=' => 4),
				),
				'CurriculumInterInfo.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('CurriculumInterInfo.id' => 'desc')
		));

		$curriculum_inter_infos4 = $this->CurriculumInterInfo->find('all', array(
			'conditions' => array(
				'and' => array(
					array('CurriculumInterInfo.month <=' => 9),
					array('CurriculumInterInfo.month >=' => 7),
				),
				'CurriculumInterInfo.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('CurriculumInterInfo.id' => 'desc')
		));



		$this->set(array(
			'lastyearterms' => $lastyearterms,
			'curriculum_inter_infos1' => $curriculum_inter_infos1,
			'curriculum_inter_infos2' => $curriculum_inter_infos2,
			'curriculum_inter_infos3' => $curriculum_inter_infos3,
			'curriculum_inter_infos4' => $curriculum_inter_infos4,
		));
	}
	public function add_curriculum_inter()
	{
		if ($this->request->data) {
			$this->CurriculumInterInfo->save($this->request->data);
			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('curriculum_inter_info');
		}
	}
	public function edit_curriculum_inter($id = NULL)
	{
		if ($this->request->data) {
			$this->CurriculumInterInfo->save($this->request->data);
			$this->Session->setFlash('แก้ไขข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('curriculum_inter_info');
		}
		$CurriculumInterInfos = $this->CurriculumInterInfo->find('first', array(
			'conditions' => array(
				'CurriculumInterInfo.id' => $id,
			),
		));
		$this->request->data = $CurriculumInterInfos;
		$this->set(array(
			'CurriculumInterInfos' => $CurriculumInterInfos,
		));
	}
	public function remove_curriculum_inter($id = NULL)
	{
		$this->CurriculumInterInfo->delete($id);
		$this->Session->write('alertType', 'danger');
		$this->Session->setFlash('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>ลบข้อมูล เรียบร้อยแล้ว');
		$this->redirect('curriculum_inter_info');
	}
	public function student_graduate_plan($yearList = null) //1.1.14
	{
		$UserName = $this->Session->read('MisEmployee');
		$admins = $this->Session->read('adminStudents');
		$this->set(array(
			'UserName' => $UserName,
			'admins' => $admins,
		));
		$dropdowns = $this->Yearterm->find('all', array(
			'conditions' => array(
				'Yearterm.level' => 1,
			),
			'group' => array('Yearterm.year'),
			'order' => array('Yearterm.id' => 'DESC')
		));
		if ($yearList == null) {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('id' => 'DESC')
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		} else {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
					'Yearterm.level' => 1,
				)
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		}
		$this->set(array(
			'years' => $years,
			'dropdowns' => $dropdowns,
			'lastyearterms' => $lastyearterms
		));

		$student_graduate_plans1 = $this->StudentGraduatePlan->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentGraduatePlan.month <=' => 12),
					array('StudentGraduatePlan.month >=' => 10),
				),
				'StudentGraduatePlan.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentGraduatePlan.year' => 'ASC'),
		));
		$student_graduate_plans2 = $this->StudentGraduatePlan->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentGraduatePlan.month <=' => 3),
					array('StudentGraduatePlan.month >=' => 1),
				),
				'StudentGraduatePlan.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentGraduatePlan.year' => 'ASC'),
		));
		$student_graduate_plans3 = $this->StudentGraduatePlan->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentGraduatePlan.month <=' => 6),
					array('StudentGraduatePlan.month >=' => 4),
				),
				'StudentGraduatePlan.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentGraduatePlan.year' => 'ASC'),
		));
		$student_graduate_plans4 = $this->StudentGraduatePlan->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentGraduatePlan.month <=' => 9),
					array('StudentGraduatePlan.month >=' => 7),
				),
				'StudentGraduatePlan.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentGraduatePlan.year' => 'ASC'),
		));
		$this->set(array(
			'student_graduate_plans1' => $student_graduate_plans1,
			'student_graduate_plans2' => $student_graduate_plans2,
			'student_graduate_plans3' => $student_graduate_plans3,
			'student_graduate_plans4' => $student_graduate_plans4,
		));
	}
	public function add_student_graduate_plan()
	{
		if ($this->request->data) {
			$this->StudentGraduatePlan->save($this->request->data);
			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('student_graduate_plan');
		}
	}
	public function edit_student_graduate_plan($student_id = NULL)
	{
		if ($this->request->data) {
			$this->StudentGraduatePlan->save($this->request->data);
			$this->Session->setFlash('แก้ไขข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('student_graduate_plan');
		}
		$StudentGraduatePlans = $this->StudentGraduatePlan->find('first', array(
			'conditions' => array(
				'StudentGraduatePlan.id' => $student_id,
			),
		));
		$this->request->data = $StudentGraduatePlans;
		$this->set(array(
			'StudentGraduatePlans' => $StudentGraduatePlans,
		));
	}
	public function remove_student_graduate_plan($student_id = NULL)
	{
		$this->StudentGraduatePlan->delete($student_id);
		$this->Session->write('alertType', 'danger');
		$this->Session->setFlash('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>ลบข้อมูล เรียบร้อยแล้ว');
		$this->redirect('student_graduate_plan');
	}
	public function student_common_european($yearList = null) //1.2.2
	{
		$UserName = $this->Session->read('MisEmployee');
		$admins = $this->Session->read('adminStudents');
		$this->set(array(
			'UserName' => $UserName,
			'admins' => $admins,
		));
		$dropdowns = $this->Yearterm->find('all', array(
			'conditions' => array(
				'Yearterm.level' => 1,
			),
			'group' => array('Yearterm.year'),
			'order' => array('Yearterm.id' => 'DESC')
		));
		if ($yearList == null) {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('id' => 'DESC')
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		} else {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
					'Yearterm.level' => 1,
				)
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		}
		$this->set(array(
			'years' => $years,
			'dropdowns' => $dropdowns,
			'lastyearterms' => $lastyearterms,
		));
		$student_common_european1 = $this->StudentCommonEuropean->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentCommonEuropean.month <=' => 12),
					array('StudentCommonEuropean.month >=' => 10),
				),
				'StudentCommonEuropean.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentCommonEuropean.year' => 'ASC'),
		));
		$student_common_european2 = $this->StudentCommonEuropean->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentCommonEuropean.month <=' => 3),
					array('StudentCommonEuropean.month >=' => 1),
				),
				'StudentCommonEuropean.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentCommonEuropean.year' => 'ASC'),
		));
		$student_common_european3 = $this->StudentCommonEuropean->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentCommonEuropean.month <=' => 6),
					array('StudentCommonEuropean.month >=' => 4),
				),
				'StudentCommonEuropean.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentCommonEuropean.year' => 'ASC'),
		));
		$student_common_european4 = $this->StudentCommonEuropean->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentCommonEuropean.month <=' => 9),
					array('StudentCommonEuropean.month >=' => 7),
				),
				'StudentCommonEuropean.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentCommonEuropean.year' => 'ASC'),
		));
		$this->set(array(
			'student_common_european1' => $student_common_european1,
			'student_common_european2' => $student_common_european2,
			'student_common_european3' => $student_common_european3,
			'student_common_european4' => $student_common_european4,
			'lastyearterms' => $lastyearterms,
		));
	}
	public function add_student_common_european()
	{
		if ($this->request->data) {
			$this->StudentCommonEuropean->save($this->request->data);
			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('student_common_european');
		}
	}
	public function edit_student_common_european($id_student_common_european = null)
	{
		if ($this->request->data) {

			$this->StudentCommonEuropean->save($this->request->data);

			$this->Session->setFlash('แก้ไขข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('student_common_european');
		}

		$StudentCommonEuropeans = $this->StudentCommonEuropean->find('first', array(
			'conditions' => array(
				'StudentCommonEuropean.id' => $id_student_common_european,
			),

		));


		$this->request->data = $StudentCommonEuropeans;


		$this->set(array(
			'StudentCommonEuropeans' => $StudentCommonEuropeans,
		));
	}
	public function remove_student_common_european($id_student_common_european = null)
	{
		$this->StudentCommonEuropean->delete($id_student_common_european);
		$this->Session->setFlash('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>ลบข้อมูล เรียบร้อยแล้ว');
		$this->Session->write('alertType', 'danger');
		$this->redirect('student_common_european');
	}

	// ------------- date 13-08-2567 ------------- //

	public function student_satisfaction_course_moral($yearList = null) //1.2.6
	{
		$UserName = $this->Session->read('MisEmployee');
		$admins = $this->Session->read('adminStudents');
		$this->set(array(
			'UserName' => $UserName,
			'admins' => $admins,
		));
		$dropdowns = $this->Yearterm->find('all', array(
			'conditions' => array(
				'Yearterm.level' => 1,
			),
			'group' => array('Yearterm.year'),
			'order' => array('Yearterm.id' => 'DESC')
		));
		if ($yearList == null) {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('id' => 'DESC')
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		} else {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
					'Yearterm.level' => 1,
				)
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		}
		$this->set(array(
			'years' => $years,
			'dropdowns' => $dropdowns,
			'lastyearterms' => $lastyearterms,
		));
		$student_satisfaction_course_moral1 = $this->StudentCitizentation->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentCitizentation.month <=' => 12),
					array('StudentCitizentation.month >=' => 10),
				),
				'StudentCitizentation.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentCitizentation.year' => 'ASC'),
		));
		$student_satisfaction_course_moral2 = $this->StudentCitizentation->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentCitizentation.month <=' => 3),
					array('StudentCitizentation.month >=' => 1),
				),
				'StudentCitizentation.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentCitizentation.year' => 'ASC'),
		));
		$student_satisfaction_course_moral3 = $this->StudentCitizentation->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentCitizentation.month <=' => 6),
					array('StudentCitizentation.month >=' => 4),
				),
				'StudentCitizentation.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentCitizentation.year' => 'ASC'),
		));
		$student_satisfaction_course_moral4 = $this->StudentCitizentation->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentCitizentation.month <=' => 9),
					array('StudentCitizentation.month >=' => 7),
				),
				'StudentCitizentation.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentCitizentation.year' => 'ASC'),
		));
		$this->set(array(
			'student_satisfaction_course_moral1' => $student_satisfaction_course_moral1,
			'student_satisfaction_course_moral2' => $student_satisfaction_course_moral2,
			'student_satisfaction_course_moral3' => $student_satisfaction_course_moral3,
			'student_satisfaction_course_moral4' => $student_satisfaction_course_moral4,
			'lastyearterms' => $lastyearterms,
		));
	}
	public function add_student_satisfaction_course_moral()
	{
		if ($this->request->data) {
			$this->StudentCitizentation->save($this->request->data);
			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('student_satisfaction_course_moral');
		}
	}
	public function edit_student_satisfaction_course_moral($student_satisfation_course_moral_id = null)
	{
		if ($this->request->data) {
			$this->StudentCitizentation->save($this->request->data);
			$this->Session->setFlash('แก้ไขข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('student_satisfaction_course_moral');
		}

		$StudentCitizentations = $this->StudentCitizentation->find('first', array(
			'conditions' => array(
				'StudentCitizentation.id' => $student_satisfation_course_moral_id,
			),
		));

		$this->request->data = $StudentCitizentations;
		$this->set(array(
			'StudentCitizentations' => $StudentCitizentations
		));
	}
	public function remove_student_satisfaction_course_moral($student_satisfation_course_moral_id = null)
	{
		$this->StudentCitizentation->delete($student_satisfation_course_moral_id);
		$this->Session->write('alertType', 'danger');
		$this->Session->setFlash('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>ลบข้อมูล เรียบร้อยแล้ว');
		$this->redirect('student_satisfaction_course_moral');
	}
	public function student_smart_agri($yearList = null) //1.2.7
	{
		$UserName = $this->Session->read('MisEmployee');
		$admins = $this->Session->read('adminStudents');
		$this->set(array(
			'UserName' => $UserName,
			'admins' => $admins,
		));
		$dropdowns = $this->Yearterm->find('all', array(
			'conditions' => array(
				'Yearterm.level' => 1,
			),
			'group' => array('Yearterm.year'),
			'order' => array('Yearterm.id' => 'DESC')
		));
		if ($yearList == null) {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('id' => 'DESC')
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		} else {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
					'Yearterm.level' => 1,
				)
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		}
		$this->set(array(
			'years' => $years,
			'dropdowns' => $dropdowns,
			'lastyearterms' => $lastyearterms,
		));
		$student_smart_agri1 = $this->StudentSmartAgri->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentSmartAgri.month <=' => 12),
					array('StudentSmartAgri.month >=' => 10),
				),
				'StudentSmartAgri.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentSmartAgri.year' => 'ASC'),
		));
		$student_smart_agri2 = $this->StudentSmartAgri->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentSmartAgri.month <=' => 3),
					array('StudentSmartAgri.month >=' => 1),
				),
				'StudentSmartAgri.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentSmartAgri.year' => 'ASC'),
		));
		$student_smart_agri3 = $this->StudentSmartAgri->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentSmartAgri.month <=' => 6),
					array('StudentSmartAgri.month >=' => 4),
				),
				'StudentSmartAgri.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentSmartAgri.year' => 'ASC'),
		));
		$student_smart_agri4 = $this->StudentSmartAgri->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentSmartAgri.month <=' => 9),
					array('StudentSmartAgri.month >=' => 7),
				),
				'StudentSmartAgri.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentSmartAgri.year' => 'ASC'),
		));
		$this->set(array(
			'student_smart_agri1' => $student_smart_agri1,
			'student_smart_agri2' => $student_smart_agri2,
			'student_smart_agri3' => $student_smart_agri3,
			'student_smart_agri4' => $student_smart_agri4,
			'lastyearterms' => $lastyearterms,
		));
	}
	public function add_student_smart_agri()
	{
		if ($this->request->data) {
			$this->StudentSmartAgri->save($this->request->data);
			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('student_smart_agri');
		}
	}
	public function edit_student_smart_agri($StudentSmartAgri_id = null)
	{
		if ($this->request->data) {
			$this->StudentSmartAgri->save($this->request->data);
			$this->Session->setFlash('แก้ไขข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('student_smart_agri');
		}

		$StudentSmartAgris = $this->StudentSmartAgri->find('first', array(
			'conditions' => array(
				'StudentSmartAgri.id' => $StudentSmartAgri_id,
			),
		));

		$this->request->data = $StudentSmartAgris;
		$this->set(array(
			'StudentSmartAgris' => $StudentSmartAgris
		));
	}
	public function remove_student_smart_agri($StudentSmartAgri_id = null)
	{
		$this->StudentSmartAgri->delete($StudentSmartAgri_id);
		$this->Session->write('alertType', 'danger');
		$this->Session->setFlash('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>ลบข้อมูล เรียบร้อยแล้ว');
		$this->redirect('student_smart_agri');
	}
	public function student_project($yearList = null) //1.2.8
	{
		$UserName = $this->Session->read('MisEmployee');
		$admins = $this->Session->read('adminStudents');
		$this->set(array(
			'UserName' => $UserName,
			'admins' => $admins,
		));
		$dropdowns = $this->Yearterm->find('all', array(
			'conditions' => array(
				'Yearterm.level' => 1,
			),
			'group' => array('Yearterm.year'),
			'order' => array('Yearterm.id' => 'DESC')
		));
		if ($yearList == null) {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('id' => 'DESC')
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		} else {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
					'Yearterm.level' => 1,
				)
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		}
		$this->set(array(
			'years' => $years,
			'dropdowns' => $dropdowns,
			'lastyearterms' => $lastyearterms,
		));
		$student_projects1 = $this->StudentProject->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentProject.month <=' => 12),
					array('StudentProject.month >=' => 10),
				),
				'StudentProject.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentProject.year' => 'ASC'),
		));
		$student_projects2 = $this->StudentProject->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentProject.month <=' => 3),
					array('StudentProject.month >=' => 1),
				),
				'StudentProject.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentProject.year' => 'ASC'),
		));
		$student_projects3 = $this->StudentProject->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentProject.month <=' => 6),
					array('StudentProject.month >=' => 4),
				),
				'StudentProject.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentProject.year' => 'ASC'),
		));
		$student_projects4 = $this->StudentProject->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentProject.month <=' => 9),
					array('StudentProject.month >=' => 7),
				),
				'StudentProject.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentProject.year' => 'ASC'),
		));
		$this->set(array(
			'student_projects1' => $student_projects1,
			'student_projects2' => $student_projects2,
			'student_projects3' => $student_projects3,
			'student_projects4' => $student_projects4,
		));
	}
	public function student_project_view()
	{
		$student_projects = $this->StudentProject->find('count', array(
			'conditions' => array(
				''
			),
			'order' => array('StudentProject.id' => 'ASC'),
		));
		$this->set(array(
			'student_projects' => $student_projects,
		));
	}
	public function add_student_project()
	{
		if ($this->request->data) {
			$this->StudentProject->save($this->request->data);
			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('student_project');
		}
	}
	public function edit_student_project($student_project_id = null)
	{
		if ($this->request->data) {
			$this->StudentProject->save($this->request->data);
			$this->Session->setFlash('แก้ไขข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('student_project');
		}

		$StudentProjects = $this->StudentProject->find('first', array(
			'conditions' => array(
				'StudentProject.id' => $student_project_id,
			),
		));

		$this->request->data = $StudentProjects;
		$this->set(array(
			'StudentProjects' => $StudentProjects
		));
	}
	public function remove_student_project($student_project_id = null)
	{
		$this->StudentProject->delete($student_project_id);
		$this->Session->write('alertType', 'danger');
		$this->Session->setFlash('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>ลบข้อมูล เรียบร้อยแล้ว');
		$this->redirect('student_project');
	}
	public function student_active() //1.2.10
	{
		$student_actives1 = $this->StudentTravel->find('count', array(
			'conditions' => array(
				'StudentTravel.degree_id' => 1,
			),
		));
		$student_actives2 = $this->StudentTravel->find('count', array(
			'conditions' => array(
				'StudentTravel.degree_id' => 3,
			),
		));
		$student_actives3 = $this->StudentTravel->find('count', array(
			'conditions' => array(
				'StudentTravel.degree_id' => 5,
			),
		));
		$this->set(array(
			'student_actives1' => $student_actives1,
			'student_actives2' => $student_actives2,
			'student_actives3' => $student_actives3,
		));
	}
	public function student_active_degree1($yearList = null)
	{
		$UserName = $this->Session->read('MisEmployee');
		$admins = $this->Session->read('adminStudents');
		$this->set(array(
			'UserName' => $UserName,
			'admins' => $admins,
		));
		$dropdowns = $this->Yearterm->find('all', array(
			'conditions' => array(
				'Yearterm.level' => 1,
			),
			'group' => array('Yearterm.year'),
			'order' => array('Yearterm.id' => 'DESC')
		));
		if ($yearList == null) {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('id' => 'DESC')
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		} else {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
					'Yearterm.level' => 1,
				)
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		}
		$this->set(array(
			'years' => $years,
			'dropdowns' => $dropdowns,
			'lastyearterms' => $lastyearterms
		));
		$student_active1 = $this->StudentTravel->find('all', array(
			'conditions' => array(
				'StudentTravel.degree_id' => 1,
				'and' => array(
					array('StudentTravel.month <=' => 12),
					array('StudentTravel.month >=' => 10),
				),
				'StudentTravel.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentTravel.year' => 'ASC'),
		));
		$student_active2 = $this->StudentTravel->find('all', array(
			'conditions' => array(
				'StudentTravel.degree_id' => 1,
				'and' => array(
					array('StudentTravel.month <=' => 3),
					array('StudentTravel.month >=' => 1),
				),
				'StudentTravel.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentTravel.year' => 'ASC'),
		));
		$student_active3 = $this->StudentTravel->find('all', array(
			'conditions' => array(
				'StudentTravel.degree_id' => 1,
				'and' => array(
					array('StudentTravel.month <=' => 6),
					array('StudentTravel.month >=' => 4),
				),
				'StudentTravel.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentTravel.year' => 'ASC'),
		));
		$student_active4 = $this->StudentTravel->find('all', array(
			'conditions' => array(
				'StudentTravel.degree_id' => 1,
				'and' => array(
					array('StudentTravel.month <=' => 9),
					array('StudentTravel.month >=' => 7),
				),
				'StudentTravel.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentTravel.year' => 'ASC'),
		));
		$this->set(array(
			'student_active1' => $student_active1,
			'student_active2' => $student_active2,
			'student_active3' => $student_active3,
			'student_active4' => $student_active4,
		));
	}
	public function add_student_active1()
	{
		if ($this->request->data) {
			$this->StudentTravel->save($this->request->data);
			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('student_active_degree1');
		}
	}
	public function edit_student_active1($StudentTravel_id = null)
	{
		if ($this->request->data) {
			$this->StudentTravel->save($this->request->data);
			$this->Session->setFlash('แก้ไขข้อมูลเรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('student_active_degree1');
		}
		$StudentTravels = $this->StudentTravel->find('first', array(
			'conditions' => array(
				'StudentTravel.id' => $StudentTravel_id,
			),
		));
		$this->request->data = $StudentTravels;
		$this->set(array(
			'StudentTravels' => $StudentTravels,
		));
	}
	public function remove_student_active1($StudentTravel_id = null)
	{
		$this->StudentTravel->delete($StudentTravel_id);
		$this->Session->setFlash('ลบข้อมูลเรียบร้อยแล้ว');
		$this->Session->write('alertType', 'danger');
		$this->redirect('student_active_degree1');
	}
	public function student_active_degree3($yearList = null)
	{
		$UserName = $this->Session->read('MisEmployee');
		$admins = $this->Session->read('adminStudents');
		$this->set(array(
			'UserName' => $UserName,
			'admins' => $admins,
		));
		$dropdowns = $this->Yearterm->find('all', array(
			'conditions' => array(
				'Yearterm.level' => 1,
			),
			'group' => array('Yearterm.year'),
			'order' => array('Yearterm.id' => 'DESC')
		));
		if ($yearList == null) {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('id' => 'DESC')
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		} else {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
					'Yearterm.level' => 1,
				)
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		}
		$this->set(array(
			'years' => $years,
			'dropdowns' => $dropdowns,
			'lastyearterms' => $lastyearterms
		));
		$student_active1 = $this->StudentTravel->find('all', array(
			'conditions' => array(
				'StudentTravel.degree_id' => 3,
				'and' => array(
					array('StudentTravel.month <=' => 12),
					array('StudentTravel.month >=' => 10),
				),
				'StudentTravel.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentTravel.year' => 'ASC'),
		));
		$student_active2 = $this->StudentTravel->find('all', array(
			'conditions' => array(
				'StudentTravel.degree_id' => 3,
				'and' => array(
					array('StudentTravel.month <=' => 3),
					array('StudentTravel.month >=' => 1),
				),
				'StudentTravel.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentTravel.year' => 'ASC'),
		));
		$student_active3 = $this->StudentTravel->find('all', array(
			'conditions' => array(
				'StudentTravel.degree_id' => 3,
				'and' => array(
					array('StudentTravel.month <=' => 6),
					array('StudentTravel.month >=' => 4),
				),
				'StudentTravel.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentTravel.year' => 'ASC'),
		));
		$student_active4 = $this->StudentTravel->find('all', array(
			'conditions' => array(
				'StudentTravel.degree_id' => 3,
				'and' => array(
					array('StudentTravel.month <=' => 9),
					array('StudentTravel.month >=' => 7),
				),
				'StudentTravel.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentTravel.year' => 'ASC'),
		));
		$this->set(array(
			'student_active1' => $student_active1,
			'student_active2' => $student_active2,
			'student_active3' => $student_active3,
			'student_active4' => $student_active4,
		));
	}
	public function add_student_active3()
	{
		if ($this->request->data) {
			$this->StudentTravel->save($this->request->data);
			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('student_active_degree3');
		}
	}
	public function edit_student_active3($StudentTravel_id = null)
	{
		if ($this->request->data) {
			$this->StudentTravel->save($this->request->data);
			$this->Session->setFlash('แก้ไขข้อมูลเรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('student_active_degree3');
		}
		$StudentTravels = $this->StudentTravel->find('first', array(
			'conditions' => array(
				'StudentTravel.id' => $StudentTravel_id,
			),
		));
		$this->request->data = $StudentTravels;
		$this->set(array(
			'StudentTravels' => $StudentTravels,
		));
	}
	public function remove_student_active3($StudentTravel_id = null)
	{
		$this->StudentTravel->delete($StudentTravel_id);
		$this->Session->setFlash('ลบข้อมูลเรียบร้อยแล้ว');
		$this->Session->write('alertType', 'danger');
		$this->redirect('student_active_degree3');
	}
	public function student_active_degree5($yearList = null)
	{
		$UserName = $this->Session->read('MisEmployee');
		$admins = $this->Session->read('adminStudents');
		$this->set(array(
			'UserName' => $UserName,
			'admins' => $admins,
		));
		$dropdowns = $this->Yearterm->find('all', array(
			'conditions' => array(
				'Yearterm.level' => 1,
			),
			'group' => array('Yearterm.year'),
			'order' => array('Yearterm.id' => 'DESC')
		));
		if ($yearList == null) {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('id' => 'DESC')
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		} else {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
					'Yearterm.level' => 1,
				)
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		}
		$this->set(array(
			'years' => $years,
			'dropdowns' => $dropdowns,
			'lastyearterms' => $lastyearterms
		));
		$student_active1 = $this->StudentTravel->find('all', array(
			'conditions' => array(
				'StudentTravel.degree_id' => 5,
				'and' => array(
					array('StudentTravel.month <=' => 12),
					array('StudentTravel.month >=' => 10),
				),
				'StudentTravel.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentTravel.year' => 'ASC'),
		));
		$student_active2 = $this->StudentTravel->find('all', array(
			'conditions' => array(
				'StudentTravel.degree_id' => 5,
				'and' => array(
					array('StudentTravel.month <=' => 3),
					array('StudentTravel.month >=' => 1),
				),
				'StudentTravel.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentTravel.year' => 'ASC'),
		));
		$student_active3 = $this->StudentTravel->find('all', array(
			'conditions' => array(
				'StudentTravel.degree_id' => 5,
				'and' => array(
					array('StudentTravel.month <=' => 6),
					array('StudentTravel.month >=' => 4),
				),
				'StudentTravel.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentTravel.year' => 'ASC'),
		));
		$student_active4 = $this->StudentTravel->find('all', array(
			'conditions' => array(
				'StudentTravel.degree_id' => 5,
				'and' => array(
					array('StudentTravel.month <=' => 9),
					array('StudentTravel.month >=' => 7),
				),
				'StudentTravel.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentTravel.year' => 'ASC'),
		));
		$this->set(array(
			'student_active1' => $student_active1,
			'student_active2' => $student_active2,
			'student_active3' => $student_active3,
			'student_active4' => $student_active4,
		));
	}
	public function add_student_active5()
	{
		if ($this->request->data) {
			$this->StudentTravel->save($this->request->data);
			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('student_active_degree5');
		}
	}
	public function edit_student_active5($StudentTravel_id = null)
	{
		if ($this->request->data) {
			$this->StudentTravel->save($this->request->data);
			$this->Session->setFlash('แก้ไขข้อมูลเรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('student_active_degree5');
		}
		$StudentTravels = $this->StudentTravel->find('first', array(
			'conditions' => array(
				'StudentTravel.id' => $StudentTravel_id,
			),
		));
		$this->request->data = $StudentTravels;
		$this->set(array(
			'StudentTravels' => $StudentTravels,
		));
	}
	public function remove_student_active5($StudentTravel_id = null)
	{
		$this->StudentTravel->delete($StudentTravel_id);
		$this->Session->setFlash('ลบข้อมูลเรียบร้อยแล้ว');
		$this->Session->write('alertType', 'danger');
		$this->redirect('student_active_degree5');
	}
	public function student_dna($yearList = null) //1.2.12
	{
		$UserName = $this->Session->read('MisEmployee');
		$admins = $this->Session->read('adminStudents');
		$this->set(array(
			'UserName' => $UserName,
			'admins' => $admins,
		));
		$dropdowns = $this->Yearterm->find('all', array(
			'conditions' => array(
				'Yearterm.level' => 1,
			),
			'group' => array('Yearterm.year'),
			'order' => array('Yearterm.id' => 'DESC')
		));
		if ($yearList == null) {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('id' => 'DESC')
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		} else {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
					'Yearterm.level' => 1,
				)
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		}
		$this->set(array(
			'years' => $years,
			'dropdowns' => $dropdowns,
			'lastyearterms' => $lastyearterms
		));
		$student_dna1 = $this->StudentDna->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentDna.month <=' => 12),
					array('StudentDna.month >=' => 10),
				),
				'StudentDna.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentDna.year' => 'ASC'),
		));
		$student_dna2 = $this->StudentDna->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentDna.month <=' => 3),
					array('StudentDna.month >=' => 1),
				),
				'StudentDna.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentDna.year' => 'ASC'),
		));
		$student_dna3 = $this->StudentDna->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentDna.month <=' => 6),
					array('StudentDna.month >=' => 4),
				),
				'StudentDna.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentDna.year' => 'ASC'),
		));
		$student_dna4 = $this->StudentDna->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentDna.month <=' => 9),
					array('StudentDna.month >=' => 7),
				),
				'StudentDna.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentDna.year' => 'ASC'),
		));
		$this->set(array(
			'student_dna1' => $student_dna1,
			'student_dna2' => $student_dna2,
			'student_dna3' => $student_dna3,
			'student_dna4' => $student_dna4,
		));
	}
	public function student_dna_view()
	{
		$student_dnas = $this->StudentDna->find('count', array(
			'conditions' => array(
				''
			),
		));
		$this->set(array(
			'student_dnas' => $student_dnas,
		));
	}
	public function add_student_dna()
	{
		if ($this->request->data) {
			$this->StudentDna->save($this->request->data);
			$this->Session->setFlash('เพิ่มข้อมูลเรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('student_dna');
		}
	}
	public function edit_student_dna($student_dna_id = null)
	{
		if ($this->request->data) {
			$this->StudentDna->save($this->request->data);
			$this->Session->setFlash('แก้ไขข้อมูลเรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('student_dna');
		}
		$StudentDnas = $this->StudentDna->find('first', array(
			'conditions' => array(
				'StudentDna.id' => $student_dna_id,
			),
		));
		$this->request->data = $StudentDnas;
		$this->set(array(
			'StudentDnas' => $StudentDnas,
		));
	}
	public function remove_student_dna($student_dna_id = null)
	{
		$this->StudentDna->delete($student_dna_id);
		$this->Session->write('alertType', 'danger');
		$this->Session->setFlash('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>ลบข้อมูล เรียบร้อยแล้ว');
		$this->redirect('student_dna');
	}
	public function student_manufacture($yearList = null) //1.2.11
	{
		$UserName = $this->Session->read('MisEmployee');
		$admins = $this->Session->read('adminStudents');
		$this->set(array(
			'UserName' => $UserName,
			'admins' => $admins,
		));
		$dropdowns = $this->Yearterm->find('all', array(
			'conditions' => array(
				'Yearterm.level' => 1,
			),
			'group' => array('Yearterm.year'),
			'order' => array('Yearterm.id' => 'DESC')
		));
		if ($yearList == null) {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('id' => 'DESC')
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		} else {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
					'Yearterm.level' => 1,
				)
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		}
		$this->set(array(
			'years' => $years,
			'dropdowns' => $dropdowns,
			'lastyearterms' => $lastyearterms
		));
		$student_manufacture1 = $this->StudentManufacture->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentManufacture.month <=' => 12),
					array('StudentManufacture.month >=' => 10),
				),
				'StudentManufacture.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentManufacture.year' => 'ASC'),
		));
		$student_manufacture2 = $this->StudentManufacture->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentManufacture.month <=' => 3),
					array('StudentManufacture.month >=' => 1),
				),
				'StudentManufacture.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentManufacture.year' => 'ASC'),
		));
		$student_manufacture3 = $this->StudentManufacture->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentManufacture.month <=' => 6),
					array('StudentManufacture.month >=' => 4),
				),
				'StudentManufacture.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentManufacture.year' => 'ASC'),
		));
		$student_manufacture4 = $this->StudentManufacture->find('all', array(
			'conditions' => array(
				'and' => array(
					array('StudentManufacture.month <=' => 9),
					array('StudentManufacture.month >=' => 7),
				),
				'StudentManufacture.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('StudentManufacture.year' => 'ASC'),
		));
		$this->set(array(
			'student_manufacture1' => $student_manufacture1,
			'student_manufacture2' => $student_manufacture2,
			'student_manufacture3' => $student_manufacture3,
			'student_manufacture4' => $student_manufacture4,
		));
	}
	public function add_student_manufacture()
	{
		if ($this->request->data) {
			$this->StudentManufacture->save($this->request->data);
			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('student_manufacture');
		}
	}
	public function edit_student_manufacture($student_manufacture_id = null)
	{
		if ($this->request->data) {
			$this->StudentManufacture->save($this->request->data);
			$this->Session->setFlash('แก้ไขข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('student_manufacture');
		}
		$studentmanufactures = $this->StudentManufacture->find('first', array(
			'conditions' => array(
				'StudentManufacture.id' => $student_manufacture_id,
			),
		));
		$this->request->data = $studentmanufactures;
		$this->set(array(
			'studentmanufactures' => $studentmanufactures,
		));
	}
	public function remove_student_manufacture($student_manufacture_id = null)
	{
		$this->StudentManufacture->delete($student_manufacture_id);
		$this->Session->write('alertType', 'danger');
		$this->Session->setFlash('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>ลบข้อมูล เรียบร้อยแล้ว');
		$this->redirect('student_manufacture');
	}
	public function kpimain() {}
	public function notification()
	{
		$UserName = $this->Session->read('MisEmployee');
		$admins = $this->Session->read('adminStudents');
		$this->set(array(
			'UserName' => $UserName,
			'admins' => $admins,
		));
		$dropdowns = $this->Yearterm->find('all', array(
			'conditions' => array(
				'Yearterm.level' => 1,
			),
			'group' => array('Yearterm.year'),
			'order' => array('Yearterm.id' => 'DESC')
		));
		$notifications = $this->Notification->find('all', array(
			'conditions' => array(),
			'order' => array('Notification.id' => 'ASC'),
		));
		$this->set(array(
			'notifications' => $notifications,
		));
	}
	public function notification_view()
	{
		$UserName = $this->Session->read('MisEmployee');
		$admins = $this->Session->read('adminStudents');
		$this->set(array(
			'UserName' => $UserName,
			'admins' => $admins,
		));
		$dropdowns = $this->Yearterm->find('all', array(
			'conditions' => array(
				'Yearterm.level' => 1,
			),
			'group' => array('Yearterm.year'),
			'order' => array('Yearterm.id' => 'DESC')
		));
		$notifications = $this->Notification->find('all', array(
			'conditions' => array(),
			'order' => array('Notification.id' => 'ASC'),
		));
		$this->set(array(
			'notifications' => $notifications,
		));
	}
	public function add_notification()
	{
		if ($this->request->data) {
			$this->Notification->save($this->request->data);
			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('notification');
		}
	}
	public function edit_notification($notification_id = null)
	{
		if ($this->request->data) {
			$this->Notification->save($this->request->data);
			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect('notification');
		}
		$Notifications = $this->Notification->find('first', array(
			'conditions' => array(
				'Notification.id' => $notification_id,
			),
		));
		$this->request->data = $Notifications;
		$this->set(array(
			'Notifications' => $Notifications,
		));
	}
	public function remove_notification($notification_id = null)
	{
		$this->Notification->delete($notification_id);
		$this->Session->write('alertType', 'danger');
		$this->Session->setFlash('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>ลบข้อมูล เรียบร้อยแล้ว');
		$this->redirect('notification');
	}
	public function curriculum_info($yearList = null) //1.1.15
	{
		$UserName = $this->Session->read('MisEmployee');
		$admins = $this->Session->read('adminStudents');
		$this->set(array(
			'UserName' => $UserName,
			'admins' => $admins,
		));
		$dropdowns = $this->Yearterm->find('all', array(
			'conditions' => array(
				'Yearterm.level' => 1,
			),
			'group' => array('Yearterm.year'),
			'order' => array('Yearterm.id' => 'DESC')
		));
		if ($yearList == null) {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('id' => 'DESC')
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		} else {
			$years = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
					'Yearterm.level' => 1,
				)
			));
			$lastyearterms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.year' => $yearList,
				),
				'order' => array('Yearterm.id' => 'desc')
			));
		}
		$this->set(array(
			'years' => $years,
			'dropdowns' => $dropdowns,
			'lastyearterms' => $lastyearterms,
		));
		$curriculum_infos1 = $this->CurriculumInfo->find('all', array(
			'conditions' => array(
				'CurriculumInfo.degree_id' => 1,
				'and' => array(
					array('CurriculumInfo.month <=' => 12),
					array('CurriculumInfo.month >=' => 10),
				),
				'CurriculumInfo.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('CurriculumInfo.year' => 'ASC'),
		));
		$curriculum_infos2 = $this->CurriculumInfo->find('all', array(
			'conditions' => array(
				'CurriculumInfo.degree_id' => 1,
				'and' => array(
					array('CurriculumInfo.month <=' => 3),
					array('CurriculumInfo.month >=' => 1),
				),
				'CurriculumInfo.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('CurriculumInfo.year' => 'ASC'),
		));
		$curriculum_infos3 = $this->CurriculumInfo->find('all', array(
			'conditions' => array(
				'CurriculumInfo.degree_id' => 1,
				'and' => array(
					array('CurriculumInfo.month <=' => 6),
					array('CurriculumInfo.month >=' => 4),
				),
				'CurriculumInfo.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('CurriculumInfo.year' => 'ASC'),
		));
		$curriculum_infos4 = $this->CurriculumInfo->find('all', array(
			'conditions' => array(
				'CurriculumInfo.degree_id' => 1,
				'and' => array(
					array('CurriculumInfo.month <=' => 9),
					array('CurriculumInfo.month >=' => 7),
				),
				'CurriculumInfo.year' => $lastyearterms['Yearterm']['year'],
			),
			'order' => array('CurriculumInfo.year' => 'ASC'),
		));
		$this->set(array(
			'curriculum_infos1' => $curriculum_infos1,
			'curriculum_infos2' => $curriculum_infos2,
			'curriculum_infos3' => $curriculum_infos3,
			'curriculum_infos4' => $curriculum_infos4,
		));
	}
	public function add_curriculum_info()
	{
		if ($this->request->data) {
			$this->CurriculumInfo->save($this->request->data);
			$this->Session->write('alertType', 'success');
			$this->Session->setFlash('เพิ่มข้อมูลเรียบร้อยแล้ว');
			$this->redirect('curriculum_info');
		}
	}
	public function edit_curriculum_info($curriculum_info_id = null)
	{
		if ($this->request->data) {
			$this->CurriculumInfo->save($this->request->data);
			$this->Session->write('alertType', 'success');
			$this->Session->setFlash('เพิ่มข้อมูลเรียบร้อยแล้ว');
			$this->redirect('curriculum_info');
		}
		$curriculuminfos = $this->CurriculumInfo->find('first', array(
			'conditions' => array(
				'CurriculumInfo.id' => $curriculum_info_id,
			),
		));
		$this->request->data = $curriculuminfos;
		$this->set(array(
			'curriculuminfos' => $curriculuminfos
		));
	}
	public function remove_curriculum_info($curriculum_info_id = null)
	{
		$this->CurriculumInfo->delete($curriculum_info_id);
		$this->Session->write('alertType', 'danger');
		$this->Session->setFlash('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>ลบข้อมูล เรียบร้อยแล้ว');
		$this->redirect('curriculum_info');
	}

	public function program()
	{
		$UserName = $this->Session->read('MisEmployee');
		$this->set('UserName', $UserName);
	}
	public function register2($projectId = null)
	{
		$this->set(array('projectId' => $projectId));
		$project = $this->Project->findById($projectId);

		if ($project != null) {

			try {
				if ($this->request->data) {
					$this->request->data['Member']['project_id'] = $projectId;
					$this->Member->save($this->request->data);
					$files = $this->request->data['MemberDocument']['files'];
					////////////////// before upload ///////////////////

					////////////////// cheack file upload ///////////////////
					if ($this->request->data['MemberDocument']['files'][0]['type'] != null) {
						foreach ($this->request->data['MemberDocument']['files'] as $check) {
							if ($check['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif" or "application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document" or "application/x-rar-compressed" or "application/octet-stream" or "application/zip" or "application/octet-stream" or "application/vnd.ms-powerpoint" or "application/vnd.openxmlformats-officedocument.presentationml.presentation" or "application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {


								foreach ($files as $file) {

									list($name, $fileType) = explode(".", $file['name']);
									if ($fileType != "doc" or "docx" or "jpg" or "jpeg" or "png" or "gif" or "zip" or "rar" or "xls" or "xlsx" or "ppt" or "pptx" or "pdf") {

										$this->Session->setFlash('กรุณาแก้ไขชื่อไฟล์ก่อนอัปโหลด โดยไม่ให้มีอักขระ จุด .');
										$this->Session->write('alertType', 'danger');
										$this->redirect('add_post');
									}
								}


								$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
								$this->Session->write('alertType', 'danger');
								$this->redirect('add_post');
							}
						}
					}
					if ($files[0]['name'] != null) {

						$Member_id = $this->Member->find('first', array(
							'order' => array('Member.id' => 'DESC'),

						));

						$i = 0;
						foreach ($files as $file) {

							//list($name, $fileType) = explode(".", $file['name']);
							$fileType = pathinfo($file['name'], PATHINFO_EXTENSION);
							$this->request->data['MemberDocument']['name'] = $file['name'] . "_" . $i . "." . $fileType;
							$this->request->data['MemberDocument']['name_old'] = $file['name'];
							$this->request->data['MemberDocument']['member_id'] = $Member_id['Member']['id'];

							if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
								$this->request->data['MemberDocument']['class'] = "file";
							} else {
								$this->request->data['MemberDocument']['class'] = "img";
							}


							//=====
							// if(!file_exists(WWW_ROOT.'files'.DS.'Document'.DS))
							// 	mkdir(WWW_ROOT.'files'.DS.'Document'.DS);

							// move_uploaded_file($file['tmp_name'],WWW_ROOT.'files'.DS.'Document'.DS.$this->request->data['MemberDocument']['name']);
							$filethai = iconv("UTF-8", "TIS-620",  $file['name']);

							$folder = WWW_ROOT . 'files' . DS . 'Document' . DS;

							if (!file_exists($folder))
								mkdir($folder);

							move_uploaded_file($file['tmp_name'], $folder . DS . $filethai);




							$this->MemberDocument->create();
							$this->MemberDocument->save($this->request->data);


							$i++;
						}
					} else {
					}

					if ($project['Project']['id'] == 1) {
						$file1s = $this->request->data['MemberDocument']['file1s'];
						////////////////// before upload ///////////////////

						////////////////// cheack file upload ///////////////////
						if ($this->request->data['MemberDocument']['file1s'][0]['type'] != null) {
							foreach ($this->request->data['MemberDocument']['file1s'] as $check) {
								if ($check['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif" or "application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document" or "application/x-rar-compressed" or "application/octet-stream" or "application/zip" or "application/octet-stream" or "application/vnd.ms-powerpoint" or "application/vnd.openxmlformats-officedocument.presentationml.presentation" or "application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {


									foreach ($file1s as $file) {

										list($name, $fileType) = explode(".", $file['name']);
										if ($fileType != "doc" or "docx" or "jpg" or "jpeg" or "png" or "gif" or "zip" or "rar" or "xls" or "xlsx" or "ppt" or "pptx" or "pdf") {

											$this->Session->setFlash('กรุณาแก้ไขชื่อไฟล์ก่อนอัปโหลด โดยไม่ให้มีอักขระ จุด .');
											$this->Session->write('alertType', 'danger');
											$this->redirect('add_post');
										}
									}


									$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
									$this->Session->write('alertType', 'danger');
									$this->redirect('add_post');
								}
							}
						}
						if ($file1s[0]['name'] != null) {

							$Member_id = $this->Member->find('first', array(
								'order' => array('Member.id' => 'DESC'),

							));

							$i = 0;
							foreach ($file1s as $file) {


								$fileType = pathinfo($file['name'], PATHINFO_EXTENSION);
								$this->request->data['MemberDocument']['name'] = $file['name'] . "_" . $i . "." . $fileType;
								$this->request->data['MemberDocument']['name_old'] = $file['name'];
								$this->request->data['MemberDocument']['member_id'] = $Member_id['Member']['id'];

								if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
									$this->request->data['MemberDocument']['class'] = "file";
								} else {
									$this->request->data['MemberDocument']['class'] = "img";
								}


								//=====
								// if(!file_exists(WWW_ROOT.'files'.DS.'Document'.DS))
								// 	mkdir(WWW_ROOT.'files'.DS.'Document'.DS);

								// move_uploaded_file($file['tmp_name'],WWW_ROOT.'files'.DS.'Document'.DS.$this->request->data['MemberDocument']['name']);
								$filethai = iconv("UTF-8", "TIS-620",  $file['name']);


								$folder = WWW_ROOT . 'files' . DS . 'Document' . DS;

								if (!file_exists($folder))
									mkdir($folder);

								move_uploaded_file($file['tmp_name'], $folder . DS . $filethai);


								$this->MemberDocument->create();
								$this->MemberDocument->save($this->request->data);


								$i++;
							}
						} else {
						}
					} elseif ($project['Project']['id'] == 2) {
						$this->Member->save($this->request->data);
					}

					$this->Session->setFlash('ดำเนินการสมัครเรียบร้อยแล้ว');
					$this->Session->write('alertType', 'success');
					$this->redirect(array('action' => 'checklist', $projectId));
				}
			} catch (Exception $e) {

				echo $e->getMessage();
			}

			$prefixes = $this->Prefix->find('list');

			$this->set(array('project' => $project, 'prefixes' => $prefixes));
		} else {
			$this->redirect(array('action' => 'index'));
		}
	}
	public function register($projectId = null)
	{
		if ($projectId == 2) {
			$this->redirect(array('action' => 'member_student'));
		}

		$this->set(array('projectId' => $projectId));
		$project = $this->Project->findById($projectId);

		if ($projectId == 3) {
			$TeamMember = 4;
		} elseif ($projectId == 4) {
			$TeamMember = 3;
		} elseif ($projectId == 1) {
			$TeamMember = 3;
		} elseif ($projectId == 5 || $projectId == 6) {
			$TeamMember = 2;
		}



		$this->set(array('TeamMember' => $TeamMember));

		// debug($TeamMember);



		if ($this->request->data) {
			$files = $this->request->data['Member']['files'];
			if ($this->request->data['Member']['files'][0]['type'] != null) {
				foreach ($this->request->data['Member']['files'] as $check) {
					if ($check['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or
						"image/gif" or "application/msword" or
						"application/vnd.openxmlformats-officedocument.wordprocessingml.document" or
						"application/x-rar-compressed" or "application/octet-stream" or "application/zip" or
						"application/octet-stream" or "application/vnd.ms-powerpoint" or
						"application/vnd.openxmlformats-officedocument.presentationml.presentation" or
						"application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
						foreach ($files as $file) {
							$fileType = pathinfo($file['name'], PATHINFO_EXTENSION);
							if (
								$fileType != "doc" or "docx" or "jpg" or "jpeg" or "png" or "gif" or "zip" or "rar" or "xls" or
								"xlsx" or "ppt" or "pptx" or "pdf"
							) {
								$this->Session->setFlash('กรุณาแก้ไขชื่อไฟล์ก่อนอัปโหลด โดยไม่ให้มีอักขระ จุด .');
								$this->Session->write('alertType', 'danger');

								$this->redirect(array('controller' => 'main', 'action' => 'register2'));
							}
						}
						$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
						$this->Session->write('alertType', 'danger');
					}
				}
			}
			if ($files[0]['name'] != null) {

				$Member_id = $this->Member->find('first', array(
					'order' => array('Member.id' => 'DESC'),

				));

				$i = 0;
				foreach ($files as $file) {
					$filethai = iconv("UTF-8", "TIS-620",  $file['name']);
					$folder = WWW_ROOT . 'files' . DS . 'Document' . DS;

					if (!file_exists($folder))
						mkdir($folder);

					move_uploaded_file($file['tmp_name'], $folder . DS . $filethai);

					$i++;
				}
			}

			if ($project['Project']['id'] == 1) {
				$file1s = $this->request->data['Member']['file1s'];

				////////////////// check file upload ///////////////////
				if ($this->request->data['Member']['file1s'][0]['type'] != null) {
					foreach ($this->request->data['Member']['file1s'] as $check) {
						if ($check['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif" or "application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document" or "application/x-rar-compressed" or "application/octet-stream" or "application/zip" or "application/octet-stream" or "application/vnd.ms-powerpoint" or "application/vnd.openxmlformats-officedocument.presentationml.presentation" or "application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {


							foreach ($file1s as $file) {

								$fileType = pathinfo($file['name'], PATHINFO_EXTENSION);
								if ($fileType != "doc" or "docx" or "jpg" or "jpeg" or "png" or "gif" or "zip" or "rar" or "xls" or "xlsx" or "ppt" or "pptx" or "pdf") {

									$this->Session->setFlash('กรุณาแก้ไขชื่อไฟล์ก่อนอัปโหลด โดยไม่ให้มีอักขระ จุด .');
									$this->Session->write('alertType', 'danger');
									$this->redirect(array('controller' => 'main', 'action' => 'register2'));
								}
							}


							$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
							$this->Session->write('alertType', 'danger');
							$this->redirect(array('controller' => 'main', 'action' => 'register2'));
						}
					}
				}
				if ($file1s[0]['name'] != null) {

					$Member_id = $this->Member->find('first', array(
						'order' => array('Member.id' => 'DESC'),

					));

					$i = 0;
					foreach ($file1s as $file) {

						$file_project = $file['name'];
						$filethai = iconv("UTF-8", "TIS-620",  $file['name']);
						$folder = WWW_ROOT . 'files' . DS . 'Document' . DS;

						if (!file_exists($folder))
							mkdir($folder);

						move_uploaded_file($file['tmp_name'], $folder . DS . $filethai);

						$i++;
					}
				}
			} else {
				$file_project = null;
			}

			$data = array();
			// **********************************คนที่ 1********************************** */  
			if ($this->request->data['Member']['fname1'] != null) {
				$add_prefixes = $this->Prefix->find('first', array(
					'conditions' => array(
						'Prefix.id' => $this->request->data['Member']['prefix_id1'],
					),
					'fields' => array('Prefix.id', 'Prefix.name')
				));
				$committee_name = $add_prefixes['Prefix']['name'] . '' . $this->request->data['Member']['fname1'] . ' ' . $this->request->data['Member']['lname1'];

				$data2 = array(
					'school' => $this->request->data['Member']['school'],
					'team_no' => $this->request->data['Member']['team_no'],
					'school_address' => $this->request->data['Member']['school_address'],
					'school_province' => $this->request->data['Member']['school_province'],
					'school_amphoe' => $this->request->data['Member']['school_amphoe'],
					'school_tambon' => $this->request->data['Member']['school_tambon'],
					'school_zipcode' => $this->request->data['Member']['school_zipcode'],
					'prefix_id' => $add_prefixes['Prefix']['id'],
					'fname' => $this->request->data['Member']['fname1'],
					'lname' => $this->request->data['Member']['lname1'],
					'age' => $this->request->data['Member']['age1'],
					'phone' => $this->request->data['Member']['phone1'],
					'level' => $this->request->data['Member']['level1'],
					'teacher' => $this->request->data['Member']['teacher'],
					'phoneteacher' => $this->request->data['Member']['phoneteacher'],
					'emailteacher' => $this->request->data['Member']['emailteacher'],
					'project_id' => $projectId,
					'file' => $files[0]['name'],
					'file1' => $file_project,

				);

				array_push($data, $data2);
			}
			//**********************************คนที่ 2********************************** */  
			if ($this->request->data['Member']['fname2'] != null) {
				$add_prefixes = $this->Prefix->find('first', array(
					'conditions' => array(
						'Prefix.id' => $this->request->data['Member']['prefix_id2'],
					),
					'fields' => array('Prefix.id', 'Prefix.name')
				));
				$committee_name = $add_prefixes['Prefix']['name'] . '' . $this->request->data['Member']['fname2'] . ' ' . $this->request->data['Member']['lname2'];

				$data2 = array(
					'school' => $this->request->data['Member']['school'],
					'team_no' => $this->request->data['Member']['team_no'],
					'school_address' => $this->request->data['Member']['school_address'],
					'school_province' => $this->request->data['Member']['school_province'],
					'school_amphoe' => $this->request->data['Member']['school_amphoe'],
					'school_tambon' => $this->request->data['Member']['school_tambon'],
					'school_zipcode' => $this->request->data['Member']['school_zipcode'],
					'prefix_id' => $add_prefixes['Prefix']['id'],
					'fname' => $this->request->data['Member']['fname2'],
					'lname' => $this->request->data['Member']['lname2'],
					'age' => $this->request->data['Member']['age2'],
					'phone' => $this->request->data['Member']['phone2'],
					'level' => $this->request->data['Member']['level2'],
					'teacher' => $this->request->data['Member']['teacher'],
					'phoneteacher' => $this->request->data['Member']['phoneteacher'],
					'emailteacher' => $this->request->data['Member']['emailteacher'],
					'project_id' => $projectId,
					'file' => $files[0]['name'],
					'file1' => $file_project,

				);

				array_push($data, $data2);
			}
			//**********************************คนที่ 3********************************** */  

			if ($TeamMember >= 3) {
				if ($this->request->data['Member']['fname3'] != null) {
					$add_prefixes = $this->Prefix->find('first', array(
						'conditions' => array(
							'Prefix.id' => $this->request->data['Member']['prefix_id3'],
						),
						'fields' => array('Prefix.id', 'Prefix.name')
					));
					$committee_name = $add_prefixes['Prefix']['name'] . '' . $this->request->data['Member']['fname3'] . ' ' . $this->request->data['Member']['lname3'];

					$data2 = array(
						'school' => $this->request->data['Member']['school'],
						'team_no' => $this->request->data['Member']['team_no'],
						'school_address' => $this->request->data['Member']['school_address'],
						'school_province' => $this->request->data['Member']['school_province'],
						'school_amphoe' => $this->request->data['Member']['school_amphoe'],
						'school_tambon' => $this->request->data['Member']['school_tambon'],
						'school_zipcode' => $this->request->data['Member']['school_zipcode'],
						'prefix_id' => $add_prefixes['Prefix']['id'],
						'fname' => $this->request->data['Member']['fname3'],
						'lname' => $this->request->data['Member']['lname3'],
						'age' => $this->request->data['Member']['age3'],
						'phone' => $this->request->data['Member']['phone3'],
						'level' => $this->request->data['Member']['level3'],
						'teacher' => $this->request->data['Member']['teacher'],
						'phoneteacher' => $this->request->data['Member']['phoneteacher'],
						'emailteacher' => $this->request->data['Member']['emailteacher'],
						'project_id' => $projectId,
						'file' => $files[0]['name'],
						'file1' => $file_project,

					);

					array_push($data, $data2);
				}
			}

			//**********************************คนที่ 4********************************** */  
			if ($TeamMember >= 4) {
				if ($this->request->data['Member']['fname4'] != null) {
					$add_prefixes = $this->Prefix->find('first', array(
						'conditions' => array(
							'Prefix.id' => $this->request->data['Member']['prefix_id4'],
						),
						'fields' => array('Prefix.id', 'Prefix.name')
					));
					$committee_name = $add_prefixes['Prefix']['name'] . '' . $this->request->data['Member']['fname4'] . ' ' . $this->request->data['Member']['lname4'];

					$data2 = array(
						'school' => $this->request->data['Member']['school'],
						'team_no' => $this->request->data['Member']['team_no'],
						'school_address' => $this->request->data['Member']['school_address'],
						'school_province' => $this->request->data['Member']['school_province'],
						'school_amphoe' => $this->request->data['Member']['school_amphoe'],
						'school_tambon' => $this->request->data['Member']['school_tambon'],
						'school_zipcode' => $this->request->data['Member']['school_zipcode'],
						'prefix_id' => $add_prefixes['Prefix']['id'],
						'fname' => $this->request->data['Member']['fname4'],
						'lname' => $this->request->data['Member']['lname4'],
						'age' => $this->request->data['Member']['age4'],
						'phone' => $this->request->data['Member']['phone4'],
						'level' => $this->request->data['Member']['level4'],
						'teacher' => $this->request->data['Member']['teacher'],
						'phoneteacher' => $this->request->data['Member']['phoneteacher'],
						'emailteacher' => $this->request->data['Member']['emailteacher'],
						'project_id' => $projectId,
						'file' => $files[0]['name'],
						'file1' => $file_project,

					);

					array_push($data, $data2);
				}
			}

			//debug($data);
			//************ */
			$this->Member->saveMany($data);



			$this->Session->setFlash('ดำเนินการสมัครเรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			// $this->redirect(array('action' => 'studentchecklist',$projectId));
			$this->redirect(array('action' => 'index'));
		}


		$prefixes = $this->Prefix->find('list');

		$this->set(array('project' => $project, 'prefixes' => $prefixes));
	}

	public function login_checklist($project_id = null)
	{
		$this->layout = 'login';

		// $students = $this->Session->read('students');
		// $studentLevelApp = $this->Session->read('studentLevelApp');

		// if(isset($students)){
		// 	$this->set('students', $students);


		// 	if($this->action == 'logins')
		// 			$this->redirect(array('action' => 'list_all_graduate'));
		// }
		// else{
		// 	if($this->action != 'logins')				
		// 		$this->redirect(array('controller' => 'webs','action' => 'logoutgraduate'));
		// }	
		if ($this->request->is('post')) {

			$Members = $this->Member->find('first', array(
				'conditions' => array(
					'emailteacher' => $this->request->data['emailteacher'],
					// 'keycode' => $this->request->data['keycode'] , 
				),
				'order' => array('Member.emailteacher' => 'DESC'),

			));
			if (count($Members) > 0) {
				$this->Session->write('Members', $Members);
				// $this->redirect(array('controller' => 'main','action' => 'studentchecklist',$Members['Member']['emailteacher']));
				$this->redirect(array('controller' => 'main', 'action' => 'studentchecklist', $project_id, $Members['Member']['emailteacher']));
			} else {
				$this->Session->setFlash('ขออภัย อีเมลอาจารย์ผู้ควบคุมดูแลไม่ถูกต้อง');
				$this->Session->write('alertType', 'danger');
				$this->redirect(array('controller' => 'main', 'action' => 'index'));
			}
		}
	}

	public function checklist($projectId = null)
	{
		$project = $this->Project->findById($projectId);

		if ($project != null) {
			if ($project['Project']['id'] == 43) {
				$this->redirect(array('action' => 'training'));
			}

			$citizenId = $this->Session->read('citizen_id');
			if (isset($citizenId)) {
				$members = $this->Member->find('all', array(
					'conditions' => array('Member.project_id' => $projectId, 'Member.citizen_id' => $citizenId)
				));

				if ($members == null) {
					$this->logout($projectId);
				}
			} else {
				$members = $this->Member->find('all', array(
					'conditions' => array('Member.project_id' => $projectId),
					'order' => array('Member.fname' => 'ASC')
				));
			}

			$admin = $this->Session->read('admin');
			if (isset($admin)) {
				$statuses = $this->Status->find('list');
				$this->set(array('statuses' => $statuses));


				if ($project['Project']['type_id'] == 7) {
					$flowerGroups = $this->ProjectFlowerGroup->find('all', array(
						'conditions' => array('ProjectFlowerGroup.project_id' => $project['Project']['id'])
					));


					$data = array();
					foreach ($members as $member) {
						$memberFlowerGroups = $this->MemberFlowerGroup->find('all', array(
							'conditions' => array('MemberFlowerGroup.member_id' => $member['Member']['id'])
						));

						$data2 = array();
						foreach ($memberFlowerGroups as $memberFlowerGroup) {
							$data2 += array($memberFlowerGroup['MemberFlowerGroup']['flower_group_id'] => $memberFlowerGroup['MemberFlowerGroup']['amount']);
						}

						$data += array($member['Member']['id'] => $data2);
					}

					$this->set(array('flowerGroups' => $flowerGroups, 'memberFlowerGroupAmounts' => $data));
				}

				if ($this->request->data) {
					if ($this->Member->save($this->request->data)) {
						$this->redirect(array('action' => 'checklist', $projectId));
					}
				}
			}

			$this->set(array('project' => $project, 'members' => $members, 'citizenId' => $citizenId, 'admin' => $admin));
		} else {
			$this->redirect(array('action' => 'index'));
		}
	}

	public function list_print($projectId = null)
	{
		$this->layout = "";
		$UserName = $this->Session->read('MisEmployee');

		$this->set(array('UserName' => $UserName));

		$project = $this->Project->findById($projectId);

		if ($project != null) {
			$citizenId = $this->Session->read('citizen_id');
			if (isset($citizenId)) {
				$members = $this->Member->find('all', array(
					'conditions' => array('Member.project_id' => $projectId, 'Member.citizen_id' => $citizenId)
				));

				if ($members == null) {
					$this->logout($projectId);
				}
			} else {
				// $members = $this->Member->find('all', array(
				// 	'conditions' => array('Member.project_id' => $projectId)
				// ));	

				//ส่วนที่แก้ไข
				$schools = $this->Member->find('all', array(
					'fields' => array('Member.school'),
					'conditions' => array(
						'Member.project_id' => $projectId
					),
					'order' => array('Member.id'),
					'group' => array('Member.school')
				));

				$students = $this->Member->find('all', array(
					'conditions' => array('Member.project_id' => $projectId),
					'order' => array('Member.id'),
				));
			}

			$admin = $this->Session->read('admin');
			if (isset($admin)) {
				$statuses = $this->Status->find('list');
				$this->set(array('statuses' => $statuses));

				if ($this->request->data) {
					if ($this->Member->save($this->request->data)) {
						$this->redirect(array('action' => 'checklist', $projectId));
					}
				}
			}

			$this->set(array(
				'project' => $project,
				// 'members' => $members,
				'citizenId' => $citizenId,
				'admin' => $admin,
				// 'output' => $output,
				'schools' => $schools,
				'students' => $students,
			));
		} else {
			$this->redirect(array('action' => 'index'));
		}
	}

	public function studentchecklist($projectId = null, $emailteacher = null)
	{
		$UserName = $this->Session->read('MisEmployee');

		$this->set(array('UserName' => $UserName));

		$project = $this->Project->findById($projectId);

		// $this->set(array('project_id' => $emailteacher));

		if ($project != null) {
			$citizenId = $this->Session->read('citizen_id');
			if (isset($citizenId)) {
				$members = $this->Member->find('all', array(
					'conditions' => array('Member.project_id' => $projectId, 'Member.citizen_id' => $citizenId)
				));

				if ($members == null) {
					$this->logout($projectId);
				}
			} else {
				// $members = $this->Member->find('all', array(
				// 	'conditions' => array('Member.project_id' => $projectId)
				// ));	

				//ส่วนที่แก้ไข
				$schools = $this->Member->find('all', array(
					'fields' => array('Member.school'),
					'conditions' => array(
						'Member.project_id' => $projectId,
						'Member.emailteacher' => $emailteacher
					),
					'order' => array('Member.id'),
					'group' => array('Member.school')
				));

				$students = $this->Member->find('all', array(
					'conditions' => array('Member.project_id' => $projectId),
					'order' => array('Member.id'),
				));
			}

			$admin = $this->Session->read('admin');
			if (isset($admin)) {
				$statuses = $this->Status->find('list');
				$this->set(array('statuses' => $statuses));

				if ($this->request->data) {
					if ($this->Member->save($this->request->data)) {
						$this->redirect(array('action' => 'checklist', $projectId));
					}
				}
			}

			$this->set(array(
				'project' => $project,
				// 'members' => $members,
				'citizenId' => $citizenId,
				'admin' => $admin,
				// 'output' => $output,
				'schools' => $schools,
				'students' => $students,
			));
		} else {
			$this->redirect(array('action' => 'index'));
		}
	}

	// public function studentchecklist2($projectId = null) {
	// 	$project = $this->Project->findById($projectId);
	// 	$output="";
	// 	if($project != null){
	// 			$members = $this->Member->find('all', array(
	// 				'conditions' => array(
	// 					'Member.project_id' => $projectId
	// 				),
	// 				'order' => array('Member.id' => 'ASC') 
	// 			));	
	// 			$i =0;
	// 			foreach ($members as $member ) {
	// 				$i++;
	// 				$memberDocuments = $this->MemberDocument->find('all', array(
	// 					'conditions' => array('MemberDocument.member_id' => $member['Member']['id']
	// 				),
	// 				'order' => array('MemberDocument.member_id' => 'ASC') 
	// 				));
	// 				$output .=
	// 				'<tr>
	// 					<td data-title="ลำดับ" align="center">'.$i.'</td>
	// 					<td data-title="ชื่อ - นามสกุล">'.$member['Prefix']['name'].''.$member['Member']['fname'].' '.$member['Member']['lname'].'

	// 					</td>
	// 					<td data-title="ชื่อโรงเรียน">'.$member['Member']['school'].'

	// 					</td>
	// 					<td data-title="ระดับ">'.$member['Member']['level'].'

	// 					</td>
	// 					<td data-title="อาจารย์ผู้ควบคุม">'.$member['Member']['teacher'].'							

	// 					</td>
	// 					<td data-title="ใบสมัคร" style="text-align: center;">';	
	// 					 foreach ($memberDocuments as $MemberDocument) { 

	// 						$output .='
	// 						<a href="'.$this->urls.'/files/Document/'.$MemberDocument['MemberDocument']['name'].'" 
	// 						class="btn btn-primary" target="_blank">
	// 							ใบสมัคร '.$MemberDocument['MemberDocument']['name'].'
	// 							<span class="glyphicon glyphicon-cloud-download"></span>
	// 						</a>
	// 						';
	// 					 } 

	// 					$output .='</td>';
	// 					$output .='
	// 					<td data-title="ยกเลิกการลงทะเบียน" style="text-align: center;">
	// 						<a href="'.$this->urls.'/main/delete_member/'.$project['Project']['id'].'/'.$member['Member']['id'].'" class="btn btn-danger">
	// 							ยกเลิก
	// 						</a>
	// 				    </td>';
	// 				$output .='	</tr>';
	// 			}


	// 		$admin = $this->Session->read('admin');
	// 		if(isset($admin)){
	// 			$statuses = $this->Status->find('list');
	// 			$this->set(array('statuses' => $statuses));

	// 			if($this->request->data){
	// 				if($this->Member->save($this->request->data)){
	// 					$this->redirect(array('action' => 'checklist',$projectId));
	// 				}	
	// 			}
	// 		}

	// 		$this->set(array(
	// 		'project' => $project,
	// 		'members' => $members,			
	// 		'admin' => $admin,
	// 		'output' => $output
	// 	));
	// 	}
	// 	else{
	// 		$this->redirect(array('action' => 'index'));
	// 	}
	// }

	// public function login($projectId = null) {
	// 	$project = $this->Project->findById($projectId);

	// 	if($project != null){
	// 		if($this->request->data){
	// 			$members = $this->Member->find('all', array(
	// 				'conditions' => array('Member.project_id' => $projectId,'Member.citizen_id' => $this->request->data['Member']['citizen_id'])
	// 			));	

	// 			if($members != null){
	// 				$this->Session->write('citizen_id',$this->request->data['Member']['citizen_id']);
	// 				$this->Session->write('citizen_idTimeout',date("Y-m-d H:i:s",strtotime(date("Y-m-d H:i:s")) + 1800));

	// 				$this->redirect(array('action' => 'checklist',$projectId));
	// 			}
	// 			else{
	// 				$alert = 'หมายเลขบัตรประชาชน ไม่ถูกต้อง, กรุณาลองใหม่อีกครั้ง';

	// 				$this->Session->setFlash($alert);
	// 			}
	// 		}	

	// 		$this->set(array('project' => $project));
	// 	}
	// 	else{
	// 		$this->redirect(array('action' => 'index'));
	// 	}
	// }

	// public function logout($projectId = null) {	
	// 	$this->Session->delete('citizen_id');
	// 	$this->Session->delete('citizen_idTimeout');

	// 	$this->redirect(array('action' => 'checklist',$projectId));
	// }

	public function adminchecklist($projectId = null)
	{
		$UserName = $this->Session->read('MisEmployee');

		$this->set(array('UserName' => $UserName));

		$project = $this->Project->findById($projectId);

		if ($project != null) {
			$citizenId = $this->Session->read('citizen_id');
			if (isset($citizenId)) {
				$members = $this->Member->find('all', array(
					'conditions' => array('Member.project_id' => $projectId, 'Member.citizen_id' => $citizenId)
				));

				if ($members == null) {
					$this->logout($projectId);
				}
			} else {
				// $members = $this->Member->find('all', array(
				// 	'conditions' => array('Member.project_id' => $projectId)
				// ));	

				//ส่วนที่แก้ไข
				$schools = $this->Member->find('all', array(
					'fields' => array('Member.school'),
					'conditions' => array(
						'Member.project_id' => $projectId
					),
					'order' => array('Member.id'),
					'group' => array('Member.school')
				));

				$students = $this->Member->find('all', array(
					'conditions' => array('Member.project_id' => $projectId),
					'order' => array('Member.id'),
				));
			}

			$admin = $this->Session->read('admin');
			if (isset($admin)) {
				$statuses = $this->Status->find('list');
				$this->set(array('statuses' => $statuses));

				if ($this->request->data) {
					if ($this->Member->save($this->request->data)) {
						$this->redirect(array('action' => 'checklist', $projectId));
					}
				}
			}

			$this->set(array(
				'project' => $project,
				// 'members' => $members,
				'citizenId' => $citizenId,
				'admin' => $admin,
				// 'output' => $output,
				'schools' => $schools,
				'students' => $students,
			));
		} else {
			$this->redirect(array('action' => 'index'));
		}
	}
	public function announce($projectId = null)
	{



		//**************Dropdown year ************** */
		$dropdowns = $this->Project->find('all', array(
			'conditions' => array(),


			'order' => array('Project.id' => 'asc'),

		));
		if ($projectId == null) {
			$projects = $this->Project->find('all', array(
				'conditions' => array(
					'Project.id' => 1,
				),
				'order' => array('Project.id' => 'DESC')
			));

			//**************************** */
			$project = $this->Project->find('first', array(
				'conditions' => array(
					'Project.id' => 1,

				),
				'order' => array('Project.id' => 'asc'),

			));
			$schools = $this->Member->find('all', array(
				'fields' => array('Member.school'),
				'conditions' => array(
					'Member.project_id' => 1
				),
				'order' => array('Member.id'),
				'group' => array('Member.school')
			));
		} else {
			$projects = $this->Project->find('all', array(
				'conditions' => array(
					'Project.id' => $projectId,

				)
			));

			//**************************** */
			$project = $this->Project->find('first', array(
				'conditions' => array(
					'Project.id' => $projectId,

				),
				'order' => array('Project.id' => 'asc'),

			));
			$schools = $this->Member->find('all', array(
				'fields' => array('Member.school'),
				'conditions' => array(
					'Member.project_id' => $projectId
				),
				'order' => array('Member.id'),
				'group' => array('Member.school')
			));
		}
		$this->set(array(
			'projects' => $projects,
			'dropdowns' => $dropdowns,

		));
		//**************Close Dropdown ************** */


		$this->set(array(
			'projectId' => $projectId,


		));

		//ส่วนที่แก้ไข


		$students = $this->Member->find('all', array(
			'conditions' => array('Member.project_id' => $projectId),
			'order' => array('Member.id'),
		));

		$this->set(array(
			'project' => $project,
			// 'members' => $members,
			// 'output' => $output,
			'schools' => $schools,
			'students' => $students,
		));
	}
	public function delete_member($projectId = null, $id = null)
	{


		$member = $this->Member->find('first', array(
			'conditions' => array(
				'Member.project_id' => $projectId,
				'Member.id' => $id
			)
		));

		if ($member) {
			$this->Member->delete($id);
		}


		$this->redirect(array('action' => 'studentchecklist2', $projectId));
	}

	public function member_detail($id = null)
	{

		$admin = $this->Session->read('admin');

		if (isset($admin)) {
			$member = $this->Member->find('first', array(
				'conditions' => array('Member.id' => $id)
			));

			if ($member) {
				$project = $this->Project->findById($member['Member']['project_id']);

				$this->set(array('project' => $project, 'member' => $member, 'admin' => $admin));
			} else {
				$this->redirect(array('action' => 'checklist', $projectId));
			}
		} else {
			$this->redirect(array('action' => 'checklist', $projectId));
		}
	}
	//==========================ทัศนศึกษา=============================
	public function list_date_guide()
	{
		$UserName = $this->Session->read('MisEmployee');

		$this->set('UserName', $UserName);
		// $listDateguides = $this->Dateguide->find('all',array(

		// 		'order' => array('id' => 'ASC') 
		// 	));
		$Guides = $this->Guide->find('all', array(
			// 'conditions' => array('Guide.dateguide_id' => $Id),
			'order' => array('Guide.dateguide_id,Guide.start,Guide.end' => 'ASC')
		));
		$schools = $this->Guide->find('all', array(
			'fields' => array('Guide.organize'),
			'order' => array('Guide.id'),
			'group' => array('Guide.organize')
		));

		// debug($schools);

		$this->set(array(
			'Guides' => $Guides,
			'schools' => $schools,
		));
	}

	public function list_date_guide_people()
	{
		$UserName = $this->Session->read('MisEmployee');

		$this->set('UserName', $UserName);
		// $listDateguides = $this->Dateguide->find('all',array(

		// 		'order' => array('id' => 'ASC') 
		// 	));
		$Guidepeoples = $this->GuidePeople->find('all', array(
			// 'conditions' => array('Guide.dateguide_id' => $Id),
			'order' => array('GuidePeople.dateguide_id,GuidePeople.start,GuidePeople.end' => 'ASC')
		));
		$schools = $this->GuidePeople->find('all', array(
			'fields' => array('GuidePeople.organize'),
			'order' => array('GuidePeople.id'),
			'group' => array('GuidePeople.organize')
		));

		// debug($schools);

		$this->set(array(
			'Guidepeoples' => $Guidepeoples,
			'schools' => $schools,
		));
	}

	public function list_date_guide_admin()
	{
		// $listDateguides = $this->Dateguide->find('all',array(

		// 		'order' => array('id' => 'ASC') 
		// 	));
		$Guides = $this->Guide->find('all', array(
			// 'conditions' => array('Guide.dateguide_id' => $Id),
			'order' => array('Guide.dateguide_id,Guide.start,Guide.end' => 'ASC')

		));

		$this->set(array('Guides' => $Guides));
	}

	public function registerguide2()
	{

		if ($this->request->data) {
			//$this->request->data['Guide']['organize'];
			// $this->Guide->save($this->request->data);


			if ($this->Guide->save($this->request->data)) {
				$insertedId = $this->Guide->getLastInsertId();
				// debug($insertedId);	
				$data = array();
				foreach ($this->request->data['source_id'] as $sourceId) {
					$data2 = array('guide_id' => $insertedId, 'source_id' => $sourceId);
					array_push($data, $data2);
				}
				if ($this->GuideSource->saveMany($data)) {
					$this->redirect(array('action' => 'list_date_guide'));
				}
			}
			$this->redirect(array('action' => 'list_date_guide'));
		}


		$listDateguides = $this->Dateguide->find('list');
		$prefixes = $this->Prefix->find('list', array(
			'conditions' => array(
				// 'and' => array(
				// 	array('Prefix.level <=' => 2),
				// 	array('Prefix.level >=' => 1),
				// 	),
				'level' => 1
			),
			'order' => array('id' => 'ASC')

		));

		$sources = $this->Source->find('all');
		$this->set(array('prefixes' => $prefixes, 'sources' => $sources, 'listDateguides' => $listDateguides));

		//$this->redirect(array('action' => 'list_date_guide'));
	}

	public function registerguide()
	{
		if ($this->request->data) {
			$files = $this->request->data['Guide']['files'];
			if ($this->request->data['Guide']['files'][0]['type'] != null) {
				foreach ($this->request->data['Guide']['files'] as $check) {
					if ($check['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or
						"image/gif" or "application/msword" or
						"application/vnd.openxmlformats-officedocument.wordprocessingml.document" or
						"application/x-rar-compressed" or "application/octet-stream" or "application/zip" or
						"application/octet-stream" or "application/vnd.ms-powerpoint" or
						"application/vnd.openxmlformats-officedocument.presentationml.presentation" or
						"application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
						foreach ($files as $file) {
							$fileType = pathinfo($file['name'], PATHINFO_EXTENSION);
							if (
								$fileType != "doc" or "docx" or "jpg" or "jpeg" or "png" or "gif" or "zip" or "rar" or "xls" or
								"xlsx" or "ppt" or "pptx" or "pdf"
							) {
								$this->Session->setFlash('กรุณาแก้ไขชื่อไฟล์ก่อนอัปโหลด โดยไม่ให้มีอักขระ จุด .');
								$this->Session->write('alertType', 'danger');

								// $this->redirect(array('controller' => 'main', 'action' => 'registerguide'));
							}
						}
						$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
						$this->Session->write('alertType', 'danger');
					}
				}
			}
			if ($files[0]['name'] != null) {

				$Guide_id = $this->Guide->find('first', array(
					'order' => array('Guide.id' => 'DESC'),

				));

				$i = 0;
				foreach ($files as $file) {
					$filethai = iconv("UTF-8", "TIS-620",  $file['name']);
					$folder = WWW_ROOT . 'files' . DS . 'Document' . DS;

					if (!file_exists($folder))
						mkdir($folder);

					move_uploaded_file($file['tmp_name'], $folder . DS . $filethai);

					$i++;
				}
			}




			$data = array();

			$data2 = array(
				'organize' => $this->request->data['Guide']['organize'],
				'school_address' => $this->request->data['Guide']['school_address'],
				'school_tambon' => $this->request->data['Guide']['school_tambon'],
				'school_amphoe' => $this->request->data['Guide']['school_amphoe'],
				'school_province' => $this->request->data['Guide']['school_province'],
				'school_zipcode' => $this->request->data['Guide']['school_zipcode'],
				'total_attendees' => $this->request->data['Guide']['total_attendees'],
				'dateguide_id' => $this->request->data['Guide']['dateguide_id'],
				'round' => $this->request->data['Guide']['round'],
				'prefix_teacher_id' => $this->request->data['Guide']['prefix_teacher_id'],
				'fname_teacher' => $this->request->data['Guide']['fname_teacher'],
				'reftel' => $this->request->data['Guide']['reftel'],
				'file' => $files[0]['name'],



			);

			array_push($data, $data2);

			//debug($data);
			$this->Guide->saveMany($data);
			$this->Session->setFlash('ดำเนินการลงทะเบียนเรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect(array('action' => 'list_date_guide'));
		}


		$listDateguides = $this->Dateguide->find('list');
		$prefixes = $this->Prefix->find('list', array(
			'conditions' => array(
				// 'and' => array(
				// 	array('Prefix.level <=' => 2),
				// 	array('Prefix.level >=' => 1),
				// 	),
				'level' => 1
			),
			'order' => array('id' => 'ASC')
		));


		// debug($prefixes);

		$outputstudent = '';
		foreach ($prefixes as $key => $value) {
			$outputstudent .= '
				<option value="' . $key . '">' . $value . '</option>
			';
		}

		$sources = $this->Source->find('all');
		$this->set(array(
			'prefixes' => $prefixes,
			'listDateguides' => $listDateguides,
			'outputstudent' => $outputstudent,
		));
	}



	public function registerguide_people()
	{

		if ($this->request->data) {
			$data = array();
			//**********************************คนที่ 1********************************** */  
			if ($this->request->data['GuidePeople']['name1'] != null) {
				$add_prefixes = $this->Prefix->find('first', array(
					'conditions' => array(
						'Prefix.id' => $this->request->data['GuidePeople']['prefix_id1'],
					),
					'fields' => array('Prefix.id', 'Prefix.name')
				));
				$committee_name = $add_prefixes['Prefix']['name'] . '' . $this->request->data['GuidePeople']['name1'] . ' ' . $this->request->data['GuidePeople']['lname1'];

				$data2 = array(
					'organize' => $this->request->data['GuidePeople']['organize'],
					'address' => $this->request->data['GuidePeople']['address'],
					'prefix_id' => $add_prefixes['Prefix']['id'],
					'fname' => $this->request->data['GuidePeople']['name1'],
					'lname' => $this->request->data['GuidePeople']['lname1'],
					'dateguide_id' => $this->request->data['GuidePeople']['dateguide_id'],
					'round' => $this->request->data['GuidePeople']['round'],
					// 'prefix_teacher_id' => $this->request->data['GuidePeople']['prefix_teacher_id'],
					// 'fname_teacher' => $this->request->data['GuidePeople']['fname_teacher'],
					'reftel' => $this->request->data['GuidePeople']['reftel'],
					'refline' => $this->request->data['GuidePeople']['refline'],

				);

				array_push($data, $data2);
			}
			//**********************************คนที่ 2********************************** */  
			if ($this->request->data['GuidePeople']['name2'] != null) {
				$add_prefixes = $this->Prefix->find('first', array(
					'conditions' => array(
						'Prefix.id' => $this->request->data['GuidePeople']['prefix_id2'],
					),
					'fields' => array('Prefix.id', 'Prefix.name')
				));
				$committee_name = $add_prefixes['Prefix']['name'] . '' . $this->request->data['GuidePeople']['name2'] . ' ' . $this->request->data['GuidePeople']['lname2'];


				$data2 = array(
					'organize' => $this->request->data['GuidePeople']['organize'],
					'address' => $this->request->data['GuidePeople']['address'],
					'prefix_id' => $add_prefixes['Prefix']['id'],
					'fname' => $this->request->data['GuidePeople']['name2'],
					'lname' => $this->request->data['GuidePeople']['lname2'],
					'dateguide_id' => $this->request->data['GuidePeople']['dateguide_id'],
					'round' => $this->request->data['GuidePeople']['round'],
					'prefix_teacher_id' => $this->request->data['GuidePeople']['prefix_teacher_id'],
					'fname_teacher' => $this->request->data['GuidePeople']['fname_teacher'],
					'reftel' => $this->request->data['GuidePeople']['reftel'],
					'refline' => $this->request->data['GuidePeople']['refline'],

				);

				array_push($data, $data2);
			}
			//**********************************คนที่ 3********************************** */  
			if ($this->request->data['GuidePeople']['name3'] != null) {
				$add_prefixes = $this->Prefix->find('first', array(
					'conditions' => array(
						'Prefix.id' => $this->request->data['GuidePeople']['prefix_id3'],
					),
					'fields' => array('Prefix.id', 'Prefix.name')
				));
				$committee_name = $add_prefixes['Prefix']['name'] . '' . $this->request->data['GuidePeople']['name3'] . ' ' . $this->request->data['GuidePeople']['lname3'];


				$data2 = array(
					'organize' => $this->request->data['GuidePeople']['organize'],
					'address' => $this->request->data['GuidePeople']['address'],
					'prefix_id' => $add_prefixes['Prefix']['id'],
					'fname' => $this->request->data['GuidePeople']['name3'],
					'lname' => $this->request->data['GuidePeople']['lname3'],
					'dateguide_id' => $this->request->data['GuidePeople']['dateguide_id'],
					'round' => $this->request->data['GuidePeople']['round'],
					'prefix_teacher_id' => $this->request->data['GuidePeople']['prefix_teacher_id'],
					'fname_teacher' => $this->request->data['GuidePeople']['fname_teacher'],
					'reftel' => $this->request->data['GuidePeople']['reftel'],
					'refline' => $this->request->data['GuidePeople']['refline'],
				);

				array_push($data, $data2);
			}
			//**********************************คนที่ 4********************************** */  
			if ($this->request->data['GuidePeople']['name4'] != null) {
				$add_prefixes = $this->Prefix->find('first', array(
					'conditions' => array(
						'Prefix.id' => $this->request->data['GuidePeople']['prefix_id4'],
					),
					'fields' => array('Prefix.id', 'Prefix.name')
				));
				$committee_name = $add_prefixes['Prefix']['name'] . '' . $this->request->data['GuidePeople']['name4'] . ' ' . $this->request->data['GuidePeople']['lname4'];


				$data2 = array(
					'organize' => $this->request->data['GuidePeople']['organize'],
					'address' => $this->request->data['GuidePeople']['address'],
					'prefix_id' => $add_prefixes['Prefix']['id'],
					'fname' => $this->request->data['GuidePeople']['name4'],
					'lname' => $this->request->data['GuidePeople']['lname4'],
					'dateguide_id' => $this->request->data['GuidePeople']['dateguide_id'],
					'round' => $this->request->data['GuidePeople']['round'],
					'prefix_teacher_id' => $this->request->data['GuidePeople']['prefix_teacher_id'],
					'fname_teacher' => $this->request->data['GuidePeople']['fname_teacher'],
					'reftel' => $this->request->data['GuidePeople']['reftel'],
					'refline' => $this->request->data['GuidePeople']['refline'],
				);

				array_push($data, $data2);
			}
			//**********************************คนที่ 5********************************** */  
			if ($this->request->data['GuidePeople']['name5'] != null) {
				$add_prefixes = $this->Prefix->find('first', array(
					'conditions' => array(
						'Prefix.id' => $this->request->data['GuidePeople']['prefix_id5'],
					),
					'fields' => array('Prefix.id', 'Prefix.name')
				));
				$committee_name = $add_prefixes['Prefix']['name'] . '' . $this->request->data['GuidePeople']['name5'] . ' ' . $this->request->data['GuidePeople']['lname5'];


				$data2 = array(
					'organize' => $this->request->data['GuidePeople']['organize'],
					'address' => $this->request->data['GuidePeople']['address'],
					'prefix_id' => $add_prefixes['Prefix']['id'],
					'fname' => $this->request->data['GuidePeople']['name5'],
					'lname' => $this->request->data['GuidePeople']['lname5'],
					'dateguide_id' => $this->request->data['GuidePeople']['dateguide_id'],
					'round' => $this->request->data['GuidePeople']['round'],
					'prefix_teacher_id' => $this->request->data['GuidePeople']['prefix_teacher_id'],
					'fname_teacher' => $this->request->data['GuidePeople']['fname_teacher'],
					'reftel' => $this->request->data['GuidePeople']['reftel'],
					'refline' => $this->request->data['GuidePeople']['refline'],
				);

				array_push($data, $data2);
			}
			//**********************************คนที่ 6********************************** */  
			if ($this->request->data['GuidePeople']['name6'] != null) {
				$add_prefixes = $this->Prefix->find('first', array(
					'conditions' => array(
						'Prefix.id' => $this->request->data['GuidePeople']['prefix_id6'],
					),
					'fields' => array('Prefix.id', 'Prefix.name')
				));
				$committee_name = $add_prefixes['Prefix']['name'] . '' . $this->request->data['GuidePeople']['name6'] . ' ' . $this->request->data['GuidePeople']['lname6'];


				$data2 = array(
					'organize' => $this->request->data['GuidePeople']['organize'],
					'address' => $this->request->data['GuidePeople']['address'],
					'prefix_id' => $add_prefixes['Prefix']['id'],
					'fname' => $this->request->data['GuidePeople']['name6'],
					'lname' => $this->request->data['GuidePeople']['lname6'],
					'dateguide_id' => $this->request->data['GuidePeople']['dateguide_id'],
					'round' => $this->request->data['GuidePeople']['round'],
					'prefix_teacher_id' => $this->request->data['GuidePeople']['prefix_teacher_id'],
					'fname_teacher' => $this->request->data['GuidePeople']['fname_teacher'],
					'reftel' => $this->request->data['GuidePeople']['reftel'],
					'refline' => $this->request->data['GuidePeople']['refline'],
				);

				array_push($data, $data2);
			}
			//**********************************คนที่ 7********************************** */  
			if ($this->request->data['GuidePeople']['name7'] != null) {
				$add_prefixes = $this->Prefix->find('first', array(
					'conditions' => array(
						'Prefix.id' => $this->request->data['GuidePeople']['prefix_id7'],
					),
					'fields' => array('Prefix.id', 'Prefix.name')
				));
				$committee_name = $add_prefixes['Prefix']['name'] . '' . $this->request->data['GuidePeople']['name7'] . ' ' . $this->request->data['GuidePeople']['lname7'];


				$data2 = array(
					'organize' => $this->request->data['GuidePeople']['organize'],
					'address' => $this->request->data['GuidePeople']['address'],
					'prefix_id' => $add_prefixes['Prefix']['id'],
					'fname' => $this->request->data['GuidePeople']['name7'],
					'lname' => $this->request->data['GuidePeople']['lname7'],
					'dateguide_id' => $this->request->data['GuidePeople']['dateguide_id'],
					'round' => $this->request->data['GuidePeople']['round'],
					'prefix_teacher_id' => $this->request->data['GuidePeople']['prefix_teacher_id'],
					'fname_teacher' => $this->request->data['GuidePeople']['fname_teacher'],
					'reftel' => $this->request->data['GuidePeople']['reftel'],
					'refline' => $this->request->data['GuidePeople']['refline'],
				);

				array_push($data, $data2);
			}
			//**********************************คนที่ 8********************************** */  
			if ($this->request->data['GuidePeople']['name8'] != null) {
				$add_prefixes = $this->Prefix->find('first', array(
					'conditions' => array(
						'Prefix.id' => $this->request->data['GuidePeople']['prefix_id8'],
					),
					'fields' => array('Prefix.id', 'Prefix.name')
				));
				$committee_name = $add_prefixes['Prefix']['name'] . '' . $this->request->data['GuidePeople']['name8'] . ' ' . $this->request->data['GuidePeople']['lname8'];


				$data2 = array(
					'organize' => $this->request->data['GuidePeople']['organize'],
					'address' => $this->request->data['GuidePeople']['address'],
					'prefix_id' => $add_prefixes['Prefix']['id'],
					'fname' => $this->request->data['GuidePeople']['name8'],
					'lname' => $this->request->data['GuidePeople']['lname8'],
					'dateguide_id' => $this->request->data['GuidePeople']['dateguide_id'],
					'round' => $this->request->data['GuidePeople']['round'],
					'prefix_teacher_id' => $this->request->data['GuidePeople']['prefix_teacher_id'],
					'fname_teacher' => $this->request->data['GuidePeople']['fname_teacher'],
					'reftel' => $this->request->data['GuidePeople']['reftel'],
					'refline' => $this->request->data['GuidePeople']['refline'],
				);

				array_push($data, $data2);
			}
			//**********************************คนที่ 9********************************** */  
			if ($this->request->data['GuidePeople']['name9'] != null) {
				$add_prefixes = $this->Prefix->find('first', array(
					'conditions' => array(
						'Prefix.id' => $this->request->data['GuidePeople']['prefix_id9'],
					),
					'fields' => array('Prefix.id', 'Prefix.name')
				));
				$committee_name = $add_prefixes['Prefix']['name'] . '' . $this->request->data['GuidePeople']['name9'] . ' ' . $this->request->data['GuidePeople']['lname9'];


				$data2 = array(
					'organize' => $this->request->data['GuidePeople']['organize'],
					'address' => $this->request->data['GuidePeople']['address'],
					'prefix_id' => $add_prefixes['Prefix']['id'],
					'fname' => $this->request->data['GuidePeople']['name9'],
					'lname' => $this->request->data['GuidePeople']['lname9'],
					'dateguide_id' => $this->request->data['GuidePeople']['dateguide_id'],
					'round' => $this->request->data['GuidePeople']['round'],
					'prefix_teacher_id' => $this->request->data['GuidePeople']['prefix_teacher_id'],
					'fname_teacher' => $this->request->data['GuidePeople']['fname_teacher'],
					'reftel' => $this->request->data['GuidePeople']['reftel'],
					'refline' => $this->request->data['GuidePeople']['refline'],
				);

				array_push($data, $data2);
			}
			//**********************************คนที่ 10********************************** */  
			if ($this->request->data['GuidePeople']['name10'] != null) {
				$add_prefixes = $this->Prefix->find('first', array(
					'conditions' => array(
						'Prefix.id' => $this->request->data['GuidePeople']['prefix_id10'],
					),
					'fields' => array('Prefix.id', 'Prefix.name')
				));
				$committee_name = $add_prefixes['Prefix']['name'] . '' . $this->request->data['GuidePeople']['name10'] . ' ' . $this->request->data['GuidePeople']['lname10'];


				$data2 = array(
					'organize' => $this->request->data['GuidePeople']['organize'],
					'address' => $this->request->data['GuidePeople']['address'],
					'prefix_id' => $add_prefixes['Prefix']['id'],
					'fname' => $this->request->data['GuidePeople']['name10'],
					'lname' => $this->request->data['GuidePeople']['lname10'],
					'dateguide_id' => $this->request->data['GuidePeople']['dateguide_id'],
					'round' => $this->request->data['GuidePeople']['round'],
					'prefix_teacher_id' => $this->request->data['GuidePeople']['prefix_teacher_id'],
					'fname_teacher' => $this->request->data['GuidePeople']['fname_teacher'],
					'reftel' => $this->request->data['GuidePeople']['reftel'],
					'refline' => $this->request->data['GuidePeople']['refline'],
				);

				array_push($data, $data2);
			}
			//debug($data);
			//************ */
			$this->GuidePeople->saveMany($data);
			$this->redirect(array('action' => 'list_date_guide_people'));
		}


		$listDateguides = $this->Dateguide->find('list');
		$prefixes = $this->Prefix->find('list', array(
			'conditions' => array(
				// 'and' => array(
				// 	array('Prefix.level <=' => 2),
				// 	array('Prefix.level >=' => 1),
				// 	),
				'level' => 1
			),
			'order' => array('id' => 'ASC')
		));


		// debug($prefixes);

		$outputstudent = '';
		foreach ($prefixes as $key => $value) {
			$outputstudent .= '
				<option value="' . $key . '">' . $value . '</option>
			';
		}

		$sources = $this->Source->find('all');
		$this->set(array(
			'prefixes' => $prefixes,
			'listDateguides' => $listDateguides,
			'outputstudent' => $outputstudent,
		));
	}


	// การแข่งขันนาเสนอผลงานทางวิชาการภาคโปสเตอร์ของนักศึกษา ระดับชั้นปีที่ 3และปีที่ 4 คณะเกษตรศาสตร์

	public function member_student()
	{
		// // $this->layout = "internship2s";


		//input data
		if ($this->request->data) {
			$this->MemberStudent->save($this->request->data);

			$MemberStudents = $this->MemberStudent->find('first', array(
				'conditions' => array(
					// 'CurriculumRequest.mis_employee_id' => $UserName['MisEmployee']['id'] ,
				),
				'order' => array('MemberStudent.student_id' => 'DESC')
			));

			$files = $this->request->data['MemberStudentDocument']['files'];
			if ($this->request->data['MemberStudentDocument']['files'][0]['type'] != null) {
				foreach ($this->request->data['MemberStudentDocument']['files'] as $check) {
					if ($check['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or
						"image/gif" or "application/msword" or
						"application/vnd.openxmlformats-officedocument.wordprocessingml.document" or
						"application/x-rar-compressed" or "application/octet-stream" or "application/zip" or
						"application/octet-stream" or "application/vnd.ms-powerpoint" or
						"application/vnd.openxmlformats-officedocument.presentationml.presentation" or
						"application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {
						foreach ($files as $file) {
							$fileType = pathinfo($file['name'], PATHINFO_EXTENSION);
							if (
								$fileType != "doc" or "docx" or "jpg" or "jpeg" or "png" or "gif" or "zip" or "rar" or "xls" or
								"xlsx" or "ppt" or "pptx" or "pdf"
							) {
								$this->Session->setFlash('กรุณาแก้ไขชื่อไฟล์ก่อนอัปโหลด โดยไม่ให้มีอักขระ จุด .');
								$this->Session->write('alertType', 'danger');
							}
						}
						$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
						$this->Session->write('alertType', 'danger');
					}
				}
			}

			if ($files[0]['name'] != null) {
				$i = 0;
				foreach ($files as $file) {
					$fileType = pathinfo($file['name'], PATHINFO_EXTENSION);
					$this->request->data['MemberStudentDocument']['name'] = date('Y-m-d His') . "_" . $file['name'];
					$this->request->data['MemberStudentDocument']['name_old'] = $file['name'];
					$this->request->data['MemberStudentDocument']['post_date'] = date('Y-m-d H:i:00');
					$this->request->data['MemberStudentDocument']['student_id'] = $MemberStudents['MemberStudent']['student_id'];

					$filethai = iconv("UTF-8", "TIS-620",  date('Y-m-d His') . "_" . $file['name']);
					if (
						$fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and
						$fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and
						$fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and
						$fileType != 'WebP' and $fileType != 'TIFF'
					) {
						$this->request->data['MemberStudentDocument']['class'] = "file";
					} else {
						$this->request->data['MemberStudentDocument']['class'] = "img";
					}

					$folder = WWW_ROOT . 'files' . DS . 'paper' . DS;

					if (!file_exists($folder))
						mkdir($folder);

					move_uploaded_file($file['tmp_name'], $folder . $filethai);
					// $files[0]['tmp_name']

					$this->MemberStudentDocument->create();
					$this->MemberStudentDocument->save($this->request->data);
				}
			}
			$this->Session->write('alertType', 'success');
			$this->Session->setFlash('บันทึกการลงทะเบียน เรียบร้อยแล้ว');
			$this->redirect(array('action' => 'index'));
		}


		// ดึงข้อมูลจากดาต้าเบส
		$prefixes = $this->Prefix->find('list');
		// $majors = $this->Major->find('list');
		$majordegrees = $this->Majordegree->find('list', array(
			'conditions' => array(
				'Majordegree.degree_id' => 1,
				// 'Majordegree.level' => 1,
			),
			'fields' => array('Majordegree.major_id', 'Majordegree.major_name'),
			'order' => array('Majordegree.id'),
		));
		$papers = $this->Paper->find('all', array(
			'order' => array('Paper.id' => 'ASC')
		));
		// debug($papers);

		$this->set(array(
			'prefixes' => $prefixes,
			'majordegrees' => $majordegrees,
			'papers' => $papers,
		));
	}

	public function member_studentchecklist()
	{
		$UserName = $this->Session->read('MisEmployee');

		$this->set(array('UserName' => $UserName));

		$projectId = 2;
		$project = $this->Project->findById($projectId);

		if ($project != null) {
			$citizenId = $this->Session->read('citizen_id');
			if (isset($citizenId)) {
				$members = $this->Member->find('all', array(
					'conditions' => array('Member.project_id' => $projectId, 'Member.citizen_id' => $citizenId)
				));

				if ($members == null) {
					$this->logout($projectId);
				}
			} else {
				// $members = $this->Member->find('all', array(
				// 	'conditions' => array('Member.project_id' => $projectId)
				// ));	

				//ส่วนที่แก้ไข
				$majordegrees = $this->Majordegree->find('all', array(
					'conditions' => array(
						'Majordegree.degree_id' => 1,
						// 'Majordegree.level' => 1,
					),
					'order' => array('Majordegree.id'),
				));
				//foreach ($majordegrees as $key) {

				// $majors = $this->Major->find('all', array(
				// 	'conditions' => array(
				// 		'Major.level' => 1,
				// 	),
				// 	'order' => array('Major.id'),
				// ));
				//}
				//debug($majordegrees);
				$students = $this->MemberStudent->find('all', array(
					'order' => array('MemberStudent.id'),
				));
				$studentDocuments = $this->MemberStudentDocument->find('all', array(
					'order' => array('MemberStudentDocument.id'),
				));
			}

			$admin = $this->Session->read('admin');
			if (isset($admin)) {
				$statuses = $this->Status->find('list');
				$this->set(array('statuses' => $statuses));

				if ($this->request->data) {
					if ($this->Member->save($this->request->data)) {
						$this->redirect(array('action' => 'checklist', $projectId));
					}
				}
			}

			$this->set(array(
				'project' => $project,
				// 'members' => $members,
				'citizenId' => $citizenId,
				'admin' => $admin,
				// 'output' => $output,
				// 'majors' => $majors,
				'majordegrees' => $majordegrees,
				'students' => $students,
				'studentDocuments' => $studentDocuments
			));
		} else {
			$this->redirect(array('action' => 'index'));
		}

		$this->set(array(
			'UserName' => $UserName,
			'project' => $project,
			// 'papers' => $papers,
		));
	}
	public function list_member_student_checklist()
	{
		// $this->layout = '';


		$project = $this->MemberProject->find('first', array(
			'order' => array('MemberProject.id'),
		));
		//ส่วนที่แก้ไข

		$memberStudents = $this->MemberStudent->find('all', array(
			'conditions' => array(
				// 'MemberStudent.student_id' => $Students['Student']['id'],

			),
			'order' => array('MemberStudent.id'),
		));

		$output = '';
		$i = 0;
		foreach ($memberStudents as $key) {
			$majordegrees = $this->Majordegree->find('first', array(
				'conditions' => array(
					'Majordegree.major_id' => $key['MemberStudent']['major_id'],
					'Majordegree.degree_id' => 1,

				),
				'fields' => array('Majordegree.major_id', 'Majordegree.major_name', 'Majordegree.organize_id'),
				'order' => array('Majordegree.id'),
			));
			$Organizes = $this->Organize->find('first', array(
				'conditions' => array(
					'Organize.id' => $majordegrees['Majordegree']['organize_id'],
				),


			));
			$i++;
			$output .= '<tr>';
			$output .= '<td data-title="ลำดับ" align="center">' . $i . ' </td>
						<td data-title="รหัสนักศึกษา">' . $key['MemberStudent']['student_id'] . '  
																
						</td>
						<td data-title="ชื่อ-สกุล">' . $key['MemberStudent']['student_firstname'] . ' ' . $key['MemberStudent']['student_lastname'] . '  
							 
																	
						</td>
						<td data-title="ชื่อหัวข้อภาษาไทย">' . $key['MemberStudent']['title'] . '</td>
						<td data-title="ชื่อหัวข้อภาษาอังกฤษ">' . $key['MemberStudent']['title_eng'] . '</td>
						<td data-title="อาจารย์ที่ปรึกษา">';
			if ($key['MemberStudent']['advisor_id'] != null) {
				$output .= '' . $key['Advisor']['advisor_name'] . ' ' . $key['Advisor']['advisor_sname'] . '  ';
			} else {
				$output .= '<font color="red"> ยังไม่ได้ระบุอาจารย์ที่ปรึกษา </font>  ';
			}
			$output .= '</td>';
			$output .= '<td data-title="สาขาวิชา">' . $key['Major']['major_name'] . ' </td>
						
						<td data-title="ภาควิชา">' . $Organizes['Organize']['name'] . '</td>
						
						<td data-title="ประทับตรา">' . $key['MemberStudent']['modified'] . '</td>
						<td data-title="ดูรายละเอียด" style="width:20%"> 
							<a href="' . $this->urls . '/main/member_student_detail/' . $key['MemberStudent']['id'] . '/' . $key['MemberStudent']['student_id'] . ' " class="btn btn-warning" target="_blank">
								<i class="glyphicon glyphicon-tag" aria-hidden="true"></i> ดูรายละเอียด
							</a>		
						 </td>';




			$output .= '</tr>';
		}

		$this->set(array(
			'output' => $output,
			'project' => $project,
			// 'majordegrees' => $majordegrees,
			// 'Organizes' => $Organizes,
			'memberStudents' => $memberStudents,

		));
	}
	public function member_student_detail($id = null, $student_id = null)
	{

		$memberStudents = $this->MemberStudent->find('first', array(
			'conditions' => array(
				'MemberStudent.student_id' => $student_id,

			),
			'order' => array('MemberStudent.id'),
		));

		//ส่วนที่แก้ไข
		$majordegrees = $this->Majordegree->find('first', array(
			'conditions' => array(
				'Majordegree.major_id' => $memberStudents['MemberStudent']['major_id'],
				'Majordegree.degree_id' => 1,

			),
			'fields' => array('Majordegree.major_id', 'Majordegree.major_name', 'Majordegree.organize_id'),
			'order' => array('Majordegree.id'),
		));
		$Organizes = $this->Organize->find('first', array(
			'conditions' => array(
				'Organize.id' => $majordegrees['Majordegree']['organize_id'],
			),


		));


		//*****************************StudentCreativeDocument***************************** */	
		$output = '';
		$memberStudentDocuments = $this->MemberStudentDocument->find('all', array(
			'conditions' => array(
				'MemberStudentDocument.member_student_id' => $id,

			),

			'group' => array('MemberStudentDocument.created'),
			'order' => array('MemberStudentDocument.created' => 'ASC')
		));
		foreach ($memberStudentDocuments as $key) {
			$num = 0;
			$memberStudentDocuments = $this->MemberStudentDocument->find('all', array(
				'conditions' => array(
					'MemberStudentDocument.member_student_id' => $id,
					'MemberStudentDocument.created' => $key['MemberStudentDocument']['created'],
				),
				'limit' => 1,
				'order' => array('MemberStudentDocument.id' => 'DESC')
			));
			if ($num == 0) {
				$output .=
					'<tr>
						<td colspan="2"> วันที่ส่งเอกสาร :
							' . $key['MemberStudentDocument']['created'] . '
						</td>
						
					</tr>';

				$num++;
			} else {
				$output .=
					'';
			}
			$i = 0;
			// $folder = WWW_ROOT.'files'.DS.'students'.DS.'agricultural_expo'.DS.$Students['Student']['id'].DS;
			foreach ($memberStudentDocuments as $memberStudentDocument) {
				$i++;
				$output .=
					'<tr>
					<td width="10%" data-title="ลำดับ" align="center"> ลำดับที่ ' . $i . '</td>
					<td width="90%">
						<a href="/smart_academic/files/students/agricultural_expo/' . $student_id . DS . $memberStudentDocument['MemberStudentDocument']['name'] . '" class="btn btn-primary" target="_blank">
						' . $memberStudentDocument['MemberStudentDocument']['name'] . ' <span class="glyphicon glyphicon-cloud-download"></span>
						</a>
					</td>
					</tr>';
			}
		}


		$this->set(array(
			'student_id' => $student_id,
			'majordegrees' => $majordegrees,
			'Organizes' => $Organizes,
			'memberStudents' => $memberStudents,

			'output' => $output
		));
	}

	public function registerguest()
	{
		if ($this->request->data) {

			$data = array();

			$i = 1;
			while ($i <= 10) {

				if ($this->request->data['Guest']['fname' . $i] != null) {

					$data2 = array(
						'organize' => $this->request->data['Guest']['organize'],
						'prefix' =>  $this->request->data['Guest']['prefix' . $i],
						'fname' => $this->request->data['Guest']['fname' . $i],
						'lname' => $this->request->data['Guest']['lname' . $i],
						'position' => $this->request->data['Guest']['position' . $i],
						'tel' => $this->request->data['Guest']['tel' . $i],
						'remark' => $this->request->data['Guest']['remark' . $i]
					);
					array_push($data, $data2);
				}

				$i++;
			}
			//debug($data);
			$this->Guest->saveMany($data);
			$this->Session->setFlash('ดำเนินการลงทะเบียนเรียบร้อยแล้ว');
			$this->Session->write('alertType', 'success');
			$this->redirect(array('action' => 'registerguest'));
		}

		$prefixes = $this->Prefix->find('list');


		// $listDateguides = $this->Dateguide->find('list');
		// $prefixes = $this->Prefix->find('list', array(
		// 	'conditions' => array(
		// 		// 'and' => array(
		// 		// 	array('Prefix.level <=' => 2),
		// 		// 	array('Prefix.level >=' => 1),
		// 		// 	),
		// 		'level' => 1
		// 	),
		// 	'order' => array('id' => 'ASC')
		// ));

		// // debug($prefixes);

		// $outputstudent = '';
		// foreach ($prefixes as $key => $value) {
		// 	$outputstudent .= '
		// 		<option value="' . $key . '">' . $value . '</option>
		// 	';
		// }

		// $sources = $this->Source->find('all');
		// $this->set(array(
		// 	'prefixes' => $prefixes,
		// 	'listDateguides' => $listDateguides,
		// 	'outputstudent' => $outputstudent,
		// ));
	}


	public function guestchecklist($guestId = null)
	{
		$UserName = $this->Session->read('MisEmployee');
		$this->set(array('UserName' => $UserName));

		$admin = $this->Session->read('admin');

		$organizes = $this->Guest->find('all', array(
			'fields' => array('Guest.organize'),
			'order' => array('Guest.id'),
			'group' => array('Guest.organize')
		));

		$guestlists = $this->Guest->find('all', array(
			'order' => array('Guest.id')
		));

		$this->set(array(
			// 'citizenId' => $citizenId,
			'UserName' => $UserName,
			'admin' => $admin,
			'organizes' => $organizes,
			'guestlists' => $guestlists,
		));

		if($this->request->data){
			 
			$check = $this->Guest->find('first',array(
				'conditions' => array(
					'Guest.id' => $this->request->data['Guest']['id']
				),
			));

			$this->Guest->save($this->request->data);

			 

			$this->Session->write('alertType','success');
			$this->Session->setFlash('แก้ไขข้อมูล '. $check['Guest']['fname'] . ' ' . $check['Guest']['lname'] .' เรียบร้อยแล้ว');
			$this->redirect(array('action' => 'guestchecklist'));
		}

		if ($guestId != null) {
			$chkSt = $this->Guest->find('first', array(
				'conditions' => array(
					'Guest.id' => $guestId
				),
			));

			// debug($chkst);
			// debug($chkst[Guest][status]);

			if ($chkSt['Guest']['status'] == 0) {
				$newSt = 1;

				$dateTime = new DateTime();
				$currentDateTime = $dateTime->format("Y-m-d H:i:s");

				$ChangeSt = $this->Guest->query("update kasetfairstudent.guests 
				set guests.status='" . $newSt . "', guests.tstamp='" . $currentDateTime . "' where guests.id='" . $guestId . "' ");
			} else {
				$newSt = 0;

				$ChangeSt = $this->Guest->query("update kasetfairstudent.guests 
				set guests.status='" . $newSt . "', guests.tstamp=null where guests.id='" . $guestId . "' ");
			}



			$this->set(array('ChangeSt' => $ChangeSt));
			$this->redirect(array('action' => 'guestchecklist'));
		}


		// 	$admin = $this->Session->read('admin');
		// 	if (isset($admin)) {
		// 		$statuses = $this->Status->find('list');
		// 		$this->set(array('statuses' => $statuses));

		// 		if ($this->request->data) {
		// 			if ($this->Member->save($this->request->data)) {
		// 				$this->redirect(array('action' => 'checklist', $projectId));
		// 			}
		// 		}
		// 	}

		// 	$this->set(array(
		// 		'project' => $project,
		// 		// 'members' => $members,
		// 		'citizenId' => $citizenId,
		// 		'admin' => $admin,
		// 		// 'output' => $output,
		// 		'schools' => $schools,
		// 		'students' => $students,
		// 	));
		// } else {
		// 	$this->redirect(array('action' => 'index'));
		// }
	}
}
