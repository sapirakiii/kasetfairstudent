<?php
set_time_limit(30000);
class PracticalsController extends AppController {
    private	$title_for_layout = 'Practical System v 1.0, Faculty of Agriculture , Chiangmai University';
	private	$keywords = 'Education, Registerpollrmation, Faculty of Agriculture, Chiangmai University';
	private	$keywordsTh = 'ระบบการฝึกงาน 1 กระบวนวิชา 400190 คณะเกษตรศาสตร์ มช.';
	private	$description = 'ระบบการฝึกงาน 1 กระบวนวิชา 400190 คณะเกษตรศาสตร์ มช.';
	
	public $layout = 'practicals';
	public $opendate = '2021-01-20 23:59:00';
	public $currentdateemp = '2022-06-03 00:00:00';
	public $currentdatestu = '2022-06-03 00:00:00';
	public $day1 = '2022-06-06';
	public $day2 = '2022-06-07';
	public $day3 = '2022-06-08';
	public $day4 = '2022-06-09';
	public $day5 = '2022-06-10';
	public $day6 = '2022-06-11';
	public $day7 = '2022-06-12';
	public $day8 = '2022-06-13';
	public $day9 = '2022-06-14';
	public $day10 = '2022-06-15';
	public $day11 = '2022-06-16';
	public $day12 = '2022-06-17';

	public $day13 = '2022-06-12';
	public $day14 = '2022-06-12';
	public $staff = 63;
	
	public $uses = array('Student','Major','Degree','Employee','Yearterm','Practical','Adminpractical','PracticalProject','PracticalReceive','PracticalStudent',
						 'Studentterm','PracticalTerm','MisEmployee','MisPosition','Domitory');		
	
	//แก้ที่ index ,list_pratical_all , loginstudent
	
	private $urls = '/smart_academic'; 
	private $studentId;
	public function DateThai($strDate){
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//, $strHour:$strMinute
	}
	public function beforeFilter() {
		$this->set('title_for_layout', $this->title_for_layout);
		$this->set('keywords', $this->keywords);
		$this->set('keywordsTh', $this->keywordsTh);
		$this->set('description', $this->description);
		$this->set('opendate', $this->opendate);
		$this->set('staff', $this->staff);
		$date = date("Y-m-d");
		$time = date("H:i:s");	
		$UserName = $this->Session->read('MisEmployee');
		$misPositions = $this->MisPosition->find('first' , array(
			'conditions' => array(
				'MisPosition.id' => $UserName['MisEmployee']['mis_position_id'],	
			),				
			'order' => array('MisPosition.id' => 'DESC')
		));
		$this->set(array(
			'misPositions' => $misPositions, 
		));
		$lastYearTerms = $this->Yearterm->find('first' , array(
			'conditions' => array(					
				'Yearterm.level' => 1
			),
			'order' => array('Yearterm.id' => 'desc'),
		));
		$this->Session->write('lastYearTerms', $lastYearTerms);
		//#####################################################################
		$this->Session->write('currentdateemp', $this->currentdateemp);
		$this->Session->write('currentdatestu', $this->currentdatestu);
		$this->Session->write('day1', $this->day1);
		$this->Session->write('day2', $this->day2);
		$this->Session->write('day3', $this->day3);
		$this->Session->write('day4', $this->day4);
		$this->Session->write('day5', $this->day5);
		$this->Session->write('day6', $this->day6);
		$this->Session->write('day7', $this->day7);
		$this->Session->write('day8', $this->day8);
		$this->Session->write('day9', $this->day9);
		$this->Session->write('day10', $this->day10);
		$this->Session->write('day11', $this->day11);
		$this->Session->write('day12', $this->day12);
		$this->Session->write('day13', $this->day13);
		$this->Session->write('day14', $this->day14);
	
		$Students = $this->Session->read('Students');
		
		$this->studentId = $Students['Student']['id'];
		$this->set(array('Students' => $Students));

		if(isset($this->studentId)){
			$student = $this->PracticalStudent->find('first', array(
						'conditions' => array('PracticalStudent.student_id' => $this->studentId
						)
					));
			$practicals = $this->Practical->find('first', array(
				'conditions' => array(
					'Practical.student_id' => $Students['Student']['id'], 
					// 'Practical.year' => $yearterms['Yearterm']['year']			
				),
			));	
			$this->set(array(
				'student' => $student,
				'practicals' => $practicals
			));
		}		
		//debug($student);
		
		$this->set(array(		
			'studentId' => $this->studentId,
			'date' => $date, 
			'time' => $time, 
		 
		));
		
		
	}
	
	public $paginate = array(
        'limit' => 30,
    );

	public function loginstudent() {
		date_default_timezone_set('Asia/Bangkok');
		$date = date("Y-m-d");
		$time = date("H:i:s");
		
		 $students = $this->Session->read('students'); // from main session
		 $yearterms = $this->Session->read('lastYearTerms');
		 $Students = $this->Student->find('first', array(
			'conditions' => array(
				'Student.id' => $students['Student']['id'], 
				 
				)
		));			
		$practicals = $this->Practical->find('first', array(
			'conditions' => array(
				'Practical.student_id' => $Students['Student']['id'], 
				'Practical.year' => $yearterms['Yearterm']['year']			
			),
		));	
		if (count($practicals) > 0) {
			if ($Students['Student']['id'] != $practicals['Practical']['student_id']) {
				// if (substr($Students['Student']['id'],0,2) == $this->staff ) { 											
				 
				$this->Session->write('Students', $Students);
				$this->Session->write('id',true);				
				
				
				$this->Session->setFlash('ยินดีต้อนรับ '.$Students['Student']['student_title'].''.$Students['Student']['student_firstname'].' '.$Students['Student']['student_surname'].' เข้าสู่ระบบเรียบร้อยแล้ว');
				$this->Session->write('alertType','success');
				$this->redirect(array('controller' => 'practicals','action' => 'project5',$this->day1));				
				 
			}else {
				//**************************Practical********************************* */
				$practicals = $this->Practical->find('first', array(
						'conditions' => array(
							'Practical.student_id' => $students['Student']['id'], 
							'year' => $yearterms['Yearterm']['year']			
						),
						'order' => array('Practical.id' => 'desc'),
					));	
					
					if (count($practicals) > 0) {	
						$Students = $this->Student->find('first', array(
							'conditions' => array(
								'Student.id' => $students['Student']['id'], 
								)
						));				
						if ($Students['Student']['degree_id'] == 1 ) {
																		
								$this->Session->write('Students', $Students);
								$this->Session->write('id',true);	
								$this->redirect(array('controller'=> 'practicals', 'action' => 'index'));			
							
						}else {
							$this->Session->setFlash('ไม่มีสิทธิ์เข้าถึงส่วนนี้');
							$this->Session->write('alertType','danger');
							$this->redirect(array('controller' => 'student_mains','action' => 'dashboard'));
						}
						
					}else{
						$this->Session->setFlash('ขออภัย ไม่มีสิทธิ์เข้าถึงส่วนนี้ สำหรับนักศึกษาที่มีสิทธิ์ลงทะเบียนการฝึกงาน 1 เท่านั้น!!!');
						$this->Session->write('alertType','danger');
						$this->redirect(array('controller' => 'student_mains','action' => 'dashboard'));
					}

			}
		}else{
			if (substr($Students['Student']['id'],0,2) == $this->staff ) {  
				$this->Session->write('Students', $Students);
				$this->Session->write('id',true);				
				
				
				$this->Session->setFlash('ยินดีต้อนรับ '.$Students['Student']['student_title'].''.$Students['Student']['student_firstname'].' '.$Students['Student']['student_surname'].' เข้าสู่ระบบเรียบร้อยแล้ว');
				$this->Session->write('alertType','success');
				$this->redirect(array('controller' => 'practicals','action' => 'project5',$this->day1));				
				 
			}else {
				$this->Session->setFlash('ขออภัย สำหรับนักศึกษาชั้นปีที่ 1 และนักศึกษาตกค้าง ที่มีสิทธิ์เข้ารับการฝึกงาน 1 กระบวนวิชา 400190 เท่านั้น !!!');
				$this->Session->write('alertType','danger');
				$this->redirect(array('controller' => 'student_mains','action' => 'dashboard'));

			}
		} 

		
	}
	public function login() {		
		$this->layout = 'login';
				
		$UserName = $this->Session->read('MisEmployee');		
		$admins = $this->Session->read('adminPracticals');
		$id = $this->Session->read('id');
		
		if(isset($UserName)){			
			$method = 'get';		
		}else {
			$method = 'post';		
		}

		if($this->request->is($method)){		
			if(isset($UserName)){
			
			}else {
				$UserName = $this->Employee->find('first', array(
					'conditions' => array(
						'UserName' => $this->request->data['UserName'],
						'Password' => $this->request->data['Password'])
				));		
				
			}
		
			if (count($UserName) > 0) {

				$admins = $this->Adminpractical->find('first', array(
								'conditions' => array('mis_employee_id' => $UserName['MisEmployee']['id']
					)
				));
				// $lastMisEmployees = $this->MisEmployee->find('first', array(
				// 	'conditions' => array(
				// 		'MisEmployee.id_card' => $UserName['Employee']['idx'],
				// 	)
				// ));
				//*********************1********************* */
				// $list_adminpracticals = $this->Adminpractical->find('all',array(	
				// 	'conditions' => array(
				// 		'Adminpractical.mis_employee_id' => $UserName['MisEmployee']['id'],
				// 	),		
					 
				// 	'order' => array('Adminpractical.id' => 'asc')
					
				// ));
				// $data = array();
				// $j = 0;
				// foreach ($list_adminpracticals as $key) {
				// 	$j ++;
					
				// 	$data2 = array(
				// 		'id' => $key['Adminpractical']['id'],
				// 		'mis_mis_employee_id' => $lastMisEmployees['MisEmployee']['id'], 
				// 	);
					 
					
				// 	array_push($data,$data2);
				// }
				// $this->Adminpractical->saveMany($data);
										
				if ($admins['Adminpractical']['employee_status_id'] == 1) {
					$this->Session->write('UserName', $UserName);				
					$this->Session->write('adminPracticals', $admins);
					$this->Session->write('id',true);
					$this->redirect(array('action' => 'index'));				
				};
					
				}else {
					$this->Session->write('UserName', $UserName);				
					$this->Session->write('adminPracticals', $admins);
					$this->Session->write('id',true);
					$this->redirect(array('action' => 'Dashboard'));
					$this->Session->setFlash('ทำการล๊อคอิน');
				}
		}else{
			$this->Session->setFlash('ชื่อผู้ดูแลระบบ หรือ รหัสผ่านผู้ดูแลระบบ ไม่ถูกต้อง!!!');
		}

	}
	public function logout() {
		$this->Session->delete('students'); // from main session
		$this->Session->delete('UserName');
		$this->Session->delete('id');
		$this->Session->delete('Students');
		$this->Session->delete('admins');
		$this->Session->delete('cmuusers');
		$this->Session->write('alertType','success');
		$this->Session->setFlash('Logout Complete');
		$this->redirect('https://www.agri.cmu.ac.th/2017/webs/index/th/1');
	}
	
	// =========================กำหนดยืนยันฝึกงาน =====================================
	// =======================================================================================================
	public function index(){
		 
	}
	public function poll_practical(){
		 
	}
	public function add_practical(){
		//สลับฝึกงาน ที่นี่
		$Students = $this->Session->read('Students');	
		$yearterms = $this->Session->read('lastYearTerms');
		if(isset($Students)){
			$this->set('Students', $Students);

			 
			 
		}
		else{
			if($this->action != 'login')
				$this->Session->setFlash('กรุณาเข้าสู่ระบบเพื่อยืนยันตัวตนก่อนการใช้งาน');
				$this->Session->write('alertType','danger');
				$this->redirect(array('controller' => 'student_mains','action' => 'login'));
		}
		 	
			if ($this->request->data) {
				//*************Practical**************** */
				$listgoodcerts = $this->Practical->find('first' ,array(
					'conditions' => array(
						'Practical.student_id' => $Students['Student']['id'],	
						'Practical.year' => $yearterms['Yearterm']['year'],												
					),						
				));
				//debug($listgoodcerts);
				date_default_timezone_set('Asia/Bangkok');

				$status = $this->request->data['Practical']['student_mobile'];
				$status1 = 2;
				$data = array(
				   'id' => $listgoodcerts['Practical']['id'],
				   'student_mobile' => $this->request->data['Practical']['student_mobile'],
				   'domitory_id' => $this->request->data['Practical']['domitory_id'],
				   'food_id' => $this->request->data['Practical']['food_id'],
				   'history_info' => $this->request->data['Practical']['history_info'],
				   'show_id' => $status1,
				);
				$this->Practical->save($data);
				// *************Save student**************** */
								
				$datastu = array(
					'id' => $Students['Student']['id'],
					'student_mobile' => $this->request->data['Practical']['student_mobile'],
				);
				$this->Student->save($datastu);
				
				$this->Session->setFlash('<i class="glyphicon glyphicon-ok-sign"></i> ยืนยันเข้ารับฝึกงานเรียบร้อยแล้ว');
				$this->Session->write('alertType','success');	
				$this->redirect(array('action' => 'index'));		
		
			}
				
	
		
		// $this->request->data = $Students;
		

		//*************Practical**************** */	
		$lastcerts = $this->Practical->find('first' ,array(
						'conditions' => array(
							'student_id' => $Students['Student']['id'],
							'Practical.year' => $yearterms['Yearterm']['year'],
						),					
						'order' => array('Practical.id' => 'desc'),
				));	

		$listDomitories = $this->Domitory->find('list',array(
			'conditions' => array(					
										
			), 					
		));
		
		$this->set(array(
			'yearterms' => $yearterms,
			'lastcerts' => $lastcerts,
			'listDomitories' => $listDomitories
		
		));
	}
	public function add_payment() {		
		$Students = $this->Session->read('Students');	
		$yearterms = $this->Session->read('lastYearTerms');
		if(isset($Students)){
			$this->set('Students', $Students);

			 
				 
		}
		else{
			if($this->action != 'login')
				$this->Session->setFlash('กรุณาเข้าสู่ระบบเพื่อยืนยันตัวตนก่อนการใช้งาน');
				$this->Session->write('alertType','danger');
				$this->redirect(array('controller' => 'student_mains','action' => 'login'));
		}
		$profiles = $this->Student->find('first', array(
			'conditions' => array(
				'Student.id' => $Students['Student']['id'],	
					
			)
		));
		
		try{
			if($this->request->data){
				date_default_timezone_set('Asia/Bangkok');
				// $id = $this->Practical->find('first',array(
				// 	'conditions' => array(
				// 		'student_id' => $this->request->data['Practical']['student_id'],			
				// 		),
				// 'fields' => array('id'),
				// 'group' => array('id')	
				// ));	
				// if ($id == null) {
					$listgoodcerts = $this->Practical->find('first' ,array(
						'conditions' => array(
							'Practical.student_id' => $Students['Student']['id'],	
							'year' => $yearterms['Yearterm']['year'],												
						),						
					));
						$files = $this->request->data['Practical']['files'];				
						$fileType = pathinfo($files['name'],PATHINFO_EXTENSION);
						////////////////// cheack img upload  ///////////////////
							
							if ($fileType != null) {
			
								if($fileType != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif" or "application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document" or "application/x-rar-compressed" or "application/octet-stream" or "application/zip" or "application/octet-stream" or "application/vnd.ms-powerpoint" or "application/vnd.openxmlformats-officedocument.presentationml.presentation" or "application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")){
			
									$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
									$this->Session->write('alertType','danger');							
									$this->redirect('add_payment');
								}
							}else{
			
								$this->Session->setFlash('คุณไม่ได้อัปโหลดไฟล์เอกสารแนบประกอบ');
								$this->Session->write('alertType','danger');
								$this->redirect('add_payment');
							}
						
						//$this->Practical->save($this->request->data['Practical']);
						// $this->Student->save($this->request->data['Student']);
			
						/////////////// files upload Document /////////////////
						if($this->request->data['Practical']['files']['name']){
			
							
							$i = date("m");
							$fileType = pathinfo($files['name'],PATHINFO_EXTENSION);
						
			
								// $this->request->data['Practical']['file'] = $this->request->data['Practical']['files']['name'];
								$filethai = iconv("UTF-8", "TIS-620",  $this->request->data['Practical']['files']['name']);
								
				
								$folder = WWW_ROOT.'files'.DS.'students'.DS.'practical'.DS.$Students['Student']['id'].DS;
			
								if(!file_exists($folder))
									mkdir($folder);
			
								move_uploaded_file($this->request->data['Practical']['files']['tmp_name'],$folder.DS.$filethai);
		
								$data = array(
									'id' => $listgoodcerts['Practical']['id'],
									'file' => $this->request->data['Practical']['files']['name'],
									
								 );
								 $this->Practical->save($data);
							
						// $this->Practical->save($this->request->data);
							
							
						
						
					}
		
					$this->Session->write('alertType','success');
					$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
					$this->redirect(array('action' => 'list_practical_all'));
				// }else {
				// 	$this->Session->setFlash('<i class="glyphicon glyphicon-remove-sign"></i> รหัสนักศึกษาซ้ำ!!!');
				// 	$this->Session->write('alertType','danger');
				// 	$this->redirect(array('action' => 'add_pre_eng'));
					
				// }


			}
		}catch(Exception $e){
			$this->Session->setFlash('<i class="glyphicon glyphicon-remove-sign"></i> ขออภัย เกิดเหตุขัดข้องของการส่งอีเมล์ไปยังบุคลากร กรุณาเช็คอีเมล์บุคลากรให้ถูกต้อง');
			$this->Session->write('alertType','danger');		
			$this->redirect(array('action' => 'list_practical_all'));	
			//echo 'window.location.href= https://stackoverflow.com/search?='+ $e->message();
		}
	
			
	}
	public function list_practical_all($show_id = null){	
		$yearterms = $this->Session->read('lastYearTerms');
		$admins = $this->Session->read('adminPracticals');		
		$this->set(array(
			'yearterms' => $yearterms,
		    'admins' => $admins
		));	
			//*************Practical**************** */	
			$countgoodcertsall = $this->Practical->find('count',array(							
				'conditions' => array(				
					'Practical.year' => $yearterms['Yearterm']['year'],
					// 'show_id' => 2				
				),
				'order' => array('Practical.student_id' => 'ASC')
			));
			$countgoodcerts2 = $this->Practical->find('count',array(							
				'conditions' => array(				
					'Practical.year' => $yearterms['Yearterm']['year'],
					'show_id' => 2				
				),
				'order' => array('Practical.student_id' => 'ASC')
			));
			$countgoodcerts1 = $this->Practical->find('count',array(							
				'conditions' => array(				
					'Practical.year' => $yearterms['Yearterm']['year'],
					'show_id' => 1				
				),
				'order' => array('Practical.student_id' => 'ASC')
			));
			$this->set(array(	
				'countgoodcertsall' => $countgoodcertsall,
				'countgoodcerts2' => $countgoodcerts2,
				'countgoodcerts1' => $countgoodcerts1,
			));


			if($show_id != null){
				$listgoodcerts = $this->Practical->find('all',array(							
					'conditions' => array(				
						'Practical.year' => $yearterms['Yearterm']['year'],
						'show_id' => $show_id						
					),
					'order' => array('Practical.student_id' => 'ASC')
				));
			}else {
				# code...
				$listgoodcerts = $this->Practical->find('all',array(							
					'conditions' => array(				
						'Practical.year' => $yearterms['Yearterm']['year'],				
					),
					'order' => array('Practical.student_id' => 'ASC')
				));
			}
		$i=0;
		$output = '';
	

	
		foreach ($listgoodcerts as $listgoodcert) {
			$infostudents = $this->Student->findById($listgoodcert['Practical']['student_id']);	
			$major  = $this->Major->findById($infostudents['Student']['major_id']);
			$i++;	
				$output .='
					<tr>
						<td>'.$i.'</td>';					
						
				$output .='<td>'.$listgoodcert['Practical']['student_id'].'</td>';
				if ($admins != null) {
					$output .='<td> <a target="_blank" href="http://www.agri.cmu.ac.th/smart_academic/files/students/profiles/'.$infostudents['Student']['photo'].'"> '.$infostudents['Student']['student_title'].''.$infostudents['Student']['student_firstname'].' '.$infostudents['Student']['student_surname'].'</a></td>';

				}else {
					$output .='<td>  '.$infostudents['Student']['student_title'].''.$infostudents['Student']['student_firstname'].' '.$infostudents['Student']['student_surname'].'</td>';

				}						
				$output .='<td>'.$major['Major']['major_name'].'</td>';
						if ($admins != null) {
							$output .='<td>'.$infostudents['Student']['student_mobile'].'</td>';
							$output .='<td>'.$listgoodcert['Domitory']['name'].'</td>';
							$output .='<td>'.$listgoodcert['Food']['name'].'</td>';
							$output .='<td>'.$listgoodcert['Practical']['history_info'].'</td>';
						} else {
							
						}
							
				
						if ($listgoodcert['Practical']['show_id'] == 1) {
							$output .='<td><span class="label label-danger">	<span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> ยังไม่ได้ยืนยัน</span></td>';	
						}else {							
							$output .='<td><span class="label label-success"> <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"> ยืนยันเรียบร้อย</span></td>';
						}

						if ($listgoodcert['Practical']['file'] == null) {
							$output .='<td><span class="label label-danger">	<span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> ยังไม่ได้ชำระเงิน</span></td>';	
						}else {							
							$output .='<td><span class="label label-success"> <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"> ชำระเงินเรียบร้อย</span></td>';
						
						}
				if ($admins != null) {
					if ($listgoodcert['Practical']['file'] == null) {
						$output .='<td> </td>';
					
					}else {	
						$output .='<td> <a class="btn btn-primary" href="http://www.agri.cmu.ac.th/smart_academic/files/students/practical/'.$listgoodcert['Practical']['student_id'].'/'.$listgoodcert['Practical']['file'].'" target="_blank"> หลักฐานการชำระเงิน </a></td>';
					
					}
					$output .='<td> <a class="btn btn-danger" href="http://www.agri.cmu.ac.th/smart_academic/practicals/remove_topic/'.$listgoodcert['Practical']['id'].'/'.$listgoodcert['Practical']['student_id'].'"> ลบ </a></td>';
						
				}		
				$output .='</tr>';
					
					
		}
		
		
		$this->set(array(	
			'output' => $output,
			
		));
		
			
	}
	public function add_topic(){
		$yearterms = $this->Session->read('lastYearTerms');
        if($this->request->is('post')){
		 
            $this->Practical->save($this->request->data);
            $this->Session->setFlash('เพิ่มเรียบร้อยแล้ว');
            $this->redirect('add_topic');
        }
		$this->set(array(
			'yearterms' => $yearterms,
		    
		));	 
	}
	public function remove_topic($id = null,$student_id=null){

		
		$this->Practical->delete($id);
	
	


	$this->Session->write('alertType','danger');
	$this->Session->setFlash('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>ลบรหัส '.$student_id.' เรียบร้อยแล้ว');
	$this->redirect(array('action' => 'list_practical_all'));
		

	}
	
	public function project2() {
		$projects = $this->PracticalProject->find('all', array(
			'conditions' => array(
				
				'level' => 1
			),
			'order' => array('PracticalProject.datestart,PracticalProject.order' => 'ASC')
		));	
		
		
		$outputProjects = '';
		$i = 1;
		foreach ($projects as $project){
			// $a = $project['PracticalProject']['id'];
			$a = '';
			// $terms = array(1,3,2);
			$terms = array(1,2);
			$rowspan = '';
				
			// if($project['PracticalProject']['id'] == 1 || $project['PracticalProject']['id'] == 2
				// 	|| $project['PracticalProject']['id'] == 4 || $project['PracticalProject']['id'] == 42
				// 	|| $project['PracticalProject']['id'] == 5 || $project['PracticalProject']['id'] == 6
				// 	|| $project['PracticalProject']['id'] == 7 || $project['PracticalProject']['id'] == 10
				// 	|| $project['PracticalProject']['id'] == 11 || $project['PracticalProject']['id'] == 12
				// 	|| $project['PracticalProject']['id'] == 15 || $project['PracticalProject']['id'] == 16
				// 	|| $project['PracticalProject']['id'] == 17 || $project['PracticalProject']['id'] == 21
				// 	|| $project['PracticalProject']['id'] == 22 || $project['PracticalProject']['id'] == 24
				// 	|| $project['PracticalProject']['id'] == 25 || $project['PracticalProject']['id'] == 26
				// 	|| $project['PracticalProject']['id'] == 31 || $project['PracticalProject']['id'] == 33
				// 	|| $project['PracticalProject']['id'] == 34 || $project['PracticalProject']['id'] == 35
				// 	|| $project['PracticalProject']['id'] == 40 || $project['PracticalProject']['id'] == 43
				// 	|| $project['PracticalProject']['id'] == 44 || $project['PracticalProject']['id'] == 49
				// 	|| $project['PracticalProject']['id'] == 51 || $project['PracticalProject']['id'] == 52
				// 	|| $project['PracticalProject']['id'] == 53 || $project['PracticalProject']['id'] == 59
				// 	|| $project['PracticalProject']['id'] == 60 || $project['PracticalProject']['id'] == 61
				// 	|| $project['PracticalProject']['id'] == 62 || $project['PracticalProject']['id'] == 63
				// 	|| $project['PracticalProject']['id'] == 66 
				// 			){
				// 			// $rowspan = 'rowspan = "3"';	
				// 			$rowspan = 'rowspan = "1"';	
							
				// 			$outputTerms = '';
				// 			$outputTerms2 = '';
				// 			foreach ($terms as $term){
				// 				$receives = $this->PracticalReceive->find('all', array(
				// 					'conditions' => array('PracticalReceive.practical_project_id' => $project['PracticalProject']['id'],
				// 										// 'PracticalReceive.practical_term_id' => $term,													
															
				// 										)
				// 				));				
				// 				$amount = 0;
				// 				$amount17 = 0;
				// 				$amount1 = 0;
				// 				$amount3 = 0;
				// 				$duration = '';
				// 				foreach ($receives as $receive){					
				// 					$amount += $receive['PracticalReceive']['amount'];
				// 					$amount17 += $receive['PracticalReceive']['amount_major_id_17'];
				// 					$amount1 += $receive['PracticalReceive']['amount_major_id_1'];
				// 					$amount3 += $receive['PracticalReceive']['amount_major_id_3'];
				// 					$duration = $receive['PracticalReceive']['duration'];
				// 				}	
							
				// 				if($amount == 0){ $amount = '-'; }
				// 				if($amount17 == 0 ){ $amount17 = '-'; }
				// 				if($amount1 == 0 ){	$amount1 = '-'; }
				// 				if($amount3 == 0 ){	$amount3 = '-'; }
								
								
				// 			}
				// 				if ($amount == 0) {
				// 					$outputTerms2 .= 
				// 					'<td class="text-center" colspan="2">'.$amount.' </td>
				// 					';
				// 				}else {
				// 					$outputTerms2 .= 
				// 					'<td class="text-center" colspan="2">'.$amount.' </td>
				// 					';
				// 				}	
				// 				// $outputTerms2 .= 
				// 				// '<td class="text-center" >'.$amount3.'</td>
				// 				// ';
							
									
				// 			$outputProjects .= 
				// 				'<tr> 
				// 					<td class="text-center" '.$rowspan.'>'.$i.'</td>
				// 					<td '.$rowspan.'>'.$project['PracticalProject']['datestart'].'</td>
				// 					<td '.$rowspan.'>'.$project['PracticalProject']['name'].' '.$a.'</td> 
				// 					<td class="text-center"><b>'.$amount.'</b></td>
				// 					'.$outputTerms2.'
				// 				</tr>';
										
							
										
				// 			$outputTerms2 = '';
				// 			$outputTerms .= 
				// 						'<tr> 
				// 							'.$outputTerms2.'
				// 						</tr> ';
										
										
				// 			$outputProjects .= $outputTerms;
				// }else{
				
				$outputTerms = '';
				foreach ($terms as $term){
					$receives = $this->PracticalReceive->find('all', array(
						'conditions' => array(
							'PracticalReceive.practical_project_id' => $project['PracticalProject']['id'],
							// 'PracticalReceive.practical_term_id' => $term,							
							)
					));				
					
					$amount = 0;
					$amount17 = 0;
					$amount1 = 0;
					$amount3 = 0;
					$duration = '';
					$gender = '';
						foreach ($receives as $receive){					
							$amount += $receive['PracticalReceive']['amount'];
							$amount17 += $receive['PracticalReceive']['amount_major_id_17'];
							$amount1 += $receive['PracticalReceive']['amount_major_id_1'];
							$amount3 += $receive['PracticalReceive']['amount_major_id_3'];
							
						}	
						
					
						if($amount == 0){ $amount = 0; }
						if($amount17 == 0 ){	$amount17 = '-'; }
						if($amount1 == 0 ){	$amount1 = '-'; }
						if($amount3 == 0 ){	$amount3 = '-'; }
					
						
						
					}
					$outputTerms .= 
						'
						<td class="text-center">'.$amount17.'</td>
						<td class="text-center">'.$amount1.'</td>
					';
				
					if ($project['PracticalProject']['datestart'] == '2022-06-05') {
						$outputProjects .= 
						'<tr class="success"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-06') {
							$outputProjects .= 
							'<tr class="warning"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-07') {
							$outputProjects .= 
							'<tr class="danger"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-08') {
							$outputProjects .= 
							'<tr class="info"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-09') {
							$outputProjects .= 
							'<tr style="background-color: #FFEBFF;"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-10') {
							$outputProjects .= 
							'<tr style="background-color: #E5F9B5;" > ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-06') {
							$outputProjects .= 
							'<tr class="warning"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-12') {
							$outputProjects .= 
							'<tr style="background-color: #F9E7B5;"  > ';
					} else {
						# code...
					}
				
				

				$outputProjects .=  '<td class="text-center" '.$rowspan.'>'.$i.'</td>';
					
				if ($project['PracticalProject']['datestart'] !=null) {
					$outputProjects .=  '<td align="center">'.$this->DateThai($project['PracticalProject']['datestart']).'</td>';		
				} else {
					$outputProjects .=  '<td ></td>';		
				}
				
				$outputProjects .=  '<td '.$rowspan.'>'.$project['PracticalProject']['name'].' '.$a.'</td>  
						<td class="text-center"><b>'.($amount17+$amount1+$amount3).'</b></td>
							
						'.$outputTerms.' 
					</tr> ';
			//}
			
			$i++;
		}
		
		$this->set(array('outputProjects' => $outputProjects));
	}
	
	public function register2($datestart = null) {
		// $this->layout='';
		$this->set(array('datestart' => $datestart));
		$Students = $this->Session->read('Students');
		$this->set(array('Students' => $Students));
		$student_id = $Students['Student']['id'];
		if(!isset($Students) || $Students == null){
				$this->redirect(array('controller' => 'student_mains','action' => 'login'));
		}else {
			
		}
		date_default_timezone_set("Asia/Bangkok");

		
			$dateTimeNow = new DateTime(date("Y-m-d H:i:s"));	
			$dateTimeStart = new DateTime('2022-10-07 12:00:00');	
			$dateTimeEnd = new DateTime('2022-10-16 12:00:00');					
			
			$student = $this->PracticalStudent->find('first', array(
					'conditions' => array(
						'PracticalStudent.student_id' => $Students['Student']['id'],								
					)
				));
			
			if($this->request->data){
				$a = 0;
				//*********radio*********** */
				//debug($this->request->data['PracticalStudent']['practical_receive_id']);
					// if(isset($this->request->data['PracticalStudent']['practical_receive_id'])){
								
					// 	$receive = $this->PracticalReceive->find('first', array(
					// 			'conditions' => array(
					// 				'PracticalReceive.id' => $this->request->data['PracticalStudent']['practical_receive_id'],
									
					// 			)
					// 		));	
					// 	$lastprojects = $this->PracticalProject->find('first', array(
					// 			'conditions' => array(
					// 				'PracticalProject.id' => $receive['PracticalReceive']['practical_project_id'] ,
									
					// 			)
					// 		));
					// 	$amountReg = $this->PracticalStudent->find('count', array(
					// 				'conditions' => array(
					// 					'PracticalStudent.practical_receive_id' => $this->request->data['PracticalStudent']['practical_receive_id'],
										
					// 				)
					// 			));	
					// 	$checkduplicatereceives = $this->PracticalStudent->find('all', array(
					// 		'conditions' => array(
					// 			'PracticalStudent.practical_receive_id' => $this->request->data['PracticalStudent']['practical_receive_id'],
					// 			'PracticalStudent.student_id' => $Students['Student']['id'],												
					// 		)
					// 	));	
					// 	foreach ($checkduplicatereceives as $key) {
					// 		if ($key['PracticalStudent']['practical_receive_id'] == $this->request->data['PracticalStudent']['practical_receive_id']) {
					// 			$this->Session->setFlash('ขออภัย ได้มีรายชื่อลงทะเบียนฝ่ายนี้แล้ว กรุณาเลือกลงทะเบียนฝ่ายใหม่ !!!');
					// 			$this->Session->write('alertType','danger');
					// 			$this->redirect(array('action' => 'register2',$datestart));
					// 		} else {
								
					// 		}
							
					// 	}
					// 	//*********เช็ควันซ้ำ****** */
					// 		$checkduplicateStudents = $this->PracticalStudent->find('all', array(
					// 			'conditions' => array(								
					// 				'PracticalStudent.student_id' => $Students['Student']['id'],												
					// 			),
					// 			'order' => array('PracticalStudent.practical_project_id,PracticalStudent.practical_term_id' => 'ASC')
					// 		));	
					// 		// debug($checkduplicateStudents);
					// 		foreach ($checkduplicateStudents as $checkStudent) {							
					// 			$checkproject2s = $this->PracticalProject->find('all', array(
					// 				'conditions' => array(
					// 					'PracticalProject.id' => $checkStudent['PracticalStudent']['practical_project_id'],
																					
					// 				),
					// 				'order' => array('PracticalProject.id' => 'ASC')
					// 			));	
							
					// 			foreach ($checkproject2s as $checkproject2 ) {

					// 				if ($lastprojects['PracticalProject']['datestart'] == $checkproject2['PracticalProject']['datestart']) {									
										
					// 						$receive2s = $this->PracticalReceive->find('first', array(
					// 							'conditions' => array(
					// 								'PracticalReceive.practical_project_id' => $receive['PracticalReceive']['practical_project_id'],
															
					// 								)
					// 						));
										
										
											
					// 					// debug($checkStudent['PracticalStudent']['practical_term_id']);
					// 					// debug($receive2s['PracticalReceive']['practical_term_id']);
					// 					if ($checkStudent['PracticalStudent']['practical_term_id'] == $receive2s['PracticalReceive']['practical_term_id']) {
					// 						$this->Session->setFlash('ขออภัย ได้เลือกช่วงเวลานี้ไปแล้ว ไม่สามารถลงซ้ำได้  !!!');
					// 						$this->Session->write('alertType','danger');
					// 						$this->redirect(array('action' => 'register2',$datestart));
					// 					}

									
					// 				} 
					// 			}
					// 		}
					// 	//********* */
					// 		$data2 = array(								
					// 			'student_id' => $Students['Student']['id'],
					// 			'practical_receive_id' => $receive['PracticalReceive']['id'],
					// 			'practical_project_id' => $receive['PracticalReceive']['practical_project_id'],	
					// 			'practical_term_id' => $receive['PracticalReceive']['practical_term_id'],			
					// 		);
						
					// 		# code...
					// 		$this->PracticalStudent->save($data2);									
					// 		$this->Session->setFlash('รหัสนักศึกษา '.$Students['Student']['id'].' เลือก'.$receive['PracticalProject']['name'].' เรียบร้อยแล้ว
					// 		');
					// 		$this->Session->write('alertType','success');
					// 		$this->redirect(array('action' => 'register2',$datestart));
								
						
								
					// }
					// else{
					// 	$this->Session->setFlash('กรุณาเลือกโครงการ !!!');
					// }
				
				//debug($this->request->data);
				// ********** */
				//*********checkbox*********** */
					$checkSave = true;
					date_default_timezone_set('Asia/Bangkok');

					// foreach ($majorPolls as $key) {
					// 	//*************อัพเดต correct = 2 ทั้งหมด********** */				
					// 	$status = 2;
					// 	$data = array(
					// 	'id' => $key['Majorpoll']['id'],
					// 	'correct' => $status,
					
					// 	);
					// 	$this->Majorpoll->save($data);
					// }
					if($checkSave){
						$data = array();
						$datas = array();
						$a = 0;
						// debug($this->request->data['practical_receive_id']);
							foreach ($this->request->data['practical_receive_id'] as $sourceId){
								$receive = $this->PracticalReceive->find('first', array(
									'conditions' => array(
										'PracticalReceive.id' => $sourceId,
										
									)
								));	
								$lastprojects = $this->PracticalProject->find('first', array(
									'conditions' => array(
										'PracticalProject.id' => $receive['PracticalReceive']['practical_project_id'] ,
										
									)
								));
								//debug($lastprojects);
								
								//********************* */
								$amountReg = $this->PracticalStudent->find('count', array(
									'conditions' => array(
										'PracticalStudent.practical_receive_id' => $sourceId,
										
									)
								));	
								$checkduplicatereceives = $this->PracticalStudent->find('all', array(
									'conditions' => array(
										'PracticalStudent.practical_receive_id' => $sourceId,
										'PracticalStudent.student_id' => $Students['Student']['id'],												
									)
								));	
								foreach ($checkduplicatereceives as $key) {
									if ($key['PracticalStudent']['practical_receive_id'] == $sourceId) {
										$this->Session->setFlash('ขออภัย ได้เลือกลงทะเบียนฝ่ายนี้แล้ว กรุณาเลือกลงทะเบียนฝ่ายใหม่ !!!');
										$this->Session->write('alertType','danger');
										$this->redirect(array('action' => 'register2'));
									} else {
										
									}
									
								}
								
								//*************** */
									$checkduplicateStudents = $this->PracticalStudent->find('all', array(
										'conditions' => array(								
											'PracticalStudent.student_id' => $Students['Student']['id'],												
										),
										'order' => array('PracticalStudent.id' => 'DESC')
									));	
									foreach ($checkduplicateStudents as $checkStudent) {							
										$checkproject2s = $this->PracticalProject->find('all', array(
											'conditions' => array(
												'PracticalProject.id' => $checkStudent['PracticalStudent']['practical_project_id'],
																							
											),
											'order' => array('PracticalProject.id' => 'DESC')
										));	
										//debug($checkproject2s);
										foreach ($checkproject2s as $checkproject2 ) {
										
											if ($lastprojects['PracticalProject']['datestart'] == $checkproject2['PracticalProject']['datestart']) {
												$this->Session->setFlash('ขออภัย นักศึกษาได้ลงทะเบียนในฝ่ายอื่นแล้ว ไม่สามารถเลือกช่วงเวลาซ้ำกันในวันเดียวกัน  !!!');
												$this->Session->write('alertType','danger');
												$this->redirect(array('action' => 'register2'));
											} else {
												# code...
											}
										}
									}
								//********* */
								//ดักเลือกช่วงเช้า เหมือนกัน ช่วงบ่ายเหมือนกันในวันเดียว

								if($amountReg < $receive['PracticalReceive']['amount']){
									$a = 1;
								}else{
									$this->Session->setFlash('ขออภัย กรุณาเลือกลงทะเบียนฝ่ายใหม่ !!!');
									$this->Session->write('alertType','danger');
									$this->redirect(array('action' => 'register2'));
								}
								$data2 = array(								
									'student_id' => $Students['Student']['id'],
									'practical_receive_id' => $receive['PracticalReceive']['id'],
									'practical_project_id' => $receive['PracticalReceive']['practical_project_id'],	
									'practical_term_id' => $receive['PracticalReceive']['practical_term_id'],			
								);
								array_push($data,$data2);
								
								
							}	
							if ($a == 1) {
								$this->PracticalStudent->saveMany($data);
							}
							
							$this->Session->setFlash('รหัสนักศึกษา '.$Students['Student']['id'].' เลือกเรียบร้อยแล้ว  ');
							$this->Session->write('alertType','success');
							$this->redirect(array('action' => 'register2'));
					}
				//********** */	
					
			}else{
				$this->request->data = $this->PracticalStudent->find('first', array(
					'conditions' => array(
						'PracticalStudent.student_id' => $Students['Student']['id'])
				));
			
			}
			
					
					
				$projects = $this->PracticalProject->find('all', array(
					'conditions' => array(		
						'datestart' => $datestart,				
						'level' => 1
					),
					'order' => array('id' => 'ASC')
				));				
				$outputProjects = '';
				if($dateTimeNow > $dateTimeStart && $dateTimeNow < $dateTimeEnd){
					$i = 1;
					foreach ($projects as $project){
						$a = $project['PracticalProject']['id'];
						// $terms = array(1,3,2);
						$terms = array(1,2);
						$amoutes = array(1,2,3);
						$rowspan = '';	
						// if($project['PracticalProject']['id'] == 1 || $project['PracticalProject']['id'] == 2
						// 	|| $project['PracticalProject']['id'] == 4 || $project['PracticalProject']['id'] == 42
						// 	|| $project['PracticalProject']['id'] == 5 || $project['PracticalProject']['id'] == 6
						// 	|| $project['PracticalProject']['id'] == 7 || $project['PracticalProject']['id'] == 10
						// 	|| $project['PracticalProject']['id'] == 11 || $project['PracticalProject']['id'] == 12
						// 	|| $project['PracticalProject']['id'] == 15 || $project['PracticalProject']['id'] == 16
						// 	|| $project['PracticalProject']['id'] == 17 || $project['PracticalProject']['id'] == 21
						// 	|| $project['PracticalProject']['id'] == 22 || $project['PracticalProject']['id'] == 24
						// 	|| $project['PracticalProject']['id'] == 25 || $project['PracticalProject']['id'] == 26
						// 	|| $project['PracticalProject']['id'] == 31 || $project['PracticalProject']['id'] == 33
						// 	|| $project['PracticalProject']['id'] == 34 || $project['PracticalProject']['id'] == 35
						// 	|| $project['PracticalProject']['id'] == 40 || $project['PracticalProject']['id'] == 43
						// 	|| $project['PracticalProject']['id'] == 44 || $project['PracticalProject']['id'] == 49
						// 	|| $project['PracticalProject']['id'] == 51 || $project['PracticalProject']['id'] == 52
						// 	|| $project['PracticalProject']['id'] == 53 || $project['PracticalProject']['id'] == 59
						// 	|| $project['PracticalProject']['id'] == 60 || $project['PracticalProject']['id'] == 61
						// 	|| $project['PracticalProject']['id'] == 62 || $project['PracticalProject']['id'] == 63
						// 	|| $project['PracticalProject']['id'] == 66 
						// 		){
						// 	// $rowspan = 'rowspan = "3"';	
						// 	$rowspan = 'rowspan = "1"';	
								
						// 	$outputTerms = '';
						// 	$outputTerms2 = '';
						// 	foreach ($terms as $term){
						// 		$receives = $this->PracticalReceive->find('all', array(
						// 			'conditions' => array(
						// 				'PracticalReceive.practical_project_id' => $project['PracticalProject']['id'],
						// 				// 'PracticalReceive.practical_term_id' => $term,													
						// 			)
						// 		));				
								
						// 		$amount = 0;
						// 		$amount17 = 0;
						// 		$amount1 = 0;
						// 		$amount3 = 0;
						// 		$duration = '';
						// 		$receiveId = null;
						// 		foreach ($receives as $receive){					
						// 			$amount += $receive['PracticalReceive']['amount'];
						// 			$amount17 += $receive['PracticalReceive']['amount_major_id_17'];
						// 			$amount1 += $receive['PracticalReceive']['amount_major_id_1'];
						// 			$amount3 += $receive['PracticalReceive']['amount_major_id_3'];
						// 			$duration = $receive['PracticalReceive']['duration'];
						// 			$receiveId = $receive['PracticalReceive']['id'];
						// 		}
								
						// 		$amountReg = $this->PracticalStudent->find('count', array(
						// 			'conditions' => array(
						// 				'PracticalStudent.practical_receive_id' => $receiveId,										
						// 				)
						// 		));	
								
						// 		$amountRest = $amount - $amountReg;
						// 		$amountRestInput = '';
						// 			if($amountRest > 0){
						// 				$amountRestInput = '
						// 				<input type="checkbox" name="data[practical_receive_id]['.$receiveId.']" value="'.$receiveId.'"> '.$amountRest.'<br>';
						// 				// <input type="checkbox" name="data[correct_id][4365]"  value="4365" id="checkItem" data-error="กรุณากรอกข้อมูล" class="largerCheckbox2"/>
						// 			}
	
									
						// 	}
							
						// 	// if ($amount == 0) {
						// 	// 	$outputTerms .= 
						// 	// 	'<td class="text-center" colspan="2">'.$amountRestInput.' </td>
						// 	// 	';
						// 	// }else {
						// 	// 	$outputTerms .= 
						// 	// 	'<td class="text-center" colspan="2">'.$amountRestInput.' (ไม่จำกัดสาขา)</td>
						// 	// 	';
						// 	// }
						// 	if ($amount == 0) {
						// 		$outputTerms2 .= 
						// 		'<td class="text-center" colspan="2">'.$amountRestInput.' </td>
						// 		';
						// 	}else {
						// 		$outputTerms2 .= 
						// 		'<td class="text-center" colspan="2">'.$amountRestInput.' </td>
						// 		';
						// 	}	
						// 	// $outputTerms2 .= 
						// 	// '<td class="text-center" > -</td>
						// 	// ';	
						// 	$outputProjects .= 
						// 		'<tr> 
						// 			<td class="text-center" '.$rowspan.'>'.$i.'</td>
						// 			<td '.$rowspan.'>'.$project['PracticalProject']['datestart'].'</td>
						// 			<td '.$rowspan.'>'.$project['PracticalProject']['name'].' '.$a.'</td> 
						// 			<td class="text-center"><b>'.$amount.'</b></td> 
						// 			'.$outputTerms2.'
						// 		</tr>';
										
							
										
						// 	$outputTerms2 = '';
						// 	$outputTerms .= 
						// 				'<tr> 
						// 					'.$outputTerms2.'
						// 				</tr> ';
										
										
						// 	$outputProjects .= $outputTerms;
						// }else{
							
							$outputTerms = '';
							foreach ($terms as $term){
								$receiveId = array();
								$amount = 0;
								$amount17 = 0;
								$amount1 = 0;
								$amount3 = 0;
								$gender = '';
									$receives = $this->PracticalReceive->find('all', array(
										'conditions' => array(
											'PracticalReceive.practical_project_id' => $project['PracticalProject']['id'],
											'PracticalReceive.practical_term_id' => $term,											
																
										)
									));				
									$receive2s = $this->PracticalReceive->find('all', array(
										'conditions' => array(
											'PracticalReceive.practical_project_id' => $project['PracticalProject']['id'],
																					
																
										)
									));	
									foreach ($receive2s as $receive){				
									
										$amount17 += $receive['PracticalReceive']['amount_major_id_17'];
										$amount1 += $receive['PracticalReceive']['amount_major_id_1'];		
										$amount3 += $receive['PracticalReceive']['amount_major_id_3'];								
									
										
									}	
								
									foreach ($receives as $receive){					
										$amount += $receive['PracticalReceive']['amount'];																		
										$receiveId = $receive['PracticalReceive']['id'];
										
									}	
									
									$amountReg = $this->PracticalStudent->find('count', array(
										'conditions' => array(
											'PracticalStudent.practical_receive_id' => $receiveId,									
										
										)
									));	
									$amountRest17 = $amount - $amountReg;
									$amountRestInput17 = '';
									if($amountRest17 > 0){
										$amountRestInput17 = 
										// '<input type="radio" name="data[PracticalStudent][practical_receive_id]" value="'.$receiveId.'"> '.$amountRest17.'<br>';
										'<input type="radio" name="data[practical_receive_id]['.$receiveId.']" value="'.$receiveId.'"> '.$amountRest17.'<br>';
									
									}
									

									$outputTerms .= 
										'<td class="text-center">'.$amountRestInput17.'</td>';

									
									//debug($receiveId[0]);
									
										
								
								
							}
							
							
							$outputProjects .= 
								'<tr> 
									<td class="text-center" '.$rowspan.'>'.$i.'</td>';							
									if ($project['PracticalProject']['datestart'] !=null) {
										$outputProjects .=  '<td >'.$this->DateThai($project['PracticalProject']['datestart']).'</td>';		
									} else {
										$outputProjects .=  '<td ></td>';		
									}
							$outputProjects .= '<td '.$rowspan.'>'.$project['PracticalProject']['name'].' '.$a.'</td> 
									<td class="text-center"><b>'.($amount17+$amount1+$amount3).'</b></td>
									<td>
										<input type="radio" name="data[PracticalStudent][practical_receive_id]" value="'.$receiveId.'"> อยู่ได้ทั้งวัน
									</td>	
									'.$outputTerms.' 
								</tr> ';
							//}
							
							$i++;
					}
				}else {
					$this->Session->setFlash('รอวันเปิดระบบ !!!');
					
				}
				$this->set(array('outputProjects' => $outputProjects));

			
		
	}
	//************ */
	public function list_join_student() {
		$date = date("Y-m-d");
		$time = date("H:i:s");
	 
			
			$Students = $this->Session->read('Students');
			$this->set(array('Students' => $Students));
			$student_id = $Students['Student']['id'];
			if(!isset($Students) || $Students == null){
					$this->redirect('http://www.agri.cmu.ac.th/smart_academic/student_mains/login');
			}else {
				
			}
			
		 	
		$projects = $this->PracticalStudent->find('all', array(
			'conditions' => array(
				'student_id' => $Students['Student']['id'],
				
			),
			'order' => array('PracticalStudent.id' => 'ASC')
		));	
	
		$outputProjects = '';
		$i = 1;
		$total = 0;
		$totalall = 0;
		foreach ($projects as $project){
				$outputProjects .=  '
				<td class="text-center" >'.$i.'</td>
				<td >'.$this->DateThai($project['PracticalProject']['datestart']).'</td>';
				
				$outputProjects .=  '<td >'.$project['PracticalProject']['name'].' </td>';  
				if(((( strtotime($date.' '.$time) -   strtotime($this->currentdatestu) )/  ( 60 * 60 * 24 )) > 0) ) {
						
					$link3= '';	

				}else {
					if ($project['PracticalStudent']['exclusive'] == 1) {
						$link3= '<font color="green"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></font> ';	
						
					} else {
						$link3= '<a href="http://www.agri.cmu.ac.th/smart_academic/practicals/delete_receivestudents/'.$project['PracticalStudent']['id'].'" class="btn btn-danger">ลบ</a>';	
						
					}
					

				}
				$outputProjects .=  '<td class="text-center">'.$link3.'</td>	 
					</tr> ';
		
			
			$i++;
		}
	
		$this->set(array('outputProjects' => $outputProjects));
	}
	public function delete_receivestudents($id = null) {
		$this->request->data = $this->PracticalStudent->findById($id);
		if($this->request->data){
			
			$this->PracticalStudent->delete($id);

			$this->Session->write('alertType','danger');
			$this->Session->setFlash('ลบข้อมูล เรียบร้อยแล้ว');
			$this->redirect(array('action' => 'list_join_student'));
		}
	}
    // =========================สำหรับนักศึกษาพี่เลี้ยงช่วยงาน =====================================
	public function project5($datestart = null) {
		$date = date("Y-m-d");
		$time = date("H:i:s");
		
		$this->set(array('datestart' => $datestart));
	
		// $this->layout='';
		if(((( strtotime($date.' '.$time) -   strtotime($this->currentdatestu) )/  ( 60 * 60 * 24 )) > 0) ) {						
			// $this->redirect(array('controller' => 'internships','action' => 'project5','2022-11-05'));	
		}else {
			
			$Students = $this->Session->read('Students');
			$this->set(array('Students' => $Students));
			$student_id = $Students['Student']['id'];
			if(!isset($Students) || $Students == null){
					$this->redirect('http://www.agri.cmu.ac.th/smart_academic/student_mains/login');
			}else {
				
			}
			 
		}	
		$projects = $this->PracticalProject->find('all', array(
			'conditions' => array(
				'datestart' => $datestart,
				'level' => 1
			),
			'order' => array('PracticalProject.datestart,PracticalProject.order' => 'ASC')
		));	
	
		$this->set(array(
			'datestart' => $datestart,
			
		));
		$countMembersList = array();
		$outputProjects = '';
		$i = 1;
		$total = 0;
		$totalall = 0;
		foreach ($projects as $project){
			// $a = $project['PracticalProject']['id'];
			$a = '';
			// $terms = array(1,3,2);
			// $terms = array(1,2);
			$terms = array(1);
			$rowspan = '';
				
			
				
				$outputTerms = '';
				foreach ($terms as $term){
					
					$amount = 0;
					$amount17 = 0;
					$amount1 = 0;
					$amount3 = 0;
					$amount2 = 0;
					$duration = '';
					$gender = '';
						$receive2s = $this->PracticalReceive->find('all', array(
							'conditions' => array(
								'PracticalReceive.practical_project_id' => $project['PracticalProject']['id'],
													
								)
						));	
						foreach ($receive2s as $key){
							$amount17 += $key['PracticalReceive']['amount_major_id_17'];
							$amount1 += $key['PracticalReceive']['amount_major_id_1'];
						}	
						//************** */
						$receives = $this->PracticalReceive->find('all', array(
							'conditions' => array(
								'PracticalReceive.practical_project_id' => $project['PracticalProject']['id'],
								'PracticalReceive.practical_term_id' => $term,							
								)
						));				
						
						foreach ($receives as $receive){				
							$amount += $receive['PracticalReceive']['amount_major_id_17'] + $receive['PracticalReceive']['amount_major_id_1'];
							$receiveId = $receive['PracticalReceive']['id'];
						}	
						
					
						
						if($amount == 0){ $amount = 0; }
						if($amount17 == 0 ){	$amount17 = '-'; }
						if($amount1 == 0 ){	$amount1 = '-'; }
						if($amount3 == 0 ){	$amount3 = '-'; }
						
						
						
						$amountReg = $this->PracticalStudent->find('count', array(
							'conditions' => array(
								'PracticalStudent.practical_receive_id' => $receiveId,									
							
							)
						));	
						$amountRest17 = $amount - $amountReg;
						$amountRestInput17 = '';
						if($amountRest17 > 0){
							$amountRestInput17 = ''.$amountRest17.'<br>';
						}
						if ($amount == null) {
							$outputTerms .= 
								'<td class="text-center"><font color="red">ไม่มีจำนวนรับ</font>	</td>';
						}else {
							if ($amountRestInput17 == 0) {
								$outputTerms .= 
								'<td class="text-center"><font color="green">เต็ม</font>	</td>';
							} else {
								$outputTerms .= 
									'<td class="text-center">'.$amountRestInput17.'</td>';
							}

						}
						$total = $total + $amount;
						$totalall = $totalall+($amountReg);
						
						
				}
				if(((( strtotime($date.' '.$time) -   strtotime($this->currentdatestu) )/  ( 60 * 60 * 24 )) > 0) ) {
						
					$link3= '';	

				}else {
					$link3= '<a href="http://www.agri.cmu.ac.th/smart_academic/practicals/register5/'.$project['PracticalProject']['id'].'/'.$datestart.'" class="btn btn-primary">ลงทะเบียน</a>';	

				}
				
					
					$countStudent = $this->PracticalStudent->find('count', array(
						'conditions' => array(
							'PracticalStudent.practical_project_id' =>  $project['PracticalProject']['id'],									
						
						),
						'order' => array('PracticalStudent.practical_project_id' => 'asc')
					));	
					
					
					array_push($countMembersList,$countStudent);

					if ($project['PracticalProject']['datestart'] == '2022-06-05') {
						$outputProjects .= 
						'<tr class="success"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-06') {
							$outputProjects .= 
							'<tr class="warning"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-07') {
							$outputProjects .= 
							'<tr class="danger"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-08') {
							$outputProjects .= 
							'<tr class="info"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-09') {
							$outputProjects .= 
							'<tr style="background-color: #FFEBFF;"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-10') {
							$outputProjects .= 
							'<tr style="background-color: #E5F9B5;" > ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-11') {
							$outputProjects .= 
							'<tr class="warning"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-12') {
							$outputProjects .= 
							'<tr style="background-color: #F9E7B5;"  > ';
					} else {
						# code...
					}
				

				$outputProjects .=  '<td class="text-center" '.$rowspan.'>'.$i.'</td>';
					
				if ($project['PracticalProject']['datestart'] !=null) {
					$outputProjects .=  '<td >'.$this->DateThai($project['PracticalProject']['datestart']).'</td>';		
				} else {
					$outputProjects .=  '<td ></td>';		
				}	
				if ($countMembersList[$i - 1] == null) {
					$link4= ''.$countMembersList[$i - 1].'';	
				} else {
					$link4= '<a href="
				http://www.agri.cmu.ac.th/smart_academic/practicals/list_student/'.$project['PracticalProject']['id'].'" 
				class="btn btn-success btn-block" target="_blank">'.$countMembersList[$i - 1].'</a>';	
				}
				
				
				$outputProjects .=  '<td '.$rowspan.'>'.$project['PracticalProject']['name'].' '.$a.'</td>';  
				if (( ($amount17+$amount1) - ($countMembersList[$i - 1]) ) == 0) {
					$outputProjects .=  '<td class="text-center">'.($amount17+$amount1).' </td>';
				} else {
					$outputProjects .=  '<td class="text-center">'.($amount17+$amount1).'<br>'.$link3.'</td>';
				}
				$outputProjects .=  '<td class="text-center">'.$link4.'</td>
						'.$outputTerms.' 
					</tr> ';
		
			
			$i++;
		}
		$outputProjects .=  '
					<tr>
						<td colspan = "3" align="center"><h4> รวม</h4></td>	
						<td>จำนวนที่ต้องการ <h4>'.$total.'</h4></td>	
						<td align="center">จำนวนที่ได้จริง<h4>'.$totalall.'</h4> </td>  
						<td > </td>  
						 
					</tr> ';
		$this->set(array('outputProjects' => $outputProjects));
	}
	public function register5($project_id =null,$datestart =null){
		$date = date("Y-m-d");
		$time = date("H:i:s");
		// $this->layout='';
		if(((( strtotime($date.' '.$time) -   strtotime($this->currentdatestu) )/  ( 60 * 60 * 24 )) > 0) ) {						
			$this->redirect(array('controller' => 'internships','action' => 'project5','2022-06-05'));	
		}else {
			
		}	
		$this->set(array('datestart' => $datestart));

		$Students = $this->Session->read('Students');
		$this->set(array('Students' => $Students));
		
		if(!isset($Students) || $Students == null){
				$this->redirect('http://www.agri.cmu.ac.th/smart_academic/student_mains/login');
		}else {
			
		}
		
		$data = array();
		// $terms = array(1,2);
		$terms = array(1);
		$amount = 0;
		$amount17 = 0;
		$amount1 = 0;
		$amountReg = 0;
		$amountRest17 = 0;
		$amountRestall = 0;
		$amountRestall2 = 0;
		$amountRestall3 = 0;
		if($this->request->data){
			date_default_timezone_set('Asia/Bangkok');
			$checkstudents = $this->Student->find('first', array(
				'conditions' => array(
					'Student.id' => $this->request->data['PracticalStudent']['stu_id'],
									
					)
			));	
			
			if ($checkstudents) {
					$receives = $this->PracticalReceive->find('all', array(
						'conditions' => array(
							'PracticalReceive.practical_project_id' => $project_id,
											
							)
					));	
					$lastprojects = $this->PracticalProject->find('first', array(
						'conditions' => array(
							'PracticalProject.id' => $project_id,
																		
						),
						'order' => array('PracticalProject.id' => 'DESC')
					));	
					$data2 = array();
					//*********เช็ควันซ้ำ****** */
						$checkduplicateStudents = $this->PracticalStudent->find('all', array(
							'conditions' => array(								
								'PracticalStudent.student_id' => $checkstudents['Student']['id'],												
							),
							'order' => array('PracticalStudent.practical_project_id,PracticalStudent.practical_term_id' => 'ASC')
						));	
						// debug($checkduplicateStudents);
						foreach ($checkduplicateStudents as $checkStudent) {							
							$checkproject2s = $this->PracticalProject->find('all', array(
								'conditions' => array(
									'PracticalProject.id' => $checkStudent['PracticalStudent']['practical_project_id'],
																				
								),
								'order' => array('PracticalProject.id' => 'ASC')
							));	
						
							foreach ($checkproject2s as $checkproject2 ) {

								if ($lastprojects['PracticalProject']['datestart'] == $checkproject2['PracticalProject']['datestart']) {									
									if ($this->request->data['PracticalStudent']['time_id'] == 1) {
										$receive2s = $this->PracticalReceive->find('first', array(
											'conditions' => array(
												'PracticalReceive.practical_project_id' => $project_id,
												'PracticalReceive.practical_term_id' => 1			
												)
										));
									} elseif ($this->request->data['PracticalStudent']['time_id'] == 2) {
										$receive2s = $this->PracticalReceive->find('first', array(
											'conditions' => array(
												'PracticalReceive.practical_project_id' => $project_id,
												'PracticalReceive.practical_term_id' => 2			
												)
										));
									} elseif ($this->request->data['PracticalStudent']['time_id'] == 3) {
										$receive2s = $this->PracticalReceive->find('first', array(
											'conditions' => array(
												'PracticalReceive.practical_project_id' => $project_id,
												// 'PracticalReceive.practical_term_id' => 2			
												)
										));
									}
									
										
									// debug($checkStudent['PracticalStudent']['practical_term_id']);
									// debug($receive2s['PracticalReceive']['practical_term_id']);
									if ($checkStudent['PracticalStudent']['practical_term_id'] == $receive2s['PracticalReceive']['practical_term_id']) {
										$this->Session->setFlash('ขออภัย ได้เลือกช่วงเวลานี้ไปแล้ว ไม่สามารถลงซ้ำได้  !!!');
										$this->Session->write('alertType','danger');
										$this->redirect(array('action' => 'register5',$project_id,$datestart));
									}

								
								} 
							}
						}
					//********* */
					foreach ($receives as $key) {
						//****************check duplicate */
						$checkduplicateprojects = $this->PracticalProject->find('first', array(
							'conditions' => array(
								'PracticalProject.id' => $project_id,
																			
							),
							'order' => array('PracticalProject.id' => 'DESC')
						));	
						
							
						$checkduplicatereceives = $this->PracticalStudent->find('all', array(
							'conditions' => array(
								'PracticalStudent.practical_receive_id' => $key['PracticalReceive']['id'],
								'PracticalStudent.student_id' => $checkstudents['Student']['id'],												
							),
							'order' => array('PracticalStudent.id' => 'DESC')
						));	
						foreach ($checkduplicatereceives as $check) {
							
							
							// if ($check['PracticalStudent']['practical_receive_id'] == $key['PracticalReceive']['id']) {
							// 	$this->Session->setFlash('ขออภัย ได้เลือกลงทะเบียนฝ่ายนี้แล้ว  !!!');
							// 	$this->Session->write('alertType','danger');
							// 	$this->redirect(array('action' => 'register5',$project_id,$datestart));
							// }
							
							
						}

						foreach ($terms as $term){
							//********เช็คว่าเต็มหรือไม่****** */
							$receives = $this->PracticalReceive->find('all', array(
								'conditions' => array(
									'PracticalReceive.practical_project_id' => $project_id,
									'PracticalReceive.practical_term_id' => $term,							
									)
							));				
							foreach ($receives as $receive){				
								$amount += $receive['PracticalReceive']['amount_major_id_17'] + $receive['PracticalReceive']['amount_major_id_1'];
								$amount17 += $receive['PracticalReceive']['amount_major_id_17']; //ช่วงเช้า
								$amount1 += $receive['PracticalReceive']['amount_major_id_1']; //ช่วงบ่าย
								
								$receiveId = $receive['PracticalReceive']['id'];
								
							}	
							
						}
							$amountReg = $this->PracticalStudent->find('count', array(
								'conditions' => array(
									'PracticalStudent.practical_project_id' => $project_id,									
								
								)
							));	
							
							
					
						//************** */
						// debug($receiveId);
						// debug($amount);
						// debug($amountReg);
						$amountRestall = $amount - $amountReg;
						
						
						if ($this->request->data['PracticalStudent']['time_id'] == 1) {
							$amountReg2 = $this->PracticalStudent->find('count', array(
								'conditions' => array(
									'PracticalStudent.practical_project_id' => $project_id,										
									'PracticalReceive.practical_term_id' => 1,
								)
							));
							$amountRestall2 = $amount17 - $amountReg2;
							if ($amountRestall2 == 0 ) {
								$this->Session->write('alertType','danger');
										$this->Session->setFlash('ขออภัย เต็ม แล้ว!!');
										$this->redirect(array('action' => 'register5',$project_id,$datestart));
							} else {
								if ($this->request->data['PracticalStudent']['time_id'] == 1) {
									if ($key['PracticalReceive']['practical_term_id'] == 1) {
										$data = array(
											// 'id' => $key['PracticalReceive']['id'],
											'student_id' => $checkstudents['Student']['id'],
											'practical_receive_id' => $key['PracticalReceive']['id'],
											'practical_project_id' => $key['PracticalReceive']['practical_project_id'],	
											'practical_term_id' => $key['PracticalReceive']['practical_term_id'],						
										);
										$this->PracticalStudent->save($data);
									}
								}
							}
						}    
						if ($this->request->data['PracticalStudent']['time_id'] == 2) {
							$amountReg2 = $this->PracticalStudent->find('count', array(
								'conditions' => array(
									'PracticalStudent.practical_project_id' => $project_id,										
									'PracticalReceive.practical_term_id' => 2,
								)
							));
							$amountRestall3 = $amount1 - $amountReg2;
							if ($amountRestall3 == 0 ) {
								$this->Session->write('alertType','danger');
										$this->Session->setFlash('ขออภัย ช่วงบ่าย เต็ม แล้ว!!');
										$this->redirect(array('action' => 'register5',$project_id,$datestart));
							} else {
								if ($this->request->data['PracticalStudent']['time_id'] == 2) {
									if ($key['PracticalReceive']['practical_term_id'] == 2) {
										$data = array(
											// 'id' => $key['PracticalReceive']['id'],
											'student_id' => $checkstudents['Student']['id'],
											'practical_receive_id' => $key['PracticalReceive']['id'],
											'practical_project_id' => $key['PracticalReceive']['practical_project_id'],
											'practical_term_id' => $key['PracticalReceive']['practical_term_id'],							
										);
										$this->PracticalStudent->save($data);
									}
								}
							}
						}  
						if ($this->request->data['PracticalStudent']['time_id'] == 3) {
							$amountReg2 = $this->PracticalStudent->find('count', array(
								'conditions' => array(
									'PracticalStudent.practical_project_id' => $project_id,										
									'PracticalReceive.practical_term_id' => 1,
								)
							));
							$amountRestall2 = $amount17 - $amountReg2;
							if ($amountRestall2 == 0 ) {
								$this->Session->write('alertType','danger');
								$this->Session->setFlash('ขออภัยไม่สามารถลงเวลาปฏิบัติงานได้ กรุณาเลือกรายการใหม่อีกครั้ง');
								$this->redirect(array('action' => 'register5',$project_id,$datestart));
							} else {
								$amountReg2 = $this->PracticalStudent->find('count', array(
									'conditions' => array(
										'PracticalStudent.practical_project_id' => $project_id,										
										'PracticalReceive.practical_term_id' => 2,
									)
								));
								$amountRestall3 = $amount1 - $amountReg2;
								if ($amountRestall3 == 0 ) {
									$this->Session->write('alertType','danger');
									$this->Session->setFlash('ขออภัยไม่สามารถลงเวลาปฏิบัติงานได้ กรุณาเลือกรายการใหม่อีกครั้ง');
									$this->redirect(array('action' => 'register5',$project_id,$datestart));
								} else {
									if ($amountRestall == 0) {
										$this->Session->write('alertType','danger');
										$this->Session->setFlash('เต็ม');
										$this->redirect(array('action' => 'register5',$project_id,$datestart));
									} else {
										if ($this->request->data['PracticalStudent']['time_id'] == 3) {
											$data2 = array(
												// 'id' => $lastid,
												'student_id' => $checkstudents['Student']['id'],
												'practical_receive_id' => $key['PracticalReceive']['id'],
												'practical_project_id' => $key['PracticalReceive']['practical_project_id'],	
												'practical_term_id' => $key['PracticalReceive']['practical_term_id'],						
											);
											array_push($data,$data2);
										}
									}
								}
								
							}
	
						}
						
					}
					if ($this->request->data['PracticalStudent']['time_id'] == 3) {
						$this->PracticalStudent->saveMany($data);
					}
					
				$this->Session->write('alertType','success');
				$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
				$this->redirect(array('action' => 'register5',$project_id,$datestart));
			}
			
		}	
		
		//*********************** */
		    $amounts = 0;
			$amountRegs = 0;
			$amountRest17s = 0;
			foreach ($terms as $term){
				//********เช็คว่าเต็มหรือไม่****** */
				$receives = $this->PracticalReceive->find('all', array(
					'conditions' => array(
						'PracticalReceive.practical_project_id' => $project_id,
						'PracticalReceive.practical_term_id' => $term,							
						)
				));				
				foreach ($receives as $receive){				
					$amounts += $receive['PracticalReceive']['amount_major_id_17'] + $receive['PracticalReceive']['amount_major_id_1'];
					$receiveId = $receive['PracticalReceive']['id'];

				}	
				
			}
				$amountRegs = $this->PracticalStudent->find('count', array(
					'conditions' => array(
						'PracticalStudent.practical_project_id' => $project_id,									
					
					)
				));	
		
			//************** */
		
			$amountRest17s = $amounts - $amountRegs;
			$this->set(array(
				'amounts' => $amounts,
				'amountRegs' => $amountRegs,
				'amountRest17s' => $amountRest17s
			));
		
		$currentStudents = $this->Studentterm->find('all', array(
			'conditions' => array(
				'and' => array(
					array('Student.degree_id <= ' => 3),
					array('Student.degree_id >= ' => 1),
				),
				'term_year' =>  2562,
				'term_id' =>  1,
				// 'Student.degree_id' => 1,
				
			),
			
			'fields' => array('Student.id','Student.student_firstname','Student.student_surname'),
			'order' => array('Studentterm.term_id' => 'DESC'),
		));  
	
			$outputstudents = '';
			$b =0;
			foreach ($currentStudents as $employee) {
				$b++;
				if ($employee['Student']['id']== "809") {
					$outputstudents .= '
					<option value="'.$employee['Student']['id'].'">'.$employee['Student']['student_firstname'].'</option>
				';
				}else {					
				$outputstudents .= '
					<option value="'.$employee['Student']['id'].'">'.$b.'. '.$employee['Student']['id'].' '.$employee['Student']['student_firstname'].' '.$employee['Student']['student_surname'].'</option>
				';
				}

			}
		$receive = $this->PracticalProject->find('first', array(
			'conditions' => array(			
				'PracticalProject.id' => $project_id,				
			),			
		));
		$studentRegs = $this->PracticalStudent->find('all', array(
			'conditions' => array(			
				'PracticalStudent.practical_project_id' => $project_id,				
			),	
			'order' => array('PracticalStudent.practical_term_id' => 'ASC')		
		));
	    //debug($studentRegs);
		// $this->request->data = $receive;
		$this->set(array(		
			'receive' => $receive,
			'studentRegs' => $studentRegs,
			'outputstudents' => $outputstudents
			
		));
	} 
	public function project6($datestart = null) {
		$date = date("Y-m-d");
		$time = date("H:i:s");
		$day = date("d");
		$this->set(array('datestart' => $datestart));
	
		// $this->layout='';
		if(((( strtotime($date.' '.$time) -   strtotime($this->currentdatestu) )/  ( 60 * 60 * 24 )) > 0) ) {						
			// $this->redirect(array('controller' => 'internships','action' => 'project5','2022-06-05'));	
		}else {
			
			$Students = $this->Session->read('Students');
			$this->set(array('Students' => $Students));
			$student_id = $Students['Student']['id'];
			// if(!isset($Students) || $Students == null){
			// 		$this->redirect('http://www.agri.cmu.ac.th/smart_academic/student_mains/login');
			// }else {
				
			// }
			$internship290Trans = $this->PracticalTran->find('all', array(
				'conditions' => array(
					'student_id' => $Students['Student']['id'],
					
				),
				
			));	
			$this->set(array(
				
				'internship290Trans' => $internship290Trans
			));
		}	
		if ($datestart == $date) {
			$projects = $this->PracticalProject->find('all', array(
				'conditions' => array(
					'datestart' => $date,
					'level' => 1
				),
				'order' => array('PracticalProject.datestart,PracticalProject.order' => 'ASC')
			));	
			
		}else {
			$projects = $this->PracticalProject->find('all', array(
				'conditions' => array(
					'datestart' => $datestart,
					'level' => 1
				),
				'order' => array('PracticalProject.datestart,PracticalProject.order' => 'ASC')
			));	
		}
		
	
		$this->set(array(
			'datestart' => $datestart,
			
		));
		$countMembersList = array();
		$outputProjects = '';
		$i = 1;
		$total = 0;
		$totalall = 0;
		foreach ($projects as $project){
			// $a = $project['PracticalProject']['id'];
			$a = '';
			// $terms = array(1,3,2);
			$terms = array(1,2);
			$rowspan = '';
				
			
				
				$outputTerms = '';
				foreach ($terms as $term){
					
					$amount = 0;
					$amount17 = 0;
					$amount1 = 0;
					$amount3 = 0;
					$amount2 = 0;
					$duration = '';
					$gender = '';
						$receive2s = $this->PracticalReceive->find('all', array(
							'conditions' => array(
								'PracticalReceive.practical_project_id' => $project['PracticalProject']['id'],
													
								)
						));	
						foreach ($receive2s as $key){
							$amount17 += $key['PracticalReceive']['amount_major_id_17'];
							$amount1 += $key['PracticalReceive']['amount_major_id_1'];
						}	
						//************** */
						$receives = $this->PracticalReceive->find('all', array(
							'conditions' => array(
								'PracticalReceive.practical_project_id' => $project['PracticalProject']['id'],
								'PracticalReceive.practical_term_id' => $term,							
								)
						));				
						
						foreach ($receives as $receive){				
							$amount += $receive['PracticalReceive']['amount_major_id_17'] + $receive['PracticalReceive']['amount_major_id_1'];
							$receiveId = $receive['PracticalReceive']['id'];
						}	
						
					
						
						if($amount == 0){ $amount = 0; }
						if($amount17 == 0 ){	$amount17 = '-'; }
						if($amount1 == 0 ){	$amount1 = '-'; }
						if($amount3 == 0 ){	$amount3 = '-'; }
						
						
						
						$amountReg = $this->PracticalStudent->find('count', array(
							'conditions' => array(
								'PracticalStudent.practical_receive_id' => $receiveId,									
							
							)
						));	
						$amountRest17 = $amount - $amountReg;
						$amountRestInput17 = '';
						if($amountRest17 > 0){
							$amountRestInput17 = ''.$amountRest17.'<br>';
						}
						if ($amount == null) {
							$outputTerms .= 
								'<td class="text-center"><font color="red">ไม่มีจำนวนรับ</font>	</td>';
						}else {
							if(((( strtotime($date.' '.$time) -   strtotime($this->currentdatestu) )/  ( 60 * 60 * 24 )) > 0) ) {						
								$outputTerms .= 
									'<td class="text-center">'.$amountReg.'</td>';
							}else {
								$outputTerms .= 
									'<td class="text-center">'.$amountRest17.'</td>';
								
							}
								

						}
						$total = $total + $amount;
						$totalall = $totalall+($amountReg);
						
						
				}
				if(((( strtotime($date.' '.$time) -   strtotime($this->currentdatestu) )/  ( 60 * 60 * 24 )) > 0) ) {
						
					$link3= '';	

				}else {
					$link3= '<a href="http://www.agri.cmu.ac.th/smart_academic/practicals/register5/'.$project['PracticalProject']['id'].'/'.$datestart.'" class="btn btn-primary">ลงทะเบียน</a>';	

				}
				
					
					$countStudent = $this->PracticalStudent->find('count', array(
						'conditions' => array(
							'PracticalStudent.practical_project_id' =>  $project['PracticalProject']['id'],									
						
						),
						'order' => array('PracticalStudent.practical_project_id' => 'asc')
					));	
					
					
					array_push($countMembersList,$countStudent);

					if ($project['PracticalProject']['datestart'] == '2022-06-05') {
						$outputProjects .= 
						'<tr class="success"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-06') {
							$outputProjects .= 
							'<tr class="warning"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-07') {
							$outputProjects .= 
							'<tr class="danger"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-08') {
							$outputProjects .= 
							'<tr class="info"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-09') {
							$outputProjects .= 
							'<tr style="background-color: #FFEBFF;"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-10') {
							$outputProjects .= 
							'<tr style="background-color: #E5F9B5;" > ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-11') {
							$outputProjects .= 
							'<tr class="warning"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-12') {
							$outputProjects .= 
							'<tr style="background-color: #F9E7B5;"  > ';
					} else {
						# code...
					}
				

				$outputProjects .=  '<td class="text-center" '.$rowspan.'>'.$i.'</td>';
					
				if ($project['PracticalProject']['datestart'] !=null) {
					$outputProjects .=  '<td >'.$this->DateThai($project['PracticalProject']['datestart']).'</td>';		
				} else {
					$outputProjects .=  '<td ></td>';		
				}	
				if ($countMembersList[$i - 1] == null) {
					$link4= ''.$countMembersList[$i - 1].'';	
				} else {
					$link4= '<a href="
				http://www.agri.cmu.ac.th/smart_academic/practicals/list_student_nophone/'.$project['PracticalProject']['id'].'" 
				class="btn btn-success btn-block" target="_blank">'.$countMembersList[$i - 1].'</a>';	
				}
				
				
				$outputProjects .=  '<td '.$rowspan.'>'.$project['PracticalProject']['name'].' '.$a.'</td>';  
				if (( ($amount17+$amount1) - ($countMembersList[$i - 1]) ) == 0) {
					$outputProjects .=  '<td class="text-center">'.($amount17+$amount1).' </td>';
				} else {
					$outputProjects .=  '<td class="text-center">'.($amount17+$amount1).'<br></td>';
				}
				$outputProjects .=  '<td class="text-center">'.$link4.'</td>
						'.$outputTerms.' 
					</tr> ';
		
			
			$i++;
		}
		$outputProjects .=  '
					<tr>
						<td colspan = "3" align="center"><h4> รวม</h4></td>	
						<td align="center">จำนวนที่ต้องการ <h4>'.$total.'</h4></td>	
						<td align="center">จำนวนที่ได้จริง<h4>'.$totalall.'</h4> </td>  
						<td > </td>  
						<td > </td>  	
					</tr> ';
		$this->set(array('outputProjects' => $outputProjects));
	}
	public function checklist2($receiveId = null) {
		// $this->layout='';
		if(isset($receiveId)){
			
			$receive = $this->PracticalReceive->find('first', array(
						'conditions' => array(
							'PracticalReceive.id' => $receiveId,
												
						)
					));	
					
				
			if(count($receive) > 0){
				
				$projectName = $receive['PracticalProject']['name'].' ('.$receive['PracticalTerm']['name'].')';
				
				// if($receiveId == 27 || $receiveId == 28 || $receiveId == 29 || $receiveId == 30){
				// 	$projectName .= '<br>'.$receive['Receive']['duration'];
				// }
			
				
				

				$studentRegs = $this->PracticalStudent->find('all', array(
							'conditions' => array(
								'PracticalStudent.practical_receive_id' => $receiveId,
								// 'PracticalStudent.year_education_id' => $this->yearEducation['YearEducation']['id']
								),
							'order' => array('PracticalStudent.student_id')
						));
				
						
				if(count($studentRegs) > 0){
					$this->set(array('studentRegs' => $studentRegs,'projectName' => $projectName));				
				}
				else{
					$this->redirect(array('action' => 'checklist'));				
				}
			}		
			else{				
				$this->redirect(array('action' => 'checklist2'));				
			}		
			
		}
		else{
			
			$projects = $this->PracticalProject->find('all', array(
				'conditions' => array(
					'level' => 1
					
				),
				'order' => array('PracticalProject.datestart,PracticalProject.order' => 'ASC')
			));				
			
			$outputProjects = '';
			$i = 1;
			
			$terms = array(1,2);
			$totalTerm = array();
			foreach ($terms as $term){
				$totalTerm[$term] = 0;
			}
			
			foreach ($projects as $project){
				$a = $project['PracticalProject']['id'];
				
				$rowspan = '';	
				// if($project['PracticalProject']['id'] == 1 || $project['PracticalProject']['id'] == 2
				// 	|| $project['PracticalProject']['id'] == 4 || $project['PracticalProject']['id'] == 42
				// 	|| $project['PracticalProject']['id'] == 5 || $project['PracticalProject']['id'] == 6
				// 	|| $project['PracticalProject']['id'] == 7 || $project['PracticalProject']['id'] == 10
				// 	|| $project['PracticalProject']['id'] == 11 || $project['PracticalProject']['id'] == 12
				// 	|| $project['PracticalProject']['id'] == 15 || $project['PracticalProject']['id'] == 16
				// 	|| $project['PracticalProject']['id'] == 17 || $project['PracticalProject']['id'] == 21
				// 	|| $project['PracticalProject']['id'] == 22 || $project['PracticalProject']['id'] == 24
				// 	|| $project['PracticalProject']['id'] == 25 || $project['PracticalProject']['id'] == 26
				// 	|| $project['PracticalProject']['id'] == 31 || $project['PracticalProject']['id'] == 33
				// 	|| $project['PracticalProject']['id'] == 34 || $project['PracticalProject']['id'] == 35
				// 	|| $project['PracticalProject']['id'] == 40 || $project['PracticalProject']['id'] == 43
				// 	|| $project['PracticalProject']['id'] == 44 || $project['PracticalProject']['id'] == 49
				// 	|| $project['PracticalProject']['id'] == 51 || $project['PracticalProject']['id'] == 52
				// 	|| $project['PracticalProject']['id'] == 53 || $project['PracticalProject']['id'] == 59
				// 	|| $project['PracticalProject']['id'] == 60 || $project['PracticalProject']['id'] == 61
				// 	|| $project['PracticalProject']['id'] == 62 || $project['PracticalProject']['id'] == 63
				// 	|| $project['PracticalProject']['id'] == 66 
				// 	){
				// 		// $rowspan = 'rowspan = "3"';	
				// 		$rowspan = 'rowspan = "1"';	
						
				// 		$outputTerms = '';
				// 		$outputTerms2 = '';
				// 		foreach ($terms as $term){
				// 			$receives = $this->PracticalReceive->find('all', array(
				// 				'conditions' => array(
				// 					'PracticalReceive.practical_project_id' => $project['PracticalProject']['id'],
				// 					// 'PracticalReceive.practical_term_id' => $term,													
														
				// 				)
				// 			));				
				// 			$amount = 0;
				// 			$amount17 = 0;
				// 			$amount1 = 0;
				// 			$amount3 = 0;
				// 			$duration = '';
				// 			foreach ($receives as $receive){					
				// 					$amount += $receive['PracticalReceive']['amount'];
				// 					$amount17 += $receive['PracticalReceive']['amount_major_id_17'];
				// 					$amount1 += $receive['PracticalReceive']['amount_major_id_1'];
				// 					$amount3 += $receive['PracticalReceive']['amount_major_id_3'];
				// 					$receiveId = $receive['PracticalReceive']['id'];
				// 			}	
							
				// 			if($amount == 0){ $amount = '-'; }
				// 			if($amount17 == 0 ){ $amount17 = '-'; }
				// 			if($amount1 == 0 ){	$amount1 = '-'; }
				// 			if($amount3 == 0 ){	$amount3 = '-'; }
							
				// 			$amountReg = $this->PracticalStudent->find('count', array(
				// 				'conditions' => array(
				// 					'PracticalStudent.practical_receive_id' => $receiveId,
				// 					// 'PracticalStudent.year_education_id' => $this->yearEducation['YearEducation']['id']
				// 				)
				// 			));	
							
				// 			//debug($receiveId[0]);
				// 			$link = '';
				// 			if($amountReg > 0){
				// 				$link = '<a href="checklist/'.$receiveId.'" target="_blank">'.$amountReg.'</a><br>';
								
				// 				$totalTerm[$term] += $amountReg;
				// 			}
							
	
							
				// 		}
				// 		// $outputTerms2 .= 
				// 		// 	'<td class="text-center">'.$link.'</td>';
				// 			if ($amount == 0) {
				// 				$outputTerms2 .= 
				// 				'<td class="text-center" colspan="2">'.$link.' </td>
				// 				';
				// 			}else {
				// 				$outputTerms2 .= 
				// 				'<td class="text-center" colspan="2">'.$link.' </td>
				// 				';
				// 			}	
				// 			// $outputTerms2 .= 
				// 			// '<td class="text-center" > -</td>
				// 			// ';	
						
									
				// 		$outputProjects .= 
				// 			'<tr> 
				// 				<td class="text-center" '.$rowspan.'>'.$i.'</td>
				// 				<td '.$rowspan.'>'.$project['PracticalProject']['datestart'].'</td>
				// 				<td '.$rowspan.'>'.$project['PracticalProject']['name'].' '.$a.'</td> 
				// 				<td class="text-center"><b>'.$amount.'</b></td>
				// 				'.$outputTerms2.'
				// 			</tr>';
									
						
									
				// 		$outputTerms2 = '';
				// 		$outputTerms .= 
				// 					'<tr> 
				// 						'.$outputTerms2.'
				// 					</tr> ';
									
									
				// 		$outputProjects .= $outputTerms;
				// }else{
					
					$outputTerms = '';
					foreach ($terms as $term){
						$receiveId = array();
								$amount = 0;
								$amount17 = 0;
								$amount1 = 0;
								$amount3 = 0;
								$gender = '';
						$receives = $this->PracticalReceive->find('all', array(
							'conditions' => array('PracticalReceive.practical_project_id' => $project['PracticalProject']['id'],
													'PracticalReceive.practical_term_id' => $term,
													// 'Receive.year_education_id' => $this->yearEducation['YearEducation']['id']
													)
						));				
						
						$receive2s = $this->PracticalReceive->find('all', array(
							'conditions' => array(
								'PracticalReceive.practical_project_id' => $project['PracticalProject']['id'],
																		
													
							)
						));	
						foreach ($receive2s as $receive){				
						
							$amount17 += $receive['PracticalReceive']['amount_major_id_17'];
							$amount1 += $receive['PracticalReceive']['amount_major_id_1'];		
							$amount3 += $receive['PracticalReceive']['amount_major_id_3'];								
						
							
						}	
						// if($project['PracticalProject']['id'] == 23 || $project['PracticalProject']['id'] == 24){
							// $receiveId = array();
						// }
							
						$receiveId = array();
						$gender = '';
						foreach ($receives as $receive){											
							
							// if($project['PracticalProject']['id'] == 23 || $project['PracticalProject']['id'] == 24){

								// array_push($receiveId,$receive['Receive']['id']);
							// }
							// else{
								$receiveId = $receive['PracticalReceive']['id'];
							//}
						}	
						
						$amountReg = $this->PracticalStudent->find('count', array(
							'conditions' => array('PracticalStudent.practical_receive_id' => $receiveId,
													// 'PracticalStudent.year_education_id' => $this->yearEducation['YearEducation']['id']
													)
						));	
						
						//debug($receiveId[0]);
					
							
						$link = '';
						if($amountReg > 0){
							$link = '<a href="checklist2/'.$receiveId.'" target="_blank">'.$amountReg.'</a><br>'.$gender;
							
							$totalTerm[$term] += $amountReg;
						}
						

						$outputTerms .= 
							'<td class="text-center">'.$link.'</td>';
							
					}
					if ($project['PracticalProject']['datestart'] == '2022-06-05') {
						$outputProjects .= 
						'<tr class="success"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-06') {
							$outputProjects .= 
							'<tr class="warning"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-07') {
							$outputProjects .= 
							'<tr class="danger"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-08') {
							$outputProjects .= 
							'<tr class="info"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-09') {
							$outputProjects .= 
							'<tr style="background-color: #FFEBFF;"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-10') {
							$outputProjects .= 
							'<tr style="background-color: #E5F9B5;" > ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-11') {
							$outputProjects .= 
							'<tr class="warning"> ';
					}elseif ($project['PracticalProject']['datestart'] == '2022-06-12') {
							$outputProjects .= 
							'<tr style="background-color: #F9E7B5;"  > ';
					} else {
						# code...
					}
					
					$outputProjects .= 
						' 
							<td class="text-center" '.$rowspan.'>'.$i.'</td>';	
							if ($project['PracticalProject']['datestart'] !=null) {
								$outputProjects .=  '<td >'.$this->DateThai($project['PracticalProject']['datestart']).'</td>';		
							} else {
								$outputProjects .=  '<td ></td>';		
							}					
					$outputProjects .='<td '.$rowspan.'>'.$project['PracticalProject']['name'].' '.$a.'</td> 
							<td class="text-center"><b>'.($amount17+$amount1+$amount3).'</b></td>
							'.$outputTerms.' 
						</tr> ';
				//}
				
				$i++;
			}
			
			
		
			$outputTotalTerm = '';
			foreach ($terms as $term){
				$outputTotalTerm .= '<td class="text-center">'.$totalTerm[$term].'</td>';
			}
			
			
			$outputProjects .= 
				'<tr> 
					<td class="text-center" colspan="2">รวม</td>								
					'.$outputTotalTerm.' 
				</tr> ';
			
			$this->set(array('outputProjects' => $outputProjects));
		}
		
	}
	//******รับนักศึกษาสำหรับเจ้าหน้าที่****** */
	public function project3($datestart = null) {
		$date = date("Y-m-d");
		$time = date("H:i:s");
		$UserName = $this->Session->read('MisEmployee');
		$day1 = $this->Session->read('day1');
		$day2 = $this->Session->read('day2');	
		$day3 = $this->Session->read('day3');
		$day4 = $this->Session->read('day4');
		$day5 = $this->Session->read('day5');
		$day6 = $this->Session->read('day6');
		$day7 = $this->Session->read('day7');
		$day8 = $this->Session->read('day8');
		$day9 = $this->Session->read('day9');
		$day10 = $this->Session->read('day10');
		$day11 = $this->Session->read('day11');
		$day12 = $this->Session->read('day12');
		$day13 = $this->Session->read('day13');
		$day14 = $this->Session->read('day14');
		if(((( strtotime($date.' '.$time) -   strtotime($this->currentdateemp) )/  ( 60 * 60 * 24 )) > 0) ) {
			
			$this->redirect(array('action' => 'project4',$this->day1));
		}else {

		}
		 
		$UserName = $this->Session->read('MisEmployee'); 
		if(isset($UserName)){
			$this->set('UserName', $UserName);
			 
			

			if($this->action == 'login')
				$this->redirect(array('action' => 'Dashboard'));
		}
		else{
			if($this->action != 'login')
				$this->redirect(array('controller' => 'mains','action' => 'login'));
			
		}
		$projects = $this->PracticalProject->find('all', array(
			'conditions' => array(
				'datestart' => $datestart,
				'level' => 1
			),
			'order' => array('PracticalProject.datestart,PracticalProject.order' => 'ASC')
		));	
		$this->set(array('datestart' => $datestart));
		
		$outputProjects = '';
		$i = 1;
		
		foreach ($projects as $project){
			// $a = $project['PracticalProject']['id'];
			$a = '';
			// $terms = array(1,3,2);
			// $terms = array(1,2);
			$terms = array(1);
			$rowspan = '';
				
			
				
				$outputTerms = '';
				foreach ($terms as $term){
					$receives = $this->PracticalReceive->find('all', array(
						'conditions' => array(
							'PracticalReceive.practical_project_id' => $project['PracticalProject']['id'],
							'PracticalReceive.practical_term_id' => $term,							
							)
					));				
					
					$amount = 0;
					$amount17 = 0;
					$amount1 = 0;
					$amount3 = 0;
					$duration = '';
					$gender = '';
						foreach ($receives as $receive){					
							$amount += $receive['PracticalReceive']['amount'];
							$amount17 += $receive['PracticalReceive']['amount_major_id_17'];
							$amount1 += $receive['PracticalReceive']['amount_major_id_1'];
							$amount3 += $receive['PracticalReceive']['amount_major_id_3'];
							
						}	
						
					
						if($amount == 0){ $amount = 0; }
						
						if($amount17 == 0 ){ $amount17 = '<font color="red">ไม่มีจำนวนรับ</font>'; }
						if($amount1 == 0 ){	$amount1 = '<font color="red">ไม่มีจำนวนรับ</font>'; }
						if($amount3 == 0 ){	$amount3 = '-'; }
					
						
						
				}
					$outputTerms .= 
						'
						<td class="text-center">'.$amount17.'</td>
						
					';
					// <td class="text-center">'.$amount1.'</td>
					if ($project['PracticalProject']['datestart'] == $day1) {
						$outputProjects .= 
						'<tr class="success"> ';
					}elseif ($project['PracticalProject']['datestart'] == $day2) {
							$outputProjects .= 
							'<tr class="warning"> ';
					}elseif ($project['PracticalProject']['datestart'] == $day3) {
							$outputProjects .= 
							'<tr class="danger"> ';
					}elseif ($project['PracticalProject']['datestart'] == $day4) {
							$outputProjects .= 
							'<tr class="info"> ';
					}elseif ($project['PracticalProject']['datestart'] == $day5) {
							$outputProjects .= 
							'<tr style="background-color: #FFEBFF;"> ';
					}elseif ($project['PracticalProject']['datestart'] == $day6) {
							$outputProjects .= 
							'<tr style="background-color: #E5F9B5;" > ';
					}elseif ($project['PracticalProject']['datestart'] == $day7) {
							$outputProjects .= 
							'<tr class="warning"> ';
					}elseif ($project['PracticalProject']['datestart'] == $day8) {
							$outputProjects .= 
							'<tr style="background-color: #F9E7B5;"  > ';
					} else {
						$outputProjects .= 
							'<tr style="background-color: #F9E7B5;"  > ';
					}
				
				

				$outputProjects .=  '<td class="text-center" '.$rowspan.'>'.$i.'</td>';
					
					
				if ($project['PracticalProject']['datestart'] !=null) {
					$outputProjects .=  '<td >'.$this->DateThai($project['PracticalProject']['datestart']).'</td>';		
				} else {
					$outputProjects .=  '<td ></td>';		
				}						
				
				$link = ''.($amount17 + $amount1).'<br>
				<a href="http://agri.cmu.ac.th/smart_academic/practicals/edit_receive/'.$project['PracticalProject']['id'].'/'.$datestart.'" class="btn btn-primary">เพิ่มจำนวนรับ</a>';	

				$outputProjects .=  '<td '.$rowspan.'>'.$project['PracticalProject']['name'].' '.$a.'</td>  
						<td class="text-center">'.$link.'</td>
							
						'.$outputTerms.' 
					</tr> ';
		
			
			$i++;
		}
		
		$this->set(array('outputProjects' => $outputProjects));
	}
	public function project4($datestart = null) {
		$date = date("Y-m-d");
		$time = date("H:i:s");
		// $this->layout ='';
	 
		$day1 = $this->Session->read('day1');
		$day2 = $this->Session->read('day2');	
		$day3 = $this->Session->read('day3');
		$day4 = $this->Session->read('day4');
		$day5 = $this->Session->read('day5');
		$day6 = $this->Session->read('day6');
		$day7 = $this->Session->read('day7');
		$day8 = $this->Session->read('day8');
		$day9 = $this->Session->read('day9');
		$day10 = $this->Session->read('day10');
		$day11 = $this->Session->read('day11');
		$day12 = $this->Session->read('day12');
		$day13 = $this->Session->read('day13');
		$day14 = $this->Session->read('day14');
		$UserName = $this->Session->read('MisEmployee'); 
		if(isset($UserName)){
			$this->set('UserName', $UserName);
			 
			

			if($this->action == 'login')
				$this->redirect(array('action' => 'Dashboard'));
		}
		else{
			if($this->action != 'login')
				$this->redirect(array('controller' => 'mains','action' => 'login'));
			
		}
		$projects = $this->PracticalProject->find('all', array(
			'conditions' => array(
				'datestart' => $datestart,
				'level' => 1
			),
			'order' => array('PracticalProject.datestart,PracticalProject.order' => 'ASC')
		));	
		$this->set(array('datestart' => $datestart));
		$countMembersList = array();
		$outputProjects = '';
		$i = 1;
		$total = 0;
		$totalall = 0;
		foreach ($projects as $project){
			// $a = $project['PracticalProject']['id'];
			$a = '';
			// $terms = array(1,3,2);
			// $terms = array(1,2);
			$terms = array(1);
			$rowspan = '';
				
			
			
				$outputTerms = '';
				foreach ($terms as $term){
					
					$amount = 0;
					$amount17 = 0;
					$amount1 = 0;
					$amount3 = 0;
					$amount2 = 0;
					
					$duration = '';
					$gender = '';
						$receive2s = $this->PracticalReceive->find('all', array(
							'conditions' => array(
								'PracticalReceive.practical_project_id' => $project['PracticalProject']['id'],
													
								)
						));	
						foreach ($receive2s as $key){
							$amount17 += $key['PracticalReceive']['amount_major_id_17'];
							$amount1 += $key['PracticalReceive']['amount_major_id_1'];
						}	
						//************** */
						$receives = $this->PracticalReceive->find('all', array(
							'conditions' => array(
								'PracticalReceive.practical_project_id' => $project['PracticalProject']['id'],
								'PracticalReceive.practical_term_id' => $term,							
								)
						));				
						
						foreach ($receives as $receive){				
							$amount += $receive['PracticalReceive']['amount_major_id_17'] + $receive['PracticalReceive']['amount_major_id_1'];
							$receiveId = $receive['PracticalReceive']['id'];
						}	
						
					
						
						if($amount == 0){ $amount = 0; }
						if($amount17 == 0 ){	$amount17 = '-'; }
						if($amount1 == 0 ){	$amount1 = '-'; }
						if($amount3 == 0 ){	$amount3 = '-'; }
						
						
						
						$amountReg = $this->PracticalStudent->find('count', array(
							'conditions' => array(
								'PracticalStudent.practical_receive_id' => $receiveId,									
							
							)
						));	
						$amountRest17 = $amount - $amountReg;
						$amountRestInput17 = '';
						if($amountRest17 > 0){
							$amountRestInput17 = ''.$amountRest17.'<br>';
						}
						if ($amount == null) {
							$outputTerms .= 
								'<td class="text-center"><font color="red">ไม่มีจำนวนรับ</font>	</td>';
						}else {
							
							if ($amountRestInput17 == 0) {
								$outputTerms .= 
								'<td class="text-center"><font color="green">เต็ม</font>	</td>';
							} else {
								$outputTerms .= 
									'<td class="text-center">'.$amountRestInput17.'</td>';
							}
						}
						$total = $total + $amount;
						$totalall = $totalall+($amountReg);
						
				}
				if(((( strtotime($date.' '.$time) -   strtotime($this->currentdateemp) )/  ( 60 * 60 * 24 )) > 0) ) {
					$link3= '';
					if ($UserName['MisEmployee']['id'] == 793) {
						$link3= '<a href="http://agri.cmu.ac.th/smart_academic/practicals/edit_receivestudent/'.$project['PracticalProject']['id'].'/'.$datestart.'" class="btn btn-dafault">เพิ่มรหัสนักศึกษา</a>';	
				
					}
				}else {
					
					$link3= '<a href="http://agri.cmu.ac.th/smart_academic/practicals/edit_receivestudent/'.$project['PracticalProject']['id'].'/'.$datestart.'" class="btn btn-primary">เพิ่มรหัสนักศึกษา</a>';	
				
				}	
					
					$countStudent = $this->PracticalStudent->find('count', array(
						'conditions' => array(
							'PracticalStudent.practical_project_id' =>  $project['PracticalProject']['id'],									
						
						),
						'order' => array('PracticalStudent.practical_project_id' => 'asc')
					));	
					
					
					array_push($countMembersList,$countStudent);

					if ($project['PracticalProject']['datestart'] == $day1) {
						$outputProjects .= 
						'<tr class="success"> ';
					}elseif ($project['PracticalProject']['datestart'] == $day2) {
							$outputProjects .= 
							'<tr class="warning"> ';
					}elseif ($project['PracticalProject']['datestart'] == $day3) {
							$outputProjects .= 
							'<tr class="danger"> ';
					}elseif ($project['PracticalProject']['datestart'] == $day4) {
							$outputProjects .= 
							'<tr class="info"> ';
					}elseif ($project['PracticalProject']['datestart'] == $day5) {
							$outputProjects .= 
							'<tr style="background-color: #FFEBFF;"> ';
					}elseif ($project['PracticalProject']['datestart'] == $day6) {
							$outputProjects .= 
							'<tr style="background-color: #E5F9B5;" > ';
					}elseif ($project['PracticalProject']['datestart'] == $day7) {
							$outputProjects .= 
							'<tr class="warning"> ';
					}elseif ($project['PracticalProject']['datestart'] == $day8) {
							$outputProjects .= 
							'<tr style="background-color: #F9E7B5;"  > ';
					} else {
						$outputProjects .= 
							'<tr style="background-color: #F9E7B5;"  > ';
					}
				

				$outputProjects .=  '<td class="text-center" '.$rowspan.'>'.$i.'</td>';
					
				if ($project['PracticalProject']['datestart'] !=null) {
					$outputProjects .=  '<td >'.$this->DateThai($project['PracticalProject']['datestart']).'</td>';		
				} else {
					$outputProjects .=  '<td ></td>';		
				}	
				if ($countMembersList[$i - 1] == null) {
					$link4= ''.$countMembersList[$i - 1].'';	
				} else {
					$link4= '<a href="
					http://agri.cmu.ac.th/smart_academic/practicals/list_student/'.$project['PracticalProject']['id'].'" 
					class="btn btn-success btn-block" target="_blank">'.$countMembersList[$i - 1].'</a>';	
				}
			
				
				$outputProjects .=  '<td '.$rowspan.'>'.$project['PracticalProject']['name'].' '.$a.'</td>';  
				if (($amount17+$amount1) == null ) {
					$outputProjects .=  '<td class="text-center">'.($amount17+$amount1).'<br> 	</td>';
				}else {
					if (( ($amount17+$amount1) - ($countMembersList[$i - 1]) ) == 0) {
						$outputProjects .=  '<td class="text-center">'.($amount17+$amount1).' </td>';
					} else {
						$outputProjects .=  '<td class="text-center">'.($amount17+$amount1).'<br>'.$link3.'</td>';
					}
				}
				if ($countMembersList[$i - 1] == null) {
					$outputProjects .=  '<td class="text-center"> </td> ';
				}else {
				 
					$outputProjects .=  '<td class="text-center">
											<a href="
											http://agri.cmu.ac.th/smart_academic/practicals/list_print/'.$project['PracticalProject']['id'].'" 
											class="btn btn-success btn-block" target="_blank">ปริ้นท์รายชื่อ</a>
										</td> ';
				}
				$outputProjects .=  '<td class="text-center">'.$link4.'</td>
						'.$outputTerms.'';
				$outputProjects .=  '</tr> ';	 
				
			 
			
			$i++;
		}
		$outputProjects .=  '
					<tr>
						<td colspan = "3" align="center"><h4> รวม</h4></td>	
						<td>จำนวนที่ต้องการ <h4>'.$total.'</h4></td>	
						<td align="center">จำนวนที่ได้จริง<h4>'.$totalall.'</h4> </td>  
						<td > </td>  
					 
					</tr> ';
		$this->set(array('outputProjects' => $outputProjects));
	}
	public function edit_receive($project_id =null,$datestart =null){
		$UserName = $this->Session->read('MisEmployee'); 
		if(isset($UserName)){
			$this->set('UserName', $UserName);
			 
			

			if($this->action == 'login')
				$this->redirect(array('action' => 'Dashboard'));
		}
		else{
			if($this->action != 'login')
				$this->redirect(array('controller' => 'mains','action' => 'login'));
			
		}
		$this->set(array('datestart' => $datestart));
		if($this->request->data){
			date_default_timezone_set('Asia/Bangkok');
			$receives = $this->PracticalReceive->find('all', array(
				'conditions' => array(
					'PracticalReceive.practical_project_id' => $project_id,
					// 'PracticalReceive.practical_term_id' => $term,							
					)
			));	
			
			foreach ($receives as $key) {
				if ($key['PracticalReceive']['practical_term_id'] == 1) {
						$data = array(
							'id' => $key['PracticalReceive']['id'],
							'amount' => $this->request->data['PracticalReceive']['amount11'],
							'amount_major_id_17' => $this->request->data['PracticalReceive']['amount_major_id_171'],						
						);
					$this->PracticalReceive->save($data);
				}elseif ($key['PracticalReceive']['practical_term_id'] == 2) {
					// $data = array(
					// 	'id' => $key['PracticalReceive']['id'],
					// 	'amount' => $this->request->data['PracticalReceive']['amount11'],
					// 	'amount_major_id_1' => $this->request->data['PracticalReceive']['amount_major_id_111'],						
					// );
				    //$this->PracticalReceive->save($data);
				}
				
			}
			
			$this->Session->write('alertType','success');
			$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
			$this->redirect(array('action' => 'project3',$datestart));
								
			 
		}	
		//*********************** */
		
		$receive = $this->PracticalProject->find('first', array(
			'conditions' => array(			
				'PracticalProject.id' => $project_id,				
			),			
		));
		$receive1s = $this->PracticalReceive->find('first', array(
			'conditions' => array(			
				'PracticalReceive.practical_project_id' => $project_id,	
				'PracticalReceive.practical_term_id' => 1,			
			),			
		));
		$receive2s = $this->PracticalReceive->find('first', array(
			'conditions' => array(			
				'PracticalReceive.practical_project_id' => $project_id,	
				'PracticalReceive.practical_term_id' => 2,			
			),			
		));
	
		// $this->request->data = $receive1s;
		$this->set(array(		
			'receive' => $receive,
			'receive1s' => $receive1s,
			'receive2s' => $receive2s
			
		));
	}
	public function edit_receivestudent($project_id =null,$datestart =null){
		$UserName = $this->Session->read('MisEmployee'); 
		if(isset($UserName)){
			$this->set('UserName', $UserName);
			 
			

			if($this->action == 'login')
				$this->redirect(array('action' => 'Dashboard'));
		}
		else{
			if($this->action != 'login')
				$this->redirect(array('controller' => 'mains','action' => 'login'));
			
		}
		$this->set(array(
			'datestart' => $datestart,
			'project_id' => $project_id,
			'UserName' => $UserName
		));
		$data = array();
		$terms = array(1,2);
		$amount = 0;
		$amount17 = 0;
		$amount1 = 0;
		$amountReg = 0;
		$amountRest17 = 0;
		$amountRestall = 0;
		$amountRestall2 = 0;
		$amountRestall3 = 0;
		if($this->request->data){
			date_default_timezone_set('Asia/Bangkok');
			$checkstudents = $this->Student->find('first', array(
				'conditions' => array(
					'Student.id' => $this->request->data['PracticalStudent']['stu_id'],
									
					)
			));	
			$data = array();
			if ($checkstudents) {
					$receives = $this->PracticalReceive->find('all', array(
						'conditions' => array(
							'PracticalReceive.practical_project_id' => $project_id,
											
							)
					));	
					$lastprojects = $this->PracticalProject->find('first', array(
						'conditions' => array(
							'PracticalProject.id' => $project_id,
																		
						),
						'order' => array('PracticalProject.id' => 'DESC')
					));	
					$data2 = array();
					//*********เช็ควันซ้ำ****** */
						$checkduplicateStudents = $this->PracticalStudent->find('all', array(
							'conditions' => array(								
								'PracticalStudent.student_id' => $checkstudents['Student']['id'],												
							),
							'order' => array('PracticalStudent.practical_project_id,PracticalStudent.practical_term_id' => 'ASC')
						));	
						// debug($checkduplicateStudents);
						foreach ($checkduplicateStudents as $checkStudent) {							
							$checkproject2s = $this->PracticalProject->find('all', array(
								'conditions' => array(
									'PracticalProject.id' => $checkStudent['PracticalStudent']['practical_project_id'],
																				
								),
								'order' => array('PracticalProject.id' => 'ASC')
							));	
						
							foreach ($checkproject2s as $checkproject2 ) {

								if ($lastprojects['PracticalProject']['datestart'] == $checkproject2['PracticalProject']['datestart']) {									
									if ($this->request->data['PracticalStudent']['time_id'] == 1) {
										$receive2s = $this->PracticalReceive->find('first', array(
											'conditions' => array(
												'PracticalReceive.practical_project_id' => $project_id,
												'PracticalReceive.practical_term_id' => 1			
												)
										));
									} elseif ($this->request->data['PracticalStudent']['time_id'] == 2) {
										$receive2s = $this->PracticalReceive->find('first', array(
											'conditions' => array(
												'PracticalReceive.practical_project_id' => $project_id,
												'PracticalReceive.practical_term_id' => 2			
												)
										));
									} elseif ($this->request->data['PracticalStudent']['time_id'] == 3) {
										$receive2s = $this->PracticalReceive->find('first', array(
											'conditions' => array(
												'PracticalReceive.practical_project_id' => $project_id,
												// 'PracticalReceive.practical_term_id' => 2			
												)
										));
									}
									
										
									// debug($checkStudent['PracticalStudent']['practical_term_id']);
									// debug($receive2s['PracticalReceive']['practical_term_id']);
									if ($checkStudent['PracticalStudent']['practical_term_id'] == $receive2s['PracticalReceive']['practical_term_id']) {
										$this->Session->setFlash('ขออภัย ได้เลือกช่วงเวลานี้ไปแล้ว ไม่สามารถลงซ้ำได้  !!!');
										$this->Session->write('alertType','danger');
										$this->redirect(array('action' => 'edit_receivestudent',$project_id,$datestart));
									}

								
								} 
							}
						}
					//********* */
					foreach ($receives as $key) {
						//****************check duplicate */
						$checkduplicateprojects = $this->PracticalProject->find('first', array(
							'conditions' => array(
								'PracticalProject.id' => $project_id,
																			
							),
							'order' => array('PracticalProject.id' => 'DESC')
						));	
						
							
						$checkduplicatereceives = $this->PracticalStudent->find('all', array(
							'conditions' => array(
								'PracticalStudent.practical_receive_id' => $key['PracticalReceive']['id'],
								'PracticalStudent.student_id' => $checkstudents['Student']['id'],												
							),
							'order' => array('PracticalStudent.id' => 'DESC')
						));	
						foreach ($checkduplicatereceives as $check) {
							
							
							if ($check['PracticalStudent']['practical_receive_id'] == $key['PracticalReceive']['id']) {
								$this->Session->setFlash('ขออภัย ได้เลือกลงทะเบียนฝ่ายนี้แล้ว  !!!');
								$this->Session->write('alertType','danger');
								$this->redirect(array('action' => 'edit_receivestudent',$project_id,$datestart));
							}
							
							
						}

						//********เช็คว่าเต็มหรือไม่****** */
						foreach ($terms as $term){
							$receives = $this->PracticalReceive->find('all', array(
								'conditions' => array(
									'PracticalReceive.practical_project_id' => $project_id,
									'PracticalReceive.practical_term_id' => $term,							
									)
							));				
							foreach ($receives as $receive){				
								$amount += $receive['PracticalReceive']['amount_major_id_17'] + $receive['PracticalReceive']['amount_major_id_1'];
								$amount17 += $receive['PracticalReceive']['amount_major_id_17']; //ช่วงเช้า
								$amount1 += $receive['PracticalReceive']['amount_major_id_1']; //ช่วงบ่าย
								
								$receiveId = $receive['PracticalReceive']['id'];
								
							}	
							
						}
							$amountReg = $this->PracticalStudent->find('count', array(
								'conditions' => array(
									'PracticalStudent.practical_project_id' => $project_id,									
								
								)
							));	
							$amountRestall = $amount - $amountReg;
						
						
							if ($this->request->data['PracticalStudent']['time_id'] == 1) {
								$amountReg2 = $this->PracticalStudent->find('count', array(
									'conditions' => array(
										'PracticalStudent.practical_project_id' => $project_id,										
										'PracticalReceive.practical_term_id' => 1,
									)
								));
								$amountRestall2 = $amount17 - $amountReg2;
								if ($amountRestall2 == 0 ) {
									$this->Session->write('alertType','danger');
											$this->Session->setFlash('เต็ม');
											$this->redirect(array('action' => 'edit_receivestudent',$project_id,$datestart));
								} else {
									if ($this->request->data['PracticalStudent']['time_id'] == 1) {
										if ($key['PracticalReceive']['practical_term_id'] == 1) {
											$data = array(
												// 'id' => $key['PracticalReceive']['id'],
												'student_id' => $checkstudents['Student']['id'],
												'practical_receive_id' => $key['PracticalReceive']['id'],
												'practical_project_id' => $key['PracticalReceive']['practical_project_id'],	
												'practical_term_id' => $key['PracticalReceive']['practical_term_id'],
												'exclusive' => 1,							
											);
											$this->PracticalStudent->save($data);
										}
									}
								}
							}    
							if ($this->request->data['PracticalStudent']['time_id'] == 2) {
								$amountReg2 = $this->PracticalStudent->find('count', array(
									'conditions' => array(
										'PracticalStudent.practical_project_id' => $project_id,										
										'PracticalReceive.practical_term_id' => 2,
									)
								));
								$amountRestall3 = $amount1 - $amountReg2;
								if ($amountRestall3 == 0 ) {
									$this->Session->write('alertType','danger');
											$this->Session->setFlash('เต็ม');
											$this->redirect(array('action' => 'edit_receivestudent',$project_id,$datestart));
								} else {
									if ($this->request->data['PracticalStudent']['time_id'] == 2) {
										if ($key['PracticalReceive']['practical_term_id'] == 2) {
											$data = array(
												// 'id' => $key['PracticalReceive']['id'],
												'student_id' => $checkstudents['Student']['id'],
												'practical_receive_id' => $key['PracticalReceive']['id'],
												'practical_project_id' => $key['PracticalReceive']['practical_project_id'],
												'practical_term_id' => $key['PracticalReceive']['practical_term_id'],	
												'exclusive' => 1,							
											);
											$this->PracticalStudent->save($data);
										}
									}
								}
							}  
							if ($this->request->data['PracticalStudent']['time_id'] == 3) {
								$amountReg2 = $this->PracticalStudent->find('count', array(
									'conditions' => array(
										'PracticalStudent.practical_project_id' => $project_id,										
										'PracticalReceive.practical_term_id' => 1,
									)
								));
								$amountRestall2 = $amount17 - $amountReg2;
								if ($amountRestall2 == 0 ) {
									$this->Session->write('alertType','danger');
									$this->Session->setFlash('ขออภัยไม่สามารถลงเวลาปฏิบัติงานได้ กรุณาเลือกรายการใหม่อีกครั้ง');
									$this->redirect(array('action' => 'edit_receivestudent',$project_id,$datestart));
								} else {
									$amountReg2 = $this->PracticalStudent->find('count', array(
										'conditions' => array(
											'PracticalStudent.practical_project_id' => $project_id,										
											'PracticalReceive.practical_term_id' => 2,
										)
									));
									$amountRestall3 = $amount1 - $amountReg2;
									if ($amountRestall3 == 0 ) {
										$this->Session->write('alertType','danger');
										$this->Session->setFlash('ขออภัยไม่สามารถลงเวลาปฏิบัติงานได้ กรุณาเลือกรายการใหม่อีกครั้ง');
										$this->redirect(array('action' => 'edit_receivestudent',$project_id,$datestart));
									} else {
										if ($amountRestall == 0) {
											$this->Session->write('alertType','danger');
											$this->Session->setFlash('เต็ม');
											$this->redirect(array('action' => 'edit_receivestudent',$project_id,$datestart));
										} else {
											if ($this->request->data['PracticalStudent']['time_id'] == 3) {
												$data2 = array(
													// 'id' => $lastid,
													'student_id' => $checkstudents['Student']['id'],
													'practical_receive_id' => $key['PracticalReceive']['id'],
													'practical_project_id' => $key['PracticalReceive']['practical_project_id'],	
													'practical_term_id' => $key['PracticalReceive']['practical_term_id'],
													'exclusive' => 1,							
												);
												array_push($data,$data2);
											}
										}
									}
									
								}
		
							}
						
							

					}
					if ($this->request->data['PracticalStudent']['time_id'] == 3) {
						$this->PracticalStudent->saveMany($data);
					}
				
				
				$this->Session->write('alertType','success');
				$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
				$this->redirect(array('action' => 'edit_receivestudent',$project_id,$datestart));
			} else {
				# code...
			}
			
		}	
		//*********************** */
		$amounts = 0;
		$amountRegs = 0;
		$amountRest17s = 0;
		foreach ($terms as $term){
			//********เช็คว่าเต็มหรือไม่****** */
			$receives = $this->PracticalReceive->find('all', array(
				'conditions' => array(
					'PracticalReceive.practical_project_id' => $project_id,
					'PracticalReceive.practical_term_id' => $term,							
					)
			));				
			foreach ($receives as $receive){				
				$amounts += $receive['PracticalReceive']['amount_major_id_17'] + $receive['PracticalReceive']['amount_major_id_1'];
				$receiveId = $receive['PracticalReceive']['id'];

			}	
			
		}
			$amountRegs = $this->PracticalStudent->find('count', array(
				'conditions' => array(
					'PracticalStudent.practical_project_id' => $project_id,									
				
				)
			));	
	
		//************** */
	
		$amountRest17s = $amounts - $amountRegs;
		$this->set(array(
			'amounts' => $amounts,
			'amountRegs' => $amountRegs,
			'amountRest17s' => $amountRest17s
		));
		$currentStudents = $this->Studentterm->find('all', array(
			'conditions' => array(
				'and' => array(
					array('Student.degree_id <= ' => 3),
					array('Student.degree_id >= ' => 1),
				),
				'term_year' =>  2562,
				'term_id' =>  1,
				// 'Student.degree_id' => 1,
				
			),
			
			'fields' => array('Student.id','Student.student_firstname','Student.student_surname'),
			'order' => array('Studentterm.term_id' => 'DESC'),
		));  
	
			$outputstudents = '';
			$b =0;
			foreach ($currentStudents as $employee) {
				$b++;
				if ($employee['Student']['id']== "809") {
					$outputstudents .= '
					<option value="'.$employee['Student']['id'].'">'.$employee['Student']['student_firstname'].'</option>
				';
				}else {					
				$outputstudents .= '
					<option value="'.$employee['Student']['id'].'">'.$b.'. '.$employee['Student']['id'].' '.$employee['Student']['student_firstname'].' '.$employee['Student']['student_surname'].'</option>
				';
				}

			}
		$receive = $this->PracticalProject->find('first', array(
			'conditions' => array(			
				'PracticalProject.id' => $project_id,				
			),			
		));
		$studentRegs = $this->PracticalStudent->find('all', array(
			'conditions' => array(			
				'PracticalStudent.practical_project_id' => $project_id,				
			),	
			'order' => array('PracticalStudent.practical_term_id' => 'ASC')		
		));
	    //debug($studentRegs);
		// $this->request->data = $receive;
		$this->set(array(		
			'receive' => $receive,
			'studentRegs' => $studentRegs,
			'outputstudents' => $outputstudents
			
		));
	}
	public function delete_receivestudent($id = null,$project_id =null,$datestart =null) {
		$this->request->data = $this->PracticalStudent->findById($id);
		if($this->request->data){

			
				
			
			$this->PracticalStudent->delete($id);

			$this->Session->write('alertType','danger');
			$this->Session->setFlash('ลบข้อมูล เรียบร้อยแล้ว');
			$this->redirect(array('action' => 'edit_receivestudent',$project_id,$datestart));
		}
	}
	public function list_student($project_id = null){
		// $this->layout='';
		$project = $this->PracticalProject->find('first', array(
			'conditions' => array(			
				'PracticalProject.id' => $project_id,				
			),	
			'order' => array('PracticalProject.id' => 'ASC')		
		));
		$studentRegs = $this->PracticalStudent->find('all', array(
			'conditions' => array(			
				'PracticalStudent.practical_project_id' => $project_id,				
			),	
			'order' => array('PracticalStudent.practical_term_id' => 'ASC')		
		));
	    //debug($studentRegs);
		// $this->request->data = $receive;
		$this->set(array(	
			'studentRegs' => $studentRegs,
			'project' => $project,
			'project_id' => $project_id
			
		));
	}
	public function list_print($project_id = null){
		$this->layout='';
		$project = $this->PracticalProject->find('first', array(
			'conditions' => array(			
				'PracticalProject.id' => $project_id,				
			),	
			'order' => array('PracticalProject.id' => 'ASC')		
		));
		$studentRegs = $this->PracticalStudent->find('all', array(
			'conditions' => array(			
				'PracticalStudent.practical_project_id' => $project_id,				
			),	
			'order' => array('PracticalStudent.practical_term_id,PracticalStudent.student_id' => 'ASC')		
		));
	    //debug($studentRegs);
		// $this->request->data = $receive;
		$this->set(array(	
			'studentRegs' => $studentRegs,
			'project' => $project,
			'project_id' => $project_id
			
		));
	}
	public function list_student_nophone($project_id = null){
		// $this->layout='';
		$project = $this->PracticalProject->find('first', array(
			'conditions' => array(			
				'PracticalProject.id' => $project_id,				
			),	
			'order' => array('PracticalProject.id' => 'ASC')		
		));
		$studentRegs = $this->PracticalStudent->find('all', array(
			'conditions' => array(			
				'PracticalStudent.practical_project_id' => $project_id,				
			),	
			'order' => array('PracticalStudent.practical_term_id' => 'ASC')		
		));
	    //debug($studentRegs);
		// $this->request->data = $receive;
		$this->set(array(	
			'studentRegs' => $studentRegs,
			'project' => $project,
			'project_id' => $project_id
			
		));
	}
	//******************************************************** */
	public function project() {
		$projects = $this->PracticalProject->find('all', array(
			'conditions' => array(
				
				'level' => 1
			),
			'order' => array('PracticalProject.id')
		));	
		
		
		$outputProjects = '';
		$i = 1;
		foreach ($projects as $project){
			// $a = $project['PracticalProject']['id'];
			$a = '';
			// $terms = array(1,3,2);
			$terms = array(1,2,3);
			$rowspan = '';
				
			if($project['PracticalProject']['id'] == 1 || $project['PracticalProject']['id'] == 2
			    || $project['PracticalProject']['id'] == 4 || $project['PracticalProject']['id'] == 42
				|| $project['PracticalProject']['id'] == 5 || $project['PracticalProject']['id'] == 6
				|| $project['PracticalProject']['id'] == 7 || $project['PracticalProject']['id'] == 10
				|| $project['PracticalProject']['id'] == 11 || $project['PracticalProject']['id'] == 12
				|| $project['PracticalProject']['id'] == 15 || $project['PracticalProject']['id'] == 16
				|| $project['PracticalProject']['id'] == 17 || $project['PracticalProject']['id'] == 21
				|| $project['PracticalProject']['id'] == 22 || $project['PracticalProject']['id'] == 24
				|| $project['PracticalProject']['id'] == 25 || $project['PracticalProject']['id'] == 26
				|| $project['PracticalProject']['id'] == 31 || $project['PracticalProject']['id'] == 33
				|| $project['PracticalProject']['id'] == 34 || $project['PracticalProject']['id'] == 35
				|| $project['PracticalProject']['id'] == 40 || $project['PracticalProject']['id'] == 43
				|| $project['PracticalProject']['id'] == 44 || $project['PracticalProject']['id'] == 49
				|| $project['PracticalProject']['id'] == 51 || $project['PracticalProject']['id'] == 52
				|| $project['PracticalProject']['id'] == 53 || $project['PracticalProject']['id'] == 59
				|| $project['PracticalProject']['id'] == 60 || $project['PracticalProject']['id'] == 61
				|| $project['PracticalProject']['id'] == 62 || $project['PracticalProject']['id'] == 63
				|| $project['PracticalProject']['id'] == 66 
						){
						// $rowspan = 'rowspan = "3"';	
						$rowspan = 'rowspan = "1"';	
						
						$outputTerms = '';
						$outputTerms2 = '';
						foreach ($terms as $term){
							$receives = $this->PracticalReceive->find('all', array(
								'conditions' => array('PracticalReceive.practical_project_id' => $project['PracticalProject']['id'],
													// 'PracticalReceive.practical_term_id' => $term,													
														
													)
							));				
							$amount = 0;
							$amount17 = 0;
							$amount1 = 0;
							$amount3 = 0;
							$duration = '';
							foreach ($receives as $receive){					
								$amount += $receive['PracticalReceive']['amount'];
								$amount17 += $receive['PracticalReceive']['amount_major_id_17'];
								$amount1 += $receive['PracticalReceive']['amount_major_id_1'];
								$amount3 += $receive['PracticalReceive']['amount_major_id_3'];
								$duration = $receive['PracticalReceive']['duration'];
					}	
					
						if($amount == 0){ $amount = '-'; }
						if($amount17 == 0 ){ $amount17 = '-'; }
					    if($amount1 == 0 ){	$amount1 = '-'; }
						if($amount3 == 0 ){	$amount3 = '-'; }
						
						
					}
							if ($amount == 0) {
								$outputTerms2 .= 
								'<td class="text-center" colspan="2">'.$amount.' </td>
								';
							}else {
								$outputTerms2 .= 
								'<td class="text-center" colspan="2">'.$amount.' </td>
								';
							}	
							$outputTerms2 .= 
							'<td class="text-center" >'.$amount3.'</td>
							';
						
								
						$outputProjects .= 
							'<tr> 
								<td class="text-center" '.$rowspan.'>'.$i.'</td>
								<td '.$rowspan.'>'.$project['PracticalProject']['datestart'].'</td>
								<td '.$rowspan.'>'.$project['PracticalProject']['name'].' '.$a.'</td> 
								<td class="text-center"><b>'.$amount.'</b></td>
								'.$outputTerms2.'
							</tr>';
									
						
									
						$outputTerms2 = '';
						$outputTerms .= 
									'<tr> 
										'.$outputTerms2.'
									</tr> ';
									
									
						$outputProjects .= $outputTerms;
			}else{
				
				$outputTerms = '';
				foreach ($terms as $term){
					$receives = $this->PracticalReceive->find('all', array(
						'conditions' => array(
							'PracticalReceive.practical_project_id' => $project['PracticalProject']['id'],
							// 'PracticalReceive.practical_term_id' => $term,							
							)
					));				
					
					$amount = 0;
					$amount17 = 0;
					$amount1 = 0;
					$amount3 = 0;
					$duration = '';
					$gender = '';
						foreach ($receives as $receive){					
							$amount += $receive['PracticalReceive']['amount'];
							$amount17 += $receive['PracticalReceive']['amount_major_id_17'];
							$amount1 += $receive['PracticalReceive']['amount_major_id_1'];
							$amount3 += $receive['PracticalReceive']['amount_major_id_3'];
							
						}	
						
					
						if($amount == 0){ $amount = 0; }
						if($amount17 == 0 ){	$amount17 = '-'; }
						if($amount1 == 0 ){	$amount1 = '-'; }
						if($amount3 == 0 ){	$amount3 = '-'; }
					
						
					}
					$outputTerms .= 
						'
						<td class="text-center">'.$amount17.'</td>
						<td class="text-center">'.$amount1.'</td>
						<td class="text-center">'.$amount3.'</td>';
				
				if ($project['PracticalProject']['datestart'] == '2022-11-05') {
					$outputProjects .= 
					'<tr class="success"> ';
				}elseif ($project['PracticalProject']['datestart'] == '2022-11-06') {
						$outputProjects .= 
						'<tr class="warning"> ';
				}elseif ($project['PracticalProject']['datestart'] == '2022-11-07') {
						$outputProjects .= 
						'<tr class="danger"> ';
				} else {
					# code...
				}
				
				

				$outputProjects .=  '<td class="text-center" '.$rowspan.'>'.$i.'</td>';
					
					
				$outputProjects .=  '<td '.$rowspan.'>'.$project['PracticalProject']['datestart'].'</td>';

				$outputProjects .=  '<td '.$rowspan.'>'.$project['PracticalProject']['name'].' '.$a.'</td>  
						<td class="text-center"><b>'.($amount17+$amount1+$amount3).'</b></td>
							
						'.$outputTerms.' 
					</tr> ';
			}
			
			$i++;
		}
		
		$this->set(array('outputProjects' => $outputProjects));
	}
	
	public function register() {
		//  $this->layout='';
		$Students = $this->Session->read('Students');
		$this->set(array('Students' => $Students));
		$student_id = $Students['Student']['id'];
		if(!isset($Students) || $Students == null){
				$this->redirect(array('controller' => 'student_mains','action' => 'login'));
		}else {
			
		}
		date_default_timezone_set("Asia/Bangkok");

		
			$dateTimeNow = new DateTime(date("Y-m-d H:i:s"));	
			$dateTimeStart = new DateTime('2022-10-07 12:00:00');	
			$dateTimeEnd = new DateTime('2022-10-16 12:00:00');					
			
			$student = $this->PracticalStudent->find('first', array(
					'conditions' => array(
						'PracticalStudent.student_id' => $Students['Student']['id'],								
					)
				));
			
			if($this->request->data){
				//debug($this->request->data['PracticalStudent']['practical_receive_id']);
				//if(isset($this->request->data['PracticalStudent']['practical_receive_id'])){
					
					$checkSave = true;
					date_default_timezone_set('Asia/Bangkok');

					// foreach ($majorPolls as $key) {
					// 	//*************อัพเดต correct = 2 ทั้งหมด********** */				
					// 	$status = 2;
					// 	$data = array(
					// 	'id' => $key['Majorpoll']['id'],
					// 	'correct' => $status,
					
					// 	);
					// 	$this->Majorpoll->save($data);
					// }
					if($checkSave){
						$data = array();
						$datas = array();
						$a = 0;
						// debug($this->request->data['practical_receive_id']);
							foreach ($this->request->data['practical_receive_id'] as $sourceId){
								$receive = $this->PracticalReceive->find('first', array(
									'conditions' => array(
										'PracticalReceive.id' => $sourceId,
										
									)
								));	
								//debug($receive['PracticalReceive']['practical_project_id']);
								//********************* */
								// $checkStudent = $this->PracticalStudent->find('all', array(
								// 	'conditions' => array(
								// 		'PracticalStudent.practical_receive_id' => $sourceId,
								// 		'PracticalStudent.student_id' => $Students['Student']['id'],	
										
								// 	)
								// ));	
								// foreach ($checkStudent as $key) {
								// 	$checkreceive2 = $this->PracticalReceive->find('first', array(
								// 		'conditions' => array(
								// 			'PracticalReceive.id' => $key['PracticalStudent']['practical_receive_id'],
											
								// 		)
								// 	));	
								// 	debug($key['PracticalStudent']['practical_receive_id']);
                                //     debug($checkreceive2['PracticalReceive']['practical_project_id']);
								// 	if ($receive['PracticalReceive']['practical_project_id'] == $checkreceive2['PracticalReceive']['practical_project_id']) {
								// 		$this->Session->setFlash('ขออภัย ไม่สามารถเลือกวันเดียวกันได้ กรุณาเลือกลงทะเบียนฝ่ายใหม่ !!!');
								// 		$this->Session->write('alertType','danger');
								// 		$this->redirect(array('action' => 'register'));
								// 	}
									
								// }
								//debug($receive);
								//********************* */
								$amountReg = $this->PracticalStudent->find('count', array(
									'conditions' => array(
										'PracticalStudent.practical_receive_id' => $sourceId,
										
									)
								));	
								$checkduplicatereceives = $this->PracticalStudent->find('all', array(
									'conditions' => array(
										'PracticalStudent.practical_receive_id' => $sourceId,
										'PracticalStudent.student_id' => $Students['Student']['id'],												
									)
								));	
								foreach ($checkduplicatereceives as $key) {
									if ($key['PracticalStudent']['practical_receive_id'] == $sourceId) {
										$this->Session->setFlash('ขออภัย ได้เลือกลงทะเบียนฝ่ายนี้แล้ว กรุณาเลือกลงทะเบียนฝ่ายใหม่ !!!');
										$this->Session->write('alertType','danger');
										$this->redirect(array('action' => 'register'));
									} else {
										
									}
									
								}
								
								if($amountReg < $receive['PracticalReceive']['amount']){
									$a = 1;
								}else{
									$this->Session->setFlash('ขออภัย กรุณาเลือกลงทะเบียนฝ่ายใหม่ !!!');
									$this->Session->write('alertType','danger');
									$this->redirect(array('action' => 'register'));
								}
								$data2 = array(								
									'student_id' => $Students['Student']['id'],
									'practical_receive_id' => $sourceId,
								);
								array_push($data,$data2);
								
								
							}	
							if ($a == 1) {
								$this->PracticalStudent->saveMany($data);
							}
							
							$this->Session->setFlash('รหัสนักศึกษา '.$Students['Student']['id'].' เลือกเรียบร้อยแล้ว  ');
							$this->Session->write('alertType','success');
							$this->redirect(array('action' => 'register'));
					}
					
						
						
					// 		if($this->request->data['PracticalStudent']['practical_receive_id'] == 1 || $this->request->data['PracticalStudent']['practical_receive_id'] == 2
					// 			|| $this->request->data['PracticalStudent']['practical_receive_id'] == 4 || $this->request->data['PracticalStudent']['practical_receive_id'] == 42
					// 			|| $this->request->data['PracticalStudent']['practical_receive_id'] == 5 || $this->request->data['PracticalStudent']['practical_receive_id'] == 6
					// 			|| $this->request->data['PracticalStudent']['practical_receive_id'] == 7 || $this->request->data['PracticalStudent']['practical_receive_id'] == 10
					// 			|| $this->request->data['PracticalStudent']['practical_receive_id'] == 11 || $this->request->data['PracticalStudent']['practical_receive_id'] == 12
					// 			|| $this->request->data['PracticalStudent']['practical_receive_id'] == 15 || $this->request->data['PracticalStudent']['practical_receive_id'] == 16
					// 			|| $this->request->data['PracticalStudent']['practical_receive_id'] == 17 || $this->request->data['PracticalStudent']['practical_receive_id'] == 21
					// 			|| $this->request->data['PracticalStudent']['practical_receive_id'] == 22 || $this->request->data['PracticalStudent']['practical_receive_id'] == 24
					// 			|| $this->request->data['PracticalStudent']['practical_receive_id'] == 25 || $this->request->data['PracticalStudent']['practical_receive_id'] == 26
					// 			|| $this->request->data['PracticalStudent']['practical_receive_id'] == 31 || $this->request->data['PracticalStudent']['practical_receive_id'] == 33
					// 			|| $this->request->data['PracticalStudent']['practical_receive_id'] == 34 || $this->request->data['PracticalStudent']['practical_receive_id'] == 35
					// 			|| $this->request->data['PracticalStudent']['practical_receive_id'] == 40 || $this->request->data['PracticalStudent']['practical_receive_id'] == 43
					// 			|| $this->request->data['PracticalStudent']['practical_receive_id'] == 44 || $this->request->data['PracticalStudent']['practical_receive_id'] == 49
					// 			|| $this->request->data['PracticalStudent']['practical_receive_id'] == 51 || $this->request->data['PracticalStudent']['practical_receive_id'] == 52
					// 			|| $this->request->data['PracticalStudent']['practical_receive_id'] == 53 || $this->request->data['PracticalStudent']['practical_receive_id'] == 59
					// 			|| $this->request->data['PracticalStudent']['practical_receive_id'] == 60 || $this->request->data['PracticalStudent']['practical_receive_id'] == 61
					// 			|| $this->request->data['PracticalStudent']['practical_receive_id'] == 62 || $this->request->data['PracticalStudent']['practical_receive_id'] == 63
					// 			|| $this->request->data['PracticalStudent']['practical_receive_id'] == 66 
					// 					){
											
						
					// }else {
						
					// 		// if($amountReg < $receive['PracticalReceive']['amount_major_id_17']){
					// 			if($this->PracticalStudent->save($this->request->data)){
					// 				$this->Session->setFlash('รหัสนักศึกษา '.$student['Student']['id'].' เลือก'.$receive['PracticalProject']['name'].' เรียบร้อยแล้ว <br>นักศึกษาสามารถปรับเปลี่ยนลงทะเบียนฝ่ายได้ จนถึงที่ปิดการลงทะเบียนตามที่กำหนดไว้ ');
					// 				$this->Session->write('alertType','success');
					// 				$this->redirect(array('action' => 'register'));
					// 			}	
								
					// 		// }else{
					// 		// 	$this->Session->setFlash('ลงทะเบียนฝ่ายที่ท่านเลือกซ้ำ กรุณาเลือกลงทะเบียนฝ่ายใหม่++ !!!');
								
					// 		// }
						
					// }	
				
				// }
				// else{
				// 	$this->Session->setFlash('กรุณาเลือกลงทะเบียนฝ่าย !!!');
				// }
				
				
			}else{
				$this->request->data = $this->PracticalStudent->find('first', array(
					'conditions' => array(
						'PracticalStudent.student_id' => $Students['Student']['id'])
				));
			
			}
			// debug($student);	
			// if($student['PracticalStudent']['practical_receive_id'] != 0){
			
			// 	$receive = $this->PracticalReceive->find('first', array(
			// 			'conditions' => array(
			// 				'PracticalReceive.id' => $student['PracticalStudent']['practical_receive_id']
			// 				)
			// 		));
				
			// 	$projectName = $receive['PracticalProject']['name'].' ('.$receive['PracticalTerm']['name'].')';
				
			// 	if($student['PracticalStudent']['practical_receive_id'] == 27 || $student['PracticalStudent']['practical_receive_id'] == 28 || $student['PracticalStudent']['practical_receive_id'] == 29 || $student['PracticalStudent']['practical_receive_id'] == 30){
			// 		$projectName .= ' '.$receive['PracticalReceive']['duration'];
			// 	}
				
			// 	$this->set(array('projectName' => $projectName));
			// }
					
					
				$projects = $this->PracticalProject->find('all', array(
					'conditions' => array(						
						'level' => 1
					),
					'order' => array('id' => 'ASC')
				));				
				$outputProjects = '';
				if($dateTimeNow > $dateTimeStart && $dateTimeNow < $dateTimeEnd){
					$i = 1;
					foreach ($projects as $project){
						$a = $project['PracticalProject']['id'];
						// $terms = array(1,3,2);
						$terms = array(1,2,3);
						$amoutes = array(1,2,3);
						$rowspan = '';	
						if($project['PracticalProject']['id'] == 1 || $project['PracticalProject']['id'] == 2
							|| $project['PracticalProject']['id'] == 4 || $project['PracticalProject']['id'] == 42
							|| $project['PracticalProject']['id'] == 5 || $project['PracticalProject']['id'] == 6
							|| $project['PracticalProject']['id'] == 7 || $project['PracticalProject']['id'] == 10
							|| $project['PracticalProject']['id'] == 11 || $project['PracticalProject']['id'] == 12
							|| $project['PracticalProject']['id'] == 15 || $project['PracticalProject']['id'] == 16
							|| $project['PracticalProject']['id'] == 17 || $project['PracticalProject']['id'] == 21
							|| $project['PracticalProject']['id'] == 22 || $project['PracticalProject']['id'] == 24
							|| $project['PracticalProject']['id'] == 25 || $project['PracticalProject']['id'] == 26
							|| $project['PracticalProject']['id'] == 31 || $project['PracticalProject']['id'] == 33
							|| $project['PracticalProject']['id'] == 34 || $project['PracticalProject']['id'] == 35
							|| $project['PracticalProject']['id'] == 40 || $project['PracticalProject']['id'] == 43
							|| $project['PracticalProject']['id'] == 44 || $project['PracticalProject']['id'] == 49
							|| $project['PracticalProject']['id'] == 51 || $project['PracticalProject']['id'] == 52
							|| $project['PracticalProject']['id'] == 53 || $project['PracticalProject']['id'] == 59
							|| $project['PracticalProject']['id'] == 60 || $project['PracticalProject']['id'] == 61
							|| $project['PracticalProject']['id'] == 62 || $project['PracticalProject']['id'] == 63
							|| $project['PracticalProject']['id'] == 66 
									){
							// $rowspan = 'rowspan = "3"';	
							$rowspan = 'rowspan = "1"';	
								
							$outputTerms = '';
							$outputTerms2 = '';
							foreach ($terms as $term){
								$receives = $this->PracticalReceive->find('all', array(
									'conditions' => array(
										'PracticalReceive.practical_project_id' => $project['PracticalProject']['id'],
										// 'PracticalReceive.practical_term_id' => $term,													
									)
								));				
								
								$amount = 0;
								$amount17 = 0;
								$amount1 = 0;
								$amount3 = 0;
								$duration = '';
								$receiveId = null;
								foreach ($receives as $receive){					
									$amount += $receive['PracticalReceive']['amount'];
									$amount17 += $receive['PracticalReceive']['amount_major_id_17'];
									$amount1 += $receive['PracticalReceive']['amount_major_id_1'];
									$amount3 += $receive['PracticalReceive']['amount_major_id_3'];
									$duration = $receive['PracticalReceive']['duration'];
									$receiveId = $receive['PracticalReceive']['id'];
								}
								
								$amountReg = $this->PracticalStudent->find('count', array(
									'conditions' => array(
										'PracticalStudent.practical_receive_id' => $receiveId,										
										)
								));	
								
								$amountRest = $amount - $amountReg;
								$amountRestInput = '';
									if($amountRest > 0){
										$amountRestInput = '
										<input type="checkbox" name="data[practical_receive_id]['.$receiveId.']" value="'.$receiveId.'"> '.$amountRest.'<br>';
										// <input type="checkbox" name="data[correct_id][4365]"  value="4365" id="checkItem" data-error="กรุณากรอกข้อมูล" class="largerCheckbox2"/>
									}
	
									
							}
							
							// if ($amount == 0) {
							// 	$outputTerms .= 
							// 	'<td class="text-center" colspan="2">'.$amountRestInput.' </td>
							// 	';
							// }else {
							// 	$outputTerms .= 
							// 	'<td class="text-center" colspan="2">'.$amountRestInput.' (ไม่จำกัดสาขา)</td>
							// 	';
							// }
							if ($amount == 0) {
								$outputTerms2 .= 
								'<td class="text-center" colspan="2">'.$amountRestInput.' </td>
								';
							}else {
								$outputTerms2 .= 
								'<td class="text-center" colspan="2">'.$amountRestInput.' </td>
								';
							}	
							$outputTerms2 .= 
							'<td class="text-center" > -</td>
							';	
							$outputProjects .= 
								'<tr> 
									<td class="text-center" '.$rowspan.'>'.$i.'</td>
									<td '.$rowspan.'>'.$project['PracticalProject']['datestart'].'</td>
									<td '.$rowspan.'>'.$project['PracticalProject']['name'].' '.$a.'</td> 
									<td class="text-center"><b>'.$amount.'</b></td> 
									'.$outputTerms2.'
								</tr>';
										
							
										
							$outputTerms2 = '';
							$outputTerms .= 
										'<tr> 
											'.$outputTerms2.'
										</tr> ';
										
										
							$outputProjects .= $outputTerms;
						}else{
							
							$outputTerms = '';
							foreach ($terms as $term){
								$receiveId = array();
								$amount = 0;
								$amount17 = 0;
								$amount1 = 0;
								$amount3 = 0;
								$gender = '';
									$receives = $this->PracticalReceive->find('all', array(
										'conditions' => array(
											'PracticalReceive.practical_project_id' => $project['PracticalProject']['id'],
											'PracticalReceive.practical_term_id' => $term,											
																
										)
									));				
									$receive2s = $this->PracticalReceive->find('all', array(
										'conditions' => array(
											'PracticalReceive.practical_project_id' => $project['PracticalProject']['id'],
																					
																
										)
									));	
									foreach ($receive2s as $receive){				
									
										$amount17 += $receive['PracticalReceive']['amount_major_id_17'];
										$amount1 += $receive['PracticalReceive']['amount_major_id_1'];		
										$amount3 += $receive['PracticalReceive']['amount_major_id_3'];								
									
										
									}	
								
									foreach ($receives as $receive){					
										$amount += $receive['PracticalReceive']['amount'];																		
										$receiveId = $receive['PracticalReceive']['id'];
										
									}	
									
									$amountReg = $this->PracticalStudent->find('count', array(
										'conditions' => array(
											'PracticalStudent.practical_receive_id' => $receiveId,									
										
										)
									));	
									$amountRest17 = $amount - $amountReg;
									$amountRestInput17 = '';
									if($amountRest17 > 0){
										$amountRestInput17 = '<input type="checkbox" name="data[practical_receive_id]['.$receiveId.']" value="'.$receiveId.'"> '.$amountRest17.'<br>';
									}
									

									$outputTerms .= 
										'<td class="text-center">'.$amountRestInput17.'</td>';

									
									//debug($receiveId[0]);
									
										
								
								
							}
							
							
							$outputProjects .= 
								'<tr> 
									<td class="text-center" '.$rowspan.'>'.$i.'</td>							
									<td '.$rowspan.'>'.$project['PracticalProject']['datestart'].'</td>
									<td '.$rowspan.'>'.$project['PracticalProject']['name'].' '.$a.'</td> 
									<td class="text-center"><b>'.($amount17+$amount1+$amount3).'</b></td>
									'.$outputTerms.' 
								</tr> ';
							}
							
							$i++;
					}
				}else {
					$this->Session->setFlash('รอวันเปิดระบบ !!!');
					
				}
				$this->set(array('outputProjects' => $outputProjects));

			
		
	}
	
	public function checklist($receiveId = null) {
		if(isset($receiveId)){
			
			$receive = $this->PracticalReceive->find('first', array(
						'conditions' => array(
							'PracticalReceive.id' => $receiveId,
												
						)
					));	
					
				
			if(count($receive) > 0){
				
				$projectName = $receive['PracticalProject']['name'].' ('.$receive['PracticalTerm']['name'].')';
				
				// if($receiveId == 27 || $receiveId == 28 || $receiveId == 29 || $receiveId == 30){
				// 	$projectName .= '<br>'.$receive['Receive']['duration'];
				// }
			
				
				

				$studentRegs = $this->PracticalStudent->find('all', array(
							'conditions' => array(
								'PracticalStudent.practical_receive_id' => $receiveId,
								// 'PracticalStudent.year_education_id' => $this->yearEducation['YearEducation']['id']
								),
							'order' => array('PracticalStudent.student_id')
						));
				
						
				if(count($studentRegs) > 0){
					$this->set(array('studentRegs' => $studentRegs,'projectName' => $projectName));				
				}
				else{
					$this->redirect(array('action' => 'checklist'));				
				}
			}		
			else{				
				$this->redirect(array('action' => 'checklist'));				
			}		
			
		}
		else{
			
			$projects = $this->PracticalProject->find('all', array(
				'conditions' => array(
					// 'PracticalProject.year_education_id' => $this->yearEducation['YearEducation']['id']
					)
			));				
			
			$outputProjects = '';
			$i = 1;
			
			$terms = array(1,2,3);
			$totalTerm = array();
			foreach ($terms as $term){
				$totalTerm[$term] = 0;
			}
			
			foreach ($projects as $project){
			    $a = $project['PracticalProject']['id'];
				$terms = array(1,2,3);
				$rowspan = '';	
				if($project['PracticalProject']['id'] == 1 || $project['PracticalProject']['id'] == 2
			    || $project['PracticalProject']['id'] == 4 || $project['PracticalProject']['id'] == 42
				|| $project['PracticalProject']['id'] == 5 || $project['PracticalProject']['id'] == 6
				|| $project['PracticalProject']['id'] == 7 || $project['PracticalProject']['id'] == 10
				|| $project['PracticalProject']['id'] == 11 || $project['PracticalProject']['id'] == 12
				|| $project['PracticalProject']['id'] == 15 || $project['PracticalProject']['id'] == 16
				|| $project['PracticalProject']['id'] == 17 || $project['PracticalProject']['id'] == 21
				|| $project['PracticalProject']['id'] == 22 || $project['PracticalProject']['id'] == 24
				|| $project['PracticalProject']['id'] == 25 || $project['PracticalProject']['id'] == 26
				|| $project['PracticalProject']['id'] == 31 || $project['PracticalProject']['id'] == 33
				|| $project['PracticalProject']['id'] == 34 || $project['PracticalProject']['id'] == 35
				|| $project['PracticalProject']['id'] == 40 || $project['PracticalProject']['id'] == 43
				|| $project['PracticalProject']['id'] == 44 || $project['PracticalProject']['id'] == 49
				|| $project['PracticalProject']['id'] == 51 || $project['PracticalProject']['id'] == 52
				|| $project['PracticalProject']['id'] == 53 || $project['PracticalProject']['id'] == 59
				|| $project['PracticalProject']['id'] == 60 || $project['PracticalProject']['id'] == 61
				|| $project['PracticalProject']['id'] == 62 || $project['PracticalProject']['id'] == 63
				|| $project['PracticalProject']['id'] == 66 
					){
						// $rowspan = 'rowspan = "3"';	
						$rowspan = 'rowspan = "1"';	
						
						$outputTerms = '';
						$outputTerms2 = '';
						foreach ($terms as $term){
							$receives = $this->PracticalReceive->find('all', array(
								'conditions' => array(
									'PracticalReceive.practical_project_id' => $project['PracticalProject']['id'],
									// 'PracticalReceive.practical_term_id' => $term,													
														
								)
							));				
							$amount = 0;
							$amount17 = 0;
							$amount1 = 0;
							$amount3 = 0;
							$duration = '';
							foreach ($receives as $receive){					
									$amount += $receive['PracticalReceive']['amount'];
									$amount17 += $receive['PracticalReceive']['amount_major_id_17'];
									$amount1 += $receive['PracticalReceive']['amount_major_id_1'];
									$amount3 += $receive['PracticalReceive']['amount_major_id_3'];
									$receiveId = $receive['PracticalReceive']['id'];
							}	
							
							if($amount == 0){ $amount = '-'; }
							if($amount17 == 0 ){ $amount17 = '-'; }
							if($amount1 == 0 ){	$amount1 = '-'; }
							if($amount3 == 0 ){	$amount3 = '-'; }
							
							$amountReg = $this->PracticalStudent->find('count', array(
								'conditions' => array(
									'PracticalStudent.practical_receive_id' => $receiveId,
									// 'PracticalStudent.year_education_id' => $this->yearEducation['YearEducation']['id']
								)
							));	
							
							//debug($receiveId[0]);
							$link = '';
							if($amountReg > 0){
								$link = '<a href="checklist/'.$receiveId.'" target="_blank">'.$amountReg.'</a><br>';
								
								$totalTerm[$term] += $amountReg;
							}
							
	
							
						}
						// $outputTerms2 .= 
						// 	'<td class="text-center">'.$link.'</td>';
							if ($amount == 0) {
								$outputTerms2 .= 
								'<td class="text-center" colspan="2">'.$link.' </td>
								';
							}else {
								$outputTerms2 .= 
								'<td class="text-center" colspan="2">'.$link.' </td>
								';
							}	
							$outputTerms2 .= 
							'<td class="text-center" > -</td>
							';	
						
									
						$outputProjects .= 
							'<tr> 
								<td class="text-center" '.$rowspan.'>'.$i.'</td>
								<td '.$rowspan.'>'.$project['PracticalProject']['datestart'].'</td>
								<td '.$rowspan.'>'.$project['PracticalProject']['name'].' '.$a.'</td> 
								<td class="text-center"><b>'.$amount.'</b></td>
								'.$outputTerms2.'
							</tr>';
									
						
									
						$outputTerms2 = '';
						$outputTerms .= 
									'<tr> 
										'.$outputTerms2.'
									</tr> ';
									
									
						$outputProjects .= $outputTerms;
			    }else{
					
					$outputTerms = '';
					foreach ($terms as $term){
						$receiveId = array();
								$amount = 0;
								$amount17 = 0;
								$amount1 = 0;
								$amount3 = 0;
								$gender = '';
						$receives = $this->PracticalReceive->find('all', array(
							'conditions' => array('PracticalReceive.practical_project_id' => $project['PracticalProject']['id'],
													'PracticalReceive.practical_term_id' => $term,
													// 'Receive.year_education_id' => $this->yearEducation['YearEducation']['id']
													)
						));				
						
						$receive2s = $this->PracticalReceive->find('all', array(
							'conditions' => array(
								'PracticalReceive.practical_project_id' => $project['PracticalProject']['id'],
																		
													
							)
						));	
						foreach ($receive2s as $receive){				
						
							$amount17 += $receive['PracticalReceive']['amount_major_id_17'];
							$amount1 += $receive['PracticalReceive']['amount_major_id_1'];		
							$amount3 += $receive['PracticalReceive']['amount_major_id_3'];								
						
							
						}	
						// if($project['PracticalProject']['id'] == 23 || $project['PracticalProject']['id'] == 24){
							// $receiveId = array();
						// }
							
						$receiveId = array();
						$gender = '';
						foreach ($receives as $receive){											
							
							// if($project['PracticalProject']['id'] == 23 || $project['PracticalProject']['id'] == 24){

								// array_push($receiveId,$receive['Receive']['id']);
							// }
							// else{
								$receiveId = $receive['PracticalReceive']['id'];
							//}
						}	
						
						$amountReg = $this->PracticalStudent->find('count', array(
							'conditions' => array('PracticalStudent.practical_receive_id' => $receiveId,
													// 'PracticalStudent.year_education_id' => $this->yearEducation['YearEducation']['id']
													)
						));	
						
						//debug($receiveId[0]);
					
							
						$link = '';
						if($amountReg > 0){
							$link = '<a href="checklist/'.$receiveId.'" target="_blank">'.$amountReg.'</a><br>'.$gender;
							
							$totalTerm[$term] += $amountReg;
						}
						

						$outputTerms .= 
							'<td class="text-center">'.$link.'</td>';
							
					}
					
					
					$outputProjects .= 
						'<tr> 
							<td class="text-center" '.$rowspan.'>'.$i.'</td>	
							<td '.$rowspan.'>'.$project['PracticalProject']['datestart'].'</td>						
							<td '.$rowspan.'>'.$project['PracticalProject']['name'].' '.$a.'</td> 
							<td class="text-center"><b>'.($amount17+$amount1+$amount3).'</b></td>
							'.$outputTerms.' 
						</tr> ';
				}
				
				$i++;
			}
			
			
			$terms = array(1,2,3);
			$outputTotalTerm = '';
			foreach ($terms as $term){
				$outputTotalTerm .= '<td class="text-center">'.$totalTerm[$term].'</td>';
			}
			
			
			$outputProjects .= 
				'<tr> 
					<td class="text-center" colspan="2">รวม</td>								
					'.$outputTotalTerm.' 
				</tr> ';
			
			$this->set(array('outputProjects' => $outputProjects));
		}
		
	}
	public function duration(){
		$this->layout = '';
		// $Students = $this->Session->read('Students');
		
		// $student_id = $Students['Student']['id'];
		// if(!isset($Students) || $Students == null){
		// 		$this->redirect(array('controller' => 'student_mains','action' => 'login'));
		// }else {
			
		// }
		// date_default_timezone_set("Asia/Bangkok");

		// if($this->request->data){	
		// 	$this->request->data['PracticalLog']['student_id'] = $Students['Student']['id'];
		// 	$this->request->data['PracticalLog']['accept'] = 1;			
		// 	$this->PracticalLog->save($this->request->data);
		// 	$this->redirect(array('action' => 'register'));
			
		// }
	}
	public function checklist_all() {
		$students = $this->PracticalStudent->find('all', array(
							'conditions' => array('PracticalStudent.year_education_id' => $this->yearEducation['Yearterm']['year']
													),
							'order' => array('PracticalStudent.practical_receive_id,PracticalStudent.student_id' => 'asc')
						));
						
		$i = 1;
		$outputStudents = '';
		foreach ($students as $student){
		
			$projectSelect = '<font color="red">ยังไม่ได้ลงชื่อเลือกลงทะเบียนฝ่าย</font>';
			if($student['PracticalStudent']['practical_receive_id'] != 0){
				$receive = $this->PracticalReceive->find('first', array(
						'conditions' => array('PracticalReceive.id' => $student['PracticalStudent']['practical_receive_id'],
												'PracticalReceive.year_education_id' => $this->yearEducation['Yearterm']['year']
												)
					));	
					
				$projectSelect = $receive['PracticalProject']['name'].' ('.$receive['PracticalTerm']['name'].')';
				
				if($student['PracticalStudent']['practical_receive_id'] == 27 || $student['PracticalStudent']['practical_receive_id'] == 28){
					$projectSelect .= '<br>'.$receive['PracticalReceive']['duration'];
				}
			}
		
			$outputStudents .= 
				'<tr> 
					<td class="text-center">'.$i.'</td> 
					<td class="text-center">'.$student['Student']['id'].'</td> 
					<td>'.$student['Student']['student_title'].$student['Student']['student_firstname']." ".$student['Student']['student_surname'].'</td> 
					<td>'.$projectSelect.'</td> 
					<td class="text-center">'.$student['PracticalStudent']['modified'].'</td> 															
				</tr>';
			
		
			$i++;
		}
						
		$this->set(array('outputStudents' => $outputStudents));
	}
	
}