<?php
set_time_limit(30000);
App::uses('CakeEmail', 'Network/Email');
ini_set('memory_limit', '512M');
class SpecialAndCooperativeController extends AppController {
    private	$title_for_layout = 'e-Special / Cooperative Management System, Faculty of Agriculture , CMU';
	private	$keywords = 'e-Special / Cooperative Management System, Faculty of Agriculture, CMU';
	private	$keywordsTh = 'ระบบจัดเก็บปัญหาพิเศษและสหกิจศึกษา คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่';
	private	$description = 'ระบบจัดเก็บปัญหาพิเศษและสหกิจศึกษา คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่';
	private $preEngId;
	private $studentId;
	public $opendatesystem = '2020-07-06 00:00:00';
	public $employee1 = 584 ; // ผศ.ดร. ปิยะวรรณ สุทธิประพันธ์ ประธานกรรมการบัณฑิตประจำคณะ
	public $employee2 = 584 ; // รองคณบดีฝ่ายวิชาการ พัฒนาคุณภาพนักศึกษาและนักศึกษาเก่าสัมพันธ์
	public $employee_form_21 = 324; // ผศ.ดร. ณัฐศักดิ์ กฤติกาเมษ กก. บัณฑิต /ปธ. หลักสูตร สาขาเทคโนโลยีหลังการเก็บเกี่ยว
	public $employee_form_22 = 625; // ชูชาติ สันธทรัพย์ กก. บัณฑิต /ปธ. หลักสูตร สาขา ปฐพีศาสตร์สิ่งแวดล้อม
	public $employee_form_2 = 553; //อ. ชาญชัย แสงชโยสวัสดิ์ กก. บัณฑิต/ ปธ. หลักสูตร การจัดการระบบบเกษตร
	public $employee_form_20_4 = 715; //เยาวเรศ เชาวนพูนผล	 กก. บัณฑิต /ปธ. หลักสูตร สาขาธุรกิจเกษตร
	public $employee_form_20_10_3 = 308; //จุฑาทิพย์ เฉลิมผล	กก. บัณฑิต/ ปธ. หลักสูตร ส่งเสริมการเกษตรและพัฒนาชนบท ป โท
	public $employee_form_20_10_5 = 669; //ผศ.ดร. บุศรา ลิ้มนิรันดร์กุล	กก. บัณฑิต/ ปธ. หลักสูตร ส่งเสริมการเกษตรและพัฒนาชนบท ป เอก
	public $dean_id = 65; // ผศ.ดร. ดรุณี นาพรหม คณบดีคณะเกษตรศาสตร์
	private $urls = '/smart_academic';
	public $layout = 'specialandcooperatives';
	public $paginate = array(
		'limit' => 10,
		'order' => array(
			'id' => 'DESC'
		)
	);
	
	public $uses = array('Student','Major','Degree','Studentgroup' ,'Admission','Banner',
							'Studentstatus','Studentgrade','Graduateaward',
							'Award','Studentcert','Research',
							'Researchstatus','Researchdatabasetype','Researchdatabase','Researchfrequencytype',
							'Frequencyresearch','Researchtype','Research' ,'Researchtype','Researchpurpose','Publishtype',
							'Studentterm','Advisor' ,'Majordegree', 'Yearterm','Question','Payment',							 
							'Employee','Prefix','Organize','Approve','PreEng','Proposal','Coursegroup','MisEmployee','MisPosition','MisOrganize', 
							'LogProposal','LogChangeProposal','CommitteeMeetingChangeProposal','Infopage','InfoDocumentpage','Grade',
							'Employee2','PreCompre','Admincurriculumrequest','SpecialAndCooperative','SpecialAndCooperativeDocument','AnswerSpecialAndCooperative',
							'AnswerSpecialAndCooperativeDocument','SpecialAndCooperativeFollowStatus','LogSpecialAndCooperative','SpecialAndCooperativePass','SpecialAndCooperativeType',
							'LogTimeStampSpecialAndCooperative','SpecialAndCooperativePassDocument','SpecialAndCooperativeEmployee'		
						);		
	
	public $components = array('Highcharts.Highcharts');

	public $Highcharts = null;
	
	public function DateDiff($strDate1,$strDate2){                             
		return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
	}
	public function DateThai($strDate){
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//, $strHour:$strMinute
	}
	public function beforeFilter() {
		$this->set('title_for_layout', $this->title_for_layout);
		$this->set('keywords', $this->keywords);
		$this->set('keywordsTh', $this->keywordsTh);
		$this->set('description', $this->description);

		date_default_timezone_set('Asia/Bangkok');
		$date = date("Y-m-d");
		$time = date("H:i:s");

		$UserName = $this->Session->read('MisEmployee');
		$misPositions = $this->MisPosition->find('first' , array(
			'conditions' => array(
				'MisPosition.id' => $UserName['MisEmployee']['mis_position_id'],	
			),				
			'order' => array('MisPosition.id' => 'DESC')
		));
		$this->set(array(
			'misPositions' => $misPositions, 
		));		
		$admins = $this->Session->read('adminCurriculumRequests');
		if(isset($UserName)){
			if(isset($admins)){
				// $this->redirect(array('controller' => 'proposals','action' => 'login'));
				if ($admins['Admincurriculumrequest']['employee_status_id'] == 3) {
					$countAllSpecialAndCooperatives1  = $this->SpecialAndCooperative->find('count',array(
						'conditions' => array(							
								// 'and' => array(
								// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 9),
								// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 4)
								// ),
								'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
								'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,	
								'SpecialAndCooperative.degree_id' => 1	
							),
							'order' => array('Proposal.modified' => 'DESC')
						)); 
					$countAllSpecialAndCooperatives3  = $this->SpecialAndCooperative->find('count',array(
							'conditions' => array(							
									// 'and' => array(
									// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 9),
									// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 4)
									// ),
									'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
									'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,	
									'SpecialAndCooperative.degree_id' => 3		
									// 'and' => array(
									// 	array('SpecialAndCooperative.degree_id <= ' => 5),
									// 	array('SpecialAndCooperative.degree_id >= ' => 3)
									// ),
								),
								'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,
								'order' => array('Proposal.modified' => 'DESC')
							)); 
					$countAllSpecialAndCooperatives5  = $this->SpecialAndCooperative->find('count',array(
						'conditions' => array(							
								// 'and' => array(
								// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 9),
								// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 4)
								// ),
								'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
								'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,	
								'SpecialAndCooperative.degree_id' => 5
								// 'and' => array(
								// 	array('SpecialAndCooperative.degree_id <= ' => 5),
								// 	array('SpecialAndCooperative.degree_id >= ' => 3)
								// ),
							),
							'order' => array('Proposal.modified' => 'DESC')
						)); 
						$this->set(array(						 
							'countAllSpecialAndCooperatives1' => $countAllSpecialAndCooperatives1 ,
							'countAllSpecialAndCooperatives3' => $countAllSpecialAndCooperatives3 ,
							'countAllSpecialAndCooperatives5' => $countAllSpecialAndCooperatives5 
						));
				}elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 1) {
				
				 
					$countAllSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
						'conditions' => array(
							'and' => array(
								array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 9),
								array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 4)
							),
							 
							'SpecialAndCooperative.degree_id' => 1,
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					));
					$countAllSpecialAndCooperatives3 = $this->SpecialAndCooperative->find('count', array(
						'conditions' => array(
							'and' => array(
								array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 9),
								array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 4)
							),
						 
							'SpecialAndCooperative.degree_id' => 3,
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					));
					$countAllSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
						'conditions' => array(
							'and' => array(
								array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 9),
								array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 4)
							),
						 
							'SpecialAndCooperative.degree_id' => 5,
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					));
					 
					$this->set(array(		
								  
						'countAllSpecialAndCooperatives1' => $countAllSpecialAndCooperatives1,
						'countAllSpecialAndCooperatives3' => $countAllSpecialAndCooperatives3,
						'countAllSpecialAndCooperatives5' => $countAllSpecialAndCooperatives5
					));
						 
					
				}elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 2) {
					$countAllSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
						'conditions' => array(
							'and' => array(
								array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
								array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
							),
							'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
							'SpecialAndCooperative.degree_id' => 1,
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					));
					$countAllSpecialAndCooperatives3 = $this->SpecialAndCooperative->find('count', array(
						'conditions' => array(
							'and' => array(
								array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
								array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
							),
							'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
							'SpecialAndCooperative.degree_id' => 3,
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					));
					$countAllSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
						'conditions' => array(
							'and' => array(
								array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
								array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
							),
							'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
							'SpecialAndCooperative.degree_id' => 5,
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					));
					 
					$this->set(array(		
								  
						'countAllSpecialAndCooperatives1' => $countAllSpecialAndCooperatives1,
						'countAllSpecialAndCooperatives3' => $countAllSpecialAndCooperatives3,
						'countAllSpecialAndCooperatives5' => $countAllSpecialAndCooperatives5
					));
				}
				
			}
		}else{
			
		}
		$lastYearTerms = $this->Yearterm->find('first' , array(
			'conditions' => array(
				'Yearterm.level' => 1,
			),				
			'order' => array('id' => 'DESC')
		));
		$this->Session->write('lastYearTerms', $lastYearTerms);
		
	}
	public function loginstudent() {		
		// $this->layout = 'login';		
		//if($this->request->is('post')){
			$students = $this->Session->read('students'); // from main session
			$Students = $this->Student->find('first', array(
				'conditions' => array(
					'Student.id' => $students['Student']['id'], 
					'Student.student_idcardno' => $students['Student']['student_idcardno'], 
					)
			));	
			$lastYearterms = $this->Yearterm->find('first' , array(	
				'conditions' => array(
					'Yearterm.level' => 1,
				),			
				'order' => array('id' => 'DESC')
			));		
			$lastStudentTerm = $this->Studentterm->find('first',array(
				'conditions' => array(
					'Studentterm.term_id' => $lastYearterms['Yearterm']['term'],
					'Studentterm.term_year' => $lastYearterms['Yearterm']['year'],
					'Studentterm.student_id' => $Students['Student']['id'],
					'Studentstatus.student_status_level' => 1,			
				),
				'fields'=> array('Studentterm.studentstatus_id'),						

			));
			if ($lastStudentTerm == null) {
				$this->Session->setFlash('ไม่มีสิทธิ์เข้าถึงส่วนนี้');
				$this->Session->write('alertType','danger');
				$this->redirect(array('controller' => 'proposals','action' => 'index'));
			}else{	
				$studentstatuses = $this->Studentstatus->find('first' , array(
					'conditions' => array(
						'Studentstatus.id' => $lastStudentTerm['Studentterm']['studentstatus_id'],			
					),
					'fields'=> array('Studentstatus.student_status_level'),					
					'order' => array('Studentstatus.id' => 'DESC')
				));
				if ($studentstatuses['Studentstatus']['student_status_level'] == 2 || $studentstatuses['Studentstatus']['student_status_level'] == 3) {
					$this->Session->setFlash('ไม่มีสิทธิ์เข้าถึงส่วนนี้ ระบบนี้สำหรับนักศึกษาที่กำลังศึกษาเท่านั้น');
					$this->Session->write('alertType','danger');
					$this->redirect(array('controller' => 'proposals','action' => 'index'));
				}else {						
					if (count($Students) > 0) {				
						if ($Students['Student']['degree_id'] == 3 || $Students['Student']['degree_id'] == 5  ) {
							$this->Session->write('Students', $Students);
							$this->Session->write('id',true);				
							$this->Session->setFlash('ยินดีต้อนรับ '.$Students['Student']['student_title'].''.$Students['Student']['student_firstname'].' '.$Students['Student']['student_surname'].' เข้าสู่ระบบเรียบร้อยแล้ว');
							$this->Session->write('alertType','success');
							$this->redirect(array('controller' => 'proposals','action' => 'list_eng_all'));									
							
						}else {
							$this->Session->setFlash('ไม่มีสิทธิ์เข้าถึงส่วนนี้ ระบบนี้สำหรับนักศึกษาระดับบัณฑิตศึกษาเท่านั้น');
							$this->Session->write('alertType','danger');
						}
						
					}else{
						$this->Session->setFlash('รหัสนักศึกษา หรือ เลขประจำตัวบัตรประชาชน ไม่ถูกต้อง!!!');
						$this->Session->write('alertType','danger');
					}
				}


			}

		//}
	}
	public function login($value=null) {
		$date = date("Y-m-d");
		$time = date("H:i:s");
		if((((strtotime($this->opendatesystem)  -    strtotime($date.' '.$time))/  ( 60 * 60 * 24 )) > 0) ) {
			$this->Session->setFlash('ขออภัย ทำการปิดระบบชั่วคราวเพื่อปรับข้อมูลนักศึกษาจากสำนักทะเบียนให้เป็นปัจจุบัน');
			$this->Session->write('alertType','danger');
			$this->redirect(array('controller' => 'mains','action' => 'dashboard'));
		}else {		
			$this->layout = 'login';
			if ($value == null) {
				$value = null;
			}elseif ($value == 1) {
				$value = 1;
			
			}elseif ($value == 2) {
				$value = 2;
			
			} 		
			$UserName = $this->Session->read('MisEmployee');		
			$admins = $this->Session->read('adminCurriculumRequests');
			$id = $this->Session->read('id');
			if(isset($id)){
				if ($value == 1) {	
					$this->redirect(array('controller' => 'proposals','action' => 'list_result_manage_eng_all'));
							
				}elseif ($value == 2) {
					if($admins['Admincurriculumrequest']['employee_status_id'] == 1){
						$this->redirect(array('controller' => 'proposals','action' => 'list_result_manage_proposal'));
					}elseif($admins['Admincurriculumrequest']['employee_status_id'] == 2){
						// $this->redirect(array('controller' => 'proposals','action' => 'list_proposal_all'));
						$this->redirect(array('action' => 'dashboard'));
					}elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 3) {
										
						$this->redirect(array('action' => 'dashboard'));
							
						
					}
					
				} 		
			}else {	}
			if(isset($UserName)){			
				$method = 'get';		
			}else {
				$method = 'post';		
			}

			if($this->request->is($method)){		
				if(isset($UserName)){
				
					// $UserName = $this->Employee->find('first', array(
					// 	'conditions' => array(
					// 		'UserName' => $UserName['Employee']['UserName'],
					// 		'Password' => $UserName['Employee']['Password']
					// 		)
					// ));	
					
				}else {
					$UserName = $this->Employee->find('first', array(
						'conditions' => array(
							'UserName' => $this->request->data['UserName'],
							'Password' => $this->request->data['Password'])
					));		
					
				}
				
					
				
			
				if (count($UserName) > 0) {

					$admins = $this->Admincurriculumrequest->find('first', array(
									'conditions' => array('mis_employee_id' => $UserName['MisEmployee']['id']
						)
					));
					$lastMisEmployees = $this->MisEmployee->find('first', array(
						'conditions' => array(
							// 'MisEmployee.id_card' => $UserName['Employee']['idx'],
							'MisEmployee.id' => $UserName['MisEmployee']['id'],
						)
					));
					 
					$this->request->data['LogTimeStampSpecialAndCooperative']['mis_employee_id'] = $UserName['MisEmployee']['id'];	
					// $this->request->data['LogTimeStampSpecialAndCooperative']['employee_id'] = $UserName['MisEmployee']['id'];	
					$this->request->data['LogTimeStampSpecialAndCooperative']['ip'] = $this->request->clientIp();				 
					$this->LogTimeStampSpecialAndCooperative->save($this->request->data);
					 
											
					if ($admins['Admincurriculumrequest']['employee_status_id'] == 1) {
						$this->Session->write('UserName', $UserName);				
						$this->Session->write('adminCurriculumRequests', $admins);
						$this->Session->write('id',true);
						$this->redirect(array('action' => 'Dashboard'));				
					}elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 2) {
						$this->Session->write('UserName', $UserName);				
						$this->Session->write('adminCurriculumRequests', $admins);
						$this->Session->write('id',true);	
						$this->redirect(array('action' => 'Dashboard'));
					}elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 3) {
							$this->Session->write('UserName', $UserName);				
							$this->Session->write('adminCurriculumRequests', $admins);
							$this->Session->write('id',true);
							$this->redirect(array('action' => 'dashboard'));
							
						
					}else {
						$this->Session->write('UserName', $UserName);				
						$this->Session->write('adminCurriculumRequests', $admins);
						$this->Session->write('id',true);
						$this->redirect(array('action' => 'Dashboard'));
						$this->Session->setFlash('ทำการล๊อคอิน');
					}
					//************ส่งค่าไปหน้าต่างๆๆ********* */
					if ($value == 1) {	
						$this->redirect(array('controller' => 'proposals','action' => 'list_result_manage_eng_all'));
					}elseif ($value == 2) {	
						if($admins['Admincurriculumrequest']['employee_status_id'] == 1){
							$this->redirect(array('controller' => 'proposals','action' => 'list_result_manage_proposal'));
						}elseif($admins['Admincurriculumrequest']['employee_status_id'] == 2){
							$this->redirect(array('controller' => 'proposals','action' => 'list_proposal_all'));
						}elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 3) {
											
							$this->redirect(array('action' => 'dashboard'));
								
							
						} 
					}else{
						if ($admins['Admincurriculumrequest']['employee_status_id'] == 1) {
							$this->redirect(array('controller' => 'proposals','action' => 'Dashboard'));
						}elseif($admins['Admincurriculumrequest']['employee_status_id'] == 2){
							$this->redirect(array('controller' => 'proposals','action' => 'Dashboard'));
						}elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 3) {
							$this->redirect(array('action' => 'dashboard'));
							
						}
					}
					
					
				}else{
					$this->Session->setFlash('ขออภัย ยังไม่พบสิทธิ์การเข้าใช้งานระบบ กรุณาติดต่อเจ้าหน้าที่เพื่อทำการเปิดสิทธิ์ให้เข้าระบบ ');
					$this->Session->write('alertType','danger');
					$this->redirect('https://mis.agri.cmu.ac.th/mis/User/dashboard');
				}
			}
		}
	}	

	public function logout() {
		$this->Session->delete('students'); // from main session
		$this->Session->delete('UserName');
		$this->Session->delete('id');
		$this->Session->delete('Students');
		$this->Session->delete('admins');
		$this->Session->delete('cmuusers');
		// $this->Session->write('alertType','success');
		// $this->Session->setFlash('Logout Complete');
		//$this->redirect('http://www.agri.cmu.ac.th/2017/webs/index/th/1');
		$this->redirect(array('controller' => 'specialandcooperatives','action' => 'index'));
	}
	 
	

	 
	public function info_page_full($id = null){
		$UserName = $this->Session->read('MisEmployee');		
		$admins = $this->Session->read('adminCurriculumRequests');		
	
		$this->set(array(
			'admins' => $admins,
			'UserName' => $UserName,
			
		));
			
		if ($id == 226) {			
			$news = $this->request->data = $this->Infopage->findById($id);
			$this->set(array('news' => $news));
			
			$CountRead = $news['Infopage']['CountRead'] + 1;
			$data = array('id' => $id ,'CountRead' => $CountRead);
			$this->Infopage->save($data);
			//======================================
			$new  = $this->Infopage->find('first',array(
				'conditions' => array(				
					'Infopage.id' => $id
				),
			));
	
			$this->set(array(
				'new' => $new ,
			));
		}else {
			$this->redirect(array('action' => 'logout'));
		}
		$this->set(array('id' => $id));
	}
 
    public function index(){
		$UserName = $this->Session->read('MisEmployee');		
		$admins = $this->Session->read('adminCurriculumRequests');
		$Students = $this->Session->read('Students');
		//$this->request->data['Table']['fields'] = $this->Session->read('UserName');

		if(isset($UserName)){
			$this->set('UserName', $UserName);
			$this->set('admins', $admins);
			

			if($this->action == 'login')
				$this->redirect(array('action' => 'Dashboard'));
		}elseif(isset($Students)){
			$this->set('Students', $Students);

			if($this->action == 'login')
			$this->redirect(array('controller' => 'proposals','action' => 'index'));
		}
		else{
			
			
		}
		
		//**********************แสดงยอดผู้ส่งหัวข้อในระบบ*************************** */
			$yearNew = $this->Yearterm->find('first' ,array(
				'conditions' => array(								
					'level' => 2
				),
				'order' => array('Yearterm.id' => 'DESC'),
			));
			

			$yearOld = $this->Yearterm->find('first' ,array(
				'conditions' => array(								
					'year' => 2561
				),
				
				'order' => array('Yearterm.id' => 'DESC'),
			));
			
			$output = "";
			if ($yearNew == null) {
				$output .=	'<tr>
								<td colspan="6" class="danger"><center>ไม่พบข้อมูลหัวข้อโครงร่างวิทยานิพนธ์</center></td>														
							</tr>';
			}else {
				$i = $yearNew['Yearterm']['year'];		
				
				for($yearNew['Yearterm']['year'] = $i ;$yearNew['Yearterm']['year'] >= $yearOld['Yearterm']['year'] ; $yearNew['Yearterm']['year']--){ 
					
						$listCountResearchMasterThesis = $this->Proposal->find('count' ,array(
								'conditions' => array(
									'Proposal.year' => $yearNew['Yearterm']['year'],						
									'Proposal.degree_id' => 3,
									'Proposal.researchtype_id' => 1
								),
							));			
					
						$listCountResearchMasterIS = $this->Proposal->find('count' ,array(
								'conditions' => array(
									'Proposal.year' => $yearNew['Yearterm']['year'],						
									'Proposal.degree_id' => 3,
									'Proposal.researchtype_id' => 2
								),
							));			
						
				
	
						$listCountResearchDoctorThesis = $this->Proposal->find('count' ,array(
								'conditions' => array(
									'Proposal.year' => $yearNew['Yearterm']['year'],						
									'Proposal.degree_id' => 5,
									'Proposal.researchtype_id' => 1
								),
							));			
						
					
						$listCountResearchDoctorIS = $this->Proposal->find('count' ,array(
								'conditions' => array(
									'Proposal.year' => $yearNew['Yearterm']['year'],						
									'Proposal.degree_id' => 5,
									'Proposal.researchtype_id' => 2
								),
							));			
					 
				
					$listCount = ($listCountResearchMasterThesis + $listCountResearchMasterIS + $listCountResearchDoctorThesis + $listCountResearchDoctorIS);
					
					$output .=
						'<tr>
							<td align="center">'.$yearNew['Yearterm']['year'].'</td>';
							if($listCountResearchMasterThesis == null){
								$output .= '<td><center>-</center></td>';	
							}else{
								$output .= '<td > <a href="'.$this->urls.'/proposals/list_thesis_is_student_detail/'.$yearNew['Yearterm']['year'].'/3/1" target="_blank" class="btn btn-primary btn-block"><center>'. $listCountResearchMasterThesis .'</center></a></td>';							
							
							}
							if($listCountResearchMasterIS == null){
								$output .= '<td><center>-</center></td>';	
							}else{
								$output .= '<td > <a href="'.$this->urls.'/proposals/list_thesis_is_student_detail/'.$yearNew['Yearterm']['year'].'/3/2" target="_blank" class="btn btn-primary btn-block"><center>'. $listCountResearchMasterIS .'</center></a></td>';							
							
							}
							if($listCountResearchDoctorThesis == null){
								$output .= '<td><center>-</center></td>';	
							}else{
								$output .= '<td > <a href="'.$this->urls.'/proposals/list_thesis_is_student_detail/'.$yearNew['Yearterm']['year'].'/5/1" target="_blank" class="btn btn-primary btn-block"><center>'. $listCountResearchDoctorThesis .'</center></a></td>';							
							
							}
							if($listCountResearchDoctorIS == null){
								$output .= '<td><center>-</center></td>';	
							}else{
								$output .= '<td > <a href="'.$this->urls.'/proposals/list_thesis_is_student_detail/'.$yearNew['Yearterm']['year'].'/5/2" target="_blank" class="btn btn-primary btn-block"><center>'. $listCountResearchDoctorIS .'</center></a></td>';							
							
							}
					$output .=		
							'<td><center>'. $listCount .'</center></td>														
						</tr>';
					

					}
				
			}
			$this->set(array(		
			'output' => $output,		
			));
		
		//====================================================		
		//===============แสดงยอดนักศึกษาปัจจุบัน========================
			$degrees = $this->Degree->find('all');	 
			$yearNew = $this->Studentterm->find('first',array(
				'fields' => array('term_year'),
				'order' => array('term_year' => 'DESC') 
			));	
			$lastId = $this->Studentterm->find('first', array(
				'conditions' => array(
					'term_year' =>  $yearNew['Studentterm']['term_year'] ),
					'Student.student_apply_id' => 1,
				'fields' => array('term_id'),
				'order' => array('Studentterm.term_id' => 'DESC'),
			));  
			$lastYearTerms = $this->Yearterm->find('first', array(
				'conditions' => array(
					'Yearterm.level' => 1,
					// 'Student.student_apply_id' => 1,
				),
				'fields' => array('Yearterm.postdate'),
				'order' => array('Yearterm.id' => 'DESC'),
			)); 
			
			$studentsDroopdowns = $this->Student->find('all',array(
				'conditions' => array(
					// 'term_year' => $lastId['Studentterm']['term_year'],
					// 'term_id' => $lastId['Studentterm']['term_id'],
					'Student.student_apply_id' => 1,
					// 'Studentstatus.student_status_level' => 1,
				),
				'fields' => array('Student.id','Student.student_firstname','Student.student_surname','Student.student_eng'),
				'order' => array('Student.id' => 'ASC')
			));
			// debug($studentsDroopdowns );
				$i = 0;
				$outputstudent = '';
				foreach ($studentsDroopdowns as $studentsDroopdown) {
					$i++;
					$outputstudent .= '
						<option value="'.$studentsDroopdown['Student']['id'].'">'.$studentsDroopdown['Student']['id'].' '.$studentsDroopdown['Student']['student_firstname'].' '.$studentsDroopdown['Student']['student_surname'].' '.$studentsDroopdown['Student']['student_eng'].'</option>
					';
	
				}
			foreach ($degrees as $degree) { 
				$students[$degree['Degree']['degree_name']] = $this->Studentterm->find('count', array(
									'conditions' => array(
										'Student.degree_id' => $degree['Degree']['id'] ,
										'term_year' => $yearNew['Studentterm']['term_year'],	
										'Student.student_apply_id' => 1,								
										'Studentstatus.student_status_level'  => 1, 'Student.student_apply_id' =>1,
										'term_id' => $lastId['Studentterm']['term_id']
									)
				)); 
				
			}
		
		$this->set(array(
			'degrees' => $degrees,
		
			'students' => $students,
			'yearNew' => $yearNew,
			'outputstudent' => $outputstudent,						
		
			'lastId' => $lastId	,
			'lastYearTerms' => $lastYearTerms	
			)); 
		//===================================================
		 
	}
	
	 
	##########################################################################
	################################# เสนอ มคอ 3-4 ##########################
	public function list_proposal_all(){
		$UserName = $this->Session->read('MisEmployee');		
		$admins = $this->Session->read('adminCurriculumRequests');
		
		if(isset($UserName)){
			$this->set('UserName', $UserName);
			$this->set('admins', $admins);
			

			if($this->action == 'login')
				$this->redirect(array('action' => 'Dashboard'));
		}
		else{
			if($this->action != 'login')
				$this->redirect('https://mis.agri.cmu.ac.th/mis/User/dashboard');
			
		}
		$this->set(array(
			'admins' => $admins,
			'UserName' => $UserName,
			
		));	
		
			$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all',array(
				'conditions' => array(
					'SpecialAndCooperative.mis_employee_id' => $UserName['MisEmployee']['id'] ,							
					),
					'order' => array('SpecialAndCooperative.postdate' => 'DESC')
				));	
			$lastSpecialAndCooperatives = $this->SpecialAndCooperative->find('first',array(
				'conditions' => array(
					'SpecialAndCooperative.mis_employee_id' => $UserName['MisEmployee']['id'] ,							
					),
					'order' => array('SpecialAndCooperative.id' => 'DESC')
				));		
			
			$output = "";
			 
				 
				$i=0;
			foreach ($SpecialAndCooperatives as $SpecialAndCooperative){ 
				 $i++;	
							
					$answers = $this->AnswerSpecialAndCooperative->find('count',array(
					'conditions' => array(			
						'AnswerSpecialAndCooperative.special_and_cooperative_id' => $SpecialAndCooperative['SpecialAndCooperative']['id'],					 
						'AnswerSpecialAndCooperative.show' => 1
						),
						'order' => array('AnswerSpecialAndCooperative.modified' => 'ASC')
					));	
			  
				 		 
				$output .= '<tr>
							<td data-title="ลำดับ" >'.$i.'</td>
							<td data-title="รหัสวิชา" >'.$SpecialAndCooperative['SpecialAndCooperative']['code'].'</td>
							<td data-title="รหัสวิชา" >'.$SpecialAndCooperative['SpecialAndCooperative']['title'].'</td>
							<td data-title="สาขาวิชา" >'.$SpecialAndCooperative['Major']['major_name'].'</td>					
							<td data-title="ระดับปริญญา" >'.$SpecialAndCooperative['Degree']['degree_name'].'</td>	
							<td data-title="วันที่ยื่นเสนอหัวข้อ">'. $this->DateThai($SpecialAndCooperative['SpecialAndCooperative']['postdate']).'</td> 
							<td data-title="สถานะ" > '; 			
								if ($SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 1 || $SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 2) {  
									$output .= '<font color="red"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>'.$SpecialAndCooperative['SpecialAndCooperativePass']['name'].'</font>';	
									
								}elseif ($SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 4){  		
									$output .= '<font color="green"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>'.$SpecialAndCooperative['SpecialAndCooperativePass']['name'].'</font>'; 
									
								}else{  		
									$output .= '<font color="orange"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>'.$SpecialAndCooperative['SpecialAndCooperativePass']['name'].'</font>'; 
								  }  
					$output .= '<br> 
							</td>
							<td data-title="สถานะ" >';
								if ($SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] >= 1 && $SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] <= 4) { 
										$output .= '<span class="label label-danger"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>'.$SpecialAndCooperative['SpecialAndCooperativeFollowStatus']['name'].'</span>';
													
								}elseif ($SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 10) { 
									if($SpecialAndCooperative['SpecialAndCooperative']['file'] != null){ 
											$output .= '<span class="label label-success"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>'.$SpecialAndCooperative['SpecialAndCooperativeFollowStatus']['name'].'</span>';
									}else{ 	
											$output .= '<span class="label label-warning"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>'.$SpecialAndCooperative['SpecialAndCooperativeFollowStatus']['name'].'</span>';
									
									} 		 			
								}else{  			
										$output .= '<span class="label label-warning"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>'.$SpecialAndCooperative['SpecialAndCooperativeFollowStatus']['name'].'</span>';
									
								}  
							 
								
							$output .= '</td>
							<td data-title="การตอบกลับ" >
								
								<a class="btn btn-default"
									href="'.$this->urls.'/specialAndCooperatives/proposal_detail/'.$SpecialAndCooperative['SpecialAndCooperative']['id'].'/'.$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'].'"
									role="button" target="_blank"><span class="glyphicon glyphicon-circle-arrow-right"></span>
									<font color="red"> ดูการพิจารณา  </font>';
								if ($answers == 0) { }else { 
									$output .= '<button type="button" class="btn btn-danger btn-circle">'.$answers.' </button>';															
									}  
									$output .= '
								</a><br>'; 					
				$output .= '</td>';
			$output .= '</tr>';
		} 
		$this->set(array(
			'output' => $output,  
			));
		
	}
	 
	public function answer_proposal($proposal_id=null){
		$Students = $this->Session->read('Students');
		try{
			if ($this->request->data) {
				date_default_timezone_set('Asia/Bangkok');
											
						
						$this->request->data['AnswerProposal']['PostName'] = $Students['Student']['student_title'].''.$Students['Student']['student_firstname'].' '.$Students['Student']['student_surname'];
						$this->request->data['AnswerProposal']['status'] = 1;
						$this->request->data['AnswerProposal']['student_id'] = $Students['Student']['id'];
						$this->request->data['AnswerProposal']['proposal_id'] = $proposal_id;
						// $status = 3;
						// $data = array('id' => $complaint_id['Complaint']['id'], 'status' => $status);
						// $this->Complaint->save($data);
						$this->AnswerProposal->save($this->request->data['AnswerProposal']);
						// if ($this->Complaint->save($data))
											
						$this->Session->setFlash('<i class="glyphicon glyphicon-ok-sign"></i> ตอบกลับความคิดเห็นไปยังกรรมการ เรียบร้อยแล้ว');
						$this->Session->write('alertType','success');	
						$this->redirect(array('action' => 'answer_proposal',$proposal_id));		
					
				
				}
	
		}catch(Exception $e){
			$this->Session->setFlash('<i class="glyphicon glyphicon-remove-sign"></i> ขออภัย เกิดเหตุขัดข้อง ไม่สามารถดำเนินการได้');
			$this->Session->write('alertType','danger');		
			$this->redirect(array('action' => 'list_proposal_all'));	
			//echo 'window.location.href= https://stackoverflow.com/search?='+ $e->message();
		}
		
		//================================================	
		$proposals = $this->Proposal->find('all',array(
			'conditions' => array(				
				'Proposal.id' => $proposal_id,
			),
			'order' => array('Proposal.modified' => 'DESC')
		));
		$output ='';
		$proposaldocumentdates = $this->ProposalDocument->find('all',array(
			'conditions' => array(				
				'ProposalDocument.proposal_id' => $proposal_id,
				),
				'group' => array('ProposalDocument.postdate'),
				'order' => array('ProposalDocument.postdate' => 'ASC')
			));
			foreach ($proposaldocumentdates as $proposaldocumentdate) {
				$num=0;
				$proposaldocuments = $this->ProposalDocument->find('all',array(
					'conditions' => array(				
						'ProposalDocument.proposal_id' => $proposal_id,
						'ProposalDocument.postdate' => $proposaldocumentdate['ProposalDocument']['postdate'] ,
						),
						'order' => array('ProposalDocument.postdate' => 'ASC')
					));
					if($num == 0){								
						$output .=
						'<tr>
							<td colspan="2"> วันที่ส่งเอกสาร :
								'. $proposaldocumentdate['ProposalDocument']['postdate'] .'
							</td>
							
						</tr>';
						
						$num++;
					}else {
						$output .=
						'';
					}
					$i=0;
					foreach ($proposaldocuments as $proposaldocument) {
						$i++;
						$output .=
						'<tr>
						<td data-title="ลำดับ" align="center"> '.$i.'.</td></td>
						<td>
							<a href="'.$this->urls.'/files/students/proposals/'.$Students['Student']['id'].DS.$proposaldocument['ProposalDocument']['name'].'" class="btn btn-primary" target="_blank">
							Download'.$proposaldocument['ProposalDocument']['name'].'<span class="glyphicon glyphicon-cloud-download"></span></a>
						</td>
					  </tr>';
					}
					
			}

		$answers = $this->AnswerProposal->find('all',array(
			'conditions' => array(			
				'AnswerProposal.proposal_id' => $proposal_id,
				'AnswerProposal.student_id' => $Students['Student']['id']
				),
				'order' => array('AnswerProposal.modified' => 'ASC')
			));
		
		$this->set(array(
			'proposals' => $proposals,
			'answers' => $answers,
			'proposal_id' => $proposal_id,
			'output' => $output
			));
	}
	 
	##########################################################################
	################################# ADMIN #################################
	##########################################################################
	
	public function dashboard($yearList = null,$term = null){		
			
			$UserName = $this->Session->read('MisEmployee');		
			$admins = $this->Session->read('adminCurriculumRequests');
		
			if(isset($UserName)){
				$this->set('UserName', $UserName);
				$this->set('admins', $admins);
				
	
				if($this->action == 'login')
					$this->redirect(array('action' => 'Dashboard'));
			}
			else{
				if($this->action != 'login')
					$this->redirect('https://mis.agri.cmu.ac.th/mis/User/dashboard');
				
			}
			$lastLogTimeStampSpecialAndCooperative = $this->LogTimeStampSpecialAndCooperative->find('first', array(
				'conditions' => array(
				 
					'LogTimeStampSpecialAndCooperative.mis_employee_id' => $UserName['MisEmployee']['id']
				),
				'order' => array('LogTimeStampSpecialAndCooperative.id' => 'DESC')
			));
			if($admins['Admincurriculumrequest']['employee_status_id'] == 3){
				$countAllSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,	
						'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
						'SpecialAndCooperative.degree_id' => 1,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countAllSpecialAndCooperatives3 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,
						'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
						'SpecialAndCooperative.degree_id' => 3,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countAllSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,
						'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
						'SpecialAndCooperative.degree_id' => 5,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 8),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
						'SpecialAndCooperative.special_and_cooperative_follow_status_id !=' => 10,
						'SpecialAndCooperative.degree_id' => 1,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$this->set(array(		
					'SpecialAndCooperatives' => $SpecialAndCooperatives,				 
					'countAllSpecialAndCooperatives1' => $countAllSpecialAndCooperatives1,
					'countAllSpecialAndCooperatives3' => $countAllSpecialAndCooperatives3,
					'countAllSpecialAndCooperatives5' => $countAllSpecialAndCooperatives5
				));
			}elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 1) {				 
				$countAllSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 9),
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 4)
						),
						 
						'SpecialAndCooperative.degree_id' => 1,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countAllSpecialAndCooperatives3 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 9),
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 4)
						),
					 
						'SpecialAndCooperative.degree_id' => 3,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countAllSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 9),
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 4)
						),
					 
						'SpecialAndCooperative.degree_id' => 5,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				 
					 
					 
					$this->set(array(
					 
						'countAllSpecialAndCooperatives1' => $countAllSpecialAndCooperatives1,
						'countAllSpecialAndCooperatives3' => $countAllSpecialAndCooperatives3,
						'countAllSpecialAndCooperatives5' => $countAllSpecialAndCooperatives5,
	
					 
				));
			}elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 2) {
				
					$countAllSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
						'conditions' => array(
							'and' => array(
								array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
								array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
							),
							'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
							'SpecialAndCooperative.degree_id' => 1,
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					));
					$countAllSpecialAndCooperatives3 = $this->SpecialAndCooperative->find('count', array(
						'conditions' => array(
							'and' => array(
								array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
								array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
							),
							'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
							'SpecialAndCooperative.degree_id' => 3,
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					));
					$countAllSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
						'conditions' => array(
							'and' => array(
								array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
								array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
							),
							'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
							'SpecialAndCooperative.degree_id' => 5,
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					));
				 
					 
					$countSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
						'conditions' => array(
							// 'and' => array(
							// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
							// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
							// ),
							'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,
							'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
							'SpecialAndCooperative.degree_id' => 1,
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					));
					$countSpecialAndCooperatives2 = $this->SpecialAndCooperative->find('count', array(
						'conditions' => array(
							// 'and' => array(
							// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
							// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
							// ),
							'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 2,
							'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
							'SpecialAndCooperative.degree_id' => 1,
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					));
					$countSpecialAndCooperatives4 = $this->SpecialAndCooperative->find('count', array(
						'conditions' => array(
							'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 4,
							'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],	
							'SpecialAndCooperative.degree_id' => 1,	
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					));
					$countSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
						'conditions' => array(
							'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 5,
							'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],	
							'SpecialAndCooperative.degree_id' => 1,			
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					));
					$countSpecialAndCooperatives6 = $this->SpecialAndCooperative->find('count', array(
						'conditions' => array(
							'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 6,
							'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
							'SpecialAndCooperative.degree_id' => 1,					
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					));
					$countSpecialAndCooperatives7 = $this->SpecialAndCooperative->find('count', array(
						'conditions' => array(
							'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 7,
							'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
							'SpecialAndCooperative.degree_id' => 1,
									 
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					));
					$countSpecialAndCooperatives8 = $this->SpecialAndCooperative->find('count', array(
						'conditions' => array(
							'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 8,
							'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
							'SpecialAndCooperative.degree_id' => 1,					
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					));
					$countSpecialAndCooperatives9 = $this->SpecialAndCooperative->find('count', array(
						'conditions' => array(
							'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 9,
							'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],	
							'SpecialAndCooperative.degree_id' => 1,				
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					));
					$countSpecialAndCooperatives10 = $this->SpecialAndCooperative->find('count', array(
						'conditions' => array(
							'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 10,
							'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
							'SpecialAndCooperative.degree_id' => 1,					
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					));
					$this->set(array(
						
						'countAllSpecialAndCooperatives1' => $countAllSpecialAndCooperatives1,
						'countAllSpecialAndCooperatives3' => $countAllSpecialAndCooperatives3,
						'countAllSpecialAndCooperatives5' => $countAllSpecialAndCooperatives5,
	
						'countSpecialAndCooperatives1' => $countSpecialAndCooperatives1,
						'countSpecialAndCooperatives2' => $countSpecialAndCooperatives2,
						'countSpecialAndCooperatives4' => $countSpecialAndCooperatives4,
						'countSpecialAndCooperatives5' => $countSpecialAndCooperatives5,
						'countSpecialAndCooperatives6' => $countSpecialAndCooperatives6,
						'countSpecialAndCooperatives7' => $countSpecialAndCooperatives7,
						'countSpecialAndCooperatives8' => $countSpecialAndCooperatives8,
						'countSpecialAndCooperatives9' => $countSpecialAndCooperatives9,
						'countSpecialAndCooperatives10' => $countSpecialAndCooperatives10,
				));
			}
			$misPositions = $this->MisPosition->find('first', array(
				'conditions' => array( 
					'MisPosition.id' => $UserName['MisEmployee']['mis_position_id']
				),
				'order' => array('MisPosition.id' => 'DESC')
			));
             
			$this->set(array(
				'lastLogTimeStampSpecialAndCooperative' => $lastLogTimeStampSpecialAndCooperative,	
			 	'misPositions' => $misPositions				 
					
			));
	}
	 
	 
	//*****************มคอ.3-4 ป.ตรี********************* */
	public function list_result_manage_proposal($degree_id = null , $id = null){

		$UserName = $this->Session->read('MisEmployee');		
		$admins = $this->Session->read('adminCurriculumRequests');
		
		if(isset($UserName)){
			$this->set('UserName', $UserName);
			$this->set('admins', $admins);
			

			if($this->action == 'login')
				$this->redirect(array('action' => 'Dashboard'));
		}
		else{
			if($this->action != 'login')
				// $this->redirect(array('controller' => 'mains','action' => 'login'));
				$this->redirect('https://mis.agri.cmu.ac.th/mis/User/dashboard');
				
		}
		$this->set(array(
			'admins' => $admins,
			'UserName' => $UserName,
			'degree_id' => $degree_id,
			'id' => $id
			 
		));
		
		 
		if($admins['Admincurriculumrequest']['employee_status_id'] == 3){
			if ($id == null) { 

				$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 8),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
						'SpecialAndCooperative.special_and_cooperative_follow_status_id !=' => 10,
						'SpecialAndCooperative.degree_id' => $degree_id,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));

				
			}else {
			 
				
				$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => $id,
						'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
						'SpecialAndCooperative.degree_id' => $degree_id,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				
				 
			}
			$countAllSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					// 'and' => array(
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
					// ),
					'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,	
					'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
					'SpecialAndCooperative.degree_id' => 1,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$countAllSpecialAndCooperatives3 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					// 'and' => array(
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
					// ),
					'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,
					'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
					'SpecialAndCooperative.degree_id' => 3,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$countAllSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					// 'and' => array(
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
					// ),
					'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,
					'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
					'SpecialAndCooperative.degree_id' => 5,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$countSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					// 'and' => array(
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
					// ),
					'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,
					'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
					'SpecialAndCooperative.degree_id' => $degree_id,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$countSpecialAndCooperatives2 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					// 'and' => array(
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
					// ),
					'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 2,
					'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
					'SpecialAndCooperative.degree_id' => $degree_id,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$countSpecialAndCooperatives4 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 4,
					'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],	
					'SpecialAndCooperative.degree_id' => $degree_id,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,	
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$countSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 5,
					'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],	
					'SpecialAndCooperative.degree_id' => $degree_id,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,			
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$countSpecialAndCooperatives6 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 6,
					'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
					'SpecialAndCooperative.degree_id' => $degree_id,	
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$countSpecialAndCooperatives7 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 7,
					'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
					'SpecialAndCooperative.degree_id' => $degree_id,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
							 
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$countSpecialAndCooperatives8 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 8,
					'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
					'SpecialAndCooperative.degree_id' => $degree_id,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,					
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$countSpecialAndCooperatives9 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 9,
					'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],	
					'SpecialAndCooperative.degree_id' => $degree_id,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$countSpecialAndCooperatives10 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 10,
					'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
					'SpecialAndCooperative.degree_id' => $degree_id,	
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$checkSpecialAndCooperatives10 = $this->SpecialAndCooperative->find('first', array(
				'conditions' => array(
					'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 10,
					'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
					'SpecialAndCooperative.check_pass_id' => 5,	
					'SpecialAndCooperative.degree_id' => $degree_id,	
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$this->set(array(
				'checkSpecialAndCooperatives10' => $checkSpecialAndCooperatives10,
			 
			));
			$this->set(array(
				'SpecialAndCooperatives' => $SpecialAndCooperatives,
				'countAllSpecialAndCooperatives1' => $countAllSpecialAndCooperatives1,
				'countAllSpecialAndCooperatives3' => $countAllSpecialAndCooperatives3,
				'countAllSpecialAndCooperatives5' => $countAllSpecialAndCooperatives5,

				'countSpecialAndCooperatives1' => $countSpecialAndCooperatives1,
				'countSpecialAndCooperatives2' => $countSpecialAndCooperatives2,
				'countSpecialAndCooperatives4' => $countSpecialAndCooperatives4,
				'countSpecialAndCooperatives5' => $countSpecialAndCooperatives5,
				'countSpecialAndCooperatives6' => $countSpecialAndCooperatives6,
				'countSpecialAndCooperatives7' => $countSpecialAndCooperatives7,
				'countSpecialAndCooperatives8' => $countSpecialAndCooperatives8,
				'countSpecialAndCooperatives9' => $countSpecialAndCooperatives9,
				'countSpecialAndCooperatives10' => $countSpecialAndCooperatives10,
				 
			));
		}elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 1) {
			if ($id == null) { 

				$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 8),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id !=' => 10,
						'SpecialAndCooperative.degree_id' => $degree_id,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));

				
			}else {
			 
				
				$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => $id,
						'SpecialAndCooperative.degree_id' => $degree_id,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				
			}
			$countAllSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					'and' => array(
						array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 9),
						array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 4)
					),
					 
					'SpecialAndCooperative.degree_id' => 1,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$countAllSpecialAndCooperatives3 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					'and' => array(
						array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 9),
						array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 4)
					),
				 
					'SpecialAndCooperative.degree_id' => 3,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$countAllSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					'and' => array(
						array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 9),
						array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 4)
					),
				 
					'SpecialAndCooperative.degree_id' => 5,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			 
				 
				$countSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,
						'SpecialAndCooperative.degree_id' => $degree_id,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives2 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 2,
						'SpecialAndCooperative.degree_id' => $degree_id,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives4 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 4,	
						'SpecialAndCooperative.degree_id' => $degree_id,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,	
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 5,	
						'SpecialAndCooperative.degree_id' => $degree_id,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,			
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives6 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 6,
						'SpecialAndCooperative.degree_id' => $degree_id,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives7 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 7,
						'SpecialAndCooperative.degree_id' => $degree_id,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					 			
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives8 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 8,
						'SpecialAndCooperative.degree_id' => $degree_id,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,					
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives9 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 9,	
						'SpecialAndCooperative.degree_id' => $degree_id,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives10 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 10,
						'SpecialAndCooperative.degree_id' => $degree_id,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$checkSpecialAndCooperatives10 = $this->SpecialAndCooperative->find('first', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 10,						 
						'SpecialAndCooperative.check_pass_id' => 5,	
						'SpecialAndCooperative.degree_id' => $degree_id,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$this->set(array(
					'checkSpecialAndCooperatives10' => $checkSpecialAndCooperatives10,
				 
				));
				$this->set(array(
					'SpecialAndCooperatives' => $SpecialAndCooperatives,
					'countAllSpecialAndCooperatives1' => $countAllSpecialAndCooperatives1,
					'countAllSpecialAndCooperatives3' => $countAllSpecialAndCooperatives3,
					'countAllSpecialAndCooperatives5' => $countAllSpecialAndCooperatives5,

					'countSpecialAndCooperatives1' => $countSpecialAndCooperatives1,
					'countSpecialAndCooperatives2' => $countSpecialAndCooperatives2,
					'countSpecialAndCooperatives4' => $countSpecialAndCooperatives4,
					'countSpecialAndCooperatives5' => $countSpecialAndCooperatives5,
					'countSpecialAndCooperatives6' => $countSpecialAndCooperatives6,
					'countSpecialAndCooperatives7' => $countSpecialAndCooperatives7,
					'countSpecialAndCooperatives8' => $countSpecialAndCooperatives8,
					'countSpecialAndCooperatives9' => $countSpecialAndCooperatives9,
					'countSpecialAndCooperatives10' => $countSpecialAndCooperatives10,
		    	));
		}elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 2) {
			if ($id == null) { 

				$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 9),
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						),
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						 
						'SpecialAndCooperative.degree_id' => $degree_id,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));

				
			}else {
			 
				
				$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => $id,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => $degree_id,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,					
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				
			}
				$countAllSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						),
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => 1,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countAllSpecialAndCooperatives3 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						),
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => 3,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countAllSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						),
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => 5,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
			 
				 
				$countSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => $degree_id,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives2 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 2,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => $degree_id,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives4 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 4,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],	
						'SpecialAndCooperative.degree_id' => $degree_id,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,	
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 5,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],	
						'SpecialAndCooperative.degree_id' => $degree_id,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,			
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives6 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 6,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => $degree_id,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,					
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives7 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 7,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => $degree_id,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					 			
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives8 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 8,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => $degree_id,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,					
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives9 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 9,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],	
						'SpecialAndCooperative.degree_id' => $degree_id,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives10 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 10,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => $degree_id,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$checkSpecialAndCooperatives10 = $this->SpecialAndCooperative->find('first', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 10,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.check_pass_id' => 5,	
						'SpecialAndCooperative.degree_id' => $degree_id,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$this->set(array(
					'checkSpecialAndCooperatives10' => $checkSpecialAndCooperatives10,
				 
				));
				$this->set(array(
					'SpecialAndCooperatives' => $SpecialAndCooperatives,
					'countAllSpecialAndCooperatives1' => $countAllSpecialAndCooperatives1,
					'countAllSpecialAndCooperatives3' => $countAllSpecialAndCooperatives3,
					'countAllSpecialAndCooperatives5' => $countAllSpecialAndCooperatives5,

					'countSpecialAndCooperatives1' => $countSpecialAndCooperatives1,
					'countSpecialAndCooperatives2' => $countSpecialAndCooperatives2,
					'countSpecialAndCooperatives4' => $countSpecialAndCooperatives4,
					'countSpecialAndCooperatives5' => $countSpecialAndCooperatives5,
					'countSpecialAndCooperatives6' => $countSpecialAndCooperatives6,
					'countSpecialAndCooperatives7' => $countSpecialAndCooperatives7,
					'countSpecialAndCooperatives8' => $countSpecialAndCooperatives8,
					'countSpecialAndCooperatives9' => $countSpecialAndCooperatives9,
					'countSpecialAndCooperatives10' => $countSpecialAndCooperatives10,
		    	));
		}
		
	}
	
	//=============================มคอ.3-4 ป.โท ================================
	public function list_result_manage_proposals($id = null){

		$UserName = $this->Session->read('MisEmployee');		
		$admins = $this->Session->read('adminCurriculumRequests');
		
		if(isset($UserName)){
			$this->set('UserName', $UserName);
			$this->set('admins', $admins);
			

			if($this->action == 'login')
				$this->redirect(array('action' => 'Dashboard'));
		}
		else{
			if($this->action != 'login')
				$this->redirect('https://mis.agri.cmu.ac.th/mis/User/dashboard');
			
		}
		$this->set(array(
			'admins' => $admins,
			'UserName' => $UserName,
			 
		));
		if($admins['Admincurriculumrequest']['employee_status_id'] == 3){
			$countAllSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					// 'and' => array(
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
					// ),
					'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,
					'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
					'SpecialAndCooperative.degree_id' => 1,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$countAllSpecialAndCooperatives3 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					// 'and' => array(
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
					// ),
					'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,
					'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
					'SpecialAndCooperative.degree_id' => 3,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$countAllSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					// 'and' => array(
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
					// ),
					'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,
					'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
					'SpecialAndCooperative.degree_id' => 5,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
				'conditions' => array(
					// 'and' => array(
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 8),
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
					// ),
					'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
					'SpecialAndCooperative.special_and_cooperative_follow_status_id !=' => 10,
					'SpecialAndCooperative.degree_id' => 3,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$this->set(array(		
				'SpecialAndCooperatives' => $SpecialAndCooperatives,				 
				'countAllSpecialAndCooperatives1' => $countAllSpecialAndCooperatives1,
				'countAllSpecialAndCooperatives3' => $countAllSpecialAndCooperatives3,
				'countAllSpecialAndCooperatives5' => $countAllSpecialAndCooperatives5
			));
		}elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 1) {
			if ($id == null) { 

				$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 8),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id !=' => 10,
						'SpecialAndCooperative.degree_id' => 3,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));

				
			}else {
			 
				
				$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => $id,
						'SpecialAndCooperative.degree_id' => 3,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,					
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				
			}
			$countAllSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					'and' => array(
						array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 9),
						array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 4)
					),
					 
					'SpecialAndCooperative.degree_id' => 1,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$countAllSpecialAndCooperatives3 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					'and' => array(
						array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 9),
						array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 4)
					),
				 
					'SpecialAndCooperative.degree_id' => 3,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$countAllSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					'and' => array(
						array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 9),
						array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 4)
					),
				 
					'SpecialAndCooperative.degree_id' => 5,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			 
				 
				$countSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,
						'SpecialAndCooperative.degree_id' => 3,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives2 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 2,
						'SpecialAndCooperative.degree_id' => 3,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives4 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 4,	
						'SpecialAndCooperative.degree_id' => 3,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,	
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 5,	
						'SpecialAndCooperative.degree_id' => 3,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,			
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives6 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 6,
						'SpecialAndCooperative.degree_id' => 3,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,					
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives7 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 7,
						'SpecialAndCooperative.degree_id' => 3,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					 			
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives8 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 8,
						'SpecialAndCooperative.degree_id' => 3,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives9 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 9,	
						'SpecialAndCooperative.degree_id' => 3,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,			
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives10 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 10,
						'SpecialAndCooperative.degree_id' => 3,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,					
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$this->set(array(
					'SpecialAndCooperatives' => $SpecialAndCooperatives,
					'countAllSpecialAndCooperatives1' => $countAllSpecialAndCooperatives1,
					'countAllSpecialAndCooperatives3' => $countAllSpecialAndCooperatives3,
					'countAllSpecialAndCooperatives5' => $countAllSpecialAndCooperatives5,

					'countSpecialAndCooperatives1' => $countSpecialAndCooperatives1,
					'countSpecialAndCooperatives2' => $countSpecialAndCooperatives2,
					'countSpecialAndCooperatives4' => $countSpecialAndCooperatives4,
					'countSpecialAndCooperatives5' => $countSpecialAndCooperatives5,
					'countSpecialAndCooperatives6' => $countSpecialAndCooperatives6,
					'countSpecialAndCooperatives7' => $countSpecialAndCooperatives7,
					'countSpecialAndCooperatives8' => $countSpecialAndCooperatives8,
					'countSpecialAndCooperatives9' => $countSpecialAndCooperatives9,
					'countSpecialAndCooperatives10' => $countSpecialAndCooperatives10,
		    ));
		}elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 2) {
			if ($id == null) { 

				$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						 
						'SpecialAndCooperative.degree_id' => 3,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));

				
			}else {
			 
				
				$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => $id,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => 3,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				
			}
				$countAllSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						),
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => 1,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countAllSpecialAndCooperatives3 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						),
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => 3,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countAllSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						),
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => 5,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
			 
				 
				$countSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => 3,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives2 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 2,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => 3,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives4 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 4,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],	
						'SpecialAndCooperative.degree_id' => 3,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 5,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],	
						'SpecialAndCooperative.degree_id' => 3,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,		
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives6 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 6,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => 3,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives7 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 7,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => 3,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					 			
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives8 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 8,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => 3,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives9 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 9,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],	
						'SpecialAndCooperative.degree_id' => 3,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,			
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives10 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 10,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => 3,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$this->set(array(
					'SpecialAndCooperatives' => $SpecialAndCooperatives,
					'countAllSpecialAndCooperatives1' => $countAllSpecialAndCooperatives1,
					'countAllSpecialAndCooperatives3' => $countAllSpecialAndCooperatives3,
					'countAllSpecialAndCooperatives5' => $countAllSpecialAndCooperatives5,

					'countSpecialAndCooperatives1' => $countSpecialAndCooperatives1,
					'countSpecialAndCooperatives2' => $countSpecialAndCooperatives2,
					'countSpecialAndCooperatives4' => $countSpecialAndCooperatives4,
					'countSpecialAndCooperatives5' => $countSpecialAndCooperatives5,
					'countSpecialAndCooperatives6' => $countSpecialAndCooperatives6,
					'countSpecialAndCooperatives7' => $countSpecialAndCooperatives7,
					'countSpecialAndCooperatives8' => $countSpecialAndCooperatives8,
					'countSpecialAndCooperatives9' => $countSpecialAndCooperatives9,
					'countSpecialAndCooperatives10' => $countSpecialAndCooperatives10,
		    ));
		}
	}
	//=============================มคอ.3-4 ป.เอก ================================
	public function list_result_manage_proposales($id = null){

		$UserName = $this->Session->read('MisEmployee');		
		$admins = $this->Session->read('adminCurriculumRequests');
		
		if(isset($UserName)){
			$this->set('UserName', $UserName);
			$this->set('admins', $admins);
			

			if($this->action == 'login')
				$this->redirect(array('action' => 'Dashboard'));
		}
		else{
			if($this->action != 'login')
				$this->redirect('https://mis.agri.cmu.ac.th/mis/User/dashboard');
			
		}
		$this->set(array(
			'admins' => $admins,
			'UserName' => $UserName,
			 
		));
		if($admins['Admincurriculumrequest']['employee_status_id'] == 3){
			$countAllSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					// 'and' => array(
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
					// ),
					'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,
					'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
					'SpecialAndCooperative.degree_id' => 1,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$countAllSpecialAndCooperatives3 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					// 'and' => array(
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
					// ),
					'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,
					'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
					'SpecialAndCooperative.degree_id' => 3,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$countAllSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					// 'and' => array(
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
					// ),
					'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,
					'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
					'SpecialAndCooperative.degree_id' => 5,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
				'conditions' => array(
					// 'and' => array(
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 8),
					// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
					// ),
					'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
					'SpecialAndCooperative.special_and_cooperative_follow_status_id !=' => 10,
					'SpecialAndCooperative.degree_id' => 5,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$this->set(array(		
				'SpecialAndCooperatives' => $SpecialAndCooperatives,				 
				'countAllSpecialAndCooperatives1' => $countAllSpecialAndCooperatives1,
				'countAllSpecialAndCooperatives3' => $countAllSpecialAndCooperatives3,
				'countAllSpecialAndCooperatives5' => $countAllSpecialAndCooperatives5
			));
		}elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 1) {
			if ($id == null) { 

				$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 8),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id !=' => 10,
						'SpecialAndCooperative.degree_id' => 5,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));

				
			}else {
			 
				
				$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => $id,
						'SpecialAndCooperative.degree_id' => 5,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,					
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				
			}
			$countAllSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					'and' => array(
						array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 9),
						array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 4)
					),
					 
					'SpecialAndCooperative.degree_id' => 1,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$countAllSpecialAndCooperatives3 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					'and' => array(
						array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 9),
						array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 4)
					),
				 
					'SpecialAndCooperative.degree_id' => 3,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			$countAllSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
				'conditions' => array(
					'and' => array(
						array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 9),
						array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 4)
					),
				 
					'SpecialAndCooperative.degree_id' => 5,
					'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			 
				 
				$countSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,
						'SpecialAndCooperative.degree_id' => 5,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives2 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 2,
						'SpecialAndCooperative.degree_id' => 5,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives4 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 4,	
						'SpecialAndCooperative.degree_id' => 5,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,	
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 5,	
						'SpecialAndCooperative.degree_id' => 5,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,		
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives6 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 6,
						'SpecialAndCooperative.degree_id' => 5,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives7 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 7,
						'SpecialAndCooperative.degree_id' => 5,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					 			
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives8 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 8,
						'SpecialAndCooperative.degree_id' => 5,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives9 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 9,	
						'SpecialAndCooperative.degree_id' => 5,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,			
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives10 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 10,
						'SpecialAndCooperative.degree_id' => 5,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$this->set(array(
					'SpecialAndCooperatives' => $SpecialAndCooperatives,
					'countAllSpecialAndCooperatives1' => $countAllSpecialAndCooperatives1,
					'countAllSpecialAndCooperatives3' => $countAllSpecialAndCooperatives3,
					'countAllSpecialAndCooperatives5' => $countAllSpecialAndCooperatives5,

					'countSpecialAndCooperatives1' => $countSpecialAndCooperatives1,
					'countSpecialAndCooperatives2' => $countSpecialAndCooperatives2,
					'countSpecialAndCooperatives4' => $countSpecialAndCooperatives4,
					'countSpecialAndCooperatives5' => $countSpecialAndCooperatives5,
					'countSpecialAndCooperatives6' => $countSpecialAndCooperatives6,
					'countSpecialAndCooperatives7' => $countSpecialAndCooperatives7,
					'countSpecialAndCooperatives8' => $countSpecialAndCooperatives8,
					'countSpecialAndCooperatives9' => $countSpecialAndCooperatives9,
					'countSpecialAndCooperatives10' => $countSpecialAndCooperatives10,
		    ));
		}elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 2) {
			if ($id == null) { 

				$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						 
						'SpecialAndCooperative.degree_id' => 5,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));

				
			}else {
			 
				
				$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => $id,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => 5,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				
			}
				$countAllSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						),
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => 1,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countAllSpecialAndCooperatives3 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						),
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => 3,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countAllSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						),
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => 5,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
			 
				 
				$countSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 1,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => 5,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives2 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 2,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => 5,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives4 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 4,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],	
						'SpecialAndCooperative.degree_id' => 5,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 5,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],	
						'SpecialAndCooperative.degree_id' => 5,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,			
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives6 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 6,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => 5,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives7 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 7,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => 5,
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,
					 			
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives8 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 8,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => 5,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives9 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 9,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],	
						'SpecialAndCooperative.degree_id' => 5,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,			
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives10 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 10,
						'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
						'SpecialAndCooperative.degree_id' => 5,	
						'SpecialAndCooperative.special_and_cooperative_level_id' => 2,				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$this->set(array(
					'SpecialAndCooperatives' => $SpecialAndCooperatives,
					'countAllSpecialAndCooperatives1' => $countAllSpecialAndCooperatives1,
					'countAllSpecialAndCooperatives3' => $countAllSpecialAndCooperatives3,
					'countAllSpecialAndCooperatives5' => $countAllSpecialAndCooperatives5,

					'countSpecialAndCooperatives1' => $countSpecialAndCooperatives1,
					'countSpecialAndCooperatives2' => $countSpecialAndCooperatives2,
					'countSpecialAndCooperatives4' => $countSpecialAndCooperatives4,
					'countSpecialAndCooperatives5' => $countSpecialAndCooperatives5,
					'countSpecialAndCooperatives6' => $countSpecialAndCooperatives6,
					'countSpecialAndCooperatives7' => $countSpecialAndCooperatives7,
					'countSpecialAndCooperatives8' => $countSpecialAndCooperatives8,
					'countSpecialAndCooperatives9' => $countSpecialAndCooperatives9,
					'countSpecialAndCooperatives10' => $countSpecialAndCooperatives10,
		    ));
		}
	}
	public function list_result_manage_proposal_by_follow_status($id=null){

		$UserName = $this->Session->read('MisEmployee');		
		$admins = $this->Session->read('adminCurriculumRequests');
		
		if(isset($UserName)){
			$this->set('UserName', $UserName);
			$this->set('admins', $admins);
			

			if($this->action == 'login')
				$this->redirect(array('action' => 'Dashboard'));
		}
		else{
			if($this->action != 'login')
				$this->redirect('https://mis.agri.cmu.ac.th/mis/User/dashboard');
			
		}
		$this->set(array(
			'admins' => $admins,
			'UserName' => $UserName,
			'id' => $id
		));
	
		if ($admins['Admincurriculumrequest']['employee_status_id'] == 1 || $admins['Admincurriculumrequest']['employee_status_id'] == 3) {
			if ($id == null) { 

				$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 8),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.degree_id' => 1,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));

				
			}elseif($id == 9) {
				 
				
				$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 9,	
						'SpecialAndCooperative.degree_id' => 1,			
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				
			}else {
			 
				
				$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => $id,
						'SpecialAndCooperative.degree_id' => 1,					
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				
			}
			
			 
				 
				$countSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						),
						'SpecialAndCooperative.degree_id' => 1,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives4 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 4,	
						'SpecialAndCooperative.degree_id' => 1,	
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 5,	
						'SpecialAndCooperative.degree_id' => 1,			
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives6 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 6,
						'SpecialAndCooperative.degree_id' => 1,					
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives7 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 7,
						'SpecialAndCooperative.degree_id' => 1,
					 			
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives8 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 8,
						'SpecialAndCooperative.degree_id' => 1,					
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives9 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 9,	
						'SpecialAndCooperative.degree_id' => 1,				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives10 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 10,
						'SpecialAndCooperative.degree_id' => 1,					
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$this->set(array(
					'SpecialAndCooperatives' => $SpecialAndCooperatives,
					'countSpecialAndCooperatives1' => $countSpecialAndCooperatives1,
					'countSpecialAndCooperatives4' => $countSpecialAndCooperatives4,
					'countSpecialAndCooperatives5' => $countSpecialAndCooperatives5,
					'countSpecialAndCooperatives6' => $countSpecialAndCooperatives6,
					'countSpecialAndCooperatives7' => $countSpecialAndCooperatives7,
					'countSpecialAndCooperatives8' => $countSpecialAndCooperatives8,
					'countSpecialAndCooperatives9' => $countSpecialAndCooperatives9,
					'countSpecialAndCooperatives10' => $countSpecialAndCooperatives10,
		    ));
		} 

		 
		
		
		$this->set(array(
				'SpecialAndCooperatives' => $SpecialAndCooperatives,
				 
				 
		));
	}
	public function list_result_manage_proposal_by_follow_statuss($id=null){

		$UserName = $this->Session->read('MisEmployee');		
		$admins = $this->Session->read('adminCurriculumRequests');
		
		if(isset($UserName)){
			$this->set('UserName', $UserName);
			$this->set('admins', $admins);
			

			if($this->action == 'login')
				$this->redirect(array('action' => 'Dashboard'));
		}
		else{
			if($this->action != 'login')
				$this->redirect('https://mis.agri.cmu.ac.th/mis/User/dashboard');
			
		}
		$this->set(array(
			'admins' => $admins,
			'UserName' => $UserName,
			'id' => $id
		));
	
		if ($admins['Admincurriculumrequest']['employee_status_id'] == 1) {
			if ($id == null) { 

				$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.degree_id <= ' => 5),
							array('SpecialAndCooperative.degree_id >= ' => 3)
						),
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));

				
			}elseif($id == 9) {
				 
				
				$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.degree_id <= ' => 5),
							array('SpecialAndCooperative.degree_id >= ' => 3)
						),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 9,	
						 			
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				
			}else {
			 
				
				$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.degree_id <= ' => 5),
							array('SpecialAndCooperative.degree_id >= ' => 3)
						),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => $id,					
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				
			}
			
			 
				 
				$countSpecialAndCooperatives1 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'and' => array(
							'and' => array(
							array('SpecialAndCooperative.degree_id <= ' => 5),
							array('SpecialAndCooperative.degree_id >= ' => 3)
						),
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 3),
							array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						),
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives4 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.degree_id <= ' => 5),
							array('SpecialAndCooperative.degree_id >= ' => 3)
						),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 4,	
					 				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives5 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.degree_id <= ' => 5),
							array('SpecialAndCooperative.degree_id >= ' => 3)
						),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 5,	
					 				
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives6 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.degree_id <= ' => 5),
							array('SpecialAndCooperative.degree_id >= ' => 3)
						),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 6,					
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives7 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.degree_id <= ' => 5),
							array('SpecialAndCooperative.degree_id >= ' => 3)
						),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 7,
					 			
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives8 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.degree_id <= ' => 5),
							array('SpecialAndCooperative.degree_id >= ' => 3)
						),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 8,					
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives9 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.degree_id <= ' => 5),
							array('SpecialAndCooperative.degree_id >= ' => 3)
						),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 9,					
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$countSpecialAndCooperatives10 = $this->SpecialAndCooperative->find('count', array(
					'conditions' => array(
						'and' => array(
							array('SpecialAndCooperative.degree_id <= ' => 5),
							array('SpecialAndCooperative.degree_id >= ' => 3)
						),
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 10,					
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
				$this->set(array(
					'SpecialAndCooperatives' => $SpecialAndCooperatives,
					'countSpecialAndCooperatives1' => $countSpecialAndCooperatives1,
					'countSpecialAndCooperatives4' => $countSpecialAndCooperatives4,
					'countSpecialAndCooperatives5' => $countSpecialAndCooperatives5,
					'countSpecialAndCooperatives6' => $countSpecialAndCooperatives6,
					'countSpecialAndCooperatives7' => $countSpecialAndCooperatives7,
					'countSpecialAndCooperatives8' => $countSpecialAndCooperatives8,
					'countSpecialAndCooperatives9' => $countSpecialAndCooperatives9,
					'countSpecialAndCooperatives10' => $countSpecialAndCooperatives10,
		    ));
		} 

		if ($admins['Admincurriculumrequest']['employee_status_id'] == 4) {
			$passes = $this->SpecialAndCooperativePass->find('list',array(
				'conditions' => array(			
					'level' => 1
					),
					
				));
		}else {			
			$passes = $this->SpecialAndCooperativePass->find('list',array(
				'conditions' => array(			
					'and' => array(
						array('SpecialAndCooperativePass.level <= ' => 2),
						array('SpecialAndCooperativePass.level >= ' => 1)
					),
					
				),
			));
			
		}
		
		
		$this->set(array(
				'SpecialAndCooperatives' => $SpecialAndCooperatives,
				'passes' => $passes,
				 
		));
	}
	//******************************** */
	public function list_result_pass_proposal($id=null){

		$UserName = $this->Session->read('MisEmployee');		
		$admins = $this->Session->read('adminCurriculumRequests');
		
		if(isset($UserName)){
			$this->set('UserName', $UserName);
			$this->set('admins', $admins);
			

			if($this->action == 'login')
				$this->redirect(array('action' => 'Dashboard'));
		}
		else{
			if($this->action != 'login')
			$this->redirect('https://mis.agri.cmu.ac.th/mis/User/dashboard');
			
		}
		$this->set(array(
			'admins' => $admins,
			'UserName' => $UserName,
			'id' => $id
		));
		if($id == null){
			if ($admins['Admincurriculumrequest']['employee_status_id'] == 3) {			 
				//อาจารย์ผู้รับผิดชอบ 
				$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 8),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 10,
						'SpecialAndCooperative.degree_id' => 1,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
	
				//อาจารย์ผู้สอน
				
				$SpecialAndCooperativeEmployees = $this->SpecialAndCooperativeEmployee->find('all', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 8),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperativeEmployee.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
						 
					),
					'order' => array('SpecialAndCooperativeEmployee.modified' => 'DESC')
				));
				$i = 0;
				$outputSpecialAndCooperativeEmployee = "";  
				// debug($SpecialAndCooperativeEmployees);
				foreach ($SpecialAndCooperativeEmployees as $key) {
					$SpecialAndCooperative2s = $this->SpecialAndCooperative->find('all', array(
						'conditions' => array( 
							'SpecialAndCooperative.id' => $key['SpecialAndCooperativeEmployee']['special_and_cooperative_id'],
							'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 10,
							'SpecialAndCooperative.degree_id' => 1,
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					));
					// debug($SpecialAndCooperative2s);
					
					foreach ($SpecialAndCooperative2s as $SpecialAndCooperative) {
						$i++; 
						$outputSpecialAndCooperativeEmployee .='<tr>
							<td data-title="ลำดับ" align="center">'.$i.'</td>
							<td data-title="รหัสวิชา"> '.$SpecialAndCooperative['SpecialAndCooperative']['code'].'</td>
							<td data-title="ชื่อวิชาภาษาไทย"> '.$SpecialAndCooperative['SpecialAndCooperative']['title'].'</td>
							<td data-title="ชื่อวิชาภาษาอังกฤษ"> '.$SpecialAndCooperative['SpecialAndCooperative']['title_eng'].' </td>
						
							<td data-title="สาขาวิชา">'.$SpecialAndCooperative['Major']['major_name'].'</td>
							<td data-title="ภาควิชา">'.$SpecialAndCooperative['Organize']['name'].'</td>
							<td data-title="ระดับการศึกษา">'.$SpecialAndCooperative['Degree']['degree_name'].'</td>
							<td data-title="สถานะ " >'.$SpecialAndCooperative['Yearterm']['name'].'
									  
							</td>
							<td data-title="ไฟล์ มคอ.3-4 ฉบับสมบูรณ์">
								<a href="'.$this->urls.'/specialandcooperatives/add_answer_pass_abstract/'.$SpecialAndCooperative['SpecialAndCooperative']['id'].'/'.$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'].'" class="btn btn-success" target="_blank">
									
									<i class="glyphicon glyphicon-tag" aria-hidden="true"></i> ดูมคอ.3-4 สมบูรณ์
								</a>
							</td> 
								 
						</tr> ';
						 
					}
				}
				// debug($outputSpecialAndCooperativeEmployee);
					$this->set(array(
						'SpecialAndCooperatives' => $SpecialAndCooperatives,
						'outputSpecialAndCooperativeEmployee' => $outputSpecialAndCooperativeEmployee
					
				));
			}elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 2) {			 
				 
					$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
						'conditions' => array(
							'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 10,	
							'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
							'SpecialAndCooperative.degree_id' => 1,			
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					)); 
					$this->set(array(
						'SpecialAndCooperatives' => $SpecialAndCooperatives,
					
				));
			}elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 1) {			 
				 
					$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
						'conditions' => array(
							'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 10,	
										 
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					)); 
					$this->set(array(
						'SpecialAndCooperatives' => $SpecialAndCooperatives,
					 
				));
			} 
			
		}else {
			if ($admins['Admincurriculumrequest']['employee_status_id'] == 3) {			 
				//อาจารย์ผู้รับผิดชอบ 
				$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 8),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperative.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
						'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 10,
						'SpecialAndCooperative.degree_id' => $id,
					),
					'order' => array('SpecialAndCooperative.modified' => 'DESC')
				));
	
				//อาจารย์ผู้สอน
				
				$SpecialAndCooperativeEmployees = $this->SpecialAndCooperativeEmployee->find('all', array(
					'conditions' => array(
						// 'and' => array(
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id <= ' => 8),
						// 	array('SpecialAndCooperative.special_and_cooperative_follow_status_id >= ' => 1)
						// ),
						'SpecialAndCooperativeEmployee.mis_employee_id' => $admins['Admincurriculumrequest']['mis_employee_id'],
						 
					),
					'order' => array('SpecialAndCooperativeEmployee.modified' => 'DESC')
				));
				$i = 0;
				$outputSpecialAndCooperativeEmployee = "";  
				// debug($SpecialAndCooperativeEmployees);
				foreach ($SpecialAndCooperativeEmployees as $key) {
					$SpecialAndCooperative2s = $this->SpecialAndCooperative->find('all', array(
						'conditions' => array( 
							'SpecialAndCooperative.id' => $key['SpecialAndCooperativeEmployee']['special_and_cooperative_id'],
							'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 10,
							'SpecialAndCooperative.degree_id' => $id,
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					));
					// debug($SpecialAndCooperative2s);
					
					foreach ($SpecialAndCooperative2s as $SpecialAndCooperative) {
						$i++; 
						$outputSpecialAndCooperativeEmployee .='<tr>
							<td data-title="ลำดับ" align="center">'.$i.'</td>
							<td data-title="รหัสวิชา"> '.$SpecialAndCooperative['SpecialAndCooperative']['code'].'</td>
							<td data-title="ชื่อวิชาภาษาไทย"> '.$SpecialAndCooperative['SpecialAndCooperative']['title'].'</td>
							<td data-title="ชื่อวิชาภาษาอังกฤษ"> '.$SpecialAndCooperative['SpecialAndCooperative']['title_eng'].' </td>
						
							<td data-title="สาขาวิชา">'.$SpecialAndCooperative['Major']['major_name'].'</td>
							<td data-title="ภาควิชา">'.$SpecialAndCooperative['Organize']['name'].'</td>
							<td data-title="ระดับการศึกษา">'.$SpecialAndCooperative['Degree']['degree_name'].'</td>
							<td data-title="สถานะ " >'.$SpecialAndCooperative['Yearterm']['name'].'
									  
							</td>
							<td data-title="ไฟล์ มคอ.3-4 ฉบับสมบูรณ์">
								<a href="'.$this->urls.'/specialandcooperatives/add_answer_pass_abstract/'.$SpecialAndCooperative['SpecialAndCooperative']['id'].'/'.$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'].'" class="btn btn-success" target="_blank">
									
									<i class="glyphicon glyphicon-tag" aria-hidden="true"></i><font class="txt-content_SpecialAndCooperative26"> ดูมคอ.3-4 สมบูรณ์</font>
								</a>
							</td> 
								 
						</tr> ';
						 
					}
				}
				// debug($outputSpecialAndCooperativeEmployee);
					$this->set(array(
						'SpecialAndCooperatives' => $SpecialAndCooperatives,
						'outputSpecialAndCooperativeEmployee' => $outputSpecialAndCooperativeEmployee
					
				));
			}elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 2) {			 
				 
					$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
						'conditions' => array(
							'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 10,	
							'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
							'SpecialAndCooperative.degree_id' => $id,			
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					)); 
					$this->set(array(
						'SpecialAndCooperatives' => $SpecialAndCooperatives,
					
				));
			}elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 1) {			 
				 
					$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
						'conditions' => array(
							'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 10,	
							'SpecialAndCooperative.degree_id' => $id,			 
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					)); 
					$this->set(array(
						'SpecialAndCooperatives' => $SpecialAndCooperatives,
					 
				));
			} 
		}
 
	}
	public function list_result_pass_proposal_head_division($id=null){

		$UserName = $this->Session->read('MisEmployee');		
		$admins = $this->Session->read('adminCurriculumRequests');
		
		if(isset($UserName)){
			$this->set('UserName', $UserName);
			$this->set('admins', $admins);
			

			if($this->action == 'login')
				$this->redirect(array('action' => 'Dashboard'));
		}
		else{
			if($this->action != 'login')
			$this->redirect('https://mis.agri.cmu.ac.th/mis/User/dashboard');
			
		}
		$this->set(array(
			'admins' => $admins,
			'UserName' => $UserName,
			'id' => $id
		));
		if($id == null){ 
					$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
						'conditions' => array(
							'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 10,	
							'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],			
						),
						'order' => array('SpecialAndCooperative.modified' => 'DESC')
					)); 
					$this->set(array(
						'SpecialAndCooperatives' => $SpecialAndCooperatives,
					
				));
			 
		}else {
			 		 
				 
			$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all', array(
				'conditions' => array(
					'SpecialAndCooperative.special_and_cooperative_follow_status_id' => 10,	
					'SpecialAndCooperative.organize_id' => $admins['Admincurriculumrequest']['organize_id'],
					'SpecialAndCooperative.degree_id' => $id,			
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			)); 
			$this->set(array(
				'SpecialAndCooperatives' => $SpecialAndCooperatives,
			
			));
			 
		}
 
	} 
	public function add_proposals($id = null){
		$UserName = $this->Session->read('MisEmployee');		
		$admins = $this->Session->read('adminCurriculumRequests');
		
		if(isset($UserName)){
			$this->set('UserName', $UserName);
			$this->set('admins', $admins);
			// $this->redirect(array('action' => 'Dashboard'));
		}
		else{
			 
			$this->redirect('https://mis.agri.cmu.ac.th/mis/User/dashboard');
			
		}
		$this->set(array(
			'admins' => $admins,
			'UserName' => $UserName,
			
		));
		
		$organizes = $this->Organize->find('list',array(	
			'conditions' => array(
				'Organize.id ' => $admins['Admincurriculumrequest']['organize_id'],
					 
			), 
			'fields' => array('id','name'),
		));	
		//debug($organizes);
		$lastYearTerms = $this->Session->read('lastYearTerms');
	 
		if($this->request->data){
			date_default_timezone_set('Asia/Bangkok');
			 
		 
				 
				//***********************Save Proposal******************************* */
					$this->request->data['SpecialAndCooperative']['postdate'] = date('Y-m-d');
					// $this->request->data['SpecialAndCooperative']['yearterm_id'] = $lastYearTerms['Yearterm']['id'];
				    // $this->request->data['SpecialAndCooperative']['year'] = $lastYearTerms['Yearterm']['year'];
					$this->request->data['SpecialAndCooperative']['mis_employee_request_id'] = $UserName['MisEmployee']['id'];
				   
					if($admins['Admincurriculumrequest']['employee_status_id'] == 3){
						$this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'] = 3;
						$this->request->data['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] = 4;
						$this->request->data['SpecialAndCooperative']['special_and_cooperative_level_id'] = 2;
					}elseif($admins['Admincurriculumrequest']['employee_status_id'] == 2 ) {
						$this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'] = 3;
						$this->request->data['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] = 4;
						$this->request->data['SpecialAndCooperative']['special_and_cooperative_level_id'] = 2;
					}else {
						$this->request->data['SpecialAndCooperative']['special_and_cooperative_level_id'] = 2;
					}
				   
					$majors = $this->Majordegree->find('first',array(	
						'conditions' => array(
						 
							'and' => array(
							array('Majordegree.level <= ' => 2),
							array('Majordegree.level >= ' => 0)
							),
								'Majordegree.major_id ' => $this->request->data['SpecialAndCooperative']['major_id'],
								'Majordegree.degree_id ' => $id,
								// 'Majordegree.level' => 1
						),
						'fields' => array('Majordegree.major_id','Majordegree.major_name','Majordegree.organize_id'),
						'order' => array('Majordegree.major_id' => 'ASC'),
						'group' => array('Majordegree.major_id')
						
					));	
					
					 //debug($majors); 
				   
					$this->request->data['SpecialAndCooperative']['organize_id'] = $admins['Admincurriculumrequest']['organize_id'];
					$this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_postdate'] = date('Y-m-d', strtotime($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_postdate']));
					// $this->request->data['SpecialAndCooperative']['degree_id'] = 1;
			 
					$this->SpecialAndCooperative->save($this->request->data['SpecialAndCooperative']);

				//***********************Save LogSpecialAndCooperative******************************* */
					
					$studentEmails = $this->SpecialAndCooperative->find('first',array(
						'conditions' => array(
						 
							'SpecialAndCooperative.mis_employee_id' => $this->request->data['SpecialAndCooperative']['mis_employee_id'],							
							),
							'order' => array('SpecialAndCooperative.id' => 'DESC')
						));
					  
						if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'] == null) {
								
						}else {
							 
							$studentEmails = $this->SpecialAndCooperative->find('first',array(
								'conditions' => array(
									 
									'SpecialAndCooperative.mis_employee_id' => $this->request->data['SpecialAndCooperative']['mis_employee_id'],							
									),
									'order' => array('SpecialAndCooperative.id' => 'DESC')
								));
						 
							 
								//======================= เก็บ Log SpecialAndCooperative	====================================
								$this->request->data['LogSpecialAndCooperative']['status'] = 1;
								$this->request->data['LogSpecialAndCooperative']['special_and_cooperative_id'] = $studentEmails['SpecialAndCooperative']['id'];
								$this->request->data['LogSpecialAndCooperative']['mis_employee_id'] = $studentEmails['SpecialAndCooperative']['mis_employee_id'];
								$this->request->data['LogSpecialAndCooperative']['employee_status_id'] = $admins['Admincurriculumrequest']['employee_status_id'];	
								if($admins['Admincurriculumrequest']['employee_status_id'] == 3){
									
									if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_type_id'] == 1) {
										$this->request->data['LogSpecialAndCooperative']['PostName'] =
										 '<font color="orange">&nbsp;&nbsp;เสนอ มคอ.3-4 </font>  จาก <b>ระดับอาจารย์/ภาควิชา </b> เข้าที่ประชุมสาขา/ภาควิชา ครั้งที่ '.$this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_num'].' วันที่ '.date('Y-m-d', strtotime($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_postdate'])).' ';
									
									}elseif ($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_type_id'] == 2) {
										$this->request->data['LogSpecialAndCooperative']['PostName'] =
										 '<font color="orange">&nbsp;&nbsp;เสนอ มคอ.3-4 </font>  จาก <b>ระดับอาจารย์/ภาควิชา </b> แจ้งเวียน วันที่ '.date('Y-m-d', strtotime($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_postdate'])).' ';
									
									}
									// $this->request->data['LogSpecialAndCooperative']['PostName'] = '<font color="orange">&nbsp;&nbsp;เสนอ มคอ.3-4 </font>  จาก <b>ระดับอาจารย์/ภาควิชา </b>';
								}elseif($admins['Admincurriculumrequest']['employee_status_id'] == 2) {
									if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_type_id'] == 1) {
										$this->request->data['LogSpecialAndCooperative']['PostName'] =
										 '<font color="orange">&nbsp;&nbsp;เสนอ มคอ.3-4 </font>  จาก <b>ระดับภาควิชา </b> เข้าที่ประชุมสาขา/ภาควิชา ครั้งที่ '.$this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_num'].' วันที่ '.date('Y-m-d', strtotime($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_postdate'])).' ';
									
									}elseif ($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_type_id'] == 2) {
										$this->request->data['LogSpecialAndCooperative']['PostName'] =
										 '<font color="orange">&nbsp;&nbsp;เสนอ มคอ.3-4 </font>  จาก <b>ระดับภาควิชา </b> แจ้งเวียน วันที่ '.date('Y-m-d', strtotime($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_postdate'])).' ';
									
									}
									
									// $this->request->data['LogSpecialAndCooperative']['PostName'] = '<font color="orange">&nbsp;&nbsp;เสนอ มคอ.3-4 </font>  จาก <b>ระดับภาควิชา </b>';

								}elseif($admins['Admincurriculumrequest']['employee_status_id'] == 1) {
									if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_type_id'] == 1) {
										$this->request->data['LogSpecialAndCooperative']['PostName'] =
										 '<font color="orange">&nbsp;&nbsp;เสนอ มคอ.3-4 </font>  จาก <b>ระดับคณะ </b> เข้าที่ประชุมสาขา/ภาควิชา ครั้งที่ '.$this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_num'].' วันที่ '.date('Y-m-d', strtotime($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_postdate'])).' ';
									
									}elseif ($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_type_id'] == 2) {
										$this->request->data['LogSpecialAndCooperative']['PostName'] =
										 '<font color="orange">&nbsp;&nbsp;เสนอ มคอ.3-4 </font>  จาก <b>ระดับคณะ </b> แจ้งเวียน วันที่ '.date('Y-m-d', strtotime($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_postdate'])).' ';
									
									}
									// $this->request->data['LogSpecialAndCooperative']['PostName'] = '<font color="orange">&nbsp;&nbsp;เสนอ มคอ.3-4 </font>  จาก <b>ระดับคณะ </b>';

								}	   
			
									 
								 
								$this->LogSpecialAndCooperative->save($this->request->data['LogSpecialAndCooperative']);	
						
							
								
							 
 
						} 
				//***********************Save SpecialAndCooperativeEmployee*********************** */			
					$data = array();
						 
					//**********************************กรรมการคนที่ 1 ประธาน********************************** */  
						// debug($this->request->data['SpecialAndCooperative']['advisor_id1']);
						if ($this->request->data['SpecialAndCooperative']['advisor_id1'] == null || $this->request->data['SpecialAndCooperative']['advisor_id1'] == '00' ) {
								
						}else {							
							$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id1']);
							// debug($advisors);
							if ($advisors['Advisor']['mis_employee_id'] == null ) {
								
							}else {
								$check_employees = $this->MisEmployee->find('first' ,array(
									'conditions' => array(
										'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
										),						
									'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
								));	
							}
							
							$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
								'conditions' => array(
									// 'SpecialAndCooperative.mis_employee_id' => $this->request->data['SpecialAndCooperative']['mis_employee_id'],
								),					
								'order' => array('SpecialAndCooperative.id' => 'DESC')
							));	
							// $this->request->data['SpecialAndCooperative']['mis_employee_id'],
							// $UserName['MisEmployee']['id']
							$data2 = array(
								
								'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
								'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id1'],
								
								);

						
							array_push($data,$data2);				

							
							
						}

					//**********************************กรรมการคนที่ 2********************************** */  
						if ($this->request->data['SpecialAndCooperative']['advisor_id2'] == null || $this->request->data['SpecialAndCooperative']['advisor_id2'] == '00' ) {
								
						}else {							
							$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id2']);
							// debug($advisors);
							if ($advisors['Advisor']['mis_employee_id'] == null ) {
								
							}else {
								$check_employees = $this->MisEmployee->find('first' ,array(
									'conditions' => array(
										'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
										),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
								));	
							}
							
							$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
								'conditions' => array(
									// 'SpecialAndCooperative.mis_employee_id' => $UserName['MisEmployee']['id'] ,	
								),					
								'order' => array('SpecialAndCooperative.id' => 'DESC')
							));	
						
							
							$data2 = array(
								
								'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
								'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id2'],
								
								);

						
							array_push($data,$data2);				

							
							
						}

					//**********************************กรรมการคนที่ 3********************************** */  
						if ($this->request->data['SpecialAndCooperative']['advisor_id3'] == null || $this->request->data['SpecialAndCooperative']['advisor_id3'] == '00' ) {
								
						}else {							
							$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id3']);
							//debug($advisors);
							if ($advisors['Advisor']['mis_employee_id'] == null ) {
								
							}else {
								$check_employees = $this->MisEmployee->find('first' ,array(
									'conditions' => array(
										'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
										),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
								));	
							}
							
							$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
								'conditions' => array(
									// 'SpecialAndCooperative.mis_employee_id' => $UserName['MisEmployee']['id'] ,	
								),					
								'order' => array('SpecialAndCooperative.id' => 'DESC')
							));	
						
							
							$data2 = array(
								
								'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
								'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id3'],
								
								);

						
							array_push($data,$data2);				

							
							
						}

					//**********************************กรรมการคนที่ 4********************************** */  
						if ($this->request->data['SpecialAndCooperative']['advisor_id4'] == null || $this->request->data['SpecialAndCooperative']['advisor_id4'] == '00' ) {
								
						}else {							
							$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id4']);
							// debug($advisors);
							if ($advisors['Advisor']['mis_employee_id'] == null ) {
								
							}else {
								$check_employees = $this->MisEmployee->find('first' ,array(
									'conditions' => array(
										'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
										),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
								));	
							}
							
							$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
								'conditions' => array(
									// 'SpecialAndCooperative.mis_employee_id' => $UserName['MisEmployee']['id'] ,	
								),					
								'order' => array('SpecialAndCooperative.id' => 'DESC')
							));	
						
							
							$data2 = array(
								
								'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
								'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id4'],
								
								);

						
							array_push($data,$data2);				

							
							
						}

					//**********************************กรรมการคนที่ 5********************************** */  
						if ($this->request->data['SpecialAndCooperative']['advisor_id5'] == null || $this->request->data['SpecialAndCooperative']['advisor_id5'] == '00' ) {
								
						}else {							
							$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id5']);
							// debug($advisors);
							if ($advisors['Advisor']['mis_employee_id'] == null ) {
								
							}else {
								$check_employees = $this->MisEmployee->find('first' ,array(
									'conditions' => array(
										'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
										),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
								));	
							}
							
							$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
								'conditions' => array(
									// 'SpecialAndCooperative.mis_employee_id' => $UserName['MisEmployee']['id'] ,	
								),					
								'order' => array('SpecialAndCooperative.id' => 'DESC')
							));	
						
							
							$data2 = array(
								
								'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
								'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id5'],
								
								);

						
							array_push($data,$data2);				

							
							
						}
					//**********************************กรรมการคนที่ 6********************************** */  
					if ($this->request->data['SpecialAndCooperative']['advisor_id6'] == null || $this->request->data['SpecialAndCooperative']['advisor_id6'] == '00' ) {
								
					}else {							
						$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id6']);
						// debug($advisors);
						if ($advisors['Advisor']['mis_employee_id'] == null ) {
							
						}else {
							$check_employees = $this->MisEmployee->find('first' ,array(
								'conditions' => array(
									'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
									),						
									'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
							));	
						}
						
						$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
							'conditions' => array(
								// 'SpecialAndCooperative.mis_employee_id' => $UserName['MisEmployee']['id'] ,	
							),					
							'order' => array('SpecialAndCooperative.id' => 'DESC')
						));	
					
						
						$data2 = array(
							
							'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
							'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id6'],
							
							);

					
						array_push($data,$data2);				

						
						
					}
					//**********************************กรรมการคนที่ 7********************************** */  
					if ($this->request->data['SpecialAndCooperative']['advisor_id7'] == null || $this->request->data['SpecialAndCooperative']['advisor_id7'] == '00' ) {
								
					}else {							
						$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id7']);
						// debug($advisors);
						if ($advisors['Advisor']['mis_employee_id'] == null ) {
							
						}else {
							$check_employees = $this->MisEmployee->find('first' ,array(
								'conditions' => array(
									'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
									),						
									'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
							));	
						}
						
						$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
							'conditions' => array(
								// 'SpecialAndCooperative.mis_employee_id' => $UserName['MisEmployee']['id'] ,	
							),					
							'order' => array('SpecialAndCooperative.id' => 'DESC')
						));	
					
						
						$data2 = array(
							
							'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
							'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id7'],
							
							);

					
						array_push($data,$data2);				

						
						
					}
					//**********************************กรรมการคนที่ 8********************************** */  
					if ($this->request->data['SpecialAndCooperative']['advisor_id8'] == null || $this->request->data['SpecialAndCooperative']['advisor_id8'] == '00' ) {
								
					}else {							
						$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id8']);
						// debug($advisors);
						if ($advisors['Advisor']['mis_employee_id'] == null ) {
							
						}else {
							$check_employees = $this->MisEmployee->find('first' ,array(
								'conditions' => array(
									'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
									),						
									'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
							));	
						}
						
						$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
							'conditions' => array(
								// 'SpecialAndCooperative.mis_employee_id' => $UserName['MisEmployee']['id'] ,	
							),					
							'order' => array('SpecialAndCooperative.id' => 'DESC')
						));	
					
						
						$data2 = array(
							
							'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
							'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id8'],
							
							);

					
						array_push($data,$data2);				

						
						
					}
					//**********************************กรรมการคนที่ 9********************************** */  
					if ($this->request->data['SpecialAndCooperative']['advisor_id9'] == null || $this->request->data['SpecialAndCooperative']['advisor_id9'] == '00' ) {
								
					}else {							
						$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id9']);
						// debug($advisors);
						if ($advisors['Advisor']['mis_employee_id'] == null ) {
							
						}else {
							$check_employees = $this->MisEmployee->find('first' ,array(
								'conditions' => array(
									'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
									),						
									'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
							));	
						}
						
						$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
							'conditions' => array(
								// 'SpecialAndCooperative.mis_employee_id' => $UserName['MisEmployee']['id'] ,	
							),					
							'order' => array('SpecialAndCooperative.id' => 'DESC')
						));	
					
						
						$data2 = array(
							
							'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
							'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id9'],
							
							);

					
						array_push($data,$data2);				

						
						
					}	
					//**********************************กรรมการคนที่ 10********************************** */  
					if ($this->request->data['SpecialAndCooperative']['advisor_id10'] == null || $this->request->data['SpecialAndCooperative']['advisor_id10'] == '00' ) {
								
					}else {							
						$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id10']);
						// debug($advisors);
						if ($advisors['Advisor']['mis_employee_id'] == null ) {
							
						}else {
							$check_employees = $this->MisEmployee->find('first' ,array(
								'conditions' => array(
									'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
									),						
									'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
							));	
						}
						
						$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
							'conditions' => array(
								// 'SpecialAndCooperative.mis_employee_id' => $UserName['MisEmployee']['id'] ,	
							),					
							'order' => array('SpecialAndCooperative.id' => 'DESC')
						));	
					
						
						$data2 = array(
							
							'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
							'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id10'],
							
							);

					
						array_push($data,$data2);				

						
						
					}
					$this->SpecialAndCooperativeEmployee->saveMany($data);
				//***********************Save ProposalDocument*********************** */
						$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first',array(
							'conditions' => array(
								// 'SpecialAndCooperative.mis_employee_id' => $UserName['MisEmployee']['id'] ,							
								),
								'order' => array('SpecialAndCooperative.id' => 'DESC')
							));	
							
					$files = $this->request->data['SpecialAndCooperativeDocument']['files'];	
					if ($this->request->data['SpecialAndCooperativeDocument']['files'][0]['type'] != null) {
						foreach ($this->request->data['SpecialAndCooperativeDocument']['files'] as $check) {
							if($check['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif" or "application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document" or "application/x-rar-compressed" or "application/octet-stream" or "application/zip" or "application/octet-stream" or "application/vnd.ms-powerpoint" or "application/vnd.openxmlformats-officedocument.presentationml.presentation" or "application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")){


								foreach ($files as $file) {

									$fileType = pathinfo($file['name'],PATHINFO_EXTENSION);
									if($fileType != "doc" or "docx" or "jpg" or "jpeg" or "png" or "gif" or "zip" or "rar" or "xls" or "xlsx" or "ppt" or "pptx" or "pdf"){

										$this->Session->setFlash('กรุณาแก้ไขชื่อไฟล์ก่อนอัปโหลด โดยไม่ให้มีอักขระ จุด .');
										$this->Session->write('alertType','danger');
									
										$this->redirect(array('controller' => 'specialandCooperatives','action' => 'add_proposals'));
									}
								}


								$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
								$this->Session->write('alertType','danger');
								
							}
						}
					}
						$numcount = $this->SpecialAndCooperativeDocument->find('count',array(
									'conditions' => array(
										'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id']
									),
								));
					if($files[0]['name'] != null){
						$checkLastLogSpecialAndCooperative = $this->LogSpecialAndCooperative->find('first',array(
							'conditions' => array(				
								// 'LogSpecialAndCooperative.special_and_cooperative_id' => $proposal_id,
								// 'LogSpecialAndCooperative.mis_employee_id' => $UserName['MisEmployee']['id'] ,
							),
							'order' => array('LogSpecialAndCooperative.id' => 'DESC')
						)); 
						$i = 0;
						foreach ($files as $file) {
							if ($numcount == null) {
								$numcount = 1;
							}else {							
								$numcount = $numcount++;
							}
							$fileType = pathinfo($file['name'],PATHINFO_EXTENSION);
								
							$this->request->data['SpecialAndCooperativeDocument']['name'] = $numcount."_".$file['name'];
							$this->request->data['SpecialAndCooperativeDocument']['name_old'] = $file['name'];	
							$this->request->data['SpecialAndCooperativeDocument']['postdate'] = date('Y-m-d H:i:00');		
							$this->request->data['SpecialAndCooperativeDocument']['special_and_cooperative_id'] = $SpecialAndCooperatives['SpecialAndCooperative']['id'];
							$this->request->data['SpecialAndCooperativeDocument']['log_special_and_cooperative_id'] = $checkLastLogSpecialAndCooperative['LogSpecialAndCooperative']['id'];
							$this->request->data['SpecialAndCooperativeDocument']['mis_employee_id'] = $UserName['MisEmployee']['id'];
	
								$filethai = iconv("UTF-8", "TIS-620",  $numcount."_".$file['name']);
								
							
								if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
									$this->request->data['SpecialAndCooperativeDocument']['class'] = "file";
								}else{
									$this->request->data['SpecialAndCooperativeDocument']['class'] = "img";
								}
								
								$folder = WWW_ROOT.'files'.DS.'students'.DS.'specialandcooperatives'.DS.$SpecialAndCooperatives['SpecialAndCooperative']['id'].DS;
			
								if(!file_exists($folder))
									mkdir($folder);
									
								move_uploaded_file($file['tmp_name'],$folder.$filethai);
								// $files[0]['tmp_name']
								
							
							$this->SpecialAndCooperativeDocument->create();					
							$this->SpecialAndCooperativeDocument->save($this->request->data);	
							
						}

					} 
						// $fileDOC = $this->request->data['SpecialAndCooperativeDocument']['fileDOC'];
							// $filePDF = $this->request->data['SpecialAndCooperativeDocument']['filePDF'];	
							 			
							
								 
							// 	$pdf = null;
							// 	$doc = null;
			
							// 	if ($fileDOC[0]['name'] != null) {
							// 		$fileType = pathinfo($fileDOC[0]['name'],PATHINFO_EXTENSION);
							// 		if($fileDOC[0]['type'] != ("application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document"or "application/vnd.openxmlformats-officedocument.wordprocessingml.document")){
			
							// 			$this->Session->setFlash('ไฟล์ DOC อัฟโหลดไม่ถูกต้อง');
							// 			$this->Session->write('alertType','danger');							
							// 			$this->redirect(array('controller' => 'proposals','action' => 'add_proposals'));
										
							// 		}else{
							// 			$doc = true;
							// 		}
							// 	}
			
							// 	if ($filePDF[0]['name'] != null) {
							// 		$fileType = pathinfo($filePDF[0]['name'],PATHINFO_EXTENSION);
							// 		if($filePDF[0]['type'] != ('application/pdf')){
			
							// 			$this->Session->setFlash('ไฟล์ PDF อัฟโหลดไม่ถูกต้อง');
							// 			$this->Session->write('alertType','danger');
							// 			$this->redirect(array('controller' => 'proposals','action' => 'add_proposals'));						
									
										
							// 		}else{
							// 			$pdf = true;
							// 		}
							// 	}
								
								 
							// 	$numcount = $this->SpecialAndCooperativeDocument->find('count',array(
							// 		'conditions' => array(
							// 			'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id']
							// 		),
							// 	));
							// 	if($fileDOC[0]['name'] != null){
							// 		if ($numcount == null) {
							// 			$numcount = 1;
							// 		}else {							
							// 			$numcount = $numcount++;
							// 		}
						
							// 		$i = date("m");
							// 		foreach ($fileDOC as $fileDOC) {
							// 		$fileType = pathinfo($fileDOC['name'],PATHINFO_EXTENSION);
								
							// 		$this->request->data['SpecialAndCooperativeDocument']['name'] = $numcount."_".$fileDOC['name'];
							// 		$this->request->data['SpecialAndCooperativeDocument']['name_old'] = $fileDOC['name'];	
							// 		$this->request->data['SpecialAndCooperativeDocument']['postdate'] = date('Y-m-d H:i:00');		
							// 		$this->request->data['SpecialAndCooperativeDocument']['special_and_cooperative_id'] = $SpecialAndCooperatives['SpecialAndCooperative']['id'];
							// 		$this->request->data['SpecialAndCooperativeDocument']['mis_employee_id'] = $UserName['MisEmployee']['id'];
			
									
			
							// 			$filethai = iconv("UTF-8", "TIS-620",  $numcount."_".$fileDOC['name']);
										
									
							// 			if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
							// 				$this->request->data['SpecialAndCooperativeDocument']['class'] = "file";
							// 			}else{
							// 				$this->request->data['SpecialAndCooperativeDocument']['class'] = "img";
							// 			}
										
							// 			$folder = WWW_ROOT.'files'.DS.'students'.DS.'specialandcooperatives'.DS.$SpecialAndCooperatives['SpecialAndCooperative']['id'].DS;
					
							// 			if(!file_exists($folder))
							// 				mkdir($folder);
											
							// 			move_uploaded_file($fileDOC['tmp_name'],$folder.$filethai);
									
									
							// 		$this->SpecialAndCooperativeDocument->create();					
							// 		$this->SpecialAndCooperativeDocument->save($this->request->data);						
							// 		}
							// 	}
			
							// 	if($filePDF[0]['name'] != null){
							// 		if ($numcount == null) {
							// 			$numcount = 1;
							// 		}else {							
							// 			$numcount = $numcount++;
							// 		}
							// 		$i = date("m");
							// 		foreach ($filePDF as $filePDF) {
							// 		$fileType = pathinfo($filePDF['name'],PATHINFO_EXTENSION);
								
							// 		$this->request->data['SpecialAndCooperativeDocument']['name'] = $numcount."_".$filePDF['name'];
							// 		$this->request->data['SpecialAndCooperativeDocument']['name_old'] = $filePDF['name'];	
							// 		$this->request->data['SpecialAndCooperativeDocument']['postdate'] = date('Y-m-d H:i:00');		
							// 		$this->request->data['SpecialAndCooperativeDocument']['special_and_cooperative_id'] = $SpecialAndCooperatives['SpecialAndCooperative']['id'];
							// 		$this->request->data['SpecialAndCooperativeDocument']['mis_employee_id'] = $UserName['MisEmployee']['id'];
			
							// 			$filethai = iconv("UTF-8", "TIS-620",  $numcount."_".$filePDF['name']);
										
									
							// 			if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
							// 				$this->request->data['SpecialAndCooperativeDocument']['class'] = "file";
							// 			}else{
							// 				$this->request->data['SpecialAndCooperativeDocument']['class'] = "img";
							// 			}
										
							// 			$folder = WWW_ROOT.'files'.DS.'students'.DS.'specialandcooperatives'.DS.$SpecialAndCooperatives['SpecialAndCooperative']['id'].DS;
					
							// 			if(!file_exists($folder))
							// 				mkdir($folder);
											
							// 			move_uploaded_file($filePDF['tmp_name'],$folder.$filethai);
							// 			// $files[0]['tmp_name']
										
									
							// 		$this->SpecialAndCooperativeDocument->create();					
							// 		$this->SpecialAndCooperativeDocument->save($this->request->data);						
							// 		}
							// 	}		 
						 
			
										
						
				
			    //*************save************* */		
					 		
				try{
					//======================= ส่งหาคณะ	====================================	
					if($admins['Admincurriculumrequest']['employee_status_id'] != 1) {
						$studentEmails = $this->SpecialAndCooperative->find('first',array(
							'conditions' => array(
									'SpecialAndCooperative.id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],							
								),
								'order' => array('SpecialAndCooperative.id' => 'DESC')
							));				
						// $adminProposals = $this->Admincurriculumrequest->find('all', array(
						// 	'conditions' => array(
						// 		'employee_status_id' => 1
						// 	)
						// ));


						// $adminProposals = $this->Admincurriculumrequest->find('all', array(
						// 	'conditions' => array(
						// 		'mis_employee_id' => 793
						// 	)
						// ));
						// $tests = array();
						// $tests2 = array();
						// foreach($adminProposals as $user)
						// {
						// 	$tests[] = $user['MisEmployee']['email_cmu'];
						// 	$tests2[] = $user['MisEmployee']['email_other'];
						// }
						
						// $email = new CakeEmail('gmail');
						// $email->to($tests) 									
						// 			->template('special_and_cooperative_confirmation')
						// 			->emailFormat('html')
						// 			->viewVars(array('studentEmails' => $studentEmails))
						// 			->helpers('Html')
						// 			->from('agriculturecmu@gmail.com')												
						// 			->subject('e-TQF.3-4 Notification - MAJOR CODE'.$studentEmails['Major']['major_name'].'')
						// 			->send(); 

						// $email = new CakeEmail('gmail');
						// $email->to($tests2) 									
						// 			->template('special_and_cooperative_confirmation')
						// 			->emailFormat('html')
						// 			->viewVars(array('studentEmails' => $studentEmails))
						// 			->helpers('Html')
						// 			->from('agriculturecmu@gmail.com')												
						// 			->subject('e-TQF.3-4 Notification - MAJOR CODE'.$studentEmails['Major']['major_name'].'')
						// 			->send(); 
						
					}		
				//======================= ปิดการส่งอีเมล์	====================================
				}catch(Exception $e){
					
					$this->Session->setFlash('<i class="glyphicon glyphicon-remove-sign"></i> ขออภัย เกิดเหตุขัดข้องของการส่งอีเมล์ไปยังบุคลากร กรุณาเช็คอีเมล์บุคลากรให้ถูกต้อง');
					$this->Session->write('alertType','danger');
					$this->redirect(array('action' => 'list_result_manage_proposal',$id));	

					 			
					
				}
			
				$this->Session->write('alertType','success');
				$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
				$this->redirect(array('action' => 'list_result_manage_proposal',$id));	
			 
		}
		 
		$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all',array(
			'conditions' => array(
				'SpecialAndCooperative.mis_employee_id' => $UserName['MisEmployee']['id'] ,							
				),
				'order' => array('SpecialAndCooperative.postdate' => 'DESC')
			));	
		$advisors = $this->Advisor->find('all', array(
			'conditions' => array(
				'Advisor.level' => 1,							
				),
			'fields' => array('id','advisor_code','advisor_title','advisor_name','advisor_sname','mis_employee_id'),
			'order' => array('id' => 'ASC')
		));

		$outputadvisor = '';		
		foreach ($advisors as $advisor) {
			$outputadvisor .= '
				<option value="'.$advisor['Advisor']['mis_employee_id'].'">'.$advisor['Advisor']['advisor_code'].' '.$advisor['Advisor']['advisor_title'].$advisor['Advisor']['advisor_name'].' '.$advisor['Advisor']['advisor_sname'].'</option>
			';
		}
		
		 
		$yearterms = $this->Yearterm->find('list',array(
			'conditions' => array(
				'and' => array(
					array('Yearterm.level <= ' => 2),
					array('Yearterm.level >= ' => 1)
					),
			),
			'limit' => 11,
			'fields' => array('Yearterm.id','Yearterm.name'),
			'order' => array('Yearterm.id' => 'DESC'),
			
		));						
		$degrees = $this->Degree->find('list',array(
			'conditions' => array(
				 
			), 
			'fields' => array('Degree.id','Degree.degree_name'),
			'order' => array('id' => 'ASC'),
			
		));	
		if($admins['Admincurriculumrequest']['employee_status_id'] == 3 || $admins['Admincurriculumrequest']['employee_status_id'] == 2){
			$majors = $this->Majordegree->find('list',array(	
				'conditions' => array(
					// 'or' => array(
					// 	array('Majordegree.degree_id <= ' => 5),
					// 	array('Majordegree.degree_id >= ' => 3)
					// 	),
					'and' => array(
					array('Majordegree.level <= ' => 2),
					array('Majordegree.level >= ' => 1)
					),
						'Majordegree.organize_id ' => $admins['Admincurriculumrequest']['organize_id'] ,
						'Majordegree.degree_id ' => $id,
						// 'Majordegree.level' => 1
				),
				'fields' => array('Majordegree.major_id','Majordegree.major_name'),
				'order' => array('Majordegree.major_id' => 'ASC'),
				'group' => array('Majordegree.major_id')
				
			));	

		}else {
			$majors = $this->Majordegree->find('list',array(	
				'conditions' => array(
					// 'or' => array(
					// 	array('Majordegree.degree_id <= ' => 5),
					// 	array('Majordegree.degree_id >= ' => 3)
					// 	),
					'and' => array(
					array('Majordegree.level <= ' => 2),
					array('Majordegree.level >= ' => 0)
					),
						// 'Majordegree.organize_id ' => $admins['Admincurriculumrequest']['organize_id'] ,
						'Majordegree.degree_id ' => $id,
						// 'Majordegree.level' => 1
				),
				'fields' => array('Majordegree.major_id','Majordegree.major_name'),
				'order' => array('Majordegree.major_id' => 'ASC'),
				'group' => array('Majordegree.major_id')
				
			));	
			$passes = $this->SpecialAndCooperativePass->find('list',array(
				'conditions' => array(			
					'and' => array(
						array('SpecialAndCooperativePass.level <= ' => 2),
						array('SpecialAndCooperativePass.level >= ' => 1)
						),
					),
					
				)); 
			$SpecialAndCooperativeFollowpasses = $this->SpecialAndCooperativeFollowStatus->find('list',array(
				'conditions' => array(			
					'SpecialAndCooperativeFollowStatus.level' => 1,
					),
					
				));	
			$this->set(array( 
				'passes' => $passes,
				'SpecialAndCooperativeFollowpasses' => $SpecialAndCooperativeFollowpasses
			));	
		}
		$SpecialAndCooperativeTypes = $this->SpecialAndCooperativeType->find('list',array(
			'conditions' => array(			
				 
				),
				
			)); 
		 //debug($majors);
		 $coursegroups = $this->Coursegroup->find('list',array(
			'conditions' => array(
				'level' => 1
			), 
			'fields' => array('Coursegroup.id','Coursegroup.name2'),
			'order' => array('id' => 'ASC'),
			
		));	
		$this->set(array( 
			'majors' => $majors,
			'degrees' => $degrees,
			'yearterms' => $yearterms, 
			'SpecialAndCooperatives' => $SpecialAndCooperatives, 
			'id' => $id	,
			'outputadvisor' => $outputadvisor,
			'specialandCooperativeTypes' => $specialandCooperativeTypes,
			'organizes' => $organizes,
			));

	} 
	public function add_pass_proposals($id = null){
		$UserName = $this->Session->read('MisEmployee');		
		$admins = $this->Session->read('adminCurriculumRequests');
		
		if(isset($UserName)){
			$this->set('UserName', $UserName);
			$this->set('admins', $admins);
			
 		 
		}
		else{
			$this->redirect('https://mis.agri.cmu.ac.th/mis/User/dashboard');
			
		}
		$this->set(array(
			'admins' => $admins,
			'UserName' => $UserName,
			
		));
		$lastYearTerms = $this->Session->read('lastYearTerms');
	 
		if($this->request->data){
			date_default_timezone_set('Asia/Bangkok');
			 
			$checkyearterms = $this->Yearterm->find('first',array(
				'conditions' => array(
					'Yearterm.id' => $this->request->data['SpecialAndCooperative']['yearterm_id']
				), 
				'order' => array('Yearterm.id' => 'DESC'),
				
			));	
				 
				//***********************Save Proposal******************************* */
					$this->request->data['SpecialAndCooperative']['postdate'] = date('Y-m-d');
					$this->request->data['SpecialAndCooperative']['yearterm_id'] = $checkyearterms['Yearterm']['id'];
				    $this->request->data['SpecialAndCooperative']['year'] = $checkyearterms['Yearterm']['year'];
				    $this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'] = 4;
					$this->request->data['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] = 10;
					 
					$majors = $this->Majordegree->find('first',array(	
						'conditions' => array(
						 
							'and' => array(
							array('Majordegree.level <= ' => 2),
							array('Majordegree.level >= ' => 0)
							),
								'Majordegree.major_id ' => $this->request->data['SpecialAndCooperative']['major_id'],
								'Majordegree.degree_id ' => $id,
								// 'Majordegree.level' => 1
						),
						'fields' => array('Majordegree.major_id','Majordegree.major_name','Majordegree.organize_id'),
						'order' => array('Majordegree.major_id' => 'ASC'),
						'group' => array('Majordegree.major_id')
						
					));	
					 //debug($majors); 
				   
					// $this->request->data['SpecialAndCooperative']['organize_id'] = $majors['Majordegree']['organize_id'];
					// $this->request->data['SpecialAndCooperative']['degree_id'] = 1;
				  
			 
					$this->SpecialAndCooperative->save($this->request->data['SpecialAndCooperative']);

				//***********************Save SpecialAndCooperativeEmployee*********************** */			
					$data = array();
					 
					
						//**********************************กรรมการคนที่ 1 ประธาน********************************** */  
						// debug($this->request->data['SpecialAndCooperative']['advisor_id1']);
							if ($this->request->data['SpecialAndCooperative']['advisor_id1'] == null || $this->request->data['SpecialAndCooperative']['advisor_id1'] == '00' ) {
									
							}else {							
								$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id1']);
								// debug($advisors);
								if ($advisors['Advisor']['mis_employee_id'] == null ) {
									
								}else {
									$check_employees = $this->MisEmployee->find('first' ,array(
										'conditions' => array(
											'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
											),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
									));	
								}
								
								$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
									'conditions' => array(
										// 'SpecialAndCooperative.student_id' => $Students['Student']['id'],
									),					
									'order' => array('SpecialAndCooperative.id' => 'DESC')
								));	
							
								
								$data2 = array(
									
									'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
									'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id1'],
									'order' => 1,
									
									);

							
								array_push($data,$data2);				

								
								
							}

						//**********************************กรรมการคนที่ 2********************************** */  
							if ($this->request->data['SpecialAndCooperative']['advisor_id2'] == null || $this->request->data['SpecialAndCooperative']['advisor_id2'] == '00' ) {
									
							}else {							
								$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id2']);
								// debug($advisors);
								if ($advisors['Advisor']['mis_employee_id'] == null ) {
									
								}else {
									$check_employees = $this->MisEmployee->find('first' ,array(
										'conditions' => array(
											'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
											),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
									));	
								}
								
								$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
									'conditions' => array(
										// 'SpecialAndCooperative.student_id' => $Students['Student']['id'],
									),					
									'order' => array('SpecialAndCooperative.id' => 'DESC')
								));	
							
								
								$data2 = array(
									
									'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
									'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id2'],
									'order' => 2,
									);

							
								array_push($data,$data2);				

								
								
							}

						//**********************************กรรมการคนที่ 3********************************** */  
							if ($this->request->data['SpecialAndCooperative']['advisor_id3'] == null || $this->request->data['SpecialAndCooperative']['advisor_id3'] == '00' ) {
									
							}else {							
								$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id3']);
								// debug($advisors);
								if ($advisors['Advisor']['mis_employee_id'] == null ) {
									
								}else {
									$check_employees = $this->MisEmployee->find('first' ,array(
										'conditions' => array(
											'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
											),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
									));	
								}
								
								$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
									'conditions' => array(
										// 'SpecialAndCooperative.student_id' => $Students['Student']['id'],
									),					
									'order' => array('SpecialAndCooperative.id' => 'DESC')
								));	
							
								
								$data2 = array(
									
									'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
									'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id3'],
									'order' => 3,
									);

							
								array_push($data,$data2);				

								
								
							}

						//**********************************กรรมการคนที่ 4********************************** */  
							if ($this->request->data['SpecialAndCooperative']['advisor_id4'] == null || $this->request->data['SpecialAndCooperative']['advisor_id4'] == '00' ) {
									
							}else {							
								$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id4']);
								// debug($advisors);
								if ($advisors['Advisor']['mis_employee_id'] == null ) {
									
								}else {
									$check_employees = $this->MisEmployee->find('first' ,array(
										'conditions' => array(
											'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
											),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
									));	
								}
								
								$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
									'conditions' => array(
										// 'SpecialAndCooperative.student_id' => $Students['Student']['id'],
									),					
									'order' => array('SpecialAndCooperative.id' => 'DESC')
								));	
							
								
								$data2 = array(
									
									'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
									'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id4'],
									'order' => 4,
									);

							
								array_push($data,$data2);				

								
								
							}

						//**********************************กรรมการคนที่ 5********************************** */  
							if ($this->request->data['SpecialAndCooperative']['advisor_id5'] == null || $this->request->data['SpecialAndCooperative']['advisor_id5'] == '00' ) {
									
							}else {							
								$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id5']);
								// debug($advisors);
								if ($advisors['Advisor']['mis_employee_id'] == null ) {
									
								}else {
									$check_employees = $this->MisEmployee->find('first' ,array(
										'conditions' => array(
											'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
											),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
									));	
								}
								
								$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
									'conditions' => array(
										// 'SpecialAndCooperative.student_id' => $Students['Student']['id'],
									),					
									'order' => array('SpecialAndCooperative.id' => 'DESC')
								));	
							
								
								$data2 = array(
									
									'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
									'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id5'],
									'order' => 5,
									);

							
								array_push($data,$data2);				

								
								
							}
						//**********************************กรรมการคนที่ 6********************************** */  
							if ($this->request->data['SpecialAndCooperative']['advisor_id6'] == null || $this->request->data['SpecialAndCooperative']['advisor_id6'] == '00' ) {
										
							}else {							
								$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id6']);
								// debug($advisors);
								if ($advisors['Advisor']['mis_employee_id'] == null ) {
									
								}else {
									$check_employees = $this->MisEmployee->find('first' ,array(
										'conditions' => array(
											'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
											),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
									));	
								}
								
								$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
									'conditions' => array(
										// 'SpecialAndCooperative.student_id' => $Students['Student']['id'],
									),					
									'order' => array('SpecialAndCooperative.id' => 'DESC')
								));	
							
								
								$data2 = array(
									
									'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
									'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id6'],
									'order' => 6,
									);

							
								array_push($data,$data2);				

								
								
							}
						//**********************************กรรมการคนที่ 7********************************** */  
							if ($this->request->data['SpecialAndCooperative']['advisor_id7'] == null || $this->request->data['SpecialAndCooperative']['advisor_id7'] == '00' ) {
										
							}else {							
								$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id7']);
								// debug($advisors);
								if ($advisors['Advisor']['mis_employee_id'] == null ) {
									
								}else {
									$check_employees = $this->MisEmployee->find('first' ,array(
										'conditions' => array(
											'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
											),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
									));	
								}
								
								$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
									'conditions' => array(
										// 'SpecialAndCooperative.student_id' => $Students['Student']['id'],
									),					
									'order' => array('SpecialAndCooperative.id' => 'DESC')
								));	
							
								
								$data2 = array(
									
									'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
									'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id7'],
									'order' => 7,
									);

							
								array_push($data,$data2);				

								
								
							}
						//**********************************กรรมการคนที่ 8********************************** */  
							if ($this->request->data['SpecialAndCooperative']['advisor_id8'] == null || $this->request->data['SpecialAndCooperative']['advisor_id8'] == '00' ) {
										
							}else {							
								$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id8']);
								// debug($advisors);
								if ($advisors['Advisor']['mis_employee_id'] == null ) {
									
								}else {
									$check_employees = $this->MisEmployee->find('first' ,array(
										'conditions' => array(
											'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
											),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
									));	
								}
								
								$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
									'conditions' => array(
										// 'SpecialAndCooperative.student_id' => $Students['Student']['id'],
									),					
									'order' => array('SpecialAndCooperative.id' => 'DESC')
								));	
							
								
								$data2 = array(
									
									'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
									'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id8'],
									'order' => 8,
									);

							
								array_push($data,$data2);				

								
								
							}
						//**********************************กรรมการคนที่ 9********************************** */  
							if ($this->request->data['SpecialAndCooperative']['advisor_id9'] == null || $this->request->data['SpecialAndCooperative']['advisor_id9'] == '00' ) {
										
							}else {							
								$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id9']);
								// debug($advisors);
								if ($advisors['Advisor']['mis_employee_id'] == null ) {
									
								}else {
									$check_employees = $this->MisEmployee->find('first' ,array(
										'conditions' => array(
											'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
											),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
									));	
								}
								
								$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
									'conditions' => array(
										// 'SpecialAndCooperative.student_id' => $Students['Student']['id'],
									),					
									'order' => array('SpecialAndCooperative.id' => 'DESC')
								));	
							
								
								$data2 = array(
									
									'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
									'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id9'],
									'order' => 9,
									);

							
								array_push($data,$data2);				

								
								
							}
						//**********************************กรรมการคนที่ 10********************************** */  
							if ($this->request->data['SpecialAndCooperative']['advisor_id10'] == null || $this->request->data['SpecialAndCooperative']['advisor_id10'] == '00' ) {
										
							}else {							
								$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id10']);
								// debug($advisors);
								if ($advisors['Advisor']['mis_employee_id'] == null ) {
									
								}else {
									$check_employees = $this->MisEmployee->find('first' ,array(
										'conditions' => array(
											'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
											),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
									));	
								}
								
								$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
									'conditions' => array(
										// 'SpecialAndCooperative.student_id' => $Students['Student']['id'],
									),					
									'order' => array('SpecialAndCooperative.id' => 'DESC')
								));	
							
								
								$data2 = array(
									
									'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
									'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id10'],
									'order' => 10,
									);

							
								array_push($data,$data2);				

								
								
							}
						//**********************************กรรมการคนที่ 11********************************** */  
							if ($this->request->data['SpecialAndCooperative']['advisor_id11'] == null || $this->request->data['SpecialAndCooperative']['advisor_id11'] == '00' ) {
										
							}else {							
								$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id11']);
								// debug($advisors);
								if ($advisors['Advisor']['mis_employee_id'] == null ) {
									
								}else {
									$check_employees = $this->MisEmployee->find('first' ,array(
										'conditions' => array(
											'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
											),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
									));	
								}
								
								$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
									'conditions' => array(
										// 'SpecialAndCooperative.student_id' => $Students['Student']['id'],
									),					
									'order' => array('SpecialAndCooperative.id' => 'DESC')
								));	
							
								
								$data2 = array(
									
									'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
									'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id11'],
									'order' => 11,
									);

							
								array_push($data,$data2);				

								
								
							}
						//**********************************กรรมการคนที่ 12********************************** */  
							if ($this->request->data['SpecialAndCooperative']['advisor_id12'] == null || $this->request->data['SpecialAndCooperative']['advisor_id12'] == '00' ) {
										
							}else {							
								$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id12']);
								// debug($advisors);
								if ($advisors['Advisor']['mis_employee_id'] == null ) {
									
								}else {
									$check_employees = $this->MisEmployee->find('first' ,array(
										'conditions' => array(
											'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
											),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
									));	
								}
								
								$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
									'conditions' => array(
										// 'SpecialAndCooperative.student_id' => $Students['Student']['id'],
									),					
									'order' => array('SpecialAndCooperative.id' => 'DESC')
								));	
							
								
								$data2 = array(
									
									'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
									'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id12'],
									'order' => 12,
									);

							
								array_push($data,$data2);				

								
								
							}
						//**********************************กรรมการคนที่ 13********************************** */  
							if ($this->request->data['SpecialAndCooperative']['advisor_id13'] == null || $this->request->data['SpecialAndCooperative']['advisor_id13'] == '00' ) {
										
							}else {							
								$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id13']);
								// debug($advisors);
								if ($advisors['Advisor']['mis_employee_id'] == null ) {
									
								}else {
									$check_employees = $this->MisEmployee->find('first' ,array(
										'conditions' => array(
											'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
											),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
									));	
								}
								
								$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
									'conditions' => array(
										// 'SpecialAndCooperative.student_id' => $Students['Student']['id'],
									),					
									'order' => array('SpecialAndCooperative.id' => 'DESC')
								));	
							
								
								$data2 = array(
									
									'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
									'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id13'],
									'order' => 13,
									);

							
								array_push($data,$data2);				

								
								
							}
						//**********************************กรรมการคนที่ 14********************************** */  
							if ($this->request->data['SpecialAndCooperative']['advisor_id14'] == null || $this->request->data['SpecialAndCooperative']['advisor_id14'] == '00' ) {
										
							}else {							
								$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id14']);
								// debug($advisors);
								if ($advisors['Advisor']['mis_employee_id'] == null ) {
									
								}else {
									$check_employees = $this->MisEmployee->find('first' ,array(
										'conditions' => array(
											'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
											),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
									));	
								}
								
								$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
									'conditions' => array(
										// 'SpecialAndCooperative.student_id' => $Students['Student']['id'],
									),					
									'order' => array('SpecialAndCooperative.id' => 'DESC')
								));	
							
								
								$data2 = array(
									
									'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
									'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id14'],
									'order' => 14,
									);

							
								array_push($data,$data2);				

								
								
							}
						//**********************************กรรมการคนที่ 15********************************** */  
							if ($this->request->data['SpecialAndCooperative']['advisor_id15'] == null || $this->request->data['SpecialAndCooperative']['advisor_id15'] == '00' ) {
										
							}else {							
								$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id15']);
								// debug($advisors);
								if ($advisors['Advisor']['mis_employee_id'] == null ) {
									
								}else {
									$check_employees = $this->MisEmployee->find('first' ,array(
										'conditions' => array(
											'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
											),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
									));	
								}
								
								$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
									'conditions' => array(
										// 'SpecialAndCooperative.student_id' => $Students['Student']['id'],
									),					
									'order' => array('SpecialAndCooperative.id' => 'DESC')
								));	
							
								
								$data2 = array(
									
									'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
									'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id15'],
									'order' => 15,
									);

							
								array_push($data,$data2);				

								
								
							}
						//**********************************กรรมการคนที่ 16********************************** */  
							if ($this->request->data['SpecialAndCooperative']['advisor_id16'] == null || $this->request->data['SpecialAndCooperative']['advisor_id16'] == '00' ) {
										
							}else {							
								$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id16']);
								// debug($advisors);
								if ($advisors['Advisor']['mis_employee_id'] == null ) {
									
								}else {
									$check_employees = $this->MisEmployee->find('first' ,array(
										'conditions' => array(
											'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
											),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
									));	
								}
								
								$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
									'conditions' => array(
										// 'SpecialAndCooperative.student_id' => $Students['Student']['id'],
									),					
									'order' => array('SpecialAndCooperative.id' => 'DESC')
								));	
							
								
								$data2 = array(
									
									'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
									'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id16'],
									'order' => 16,
									);

							
								array_push($data,$data2);				

								
								
							}
						//**********************************กรรมการคนที่ 17********************************** */  
							if ($this->request->data['SpecialAndCooperative']['advisor_id17'] == null || $this->request->data['SpecialAndCooperative']['advisor_id17'] == '00' ) {
										
							}else {							
								$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id17']);
								// debug($advisors);
								if ($advisors['Advisor']['mis_employee_id'] == null ) {
									
								}else {
									$check_employees = $this->MisEmployee->find('first' ,array(
										'conditions' => array(
											'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
											),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
									));	
								}
								
								$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
									'conditions' => array(
										// 'SpecialAndCooperative.student_id' => $Students['Student']['id'],
									),					
									'order' => array('SpecialAndCooperative.id' => 'DESC')
								));	
							
								
								$data2 = array(
									
									'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
									'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id17'],
									'order' => 17,
									);

							
								array_push($data,$data2);				

								
								
							}
						//**********************************กรรมการคนที่ 18********************************** */  
							if ($this->request->data['SpecialAndCooperative']['advisor_id18'] == null || $this->request->data['SpecialAndCooperative']['advisor_id18'] == '00' ) {
										
							}else {							
								$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id18']);
								// debug($advisors);
								if ($advisors['Advisor']['mis_employee_id'] == null ) {
									
								}else {
									$check_employees = $this->MisEmployee->find('first' ,array(
										'conditions' => array(
											'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
											),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
									));	
								}
								
								$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
									'conditions' => array(
										// 'SpecialAndCooperative.student_id' => $Students['Student']['id'],
									),					
									'order' => array('SpecialAndCooperative.id' => 'DESC')
								));	
							
								
								$data2 = array(
									
									'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
									'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id18'],
									'order' => 18,
									);

							
								array_push($data,$data2);				

								
								
							}
						//**********************************กรรมการคนที่ 19********************************** */  
							if ($this->request->data['SpecialAndCooperative']['advisor_id19'] == null || $this->request->data['SpecialAndCooperative']['advisor_id19'] == '00' ) {
										
							}else {							
								$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id19']);
								// debug($advisors);
								if ($advisors['Advisor']['mis_employee_id'] == null ) {
									
								}else {
									$check_employees = $this->MisEmployee->find('first' ,array(
										'conditions' => array(
											'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
											),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
									));	
								}
								
								$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
									'conditions' => array(
										// 'SpecialAndCooperative.student_id' => $Students['Student']['id'],
									),					
									'order' => array('SpecialAndCooperative.id' => 'DESC')
								));	
							
								
								$data2 = array(
									
									'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
									'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id19'],
									'order' => 19,
									);

							
								array_push($data,$data2);				

								
								
							}
						//**********************************กรรมการคนที่ 20********************************** */  
							if ($this->request->data['SpecialAndCooperative']['advisor_id20'] == null || $this->request->data['SpecialAndCooperative']['advisor_id20'] == '00' ) {
										
							}else {							
								$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id20']);
								// debug($advisors);
								if ($advisors['Advisor']['mis_employee_id'] == null ) {
									
								}else {
									$check_employees = $this->MisEmployee->find('first' ,array(
										'conditions' => array(
											'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
											),						
										'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
									));	
								}
								
								$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
									'conditions' => array(
										// 'SpecialAndCooperative.student_id' => $Students['Student']['id'],
									),					
									'order' => array('SpecialAndCooperative.id' => 'DESC')
								));	
							
								
								$data2 = array(
									
									'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
									'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id20'],
									'order' => 20,
									);

							
								array_push($data,$data2);				

								
								
							}

							
						$this->SpecialAndCooperativeEmployee->saveMany($data);		 
 
						 
					
				//***********************Save ProposalDocument*********************** */
						$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first',array(
							'conditions' => array(
								// 'SpecialAndCooperative.mis_employee_id' => $UserName['MisEmployee']['id'] ,							
								),
								'order' => array('SpecialAndCooperative.id' => 'DESC')
							));	
							
					$files = $this->request->data['SpecialAndCooperativePassDocument']['files'];	
					if ($this->request->data['SpecialAndCooperativePassDocument']['files'][0]['type'] != null) {
						foreach ($this->request->data['SpecialAndCooperativePassDocument']['files'] as $check) {
							if($check['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif" or "application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document" or "application/x-rar-compressed" or "application/octet-stream" or "application/zip" or "application/octet-stream" or "application/vnd.ms-powerpoint" or "application/vnd.openxmlformats-officedocument.presentationml.presentation" or "application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")){


								foreach ($files as $file) {

									$fileType = pathinfo($file['name'],PATHINFO_EXTENSION);
									if($fileType != "doc" or "docx" or "jpg" or "jpeg" or "png" or "gif" or "zip" or "rar" or "xls" or "xlsx" or "ppt" or "pptx" or "pdf"){

										$this->Session->setFlash('กรุณาแก้ไขชื่อไฟล์ก่อนอัปโหลด โดยไม่ให้มีอักขระ จุด .');
										$this->Session->write('alertType','danger');
									
										$this->redirect(array('controller' => 'specialandCooperatives','action' => 'add_proposals'));
									}
								}


								$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
								$this->Session->write('alertType','danger');
								
							}
						}
					}
						$numcount = $this->SpecialAndCooperativePassDocument->find('count',array(
									'conditions' => array(
										'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id']
									),
								));
					if($files[0]['name'] != null){
						$checkLastLogSpecialAndCooperative = $this->LogSpecialAndCooperative->find('first',array(
							'conditions' => array(				
								// 'LogSpecialAndCooperative.special_and_cooperative_id' => $proposal_id,
								'LogSpecialAndCooperative.mis_employee_id' => $UserName['MisEmployee']['id'] ,
							),
							'order' => array('LogSpecialAndCooperative.id' => 'DESC')
						)); 
						$i = 0;
						foreach ($files as $file) {
							if ($numcount == null) {
								$numcount = 1;
							}else {							
								$numcount = $numcount++;
							}
							$fileType = pathinfo($file['name'],PATHINFO_EXTENSION);
								
							$this->request->data['SpecialAndCooperativePassDocument']['name'] = $numcount."_".$file['name'];
							$this->request->data['SpecialAndCooperativePassDocument']['name_old'] = $file['name'];	
							$this->request->data['SpecialAndCooperativePassDocument']['postdate'] = date('Y-m-d H:i:00');		
							$this->request->data['SpecialAndCooperativePassDocument']['special_and_cooperative_id'] = $SpecialAndCooperatives['SpecialAndCooperative']['id'];
							$this->request->data['SpecialAndCooperativePassDocument']['log_special_and_cooperative_id'] = $checkLastLogSpecialAndCooperative['LogSpecialAndCooperative']['id'];
							$this->request->data['SpecialAndCooperativePassDocument']['mis_employee_id'] = $UserName['MisEmployee']['id'];
	
								$filethai = iconv("UTF-8", "TIS-620",  $numcount."_".$file['name']);
								
							
								if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
									$this->request->data['SpecialAndCooperativePassDocument']['class'] = "file";
								}else{
									$this->request->data['SpecialAndCooperativePassDocument']['class'] = "img";
								}
								
								$folder = WWW_ROOT.'files'.DS.'students'.DS.'specialandcooperatives'.DS.$SpecialAndCooperatives['SpecialAndCooperative']['id'].DS;
			
								if(!file_exists($folder))
									mkdir($folder);
									
								move_uploaded_file($file['tmp_name'],$folder.$filethai);
								// $files[0]['tmp_name']
								
							
							$this->SpecialAndCooperativePassDocument->create();					
							$this->SpecialAndCooperativePassDocument->save($this->request->data);	
							
						}

					} 
						// $fileDOC = $this->request->data['SpecialAndCooperativePassDocument']['fileDOC'];
							// $filePDF = $this->request->data['SpecialAndCooperativePassDocument']['filePDF'];	
							 			
							
								 
							// 	$pdf = null;
							// 	$doc = null;
			
							// 	if ($fileDOC[0]['name'] != null) {
							// 		$fileType = pathinfo($fileDOC[0]['name'],PATHINFO_EXTENSION);
							// 		if($fileDOC[0]['type'] != ("application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document"or "application/vnd.openxmlformats-officedocument.wordprocessingml.document")){
			
							// 			$this->Session->setFlash('ไฟล์ DOC อัฟโหลดไม่ถูกต้อง');
							// 			$this->Session->write('alertType','danger');							
							// 			$this->redirect(array('controller' => 'proposals','action' => 'add_proposals'));
										
							// 		}else{
							// 			$doc = true;
							// 		}
							// 	}
			
							// 	if ($filePDF[0]['name'] != null) {
							// 		$fileType = pathinfo($filePDF[0]['name'],PATHINFO_EXTENSION);
							// 		if($filePDF[0]['type'] != ('application/pdf')){
			
							// 			$this->Session->setFlash('ไฟล์ PDF อัฟโหลดไม่ถูกต้อง');
							// 			$this->Session->write('alertType','danger');
							// 			$this->redirect(array('controller' => 'proposals','action' => 'add_proposals'));						
									
										
							// 		}else{
							// 			$pdf = true;
							// 		}
							// 	}
								
								 
							// 	$numcount = $this->SpecialAndCooperativePassDocument->find('count',array(
							// 		'conditions' => array(
							// 			'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id']
							// 		),
							// 	));
							// 	if($fileDOC[0]['name'] != null){
							// 		if ($numcount == null) {
							// 			$numcount = 1;
							// 		}else {							
							// 			$numcount = $numcount++;
							// 		}
						
							// 		$i = date("m");
							// 		foreach ($fileDOC as $fileDOC) {
							// 		$fileType = pathinfo($fileDOC['name'],PATHINFO_EXTENSION);
								
							// 		$this->request->data['SpecialAndCooperativePassDocument']['name'] = $numcount."_".$fileDOC['name'];
							// 		$this->request->data['SpecialAndCooperativePassDocument']['name_old'] = $fileDOC['name'];	
							// 		$this->request->data['SpecialAndCooperativePassDocument']['postdate'] = date('Y-m-d H:i:00');		
							// 		$this->request->data['SpecialAndCooperativePassDocument']['special_and_cooperative_id'] = $SpecialAndCooperatives['SpecialAndCooperative']['id'];
							// 		$this->request->data['SpecialAndCooperativePassDocument']['mis_employee_id'] = $UserName['MisEmployee']['id'];
			
									
			
							// 			$filethai = iconv("UTF-8", "TIS-620",  $numcount."_".$fileDOC['name']);
										
									
							// 			if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
							// 				$this->request->data['SpecialAndCooperativePassDocument']['class'] = "file";
							// 			}else{
							// 				$this->request->data['SpecialAndCooperativePassDocument']['class'] = "img";
							// 			}
										
							// 			$folder = WWW_ROOT.'files'.DS.'students'.DS.'specialandcooperatives'.DS.$SpecialAndCooperatives['SpecialAndCooperative']['id'].DS;
					
							// 			if(!file_exists($folder))
							// 				mkdir($folder);
											
							// 			move_uploaded_file($fileDOC['tmp_name'],$folder.$filethai);
									
									
							// 		$this->SpecialAndCooperativePassDocument->create();					
							// 		$this->SpecialAndCooperativePassDocument->save($this->request->data);						
							// 		}
							// 	}
			
							// 	if($filePDF[0]['name'] != null){
							// 		if ($numcount == null) {
							// 			$numcount = 1;
							// 		}else {							
							// 			$numcount = $numcount++;
							// 		}
							// 		$i = date("m");
							// 		foreach ($filePDF as $filePDF) {
							// 		$fileType = pathinfo($filePDF['name'],PATHINFO_EXTENSION);
								
							// 		$this->request->data['SpecialAndCooperativePassDocument']['name'] = $numcount."_".$filePDF['name'];
							// 		$this->request->data['SpecialAndCooperativePassDocument']['name_old'] = $filePDF['name'];	
							// 		$this->request->data['SpecialAndCooperativePassDocument']['postdate'] = date('Y-m-d H:i:00');		
							// 		$this->request->data['SpecialAndCooperativePassDocument']['special_and_cooperative_id'] = $SpecialAndCooperatives['SpecialAndCooperative']['id'];
							// 		$this->request->data['SpecialAndCooperativePassDocument']['mis_employee_id'] = $UserName['MisEmployee']['id'];
			
							// 			$filethai = iconv("UTF-8", "TIS-620",  $numcount."_".$filePDF['name']);
										
									
							// 			if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
							// 				$this->request->data['SpecialAndCooperativePassDocument']['class'] = "file";
							// 			}else{
							// 				$this->request->data['SpecialAndCooperativePassDocument']['class'] = "img";
							// 			}
										
							// 			$folder = WWW_ROOT.'files'.DS.'students'.DS.'specialandcooperatives'.DS.$SpecialAndCooperatives['SpecialAndCooperative']['id'].DS;
					
							// 			if(!file_exists($folder))
							// 				mkdir($folder);
											
							// 			move_uploaded_file($filePDF['tmp_name'],$folder.$filethai);
							// 			// $files[0]['tmp_name']
										
									
							// 		$this->SpecialAndCooperativePassDocument->create();					
							// 		$this->SpecialAndCooperativePassDocument->save($this->request->data);						
							// 		}
							// 	}		 
						 
			
										
						
				
			    //*************save************* */		
					 		
			 
			
				$this->Session->write('alertType','success');
				$this->Session->setFlash('เพิ่มข้อมูล เรียบร้อยแล้ว');
				 $this->redirect(array('action' => 'list_result_pass_proposal'));	

				 	
			 
		}
		 
		$SpecialAndCooperatives = $this->SpecialAndCooperative->find('all',array(
			'conditions' => array(
				'SpecialAndCooperative.mis_employee_id' => $UserName['MisEmployee']['id'] ,							
				),
				'order' => array('SpecialAndCooperative.postdate' => 'DESC')
			));	
	 
		 
		$yearterms = $this->Yearterm->find('list',array(
			'conditions' => array(
				'and' => array(
					array('Yearterm.level <= ' => 2),
					array('Yearterm.level >= ' => 1)
					),
			),
			// 'limit' => 11,
			'fields' => array('Yearterm.id','Yearterm.name'),
			'order' => array('Yearterm.id' => 'DESC'),
			
		));						
		$degrees = $this->Degree->find('list',array(
			'conditions' => array(
				 
			), 
			'fields' => array('Degree.id','Degree.degree_name'),
			'order' => array('id' => 'ASC'),
			
		));	
		 
		if($admins['Admincurriculumrequest']['employee_status_id'] == 3 || $admins['Admincurriculumrequest']['employee_status_id'] == 2){
			$majors = $this->Majordegree->find('list',array(	
				'conditions' => array(
					// 'or' => array(
					// 	array('Majordegree.degree_id <= ' => 5),
					// 	array('Majordegree.degree_id >= ' => 3)
					// 	),
					'and' => array(
					array('Majordegree.level <= ' => 2),
					array('Majordegree.level >= ' => 1)
					),
						'Majordegree.organize_id ' => $admins['Admincurriculumrequest']['organize_id'] ,
						'Majordegree.degree_id ' => $id,
						// 'Majordegree.level' => 1
				),
				'fields' => array('Majordegree.major_id','Majordegree.major_name'),
				'order' => array('Majordegree.major_id' => 'ASC'),
				'group' => array('Majordegree.major_id')
				
			));	

		}else {
			$majors = $this->Majordegree->find('list',array(	
				'conditions' => array(
					// 'or' => array(
					// 	array('Majordegree.degree_id <= ' => 5),
					// 	array('Majordegree.degree_id >= ' => 3)
					// 	),
					'and' => array(
					array('Majordegree.level <= ' => 2),
					array('Majordegree.level >= ' => 1)
					),
						// 'Majordegree.organize_id ' => $admins['Admincurriculumrequest']['organize_id'] ,
						// 'Majordegree.degree_id ' => $id,
						// 'Majordegree.level' => 1
				),
				'fields' => array('Majordegree.major_id','Majordegree.major_name'),
				'order' => array('Majordegree.major_id' => 'ASC'),
				'group' => array('Majordegree.major_id')
				
			));	
			 
		}
		
		 //debug($majors);
		$this->set(array( 
			'majors' => $majors,
			'degrees' => $degrees
		));	
		$advisors = $this->Advisor->find('all', array(
			'conditions' => array(
				// 'Advisor.level' => 1,							
				),
			'fields' => array('id','advisor_code','advisor_title','advisor_name','advisor_sname','mis_employee_id'),
			'order' => array('id' => 'ASC')
		));

		$outputadvisor = '';		
		foreach ($advisors as $advisor) {
			$outputadvisor .= '
				<option value="'.$advisor['Advisor']['mis_employee_id'].'">'.$advisor['Advisor']['advisor_code'].' '.$advisor['Advisor']['advisor_title'].$advisor['Advisor']['advisor_name'].' '.$advisor['Advisor']['advisor_sname'].'</option>
			';
		}
		$specialandCooperativeTypes = $this->SpecialAndCooperativeType->find('list',array(
			'conditions' => array(			
				 
				),
				
			)); 
		$organizes = $this->Organize->find('list',array(	
			'conditions' => array(
				'Organize.group ' => 2,
						
			), 
			'fields' => array('id','name'),
		));	
		//debug($organizes);

		$this->set(array('outputadvisor' => $outputadvisor));
		$this->set(array( 
			'yearterms' => $yearterms, 
			'SpecialAndCooperatives' => $SpecialAndCooperatives, 
			'id' => $id,
			'specialandCooperativeTypes' => $specialandCooperativeTypes,
			'organizes' => $organizes	
		));

	} 
	public function edit_proposal($id=null){
	
		$UserName = $this->Session->read('MisEmployee');		
		$admins = $this->Session->read('adminCurriculumRequests');
		if ($admins != null) {
			 
		
			$this->set(array(		
				'admins' => $admins,
				'UserName' => $UserName,
			 
				));
		} 
		$SpecialAndCooperativeEmployees = $this->SpecialAndCooperativeEmployee->find('first' ,array(
			'conditions' => array(
				'SpecialAndCooperativeEmployee.special_and_cooperative_id' => $id,
			),					
			'order' => array('SpecialAndCooperativeEmployee.id' => 'DESC')
		));			
		// debug($SpecialAndCooperativeEmployees);
		if($this->request->data){
			 
			$this->SpecialAndCooperative->save($this->request->data);

			//***********************Save SpecialAndCooperativeEmployee*********************** */			
				$data = array();
						
				//**********************************กรรมการคนที่ 1 ประธาน********************************** */  
					// debug($this->request->data['SpecialAndCooperative']['advisor_id1']);
					if ($this->request->data['SpecialAndCooperative']['advisor_id1'] == null || $this->request->data['SpecialAndCooperative']['advisor_id1'] == '00' ) {
							
					}else {							
						$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id1']);
						// debug($advisors);
						if ($advisors['Advisor']['mis_employee_id'] == null ) {
							
						}else {
							$check_employees = $this->MisEmployee->find('first' ,array(
								'conditions' => array(
									'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
									),						
								'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
							));	
						}
						
						$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
							'conditions' => array(
								'SpecialAndCooperative.id' => $id,
							),					
							'order' => array('SpecialAndCooperative.id' => 'DESC')
						));	
					
						
						$data2 = array(
							
							'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
							'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id1'],
							'order' => $SpecialAndCooperativeEmployees['SpecialAndCooperativeEmployee']['order'] + 1,
							);

					
						array_push($data,$data2);				

						
						
					}

				//**********************************กรรมการคนที่ 2********************************** */  
					if ($this->request->data['SpecialAndCooperative']['advisor_id2'] == null || $this->request->data['SpecialAndCooperative']['advisor_id2'] == '00' ) {
							
					}else {							
						$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id2']);
						// debug($advisors);
						if ($advisors['Advisor']['mis_employee_id'] == null ) {
							
						}else {
							$check_employees = $this->MisEmployee->find('first' ,array(
								'conditions' => array(
									'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
									),						
								'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
							));	
						}
						
						$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
							'conditions' => array(
								'SpecialAndCooperative.id' => $id,
							),					
							'order' => array('SpecialAndCooperative.id' => 'DESC')
						));	
					
						
						$data2 = array(
							
							'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
							'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id2'],
							'order' => $SpecialAndCooperativeEmployees['SpecialAndCooperativeEmployee']['order'] + 2,
							);

					
						array_push($data,$data2);				

						
						
					}

				//**********************************กรรมการคนที่ 3********************************** */  
					if ($this->request->data['SpecialAndCooperative']['advisor_id3'] == null || $this->request->data['SpecialAndCooperative']['advisor_id3'] == '00' ) {
							
					}else {							
						$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id3']);
						// debug($advisors);
						if ($advisors['Advisor']['mis_employee_id'] == null ) {
							
						}else {
							$check_employees = $this->MisEmployee->find('first' ,array(
								'conditions' => array(
									'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
									),						
								'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
							));	
						}
						
						$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
							'conditions' => array(
								'SpecialAndCooperative.id' => $id,
							),					
							'order' => array('SpecialAndCooperative.id' => 'DESC')
						));	
					
						
						$data2 = array(
							
							'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
							'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id3'],
							'order' => $SpecialAndCooperativeEmployees['SpecialAndCooperativeEmployee']['order'] + 3,
							);

					
						array_push($data,$data2);				

						
						
					}

				//**********************************กรรมการคนที่ 4********************************** */  
					if ($this->request->data['SpecialAndCooperative']['advisor_id4'] == null || $this->request->data['SpecialAndCooperative']['advisor_id4'] == '00' ) {
							
					}else {							
						$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id4']);
						// debug($advisors);
						if ($advisors['Advisor']['mis_employee_id'] == null ) {
							
						}else {
							$check_employees = $this->MisEmployee->find('first' ,array(
								'conditions' => array(
									'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
									),						
								'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
							));	
						}
						
						$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
							'conditions' => array(
								'SpecialAndCooperative.id' => $id,
							),					
							'order' => array('SpecialAndCooperative.id' => 'DESC')
						));	
					
						
						$data2 = array(
							
							'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
							'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id4'],
							'order' => $SpecialAndCooperativeEmployees['SpecialAndCooperativeEmployee']['order'] + 4,
							);

					
						array_push($data,$data2);				

						
						
					}

				//**********************************กรรมการคนที่ 5********************************** */  
					if ($this->request->data['SpecialAndCooperative']['advisor_id5'] == null || $this->request->data['SpecialAndCooperative']['advisor_id5'] == '00' ) {
							
					}else {							
						$advisors = $this->Advisor->findByMisEmployeeId($this->request->data['SpecialAndCooperative']['advisor_id5']);
						// debug($advisors);
						if ($advisors['Advisor']['mis_employee_id'] == null ) {
							
						}else {
							$check_employees = $this->MisEmployee->find('first' ,array(
								'conditions' => array(
									'MisEmployee.id' => $advisors['Advisor']['mis_employee_id'],			
									),						
								'fields' => array('MisEmployee.id','MisEmployee.mis_prename_id','MisEmployee.mis_sub_prename_id'),
							));	
						}
						
						$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first' ,array(
							'conditions' => array(
								'SpecialAndCooperative.id' => $id,
							),					
							'order' => array('SpecialAndCooperative.id' => 'DESC')
						));	
					
						
						$data2 = array(
							
							'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id'],
							'mis_employee_id' => $this->request->data['SpecialAndCooperative']['advisor_id5'],
							'order' => $SpecialAndCooperativeEmployees['SpecialAndCooperativeEmployee']['order'] + 5,
							);

					
						array_push($data,$data2);				

						
						
					}
					
				$this->SpecialAndCooperativeEmployee->saveMany($data);	
			//**********************************Save SpecialAndCooperativeDocument********************************** */   
				date_default_timezone_set('Asia/Bangkok');
			
				////////////////// before upload ///////////////////
				$fileDOC = $this->request->data['SpecialAndCooperativeDocument']['fileDOC'];
				$filePDF = $this->request->data['SpecialAndCooperativeDocument']['filePDF'];					
				
					////////////////// cheack img upload  ///////////////////
					$pdf = null;
					$doc = null;

					if ($fileDOC[0]['name'] != null) {
						$fileType = pathinfo($fileDOC[0]['name'],PATHINFO_EXTENSION);
						if($fileDOC[0]['type'] != ("application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document"or "application/vnd.openxmlformats-officedocument.wordprocessingml.document")){

							$this->Session->setFlash('ไฟล์ DOC อัฟโหลดไม่ถูกต้อง');
							$this->Session->write('alertType','danger');							
							$this->redirect(array('controller' => 'SpecialAndCooperatives','action' => 'edit_proposal',$id ));
							
						}else{
							$doc = true;
						}
					}

					if ($filePDF[0]['name'] != null) {
						$fileType = pathinfo($filePDF[0]['name'],PATHINFO_EXTENSION);
						if($filePDF[0]['type'] != ('application/pdf')){

							$this->Session->setFlash('ไฟล์ PDF อัฟโหลดไม่ถูกต้อง');
							$this->Session->write('alertType','danger');
							$this->redirect(array('controller' => 'SpecialAndCooperatives','action' => 'edit_proposal',$id ));
							
						}else{
							$pdf = true;
						}
					}
					
 
					/////////////// files upload Document /////////////////
					$numcount = $this->SpecialAndCooperativeDocument->find('count',array(
							'conditions' => array(
								'special_and_cooperative_id' => $id
							),
						));
					if($fileDOC[0]['name'] != null){
						if ($numcount == null) {
							$numcount = 1;
						}else {							
							$numcount = $numcount++;
						}
			
						$i = date("m");
						foreach ($fileDOC as $fileDOC) {
						$fileType = pathinfo($fileDOC['name'],PATHINFO_EXTENSION);
					
						$this->request->data['SpecialAndCooperativeDocument']['name'] = $numcount."_".$fileDOC['name'];
						$this->request->data['SpecialAndCooperativeDocument']['postdate'] = date('Y-m-d H:i:00');
						$this->request->data['SpecialAndCooperativeDocument']['special_and_cooperative_id'] = $id;
					 
							$filethai = iconv("UTF-8", "TIS-620",  $numcount."_".$fileDOC['name']);
							
						
							if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
								$this->request->data['SpecialAndCooperativeDocument']['class'] = "file";
							}else{
								$this->request->data['SpecialAndCooperativeDocument']['class'] = "img";
							}
							$folder = WWW_ROOT.'files'.DS.'students'.DS.'SpecialAndCooperatives'.DS.$id.DS;
							 
							if(!file_exists($folder))
								mkdir($folder);
								
							move_uploaded_file($fileDOC['tmp_name'],$folder.$filethai);
							// $files[0]['tmp_name']
							//debug($this->request->data['ProposalDocument']['name']);
						
						$this->SpecialAndCooperativeDocument->create();					
						$this->SpecialAndCooperativeDocument->save($this->request->data);						
						}
					}
					if($filePDF[0]['name'] != null){
						if ($numcount == null) {
							$numcount = 1;
						}else {							
							$numcount = $numcount++;
						}
						$i = date("m");
						foreach ($filePDF as $filePDF) {
						$fileType = pathinfo($filePDF['name'],PATHINFO_EXTENSION);
					
						$this->request->data['SpecialAndCooperativeDocument']['name'] = $numcount."_".$filePDF['name'];
						$this->request->data['SpecialAndCooperativeDocument']['postdate'] = date('Y-m-d H:i:00');
						$this->request->data['SpecialAndCooperativeDocument']['special_and_cooperative_id'] = $id;
						 
							$filethai = iconv("UTF-8", "TIS-620",  $numcount."_".$filePDF['name']);
							
						
							if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
								$this->request->data['SpecialAndCooperativeDocument']['class'] = "file";
							}else{
								$this->request->data['SpecialAndCooperativeDocument']['class'] = "img";
							}
							
							$folder = WWW_ROOT.'files'.DS.'students'.DS.'SpecialAndCooperatives'.DS.$id.DS;
		
							if(!file_exists($folder))
								mkdir($folder);
								
							move_uploaded_file($filePDF['tmp_name'],$folder.$filethai);
							// $files[0]['tmp_name']
							//debug($this->request->data['ProposalDocument']['name']);
						
						$this->SpecialAndCooperativeDocument->create();					
						$this->SpecialAndCooperativeDocument->save($this->request->data);						
						}
					}

						
					 
				$this->Session->write('alertType','success');
				$this->Session->setFlash('แก้ไขข้อมูลเรียบร้อยแล้ว');
				$this->redirect(array('action' => 'edit_proposal',$id));
			 

		}
		$SpecialAndCooperatives = $this->SpecialAndCooperative->findById($id);
		$employees = $this->MisEmployee->find('first',array(
			'conditions' => array(
				'MisEmployee.id' => $SpecialAndCooperatives['SpecialAndCooperative']['mis_employee_id']
			),
		
		));
		$SpecialAndCooperativeEmployees = $this->SpecialAndCooperativeEmployee->find('all',array(
			'conditions' => array(				
				'SpecialAndCooperativeEmployee.special_and_cooperative_id' => $id,
			 
			),
			'order' => array('SpecialAndCooperativeEmployee.order' => 'ASC')
		));
		$outputSpecialAndCooperativeEmployee = '';
		$j= 0;
		foreach ($SpecialAndCooperativeEmployees as $SpecialAndCooperativeEmployee) {
			$j++;
			$employeeSpecialAndCooperativeEmployees = $this->MisEmployee->find('all',array(
				'conditions' => array(
					'MisEmployee.id' => $SpecialAndCooperativeEmployee['SpecialAndCooperativeEmployee']['mis_employee_id'],
				),
			
			));
			foreach ($employeeSpecialAndCooperativeEmployees as $employee) {
				$sSubPre = '';
				if ($employee['MisSubPrename']['id'] != null) {
					$sSubPre = $sSubPre . $employee['MisSubPrename']['name_short_th'] . ' ';
				}
				if ($sSubPre == '') {
					$sSubPre = $sSubPre . $employee['MisPrename']['name_full_th'] . ' ';
				}
				$outputSpecialAndCooperativeEmployee .= '<tr>
														<td>'.$j.'.</td>
														<td>'.$sSubPre . ' ' .$employee['MisEmployee']['fname_th'].' '.$employee['MisEmployee']['lname_th'].' 
														
														</td>
														
													</tr>';
			}
			# code...
		}	 
		$this->request->data = $SpecialAndCooperatives;
		$output ='';
		if($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 10){
			$proposaldocumentdates = $this->SpecialAndCooperativePassDocument->find('all',array(
				'conditions' => array(
					'SpecialAndCooperativePassDocument.special_and_cooperative_id' => $id,			
				 
					),
					// 'limit' => 1,
					'group' => array('SpecialAndCooperativePassDocument.postdate'),
					'order' => array('SpecialAndCooperativePassDocument.postdate' => 'ASC')
				));
			
			foreach ($proposaldocumentdates as $proposaldocumentdate) {
				$num=0;
				$proposaldocuments = $this->SpecialAndCooperativePassDocument->find('all',array(
					'conditions' => array(				
						'SpecialAndCooperativePassDocument.special_and_cooperative_id' => $id,		
						'SpecialAndCooperativePassDocument.postdate' => $proposaldocumentdate['SpecialAndCooperativePassDocument']['postdate'] ,
						),
						'order' => array('SpecialAndCooperativePassDocument.postdate' => 'ASC')
					));
					if($num == 0){								
						$output .=
						'<tr>
							<td colspan="2"> ผลบังคับใช้ :
								'. $SpecialAndCooperatives['Yearterm']['name'] .'
							</td>
							
						</tr>';
						
						$num++;
					}else {
						$output .= 
						'';
					}
					$i=0;
					foreach ($proposaldocuments as $proposaldocument) {
						$i++;
						$output .=
						'<tr>
							<td data-title="ลำดับ" align="center"> ลำดับที่ '.$i.'</td></td>
							<td>
								<a href="'.$this->urls.'/files/students/specialandcooperative/'.$id.DS.$proposaldocument['SpecialAndCooperativePassDocument']['name'].'" class="btn btn-primary" target="_blank">
								'.$proposaldocument['SpecialAndCooperativePassDocument']['name'].'<span class="glyphicon glyphicon-cloud-download"></span></a>
							</td>
					  </tr>';
					}
					
			}
		}else {
			# code...
			$SpecialAndCooperativedocumentdates = $this->SpecialAndCooperativeDocument->find('all',array(
				'conditions' => array(				
					'SpecialAndCooperativeDocument.special_and_cooperative_id' => $id,
					),
					'group' => array('SpecialAndCooperativeDocument.postdate'),
					'order' => array('SpecialAndCooperativeDocument.postdate' => 'ASC')
				));
				foreach ($SpecialAndCooperativedocumentdates as $SpecialAndCooperativedocumentdate) {
					$num=0;
					$SpecialAndCooperativedocuments = $this->SpecialAndCooperativeDocument->find('all',array(
						'conditions' => array(				
							'SpecialAndCooperativeDocument.special_and_cooperative_id' => $id,
							'SpecialAndCooperativeDocument.postdate' => $SpecialAndCooperativedocumentdate['SpecialAndCooperativeDocument']['postdate'] ,
							),
							'order' => array('SpecialAndCooperativeDocument.postdate' => 'ASC')
						));
						if($num == 0){								
							$output .=
							'<tr>
								<td colspan="2"> วันที่ส่งเอกสารหัวข้อโครงร่างฯ :
									'. $SpecialAndCooperativedocumentdate['SpecialAndCooperativeDocument']['postdate'] .'
								</td>
								
							</tr>';
							
							$num++;
						}else {
							$output .=
							'';
						}
						$i=0;
						foreach ($SpecialAndCooperativedocuments as $SpecialAndCooperativedocument) {							
							$i++;
							$output .=
							'<tr>
							<td data-title="ลำดับ" align="center"> ไฟล์ที่ '.$i.' </td></td>
							<td>
								<a href="'.$this->urls.'/files/students/specialandcooperatives/'.$id.DS.$SpecialAndCooperativedocument['SpecialAndCooperativeDocument']['name'].'" class="btn btn-primary" target="_blank">
								Download'.$SpecialAndCooperativedocument['SpecialAndCooperativeDocument']['name'].'<span class="glyphicon glyphicon-cloud-download"></span></a>
							</td>
						</tr>';
						}
						
				}
		}

		$this->set(array(
			'SpecialAndCooperatives' => $SpecialAndCooperatives,					 
			'id' => $id,
			'output' => $output,
			'employees' => $employees,
			'outputSpecialAndCooperativeEmployee' => $outputSpecialAndCooperativeEmployee
		));
		 
		$majors = $this->Majordegree->find('list',array(	
			'conditions' => array(
				// 'or' => array(
				// 	array('Majordegree.degree_id <= ' => 5),
				// 	array('Majordegree.degree_id >= ' => 3)
				// 	),
				'and' => array(
				array('Majordegree.level <= ' => 2),
				array('Majordegree.level >= ' => 0)
				),
					// 'Majordegree.organize_id ' => $admins['Admincurriculumrequest']['organize_id'] ,
					'Majordegree.degree_id ' => $id,
					// 'Majordegree.level' => 1
			),
			'fields' => array('Majordegree.major_id','Majordegree.major_name'),
			'order' => array('Majordegree.major_id' => 'ASC'),
			'group' => array('Majordegree.major_id')
			
		));	
		$degrees = $this->Degree->find('list',array(
			'conditions' => array(
				 
			), 
			'fields' => array('Degree.id','Degree.degree_name'),
			'order' => array('id' => 'ASC'),
			
		));	
		$yearterms = $this->Yearterm->find('list',array(
			'conditions' => array(
				'and' => array(
					array('Yearterm.level <= ' => 2),
					array('Yearterm.level >= ' => 1)
					),
			),
			// 'limit' => 11,
			'fields' => array('Yearterm.id','Yearterm.name'),
			'order' => array('Yearterm.id' => 'DESC'),
			
		));	
		$advisors = $this->Advisor->find('all', array(
			'conditions' => array(
				'Advisor.level' => 1,							
				),
			'fields' => array('id','advisor_code','advisor_title','advisor_name','advisor_sname','mis_employee_id'),
			'order' => array('id' => 'ASC')
		));

		$outputadvisor = '';	
		$outputadvisee = '';		
		foreach ($advisors as $advisor) {
			if ($advisor['Advisor']['mis_employee_id'] == $SpecialAndCooperatives['SpecialAndCooperative']['mis_employee_id']) {
				$outputadvisor .= '
					<option value="'.$advisor['Advisor']['mis_employee_id'].'" selected="selected">'.$advisor['Advisor']['advisor_code'].' '.$advisor['Advisor']['advisor_title'].$advisor['Advisor']['advisor_name'].' '.$advisor['Advisor']['advisor_sname'].'</option>
				';
			}else {
				 
				$outputadvisor .= '
					<option value="'.$advisor['Advisor']['mis_employee_id'].'">'.$advisor['Advisor']['advisor_code'].' '.$advisor['Advisor']['advisor_title'].$advisor['Advisor']['advisor_name'].' '.$advisor['Advisor']['advisor_sname'].'</option>
				';
			}
			$outputadvisee .= '
					<option value="'.$advisor['Advisor']['mis_employee_id'].'">'.$advisor['Advisor']['advisor_code'].' '.$advisor['Advisor']['advisor_title'].$advisor['Advisor']['advisor_name'].' '.$advisor['Advisor']['advisor_sname'].'</option>
				';
		}
		


		$this->set(array());
		$this->set(array( 
			'majors' => $majors,
			'degrees' => $degrees,
			'yearterms' => $yearterms,
			'outputadvisor' => $outputadvisor,
			'outputadvisee' => $outputadvisee
			
		));
	}
	
	public function delete_proposal($id =null){
		$this->request->data = $this->SpecialAndCooperative->findById($id);
		if($this->request->data){
			$listSpecialAndCooperativeDocments = $this->SpecialAndCooperativeDocument->find('all',array(
				'conditions' => array(				
					'SpecialAndCooperativeDocument.special_and_cooperative_id' => $id,				
					),
				 
			));
			$listAnswerSpecialAndCooperatives = $this->AnswerSpecialAndCooperative->find('all',array(
				'conditions' => array(				
					'AnswerSpecialAndCooperative.special_and_cooperative_id' => $id,				
					),
				 
			));
			 
				
			if ($this->SpecialAndCooperative->delete($id)) {
				if($listSpecialAndCooperativeDocments != null){
					foreach ($listSpecialAndCooperativeDocments as $key) {
						
						$this->SpecialAndCooperativeDocument->delete($key['SpecialAndCooperativeDocument']['id']);
					}	

				}
				if($listAnswerSpecialAndCooperatives != null){
					foreach ($listAnswerSpecialAndCooperatives as $key) {
						
						$this->AnswerSpecialAndCooperative->delete($key['AnswerSpecialAndCooperative']['id']);
					}

				} 
			} 
			$this->Session->write('alertType','danger');
			$this->Session->setFlash('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>ลบข้อมูล เรียบร้อยแล้ว');								
			$this->redirect(array('action' => 'list_result_manage_proposal'));	
		}
		
	}
	public function proposal_detail($proposal_id=null,$mis_employee_id=null){
		$UserName = $this->Session->read('MisEmployee');		
		$admins = $this->Session->read('adminCurriculumRequests');
		
		if(isset($UserName)){
			$this->set('UserName', $UserName);
			$this->set('admins', $admins);
			

			if($this->action == 'login')
				$this->redirect(array('action' => 'Dashboard'));
		}
		else{
			if($this->action != 'login')
				$this->redirect('https://mis.agri.cmu.ac.th/mis/User/dashboard');
			
		}
		$this->set(array(
			'admins' => $admins,
			'UserName' => $UserName,
			
		));	
	
		//================================================	
		
			try{
				if ($this->request->data){  
					date_default_timezone_set('Asia/Bangkok');
					$studentEmails = $this->SpecialAndCooperative->find('first',array(
						'conditions' => array(
							'SpecialAndCooperative.id' => $proposal_id ,	
							'SpecialAndCooperative.mis_employee_id' => $mis_employee_id ,							
							),
							'order' => array('SpecialAndCooperative.id' => 'DESC')
						));
					if($studentEmails['SpecialAndCooperative']['special_and_cooperative_pass_id'] != 5){

					 
						$outputtext = '<font color="green">&nbsp;&nbsp;แก้ไขเรียบร้อย</font>';	
						 
						//======================= เก็บ Log SpecialAndCooperative	====================================
						$this->request->data['LogSpecialAndCooperative']['status'] = 1;
						$this->request->data['LogSpecialAndCooperative']['special_and_cooperative_id'] = $studentEmails['SpecialAndCooperative']['id'];
						$this->request->data['LogSpecialAndCooperative']['mis_employee_id'] = $studentEmails['SpecialAndCooperative']['mis_employee_id'];
						$this->request->data['LogSpecialAndCooperative']['employee_status_id'] = $admins['Admincurriculumrequest']['employee_status_id'];

						$log_proposals = $this->LogSpecialAndCooperative->find('first',array(
						'conditions' => array(
							'LogSpecialAndCooperative.special_and_cooperative_id' => $studentEmails['SpecialAndCooperative']['id'],
							'LogSpecialAndCooperative.mis_employee_id' => $studentEmails['SpecialAndCooperative']['mis_employee_id'],
							'LogSpecialAndCooperative.employee_status_id' => $admins['Admincurriculumrequest']['employee_status_id'],
							),
							'order' => array('LogSpecialAndCooperative.id' => 'DESC')
						));
						 
					
						 
						$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$outputtext.' จาก<b>ระดับอาจารย์</b>';

						 
						$this->LogSpecialAndCooperative->save($this->request->data['LogSpecialAndCooperative']);	
				 
					
						 
					}
					//***********************Save SpecialAndCooperative******************************* */	
					$this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'] = 3;
					$this->request->data['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] = 2;
					$this->SpecialAndCooperative->save($this->request->data['SpecialAndCooperative']);
	
					
					 
					
					/////////////// Save SpecialAndCooperativeDocument /////////////////
					$files = $this->request->data['SpecialAndCooperativeDocument']['files'];	
					if ($this->request->data['SpecialAndCooperativeDocument']['files'][0]['type'] != null) {
						foreach ($this->request->data['SpecialAndCooperativeDocument']['files'] as $check) {
							if($check['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif" or "application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document" or "application/x-rar-compressed" or "application/octet-stream" or "application/zip" or "application/octet-stream" or "application/vnd.ms-powerpoint" or "application/vnd.openxmlformats-officedocument.presentationml.presentation" or "application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")){


								foreach ($files as $file) {

									$fileType = pathinfo($file['name'],PATHINFO_EXTENSION);
									if($fileType != "doc" or "docx" or "jpg" or "jpeg" or "png" or "gif" or "zip" or "rar" or "xls" or "xlsx" or "ppt" or "pptx" or "pdf"){

										$this->Session->setFlash('กรุณาแก้ไขชื่อไฟล์ก่อนอัปโหลด โดยไม่ให้มีอักขระ จุด .');
										$this->Session->write('alertType','danger');
									
										$this->redirect(array('controller' => 'SpecialAndCooperatives','action' => 'add_proposals'));
									}
								}


								$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
								$this->Session->write('alertType','danger');
								
							}
						}
					}
						$numcount = $this->SpecialAndCooperativeDocument->find('count',array(
									'conditions' => array(
										'special_and_cooperative_id' => $proposal_id,
									),
								));
					if($files[0]['name'] != null){
						$checkLastLogSpecialAndCooperative = $this->LogSpecialAndCooperative->find('first',array(
							'conditions' => array(				
								'LogSpecialAndCooperative.special_and_cooperative_id' => $proposal_id,
								'LogSpecialAndCooperative.mis_employee_id' => $mis_employee_id
							),
							'order' => array('LogSpecialAndCooperative.id' => 'DESC')
						)); 
						$i = 0;
						foreach ($files as $file) {
							if ($numcount == null) {
								$numcount = 1;
							}else {							
								$numcount = $numcount++;
							}
							$fileType = pathinfo($file['name'],PATHINFO_EXTENSION);
					 
							 
							
							$this->request->data['SpecialAndCooperativeDocument']['name'] = $numcount."_".$file['name'];
							$this->request->data['SpecialAndCooperativeDocument']['name_old'] = $file['name'];	
							$this->request->data['SpecialAndCooperativeDocument']['postdate'] = date('Y-m-d H:i:00');		
							$this->request->data['SpecialAndCooperativeDocument']['special_and_cooperative_id'] = $proposal_id;
							$this->request->data['SpecialAndCooperativeDocument']['log_special_and_cooperative_id'] = $checkLastLogSpecialAndCooperative['LogSpecialAndCooperative']['id'];
							$this->request->data['SpecialAndCooperativeDocument']['mis_employee_id'] = $UserName['MisEmployee']['id'];
	
								$filethai = iconv("UTF-8", "TIS-620",  $numcount."_".$file['name']);
								
							
								if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
									$this->request->data['SpecialAndCooperativeDocument']['class'] = "file";
								}else{
									$this->request->data['SpecialAndCooperativeDocument']['class'] = "img";
								}
								
								$folder = WWW_ROOT.'files'.DS.'students'.DS.'specialandcooperatives'.DS.$proposal_id.DS;
			
								if(!file_exists($folder))
									mkdir($folder);
									
								move_uploaded_file($file['tmp_name'],$folder.$filethai);
								// $files[0]['tmp_name']
								
							
							$this->SpecialAndCooperativeDocument->create();					
							$this->SpecialAndCooperativeDocument->save($this->request->data);	
							
						}

					} 
					
					////////////////// before upload ///////////////////
						// $fileDOC = $this->request->data['SpecialAndCooperativeDocument']['fileDOC'];
						// $filePDF = $this->request->data['SpecialAndCooperativeDocument']['filePDF'];					
					
						 
						// $pdf = null;
						// $doc = null;
	
						// if ($fileDOC[0]['name'] != null) {
						// 	$fileType = pathinfo($fileDOC[0]['name'],PATHINFO_EXTENSION);
						// 	if($fileDOC[0]['type'] != ("application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document"or "application/vnd.openxmlformats-officedocument.wordprocessingml.document")){
	
						// 		$this->Session->setFlash('ไฟล์ DOC อัฟโหลดไม่ถูกต้อง');
						// 		$this->Session->write('alertType','danger');							
						// 		$this->redirect(array('controller' => 'SpecialAndCooperatives','action' => 'proposal_detail',$proposal_id,$mis_employee_id));
					
								
						// 	}else{
						// 		$doc = true;
						// 	}
						// }
	
						// if ($filePDF[0]['name'] != null) {
						// 	$fileType = pathinfo($filePDF[0]['name'],PATHINFO_EXTENSION);
						// 	if($filePDF[0]['type'] != ('application/pdf')){
	
						// 		$this->Session->setFlash('ไฟล์ PDF อัฟโหลดไม่ถูกต้อง');
						// 		$this->Session->write('alertType','danger');
						// 		$this->redirect(array('controller' => 'SpecialAndCooperatives','action' => 'proposal_detail',$proposal_id,$mis_employee_id));
									
							
								
						// 	}else{
						// 		$pdf = true;
						// 	}
						// }
						// $numcount = $this->SpecialAndCooperativeDocument->find('count',array(
						// 		'conditions' => array(
						// 			'special_and_cooperative_id' => $proposal_id
						// 		),
						// 	));
						// if($fileDOC[0]['name'] != null){
						// 	if ($numcount == null) {
						// 		$numcount = 1;
						// 	}else {							
						// 		$numcount = $numcount++;
						// 	}
				
						// 	$i = date("m");
						// 	foreach ($fileDOC as $fileDOC) {
						// 		$fileType = pathinfo($fileDOC['name'],PATHINFO_EXTENSION);
							
						// 		$this->request->data['SpecialAndCooperativeDocument']['name'] = $numcount."_".$fileDOC['name'];
						// 		$this->request->data['SpecialAndCooperativeDocument']['postdate'] = date('Y-m-d H:i:00');
						// 		$this->request->data['SpecialAndCooperativeDocument']['special_and_cooperative_id'] = $proposal_id;
						// 		$this->request->data['SpecialAndCooperativeDocument']['mis_employee_id'] = $mis_employee_id;
	
						// 		$filethai = iconv("UTF-8", "TIS-620",  $numcount."_".$fileDOC['name']);
								
							
						// 		if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
						// 			$this->request->data['SpecialAndCooperativeDocument']['class'] = "file";
						// 		}else{
						// 			$this->request->data['SpecialAndCooperativeDocument']['class'] = "img";
						// 		}
								
						// 		$folder = WWW_ROOT.'files'.DS.'students'.DS.'specialandcooperatives'.DS.$proposal_id.DS;
			
						// 		if(!file_exists($folder))
						// 			mkdir($folder);
									
						// 		move_uploaded_file($fileDOC['tmp_name'],$folder.$filethai);
								 
								 
							
						// 	$this->SpecialAndCooperativeDocument->create();					
						// 	$this->SpecialAndCooperativeDocument->save($this->request->data);						
						// 	}
						// }
						// if($filePDF[0]['name'] != null){
						// 	if ($numcount == null) {
						// 		$numcount = 1;
						// 	}else {							
						// 		$numcount = $numcount++;
						// 	}
						// 	$i = date("m");
						// 	foreach ($filePDF as $filePDF) {
						// 		$fileType = pathinfo($filePDF['name'],PATHINFO_EXTENSION);
						
						// 		$this->request->data['SpecialAndCooperativeDocument']['name'] = $numcount."_".$filePDF['name'];
						// 		$this->request->data['SpecialAndCooperativeDocument']['postdate'] = date('Y-m-d H:i:00');
						// 		$this->request->data['SpecialAndCooperativeDocument']['special_and_cooperative_id'] = $proposal_id;
						// 		$this->request->data['SpecialAndCooperativeDocument']['mis_employee_id'] = $mis_employee_id;
	
						// 		$filethai = iconv("UTF-8", "TIS-620",  $numcount."_".$filePDF['name']);
								
							
						// 		if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
						// 			$this->request->data['SpecialAndCooperativeDocument']['class'] = "file";
						// 		}else{
						// 			$this->request->data['SpecialAndCooperativeDocument']['class'] = "img";
						// 		}
								
						// 		$folder = WWW_ROOT.'files'.DS.'students'.DS.'specialandcooperatives'.DS.$proposal_id.DS;
			
						// 		if(!file_exists($folder))
						// 			mkdir($folder);
									
						// 		move_uploaded_file($filePDF['tmp_name'],$folder.$filethai);
						// 		// $files[0]['tmp_name']
						// 		//debug($this->request->data['ProposalDocument']['name']);
							
						// 	$this->SpecialAndCooperativeDocument->create();					
						// 	$this->SpecialAndCooperativeDocument->save($this->request->data);						
						// 	}
						// }
					////////////////////////////////////////////

						//======================= ส่งเมล์คณะ	====================================	
						try{
							$studentEmails = $this->SpecialAndCooperative->find('first',array(
								'conditions' => array(
									'SpecialAndCooperative.id' => $proposal_id,	
									'SpecialAndCooperative.mis_employee_id' => $UserName['MisEmployee']['id'],							
									),
									'order' => array('SpecialAndCooperative.id' => 'DESC')
								));				
									
							$adminProposals = $this->Admincurriculumrequest->find('all', array(
								'conditions' => array(
									'employee_status_id' => 1
								)
							));
							// $adminProposals = $this->Admincurriculumrequest->find('all', array(
							// 	'conditions' => array(
							// 		'mis_employee_id' => 915
							// 	)
							// ));
							$tests = array();
							$tests2 = array();
							foreach($adminProposals as $user)
							{
								$tests[] = $user['MisEmployee']['email_cmu'];
								$tests2[] = $user['MisEmployee']['email_other'];
							}
							
							// $email = new CakeEmail('gmail');
							// $email->to($tests) 									
							// 			->template('special_and_cooperative_confirmation')
							// 			->emailFormat('html')
							// 			->viewVars(array('studentEmails' => $studentEmails))
							// 			->helpers('Html')
							// 			->from('agriculturecmu@gmail.com')												
							// 			->subject('e-TQF.3-4 Notification - MAJOR CODE'.$studentEmails['Major']['major_name'].'')
							// 			->send(); 
						//======================= ปิดการส่งอีเมล์	====================================
						}catch(Exception $e){
							
							$this->Session->setFlash('<i class="glyphicon glyphicon-remove-sign"></i> ขออภัย เกิดเหตุขัดข้องของการส่งอีเมล์ไปยังบุคลากร กรุณาเช็คอีเมล์บุคลากรให้ถูกต้อง');
							$this->Session->write('alertType','danger');		
							$this->redirect(array('controller' => 'SpecialAndCooperatives','action' => 'list_proposal_all' ));
							
						}
							 
						
						$this->Session->setFlash('<i class="glyphicon glyphicon-ok-sign"></i> แก้ไขข้อมูล เรียบร้อยแล้ว');
						$this->Session->write('alertType','success');	
						$this->redirect(array('controller' => 'SpecialAndCooperatives','action' => 'proposal_detail',$proposal_id,$mis_employee_id ));
						// $this->redirect(array('controller' => 'SpecialAndCooperatives','action' => 'list_proposal_all' ));
					
				
				}
			}catch(Exception $e){
				$this->Session->setFlash('<i class="glyphicon glyphicon-remove-sign"></i> ขออภัย เกิดเหตุขัดข้องของการส่งอีเมล์ไปยังบุคลากร กรุณาเช็คอีเมล์บุคลากรให้ถูกต้อง');
				$this->Session->write('alertType','danger');		
				$this->redirect(array('controller' => 'SpecialAndCooperatives','action' => 'proposal_detail',$proposal_id,$mis_employee_id ));
				
			}
		 

			$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first',array(
				'conditions' => array(				
					'SpecialAndCooperative.id' => $proposal_id,
					'SpecialAndCooperative.mis_employee_id' => $UserName['MisEmployee']['id'] ,	
				),
				'order' => array('SpecialAndCooperative.modified' => 'DESC')
			));
			 
			
			//######################### SpecialAndCooperativeDocument ############################
				$output ='';
				 
					
				$proposaldocumentdates = $this->SpecialAndCooperativeDocument->find('all',array(
					'conditions' => array(
						'SpecialAndCooperativeDocument.special_and_cooperative_id' => $proposal_id,				
					
						),
						// 'limit' => 1,
						'group' => array('SpecialAndCooperativeDocument.postdate'),
						'order' => array('SpecialAndCooperativeDocument.postdate' => 'ASC')
					));
				
				foreach ($proposaldocumentdates as $proposaldocumentdate) {
					$num=0;
					$proposaldocuments = $this->SpecialAndCooperativeDocument->find('all',array(
						'conditions' => array(				
							'SpecialAndCooperativeDocument.special_and_cooperative_id' => $proposal_id,		
							'SpecialAndCooperativeDocument.postdate' => $proposaldocumentdate['SpecialAndCooperativeDocument']['postdate'] ,
							),
							'order' => array('SpecialAndCooperativeDocument.postdate' => 'ASC')
						));
						if($num == 0){								
							$output .=
							'<tr>
								<td colspan="2"> วันที่ส่งเอกสาร :
									'. $proposaldocumentdate['SpecialAndCooperativeDocument']['postdate'] .'
								</td>
								
							</tr>';
							
							$num++;
						}else {
							$output .= 
							'';
						}
						$i=0;
						foreach ($proposaldocuments as $proposaldocument) {
							$i++;
							$output .=
							'<tr>
								<td data-title="ลำดับ" align="center"> ลำดับที่ '.$i.'</td></td>
								<td>
									<a href="'.$this->urls.'/files/students/specialandcooperatives/'.$proposal_id.DS.$proposaldocument['SpecialAndCooperativeDocument']['name'].'" class="btn btn-primary" target="_blank">
									'.$proposaldocument['SpecialAndCooperativeDocument']['name'].'<span class="glyphicon glyphicon-cloud-download"></span></a>
								</td>
						</tr>';
						}
						
				}
			//######################### LogSpecialAndCooperative ############################	
				$outputlogs =''; 
					// $logProposals = $this->LogSpecialAndCooperative->find('all',array(
					// 	'conditions' => array(				
					// 		'LogSpecialAndCooperative.special_and_cooperative_id' => $proposal_id,
							
					// 	),
					// 	'order' => array('LogSpecialAndCooperative.id' => 'ASC')
					// ));
			
					// foreach ($logProposals as $logProposal) {
					// 	$outputlogs .='
					
					// 		<tr>
					// 			<td width="70%">'.$logProposal['LogSpecialAndCooperative']['PostName'].'</td>
					// 			<td width="30%"> Date : '.$logProposal['LogSpecialAndCooperative']['created'].'</td>
					// 		</tr>
						
					// 	';
					// }	 
					$logProposals = $this->LogSpecialAndCooperative->find('all',array(
						'conditions' => array(				
							'LogSpecialAndCooperative.special_and_cooperative_id' => $proposal_id,
							'LogSpecialAndCooperative.mis_employee_id' => $mis_employee_id
						),
						'order' => array('LogSpecialAndCooperative.id' => 'ASC')
					));
					
					foreach ($logProposals as $logProposal) {					   
						$output ='';
						$i=0;
							$proposaldocuments = $this->SpecialAndCooperativeDocument->find('all',array(
								'conditions' => array(				
									'SpecialAndCooperativeDocument.special_and_cooperative_id' => $proposal_id,
									'SpecialAndCooperativeDocument.log_special_and_cooperative_id' => $logProposal['LogSpecialAndCooperative']['id'],
								
									),	
									// 'group' => array('SpecialAndCooperativeDocument.log_special_and_cooperative_id'),					
									
								));
								
								$output .= '<table>'; 
									foreach ($proposaldocuments as $proposaldocument) {
										$i++;
										$output .=
										'
										
											<tr>
												<td  data-title="ลำดับ" align="center"> ลำดับที่ '.$i.'</td>
												<td  >
													<a href="'.$this->urls.'/files/students/specialandcooperatives/'.$proposal_id.DS.$proposaldocument['SpecialAndCooperativeDocument']['name'].'" class="btn btn-primary" target="_blank">
													'.$proposaldocument['SpecialAndCooperativeDocument']['name'].' <span class="glyphicon glyphicon-cloud-download"></span></a>
												</td>
											</tr>
										';
									}
								$output .= '</table>';
							
							$outputlogs .='
						
								<tr>
									<td>'.$logProposal['LogSpecialAndCooperative']['PostName'].'</td>
									<td> Date : '.$logProposal['LogSpecialAndCooperative']['created'].'</td>
								</tr>
								<tr>';
								if($proposaldocuments == null){
									$outputlogs .=' ';
								}else{
									$outputlogs .=' <td colspan="2">&emsp;&emsp;'.$output.'</td>';
								}
									
									
									
								$outputlogs .=' </tr>
								
							';
					}
	
			$answers = $this->AnswerSpecialAndCooperative->find('all',array(
				'conditions' => array(			
					'AnswerSpecialAndCooperative.special_and_cooperative_id' => $proposal_id,				 
					'AnswerSpecialAndCooperative.show' => 1
					),
					'order' => array('AnswerSpecialAndCooperative.modified' => 'ASC')
				));
			 
			$this->set(array(
				'SpecialAndCooperatives' => $SpecialAndCooperatives,
				'answers' => $answers,			 
				'proposal_id' => $proposal_id,			 
				'output' => $output,
				'outputlogs' => $outputlogs
				));	
				 
		
	}
	public function add_answer_abstract($proposal_id=null,$mis_employee_id=null,$check = null){
		$UserName = $this->Session->read('MisEmployee');		
		$admins = $this->Session->read('adminCurriculumRequests');
		
		if(isset($UserName)){
			$this->set('UserName', $UserName);
			$this->set('admins', $admins);
			 
		}
		else{
			 
			$this->redirect('https://mis.agri.cmu.ac.th/mis/User/dashboard');
			
		}
		$this->set(array(
			'admins' => $admins,
			'UserName' => $UserName,
			
		));
		 
		$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first',array(
			'conditions' => array(
					'SpecialAndCooperative.id' => $proposal_id ,	
					'SpecialAndCooperative.mis_employee_id' => $mis_employee_id ,						
				),
				'order' => array('SpecialAndCooperative.id' => 'DESC')
			));	
		$employees = $this->MisEmployee->find('first',array(
			'conditions' => array(
				'MisEmployee.id' => $mis_employee_id
			),
		
		));
		$MisEmployeeRequests = $this->MisEmployee->find('first',array(
			'conditions' => array(
				'MisEmployee.id' => $SpecialAndCooperatives['SpecialAndCooperative']['mis_employee_request_id'],
			),
		
		));
		$MisOrganizes = $this->MisOrganize->find('first',array(
			'conditions' => array(
				'MisOrganize.id' => $MisEmployeeRequests['MisEmployee']['mis_organize_id'],
			),
		
		));
		try{
			
			if ($this->request->data) {
					$outputtext = '';
					date_default_timezone_set('Asia/Bangkok');
					//======================Comment================
						if ($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 10) {
							# code...
						} else {
						 
							if ($this->request->data['AnswerSpecialAndCooperative']['Detail'] == null) {
								
							}else {
								$checkSpecialAndCooperativeFollowstatus = $this->SpecialAndCooperativeFollowStatus->find('first',array(
									'conditions' => array(			
										'SpecialAndCooperativeFollowStatus.id' => $SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'],
										),
										
									));	
					 
								$this->request->data['AnswerSpecialAndCooperative']['status'] = 1;
								$this->request->data['AnswerSpecialAndCooperative']['show'] = 1;
								if ($admins['Admincurriculumrequest']['employee_status_id'] == 1) {
									$this->request->data['AnswerSpecialAndCooperative']['PostName'] = "งานบริการการศึกษาและพัฒนาคุณภาพนักศึกษา";
									 
								}elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 2) {
									$this->request->data['AnswerSpecialAndCooperative']['PostName'] = "เจ้าหน้าที่ระดับภาควิชา";
									 
								}else { 
									$sSubPre = '';
									if ($employees['MisSubPrename']['id'] != null) {
									$sSubPre = $sSubPre . $employees['MisSubPrename']['name_short_th'] . ' ';
									}
									if ($sSubPre == '') {
									$sSubPre = $sSubPre . $employees['MisPrename']['name_full_th'] . ' ';
									} 
									 
														  
									$this->request->data['AnswerSpecialAndCooperative']['PostName'] =  $sSubPre.''.$employees['MisEmployee']['fname_th'].' '.$employees['MisEmployee']['lname_th']; 
									
								}
								
								$this->request->data['AnswerSpecialAndCooperative']['special_and_cooperative_id'] = $proposal_id;
								$this->request->data['AnswerSpecialAndCooperative']['mis_employee_id'] = $mis_employee_id;
								$this->request->data['AnswerSpecialAndCooperative']['employee_status_id'] = $admins['Admincurriculumrequest']['employee_status_id'];
								$this->request->data['AnswerSpecialAndCooperative']['mis_employee_id'] = $admins['Admincurriculumrequest']['mis_employee_id'];
								
								$this->AnswerSpecialAndCooperative->save($this->request->data['AnswerSpecialAndCooperative']);	
								if ($admins['Admincurriculumrequest']['employee_status_id'] == 1) {

									if ($this->request->data['AnswerSpecialAndCooperativeDocument']['files'][0]['name'] == null) {
										
									}else {
											if(count($this->request->data['AnswerSpecialAndCooperativeDocument']['files']) >= 1){
													$images = $this->request->data['AnswerSpecialAndCooperativeDocument']['files'];
													//debug($this->request->data['FileProperty']['files']);
													$folderToSaveFiles = WWW_ROOT . 'files/students/specialandcooperatives/'.$proposal_id.'/';
													//debug($folderToSaveFiles);
													date_default_timezone_set('Asia/Bangkok');
													$date = date("Ymd");
													$time = date("His");
													$i = 1;
														foreach ($images as $image){
															//debug($image);
															
															$ext = pathinfo($image['name'],PATHINFO_EXTENSION);
					
																	$lastAnswerProposalId = $this->AnswerSpecialAndCooperative->find('first', array(
																	'order' => array('AnswerSpecialAndCooperative.id DESC')			
																	));
					
																	$lastFileId = $this->AnswerSpecialAndCooperativeDocument->find('first', array(
																	'order' => array('AnswerSpecialAndCooperativeDocument.id DESC')
																	));
																	if ($lastFileId == null) {
																		$last_id = 1;
																	}else {
																		$last_id = $lastFileId['AnswerSpecialAndCooperativeDocument']['id']+1;
																	}
					
																	$this->request->data['AnswerSpecialAndCooperativeDocument']['id'] = $last_id;
																	$this->request->data['AnswerSpecialAndCooperativeDocument']['answer_special_and_cooperative_id'] = $lastAnswerProposalId['AnswerSpecialAndCooperative']['id'];
					
																	$this->request->data['AnswerSpecialAndCooperativeDocument']['name'] = $lastAnswerProposalId['AnswerSpecialAndCooperative']['id'].'_'.$i.'.'.$ext;
																	$this->request->data['AnswerSpecialAndCooperativeDocument']['name_old'] = $image['name'];	
		
		
																	$filethai = iconv("UTF-8", "TIS-620",  $image['name']);
																	if ($ext != 'jpg' and $ext != 'JPG' and $ext != 'PNG' and $ext != 'png' and $ext != 'gif' and $ext != 'GIF' and $ext != 'JPEG' and $ext != 'jpeg' and $ext != 'BPG' and $ext != 'BAT' and $ext != 'HEIF' and $ext != 'HDR' and $ext != 'WebP' and $ext != 'TIFF') {
																		$this->request->data['AnswerSpecialAndCooperativeDocument']['class'] = "file";
																	}else{
																		$this->request->data['AnswerSpecialAndCooperativeDocument']['class'] = "img";
																	}
																	move_uploaded_file( $image['tmp_name'], $folderToSaveFiles .$filethai);	
					
					
																	$this->AnswerSpecialAndCooperativeDocument->save($this->data['AnswerSpecialAndCooperativeDocument']);
					
																	$i++;
					
														}  
						 
											}
									}
								}				
							}
						}
						
					

					if ($admins['Admincurriculumrequest']['employee_status_id'] == 1) {
						
						if ($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 9) {
							$data = array(
								'id' => $this->request->data['SpecialAndCooperative']['id'], 
								'check_pass_id' => 5,
									
							);
							$this->SpecialAndCooperative->save($data);
							//***********************Save ProposalDocument*********************** */
								$SpecialAndCooperatives = $this->SpecialAndCooperative->find('first',array(
									'conditions' => array(
											'SpecialAndCooperative.id' => $proposal_id ,	
											'SpecialAndCooperative.mis_employee_id' => $mis_employee_id ,						
										),
									'order' => array('SpecialAndCooperative.id' => 'DESC')
								));		
										
								$files = $this->request->data['SpecialAndCooperativePassDocument']['files'];	
								if ($this->request->data['SpecialAndCooperativePassDocument']['files'][0]['type'] != null) {
									foreach ($this->request->data['SpecialAndCooperativePassDocument']['files'] as $check) {
										if($check['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif" or "application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document" or "application/x-rar-compressed" or "application/octet-stream" or "application/zip" or "application/octet-stream" or "application/vnd.ms-powerpoint" or "application/vnd.openxmlformats-officedocument.presentationml.presentation" or "application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")){


											foreach ($files as $file) {

												$fileType = pathinfo($file['name'],PATHINFO_EXTENSION);
												if($fileType != "doc" or "docx" or "jpg" or "jpeg" or "png" or "gif" or "zip" or "rar" or "xls" or "xlsx" or "ppt" or "pptx" or "pdf"){

													$this->Session->setFlash('กรุณาแก้ไขชื่อไฟล์ก่อนอัปโหลด โดยไม่ให้มีอักขระ จุด .');
													$this->Session->write('alertType','danger');
												
													$this->redirect(array('controller' => 'SpecialAndCooperatives','action' => 'add_proposals'));
												}
											}


											$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
											$this->Session->write('alertType','danger');
											
										}
									}
								}
									$numcount = $this->SpecialAndCooperativePassDocument->find('count',array(
												'conditions' => array(
													'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id']
												),
											));
								if($files[0]['name'] != null){
									$checkLastLogSpecialAndCooperative = $this->LogSpecialAndCooperative->find('first',array(
										'conditions' => array(				
											// 'LogSpecialAndCooperative.special_and_cooperative_id' => $proposal_id,
											'LogSpecialAndCooperative.mis_employee_id' => $UserName['MisEmployee']['id'] ,
										),
										'order' => array('LogSpecialAndCooperative.id' => 'DESC')
									)); 
									$i = 0;
									foreach ($files as $file) {
										if ($numcount == null) {
											$numcount = 1;
										}else {							
											$numcount = $numcount++;
										}
										$fileType = pathinfo($file['name'],PATHINFO_EXTENSION);
											
										$this->request->data['SpecialAndCooperativePassDocument']['name'] = $numcount."_".$file['name'];
										$this->request->data['SpecialAndCooperativePassDocument']['name_old'] = $file['name'];	
										$this->request->data['SpecialAndCooperativePassDocument']['postdate'] = date('Y-m-d H:i:00');		
										$this->request->data['SpecialAndCooperativePassDocument']['special_and_cooperative_id'] = $SpecialAndCooperatives['SpecialAndCooperative']['id'];
										$this->request->data['SpecialAndCooperativePassDocument']['log_special_and_cooperative_id'] = $checkLastLogSpecialAndCooperative['LogSpecialAndCooperative']['id'];
										$this->request->data['SpecialAndCooperativePassDocument']['mis_employee_id'] = $UserName['MisEmployee']['id'];
				
											$filethai = iconv("UTF-8", "TIS-620",  $numcount."_".$file['name']);
											
										
											if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
												$this->request->data['SpecialAndCooperativePassDocument']['class'] = "file";
											}else{
												$this->request->data['SpecialAndCooperativePassDocument']['class'] = "img";
											}
											
											$folder = WWW_ROOT.'files'.DS.'students'.DS.'specialandcooperatives'.DS.$SpecialAndCooperatives['SpecialAndCooperative']['id'].DS;
						
											if(!file_exists($folder))
												mkdir($folder);
												
											move_uploaded_file($file['tmp_name'],$folder.$filethai);
											// $files[0]['tmp_name']
											
										
										$this->SpecialAndCooperativePassDocument->create();					
										$this->SpecialAndCooperativePassDocument->save($this->request->data);	
										
									}

								} 
									 	
							
							//*************save************* */
						}else {
							 
								//***********************Save Curriculum Request Pass******************************* */
									if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'] == null) {
										
									}else {
										//======================= เก็บ Log SpecialAndCooperative กรณีที่เลือกมีการแก้ไข	====================================	   	
											 
											if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 2) {
												$outputtext = 'จาก';
												$checkspecialandCooperatives = $this->SpecialAndCooperative->find('first',array(
													'conditions' => array(
														'SpecialAndCooperative.id' => $proposal_id ,	
														'SpecialAndCooperative.mis_employee_id' => $mis_employee_id ,							
														),
														'order' => array('SpecialAndCooperative.id' => 'DESC')
													));
												$status = $checkspecialandCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'];
												
												// 'SpecialAndCooperativeFollowStatus.id' => $this->request->data['SpecialAndCooperative']['special_and_cooperative_follow_status_id'],
												// 'SpecialAndCooperativeFollowStatus.id' => $checkspecialandCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'],

												$checkSpecialAndCooperativeFollowstatus = $this->SpecialAndCooperativeFollowStatus->find('first',array(
													'conditions' => array(			
														'SpecialAndCooperativeFollowStatus.id' => $checkspecialandCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'],
														),
														
													));	
												
												$SpecialAndCooperativePasses = $this->SpecialAndCooperativePass->find('first',array(
													'conditions' => array(											 	
														'SpecialAndCooperativePass.id' => $this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'],							
														),
														
													));
												
												
													$this->request->data['LogSpecialAndCooperative']['status'] = 1;
													$this->request->data['LogSpecialAndCooperative']['special_and_cooperative_id'] = $checkspecialandCooperatives['SpecialAndCooperative']['id'];
													$this->request->data['LogSpecialAndCooperative']['mis_employee_id'] = $checkspecialandCooperatives['SpecialAndCooperative']['mis_employee_id'];
													$this->request->data['LogSpecialAndCooperative']['employee_status_id'] = $admins['Admincurriculumrequest']['employee_status_id'];
							
													$log_proposals = $this->LogSpecialAndCooperative->find('first',array(
													'conditions' => array(
														'LogSpecialAndCooperative.special_and_cooperative_id' => $checkspecialandCooperatives['SpecialAndCooperative']['id'],
														'LogSpecialAndCooperative.mis_employee_id' => $checkspecialandCooperatives['SpecialAndCooperative']['mis_employee_id'],
														'LogSpecialAndCooperative.employee_status_id' => $admins['Admincurriculumrequest']['employee_status_id'],
														),
														'order' => array('LogSpecialAndCooperative.id' => 'DESC')
													));
													
													if($checkspecialandCooperatives['SpecialAndCooperative']['degree_id'] != 1){
														if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_type_id'] != null) {
															if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_type_id'] == 1) {
																 
																$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.'  <b>ระดับ'.$checkSpecialAndCooperativeFollowstatus['SpecialAndCooperativeFollowStatus']['name'].'</b> (เข้าที่ประชุม ครั้งที่ '.$this->request->data['SpecialAndCooperative']['num_id'].' วันที่ '.$this->DateThai($this->request->data['SpecialAndCooperative']['postdate']).')';
								
															}elseif ($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_type_id'] == 2) {
																 
																$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.'  <b>ระดับ'.$checkSpecialAndCooperativeFollowstatus['SpecialAndCooperativeFollowStatus']['name'].'</b> ( แจ้งเวียน วันที่ '.$this->DateThai($this->request->data['SpecialAndCooperative']['postdate']).')';
								
															}

														}else {
															$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.' <b>ระดับ'.$checkSpecialAndCooperativeFollowstatus['SpecialAndCooperativeFollowStatus']['name'].'</b>';
															
														}
													}else {

														if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_type_id'] != null) {
															if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_type_id'] == 1) {
																 
																$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.'  <b>ระดับ'.$checkSpecialAndCooperativeFollowstatus['SpecialAndCooperativeFollowStatus']['name'].'</b> (เข้าที่ประชุม ครั้งที่ '.$this->request->data['SpecialAndCooperative']['num_id'].' วันที่ '.$this->DateThai($this->request->data['SpecialAndCooperative']['postdate']).')';
								
															}elseif ($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_type_id'] == 2) {
																 
																$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.'  <b>ระดับ'.$checkSpecialAndCooperativeFollowstatus['SpecialAndCooperativeFollowStatus']['name'].'</b> ( แจ้งเวียน วันที่ '.$this->DateThai($this->request->data['SpecialAndCooperative']['postdate']).')';
								
															}

														}else {
															$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.' <b>ระดับ'.$checkSpecialAndCooperativeFollowstatus['SpecialAndCooperativeFollowStatus']['name'].'</b>';
															
														}

														// if($this->request->data['SpecialAndCooperative']['num_id'] != null){
															
														// 	$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.'  <b>ระดับ'.$checkSpecialAndCooperativeFollowstatus['SpecialAndCooperativeFollowStatus']['name'].'</b> ( ครั้งที่ '.$this->request->data['SpecialAndCooperative']['num_id'].' วันที่ '.$this->DateThai($this->request->data['SpecialAndCooperative']['postdate']).')';
								
														// }else {
														// 	$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.' <b>ระดับ'.$checkSpecialAndCooperativeFollowstatus['SpecialAndCooperativeFollowStatus']['name'].'</b>';
															
														// } 
													}
													$this->LogSpecialAndCooperative->save($this->request->data['LogSpecialAndCooperative']);	
												//*********** */	
											
												
											}
										//************บันทึกสถานะใหม่ special_and_cooperative_pass_id************ */
											if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 4) {
												$checkspecialandCooperatives = $this->SpecialAndCooperative->find('first',array(
													'conditions' => array(
														'SpecialAndCooperative.id' => $proposal_id ,	
														'SpecialAndCooperative.mis_employee_id' => $mis_employee_id ,							
														),
														'order' => array('SpecialAndCooperative.id' => 'DESC')
													));
												$status = $checkspecialandCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id']+1;
												// 'special_and_cooperative_follow_status_id' => $status,
												// 'special_and_cooperative_follow_status_id' => $this->request->data['SpecialAndCooperative']['special_and_cooperative_follow_status_id'],
												$data = array(
													'id' => $this->request->data['SpecialAndCooperative']['id'],
													'special_and_cooperative_follow_status_id' => $this->request->data['SpecialAndCooperative']['special_and_cooperative_follow_status_id']+1,
													// 'special_and_cooperative_follow_status_id' => $status,
													'special_and_cooperative_pass_id' => 4,
														
												);
												$this->SpecialAndCooperative->save($data);
												
											}elseif ($this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 2) {
												
												$checkspecialandCooperatives = $this->SpecialAndCooperative->find('first',array(
													'conditions' => array(
														'SpecialAndCooperative.id' => $proposal_id ,	
														'SpecialAndCooperative.mis_employee_id' => $mis_employee_id ,							
														),
														'order' => array('SpecialAndCooperative.id' => 'DESC')
													));
												$status = $checkspecialandCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id']+1;
												// 'special_and_cooperative_follow_status_id' => $status,
												// 'special_and_cooperative_follow_status_id' => $this->request->data['SpecialAndCooperative']['special_and_cooperative_follow_status_id'],

	
												$checkAdmincurriculumRequest = $this->AdmincurriculumRequest->find('first',array(
													'conditions' => array( 
														'Admincurriculumrequest.mis_employee_id' => $checkspecialandCooperatives['SpecialAndCooperative']['mis_employee_id']
													),
													
												));
												if ($checkAdmincurriculumrequest == null) {
													$status = 2;
												}else {
													if($checkAdmincurriculumRequest['Admincurriculumrequest']['employee_status_id'] == 3){
														 
														$status = 1;
													}elseif ($checkAdmincurriculumRequest['Admincurriculumrequest']['employee_status_id'] == 2) {
														$status = 2;
													}else {
														$status = 2;
													}
													# code...
												}
		
													$data = array(
														'id' => $this->request->data['SpecialAndCooperative']['id'],									 
														'special_and_cooperative_follow_status_id' => $status,
														'special_and_cooperative_pass_id' => 2,
															
													);
													$this->SpecialAndCooperative->save($data);
													 
											}else{
												$data = array(
													'id' => $this->request->data['SpecialAndCooperative']['id'],
													'special_and_cooperative_follow_status_id' => $this->request->data['SpecialAndCooperative']['special_and_cooperative_follow_status_id'],
													'special_and_cooperative_pass_id' => $this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'],
														
												);
												$this->SpecialAndCooperative->save($data);
													
											}
										//======================= เก็บ Log SpecialAndCooperative กรณีที่เลือกเห็นชอบ	====================================	   	
										  
											if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 4) {
												$outputtext = 'จาก';
												$checkspecialandCooperatives = $this->SpecialAndCooperative->find('first',array(
													'conditions' => array(
														'SpecialAndCooperative.id' => $proposal_id ,	
														'SpecialAndCooperative.mis_employee_id' => $mis_employee_id ,							
														),
														'order' => array('SpecialAndCooperative.id' => 'DESC')
													));
												$status = $checkspecialandCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'];
												
												// 'SpecialAndCooperativeFollowStatus.id' => $this->request->data['SpecialAndCooperative']['special_and_cooperative_follow_status_id'],
												// 'SpecialAndCooperativeFollowStatus.id' => $checkspecialandCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'],

												$checkSpecialAndCooperativeFollowstatus = $this->SpecialAndCooperativeFollowStatus->find('first',array(
													'conditions' => array(			
														'SpecialAndCooperativeFollowStatus.id' => $this->request->data['SpecialAndCooperative']['special_and_cooperative_follow_status_id'],
														),
														
													));	
												
												$SpecialAndCooperativePasses = $this->SpecialAndCooperativePass->find('first',array(
													'conditions' => array(											 	
														'SpecialAndCooperativePass.id' => $this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'],							
														),
														
													));
												
												
													$this->request->data['LogSpecialAndCooperative']['status'] = 1;
													$this->request->data['LogSpecialAndCooperative']['special_and_cooperative_id'] = $checkspecialandCooperatives['SpecialAndCooperative']['id'];
													$this->request->data['LogSpecialAndCooperative']['mis_employee_id'] = $checkspecialandCooperatives['SpecialAndCooperative']['mis_employee_id'];
													$this->request->data['LogSpecialAndCooperative']['employee_status_id'] = $admins['Admincurriculumrequest']['employee_status_id'];
							
													$log_proposals = $this->LogSpecialAndCooperative->find('first',array(
													'conditions' => array(
														'LogSpecialAndCooperative.special_and_cooperative_id' => $checkspecialandCooperatives['SpecialAndCooperative']['id'],
														'LogSpecialAndCooperative.mis_employee_id' => $checkspecialandCooperatives['SpecialAndCooperative']['mis_employee_id'],
														'LogSpecialAndCooperative.employee_status_id' => $admins['Admincurriculumrequest']['employee_status_id'],
														),
														'order' => array('LogSpecialAndCooperative.id' => 'DESC')
													));
													
													if($checkspecialandCooperatives['SpecialAndCooperative']['degree_id'] != 1){
														if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_type_id'] != null) {
															if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_type_id'] == 1) {
																 
																$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.'  <b>ระดับ'.$checkSpecialAndCooperativeFollowstatus['SpecialAndCooperativeFollowStatus']['name'].'</b> (เข้าที่ประชุม ครั้งที่ '.$this->request->data['SpecialAndCooperative']['num_id'].' วันที่ '.$this->DateThai($this->request->data['SpecialAndCooperative']['postdate']).')';
								
															}elseif ($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_type_id'] == 2) {
																$this->request->data['LogSpecialAndCooperative']['PostName'] =
																 '<font color="orange">&nbsp;&nbsp;เสนอ มคอ.3-4 </font>  จาก <b>ระดับคณะ </b> แจ้งเวียน วันที่ '.date('Y-m-d', strtotime($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_postdate'])).' ';
																
																$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.'  <b>ระดับ'.$checkSpecialAndCooperativeFollowstatus['SpecialAndCooperativeFollowStatus']['name'].'</b> ( แจ้งเวียน วันที่ '.$this->DateThai($this->request->data['SpecialAndCooperative']['postdate']).')';
								
															}

														}else {
															$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.' <b>ระดับ'.$checkSpecialAndCooperativeFollowstatus['SpecialAndCooperativeFollowStatus']['name'].'</b>';
															
														}
													}else {
														
														if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_type_id'] != null) {
															if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_type_id'] == 1) {
																 
																$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.'  <b>ระดับ'.$checkSpecialAndCooperativeFollowstatus['SpecialAndCooperativeFollowStatus']['name'].'</b> (เข้าที่ประชุม ครั้งที่ '.$this->request->data['SpecialAndCooperative']['num_id'].' วันที่ '.$this->DateThai($this->request->data['SpecialAndCooperative']['postdate']).')';
								
															}elseif ($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_type_id'] == 2) {
																$this->request->data['LogSpecialAndCooperative']['PostName'] =
																 '<font color="orange">&nbsp;&nbsp;เสนอ มคอ.3-4 </font>  จาก <b>ระดับคณะ </b> แจ้งเวียน วันที่ '.date('Y-m-d', strtotime($this->request->data['SpecialAndCooperative']['special_and_cooperative_meeting_postdate'])).' ';
																
																$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.'  <b>ระดับ'.$checkSpecialAndCooperativeFollowstatus['SpecialAndCooperativeFollowStatus']['name'].'</b> ( แจ้งเวียน วันที่ '.$this->DateThai($this->request->data['SpecialAndCooperative']['postdate']).')';
								
															}

														}else {
															$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.' <b>ระดับ'.$checkSpecialAndCooperativeFollowstatus['SpecialAndCooperativeFollowStatus']['name'].'</b>';
															
														} 
													}
													$this->LogSpecialAndCooperative->save($this->request->data['LogSpecialAndCooperative']);	
												//*********** */	
											
												
											}
										//************บันทึกสถานะใหม่ หลังจากบันทึก LOG special_and_cooperative_pass_id************ */
											$checkLastSpecialAndCooperatives = $this->SpecialAndCooperative->find('first',array(
											'conditions' => array(
												'SpecialAndCooperative.id' => $proposal_id ,	
												'SpecialAndCooperative.mis_employee_id' => $mis_employee_id ,							
												),
												'order' => array('SpecialAndCooperative.id' => 'DESC')
											));
											if ($checkLastSpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 2) {
												$checkspecialandCooperatives = $this->SpecialAndCooperative->find('first',array(
													'conditions' => array(
														'SpecialAndCooperative.id' => $proposal_id ,	
														'SpecialAndCooperative.mis_employee_id' => $mis_employee_id ,							
														),
														'order' => array('SpecialAndCooperative.id' => 'DESC')
													));
	
												$checkAdmincurriculumRequests = $this->Admincurriculumrequest->find('first',array(
													'conditions' => array( 
														'Admincurriculumrequest.mis_employee_id' => $checkspecialandCooperatives['SpecialAndCooperative']['mis_employee_id']
													),
													
												));
												
												if ($checkAdmincurriculumRequests == null) {
													$status = 2;
												}else {
													if($checkAdmincurriculumRequests['Admincurriculumrequest']['employee_status_id'] == 3){
														$status = 1;
													}elseif ($checkAdmincurriculumRequests['Admincurriculumrequest']['employee_status_id'] == 2) {
														$status = 2;
													}else {
														$status = 2;
													}
													# code...
												}
												 
												
													$data = array(
														'id' => $this->request->data['SpecialAndCooperative']['id'],									 
														'special_and_cooperative_follow_status_id' => $status,
														'special_and_cooperative_pass_id' => 2,
															
													);
													$this->SpecialAndCooperative->save($data);
													// debug($data);
											}	
									}
								//***********************Save ProposalDocument*********************** */
									// $SpecialAndCooperatives = $this->SpecialAndCooperative->find('first',array(
									// 	'conditions' => array(
									// 			'SpecialAndCooperative.id' => $proposal_id ,	
									// 			'SpecialAndCooperative.mis_employee_id' => $mis_employee_id ,						
									// 		),
									// 		'order' => array('SpecialAndCooperative.id' => 'DESC')
									// 	));	
										 
									// $files = $this->request->data['SpecialAndCooperativeDocument']['files'];	
									// if ($this->request->data['SpecialAndCooperativeDocument']['files'][0]['type'] != null) {
									// 	foreach ($this->request->data['SpecialAndCooperativeDocument']['files'] as $check) {
									// 		if($check['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif" or "application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document" or "application/x-rar-compressed" or "application/octet-stream" or "application/zip" or "application/octet-stream" or "application/vnd.ms-powerpoint" or "application/vnd.openxmlformats-officedocument.presentationml.presentation" or "application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")){
	
	
									// 			foreach ($files as $file) {
	
									// 				$fileType = pathinfo($file['name'],PATHINFO_EXTENSION);
									// 				if($fileType != "doc" or "docx" or "jpg" or "jpeg" or "png" or "gif" or "zip" or "rar" or "xls" or "xlsx" or "ppt" or "pptx" or "pdf"){
	
									// 					$this->Session->setFlash('กรุณาแก้ไขชื่อไฟล์ก่อนอัปโหลด โดยไม่ให้มีอักขระ จุด .');
									// 					$this->Session->write('alertType','danger');
													
									// 					$this->redirect(array('controller' => 'specialandcooperatives','action' => 'add_proposals'));
									// 				}
									// 			}
	
	
									// 			$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
									// 			$this->Session->write('alertType','danger');
												
									// 		}
									// 	}
									// }
									// 	$numcount = $this->SpecialAndCooperativeDocument->find('count',array(
									// 				'conditions' => array(
									// 					'special_and_cooperative_id' => $SpecialAndCooperatives['SpecialAndCooperative']['id']
									// 				),
									// 			));
									// if($files[0]['name'] != null){
									// 	$checkLastLogSpecialAndCooperative = $this->LogSpecialAndCooperative->find('first',array(
									// 		'conditions' => array(				
									// 			'LogSpecialAndCooperative.special_and_cooperative_id' => $proposal_id,
									// 			'LogSpecialAndCooperative.mis_employee_id' => $mis_employee_id ,
									// 		),
									// 		'order' => array('LogSpecialAndCooperative.id' => 'DESC')
									// 	)); 
									// 	$i = 0;
									// 	foreach ($files as $file) {
									// 		if ($numcount == null) {
									// 			$numcount = 1;
									// 		}else {							
									// 			$numcount = $numcount++;
									// 		}
									// 		$fileType = pathinfo($file['name'],PATHINFO_EXTENSION);
												
									// 		$this->request->data['SpecialAndCooperativeDocument']['name'] = $numcount."_".$file['name'];
									// 		$this->request->data['SpecialAndCooperativeDocument']['name_old'] = $file['name'];	
									// 		$this->request->data['SpecialAndCooperativeDocument']['postdate'] = date('Y-m-d H:i:00');		
									// 		$this->request->data['SpecialAndCooperativeDocument']['special_and_cooperative_id'] = $SpecialAndCooperatives['SpecialAndCooperative']['id'];
									// 		$this->request->data['SpecialAndCooperativeDocument']['log_special_and_cooperative_id'] = $checkLastLogSpecialAndCooperative['LogSpecialAndCooperative']['id'];
									// 		$this->request->data['SpecialAndCooperativeDocument']['mis_employee_id'] = $mis_employee_id ;
					
									// 			$filethai = iconv("UTF-8", "TIS-620",  $numcount."_".$file['name']);
												
											
									// 			if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
									// 				$this->request->data['SpecialAndCooperativeDocument']['class'] = "file";
									// 			}else{
									// 				$this->request->data['SpecialAndCooperativeDocument']['class'] = "img";
									// 			}
												
									// 			$folder = WWW_ROOT.'files'.DS.'students'.DS.'specialandcooperatives'.DS.$SpecialAndCooperatives['SpecialAndCooperative']['id'].DS;
							
									// 			if(!file_exists($folder))
									// 				mkdir($folder);
													
									// 			move_uploaded_file($file['tmp_name'],$folder.$filethai);
									// 			// $files[0]['tmp_name']
												
											
									// 		$this->SpecialAndCooperativeDocument->create();					
									// 		$this->SpecialAndCooperativeDocument->save($this->request->data);	
											
									// 	}
	
									// }
								//********** */		 
							 
							 
						}
						
						
					}elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 2) {
						$studentEmails = $this->SpecialAndCooperative->find('first',array(
							'conditions' => array(
								'SpecialAndCooperative.id' => $proposal_id ,	
								'SpecialAndCooperative.mis_employee_id' => $mis_employee_id ,							
								),
								'order' => array('SpecialAndCooperative.id' => 'DESC')
							));
							$checkreplyProposals = $this->SpecialAndCooperative->find('first',array(
								'conditions' => array(				
									'SpecialAndCooperative.id' => $proposal_id,
									'SpecialAndCooperative.mis_employee_id' => $mis_employee_id
								),
								'order' => array('SpecialAndCooperative.modified' => 'DESC')
							));
							//***********************Save Curriculum Request Pass******************************* */

							if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'] == null) {
									
							}else {
								// **========================LogSpecialAndCooperative****************************	   	
								$studentEmails = $this->SpecialAndCooperative->find('first',array(
									'conditions' => array(
										'SpecialAndCooperative.id' => $proposal_id ,	
										'SpecialAndCooperative.mis_employee_id' => $mis_employee_id ,							
										),
										'order' => array('SpecialAndCooperative.id' => 'DESC')
									));
								if($studentEmails['SpecialAndCooperative']['special_and_cooperative_pass_id'] != 5){
									 
										$outputtext = 'จาก';
										
									 
									$SpecialAndCooperativePasses = $this->SpecialAndCooperativePass->find('first',array(
										'conditions' => array(											 	
											'SpecialAndCooperativePass.id' => $this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'],							
											),
											
										));
									
									//======================= เก็บ Log SpecialAndCooperative	====================================
									$this->request->data['LogSpecialAndCooperative']['status'] = 1;
									$this->request->data['LogSpecialAndCooperative']['special_and_cooperative_id'] = $studentEmails['SpecialAndCooperative']['id'];
									$this->request->data['LogSpecialAndCooperative']['mis_employee_id'] = $studentEmails['SpecialAndCooperative']['mis_employee_id'];
									$this->request->data['LogSpecialAndCooperative']['employee_status_id'] = $admins['Admincurriculumrequest']['employee_status_id'];
			
									$log_proposals = $this->LogSpecialAndCooperative->find('first',array(
									'conditions' => array(
										'LogSpecialAndCooperative.special_and_cooperative_id' => $studentEmails['SpecialAndCooperative']['id'],
										'LogSpecialAndCooperative.mis_employee_id' => $studentEmails['SpecialAndCooperative']['mis_employee_id'],
										'LogSpecialAndCooperative.employee_status_id' => $admins['Admincurriculumrequest']['employee_status_id'],
										),
										'order' => array('LogSpecialAndCooperative.id' => 'DESC')
									));
									$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.' <b>ระดับภาควิชา </b>';
				
									// if($studentEmails['SpecialAndCooperative']['degree_id'] != 1){
									// 	if($this->request->data['SpecialAndCooperative']['num_id'] != null){
											
									// 		$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.'  <b>ระดับภาควิชา </b> ( ครั้งที่ '.$this->request->data['SpecialAndCooperative']['num_id'].' วันที่ '.$this->DateThai($this->request->data['SpecialAndCooperative']['postdate']).')';
				
									// 	}else {
									// 		$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.' <b>ระดับภาควิชา </b>';
				
									// 	}
									// }else {
										
									// 	if($this->request->data['SpecialAndCooperative']['num_id'] != null){
											
									// 		$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.'  <b>ระดับภาควิชา </b> ( ครั้งที่ '.$this->request->data['SpecialAndCooperative']['num_id'].' วันที่ '.$this->DateThai($this->request->data['SpecialAndCooperative']['postdate']).')';
				
									// 	}else {
									// 		$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.' <b>ระดับภาควิชา </b>';
				
									// 	} 
									// }
									$this->LogSpecialAndCooperative->save($this->request->data['LogSpecialAndCooperative']);	
							
								
									
								}

								//************บันทึกสถานะใหม่************ */


								if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 4) {									
									 
									$data = array(
										'id' => $this->request->data['SpecialAndCooperative']['id'],									 
										'special_and_cooperative_follow_status_id' => 4,
										'special_and_cooperative_pass_id' => 3,
											
									);
									$this->SpecialAndCooperative->save($data);
									
								}elseif ($this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 2) {
									
									
										$data = array(
											'id' => $this->request->data['SpecialAndCooperative']['id'],									 
											'special_and_cooperative_follow_status_id' => 2,
											'special_and_cooperative_pass_id' => 2,
												
										);
										$this->SpecialAndCooperative->save($data);
										
									
											
								
										
								}else{
									$data = array(
										'id' => $this->request->data['SpecialAndCooperative']['id'],
										'special_and_cooperative_follow_status_id' => $this->request->data['SpecialAndCooperative']['special_and_cooperative_follow_status_id'],
										'special_and_cooperative_pass_id' => $this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'],
											
									);
									$this->SpecialAndCooperative->save($data);
										
								}

								
								
								
							}
							$files = $this->request->data['SpecialAndCooperativeDocument']['files'];	
							
							if($files[0]['name'] != null) { 
								$checkLastLogSpecialAndCooperative = $this->LogSpecialAndCooperative->find('first',array(
									'conditions' => array(				
										'LogSpecialAndCooperative.special_and_cooperative_id' => $proposal_id,
										'LogSpecialAndCooperative.mis_employee_id' => $mis_employee_id
									),
									'order' => array('LogSpecialAndCooperative.id' => 'DESC')
								));
								//***********************Save SpecialAndCooperative******************************* */
								$this->SpecialAndCooperative->save($this->request->data['SpecialAndCooperative']);
								
									/////////////// Save SpecialAndCooperativeDocument /////////////////
									$files = $this->request->data['SpecialAndCooperativeDocument']['files'];	
									if ($this->request->data['SpecialAndCooperativeDocument']['files'][0]['type'] != null) {
										foreach ($this->request->data['SpecialAndCooperativeDocument']['files'] as $check) {
											if($check['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif" or "application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document" or "application/x-rar-compressed" or "application/octet-stream" or "application/zip" or "application/octet-stream" or "application/vnd.ms-powerpoint" or "application/vnd.openxmlformats-officedocument.presentationml.presentation" or "application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")){
				
				
												foreach ($files as $file) {
				
													$fileType = pathinfo($file['name'],PATHINFO_EXTENSION);
													if($fileType != "doc" or "docx" or "jpg" or "jpeg" or "png" or "gif" or "zip" or "rar" or "xls" or "xlsx" or "ppt" or "pptx" or "pdf"){
				
														$this->Session->setFlash('กรุณาแก้ไขชื่อไฟล์ก่อนอัปโหลด โดยไม่ให้มีอักขระ จุด .');
														$this->Session->write('alertType','danger');
													
														$this->redirect(array('controller' => 'specialandcooperatives','action' => 'add_proposals'));
													}
												}
				
				
												$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
												$this->Session->write('alertType','danger');
												
											}
										}
									}
										$numcount = $this->SpecialAndCooperativeDocument->find('count',array(
													'conditions' => array(
														'special_and_cooperative_id' => $proposal_id,
													),
												));
									if($files[0]['name'] != null){
										
										$i = 0;
										foreach ($files as $file) {
											if ($numcount == null) {
												$numcount = 1;
											}else {							
												$numcount = $numcount++;
											}
											$fileType = pathinfo($file['name'],PATHINFO_EXTENSION);
									
											
											
											$this->request->data['SpecialAndCooperativeDocument']['name'] = $numcount."_".$file['name'];
											$this->request->data['SpecialAndCooperativeDocument']['name_old'] = $file['name'];	
											$this->request->data['SpecialAndCooperativeDocument']['postdate'] = date('Y-m-d H:i:00');		
											$this->request->data['SpecialAndCooperativeDocument']['special_and_cooperative_id'] = $proposal_id;
											$this->request->data['SpecialAndCooperativeDocument']['log_special_and_cooperative_id'] = $checkLastLogSpecialAndCooperative['LogSpecialAndCooperative']['id'];
											$this->request->data['SpecialAndCooperativeDocument']['mis_employee_id'] = $UserName['MisEmployee']['id'];
					
												$filethai = iconv("UTF-8", "TIS-620",  $numcount."_".$file['name']);
												
											
												if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
													$this->request->data['SpecialAndCooperativeDocument']['class'] = "file";
												}else{
													$this->request->data['SpecialAndCooperativeDocument']['class'] = "img";
												}
												
												$folder = WWW_ROOT.'files'.DS.'students'.DS.'specialandcooperatives'.DS.$proposal_id.DS;
							
												if(!file_exists($folder))
													mkdir($folder);
													
												move_uploaded_file($file['tmp_name'],$folder.$filethai);
												// $files[0]['tmp_name']
												
											
											$this->SpecialAndCooperativeDocument->create();					
											$this->SpecialAndCooperativeDocument->save($this->request->data);	
											
										}
										
										$data = array(
											'id' => $this->request->data['SpecialAndCooperative']['id'],									 
											'special_and_cooperative_follow_status_id' => 4,
											'special_and_cooperative_pass_id' => 3,
												
										);
										$this->SpecialAndCooperative->save($data);
									} 

							}

							
						    //###########################################################################################
					}elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 3) {
						$studentEmails = $this->SpecialAndCooperative->find('first',array(
							'conditions' => array(
								'SpecialAndCooperative.id' => $proposal_id ,	
								'SpecialAndCooperative.mis_employee_id' => $mis_employee_id ,							
								),
								'order' => array('SpecialAndCooperative.id' => 'DESC')
							));
							$checkreplyProposals = $this->SpecialAndCooperative->find('first',array(
								'conditions' => array(				
									'SpecialAndCooperative.id' => $proposal_id,
									'SpecialAndCooperative.mis_employee_id' => $mis_employee_id
								),
								'order' => array('SpecialAndCooperative.modified' => 'DESC')
							));
							//***********************Save Curriculum Request Pass******************************* */

							if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'] == null) {
									
							}else {
								// **========================LogSpecialAndCooperative****************************	   	
								$studentEmails = $this->SpecialAndCooperative->find('first',array(
									'conditions' => array(
										'SpecialAndCooperative.id' => $proposal_id ,	
										'SpecialAndCooperative.mis_employee_id' => $mis_employee_id ,							
										),
										'order' => array('SpecialAndCooperative.id' => 'DESC')
									));
								if($studentEmails['SpecialAndCooperative']['special_and_cooperative_pass_id'] != 5){
									 
										$outputtext = 'จาก';
										
									 
									$SpecialAndCooperativePasses = $this->SpecialAndCooperativePass->find('first',array(
										'conditions' => array(											 	
											'SpecialAndCooperativePass.id' => $this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'],							
											),
											
										));
									
									//======================= เก็บ Log SpecialAndCooperative	====================================
									$this->request->data['LogSpecialAndCooperative']['status'] = 1;
									$this->request->data['LogSpecialAndCooperative']['special_and_cooperative_id'] = $studentEmails['SpecialAndCooperative']['id'];
									$this->request->data['LogSpecialAndCooperative']['mis_employee_id'] = $studentEmails['SpecialAndCooperative']['mis_employee_id'];
									$this->request->data['LogSpecialAndCooperative']['employee_status_id'] = $admins['Admincurriculumrequest']['employee_status_id'];
			
									$log_proposals = $this->LogSpecialAndCooperative->find('first',array(
									'conditions' => array(
										'LogSpecialAndCooperative.special_and_cooperative_id' => $studentEmails['SpecialAndCooperative']['id'],
										'LogSpecialAndCooperative.mis_employee_id' => $studentEmails['SpecialAndCooperative']['mis_employee_id'],
										'LogSpecialAndCooperative.employee_status_id' => $admins['Admincurriculumrequest']['employee_status_id'],
										),
										'order' => array('LogSpecialAndCooperative.id' => 'DESC')
									));
									
									if($studentEmails['SpecialAndCooperative']['degree_id'] != 1){
										if($this->request->data['SpecialAndCooperative']['num_id'] != null){
											
											$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.'  <b>ระดับอาจารย์/ภาควิชา </b> ( ครั้งที่ '.$this->request->data['SpecialAndCooperative']['num_id'].' วันที่ '.$this->DateThai($this->request->data['SpecialAndCooperative']['postdate']).')';
				
										}else {
											$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.' <b>ระดับอาจารย์/ภาควิชา </b>';
				
										}
									}else {
										
										 
										$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.' <b>ระดับอาจารย์/ภาควิชา </b>';
				
										 
									}
									$this->LogSpecialAndCooperative->save($this->request->data['LogSpecialAndCooperative']);	
							
								
									
								}

								//************บันทึกสถานะใหม่************ */


								if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 4) {									
									 
									$data = array(
										'id' => $this->request->data['SpecialAndCooperative']['id'],									 
										'special_and_cooperative_follow_status_id' => 4,
										'special_and_cooperative_pass_id' => 3,
											
									);
									$this->SpecialAndCooperative->save($data);
									
								}elseif ($this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 2) {
									
									
										$data = array(
											'id' => $this->request->data['SpecialAndCooperative']['id'],									 
											'special_and_cooperative_follow_status_id' => 2,
											'special_and_cooperative_pass_id' => 2,
												
										);
										$this->SpecialAndCooperative->save($data);
										
									
											
								
										
								}else{
									$data = array(
										'id' => $this->request->data['SpecialAndCooperative']['id'],
										'special_and_cooperative_follow_status_id' => $this->request->data['SpecialAndCooperative']['special_and_cooperative_follow_status_id'],
										'special_and_cooperative_pass_id' => $this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'],
											
									);
									$this->SpecialAndCooperative->save($data);
										
								}

								
								
								
							}
							$files = $this->request->data['SpecialAndCooperativeDocument']['files'];	
							
							if($files[0]['name'] != null) { 
								$checkLastLogSpecialAndCooperative = $this->LogSpecialAndCooperative->find('first',array(
									'conditions' => array(				
										'LogSpecialAndCooperative.special_and_cooperative_id' => $proposal_id,
										'LogSpecialAndCooperative.mis_employee_id' => $mis_employee_id
									),
									'order' => array('LogSpecialAndCooperative.id' => 'DESC')
								));
								//***********************Save SpecialAndCooperative******************************* */
								$this->SpecialAndCooperative->save($this->request->data['SpecialAndCooperative']);
								
									/////////////// Save SpecialAndCooperativeDocument /////////////////
									$files = $this->request->data['SpecialAndCooperativeDocument']['files'];	
									if ($this->request->data['SpecialAndCooperativeDocument']['files'][0]['type'] != null) {
										foreach ($this->request->data['SpecialAndCooperativeDocument']['files'] as $check) {
											if($check['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif" or "application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document" or "application/x-rar-compressed" or "application/octet-stream" or "application/zip" or "application/octet-stream" or "application/vnd.ms-powerpoint" or "application/vnd.openxmlformats-officedocument.presentationml.presentation" or "application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")){
				
				
												foreach ($files as $file) {
				
													$fileType = pathinfo($file['name'],PATHINFO_EXTENSION);
													if($fileType != "doc" or "docx" or "jpg" or "jpeg" or "png" or "gif" or "zip" or "rar" or "xls" or "xlsx" or "ppt" or "pptx" or "pdf"){
				
														$this->Session->setFlash('กรุณาแก้ไขชื่อไฟล์ก่อนอัปโหลด โดยไม่ให้มีอักขระ จุด .');
														$this->Session->write('alertType','danger');
													
														$this->redirect(array('controller' => 'SpecialAndCooperatives','action' => 'add_proposals'));
													}
												}
				
				
												$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
												$this->Session->write('alertType','danger');
												
											}
										}
									}
										$numcount = $this->SpecialAndCooperativeDocument->find('count',array(
													'conditions' => array(
														'special_and_cooperative_id' => $proposal_id,
													),
												));
									if($files[0]['name'] != null){
										
										$i = 0;
										foreach ($files as $file) {
											if ($numcount == null) {
												$numcount = 1;
											}else {							
												$numcount = $numcount++;
											}
											$fileType = pathinfo($file['name'],PATHINFO_EXTENSION);
									
											
											
											$this->request->data['SpecialAndCooperativeDocument']['name'] = $numcount."_".$file['name'];
											$this->request->data['SpecialAndCooperativeDocument']['name_old'] = $file['name'];	
											$this->request->data['SpecialAndCooperativeDocument']['postdate'] = date('Y-m-d H:i:00');		
											$this->request->data['SpecialAndCooperativeDocument']['special_and_cooperative_id'] = $proposal_id;
											$this->request->data['SpecialAndCooperativeDocument']['log_special_and_cooperative_id'] = $checkLastLogSpecialAndCooperative['LogSpecialAndCooperative']['id'];
											$this->request->data['SpecialAndCooperativeDocument']['mis_employee_id'] = $UserName['MisEmployee']['id'];
					
												$filethai = iconv("UTF-8", "TIS-620",  $numcount."_".$file['name']);
												
											
												if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
													$this->request->data['SpecialAndCooperativeDocument']['class'] = "file";
												}else{
													$this->request->data['SpecialAndCooperativeDocument']['class'] = "img";
												}
												
												$folder = WWW_ROOT.'files'.DS.'students'.DS.'specialandcooperatives'.DS.$proposal_id.DS;
							
												if(!file_exists($folder))
													mkdir($folder);
													
												move_uploaded_file($file['tmp_name'],$folder.$filethai);
												// $files[0]['tmp_name']
												
											
											$this->SpecialAndCooperativeDocument->create();					
											$this->SpecialAndCooperativeDocument->save($this->request->data);	
											
										}
				
									} 

							}
					}
					
					$this->Session->setFlash('<i class="glyphicon glyphicon-ok-sign"></i> ปรับปรุงข้อมูล เรียบร้อยแล้ว');
					$this->Session->write('alertType','success');	
					$this->redirect(array('action' => 'add_answer_abstract',$proposal_id,$mis_employee_id));	
				
					
			}
	
		}catch(Exception $e){
			$this->Session->setFlash('<i class="glyphicon glyphicon-ok-sign"></i> ปรับสถานะ เรียบร้อยแล้ว!!');
			$this->Session->write('alertType','success');	
			 
		}
		
		//================================================	
		$proposal = $this->SpecialAndCooperative->find('first',array(
			'conditions' => array(				
				'SpecialAndCooperative.id' => $proposal_id,
				'SpecialAndCooperative.mis_employee_id' => $mis_employee_id
			),
			'order' => array('SpecialAndCooperative.modified' => 'DESC')
		));
		$admincurriculumrequests = $this->Admincurriculumrequest->find('first',array(
			'conditions' => array(				
				 
				'Admincurriculumrequest.mis_employee_id' => $proposal['SpecialAndCooperative']['mis_employee_id']
			),
			 
		));
		$employees = $this->MisEmployee->find('first',array(
			'conditions' => array(
				'MisEmployee.id' => $mis_employee_id
			),
		
		));
		
		$SpecialAndCooperativeEmployees = $this->SpecialAndCooperativeEmployee->find('all',array(
			'conditions' => array(				
				'SpecialAndCooperativeEmployee.special_and_cooperative_id' => $proposal_id,
			 
			),
			'order' => array('SpecialAndCooperativeEmployee.modified' => 'DESC')
		));
		$outputSpecialAndCooperativeEmployee = '';
		$j= 0;
		foreach ($SpecialAndCooperativeEmployees as $SpecialAndCooperativeEmployee) {
			$j++;
			$employeeSpecialAndCooperativeEmployees = $this->MisEmployee->find('all',array(
				'conditions' => array(
					'MisEmployee.id' => $SpecialAndCooperativeEmployee['SpecialAndCooperativeEmployee']['mis_employee_id'],
				),
			
			));
			foreach ($employeeSpecialAndCooperativeEmployees as $employee) {
				$sSubPre = '';
				if ($employee['MisSubPrename']['id'] != null) {
					$sSubPre = $sSubPre . $employee['MisSubPrename']['name_short_th'] . ' ';
				}
				if ($sSubPre == '') {
					$sSubPre = $sSubPre . $employee['MisPrename']['name_full_th'] . ' ';
				}
				$outputSpecialAndCooperativeEmployee .= '<tr>
														<td>'.$j.'.</td>
														<td>'.$sSubPre.''.$employee['MisEmployee']['fname_th'].' '.$employee['MisEmployee']['lname_th'].' 
														
														</td>
														
													</tr>';
			}
			# code...
		}
		//######################### SpecialAndCooperativeDocument ############################
		$output = '';  
			
		$proposaldocumentdates = $this->SpecialAndCooperativeDocument->find('all',array(
			'conditions' => array(
				'SpecialAndCooperativeDocument.special_and_cooperative_id' => $proposal_id,				
			 
				),
				// 'limit' => 1,
				'group' => array('SpecialAndCooperativeDocument.postdate'),
				'order' => array('SpecialAndCooperativeDocument.postdate' => 'ASC')
			));
		
		foreach ($proposaldocumentdates as $proposaldocumentdate) {
			$num=0;
			$proposaldocuments = $this->SpecialAndCooperativeDocument->find('all',array(
				'conditions' => array(				
					'SpecialAndCooperativeDocument.special_and_cooperative_id' => $proposal_id,		
					'SpecialAndCooperativeDocument.postdate' => $proposaldocumentdate['SpecialAndCooperativeDocument']['postdate'] ,
					),
					'order' => array('SpecialAndCooperativeDocument.postdate' => 'ASC')
				));
				if($num == 0){								
					$output .=
					'<tr>
						<td colspan="2"> วันที่ส่งเอกสาร :
							'. $proposaldocumentdate['SpecialAndCooperativeDocument']['postdate'] .'
						</td>
						
					</tr>';
					
					$num++;
				}else {
					$output .= 
					'';
				}
				$i=0;
				foreach ($proposaldocuments as $proposaldocument) {
					$i++;
					$output .=
					'<tr>
						<td data-title="ลำดับ" align="center"> ลำดับที่ '.$i.'</td></td>
						<td>
							<a href="'.$this->urls.'/files/students/specialandcooperatives/'.$proposal_id.DS.$proposaldocument['SpecialAndCooperativeDocument']['name'].'" class="btn btn-primary" target="_blank">
							'.$proposaldocument['SpecialAndCooperativeDocument']['name'].'<span class="glyphicon glyphicon-cloud-download"></span></a>
						</td>
				  </tr>';
				}
				
		}
		//######################### SpecialAndCooperativePassDocument ############################
		$outputpass = '';  
			
		$proposaldocumentdates = $this->SpecialAndCooperativePassDocument->find('all',array(
			'conditions' => array(
				'SpecialAndCooperativePassDocument.special_and_cooperative_id' => $proposal_id,				
			 
				),
				// 'limit' => 1,
				'group' => array('SpecialAndCooperativePassDocument.postdate'),
				'order' => array('SpecialAndCooperativePassDocument.postdate' => 'ASC')
			));
		
		foreach ($proposaldocumentdates as $proposaldocumentdate) {
			$num=0;
			$proposaldocuments = $this->SpecialAndCooperativePassDocument->find('all',array(
				'conditions' => array(				
					'SpecialAndCooperativePassDocument.special_and_cooperative_id' => $proposal_id,		
					'SpecialAndCooperativePassDocument.postdate' => $proposaldocumentdate['SpecialAndCooperativePassDocument']['postdate'] ,
					),
					'order' => array('SpecialAndCooperativePassDocument.postdate' => 'ASC')
				));
				if($num == 0){								
					$outputpass .=
					'<tr>
						<td colspan="2"> ผลบังคับใช้ :
							'. $proposal['Yearterm']['name'] .'
						</td>
						
					</tr>';
					
					$num++;
				}else {
					$outputpass .= 
					'';
				}
				$i=0;
				foreach ($proposaldocuments as $proposaldocument) {
					$i++;
					$outputpass .=
					'<tr>
						<td data-title="ลำดับ" align="center"> ลำดับที่ '.$i.'</td></td>
						<td>
							<a href="'.$this->urls.'/files/students/specialandcooperatives/'.$proposal_id.DS.$proposaldocument['SpecialAndCooperativePassDocument']['name'].'" class="btn btn-primary" target="_blank">
							'.$proposaldocument['SpecialAndCooperativePassDocument']['name'].'<span class="glyphicon glyphicon-cloud-download"></span></a>
						</td>
				  </tr>';
				}
				
		}
		//######################### LogSpecialAndCooperative ############################	
			$outputlogs =''; 
			  
			$logProposals = $this->LogSpecialAndCooperative->find('all',array(
				'conditions' => array(				
					'LogSpecialAndCooperative.special_and_cooperative_id' => $proposal_id,
					'LogSpecialAndCooperative.mis_employee_id' => $mis_employee_id
				),
				'order' => array('LogSpecialAndCooperative.id' => 'ASC')
			));
			
			foreach ($logProposals as $logProposal) {					   
				$output ='';
				$i=0;
					$proposaldocuments = $this->SpecialAndCooperativeDocument->find('all',array(
						'conditions' => array(				
							'SpecialAndCooperativeDocument.special_and_cooperative_id' => $proposal_id,
							'SpecialAndCooperativeDocument.log_special_and_cooperative_id' => $logProposal['LogSpecialAndCooperative']['id'],
						 
							),	
							// 'group' => array('SpecialAndCooperativeDocument.log_special_and_cooperative_id'),					
							 
						));
						 
						$output .= '<table>'; 
							foreach ($proposaldocuments as $proposaldocument) {
								$i++;
								$output .=
								'
								
									<tr>
										<td  data-title="ลำดับ" align="center"> ลำดับที่ '.$i.'</td>
										<td  >
											<a href="'.$this->urls.'/files/students/specialandcooperatives/'.$proposal_id.DS.$proposaldocument['SpecialAndCooperativeDocument']['name'].'" class="btn btn-primary" target="_blank">
											'.$proposaldocument['SpecialAndCooperativeDocument']['name'].' <span class="glyphicon glyphicon-cloud-download"></span></a>
										</td>
									</tr>
								';
							}
						$output .= '</table>';
					  
					$outputlogs .='
				
						<tr>
							<td>'.$logProposal['LogSpecialAndCooperative']['PostName'].'</td>
							<td> Date : '.$logProposal['LogSpecialAndCooperative']['created'].'</td>
						</tr>
						<tr>';
						 if($proposaldocuments == null){
							$outputlogs .=' ';
						 }else{
							$outputlogs .=' <td colspan="2">&emsp;&emsp;'.$output.'</td>';
						 }
							
							
							 
						$outputlogs .=' </tr>
						  
					';
			}
			 
				 

					

			

		$answers = $this->AnswerSpecialAndCooperative->find('all',array(
			'conditions' => array(			
				'AnswerSpecialAndCooperative.special_and_cooperative_id' => $proposal_id,
				 
				),
				'order' => array('AnswerSpecialAndCooperative.modified' => 'ASC')
			));
		//debug($answers);
			if ($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 9) {
				
				$passes = $this->SpecialAndCooperativePass->find('list',array(
					'conditions' => array(
							
						'OR' => array(
							array('SpecialAndCooperativePass.level ' => 3),
							array('SpecialAndCooperativePass.level ' => 1)
							),
						),
						
				));
			}else{
				$passes = $this->SpecialAndCooperativePass->find('list',array(
					'conditions' => array(			
						'and' => array(
							array('SpecialAndCooperativePass.level <= ' => 2),
							array('SpecialAndCooperativePass.level >= ' => 1)
							),
						),
						
					));
			}

			$SpecialAndCooperativeFollowpasses = $this->SpecialAndCooperativeFollowStatus->find('list',array(
				'conditions' => array(			
					'SpecialAndCooperativeFollowStatus.level' => 1,
					),
					
				));	 
			
			 
		$this->set(array( 
			'employees' => $employees,
			'MisEmployeeRequests' => $MisEmployeeRequests,
			'MisOrganizes' => $MisOrganizes,
			'proposal' => $proposal,
			'answers' => $answers,
			'SpecialAndCooperativeFollowpasses' => $SpecialAndCooperativeFollowpasses,
			'proposal_id' => $proposal_id,
			'mis_employee_id' => $mis_employee_id,
			'passes' => $passes,
			'output' => $output,
			'outputlogs' => $outputlogs, 
			'outputSpecialAndCooperativeEmployee' => $outputSpecialAndCooperativeEmployee,
			'proposaldocumentdates' => $proposaldocumentdates,
			'outputpass' => $outputpass,
			'admincurriculumrequests' => $admincurriculumrequests
			));
	}
	public function add_answer_pass_abstract($proposal_id=null,$mis_employee_id=null,$check = null){
		$UserName = $this->Session->read('MisEmployee');		
		$admins = $this->Session->read('adminCurriculumRequests');
		
		if(isset($UserName)){
			$this->set('UserName', $UserName);
			$this->set('admins', $admins);
			
 		 
		}
		else{
			$this->redirect('https://mis.agri.cmu.ac.th/mis/User/dashboard');
			
		}
		$this->set(array(
			'admins' => $admins,
			'UserName' => $UserName,
			
		));
		try{
			
			if ($this->request->data) {
					$outputtext = '';
					date_default_timezone_set('Asia/Bangkok');
					//======================Comment================
						if ($this->request->data['AnswerSpecialAndCooperative']['Detail'] == null) {
							
						}else {	
							$sSubPre = '';
							if ($UserName['MisSubPrename']['id'] != null) {
								$sSubPre = $sSubPre . $UserName['MisSubPrename']['name_short_th'] . ' ';
							}
							if ($sSubPre == '') {
								$sSubPre = $sSubPre . $UserName['MisPrename']['name_full_th'] . ' ';
							}
							$this->request->data['AnswerSpecialAndCooperative']['status'] = 1;
							$this->request->data['AnswerSpecialAndCooperative']['show'] = 1;
							$this->request->data['AnswerSpecialAndCooperative']['PostName'] = $sSubPre.''.$UserName['MisEmployee']['fname_th'].' '.$UserName['MisEmployee']['lname_th'];
							$this->request->data['AnswerSpecialAndCooperative']['special_and_cooperative_id'] = $proposal_id;
							$this->request->data['AnswerSpecialAndCooperative']['mis_employee_id'] = $mis_employee_id;
							$this->request->data['AnswerSpecialAndCooperative']['employee_status_id'] = $admins['Admincurriculumrequest']['employee_status_id'];
							$this->request->data['AnswerSpecialAndCooperative']['mis_employee_id'] = $admins['Admincurriculumrequest']['mis_employee_id'];
							
							$this->AnswerSpecialAndCooperative->save($this->request->data['AnswerSpecialAndCooperative']);					
							if ($this->request->data['AnswerSpecialAndCooperativeDocument']['files'][0]['name'] == null) {
								
							}else {
								if(count($this->request->data['AnswerSpecialAndCooperativeDocument']['files']) >= 1){
										$images = $this->request->data['AnswerSpecialAndCooperativeDocument']['files'];
										//debug($this->request->data['FileProperty']['files']);
										$folderToSaveFiles = WWW_ROOT . 'files/students/specialandcooperatives/'.$proposal_id.'/';
										//debug($folderToSaveFiles);
										date_default_timezone_set('Asia/Bangkok');
										$date = date("Ymd");
										$time = date("His");
										$i = 1;
											foreach ($images as $image){
												//debug($image);
		
												$ext = pathinfo($image['name'],PATHINFO_EXTENSION);
		
														$lastAnswerProposalId = $this->AnswerSpecialAndCooperative->find('first', array(
														'order' => array('AnswerSpecialAndCooperative.id DESC')			
														));
		
														$lastFileId = $this->AnswerSpecialAndCooperativeDocument->find('first', array(
														'order' => array('AnswerSpecialAndCooperativeDocument.id DESC')
														));
														if ($lastFileId == null) {
															$last_id = 1;
														}else {
															$last_id = $lastFileId['AnswerSpecialAndCooperativeDocument']['id']+1;
														}
		
														$this->request->data['AnswerSpecialAndCooperativeDocument']['id'] = $last_id;
														$this->request->data['AnswerSpecialAndCooperativeDocument']['answer_special_and_cooperative_id'] = $lastAnswerProposalId['AnswerSpecialAndCooperative']['id'];
		
														$this->request->data['AnswerSpecialAndCooperativeDocument']['name'] = $lastAnswerProposalId['AnswerSpecialAndCooperative']['id'].'_'.$i.'.'.$ext;
														$this->request->data['AnswerSpecialAndCooperativeDocument']['name_old'] = $image['name'];	
														$result = move_uploaded_file( $image['tmp_name'], $folderToSaveFiles .$image['name']);	
		
		
														$this->AnswerSpecialAndCooperativeDocument->save($this->data['AnswerSpecialAndCooperativeDocument']);
		
														$i++;
		
											} // foreach images
		
		
								}
							}
						}
					

					if ($admins['Admincurriculumrequest']['employee_status_id'] == 1) {
						$checkreplyProposals = $this->SpecialAndCooperative->find('first',array(
							'conditions' => array(				
								'SpecialAndCooperative.id' => $proposal_id,
								'SpecialAndCooperative.mis_employee_id' => $mis_employee_id
							),
							'order' => array('SpecialAndCooperative.modified' => 'DESC')
						));
						if ($checkreplyProposals['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] >= 4){
							//***********************Save Curriculum Request Pass******************************* */
								if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'] == null) {
									
								}else {
									// **========================บันทึกการพิจารณา****************************	   	
									$studentEmails = $this->SpecialAndCooperative->find('first',array(
										'conditions' => array(
											'SpecialAndCooperative.id' => $proposal_id ,	
											'SpecialAndCooperative.mis_employee_id' => $mis_employee_id ,							
											),
											'order' => array('SpecialAndCooperative.id' => 'DESC')
										));
									if($studentEmails['SpecialAndCooperative']['special_and_cooperative_pass_id'] != 5){
										$outputtext = 'จาก';
											

										
										$SpecialAndCooperativePasses = $this->SpecialAndCooperativePass->find('first',array(
											'conditions' => array(											 	
												'SpecialAndCooperativePass.id' => $this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'],							
												),
												
											));
										
										//======================= เก็บ Log SpecialAndCooperative	====================================
										$this->request->data['LogSpecialAndCooperative']['status'] = 1;
										$this->request->data['LogSpecialAndCooperative']['special_and_cooperative_id'] = $studentEmails['SpecialAndCooperative']['id'];
										$this->request->data['LogSpecialAndCooperative']['mis_employee_id'] = $studentEmails['SpecialAndCooperative']['mis_employee_id'];
										$this->request->data['LogSpecialAndCooperative']['employee_status_id'] = $admins['Admincurriculumrequest']['employee_status_id'];
				
										$log_proposals = $this->LogSpecialAndCooperative->find('first',array(
										'conditions' => array(
											'LogSpecialAndCooperative.special_and_cooperative_id' => $studentEmails['SpecialAndCooperative']['id'],
											'LogSpecialAndCooperative.mis_employee_id' => $studentEmails['SpecialAndCooperative']['mis_employee_id'],
											'LogSpecialAndCooperative.employee_status_id' => $admins['Admincurriculumrequest']['employee_status_id'],
											),
											'order' => array('LogSpecialAndCooperative.id' => 'DESC')
										));
										
										if($studentEmails['SpecialAndCooperative']['degree_id'] != 1){
											if($this->request->data['SpecialAndCooperative']['num_id'] != null){
												
												$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.'  <b>ระดับ'.$studentEmails['SpecialAndCooperativeFollowStatus']['name_en'].'</b> ( ครั้งที่ '.$this->request->data['SpecialAndCooperative']['num_id'].' วันที่ '.$this->DateThai($this->request->data['SpecialAndCooperative']['postdate']).')';
					
											}else {
												$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.' <b>ระดับ'.$studentEmails['SpecialAndCooperativeFollowStatus']['name_en'].'</b>';
					
											}
										}else {
											
											if($this->request->data['SpecialAndCooperative']['num_id'] != null){
												
												$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.'  <b>ระดับ'.$studentEmails['SpecialAndCooperativeFollowStatus']['name'].'</b> ( ครั้งที่ '.$this->request->data['SpecialAndCooperative']['num_id'].' วันที่ '.$this->DateThai($this->request->data['SpecialAndCooperative']['postdate']).')';
					
											}else {
												$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.' <b>ระดับ'.$studentEmails['SpecialAndCooperativeFollowStatus']['name'].'</b>';
					
											} 
										}
										$this->LogSpecialAndCooperative->save($this->request->data['LogSpecialAndCooperative']);	
								
									
										
									}

									//************บันทึกสถานะใหม่************ */


									if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 5) {
									
										$data = array(
											'id' => $this->request->data['SpecialAndCooperative']['id'],									 
											'special_and_cooperative_follow_status_id' => 10,
											'special_and_cooperative_pass_id' => 5,
												
										);
										$this->SpecialAndCooperative->save($data);
										
									}elseif ($this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 4) {
									
										$status = $checkreplyProposals['SpecialAndCooperative']['special_and_cooperative_follow_status_id']+1;
										$data = array(
											'id' => $this->request->data['SpecialAndCooperative']['id'],									 
											'special_and_cooperative_follow_status_id' => $status,
											'special_and_cooperative_pass_id' => 3,
												
										);
										$this->SpecialAndCooperative->save($data);
										
									}elseif ($this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 2) {
										$checkAdmincurriculumRequests = $this->Admincurriculumrequest->find('first',array(
											'conditions' => array( 
												'Admincurriculumrequest.mis_employee_id' => $mis_employee_id
											),
											
										));
										if($checkAdmincurriculumRequests['Admincurriculumrequest']['employee_status_id'] == 3){
											$status = 1;
										}elseif ($checkAdmincurriculumRequests['Admincurriculumrequest']['employee_status_id'] == 2) {
											$status = 2;
										}

											$data = array(
												'id' => $this->request->data['SpecialAndCooperative']['id'],									 
												'special_and_cooperative_follow_status_id' => $status,
												'special_and_cooperative_pass_id' => 2,
													
											);
											$this->SpecialAndCooperative->save($data);
											
										
												
									
											
									}else{
										$data = array(
											'id' => $this->request->data['SpecialAndCooperative']['id'],
											'special_and_cooperative_follow_status_id' => $this->request->data['SpecialAndCooperative']['special_and_cooperative_follow_status_id'],
											'special_and_cooperative_pass_id' => $this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'],
												
										);
										$this->SpecialAndCooperative->save($data);
											
									}

									
									
									
								}
							//***********************Save Curriculum Request ******************************* */
							 
							$this->SpecialAndCooperative->save($this->request->data['SpecialAndCooperative']);
							// $files = $this->request->data['SpecialAndCooperativeDocument']['files'];	
							
							// if($files[0]['name'] != null) { 
							// 	$checkLastLogSpecialAndCooperative = $this->LogSpecialAndCooperative->find('first',array(
							// 		'conditions' => array(				
							// 			'LogSpecialAndCooperative.special_and_cooperative_id' => $proposal_id,
							// 			'LogSpecialAndCooperative.mis_employee_id' => $mis_employee_id
							// 		),
							// 		'order' => array('LogSpecialAndCooperative.id' => 'DESC')
							// 	));
							// 	//***********************Save SpecialAndCooperative******************************* */
							// 		$this->SpecialAndCooperative->save($this->request->data['SpecialAndCooperative']);
								
							// 		/////////////// Save SpecialAndCooperativeDocument /////////////////
							// 		$files = $this->request->data['SpecialAndCooperativeDocument']['files'];	
							// 		if ($this->request->data['SpecialAndCooperativeDocument']['files'][0]['type'] != null) {
							// 			foreach ($this->request->data['SpecialAndCooperativeDocument']['files'] as $check) {
							// 				if($check['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif" or "application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document" or "application/x-rar-compressed" or "application/octet-stream" or "application/zip" or "application/octet-stream" or "application/vnd.ms-powerpoint" or "application/vnd.openxmlformats-officedocument.presentationml.presentation" or "application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")){
				
				
							// 					foreach ($files as $file) {
				
							// 						$fileType = pathinfo($file['name'],PATHINFO_EXTENSION);
							// 						if($fileType != "doc" or "docx" or "jpg" or "jpeg" or "png" or "gif" or "zip" or "rar" or "xls" or "xlsx" or "ppt" or "pptx" or "pdf"){
				
							// 							$this->Session->setFlash('กรุณาแก้ไขชื่อไฟล์ก่อนอัปโหลด โดยไม่ให้มีอักขระ จุด .');
							// 							$this->Session->write('alertType','danger');
													
							// 							$this->redirect(array('controller' => 'specialandcooperatives','action' => 'add_proposals'));
							// 						}
							// 					}
				
				
							// 					$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
							// 					$this->Session->write('alertType','danger');
												
							// 				}
							// 			}
							// 		}
							// 			$numcount = $this->SpecialAndCooperativeDocument->find('count',array(
							// 						'conditions' => array(
							// 							'special_and_cooperative_id' => $proposal_id,
							// 						),
							// 					));
							// 		if($files[0]['name'] != null){
										
							// 			$i = 0;
							// 			foreach ($files as $file) {
							// 				if ($numcount == null) {
							// 					$numcount = 1;
							// 				}else {							
							// 					$numcount = $numcount++;
							// 				}
							// 				$fileType = pathinfo($file['name'],PATHINFO_EXTENSION);
									
											
											
							// 				$this->request->data['SpecialAndCooperativeDocument']['name'] = $numcount."_".$file['name'];
							// 				$this->request->data['SpecialAndCooperativeDocument']['name_old'] = $file['name'];	
							// 				$this->request->data['SpecialAndCooperativeDocument']['postdate'] = date('Y-m-d H:i:00');		
							// 				$this->request->data['SpecialAndCooperativeDocument']['special_and_cooperative_id'] = $proposal_id;
							// 				$this->request->data['SpecialAndCooperativeDocument']['log_special_and_cooperative_id'] = $checkLastLogSpecialAndCooperative['LogSpecialAndCooperative']['id'];
							// 				$this->request->data['SpecialAndCooperativeDocument']['mis_employee_id'] = $UserName['MisEmployee']['id'];
					
							// 					$filethai = iconv("UTF-8", "TIS-620",  $numcount."_".$file['name']);
												
											
							// 					if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
							// 						$this->request->data['SpecialAndCooperativeDocument']['class'] = "file";
							// 					}else{
							// 						$this->request->data['SpecialAndCooperativeDocument']['class'] = "img";
							// 					}
												
							// 					$folder = WWW_ROOT.'files'.DS.'students'.DS.'specialandcooperatives'.DS.$proposal_id.DS;
							
							// 					if(!file_exists($folder))
							// 						mkdir($folder);
													
							// 					move_uploaded_file($file['tmp_name'],$folder.$filethai);
							// 					// $files[0]['tmp_name']
												
											
							// 				$this->SpecialAndCooperativeDocument->create();					
							// 				$this->SpecialAndCooperativeDocument->save($this->request->data);	
											
							// 			}
				
							// 		} 

							// }
						}
						
						
					}elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 2) {
						$studentEmails = $this->SpecialAndCooperative->find('first',array(
							'conditions' => array(
								'SpecialAndCooperative.id' => $proposal_id ,	
								'SpecialAndCooperative.mis_employee_id' => $mis_employee_id ,							
								),
								'order' => array('SpecialAndCooperative.id' => 'DESC')
							));
							$checkreplyProposals = $this->SpecialAndCooperative->find('first',array(
								'conditions' => array(				
									'SpecialAndCooperative.id' => $proposal_id,
									'SpecialAndCooperative.mis_employee_id' => $mis_employee_id
								),
								'order' => array('SpecialAndCooperative.modified' => 'DESC')
							));
							//***********************Save Curriculum Request Pass******************************* */

							if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'] == null) {
									
							}else {
								// **========================LogSpecialAndCooperative****************************	   	
								$studentEmails = $this->SpecialAndCooperative->find('first',array(
									'conditions' => array(
										'SpecialAndCooperative.id' => $proposal_id ,	
										'SpecialAndCooperative.mis_employee_id' => $mis_employee_id ,							
										),
										'order' => array('SpecialAndCooperative.id' => 'DESC')
									));
								if($studentEmails['SpecialAndCooperative']['special_and_cooperative_pass_id'] != 5){
									 
										$outputtext = 'จาก';
										
									 
									$SpecialAndCooperativePasses = $this->SpecialAndCooperativePass->find('first',array(
										'conditions' => array(											 	
											'SpecialAndCooperativePass.id' => $this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'],							
											),
											
										));
									
									//======================= เก็บ Log SpecialAndCooperative	====================================
									$this->request->data['LogSpecialAndCooperative']['status'] = 1;
									$this->request->data['LogSpecialAndCooperative']['special_and_cooperative_id'] = $studentEmails['SpecialAndCooperative']['id'];
									$this->request->data['LogSpecialAndCooperative']['mis_employee_id'] = $studentEmails['SpecialAndCooperative']['mis_employee_id'];
									$this->request->data['LogSpecialAndCooperative']['employee_status_id'] = $admins['Admincurriculumrequest']['employee_status_id'];
			
									$log_proposals = $this->LogSpecialAndCooperative->find('first',array(
									'conditions' => array(
										'LogSpecialAndCooperative.special_and_cooperative_id' => $studentEmails['SpecialAndCooperative']['id'],
										'LogSpecialAndCooperative.mis_employee_id' => $studentEmails['SpecialAndCooperative']['mis_employee_id'],
										'LogSpecialAndCooperative.employee_status_id' => $admins['Admincurriculumrequest']['employee_status_id'],
										),
										'order' => array('LogSpecialAndCooperative.id' => 'DESC')
									));
									
									if($studentEmails['SpecialAndCooperative']['degree_id'] != 1){
										if($this->request->data['SpecialAndCooperative']['num_id'] != null){
											
											$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.'  <b>ระดับภาควิชา </b> ( ครั้งที่ '.$this->request->data['SpecialAndCooperative']['num_id'].' วันที่ '.$this->DateThai($this->request->data['SpecialAndCooperative']['postdate']).')';
				
										}else {
											$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.' <b>ระดับภาควิชา </b>';
				
										}
									}else {
										
										if($this->request->data['SpecialAndCooperative']['num_id'] != null){
											
											$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.'  <b>ระดับภาควิชา </b> ( ครั้งที่ '.$this->request->data['SpecialAndCooperative']['num_id'].' วันที่ '.$this->DateThai($this->request->data['SpecialAndCooperative']['postdate']).')';
				
										}else {
											$this->request->data['LogSpecialAndCooperative']['PostName'] = ''.$SpecialAndCooperativePasses['SpecialAndCooperativePass']['name'].' '.$outputtext.' <b>ระดับภาควิชา </b>';
				
										} 
									}
									$this->LogSpecialAndCooperative->save($this->request->data['LogSpecialAndCooperative']);	
							
								
									
								}

								//************บันทึกสถานะใหม่************ */


								if ($this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 4) {									
									 
									$data = array(
										'id' => $this->request->data['SpecialAndCooperative']['id'],									 
										'special_and_cooperative_follow_status_id' => 4,
										'special_and_cooperative_pass_id' => 3,
											
									);
									$this->SpecialAndCooperative->save($data);
									
								}elseif ($this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 2) {
									
									
										$data = array(
											'id' => $this->request->data['SpecialAndCooperative']['id'],									 
											'special_and_cooperative_follow_status_id' => 2,
											'special_and_cooperative_pass_id' => 2,
												
										);
										$this->SpecialAndCooperative->save($data);
										
									
											
								
										
								}else{
									$data = array(
										'id' => $this->request->data['SpecialAndCooperative']['id'],
										'special_and_cooperative_follow_status_id' => $this->request->data['SpecialAndCooperative']['special_and_cooperative_follow_status_id'],
										'special_and_cooperative_pass_id' => $this->request->data['SpecialAndCooperative']['special_and_cooperative_pass_id'],
											
									);
									$this->SpecialAndCooperative->save($data);
										
								}

								
								
								
							}
							$files = $this->request->data['SpecialAndCooperativeDocument']['files'];	
							
							if($files[0]['name'] != null) { 
								$checkLastLogSpecialAndCooperative = $this->LogSpecialAndCooperative->find('first',array(
									'conditions' => array(				
										'LogSpecialAndCooperative.special_and_cooperative_id' => $proposal_id,
										'LogSpecialAndCooperative.mis_employee_id' => $mis_employee_id
									),
									'order' => array('LogSpecialAndCooperative.id' => 'DESC')
								));
								//***********************Save SpecialAndCooperative******************************* */
								$this->SpecialAndCooperative->save($this->request->data['SpecialAndCooperative']);
								
									/////////////// Save SpecialAndCooperativeDocument /////////////////
									$files = $this->request->data['SpecialAndCooperativeDocument']['files'];	
									if ($this->request->data['SpecialAndCooperativeDocument']['files'][0]['type'] != null) {
										foreach ($this->request->data['SpecialAndCooperativeDocument']['files'] as $check) {
											if($check['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif" or "application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document" or "application/x-rar-compressed" or "application/octet-stream" or "application/zip" or "application/octet-stream" or "application/vnd.ms-powerpoint" or "application/vnd.openxmlformats-officedocument.presentationml.presentation" or "application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")){
				
				
												foreach ($files as $file) {
				
													$fileType = pathinfo($file['name'],PATHINFO_EXTENSION);
													if($fileType != "doc" or "docx" or "jpg" or "jpeg" or "png" or "gif" or "zip" or "rar" or "xls" or "xlsx" or "ppt" or "pptx" or "pdf"){
				
														$this->Session->setFlash('กรุณาแก้ไขชื่อไฟล์ก่อนอัปโหลด โดยไม่ให้มีอักขระ จุด .');
														$this->Session->write('alertType','danger');
													
														$this->redirect(array('controller' => 'SpecialAndCooperatives','action' => 'add_proposals'));
													}
												}
				
				
												$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
												$this->Session->write('alertType','danger');
												
											}
										}
									}
										$numcount = $this->SpecialAndCooperativeDocument->find('count',array(
													'conditions' => array(
														'special_and_cooperative_id' => $proposal_id,
													),
												));
									if($files[0]['name'] != null){
										
										$i = 0;
										foreach ($files as $file) {
											if ($numcount == null) {
												$numcount = 1;
											}else {							
												$numcount = $numcount++;
											}
											$fileType = pathinfo($file['name'],PATHINFO_EXTENSION);
									
											
											
											$this->request->data['SpecialAndCooperativeDocument']['name'] = $numcount."_".$file['name'];
											$this->request->data['SpecialAndCooperativeDocument']['name_old'] = $file['name'];	
											$this->request->data['SpecialAndCooperativeDocument']['postdate'] = date('Y-m-d H:i:00');		
											$this->request->data['SpecialAndCooperativeDocument']['special_and_cooperative_id'] = $proposal_id;
											$this->request->data['SpecialAndCooperativeDocument']['log_special_and_cooperative_id'] = $checkLastLogSpecialAndCooperative['LogSpecialAndCooperative']['id'];
											$this->request->data['SpecialAndCooperativeDocument']['mis_employee_id'] = $UserName['MisEmployee']['id'];
					
												$filethai = iconv("UTF-8", "TIS-620",  $numcount."_".$file['name']);
												
											
												if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
													$this->request->data['SpecialAndCooperativeDocument']['class'] = "file";
												}else{
													$this->request->data['SpecialAndCooperativeDocument']['class'] = "img";
												}
												
												$folder = WWW_ROOT.'files'.DS.'students'.DS.'specialandcooperatives'.DS.$proposal_id.DS;
							
												if(!file_exists($folder))
													mkdir($folder);
													
												move_uploaded_file($file['tmp_name'],$folder.$filethai);
												// $files[0]['tmp_name']
												
											
											$this->SpecialAndCooperativeDocument->create();					
											$this->SpecialAndCooperativeDocument->save($this->request->data);	
											
										}
				
									} 

							}

							
						    //###########################################################################################
					}
					
					$this->Session->setFlash('<i class="glyphicon glyphicon-ok-sign"></i> เพิ่มข้อมูล เรียบร้อยแล้ว');
					$this->Session->write('alertType','success');	
					$this->redirect(array('action' => 'add_answer_abstract',$proposal_id,$mis_employee_id));	
				
					
			}
	
		}catch(Exception $e){
			$this->Session->setFlash('<i class="glyphicon glyphicon-ok-sign"></i> ปรับสถานะ เรียบร้อยแล้ว!!');
			$this->Session->write('alertType','success');	
			 
		}
		
		//================================================	
		$proposal = $this->SpecialAndCooperative->find('first',array(
			'conditions' => array(				
				'SpecialAndCooperative.id' => $proposal_id,
				'SpecialAndCooperative.mis_employee_id' => $mis_employee_id
			),
			'order' => array('SpecialAndCooperative.modified' => 'DESC')
		));
		 
		$employees = $this->MisEmployee->find('first',array(
			'conditions' => array(
				'MisEmployee.id' => $mis_employee_id
			),
		
		));
		$SpecialAndCooperativeEmployees = $this->SpecialAndCooperativeEmployee->find('all',array(
			'conditions' => array(				
				'SpecialAndCooperativeEmployee.special_and_cooperative_id' => $proposal_id,
			 
			),
			'order' => array('SpecialAndCooperativeEmployee.order' => 'ASC')
		));
		// debug($SpecialAndCooperativeEmployees);
		$outputSpecialAndCooperativeEmployee = '';
		$j= 0;
		foreach ($SpecialAndCooperativeEmployees as $SpecialAndCooperativeEmployee) {
			$j++;
			$employeeSpecialAndCooperativeEmployees = $this->MisEmployee->find('all',array(
				'conditions' => array(
					'MisEmployee.id' => $SpecialAndCooperativeEmployee['SpecialAndCooperativeEmployee']['mis_employee_id'],
				),
			
			));
			foreach ($employeeSpecialAndCooperativeEmployees as $employee) {
				$sSubPre = '';
				if ($employee['MisSubPrename']['id'] != null) {
					$sSubPre = $sSubPre . $employee['MisSubPrename']['name_short_th'] . ' ';
				}
				if ($sSubPre == '') {
					$sSubPre = $sSubPre . $employee['MisPrename']['name_full_th'] . ' ';
				}
				$outputSpecialAndCooperativeEmployee .= '<tr>
														<td>'.$j.'.</td>
														<td>'.$sSubPre.''.$employee['MisEmployee']['fname_th'].' '.$employee['MisEmployee']['lname_th'].' 
														
														</td>
														
													</tr>';

			}
			# code...
		}
		
		//######################### SpecialAndCooperativePassDocument ############################
		$output = '';  
			
		$proposaldocumentdates = $this->SpecialAndCooperativePassDocument->find('all',array(
			'conditions' => array(
				'SpecialAndCooperativePassDocument.special_and_cooperative_id' => $proposal_id,				
			 
				),
				// 'limit' => 1,
				'group' => array('SpecialAndCooperativePassDocument.postdate'),
				'order' => array('SpecialAndCooperativePassDocument.postdate' => 'ASC')
			));
		
		foreach ($proposaldocumentdates as $proposaldocumentdate) {
			$num=0;
			$proposaldocuments = $this->SpecialAndCooperativePassDocument->find('all',array(
				'conditions' => array(				
					'SpecialAndCooperativePassDocument.special_and_cooperative_id' => $proposal_id,		
					'SpecialAndCooperativePassDocument.postdate' => $proposaldocumentdate['SpecialAndCooperativePassDocument']['postdate'] ,
					),
					'order' => array('SpecialAndCooperativePassDocument.postdate' => 'ASC')
				));
				if($num == 0){								
					$output .=
					'<tr>
						<td colspan="2"> ผลบังคับใช้ :
							'. $proposal['Yearterm']['name'] .'
						</td>
						
					</tr>';
					
					$num++;
				}else {
					$output .= 
					'';
				}
				$i=0;
				foreach ($proposaldocuments as $proposaldocument) {
					$i++;
					$output .=
					'<tr>
						<td data-title="ลำดับ" align="center"> ลำดับที่ '.$i.'</td></td>
						<td>
							<a href="'.$this->urls.'/files/students/specialandcooperatives/'.$proposal_id.DS.$proposaldocument['SpecialAndCooperativePassDocument']['name'].'" class="btn btn-primary" target="_blank">
							'.$proposaldocument['SpecialAndCooperativePassDocument']['name'].'<span class="glyphicon glyphicon-cloud-download"></span></a>
						</td>
				  </tr>';
				}
				
		}
		 
		$answers = $this->AnswerSpecialAndCooperative->find('all',array(
			'conditions' => array(			
				'AnswerSpecialAndCooperative.special_and_cooperative_id' => $proposal_id,
				 
				),
				'order' => array('AnswerSpecialAndCooperative.modified' => 'ASC')
			));
		//debug($answers);
			if ($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 9) {
				
				$passes = $this->SpecialAndCooperativePass->find('list',array(
					'conditions' => array(
							
						'OR' => array(
							array('SpecialAndCooperativePass.level ' => 3),
							array('SpecialAndCooperativePass.level ' => 1)
							),
						),
						
				));
			}else{
				$passes = $this->SpecialAndCooperativePass->find('list',array(
					'conditions' => array(			
						'and' => array(
							array('SpecialAndCooperativePass.level <= ' => 2),
							array('SpecialAndCooperativePass.level >= ' => 1)
							),
						),
						
					));
			}

			$SpecialAndCooperativeFollowpasses = $this->SpecialAndCooperativeFollowStatus->find('list',array(
				'conditions' => array(			
					'SpecialAndCooperativeFollowStatus.level' => 1,
					),
					
				));	 
			
			 
		$this->set(array(
		 
			'employees' => $employees,
			'proposal' => $proposal,
			'answers' => $answers,
			'SpecialAndCooperativeFollowpasses' => $SpecialAndCooperativeFollowpasses,
			'proposal_id' => $proposal_id,
			'mis_employee_id' => $mis_employee_id,
			'passes' => $passes,
			'output' => $output,			
			'SpecialAndCooperativeEmployees' => $SpecialAndCooperativeEmployees,
			'outputSpecialAndCooperativeEmployee' => $outputSpecialAndCooperativeEmployee
			));
	}
	public function edit_answer_abstract($id =null,$idpage =null,$proposal_id=null,$mis_employee_id=null){
		$UserName = $this->Session->read('MisEmployee');		
		$admins = $this->Session->read('adminCurriculumRequests');
		
		if(isset($UserName)){
			$this->set('UserName', $UserName);
			$this->set('admins', $admins);
			

			if($this->action == 'login')
				$this->redirect(array('action' => 'Dashboard'));
		}
		else{
			if($this->action != 'login')
				$this->redirect('https://mis.agri.cmu.ac.th/mis/User/dashboard');
			
		}
		$this->set(array(
			'admins' => $admins,
			'UserName' => $UserName,
			
		));
		try{
			
			if ($this->request->data) {
				
					date_default_timezone_set('Asia/Bangkok');
					
					if ($this->request->data['AnswerSpecialAndCooperative']['Detail'] == null) {
						# code...
					}else {						
						// $this->request->data['AnswerSpecialAndCooperative']['PostName'] = $complaint_id['Complaint']['name'];
						$this->request->data['AnswerSpecialAndCooperative']['status'] = 1;
						$this->request->data['AnswerSpecialAndCooperative']['special_and_cooperative_id'] = $proposal_id;
					 
						$this->request->data['AnswerSpecialAndCooperative']['employee_status_id'] = $admins['Admincurriculumrequest']['employee_status_id'];
						$this->request->data['AnswerSpecialAndCooperative']['mis_employee_id'] = $admins['Admincurriculumrequest']['mis_employee_id'];
					 
					
	
						$this->AnswerSpecialAndCooperative->save($this->request->data['AnswerSpecialAndCooperative']);					
						if ($this->request->data['AnswerSpecialAndCooperativeDocument']['files'][0]['name'] == null) {
							
						}else {
							if(count($this->request->data['AnswerSpecialAndCooperativeDocument']['files']) >= 1){
								$images = $this->request->data['AnswerSpecialAndCooperativeDocument']['files'];
								//debug($this->request->data['FileProperty']['files']);
								$folderToSaveFiles = WWW_ROOT . 'files/students/specialandcooperatives/'.$mis_employee_id.'/';
								//debug($folderToSaveFiles);
								date_default_timezone_set('Asia/Bangkok');
								$date = date("Ymd");
								$time = date("His");
								$i = 1;
									foreach ($images as $image){
										//debug($image);

										$ext = pathinfo($image['name'],PATHINFO_EXTENSION);

												$lastAnswerSpecialAndCooperativeId = $this->AnswerSpecialAndCooperative->find('first', array(
												'order' => array('AnswerSpecialAndCooperative.id DESC')			
												));

												$lastFileId = $this->AnswerSpecialAndCooperativeDocument->find('first', array(
												'order' => array('AnswerSpecialAndCooperativeDocument.id DESC')
												));
												if ($lastFileId == null) {
													$last_id = 1;
												}else {
													$last_id = $lastFileId['AnswerSpecialAndCooperativeDocument']['id']+1;
												}

												$this->request->data['AnswerSpecialAndCooperativeDocument']['id'] = $last_id;

												$this->request->data['AnswerSpecialAndCooperativeDocument']['name_old'] = $image['name'];

												$result = move_uploaded_file( $image['tmp_name'], $folderToSaveFiles .$image['name']);

												$this->request->data['AnswerSpecialAndCooperativeDocument']['answer_special_and_cooperative_id'] = $lastAnswerSpecialAndCooperativeId['AnswerSpecialAndCooperative']['id'];

												$this->request->data['AnswerSpecialAndCooperativeDocument']['name'] = $lastAnswerSpecialAndCooperativeId['AnswerSpecialAndCooperative']['id'].'_'.$i.'.'.$ext;

												$this->AnswerSpecialAndCooperativeDocument->save($this->data['AnswerSpecialAndCooperativeDocument']);

												$i++;

									} // foreach images
	
	
							}
						}
					}

					
						$this->Session->setFlash('<i class="glyphicon glyphicon-ok-sign"></i> แก้ไขข้อมูล เรียบร้อยแล้ว');
						$this->Session->write('alertType','success');	
					
						$this->redirect(array('action' => 'add_answer_abstract',$proposal_id,$mis_employee_id));	
						
				}
	
		}catch(Exception $e){
			$this->Session->setFlash('<i class="glyphicon glyphicon-remove-sign"></i> ขออภัย เกิดเหตุขัดข้องของการส่งอีเมล์ไปยังบุคลากร กรุณาเช็คการใส่อีเมล์ของบุคลากรให้ถูกต้อง');
			$this->Session->write('alertType','danger');	
			$this->redirect(array('action' => 'add_answer_abstract',$proposal_id,$mis_employee_id,$check));	
			// $this->redirect(array('action' => 'list_result_manage_proposal'));	
			//echo 'window.location.href= https://stackoverflow.com/search?='+ $e->message();
		}
		
		//================================================	
		$replySpecialAndCooperatives = $this->SpecialAndCooperative->find('first',array(
			'conditions' => array(				
				'SpecialAndCooperative.id' => $proposal_id,
				'SpecialAndCooperative.mis_employee_id' => $mis_employee_id
			),
			'order' => array('SpecialAndCooperative.modified' => 'DESC')
		));
		
	 
	 
	  
		$answers = $this->AnswerSpecialAndCooperative->find('first',array(
			'conditions' => array(			
				'AnswerSpecialAndCooperative.id' => $id,			
				),
				'order' => array('AnswerSpecialAndCooperative.modified' => 'ASC')
			));
			
		$this->request->data = $answers;
		
		$this->set(array(
			'replySpecialAndCooperatives' => $replySpecialAndCooperatives,
			'answers' => $answers,
			'proposal_id' => $proposal_id,
			'mis_employee_id' => $mis_employee_id,			
			 
		 
		 
			));
	}
	public function delete_comment($id =null,$idpage =null,$proposal_id=null,$mis_employee_id=null){
		 
			$answerSpecialAndCooperativeDocuments = $this->AnswerSpecialAndCooperativeDocument->find('all',array(
				'conditions' => array(			
					'AnswerSpecialAndCooperativeDocument.answer_special_and_cooperative_id' => $id,			
					),
					'order' => array('AnswerSpecialAndCooperativeDocument.id' => 'ASC')
				));
				if($answerSpecialAndCooperativeDocuments != null){
					foreach ($answerSpecialAndCooperativeDocuments as $key) {
						$this->AnswerSpecialAndCooperativeDocument->delete($key['AnswerSpecialAndCooperativeDocument']['id']); 
					}

				}
			$this->AnswerSpecialAndCooperative->delete($id); 
								
				$this->Session->write('alertType','danger');
				$this->Session->setFlash('<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>ลบข้อมูล เรียบร้อยแล้ว');
				if ($idpage == 1) {					
					$this->redirect(array('action' => 'add_answer_abstract',$proposal_id,$mis_employee_id));	
				}elseif ($idpage == 2) {
					$this->redirect(array('action' => 'proposal_detail',$proposal_id,$mis_employee_id));	
				}
			 
	 
		
	}
	public function alert_comment($id =null,$idpage =null,$proposal_id=null,$student_id=null){
		//============================-สำหรับคณะแจ้งเตือน-===================================
		try{
			// 1 รอ อ.ที่ปรึกษาพิจารณา
			// 2 อยู่ในระหว่างการพิจารณากับ อ.ที่ปรึกษา
			// 3 รอการแก้ไขจากนักศึกษาจากมติที่ประชุมคณะ
			// 4 อยู่ระหว่างการพิจารณาจาก อ.ที่ปรึกษา
				$status = 1;
				$data = array(
					'id' => $proposal_id,
					'proposal_follow_status_id' => 3,
					'pass_id' => 1,
					'passed_fac_committee' => 1
				);
				$this->Proposal->save($data);
        //======================= โชว์คอมเม้น	====================================
			
				$data2 = array(
					'id' => $id,				
					'show' => 1,
				);
				$this->AnswerProposal->save($data2);
			//======================= ส่งหานักศึกษา	====================================				
			$studentEmails = $this->Proposal->findById($proposal_id);						
			$check_Emails = $this->Student->find('first' ,array(
				'conditions' => array(
					'Student.id' => $studentEmails['Proposal']['student_id'],			
					),
				'order' => array('Student.id' => 'DESC'),
				
			));			
			$email = new CakeEmail('gmail');
		
			$email->template('reply_proposal_confirmation')				
					->emailFormat('html')
					->viewVars(array('studentEmails' => $studentEmails))
					->helpers('Html')
					->from('agriculturecmu@gmail.com')
					->to(array($check_Emails['Student']['student_email']))
					->subject('Reply Proposal from Faculty Committee Notification  :: CODE '.$studentEmails['Proposal']['student_id'].'')
					->send();
			//======================= ส่งหาภาควิชา	====================================	
			$listProposals = $this->Proposal->find('first',array(
				'conditions' => array(
					'Proposal.student_id' => $studentEmails['Proposal']['student_id'] ,							
					),
					'order' => array('Proposal.id' => 'DESC')
				));	
			$AdmincurriculumrequestDivisions = $this->Admincurriculumrequest->find('all', array(
				'conditions' => array(
					'employee_status_id' => 2,
					'Admincurriculumrequest.organize_id' => $listProposals['Proposal']['organize_id']	
				)
			));	
			// $tests1 = array();
			// foreach($AdmincurriculumrequestDivisions as $userDivision)
			// {
			// 	$tests1[] = $userDivision['MisEmployee']['email'];
			// }
			
			// $email = new CakeEmail('gmail');
			// $email->to($tests1) 
			// 			->template('reply_proposal_for_employee_confirmation')
			// 			->emailFormat('html')
			// 			->viewVars(array('studentEmails' => $studentEmails))
			// 			->helpers('Html')
			// 			->from('agriculturecmu@gmail.com')				
			// 			->subject('Reply Proposal from Faculty Committee Notification  :: CODE '.$studentEmails['Proposal']['student_id'].'')
			// 			->send();
			//======================= ส่งหาอาจารย์ที่ปรึกษา	====================================				
			$committeestudentEmails = $this->Committeestudent->find('all',array(	
				'conditions' => array(
					'Committeestudent.student_id' => $studentEmails['Proposal']['student_id'],										
					'Committeestudent.committeetype_id' => 2	
				)
			));	
		    //debug($committeestudentEmails);
			foreach ($committeestudentEmails as $committeestudentEmail) {
				$advisors = $this->Advisor->find('all', array(
					'conditions' => array(
						'Advisor.id' => $committeestudentEmail['Committeestudent']['advisor_id'],
					),
					'fields' => array('Advisor.id','advisor_code','Advisor.mis_employee_id','advisor_title','advisor_name','advisor_sname'),
					'order' => array('Advisor.id' => 'ASC')
				));
				foreach ($advisors as $advisor) {							
					$employeeEMails = $this->MisEmployee->find('all', array(
						'conditions' => array(
							'MisEmployee.id' => $advisor['Advisor']['mis_employee_id'],									
						),
						'fields' => array('MisEmployee.id','MisEmployee.email_cmu','MisEmployee.email_other'),
					));
					$tests = array();
					$tests2 = array();
					foreach($employeeEMails as $user)
					{
						// $tests[] = $user['Employee']['email'];
						$tests[] = $user['MisEmployee']['email_cmu'];
						$tests2[] = $user['MisEmployee']['email_other'];
					}
			
					$email = new CakeEmail('gmail');
					$email->to($tests) 										
								->template('reply_proposal_for_employee_confirmation')
								->emailFormat('html')
								->viewVars(array('studentEmails' => $studentEmails))
								->helpers('Html')
								->from('agriculturecmu@gmail.com')																							
								->subject('Reply Proposal from Faculty Committee Notification  :: CODE '.$studentEmails['Proposal']['student_id'].'')
								->send();

					$email = new CakeEmail('gmail');
					$email->to($tests2) 										
								->template('reply_proposal_for_employee_confirmation')
								->emailFormat('html')
								->viewVars(array('studentEmails' => $studentEmails))
								->helpers('Html')
								->from('agriculturecmu@gmail.com')																							
								->subject('Reply Proposal from Faculty Committee Notification  :: CODE '.$studentEmails['Proposal']['student_id'].'')
								->send();
				}
			}
		
			if ($idpage == 1) {					
				$this->redirect(array('action' => 'add_answer_abstract',$proposal_id,$student_id));	
			}elseif ($idpage == 2) {
				$this->redirect(array('action' => 'proposal_detail',$proposal_id,$student_id));	
			}
		}catch(Exception $e){
			$this->Session->setFlash('<i class="glyphicon glyphicon-remove-sign"></i> ขออภัย เกิดเหตุขัดข้องของการส่งอีเมล์ไปยังนักศึกษาหรือบุคลากร กรุณาเช็คการใส่การสะกดอีเมล์ให้ถูกต้อง');
			$this->Session->write('alertType','danger');		
			$this->redirect(array('action' => 'add_answer_abstract',$proposal_id,$student_id));				
		}
		
	}
	public function attach_document($proposal_id=null,$mis_employee_id=null){
		$UserName = $this->Session->read('MisEmployee');		
		$admins = $this->Session->read('adminCurriculumRequests');
		
		if(isset($UserName)){
			$this->set('UserName', $UserName);
			$this->set('admins', $admins);
			

			if($this->action == 'login')
				$this->redirect(array('action' => 'Dashboard'));
		}
		else{
			if($this->action != 'login')
				$this->redirect('https://mis.agri.cmu.ac.th/mis/User/dashboard');
			
		}
		$this->set(array(
			'admins' => $admins,
			'UserName' => $UserName,
			
		));		
			if ($this->request->data) {
				date_default_timezone_set('Asia/Bangkok');
						 


				$newformat = date('Y-m-d', strtotime($this->request->data['SpecialAndCooperative']['pass_special_and_cooperative_date']));
				$this->request->data['SpecialAndCooperative']['pass_special_and_cooperative_date'] = $newformat;
				$this->Proposal->save($this->request->data['SpecialAndCooperative']);

				/////////////// Save SpecialAndCooperativePassDocument /////////////////
				$files = $this->request->data['SpecialAndCooperativePassDocument']['files'];	
				if ($this->request->data['SpecialAndCooperativePassDocument']['files'][0]['type'] != null) {
					foreach ($this->request->data['SpecialAndCooperativePassDocument']['files'] as $check) {
						if($check['type'] != ("image/jpg" or "image/jpeg" or "image/pjpeg" or "image/png" or "image/gif" or "application/msword" or "application/vnd.openxmlformats-officedocument.wordprocessingml.document" or "application/x-rar-compressed" or "application/octet-stream" or "application/zip" or "application/octet-stream" or "application/vnd.ms-powerpoint" or "application/vnd.openxmlformats-officedocument.presentationml.presentation" or "application/vnd.ms-excel" or "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")){


							foreach ($files as $file) {

								$fileType = pathinfo($file['name'],PATHINFO_EXTENSION);
								if($fileType != "doc" or "docx" or "jpg" or "jpeg" or "png" or "gif" or "zip" or "rar" or "xls" or "xlsx" or "ppt" or "pptx" or "pdf"){

									$this->Session->setFlash('กรุณาแก้ไขชื่อไฟล์ก่อนอัปโหลด โดยไม่ให้มีอักขระ จุด .');
									$this->Session->write('alertType','danger');
								
									$this->redirect(array('controller' => 'SpecialAndCooperatives','action' => 'add_proposals'));
								}
							}


							$this->Session->setFlash('ไฟล์อัฟโหลดไม่ถูกต้อง');
							$this->Session->write('alertType','danger');
							
						}
					}
				}
					$numcount = $this->SpecialAndCooperativePassDocument->find('count',array(
								'conditions' => array(
									'special_and_cooperative_id' => $proposal_id,
								),
							));
				if($files[0]['name'] != null){
					  
					$i = 0;
					foreach ($files as $file) {
						if ($numcount == null) {
							$numcount = 1;
						}else {							
							$numcount = $numcount++;
						}
						$fileType = pathinfo($file['name'],PATHINFO_EXTENSION);
				 
						 
						
						$this->request->data['SpecialAndCooperativePassDocument']['name'] = $numcount."_".$file['name'];
						$this->request->data['SpecialAndCooperativePassDocument']['name_old'] = $file['name'];	
						$this->request->data['SpecialAndCooperativePassDocument']['postdate'] = date('Y-m-d H:i:00');		
						$this->request->data['SpecialAndCooperativePassDocument']['special_and_cooperative_id'] = $proposal_id;
					 
						$this->request->data['SpecialAndCooperativePassDocument']['mis_employee_id'] = $UserName['MisEmployee']['id'];

							$filethai = iconv("UTF-8", "TIS-620",  $numcount."_".$file['name']);
							
						
							if ($fileType != 'jpg' and $fileType != 'JPG' and $fileType != 'PNG' and $fileType != 'png' and $fileType != 'gif' and $fileType != 'GIF' and $fileType != 'JPEG' and $fileType != 'jpeg' and $fileType != 'BPG' and $fileType != 'BAT' and $fileType != 'HEIF' and $fileType != 'HDR' and $fileType != 'WebP' and $fileType != 'TIFF') {
								$this->request->data['SpecialAndCooperativePassDocument']['class'] = "file";
							}else{
								$this->request->data['SpecialAndCooperativePassDocument']['class'] = "img";
							}
							
							$folder = WWW_ROOT.'files'.DS.'students'.DS.'specialandcooperatives'.DS.$proposal_id.DS;
		
							if(!file_exists($folder))
								mkdir($folder);
								
							move_uploaded_file($file['tmp_name'],$folder.$filethai);
						 
							
						
						$this->SpecialAndCooperativePassDocument->create();					
						$this->SpecialAndCooperativePassDocument->save($this->request->data);	
						
					}

				} 
				// if($this->request->data['SpecialAndCooperative']['files']['name']){
					// 	$files = $this->request->data['SpecialAndCooperative']['files'];										
					// 	$i = date("YmDHis");						
									
					// 		$fileType = pathinfo($files['name'],PATHINFO_EXTENSION);
					// 		$this->request->data['SpecialAndCooperative']['file'] = $this->request->data['SpecialAndCooperative']['files']['name'];
					// 		$filethai = iconv("UTF-8", "TIS-620",  $this->request->data['SpecialAndCooperative']['files']['name']);

															
					// 		$folder = WWW_ROOT.'files'.DS.'students'.DS.'specialandcooperatives'.DS.$proposal_id.DS;

					// 		if(!file_exists($folder))
					// 			mkdir($folder);

					// 		move_uploaded_file($this->request->data['SpecialAndCooperative']['files']['tmp_name'],$folder.DS.$filethai);

					// 		$this->SpecialAndCooperative->save($this->request->data['SpecialAndCooperative']);
							
						 
				// }
							 

				$this->Session->setFlash('<i class="glyphicon glyphicon-ok-sign"></i> แนบไฟล์ มคอ. เรียบร้อยแล้ว');
				$this->Session->write('alertType','success');	
				$this->redirect(array('action' => 'attach_document',$proposal_id,$mis_employee_id));
			}

		//================================================	
	 
		$SpecialAndCooperatives  = $this->SpecialAndCooperative->findById($proposal_id);
		$this->request->data = $SpecialAndCooperatives;

		$time = strtotime($SpecialAndCooperatives['SpecialAndCooperative']['pass_special_and_cooperative_date']);
		$newformat = date('d-M-Y', $time);	
		$this->request->data['SpecialAndCooperative']['pass_special_and_cooperative_date'] = $newformat;

		//######################### SpecialAndCooperativePassDocument ############################
		 	$output ='';
				 
					
			$proposaldocumentdates = $this->SpecialAndCooperativePassDocument->find('all',array(
				'conditions' => array(
					'SpecialAndCooperativePassDocument.special_and_cooperative_id' => $proposal_id,				
				
					),
					// 'limit' => 1,
					'group' => array('SpecialAndCooperativePassDocument.postdate'),
					'order' => array('SpecialAndCooperativePassDocument.postdate' => 'ASC')
				));
		 
			foreach ($proposaldocumentdates as $proposaldocumentdate) {
				$num=0;
				$proposaldocuments = $this->SpecialAndCooperativePassDocument->find('all',array(
					'conditions' => array(				
						'SpecialAndCooperativePassDocument.special_and_cooperative_id' => $proposal_id,		
						'SpecialAndCooperativePassDocument.postdate' => $proposaldocumentdate['SpecialAndCooperativePassDocument']['postdate'] ,
						),
						'order' => array('SpecialAndCooperativePassDocument.postdate' => 'ASC')
					));
					if($num == 0){								
						$output .=
						'<tr>
							<td colspan="2"> วันที่อัพโหลดไฟล์ มคอ. ฉบับสมบูรณ์ :
								'. $proposaldocumentdate['SpecialAndCooperativePassDocument']['postdate'] .'
							</td>
							
						</tr>';
						
						$num++;
					}else {
						$output .= 
						'';
					}
					$i=0;
					foreach ($proposaldocuments as $proposaldocument) {
						$i++;
						$output .=
						'<tr>
							<td data-title="ลำดับ" align="center"> ลำดับที่ '.$i.'</td></td>
							<td>
								<a href="'.$this->urls.'/files/students/specialandcooperatives/'.$proposal_id.DS.$proposaldocument['SpecialAndCooperativePassDocument']['name'].'" class="btn btn-primary" target="_blank">
								'.$proposaldocument['SpecialAndCooperativePassDocument']['name'].'<span class="glyphicon glyphicon-cloud-download"></span></a>
							</td>
					</tr>';
					}
					
			}	
		$this->set(array(
			'SpecialAndCooperatives' => $SpecialAndCooperatives,			 
			'proposal_id' => $proposal_id,
			'output' => $output
			));
	}
	//******************************** */
	 
} 
							