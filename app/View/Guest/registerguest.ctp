<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dependencies/JQL.min.js"></script>
<script type="text/javascript" src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>

<link rel="stylesheet" href="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dist/jquery.Thailand.min.css">
<script type="text/javascript" src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>

<div class="container" style="padding:15px 0 0; width:65%;">
	<div class="panel-heading">
		<div class="panel-title">
			<!-- <h3 style="margin:0px !important;">
				<center>ลงทะเบียน Open House ระหว่างวันที่ 2-4 พฤศจิกายน 2567</center>
			</h3> -->
			<h2>
				<!-- <center>ระบบลงทะเบียนสำหรับหน่วยงานภายนอก</center> -->
				<center>ระบบลงทะเบียนเข้าร่วมงานพิธีเปิดงานเกษตรแห่งชาติ 2567</center>
			</h2>
			<h2>
				<center>วันที่ 27 พฤศจิกายน 2567</center>
			</h2>

		</div>
	</div>

	<!-- <div align="center" style="padding-top: 15px; ">
		<h3>
			<div class="font_panel2"> ลงทะเบียน Open House ระหว่างวันที่ 2-4 พฤศจิกายน 2567</div>
		</h3>
	</div> -->
	<br />
	<form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">
		<?php
		//debug($this->request->data);
		$required = 'กรุณากรอกข้อมูล';
		echo $this->Form->create('Guest', array('role' => 'form', 'data-toggle' => 'validator'));
		?>

		<div class="form-group col-md-12" style="margin-bottom:20px;">
			<label>หน่วยงาน<span style="color: red;">*</span></label>
			<?php echo $this->Form->input('organize', array(
				'type' => 'text',
				'label' => false,
				'class' => 'form-control',
				'data-error' => $required,
				'required' => true
			)); ?>
		</div>

		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th style="text-align: center;">ลำดับ</th>
					<th style="text-align: center;">คำนำหน้า</th>
					<th style="text-align: center;">ชื่อ </th>
					<th style="text-align: center;">สกุล</th>
					<th style="text-align: center;">ตำแหน่ง</th>
					<th style="text-align: center;">โทรศัพท์</th>
					<th style="text-align: center;">หมายเหตุ</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$i = 1;
				while ($i <= 10) {
				?>
					<tr>

						<td data-title="ลำดับ" align="center"> <?php echo $i; ?> </td>
						<td data-title="คำนำหน้า">
							<div class="form-group has-feedback">
								<?php
								echo $this->Form->input('Guest.prefix' . $i, array(
									'type' => 'text',
									'label' => false,
									'class' => 'form-control',
									'data-error' => $required,
									'required' => false
								)); ?>
								<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
							</div>
						</td>
						<td data-title="ชื่อ">
							<div class="form-group has-feedback">
								<?php
								echo $this->Form->input('Guest.fname' . $i, array(
									'type' => 'text',
									'label' => false,
									'class' => 'form-control',
									'data-error' => $required,
									'required' => false
								)); ?>
								<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
							</div>
						</td>
						<td data-title="นามสกุล">
							<div class="form-group has-feedback">
								<?php
								echo $this->Form->input('Guest.lname' . $i, array(
									'type' => 'text',
									'label' => false,
									'class' => 'form-control',
									'data-error' => $required,
									'required' => false
								)); ?>
								<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
							</div>
						</td>
						<td data-title="ตำแหน่ง">
							<div class="form-group has-feedback">
								<?php
								echo $this->Form->input('Guest.position' . $i, array(
									'type' => 'text',
									'label' => false,
									'class' => 'form-control',
									'data-error' => $required,
									'required' => false
								)); ?>
								<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
							</div>
						</td>
						<td data-title="โทรศัพท์">
							<div class="form-group has-feedback">
								<?php
								echo $this->Form->input('Guest.tel' . $i, array(
									'type' => 'text',
									'label' => false,
									'class' => 'form-control',
									'data-error' => $required,
									'required' => false
								)); ?>
								<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
							</div>
						</td>
						<td data-title="หมายเหตุ">
							<div class="form-group has-feedback">
								<?php
								echo $this->Form->input('Guest.remark' . $i, array(
									'type' => 'text',
									'label' => false,
									'class' => 'form-control',
									'data-error' => $required,
									'required' => false
								)); ?>
								<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
							</div>
						</td>
					</tr>
				<?php
					$i++;
				}

				?>
			</tbody>
		</table>
		<div class="row" style="margin:30px 0px 30px;">
			<div class="col-md-12">
				<center><input type="submit" value="คลิกส่งข้อมูลการลงทะเบียน" class="btn btn-success btn-lg btn-block"></center>
			</div>
		</div>
	</form>
</div>