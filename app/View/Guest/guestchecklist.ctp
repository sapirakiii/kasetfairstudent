<?php
date_default_timezone_set('Asia/Bangkok');
$date = date("Y-m-d");
$time = date("H:i:s");


function DateDiff($strDate1, $strDate2)
{
	return (strtotime($strDate2) - strtotime($strDate1)) /  (60 * 60 * 24);  // 1 day = 60*60*24
}

function TimeDiff($strTime1, $strTime2)
{
	return (strtotime($strTime2) - strtotime($strTime1)) /  (60 * 60); // 1 Hour = 60*60
}

?>

<div class="container-fluid" style="padding:15px 0 0;">
	<div class="panel panel-default" style="border-color: #F2F2F2!important">
		<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<div class="panel-title">
				<h3 style="margin:5px !important;">
					<center>
						<div class="font_panel2">รายชื่อผู้ลงทะเบียนเข้าร่วมงานพิธีเปิดงานเกษตรแห่งชาติ 2567</div>
					</center>
				</h3>
				<h3 style="margin:5px !important;">
					<center>
						<div class="font_panel2">วันที่ 27 พฤศจิกายน 2567 2567</div>
					</center>
				</h3>
			</div>
		</div>
	</div>


	<div id="no-more-tables">
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th style="text-align: center;">ลำดับ</th>
					<th style="text-align: center;width:15%">ชื่อ - นามสกุล</th>
					<th style="text-align: center;width:20%">ตำแหน่ง</th>

					<?php
					// if (isset($citizenId) || isset($admin)) { 
					?>
					<th style="text-align: center;">เบอร์โทรศัพท์ติดต่อ</th>
					<?php
					// } 
					?>

					<th style="text-align: center;">หมายเหตุ</th>
					<th colspan="2" style="text-align: center;">เพิ่มหมายเหตุ</th>
					<th style="text-align: center;">สถานะ</th>
					<th style="text-align: center;">วัน/เวลา</th>
					<?php
					// if (isset($citizenId) || isset($admin)) { 
					?>
					<th style="text-align: center;">เปลี่ยนสถานะ</th>
					<?php
					// } 
					?>
				</tr>
			</thead>

			<tbody>

				<?php
				$i = 0;
				foreach ($organizes as $organize) {
				?>
					<tr>
						<td data-title="กลุ่มหน่วยงาน" colspan="7">
							<b><?php echo $organize['Guest']['organize']; ?></b>
						</td>
					</tr>
					<?php
					foreach ($guestlists as $guestlist) {
						if ($organize['Guest']['organize'] == $guestlist['Guest']['organize']) {
							$i++
					?>
							<tr>
								<td data-title="ลำดับ" align="center"><?php echo $i; ?></td>

								<td data-title="ชื่อ - นามสกุล">
									<?php
									echo $guestlist['Guest']['prefix'] . ' ' . $guestlist['Guest']['fname'] . ' ' . $guestlist['Guest']['lname'];
									?>
								</td>
								<td data-title="ตำแหน่ง">
									<?php
									echo $guestlist['Guest']['position'];
									?>
								</td>
								<?php
								// if (isset($citizenId) || isset($admin)) { 
								?>
								<td data-title="เบอร์โทรศัพท์ติดต่อ">
									<?php echo $guestlist['Guest']['tel']; ?>
								</td>
								<?php
								// } 
								?>
								<td data-title="หมายเหตุ">
									<?php
									echo $guestlist['Guest']['remark'];
									?>
								</td>
								<form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">
									<td data-title="สถานะ">
										<?php 
											echo $this->Form->input('Guest.id', array(
												'class' => 'form-control',
												'label' => false,
												'value' => $guestlist['Guest']['id'],
												'required'
											)); 
											echo $this->Form->input('Guest.remark',array(
												'type' => 'text',
												'div' => false,
												'label' => false,
												'class' => array('form-control css-require'),
												'placeholder' => 'หมายเหตุ',
												'required'
											));
										?>
										
										 
										
										 
									</td>
									<td data-title="สถานะ"> 
										
										<input type="submit" name="บันทึก" class="btn btn-success btn-block">
									</td>
								</form>
								<td data-title="สถานะ" align="center">
									<?php
									if ($guestlist['Guest']['status'] == 0) {
									?>
										<font color="red"> ยังไม่เข้าร่วม </font>
									<?php
									} else {
									?>
										<font color="green">
											เข้าร่วม
											 
										</font>

									<?php
									}
									?>
									

								</td>
								<td data-title="วัน/เวลา" align="center">
									<?php
									if ($guestlist['Guest']['status'] == 0) {
									?>
										 
									<?php
									} else {
									?>
										<font color="green">
											 
											<div style="font-size: small;">
												<?php
													echo $guestlist['Guest']['fname'] . " " . $guestlist['Guest']['lname'] . " (" . $guestlist['Guest']['tstamp'] . ")";
												?>
											</div>
										</font>

									<?php
									}
									?>
									

								</td>
								<!-- <form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8"> -->
								<td data-title="เปลี่ยนสถานะ" style="text-align: center;">
									<a href="<?php echo $this->Html->url(array('action' => 'guestchecklist', $guestlist['Guest']['id'])); ?>" class="btn btn-primary btn-sm">
										เปลี่ยนสถานะ
									</a>
									<!-- <input type="submit" value="เปลี่ยนสถานะ" class="btn btn-primary col-md-12"> -->


								</td>
								<!-- </form> -->
					<?php
						}
					}
				}
					?>

			</tbody>
		</table>
	</div>

</div>