<?php //echo debug($this->request->data)?>
<!-- start content here -->
<div class="container" style="padding:15px 0 0;">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-primary">
				<div class="panel-heading"> 
          รายการที่ขอเปิดกระบวนวิชาใหม่/ปรับปรุงกระบวนวิชา
				</div>
					<div class="panel-body">
						<a class="btn btn-default dropdown-toggle btn-primary" href="<?php echo $this->Html->url(array('action' => 'list_result_manage_proposal')); ?>">
							ย้อนกลับ
							<span class="glyphicon glyphicon-home"></span>			
						</a>			
						
					</div>
				</div>
			</div>
      </div>

    <div class="panel panel-default" style="border-color: #F2F2F2!important">
      <div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
        <div class="panel-title">
          <h3 style="margin:0px !important;">
          <center> 
              <div class="font_panel2">
               แก้ไขการตอบกลับการพิจารณาขอเปิดกระบวนวิชาใหม่/ปรับปรุงกระบวนวิชา
              </div>
            </center>
          </h3> 
        </div>
      </div>	
      <div class="panel-body" style="padding-top: 15px; ">         
          <tbody> 
            <div class="form-group has-feedback">      
                <?php echo $answers['AnswerSpecialAndCooperative']['Detail']; ?>
                
            </div>
            <div class="col-sm-12">
            
              <?php 

              if($answers['AnswerSpecialAndCooperativeDocument'] != array()){

                echo '<br><label><span class="glyphicon glyphicon-list-alt"></span>เอกสารแนบ</label>';

                foreach ($answers['AnswerSpecialAndCooperativeDocument'] as $key) {
                  $fileAction = $this->webroot.'files/students/specialandcooperatives/'.$mis_employee_id.'/'.$key['name_old'] ;
                  echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="'.$fileAction.'" class="btn btn-primary" target="_blank">'.$key['name_old'].' <span class="glyphicon glyphicon-download-alt"></span></a>';
                }
              }


              ?>
            </div> 
            <div class="col-sm-12">
                <div class="form-group has-feedback">
                          <blockquote>
                          <h4><i class="fa fa-user-circle-o" aria-hidden="true"></i> <b>ตอบกลับโดย :</b>
                          
                          <font class="txt-content_SpecialAndCooperative26"> <?php echo $answers['AnswerSpecialAndCooperative']['PostName']; ?></font></h4>  <small><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $answers['AnswerSpecialAndCooperative']['created']; ?></small>
                          </blockquote>    

                </div>
              </div>   
          </tbody>            
        </div>
      </div>           
                        
          <form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                  <div class="panel panel-success">
                      <div class="panel-heading"> 
                        <h3 class="panel-title"><i class="glyphicon glyphicon-plus"></i> แก้ไขตอบกลับการพิจารณา </h3> 
                      </div>
                      <div class="panel-body">
                        <tbody>   
                           
                          <?php  if ($admins['Admincurriculumrequest']['employee_status_id'] == 1) { ?>                                   
                            <!-- ********** -->
                            <?php 
                                  $required = 'กรุณากรอกข้อมูล';
                                      echo $this->Form->input('AnswerSpecialAndCooperative.id', array(                                                                 
                                        'label' => false,
                                        'div' => false,
                                        'class' => array('form-control css-require'),                                                           
                                        'error' => false,
                                        
                                        )); 
                                        echo $this->Form->hidden('AnswerSpecialAndCooperative.special_and_cooperative_id', array(
                                          'value' => $proposal_id,
                                          'class' => 'form-control',
                                        ));
                                        
                                        
                                  ?>
                                
                                <label class="col-sm-3 control-label">รายละเอียด <font color="red">*</font> </label>
                                  <div class="col-sm-7">
                                        <div class="form-group has-feedback">      
                                            <?php 
                                                  echo $this->Form->input('AnswerSpecialAndCooperative.Detail', array(
                                                      'type' => 'textarea',
                                                      'label' => false,
                                                      'div' => false,
                                                      'value' => $answers['AnswerSpecialAndCooperative']['Detail'],
                                                      'class' => 'ckeditor',                                                           
                                                      'error' => false,
                                                      'required'
                                                      ));
                                              
                                                ?>
                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                </div>
                                <label class="col-sm-3 control-label" ><br>อัพโหลดเอกสาร/รูปภาพแนบ</label>
                                <div class="col-sm-7">
                                  <div class="form-group has-feedback">   
                                  <?php
                                      echo $this->Form->input('AnswerSpecialAndCooperativeDocument.files.', array('type' => 'file', 'multiple'));
                                      ?>
                                  </div>
                                </div>
                              
                                
                                
                                  <div class="form-group">
                                          <label class="col-sm-3 control-label"></label>
                                          <div class="col-sm-7">
                                        
                                          <input type="submit" class="btn btn-success" value="บันทึกข้อมูล" onclick="return confirm('ยืนยันการเพิ่มข้อมูล')">
                                          
                                  </div>
                                </div> 
                            </div>           
                          <?php } ?>        
                            <!-- ********** -->
                        </tbody>  
                      </div> 
                  </div>
          </form>
        
        </div> 
            <!-- /container -->  
                                  
          

            
          </div>
        </div>
      </div>
    </div>
  </div>   
<div style="clear: both;"></div>