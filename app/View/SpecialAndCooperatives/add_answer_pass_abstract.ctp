<?php //echo debug($this->request->data)?>
<!-- onclick="return confirm('ยืนยันการเพิ่มข้อมูล')" -->
<style>
.numberCircle {
    border-radius: 50%;
    behavior: url(PIE.htc); /* remove if you don't care about IE8 */
    width: 36px;
    height: 36px;
    padding: 8px;
    background: #fff;
    border: 2px solid black;
    color: black;
    text-align: center;
    font: 20px Arial, sans-serif;
    display: inline-block;
}
</style>
 
<!-- start content here -->

<div  style="padding:15px 0 0;">
  <div class="row">
      
    <div class="col-md-12">
      <div class="panel panel-primary">
				<div class="panel-heading"> 
          รายการที่ขอเปิดกระบวนวิชาใหม่/ปรับปรุงกระบวนวิชา
				</div>
					<div class="panel-body">
            
            <div class="col-md-12 panel with-nav-tabs panel-default" style="border-color: #F2F2F2!important;padding-left: 0px;padding-right: 0px;">
                  
                            <div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
                        <ul class="nav nav-tabs">
                          <li class="active"><a href="#tab8primary" data-toggle="tab"><i class="fa fa-university" aria-hidden="true"></i> ข้อมูลหลักสูตร</a></li>
                          <!-- <li><a href="#tab9primary" data-toggle="tab"> ประวัติการพิจารณา</a></li> -->
                          <!-- <li><a href="#tab10primary" data-toggle="tab"> ประวัติการพิจารณา</a></li> -->
                    
                        </ul>
                          </div>
                      <div class="panel-body">
                        <div class="tab-content">
                          <div class="tab-pane fade in active" id="tab8primary">
                            <div class="col-md-6"> 
                                  <table class="table table-bordered" width=" 60%">
                                    
                                    
                                  </table>
                            
                                
                              <br>
                              <div class="panel panel-warning">
                                <div class="panel-heading"> 
                                <i class="glyphicon glyphicon-bookmark"></i>
                                <font class="txt-content_SpecialAndCooperative26">  รายละเอียดหลักสูตรกระบวนวิชา </font>
                                
                                </div>
                                  <div class="panel-body">
                                      
                                      <table class="table table-hovered">
                                          <tbody>
                                              <tr>
                                                  <td>รหัสกระบวนวิชา</td>
                                                  <td><?php echo $proposal['SpecialAndCooperative']['code']; ?></td>
                                              </tr> 
                                              <tr>
                                                  <td>ชื่อวิชาภาษาอังกฤษ</td>
                                                  <td><?php echo $proposal['SpecialAndCooperative']['title']; ?></td>
                                              </tr>  
                                              <tr>
                                                  <td>ชื่อวิชาภาษาอังกฤษ</td>
                                                  <td><?php echo $proposal['SpecialAndCooperative']['title_eng']; ?></td>
                                              </tr> 
                                              <tr>
                                                  <td><b>ภาควิชา</b> </td>
                                                  <td>  <?php echo $proposal['Organize']['name'];?></td>

                                              </tr>
                                              <?php if($proposal['SpecialAndCooperative']['major_id'] != null){ ?>
                                                <tr>
                                                    <td>สาขาวิชา</td>
                                                    <td><?php echo $proposal['Major']['major_name']; ?></td>
                                                </tr> 
                                              <?php }else{ ?>
                                                <tr>
                                                    <td>สาขาวิชา</td>
                                                    <td>ส่วนกลางภาควิชา</td>
                                                </tr>
                                              <?php } ?>
                                             
                                              <tr>
                                                  <td>ระดับ </td>
                                                  <td><?php echo $proposal['Degree']['degree_name']; ?></td>
                                              </tr>  
                                              <tr>
                                                  <td>ผลบังคับใช้ </td>
                                                  <td><font color="green"><?php echo $proposal['Yearterm']['name']; ?></font></td>
                                              </tr>   
                                              
                                               
                                             
                                   
                                                                  
                                          </tbody>
                                      </table>       
                                  
                                                                                
                                  </div>
                              </div>  
                              <div class="panel panel-primary">
                                <div class="panel-heading"> 
                                    <font class="txt-content_SpecialAndCooperative26"> อาจารย์ผู้รับผิดชอบกระบวนวิชาและอาจารย์ผู้สอน</font>

                                    </div>
                                    <div class="panel-body">
                                        <table class="table table-hovered">
                                              <tbody>
                                                   
                                                  <tr>
                                                      <td><b>อาจารย์ผู้รับผิดชอบ </b></td>
                                                      <td>
                                                        <?php 
            
                                                          $sSubPre = '';
                                                          if ($employees['MisSubPrename']['id'] != null) {
                                                            $sSubPre = $sSubPre . $employees['MisSubPrename']['name_short_th'] . ' ';
                                                          }
                                                          if ($sSubPre == '') {
                                                            $sSubPre = $sSubPre . $employees['MisPrename']['name_full_th'] . ' ';
                                                          } 
                                                        ?>  
                                                        <?php //echo $employees['Prefix']['fullname']," ",$employees['Employee']['fname']," ",$employees['Employee']['lname'];?> 
                                                        <b> <?php echo $sSubPre.''.$employees['MisEmployee']['fname_th'].' '.$employees['MisEmployee']['lname_th']; ?></b>
                                                      </td>
                                                   </tr>  
                                                  <tr>
                                                      <td><b>อาจารย์ผู้สอน </b></td>
                                                      <td>
                                                        <table class="table table-bordered">
                                                            <tr>
                                                              <td>ลำดับ</td>
                                                              <td>รายนาม</td>
                                                              
                                                            </tr>
                                                          <?php echo  $outputSpecialAndCooperativeEmployee;  ?> 
                                                        </table>
                                                      </td>

                                                  </tr>                 
                                              </tbody>
                                          </table> 

                                    </div> 
                                </div>
                              </div>
                              
                              
                              <!-- ************** -->
                        
                            </div>
                            <div class="col-md-6"> 
                                    <form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                      <div class="panel panel-success">
                                        <div class="panel-heading"> 
                                          <div class="panel-title">
                                              &emsp;<i class="glyphicon glyphicon-book"></i>
                                              <font class="txt-content_SpecialAndCooperative26"> ไฟล์ มคอ.3-4 สมบูรณ์ กระบวนวิชา <?php echo $proposal['SpecialAndCooperative']['code']; ?></font>
                                            </div> 
                                          </div>
                                          <div class="panel-body">
                                            <tbody>   
                                                <table class="table table-bordered">
                                                    <tbody>
                                                      <tr>
                                                          <td>
                                                              <?php echo $output; ?>
                                                          </td>
                                                      </tr>                                
                                                    </tbody>
                                                </table>  
                                          </tbody>      
                        
                                        </div> 
                                      </div>         
                                    </form>
                                                
                              
                              
                              
                            </div>
                            <!-- ********** -->
                          </div> 
                          <div class="tab-pane fade" id="tab9primary">
                            
                            
                          </div> 
                          <div class="tab-pane fade" id="tab10primary">
                          
                          </div>
                    
                      
                        <!-- ถึงตรงนี้ -->
                      </div>
                    </div>
                  </div>

              <!-- ถึงตรงนี้ -->

              
            </div>
            
		</div>
	 
       
     
  </div>   
</div>