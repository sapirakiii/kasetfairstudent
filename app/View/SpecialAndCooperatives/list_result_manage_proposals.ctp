<?php //echo debug($SpecialAndCooperatives);?>
<script type="text/javascript">
	function confirm_click()
	{
		return confirm("!!!ข้อพึงระวัง !!! การลบหัวข้อดังกล่าว ไม่สามารถย้อนคืนสถานะใดๆ กลับคืนมาได้ สถานะการดำเนินการทั้งหมดจะถูกลบทั้งหมด โปรดตรวจสอบให้แน่ใจก่อนดำเนินการขั้นตอนนี้	  !! ยืนยันข้อมูล !!! ท่านแน่ใจหรือไม่ว่าต้องการลบข้อมูล หากถูกต้องแล้ว โปรดยืนยันการลบข้อมูล ? ");
	}

</script>
<?php $paginator = $this->Paginator;?>
	<?php 
		date_default_timezone_set('Asia/Bangkok');
		$date = date("Y-m-d");
		$time = date("H:i:s");

		function DateDiff($strDate1,$strDate2){                             
		return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
		}


	?>
	<?php function DateThai($strDate)
		{

		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//, $strHour:$strMinute
	}
	?>
	
	 
	 
	<div   style="padding:30px 15px 0;">	
		<!-- <div class="row"> -->
		 
				
				 
				<div class="panel panel-primary">
					<div class="panel-heading"> 
						ขอเปิดกระบวนวิชาใหม่/ปรับปรุงกระบวนวิชา
					</div>
					<div class="panel-body">
							
							<center>
								<a class="btn btn-danger btn-lg" href="<?php echo $this->Html->url(array('action' => 'add_proposals',3)); ?>" role="button">											
									<i class="fa fa-plus" aria-hidden="true"></i> เพิ่มกระบวนวิชาเปิด/ปรับปรุง มคอ.3-4 ระดับปริญญาโท
								</a>																	
							</center>
					</div>	
				</div>		
			 
	 
		<?php if ($admins['Admincurriculumrequest']['employee_status_id'] != 3) { ?> 
			<table class="table table-bordered">						
				<tr>
					<td colspan="4">
						<a href="<?php echo $this->Html->url(array('controller' => 'SpecialAndCooperatives','action' => 'list_result_manage_proposals')); ?>" class="btn btn-info font_panel7 pull-left"> 
							<span class="glyphicon glyphicon-book" aria-hidden="true"></span> ดูเสนอ มคอ.3-4 ปริญญาโท ทั้งหมด
						</a>
					</td>
						
				</tr>	
				<tr>
					<td>ระดับภาควิชา </td>
					<td>
						<!-- <a href="<?php echo $this->Html->url(array('controller' => 'SpecialAndCooperatives','action' => 'list_result_manage_proposals',1)); ?>" class="btn btn-default ">									
							<center>
							<span class="glyphicon glyphicon-book" aria-hidden="true"></span> <font class="font_red">อาจารย์/ภาควิชา </font>		
								<?php if($countSpecialAndCooperatives1 != null){ ?>
									<button type="button" class="btn btn-danger btn-circle"><?php echo $countSpecialAndCooperatives1 ?></button> 
								<?php } ?>
							</center>
						</a> -->
					</td>
					<td>
						<a href="<?php echo $this->Html->url(array('controller' => 'SpecialAndCooperatives','action' => 'list_result_manage_proposals',1)); ?>" class="btn btn-default ">									
							<center>
							<span class="glyphicon glyphicon-book" aria-hidden="true"></span> <font class="font_red">อาจารย์/ภาควิชา </font>		
								<?php if($countSpecialAndCooperatives1 != null){ ?>
									<button type="button" class="btn btn-danger btn-circle"><?php echo $countSpecialAndCooperatives1 ?></button> 
								<?php } ?>
							</center>
						</a>
						<!-- <a href="<?php echo $this->Html->url(array('controller' => 'SpecialAndCooperatives','action' => 'list_result_manage_proposals',2)); ?>" class="btn btn-default ">									
							<center>
							<span class="glyphicon glyphicon-book" aria-hidden="true"></span> <font class="font_red">สาขาวิชา/ภาควิชา </font>		
								<?php if($countSpecialAndCooperatives2 != null){ ?>
									<button type="button" class="btn btn-danger btn-circle"><?php echo $countSpecialAndCooperatives2 ?></button> 
								<?php } ?>
							</center>
						</a> -->
					</td>
					<td>ระดับคณะ 
						<a href="<?php echo $this->Html->url(array('controller' => 'SpecialAndCooperatives','action' => 'list_result_manage_proposals',4)); ?>" class="btn btn-default  ">
								
							<center>
								
								<span class="glyphicon glyphicon-book" aria-hidden="true"></span> <font class="font_red">งานบริการการศึกษาฯ  </font>	  
								<?php if($countSpecialAndCooperatives4 != null){ ?>
									<button type="button" class="btn btn-danger btn-circle"><?php echo $countSpecialAndCooperatives4 ?></button> 
								<?php } ?>
							</center>
						</a>
					
					</td>
						
					<td>
						
					</td>
					
				</tr>
				<tr>
					<td>ระดับผู้บริหาร </td>
					<td>
						<a href="<?php echo $this->Html->url(array('controller' => 'SpecialAndCooperatives','action' => 'list_result_manage_proposals',5)); ?>" class="btn btn-default ">
						
							<center>
								
								<span class="glyphicon glyphicon-book" aria-hidden="true"></span><font class="font_yellow">กรรมการบัณฑิตศึกษาประจำคณะ  </font>	  
								<?php if($countSpecialAndCooperatives5 != null){ ?>
									<button type="button" class="btn btn-danger btn-circle"><?php echo $countSpecialAndCooperatives5 ?></button> 
								<?php } ?>
							</center>
						</a>
					</td>
					<td>
						<a href="<?php echo $this->Html->url(array('controller' => 'SpecialAndCooperatives','action' => 'list_result_manage_proposals',6)); ?>" class="btn btn-default ">
						  	<center> 
								<span class="glyphicon glyphicon-book" aria-hidden="true"></span><font class="font_yellow">กรรมการวิชาการบัณฑิตวิทยาลัย  </font>	   
									<?php if($countSpecialAndCooperatives6 != null){ ?>
										<button type="button" class="btn btn-danger btn-circle"><?php echo $countSpecialAndCooperatives6 ?></button> 
									<?php } ?>
							</center>
						</a>
					</td>
					<td>
						<a href="<?php echo $this->Html->url(array('controller' => 'SpecialAndCooperatives','action' => 'list_result_manage_proposals',7)); ?>" class="btn btn-default  ">
							 
							<center>
								
								<span class="glyphicon glyphicon-book" aria-hidden="true"></span><font class="font_yellow">กรรมการบริหารวิชาการประจำบัณฑิตวิทยาลัย  </font> 
								<?php if($countSpecialAndCooperatives7 != null){ ?>
									<button type="button" class="btn btn-danger btn-circle"><?php echo $countSpecialAndCooperatives7 ?></button> 
								<?php } ?>
							</center>
						</a>
					</td>
					
				</tr>
				<tr>
					<td>ระดับหน่วยงานภายนอก </td>
					<td>
						<a href="<?php echo $this->Html->url(array('controller' => 'SpecialAndCooperatives','action' => 'list_result_manage_proposals',8)); ?>" class="btn btn-default  ">
								
							<center>
									
								<span class="glyphicon glyphicon-book" aria-hidden="true"></span><font class="font_yellow">กรรมการบริหารมหาวิทยาลัย  </font>  
									<?php if($countSpecialAndCooperatives8 != null){ ?>
										<button type="button" class="btn btn-danger btn-circle"><?php echo $countSpecialAndCooperatives8 ?></button> 
									<?php } ?>
							</center>
						</a>
					</td>
					<td>
						<a href="<?php echo $this->Html->url(array('controller' => 'SpecialAndCooperatives','action' => 'list_result_manage_proposals',9)); ?>" class="btn btn-default  ">
								
									<center>
										
									<span class="glyphicon glyphicon-book" aria-hidden="true"></span><font class="font_yellow">สภาวิชาการ  </font>   
										<?php if($countSpecialAndCooperatives9 != null){ ?>
											<button type="button" class="btn btn-danger btn-circle"><?php echo $countSpecialAndCooperatives9 ?></button> 
										<?php } ?>
									</center>
								</a>
					</td>
					<td>
						<a href="<?php echo $this->Html->url(array('controller' => 'SpecialAndCooperatives','action' => 'list_result_manage_proposals',10)); ?>" class="btn btn-default ">
							
									<center>
										
									<span class="glyphicon glyphicon-book" aria-hidden="true"></span><font class="font_green">สภามหาวิทยาลัย  </font>    
										<?php if($countSpecialAndCooperatives10 != null){ ?>
										
											<button type="button" class="btn btn-danger btn-circle"><?php echo $countSpecialAndCooperatives10 ?></button> 
										<?php } ?>
									</center>
								</a>
					</td>
					
				</tr>
			</table>
		<?php } ?>
					 
					<div id="no-more-tables"> 				
						<table class="table table-bordered table-striped" id="runner_table">
							<thead>
								<tr> 
									<th align="center">ลำดับ</th>
									<th style="width:20%">รหัสกระบวนวิชา</th>
									<th style="width:20%">ชื่อวิชาภาษาไทย</th>
									<th style="width:20%">ชื่อวิชาภาษาอังกฤษ</th>
									<th >สาขา</th>
									<th style="width:10%">ภาควิชา</th>
									<!-- <th style="width:20%">วันที่ยื่น</th>										  -->
									<!-- <th style="width:15%">สถานะ</th> -->
									<th style="width:20%">ระดับ</th>								
									<th>การพิจารณา</th> 
									 
									<?php  if ($admins['Admincurriculumrequest']['employee_status_id'] == 1) { ?>
										<?php if ($admins['Admincurriculumrequest']['employees_status_id'] == 0) { ?> 
											<th>จัดการข้อมูล</th>
										<?php } ?>
									<?php } ?>
								</tr>
							</thead>
							<tbody>
								<?php
									$i = 0;
									if ($SpecialAndCooperatives == null) {
										echo '<tr> <td colspan = "11"> <center> ขออภัย ไม่พบข้อมูลที่ต้องการ </center> </td></tr>'; ?>											 
									<?php  
									}else {								
										foreach ($SpecialAndCooperatives as $SpecialAndCooperative){
											
										$i++;
									?>
										<tr>
											<td data-title="ลำดับ" align="center"><?php echo $i; ?></td>
											<td data-title="รหัสวิชา"> <?php echo $SpecialAndCooperative['SpecialAndCooperative']['code'];?> 
												<?php  if($SpecialAndCooperative['SpecialAndCooperative']['pass_special_and_cooperative_date'] != null){ ?>
													<font color="green">วันที่มีผลบังคับใช้ &nbsp; :<?php echo DateThai($SpecialAndCooperative['SpecialAndCooperative']['pass_special_and_cooperative_date']);?></font>
												
												<?php } ?>	
											</td>
											<td data-title="ชื่อวิชาภาษาไทย"> <?php echo $SpecialAndCooperative['SpecialAndCooperative']['title'];?> </td>
											<td data-title="ชื่อวิชาภาษาอังกฤษ"> <?php echo $SpecialAndCooperative['SpecialAndCooperative']['title_eng'];?> </td>
											<td data-title="สาขาวิชา"> <?php echo $SpecialAndCooperative['Major']['major_name']; ?> </td>
											<td data-title="ภาควิชา"> <?php echo $SpecialAndCooperative['Organize']['name']; ?> </td>
											<!-- <td data-title="วันที่ยื่น"> <?php echo DateThai($SpecialAndCooperative['SpecialAndCooperative']['postdate']);?> </td>	  -->
											<!-- <td data-title="สถานะ"> <?php echo $SpecialAndCooperative['SpecialAndCooperativePass']['name'];?> </td>	  -->
											<td data-title="ระดับ" >
												
													<b>สถานะการติดตาม </b>
													<?php if($SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 4){ ?>
														<span class="label label-danger"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?php echo $SpecialAndCooperative['SpecialAndCooperativeFollowStatus']['name']; ?> </span>	
													<?php }elseif($SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] >= 4 && $SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] <= 9){ ?>
														<span class="label label-warning"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?php echo $SpecialAndCooperative['SpecialAndCooperativeFollowStatus']['name']; ?> </span>	
													<?php }elseif($SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 10){ ?>
														<span class="label label-success"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?php echo $SpecialAndCooperative['SpecialAndCooperativeFollowStatus']['name']; ?> </span>
													<?php } ?>
											</td>
												
										
											<?php if ($SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] != 10) { ?>
														
												<td data-title="การพิจารณา" style="width:20%" > 
													<?php if ($admins['Admincurriculumrequest']['employee_status_id'] == 3) { ?>
															
															<?php  if ($SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 10) { 
																echo 'เสร็จสิ้นกระบวนการ'; ?>
																	
															<?php }else { ?>
																	<?php	if($SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] >= 1 && $SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] <= 3 ){ ?>
																			พิจารณากระบวนวิชา <img src="https://www1.reg.cmu.ac.th/ugradapply/images/update.gif">										
																			
																				
																				<a class="btn btn-danger btn-lg" href="<?php echo $this->Html->url(array('action' => 'proposal_detail',$SpecialAndCooperative['SpecialAndCooperative']['id'],$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'])); ?>"
																					role="button" target="_blank"><i class="fa fa-commenting" aria-hidden="true"></i><font class="txt-content_SpecialAndCooperative26"> พิจารณากระบวนวิชา <?php echo $SpecialAndCooperative['SpecialAndCooperative']['code'];?></font></a><br>
																			
																			
																	<?php }else { ?>
																		<a class="btn btn-danger btn-lg" href="<?php echo $this->Html->url(array('action' => 'add_answer_abstract',$SpecialAndCooperative['SpecialAndCooperative']['id'],$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'])); ?>"
																			role="button" target="_blank"><i class="glyphicon glyphicon-tag" aria-hidden="true"></i><font class="txt-content_SpecialAndCooperative26"> ดูการพิจารณา</font></a><br>
																			
																	<?php } ?>
															<?php	} ?>
															
													<?php }elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 2) { ?>
															
															<?php  if ($SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 10) { 
																echo 'เสร็จสิ้นกระบวนการ'; ?>
																	
															<?php }else { ?>
																<?php	if($SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] >= 1 && $SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] <= 3){ ?>
																			พิจารณากระบวนวิชา <img src="https://www1.reg.cmu.ac.th/ugradapply/images/update.gif">										
																			
																				
																				<a class="btn btn-danger btn-lg" href="<?php echo $this->Html->url(array('action' => 'add_answer_abstract',$SpecialAndCooperative['SpecialAndCooperative']['id'],$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'])); ?>"
																					role="button" target="_blank"><i class="fa fa-commenting" aria-hidden="true"></i><font class="txt-content_SpecialAndCooperative26"> พิจารณากระบวนวิชา <?php echo $SpecialAndCooperative['SpecialAndCooperative']['code'];?></font></a><br>
																			
																			
																	<?php }else { ?>
																		<a class="btn btn-danger btn-lg" href="<?php echo $this->Html->url(array('action' => 'add_answer_abstract',$SpecialAndCooperative['SpecialAndCooperative']['id'],$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'])); ?>"
																			role="button" target="_blank"><i class="glyphicon glyphicon-tag" aria-hidden="true"></i><font class="txt-content_SpecialAndCooperative26"> ดูการพิจารณา</font></a><br>
																			
																	<?php } ?>
															<?php	} ?>
															
													<?php }elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 1 ) { ?>
															<?php if ($admins['Admincurriculumrequest']['employees_status_id'] == 0) { ?> 
																	<?php  if ($SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 10) { 
																		echo 'เสร็จสิ้นกระบวนการ'; ?>
																			
																	<?php }else { ?>
																			<?php	if($SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] >= 4 && $SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] <= 10){ ?>
																					พิจารณากระบวนวิชา <img src="https://www1.reg.cmu.ac.th/ugradapply/images/update.gif">										
																					
																						
																						<a class="btn btn-danger btn-lg" href="<?php echo $this->Html->url(array('action' => 'add_answer_abstract',$SpecialAndCooperative['SpecialAndCooperative']['id'],$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'])); ?>"
																							role="button" target="_blank"><i class="fa fa-commenting" aria-hidden="true"></i><font class="txt-content_SpecialAndCooperative26"> พิจารณารหัสกระบวนวิชา <?php echo $SpecialAndCooperative['SpecialAndCooperative']['code'];?></font></a><br>
																					
																					
																			<?php }else { ?>
																				<a class="btn btn-danger btn-lg" href="<?php echo $this->Html->url(array('action' => 'add_answer_abstract',$SpecialAndCooperative['SpecialAndCooperative']['id'],$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'])); ?>"
																					role="button" target="_blank"><i class="glyphicon glyphicon-tag" aria-hidden="true"></i><font class="txt-content_SpecialAndCooperative26"> ดูการพิจารณา </font></a><br>
																					
																			<?php } ?>
																	<?php	} ?>
															<?php }elseif ($admins['Admincurriculumrequest']['employees_status_id'] == 1) { ?>
																<?php if($SpecialAndCooperative['SpecialAndCooperative']['degree_id'] == 1){ ?> 
																	<?php  if ($SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 10) { 
																		echo 'เสร็จสิ้นกระบวนการ'; ?>
																			
																	<?php }else { ?>
																			<?php	if($SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] >= 4 && $SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] <= 10){ ?>
																					พิจารณากระบวนวิชา <img src="https://www1.reg.cmu.ac.th/ugradapply/images/update.gif">										
																					
																						
																						<a class="btn btn-danger btn-lg" href="<?php echo $this->Html->url(array('action' => 'add_answer_abstract',$SpecialAndCooperative['SpecialAndCooperative']['id'],$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'])); ?>"
																							role="button" target="_blank"><i class="fa fa-commenting" aria-hidden="true"></i><font class="txt-content_SpecialAndCooperative26"> พิจารณากระบวนวิชา <?php echo $SpecialAndCooperative['SpecialAndCooperative']['code'];?></font></a><br>
																					
																					
																			<?php }else { ?>
																				<a class="btn btn-danger btn-lg" href="<?php echo $this->Html->url(array('action' => 'add_answer_abstract',$SpecialAndCooperative['SpecialAndCooperative']['id'],$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'])); ?>"
																					role="button" target="_blank"><i class="glyphicon glyphicon-tag" aria-hidden="true"></i><font class="txt-content_SpecialAndCooperative26"> ดูการพิจารณา</font></a><br>
																					
																			<?php } ?>
																	<?php	} ?>
																<?php }else{ ?>
																	<a class="btn btn-danger btn-lg" href="<?php echo $this->Html->url(array('action' => 'add_answer_abstract',$SpecialAndCooperative['SpecialAndCooperative']['id'],$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'])); ?>"
																		role="button" target="_blank"><i class="glyphicon glyphicon-tag" aria-hidden="true"></i><font class="txt-content_SpecialAndCooperative26"> ดูการพิจารณา</font></a><br>
																					
																<?php } ?>
															<?php }elseif ($admins['Admincurriculumrequest']['employees_status_id'] == 2) { ?>
																<?php if($SpecialAndCooperative['SpecialAndCooperative']['degree_id'] != 1){ ?> 
																	<?php  if ($SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 10) { 
																		echo 'เสร็จสิ้นกระบวนการ'; ?>
																			
																	<?php }else { ?>
																			<?php	if($SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] >= 4 && $SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] <= 10){ ?>
																					พิจารณากระบวนวิชา <img src="https://www1.reg.cmu.ac.th/ugradapply/images/update.gif">										
																					
																						
																						<a class="btn btn-danger btn-lg" href="<?php echo $this->Html->url(array('action' => 'add_answer_abstract',$SpecialAndCooperative['SpecialAndCooperative']['id'],$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'])); ?>"
																							role="button" target="_blank"><i class="fa fa-commenting" aria-hidden="true"></i><font class="txt-content_SpecialAndCooperative26"> พิจารณากระบวนวิชา <?php echo $SpecialAndCooperative['SpecialAndCooperative']['code'];?></font></a><br>
																					
																					
																			<?php }else { ?>
																				<a class="btn btn-danger btn-lg" href="<?php echo $this->Html->url(array('action' => 'add_answer_abstract',$SpecialAndCooperative['SpecialAndCooperative']['id'],$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'])); ?>"
																					role="button" target="_blank"><i class="glyphicon glyphicon-tag" aria-hidden="true"></i><font class="txt-content_SpecialAndCooperative26"> ดูการพิจารณา</font></a><br>
																					
																			<?php } ?>
																	<?php	} ?>
																<?php }else{ ?>
																	<a class="btn btn-danger btn-lg" href="<?php echo $this->Html->url(array('action' => 'add_answer_abstract',$SpecialAndCooperative['SpecialAndCooperative']['id'],$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'])); ?>"
																		role="button" target="_blank"><i class="glyphicon glyphicon-tag" aria-hidden="true"></i><font class="txt-content_SpecialAndCooperative26"> ดูการพิจารณา</font></a><br>
																					
																<?php } ?>
															<?php } ?>
														
													<?php }else{ ?>
														<a class="btn btn-danger btn-lg" href="<?php echo $this->Html->url(array('action' => 'add_answer_abstract',$SpecialAndCooperative['SpecialAndCooperative']['id'],$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'])); ?>"
															role="button" target="_blank"><i class="glyphicon glyphicon-tag" aria-hidden="true"></i><font class="txt-content_SpecialAndCooperative26"> ดูการพิจารณา</font></a><br>
																	
													
													<?php } ?>
														
														
												</td>
											<?php }elseif ($SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 10) { ?>	
												<td data-title="ดูการพิจารณา" style="width:20%" >	
												<?php  if($SpecialAndCooperative['SpecialAndCooperative']['file'] != null){ ?>
													<font color="green">เสร็จสิ้นกระบวนการ </font><br>
														<a class="btn btn-success" href="<?php echo $this->Html->url(array('action' => 'add_answer_abstract',$SpecialAndCooperative['SpecialAndCooperative']['id'],$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'])); ?>"
															role="button" target="_blank"><i class="glyphicon glyphicon-tag" aria-hidden="true"></i><font class="txt-content_SpecialAndCooperative26"> ดูการพิจารณา</font>
														</a>
													<?php }else{ ?>		
																					
														<a class="btn btn-success" href="<?php echo $this->Html->url(array('action' => 'add_answer_abstract',$SpecialAndCooperative['SpecialAndCooperative']['id'],$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'])); ?>"
															role="button" target="_blank"><i class="glyphicon glyphicon-tag" aria-hidden="true"></i><font class="txt-content_SpecialAndCooperative26"> ดูการพิจารณา</font>
															</a>
													<?php } ?>	
												</td>		
											<?php } ?>		
													
												
											
											
											 
												<td data-title="อัพโหลดคำสั่ง">
													<?php if ($SpecialAndCooperative['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 10) { ?>
														<?php  if ($admins['Admincurriculumrequest']['employee_status_id'] == 1) { ?>
															<?php if ($admins['Admincurriculumrequest']['employees_status_id'] == 0) { ?> 
																<?php  if($SpecialAndCooperative['SpecialAndCooperative']['file'] != null){ ?>
																		<a href="<?php echo $this->Html->url('/files/students/SpecialAndCooperatives/'.$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'].'/') , $SpecialAndCooperative['SpecialAndCooperative']['file']; ?>" 
																		class="btn btn-active" target="_blank">
																		<span class="glyphicon glyphicon-print" aria-hidden="true"></span> ไฟล์ มคอ.3-4 ฉบับสมบูรณ์</a>
																
																<?php }else{ ?>
																	
																		<img src="http://www.agri.cmu.ac.th/2017/img/post/new_dark.gif">		
																			<a class="btn btn-success" href="<?php echo $this->Html->url(array('action' => 'attach_document',$SpecialAndCooperative['SpecialAndCooperative']['id'],$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'])); ?>"
																				role="button" ><i class="glyphicon glyphicon-arrow-up"></i>
																				อัพโหลดไฟล์ มคอ. ฉบับสมบูรณ์
																			</a>
																	
																<?php } ?>	 
															<?php }elseif ($admins['Admincurriculumrequest']['employees_status_id'] == 1) { ?>
																<?php if($SpecialAndCooperative['SpecialAndCooperative']['degree_id'] == 1){ ?> 
																	<?php  if($SpecialAndCooperative['SpecialAndCooperative']['file'] != null){ ?>
																			<a href="<?php echo $this->Html->url('/files/students/SpecialAndCooperatives/'.$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'].'/') , $SpecialAndCooperative['SpecialAndCooperative']['file']; ?>" 
																			class="btn btn-active" target="_blank">
																			<span class="glyphicon glyphicon-print" aria-hidden="true"></span> ไฟล์ มคอ.3-4 ฉบับสมบูรณ์</a>
																	
																	<?php }else{ ?>
																		
																			<img src="http://www.agri.cmu.ac.th/2017/img/post/new_dark.gif">		
																				<a class="btn btn-success" href="<?php echo $this->Html->url(array('action' => 'attach_document',$SpecialAndCooperative['SpecialAndCooperative']['id'],$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'])); ?>"
																					role="button" ><i class="glyphicon glyphicon-arrow-up"></i>
																					อัพโหลดไฟล์ มคอ. ฉบับสมบูรณ์
																				</a>
																		
																	<?php } ?>			
																<?php }else{ ?>
																	<td >
																		ไม่มีสิทธิ์เข้าถึงส่วนนี้
																	</td>
																<?php } ?>
															<?php }elseif ($admins['Admincurriculumrequest']['employees_status_id'] == 2) { ?>
																<?php if($SpecialAndCooperative['SpecialAndCooperative']['degree_id'] != 1){ ?> 
																	<?php  if($SpecialAndCooperative['SpecialAndCooperative']['file'] != null){ ?>
																			<a href="<?php echo $this->Html->url('/files/students/SpecialAndCooperatives/'.$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'].'/') , $SpecialAndCooperative['SpecialAndCooperative']['file']; ?>" 
																			class="btn btn-active" target="_blank">
																			<span class="glyphicon glyphicon-print" aria-hidden="true"></span> ไฟล์ มคอ.3-4 ฉบับสมบูรณ์</a>
																	
																	<?php }else{ ?>
																		<?php if ($admins['Admincurriculumrequest']['employee_status_id'] == 1) { ?>
																			<img src="http://www.agri.cmu.ac.th/2017/img/post/new_dark.gif">		
																				<a class="btn btn-success" href="<?php echo $this->Html->url(array('action' => 'attach_document',$SpecialAndCooperative['SpecialAndCooperative']['id'],$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'])); ?>"
																					role="button" ><i class="glyphicon glyphicon-arrow-up"></i>
																					อัพโหลดไฟล์ มคอ. ฉบับสมบูรณ์
																				</a>
																		<?php } ?>
																	<?php } ?>	
																<?php }else{ ?>
																	<td  data-title="จัดการข้อมูล">
																		ไม่มีสิทธิ์เข้าถึงส่วนนี้
																	</td>		
																<?php } ?>
															<?php } ?>
															
														<?php } ?>
																		
													<?php } ?>
											
												</td>
											
											
											 
											<?php  if ($admins['Admincurriculumrequest']['employee_status_id'] == 1) { ?>
												<?php if ($admins['Admincurriculumrequest']['employees_status_id'] == 0) { ?> 
														<td data-title="จัดการข้อมูล">
																
															<a class="btn btn-warning" target="_blank" href="<?php echo $this->Html->url(array('action' => 'edit_proposal',$SpecialAndCooperative['SpecialAndCooperative']['id'])); ?>">
																แก้ไขข้อมูลกระบวนวิชา
															</a>
															<br>	
															<a class="btn btn-danger" href="<?php echo $this->Html->url(array('action' => 'delete_proposal',$SpecialAndCooperative['SpecialAndCooperative']['id'])); ?>" onclick="return confirm_click();">
																ลบข้อมูลกระบวนวิชา
															</a>		
														</td>
													 
													<?php } ?>
												
											<?php } ?>
										</tr>
									<?php  } ?>						
								<?php  } ?>	
							</tbody>
						</table>
						
					</div>
			
					
				</div>
		
			</div>
			<hr>
			<script type="text/javascript">
				$(document).ready(function() {
				$('#runner_table').DataTable({
					// "language": {
					// 	"search": "<?php //echo $search; ?>",
					"lengthMenu": [[100, 75, 50, -1], [100, 75, 50, "All"]]
					// 	"Question": "<?php //echo $Question; ?>",
					// 	"QuestionEmpty": "<?php //echo $QuestionEmpty; ?>",
					// 	"emptyTable": "<?php //echo $emptyTable; ?>",
					// 	"paginate": {
					// 		"next":       ">",
					// 		"previous":   "<"
					// 	}
					// }
				});
				} );
			</script>
			
		<!-- </div>	 -->
	</div>
