<?php //debug($Students);?>
<?php
	function DateThai($strDate)
		{

		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//, $strHour:$strMinute
	}
	?>
<style>
.numberCircle {
    border-radius: 50%;
    behavior: url(PIE.htc); /* remove if you don't care about IE8 */
    width: 36px;
    height: 36px;
    padding: 8px;
    background: #fff;
    border: 2px solid black;
    color: black;
    text-align: center;
    font: 20px Arial, sans-serif;
    display: inline-block;
}
</style>
 
<div class="container">
	<div class="row">
  <div class="col-md-12">
			<div class="panel panel-primary">
					<div class="panel-heading"> 
					  ระบบขอเปิด/ปรับปรุงกระบวนวิชา (มคอ.3-4) คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่
					</div>
					<div class="panel-body">
            <div class="col-md-6">
              <?php 
                if ($admins != null) { ?>
                  <a class="btn btn-default dropdown-toggle btn-primary" href="<?php echo $this->Html->url(array('action' => 'list_result_manage_proposal')); ?>">
                  ย้อนกลับ
                  <span class="glyphicon glyphicon-home"></span>			
                </a>
              <?php }else{ ?>	
                <a class="btn btn-default dropdown-toggle btn-primary" href="<?php echo $this->Html->url(array('action' => 'list_proposal_all')); ?>">
                  ย้อนกลับ
                  <span class="glyphicon glyphicon-home"></span>			
                </a>
              <?php } ?>	
              <table class="table table-hovered">
                  <tbody>
                      <tr>
                          <td>รหัสกระบวนวิชา</td>
                          <td><?php echo $SpecialAndCooperatives['SpecialAndCooperative']['code']; ?></td>
                      </tr> 
                      <tr>
                          <td>ชื่อวิชา</td>
                          <td><?php echo $SpecialAndCooperatives['SpecialAndCooperative']['title']; ?></td>
                      </tr>  
                      <tr>
                          <td>สาขาวิชา</td>
                          <td><?php echo $SpecialAndCooperatives['Major']['major_name']; ?></td>
                      </tr>    
                                    
                        
                                  
                  </tbody>
              </table>       
      
            </div>
            <div class="col-md-6">
              <h4>เอกสารแนบ</h4>
              <table class="table table-bordered">
                  <tbody>
                    <tr>
                        <td >
                            <?php echo $output; ?>
                        </td>
                    </tr> 
                                                        
                  </tbody>
              </table>  
            </div>

             
                
					</div>
				</div>
			</div>

      
      <form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">
        <?php   $required = 'กรุณากรอกข้อมูล'; ?>
        <div class="col-md-6">
          
            <table class="table table-hover table-post" style="width: 100%;">
                <tr>
                    <td>    
                    <div class="font_panel5">	
                      <h3 ><div class="numberCircle">1</div> แก้ไขข้อมูลกระบวนวิชา </h3>
                      
                    </div> 
                    </td>
                </tr>
                
                <tr>
                  <td>
                      <?php 
                            echo $this->Form->hidden('SpecialAndCooperative.id', array(
                              'value' => $id,
                              'class' => 'form-control',
                            )); 
                                  
                        ?> 
                  </td>
                </tr>
                <tr>
                  <td>
                        <label for="PreEngDetail" class="addpost_input">
                        <span class="glyphicon glyphicon-book" style="font-size:16px"></span>
                        รหัสกระบวนวิชา </label>
                        <?php 
                        echo $this->Form->input('SpecialAndCooperative.code', array(                          
                          'type' => 'text',
                          'label' => false,                                                        
                          'class' => array('form-control'),                                                           
                          'error' => false,
                          'value' => ''.$SpecialAndCooperatives['SpecialAndCooperative']['code'].'',
                              
                      ));
                        ?>
                      
                  </td>
                </tr>
                <tr>
                  <td>
                        <label for="PreEngDetail" class="addpost_input">
                        <span class="glyphicon glyphicon-book" style="font-size:16px"></span>
                        ชื่อวิชาภาษาไทย </label>
                        <?php 
                        echo $this->Form->input('SpecialAndCooperative.title', array(                          
                          'type' => 'text',
                          'label' => false,                                                        
                          'class' => array('form-control'),                                                           
                          'error' => false,
                          'value' => ''.$SpecialAndCooperatives['SpecialAndCooperative']['title'].'',
                              
                      ));
                      ?>
                      
                  </td>
                </tr>
                <tr>
                  <td>
                        <label for="PreEngDetail" class="addpost_input">
                        <span class="glyphicon glyphicon-book" style="font-size:16px"></span>
                        ชื่อวิชาภาษาอังกฤษ </label>
                        <?php 
                        echo $this->Form->input('SpecialAndCooperative.title_eng', array(                          
                          'type' => 'text',
                          'label' => false,                                                        
                          'class' => array('form-control'),                                                           
                          'error' => false,
                          'value' => ''.$SpecialAndCooperatives['SpecialAndCooperative']['title_eng'].'',
                              
                      ));
                      ?>
                      
                  </td>
                </tr>
                <tr>
                  <td>
                      <label for="PreEngDetail" class="addpost_input">
                      <span class="glyphicon glyphicon-book" style="font-size:16px"></span>
                      สาขาวิชา </label>
                      <div class="form-group has-feedback">      
                                                                      
                        <?php  
                          echo $this->Form->radio('SpecialAndCooperative.major_id', 
                          $majors, 
                          array(
                            'legend' => false,   
                            'value' => $SpecialAndCooperatives['SpecialAndCooperative']['major_id'],                                                          
                            'separator' => '<br/>',
                          
                            'required',
                            'style' => 'width: 25px; 
                                  height: 25px; 
                                  vertical-align:text-bottom;'));
                        ?>
                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                    </div>  
                    
                  </td>
                </tr> 
                <tr>
                  <td>
                    <label for="DownloadDetail" class="addpost_input">ระบุภาคการศึกษาที่มีผลบังคับใช้</label>
                    <?php 
                              
                      echo $this->Form->input('SpecialAndCooperative.yearterm_id', array(
                        'options' => $yearterms,
                        'class' => 'form-control',
                        'value' => $SpecialAndCooperatives['SpecialAndCooperative']['yearterm_id'],         
                        'label' => false,
                      ));
                    ?>
                    <br>
                  </td>
                </tr> 
                <!-- <tr>
                  <td>
                    <div class="form-group">
                      <label class="col-sm-3 control-label">เลือกระดับปริญญา</label>
                            <div class="col-sm-9"> 
                              <?php  
                                  echo $this->Form->radio('SpecialAndCooperative.degree_id', 
                                $degrees, 
                                array(
                                  'legend' => false,                                                                                                 
                                  'separator' => '<br/>',
                                
                                  'required',
                                  'style' => 'width: 25px; 
                                        height: 25px; 
                                        vertical-align:text-bottom;'));
                              ?>                                          
                            </div>     
                        </div>
                      </div>
                    </td>
                 </tr> -->
                
                
                  
            </table> 
        </div> 
        <div class="col-md-6">
          <div class="row">
          	
            <table class="table table-hover table-post" style="width: 100%;">
                  <tr>
                      <td>                
                          <div class="font_panel5">	
                            <h3 ><div class="numberCircle">2</div> เพิ่มรายชื่ออาจารย์ผู้รับผิดชอบและอาจารย์ผู้สอน</h3>
                          </div>
                      </td>
                  </tr>
                  <tr>
                      <td><b>อาจารย์ผู้รับผิดชอบ </b> :
                          <?php 
            
                            $sSubPre = '';
                            if ($employees['MisSubPrename']['id'] != null) {
                              $sSubPre = $sSubPre . $employees['MisSubPrename']['name_short_th'] . ' ';
                            }
                            if ($sSubPre == '') {
                              $sSubPre = $sSubPre . $employees['MisPrename']['name_full_th'] . ' ';
                            } 
                          ?>  
                          <?php //echo $employees['Prefix']['fullname']," ",$employees['Employee']['fname']," ",$employees['Employee']['lname'];?> 
                          <b> <?php echo $sSubPre.''.$employees['MisEmployee']['fname_th'].' '.$employees['MisEmployee']['lname_th']; ?></b>
                        
                      </td>
                      
                  </tr>  
                  <tr>
                        <td>
                            <label class="col-sm-3 control-label"> <font color="red"> แก้ไขอาจารย์ผู้รับผิดชอบ *</font> </label> 
                              <div class="col-sm-9">
                                    <div class="form-group has-feedback">
                                  
                                      <select name="data[SpecialAndCooperative][employee_id]" class="form-control js-example-basic-single" >
                                          <?php echo $outputadvisor; ?>
                                      </select>
                                       
                                     
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                  </div>
                              </div>   
                        
                                                      
                        </td>
                      </tr>
                      <tr>
                          <td>   
                            เพิ่มรายชื่ออาจารย์ผู้สอน 
                          </td>
                      </tr>
                  <tr>
                    <td>
                        <table class="table table-bordered">
                            <tr>
                              <td>ลำดับ</td>
                              <td>รายนาม</td>
                           
                            </tr>
                          <?php echo  $outputSpecialAndCooperativeEmployee;  ?> 
                        </table>          
                        <table class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th style="text-align: center;">ลำดับ</th>  
                                <th style="text-align: center;">รายชื่ออาจารย์ผู้สอน</th>
                                
                              
                              
                              </tr>
                            </thead>
                            <tbody>  
                              <!-- **************************ประธานกรรมการคนที่ 1********************************                 								 -->
                                <tr style="text-align: center;">	
                                  <td data-title="อันดับที่" align="center">1	</td>
                                  <td data-title="รายชื่อคณะกรรมการ">
                                    <div class="form-group has-feedback">
                                      <select name="data[SpecialAndCooperative][advisor_id1]" class="form-control js-example-basic-single" >
                                        <?php echo $outputadvisee; ?>
                                      </select>
                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>                                                 
                                    </div>
                                       
                                  </td>
                                   
                                </tr>
                                <!-- **************************กรรมการคนที่ 2********************************   -->
                                <tr style="text-align: center;">	
                                  <td data-title="อันดับที่" align="center">2	</td>
                                  <td data-title="รายชื่อคณะกรรมการ">
                                    <div class="form-group has-feedback">
                                      <select name="data[SpecialAndCooperative][advisor_id2]" class="form-control js-example-basic-single" >
                                        <?php echo $outputadvisee; ?>
                                      </select>
                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>                                                 
                                    </div>
                                    
                                  </td>
                                  
                                </tr>
                                <!-- **************************กรรมการคนที่ 3********************************                 								 -->
                                <tr style="text-align: center;">	
                                  <td data-title="อันดับที่" align="center">3	</td>
                                  <td data-title="รายชื่อคณะกรรมการ">
                                    <div class="form-group has-feedback">
                                      <select name="data[SpecialAndCooperative][advisor_id3]" class="form-control js-example-basic-single" >
                                        <?php echo $outputadvisee; ?>
                                      </select>
                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>                                                 
                                    </div>
                                    
                                  </td>
                                   
                                </tr>
                                <!-- **************************กรรมการคนที่ 4********************************                 								 -->
                                <tr style="text-align: center;">	
                                  <td data-title="อันดับที่" align="center">4	</td>
                                  <td data-title="รายชื่อคณะกรรมการ">
                                    <div class="form-group has-feedback">
                                      <select name="data[SpecialAndCooperative][advisor_id4]" class="form-control js-example-basic-single" >
                                        <?php echo $outputadvisee; ?>
                                      </select>
                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>                                                 
                                    </div>
                                    
                                  </td>
                                   
                                </tr>
                                <!-- **************************กรรมการคนที่ 5********************************                 								 -->
                                <tr style="text-align: center;">	
                                  <td data-title="อันดับที่" align="center">5	</td>
                                  <td data-title="รายชื่อคณะกรรมการ">
                                    <div class="form-group has-feedback">
                                      <select name="data[SpecialAndCooperative][advisor_id5]" class="form-control js-example-basic-single" >
                                        <?php echo $outputadvisee; ?>
                                      </select>
                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>                                                 
                                    </div>
                                    
                                  </td>
                                   
                                </tr>
                              </tbody>
                          </table>
                    </td>                
                  </tr>
            </table>
            <div class="font_panel5">	
              <h3 ><div class="numberCircle">3</div> แนบไฟล์ มคอ.3-4 (ถ้ามี)</h3>
            </div>                      
            <div class="form-group col-md-6">                                
              <label>
              <span class="glyphicon glyphicon-book" style="font-size:16px"></span>
              แนบไฟล์ มคอ word  </label><br>
               
              <?php
                  echo $this->Form->input('SpecialAndCooperativeDocument.fileDOC.', array(
                    'type' => 'file',
                    'accept' => '.doc , .docx',
                    ));
                ?>
            </div>
            <div class="form-group col-md-6">  
              <span class="glyphicon glyphicon-book" style="font-size:16px"></span>                              
              <label> แนบไฟล์ มคอ pdf  </label><br> 
              <?php
                  
                  echo $this->Form->input('SpecialAndCooperativeDocument.filePDF.', array(
                    'type' => 'file',
                    'accept' => '.pdf',
                    ));
                ?>
            </div>				  
            
                    
                      
          </div>                
        </div> 
        <div class="col-md-12">
              <input type="submit" value="บันทึกข้อมูล" class="btn btn-success btn-lg btn-block"> 
        </div>                   
    	</form>
	</div>
</div> <!-- /container -->