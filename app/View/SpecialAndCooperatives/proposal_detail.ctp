<style>
.numberCircle {
    border-radius: 50%;
    behavior: url(PIE.htc); /* remove if you don't care about IE8 */
    width: 36px;
    height: 36px;
    padding: 8px;
    background: #fff;
    border: 2px solid black;
    color: black;
    text-align: center;
    font: 20px Arial, sans-serif;
    display: inline-block;
}
</style>
<?php //echo debug($this->request->data)?>
<!-- start content here -->
<div  style="padding:15px 0 0;">
  <div class="row">

    <div class="col-md-12">
        <div class="panel panel-primary">
          <div class="panel-heading"> 
            รายการที่ขอเปิดกระบวนวิชาใหม่/ปรับปรุงกระบวนวิชา
          </div>
            <div class="panel-body">

            <div class="col-md-12 panel with-nav-tabs panel-default" style="border-color: #F2F2F2!important;padding-left: 0px;padding-right: 0px;">
                  
                    <div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
                        <ul class="nav nav-tabs">
                          <li class="active"><a href="#tab8primary" data-toggle="tab"><i class="fa fa-university" aria-hidden="true"></i> ข้อมูลการขอเปิด/ปรังปรุงกระบวนวิชา</a></li>
                          <!-- <li><a href="#tab9primary" data-toggle="tab"> คณะกรรมการที่ปรึกษาวิทยานิพนธ์/การค้นคว้าอิสระ</a></li> -->
                          <!-- <li><a href="#tab10primary" data-toggle="tab"> ประวัติการพิจารณา</a></li> -->
                    
                        </ul>
                          </div>
                      <div class="panel-body">
                        <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab8primary">
                          <div class="col-md-6">
                          
                            <table class="table table-bordered">
                                <tbody>  
                                      <tr>
                                          <td><b>ชื่อ-สกุล</b></td>
                                          <td><?php echo $SpecialAndCooperatives['Employee']['fname']," ",$SpecialAndCooperatives['Employee']['lname'];?> </td>

                                      </tr> 
                                      <tr>
                                          <td><b>ภาควิชา</b> </td>
                                          <td>  <?php echo $SpecialAndCooperatives['Organize']['name'];?></td>

                                      </tr> 
                                </tbody>
                            </table> <br>
                            <div class="panel panel-warning">
                              <div class="panel-heading"> 
                              <i class="glyphicon glyphicon-bookmark"></i>
                              <font class="txt-content_SpecialAndCooperative26"> รายละเอียดหลักสูตรกระบวนวิชา <?php echo $SpecialAndCooperatives['SpecialAndCooperative']['code'];?></font>
                              
                              </div>
                                <div class="panel-body"> 
                                  <table class="table table-hovered">
                                      <tbody>
                                                          
                                          <tr>
                                              <td>ชื่อหลักสูตร</td>
                                              <td><?php echo $SpecialAndCooperatives['SpecialAndCooperative']['title']; ?></td>
                                          </tr>  
                                          <tr>
                                              <td>สาขาวิชา</td>
                                              <td><?php echo $SpecialAndCooperatives['Major']['major_name']; ?></td>
                                          </tr>    
                                          <tr>
                                              <td>สถานะ</td>
                                              <td>
                                                <?php
                                                  if ($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 1 || $SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 2) { ?>
                                                    <font color="red"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> <?php echo $SpecialAndCooperatives['SpecialAndCooperativePass']['name']; ?> </font>	
                                                  <?php	}elseif ($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 3){ ?>			
                                                      <font color="orange"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>  <?php echo $SpecialAndCooperatives['SpecialAndCooperativePass']['name']; ?></font> 
                                                  <?php	}elseif ($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 4 || $SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 5){ ?>		
                                                      <font color="green"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>  <?php echo $SpecialAndCooperatives['SpecialAndCooperativePass']['name']; ?></font> 
                                                  <?php } ?>
                                              </td>
                                          </tr> 
                                          <tr>
                                                  <td><b>สถานะการติดตาม : </b></td>
                                                  <td> 
                                                    <?php if($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 4){ ?>
                                                      <span class="label label-danger"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?php echo $SpecialAndCooperatives['SpecialAndCooperativeFollowStatus']['name']; ?> </span>	
                                                    <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] >= 4 && $SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] <= 9){ ?>
                                                      <span class="label label-warning"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?php echo $SpecialAndCooperatives['SpecialAndCooperativeFollowStatus']['name']; ?> </span>	
                                                    <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 10){ ?>
                                                      <span class="label label-success"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?php echo $SpecialAndCooperatives['SpecialAndCooperativeFollowStatus']['name']; ?> </span>
                                                    <?php } ?>
                                                  </td>
                                          </tr> 
                                              
                                      </tbody>  
                                  </table> 
                                    <!-- <h4>เอกสารแนบ</h4>
                                    <table class="table table-bordered">
                                        <tbody>
                                          <tr>
                                              <td >
                                                  <?php echo $output; ?>
                                              </td>
                                          </tr> 
                                                                              
                                        </tbody>
                                    </table>                                            -->
                                </div>
                            </div>
                            <div class="panel panel-primary">
                                <div class="panel-heading"> 
                                    <font class="txt-content_SpecialAndCooperative26"> ระดับการติดตามรหัสกระบวนวิชา <?php echo $SpecialAndCooperatives['SpecialAndCooperative']['code'] ?></font>

                                    </div>
                                    <div class="panel-body">
                                      <?php if($SpecialAndCooperatives['SpecialAndCooperative']['degree_id'] == 1){ ?>
                                        <div class="row bs-wizard" style="border-bottom:0;">
                                            <?php if($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 1){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 1){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete">
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 1) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?>
                                              <div class="text-center bs-wizard-stepnum">Step 1</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center  ">อาจารย์</div>
                                            </div>
                                            
                                            <?php if($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 2){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 2){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 2) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 2</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center  ">ภาควิชา</div>
                                            </div>
                                            <?php if($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 4){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 4){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 4) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 3</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">งานบริการการศึกษา</div>
                                            </div>
                                      
                                            <?php if($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 5){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 5){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 5) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 4</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">ผู้บริหาร(ฝ่ายวิชาการ)</div>
                                            </div>  
                                        </div>
                                        <div class="row bs-wizard" style="border-bottom:0;"> 
                                            <?php if($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 6){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 6){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 6) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 5</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">กรรมการวิชาการ <br> ระดับปริญญาตรี</div>
                                            </div>
                                            
                                            <?php if($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 7){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 7){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 7) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 6</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">กรรมการบริหารประจำคณะ</div>
                                            </div>
                                            <?php if($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 8){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 8){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 8) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 7</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">สำนักพัฒนาคุณภาพนักศึกษา</div>
                                            </div>
                                      
                                            <?php if($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 9){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 9){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 9) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 8</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">กรรมการบริหารและ <br>ประสานงานวิชาการ	</div>
                                            </div>  
                                        </div>
                                        <div class="row bs-wizard" style="border-bottom:0;"> 
                                            <?php if($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 10){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 10){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 10) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 9</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">มหาวิทยาลัย</div>
                                            </div>
                                            
                                              
                                        </div>
                                      <?php }else { ?>
                                        <div class="row bs-wizard" style="border-bottom:0;"> 
                                            <?php if($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 1){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 1){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 1) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 1</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center  ">อาจารย์</div>
                                            </div>
                                            
                                            <?php if($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 2){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 2){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 2) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 2</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center  ">ภาควิชา</div>
                                            </div>
                                            <?php if($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 4){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 4){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 4) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 3</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">งานบริการการศึกษา</div>
                                            </div>
                                      
                                            <?php if($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 5){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 5){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 5) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 4</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">กรรมการบัณฑิตศึกษา <br> ประจำคณะ</div>
                                            </div>  
                                        </div>
                                        <div class="row bs-wizard" style="border-bottom:0;"> 
                                            <?php if($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 6){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 6){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 6) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 5</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">กรรมการวิชาการ <br> บัณฑิตวิทยาลัย</div>
                                            </div>
                                            
                                            <?php if($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 7){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 7){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 7) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 6</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">กรรมการบริหารวิชาการ <br>ประจำบัณฑิตวิทยาลัย</div>
                                            </div>
                                            <?php if($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 8){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 8){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 8) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 7</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">กรรมการบริหารมหาวิทยาลัย</div>
                                            </div>
                                      
                                            <?php if($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 9){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 9){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 9) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 8</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">สภาวิชาการ</div>
                                            </div>  
                                        </div>
                                        <div class="row bs-wizard" style="border-bottom:0;"> 
                                            <?php if($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 10){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 10){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 10) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 9</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">สภามหาวิทยาลัย</div>
                                            </div>
                                            
                                              
                                        </div>
                                       
                                      <?php } ?>

                                    </div> 
                                </div>
                              </div>

                          </div>
                          <div class="col-md-6">
                            <?php if ($SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] >= 1 && $SpecialAndCooperatives['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] <= 3) { ?>
                              <div class="panel panel-danger">
                                <div class="panel-heading"> 
                                  <i class="glyphicon glyphicon-upload"></i>
                                  <font class="txt-content_SpecialAndCooperative26">  แก้ไขการเสนอ มคอ.3-4 <img alt="" src="https://www1.reg.cmu.ac.th/ugradapply/images/update.gif" /></font>
                                </div>
                                  <div class="panel-body">
                                    <form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">	                              
                                          <?php 
                                              
                                              $required = 'กรุณากรอกข้อมูล';
                                              
                                              echo $this->Form->create('SpecialAndCooperative',array('role' => 'form','data-toggle' => 'validator')); 
                                             
                                              echo $this->Form->hidden('SpecialAndCooperative.id', array(
                                                'value' => $proposal_id,
                                                'data-error' => $required,
                                                'label' => false,
                                                'class' => 'form-control'
                                              ));

                                              
                                          
                                          ?>  
                                            <table class="table table-bordered">
                                              <tbody>
                                                <tr>
                                                    <td>    
                                                    <div class="font_panel5">	
                                                      <h3 ><div class="numberCircle">1</div> แก้ไขข้อมูลรหัสกระบวนวิชา <?php echo $SpecialAndCooperatives['SpecialAndCooperative']['code'] ?> </h3>
                                                      
                                                    </div> 
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label class="col-sm-3 control-label"> รหัสวิชา <font color="red">*</font> </label> 
                                                        <div class="col-sm-9">
                                                                  <div class="form-group has-feedback">      
                                                                    <?php 
                                                                          echo $this->Form->input('SpecialAndCooperative.code', array(
                                                                              'type' => 'text',
                                                                              'label' => false,
                                                                              'div' => false,
                                                                              'value' => $SpecialAndCooperatives['SpecialAndCooperative']['code'],
                                                                              'class' => array('form-control css-require'),                                                           
                                                                              'error' => false,
                                                                              'required'
                                                                              ));
                                                                      
                                                                        ?>
                                                              ตัวอย่าง เช่น <font color="red"> 001101 </font>
                                                              <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                            </div>
                                                        </div>    
                                                    
                                              
                                                    </td>
                                                </tr>
                                                <tr>
                                                  <td>
                                                      <label class="col-sm-3 control-label"> ชื่อวิชาภาษาไทย <font color="red">*</font> </label> 
                                                      <div class="col-sm-9">
                                                                <div class="form-group has-feedback">      
                                                                  <?php 
                                                                        echo $this->Form->input('SpecialAndCooperative.title', array(
                                                                            'type' => 'text',
                                                                            'label' => false,
                                                                            'div' => false,
                                                                            'value' => $SpecialAndCooperatives['SpecialAndCooperative']['title'],
                                                                            'class' => array('form-control css-require'),                                                           
                                                                            'error' => false,
                                                                            'required'
                                                                            ));
                                                                    
                                                                      ?>
                                                            ตัวอย่าง เช่น <font color="red"> ภาษาอังกฤษพื้นฐาน 1  </font>                                                           
                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                          </div>
                                                      </div>    
                                                  
                                            
                                                  </td>
                                                </tr> 
                                                <tr>
                                                  <td>
                                                      <label class="col-sm-3 control-label"> ชื่อวิชาภาษาอังกฤษ <font color="red">*</font> </label> 
                                                      <div class="col-sm-9">
                                                                <div class="form-group has-feedback">      
                                                                  <?php 
                                                                        echo $this->Form->input('SpecialAndCooperative.title_eng', array(
                                                                            'type' => 'text',
                                                                            'label' => false,
                                                                            'div' => false,
                                                                            'value' => $SpecialAndCooperatives['SpecialAndCooperative']['title_eng'],
                                                                            'class' => array('form-control css-require'),                                                           
                                                                            'error' => false,
                                                                            'required'
                                                                            ));
                                                                    
                                                                      ?>
                                                            ตัวอย่าง เช่น <font color="red"> Fundamental English 1 </font>
                                                            
                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                          </div>
                                                      </div>    
                                                  
                                            
                                                  </td>
                                              </tr> 
                                                <tr>
                                                  <td>
                                                    <table class="table table-hover table-post" style="width: 100%;">  
                                                      <tr>
                                                          <td>                
                                                              <div class="font_panel5">	
                                                                <h3 ><div class="numberCircle">2</div> อัพโหลดไฟล์ มคอ.3/4</h3>
                                                                ท่านสามารถแนบไฟล์ word หรือ ไฟล์ pdf /แนบได้ทั้ง 2 ไฟล์  
                                                              </div>
                                                          </td>
                                                      </tr> 
                                                      <tr>
                                                        <td>
                                                          <label for="InfoDetail" class="addpost_input">อัปโหลดรูปภาพ และไฟล์เอกสารประกอบ</label> <font color="red">(กรณีเลือกมากกว่า 1 ไฟล์ ให้กด Ctrl หรือ Shift ค้างไว้และเลือกรูปภาพหรือไฟล์ที่ต้องการ)</font>
                                                            <?php
                                                                echo $this->Form->input('SpecialAndCooperativeDocument.files.', array('type' => 'file', 'multiple','accept' => '.xlsx,.xls,image/*, .doc, .docx, .ppt, .pptx, .txt, .pdf, .zip, .rar'));
                                                            ?>
                                                        </td>
                                                      </tr>                                                       
                                                       
                                                       
                                                    </table>
                                                  </td>
                                                </tr>
                                                <!-- <tr>
                                                        <td >
                                                            <center> <font style="font-size:1.5em" class="text-danger">อัพโหลดไฟล์ มคอ.3/4 ที่แก้ไข </font></center>
                                                            <font style="font-size:1.5em" class="text-danger">ไฟล์ WORD</font>
                                                            ไฟล์ที่แก้ไขแล้ว .docx 
                                                            <?php
                                                                echo $this->Form->input('SpecialAndCooperativeDocument.fileDOC.', array(
                                                                  'type' => 'file',
                                                                  'accept' => '.doc , .docx',
                                                                  ));
                                                              ?>                                               
                                                        </td>
                                                    </tr>   
                                                    <tr>
                                                        <td >
                                                        <font style="font-size:1.5em" class="text-danger">ไฟล์ PDF</font>
                                                            ไฟล์ที่แก้ไขแล้ว .pdf <br> 
                                                                              <?php
                                                                                  
                                                                                  echo $this->Form->input('SpecialAndCooperativeDocument.filePDF.', array(
                                                                                    'type' => 'file',
                                                                                    'accept' => '.pdf',
                                                                                    ));
                                                                                ?>                                          
                                                        </td>
                                                    </tr>                                      -->
                                              </tbody>
                                            </table> 
                                            <input type="submit" value="บันทึกข้อมูล" class="btn btn-success btn-lg btn-block"> 
                                                
                                    </form>                                         
                                  </div>
                              </div>
                              
                            <?php }else { ?> 
                              
                             
                            <?php } ?>                    
                          </div>
                        
                        
                        
                        </div>
                        <div class="tab-pane fade" id="tab9primary">
                          
                          
                        </div> 
                        <div class="tab-pane fade" id="tab10primary">
                          <!-- <table class="table table-bordered">
                              <?php 
                                echo $outputlogs; 
                              ?>
                          </table> -->
                        </div>
                    
                      
                        <!-- ถึงตรงนี้ -->
                      </div>
                    </div>
                  </div>

              <!-- ถึงตรงนี้ -->

              
            </div>
    </div>
    <div class="col-md-6">
      <div class="panel panel-warning">
          <div class="panel-heading"> 
          <font class="txt-content_SpecialAndCooperative26"> กระบวนการติดตามรหัสกระบวนวิชา <?php echo $SpecialAndCooperatives['SpecialAndCooperative']['code'] ?></font>

          </div>
          <div class="panel-body">
            <table class="table table-bordered">
              <?php 
                echo $outputlogs; 
              ?>
            </table>				
            
          </div>
      </div>
                           
         
        
         
    </div>
    <div class="col-md-6">
                                     

        <div class="panel panel-success">
          <div class="panel-heading"> 
            <i class="glyphicon glyphicon-modal-window"></i>
            <font class="txt-content_SpecialAndCooperative26">  การพิจารณาขอเปิด/ปรับปรุงรหัสกระบวนวิชา <?php echo $SpecialAndCooperatives['SpecialAndCooperative']['code'];?></font>
          </div>
            <div class="panel-body">
              <?php	  $i= 1;
                  foreach ($answers as $answer){ ?>
                                    
                    <?php if ($answer['AnswerSpecialAndCooperative']['status'] == 1) { ?>
                      
                      <div class="panel panel-default" style="border-color: #d9edf7!important">
                              <div class="panel-heading"  style="background-color: #d9edf7!important;border-color: #d9edf7!important"> 
                                <h3 class="panel-title">ตอบกลับครั้งที่  <?php echo $i; ?></h3> 
                              </div>
                              <div class="panel-body">
                                <tbody>
                                  <div class="col-sm-12">
                                      <div class="form-group has-feedback">      
                                            <?php echo $answer['AnswerSpecialAndCooperative']['Detail']; ?>
                                            
                                      </div>
                                      <div class="col-sm-12">
                                        
                                        <?php  

                                        if($answer['AnswerSpecialAndCooperativeDocument'] != array()){

                                            echo '<br><label><span class="glyphicon glyphicon-list-alt"></span>เอกสารแนบ</label>';

                                          foreach ($answer['AnswerSpecialAndCooperativeDocument'] as $key) {
                                              $fileAction = $this->webroot.'files/students/SpecialAndCooperatives/'.$answer['AnswerSpecialAndCooperative']['employee_id'].'/'.$key['name'] ;
                                            echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="'.$fileAction.'" class="btn btn-primary" target="_blank">'.$key['name'].' <span class="glyphicon glyphicon-download-alt"></span></a>';
                                          }
                                        }


                                          ?>
                                      </div>
                                  </div>                                          
                                  <div class="col-sm-12">
                                    <div class="form-group has-feedback">
                                                <blockquote>
                                                <i class="fa fa-user-circle-o" aria-hidden="true"></i> <b>ตอบกลับโดย :</b>
                                                <font class="txt-content_SpecialAndCooperative26">
                                            <?php echo $answer['AnswerSpecialAndCooperative']['PostName']; ?>
                                            </font>  <small><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $answer['AnswerSpecialAndCooperative']['created']; ?></small>
                                              </blockquote>      
                                </tbody>
                        </div> 
                      </div>           
                    <?php }else{ ?>
                              <div class="panel panel-default" style="border-color: #DABAFA!important">
                                <div class="panel-heading" style="background-color: #DABAFA!important;border-color: #DABAFA!important"> 
                                      <h3 class="panel-title">ตอบกลับครั้งที่  <?php echo $i; ?></h3> 
                                </div>
                                <div class="panel-body">
                                  <tbody>                                           
                                  
                                      <div class="col-sm-12">
                                            <div class="form-group has-feedback">      
                                            <?php echo $answer['AnswerSpecialAndCooperative']['Detail']; ?>
                                            <?php  $admins = $this->Session->read('adminCurriculumRequests'); ?>
                                            <?php if ($answer['AnswerSpecialAndCooperative']['employee_status_id'] == $admins['AdminCurriculumRequest']['employee_status_id']) {                                                       
                                                  ?>  
                                                  <!-- <a class="btn btn-danger pull-right" href="<?php echo $this->Html->url(array('action' => 'delete_comment',$answer['AnswerSpecialAndCooperative']['id'],1,$SpecialAndCooperative_id,$student_id)); ?>">
                                                    ลบ
                                                  </a>                                                         -->
                                              <?php  }else { ?>
                                              
                                                <?php } ?> 
                                        </div>
                                                                            
                                      <div class="col-sm-12">
                                            <div class="form-group has-feedback">
                                            <blockquote>
                                            <i class="fa fa-user-circle-o" aria-hidden="true"></i> <b>ตอบกลับโดย :</b>
                                            <font class="txt-content_SpecialAndCooperative26">
                                            <?php echo $answer['AnswerSpecialAndCooperative']['PostName']; ?>
                                            </font>
                                              <small><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $answer['AnswerSpecialAndCooperative']['created']; ?></small>
                                            </blockquote>      
                                            
                                  </tbody> 
                                </div>
                              </div>
                                     
                                                                                              
                                                    
                             

                    <?php } $i++;  ?>
                    </div>
              </div>
              <?php  } ?>
            
            </div>
        </div>
                                                          
    </div> 

  
  </div>   
</div>   