<?php //echo debug($SpecialAndCooperatives);?>
<?php $paginator = $this->Paginator;?>
	<?php 
		date_default_timezone_set('Asia/Bangkok');
		$date = date("Y-m-d");
		$time = date("H:i:s");

		function DateDiff($strDate1,$strDate2){                             
		return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
		}


	?>
	<?php function DateThai($strDate)
		{

		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//, $strHour:$strMinute
	}
	?>
	
	<!-- class="container" -->
<div    style="padding:45px 0 0 0;">
	
 
		 
				<?php $admins = $this->Session->read('adminCurriculumRequests'); ?>
				<div id="no-more-tables">
				 
					<table class="table table-bordered table-striped" id="runner_table">
						<thead>
							<tr>
								<th style="text-align: center;">ลำดับ</th>
								<th style="width:5%">รหัสกระบวนวิชา</th>
								<th style="width:10%">ชื่อวิชาภาษาไทย</th>
								<th style="width:10%">ชื่อวิชาภาษาอังกฤษ</th>
					
								<th >สาขาวิชา</th>
								<th style="width:10%">ภาควิชา</th>
								
								<th style="width:20%">ภาคการศึกษาที่มีผลบังคับใช้</th> 
								<th>ไฟล์ มคอ.3-4 ฉบับสมบูรณ์ </th>
							
								
							</tr>
						</thead>
						<tbody>
							<?php
								$i = 0;
								if ($SpecialAndCooperatives == null) {
									echo '<tr>
										<td colspan = "11"> <center>
											ขออภัย ยังไม่พบข้อมูล </center><br>'; ?>

											
								<?php echo '</td>
									</tr>';
								}else {								
									foreach ($SpecialAndCooperatives as $SpecialAndCooperative){
										
									$i++;
								?>
									<tr>
										<td data-title="ลำดับ" align="center"><?php echo $i; ?></td>
										<td data-title="รหัสวิชา"> <?php echo $SpecialAndCooperative['SpecialAndCooperative']['code'];?> </td>
										<td data-title="ชื่อวิชาภาษาไทย"> <?php echo $SpecialAndCooperative['SpecialAndCooperative']['title'];?> </td>
										<td data-title="ชื่อวิชาภาษาอังกฤษ"> <?php echo $SpecialAndCooperative['SpecialAndCooperative']['title_eng'];?> </td>
									
										<td data-title="สาขาวิชา"> <?php echo $SpecialAndCooperative['Major']['major_name']; ?> </td>
										<td data-title="ภาควิชา"> <?php echo $SpecialAndCooperative['Organize']['name']; ?> </td>
										<td data-title="สถานะ " >
											<?php echo $SpecialAndCooperative['Yearterm']['name']; ?>		 
										</td>
										
									
										<td data-title="ไฟล์ มคอ.3-4 ฉบับสมบูรณ์">
											<a class="btn btn-success" href="<?php echo $this->Html->url(array('action' => 'add_answer_pass_abstract',$SpecialAndCooperative['SpecialAndCooperative']['id'],$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'])); ?>"
														role="button" target="_blank"><i class="glyphicon glyphicon-tag" aria-hidden="true"></i>  ดูมคอ.3-4 สมบูรณ์ 
														</a>
											<?php if ($admins['Admincurriculumrequest']['employee_status_id'] == 1) { ?> 
												<a class="btn btn-warning" target="_blank" href="<?php echo $this->Html->url(array('action' => 'edit_proposal',$SpecialAndCooperative['SpecialAndCooperative']['id'])); ?>">
													แก้ไขข้อมูลกระบวนวิชา
												</a>
											<?php } ?>
										</td>
										
										
									
											
									</tr>
								<?php  } ?>						
							<?php  } ?>	
						</tbody>
					</table>
					 
				</div>
		
				
			</div>
	
		</div>
		<hr>
		<script type="text/javascript">
			$(document).ready(function() {
			$('#runner_table').DataTable({
				// "language": {
				// 	"search": "<?php //echo $search; ?>",
				"lengthMenu": [[100, 75, 50, -1], [100, 75, 50, "All"]]
				// 	"Question": "<?php //echo $Question; ?>",
				// 	"QuestionEmpty": "<?php //echo $QuestionEmpty; ?>",
				// 	"emptyTable": "<?php //echo $emptyTable; ?>",
				// 	"paginate": {
				// 		"next":       ">",
				// 		"previous":   "<"
				// 	}
				// }
			});
			} );
		</script>
		
 	
</div>
