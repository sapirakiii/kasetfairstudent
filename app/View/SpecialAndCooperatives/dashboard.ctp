<style type="text/css">
  .shape{ 
  border-style: solid; border-width: 0 70px 40px 0; float:right; height: 0px; width: 0px;
  -ms-transform:rotate(360deg); /* IE 9 */
  -o-transform: rotate(360deg);  /* Opera 10.5 */
  -webkit-transform:rotate(360deg); /* Safari and Chrome */
  transform:rotate(360deg);
  }
  .offer{
    background:#fff; border:1px solid #ddd; box-shadow: 0 10px 20px rgba(0, 0, 0, 0.2); margin: 15px 0; overflow:hidden;
  }
  .offer-radius{
    border-radius:7px;
  }
  .offer-danger { border-color: #d9534f; }
  .offer-danger .shape{
    border-color: transparent #d9534f transparent transparent;
    border-color: rgba(255,255,255,0) #d9534f rgba(255,255,255,0) rgba(255,255,255,0);
  }
  .offer-success {  border-color: #5cb85c; }
  .offer-success .shape{
    border-color: transparent #5cb85c transparent transparent;
    border-color: rgba(255,255,255,0) #5cb85c rgba(255,255,255,0) rgba(255,255,255,0);
  }
  .offer-default {  border-color: #999999; }
  .offer-default .shape{
    border-color: transparent #999999 transparent transparent;
    border-color: rgba(255,255,255,0) #999999 rgba(255,255,255,0) rgba(255,255,255,0);
  }
  .offer-primary {  border-color: #428bca; }
  .offer-primary .shape{
    border-color: transparent #428bca transparent transparent;
    border-color: rgba(255,255,255,0) #428bca rgba(255,255,255,0) rgba(255,255,255,0);
  }
  .offer-info { border-color: #5bc0de; }
  .offer-info .shape{
    border-color: transparent #5bc0de transparent transparent;
    border-color: rgba(255,255,255,0) #5bc0de rgba(255,255,255,0) rgba(255,255,255,0);
  }
  .offer-warning {  border-color: #f0ad4e; }
  .offer-warning .shape{
    border-color: transparent #f0ad4e transparent transparent;
    border-color: rgba(255,255,255,0) #f0ad4e rgba(255,255,255,0) rgba(255,255,255,0);
  }

  .shape-text{
    color:#fff; font-size:12px; font-weight:bold; position:relative; right:-40px; top:2px; white-space: nowrap;
    -ms-transform:rotate(30deg); /* IE 9 */
    -o-transform: rotate(360deg);  /* Opera 10.5 */
    -webkit-transform:rotate(30deg); /* Safari and Chrome */
    transform:rotate(30deg);
  } 
  .offer-content{
    padding:0 20px 10px;
  }
</style>

<style>
  
  .rectangle {
    height: 125px;
    width: 165px;
    background-color: #CCFF99;
  }
  .rectangle2 {
    height: 125px;
    width: 165px;
    background-color: #FDFD99;
  }
  .rectangle3 {
    height: 125px;
    width: 165px;
    background-color: #581A7C;
  }
  .rectangle4 {
    height: 125px;
    width: 165px;
    background-color: #FFCCE5;
  }
  .rectangle5 {
    height: 125px;
    width: 165px;
    background-color: #99CCFF;
  }
</style>
<style>
	.btn-circle {
	width: 25px;
	height: 25px;
	text-align: center;
	padding: 2px 0;
	font-size: 14px;
	line-height: 1.428571429;
	border-radius: 15px;
	}
	.btn-circle.btn-lg {
	width: 50px;
	height: 50px;
	padding: 10px 16px;
	font-size: 18px;
	line-height: 1.33;
	border-radius: 25px;
	}
	.btn-circle.btn-xl {
	width: 70px;
	height: 70px;
	padding: 10px 16px;
	font-size: 24px;
	line-height: 1.33;
	border-radius: 35px;
	}
</style>
<?php
	function DateThai($strDate)
		{

		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//, $strHour:$strMinute
	}
  ?>
  <?php 
		date_default_timezone_set('Asia/Bangkok');
		$date = date("Y-m-d");
		$time = date("H:i:s");

		function DateDiff($strDate1,$strDate2){                             
		return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
		}
		
  ?>
  <style>
     @media only screen and (max-width: 1280px) {
   .v1 { 
      width: 20%;
      }
    }
    @media only screen and (max-width: 400px) {
   .v1 { 
      width: 70%;
      }
    }
    @media only screen and (max-width: 350px) {
      .v1 { 
        width: 70%;
      }   
    }
    @media only screen and (max-width: 315px) {
      .v1 { 
        width: 70%;
      }    
    }
    @media only screen and (max-width: 295px) {
      .v1 { 
        width: 70%;
      }    
    }
    .containers{
        padding:5%;
    }
    .containers .img{
        text-align:center;
    }
    .containers .details{
        border-left:3px solid #ded4da;
    }
    .containers .details p{
        font-size:15px;
        font-weight:bold;
    }
  </style>
 
 <div class="containers">
  <div class="row">
    <div class="col-md-6 img">
          <?php 
            
            $sSubPre = '';
            if ($UserName['MisSubPrename']['id'] != null) {
              $sSubPre = $sSubPre . $UserName['MisSubPrename']['name_short_th'] . ' ';
            }
            if ($sSubPre == '') {
              $sSubPre = $sSubPre . $UserName['MisPrename']['name_full_th'] . ' ';
            } 
          ?> 
           <img class="img-rounded pull-right" src="https://mis.agri.cmu.ac.th/mis/img/employee_profile/<?php echo $UserName['MisEmployee']['photo'] ?>"  style=" height: 230px;"  alt=""  />
         
       <!-- <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRvzOpl3-kqfNbPcA_u_qEZcSuvu5Je4Ce_FkTMMjxhB-J1wWin-Q"  alt="" class="img-rounded"> -->
    </div>
    <div class="col-md-6 details">
       
      <font class="font_panel7"><b> <?php echo $sSubPre.''.$UserName['MisEmployee']['fname_th'].' '.$UserName['MisEmployee']['lname_th']; ?></b></font><br>
           <?php echo $misPositions['MisPosition']['name_th']  ?> 
       <br><br>
         <table>
           <tr>
             <td><b>E-mail CMU </b></td>
             <td><?php echo $UserName['MisEmployee']['email_cmu']  ?></td>
           </tr>
           <tr>
             <td><b>E-mail Other </b></td>
             <td><?php echo $UserName['MisEmployee']['email_other']  ?></td>
           </tr>
            
            
         </table>
       
    </div>
  </div>
</div>

<div class="container-fluid">
  <div class="row content">   
    <div class="col-md-12">
    
      <div class="row" >
        <div class= 'col-md-12'>
          
          <?php if ($admins['Admincurriculumrequest']['employee_status_id'] == 1) { ?>
           <!-- ****** -->
            <div class="container-fluid">
              <div class="col-md-12">
                
                <center>
                    <div class="row" >
                        <div class="col-md-6">
                          <a href="<?php echo $this->Html->url(array('controller' => 'specialandcooperatives','action' => 'list_result_manage_proposal',1)); ?>" >
                            <div class="offer offer-success" style="height: 152px;width:400px;" style="background-color: #FFCCE5!important;border-color: #FFCCE5!important">
                                <div class="rectangle4 pull-right" >
                                  <font color="black">  มคอ. 3-4  
                                        <?php if ($admins['Admincurriculumrequest']['employees_status_id'] == 0) { ?>
                                          <?php if ($countAllSpecialAndCooperatives1 == 0) { }else { ?>
                                              <button type="button" class="btn btn-danger btn-circle btn-lg"><?php echo $countAllSpecialAndCooperatives1 ?></button>															
                                            <?php } ?>
                                        <?php }elseif ($admins['Admincurriculumrequest']['employees_status_id'] == 1) {?>
                                          <?php if ($countAllSpecialAndCooperatives1 == 0) { }else { ?>
                                              <button type="button" class="btn btn-danger btn-circle btn-lg"><?php echo $countAllSpecialAndCooperatives1 ?></button>															
                                            <?php } ?>
                                        <?php }elseif ($admins['Admincurriculumrequest']['employees_status_id'] == 2) {?>

                                        <?php } ?> 
                                        
                                        
                                        
                                    </font>      
                                </div>
                              
                                <div class="offer-content font_panel2">
                                    <p style=" padding:25px 0 0;">
                                      
                                      <span class="glyphicon glyphicon-tasks" style="font-size:30px"></span> 
                                        เสนอ มคอ. 3-4 ปริญญาตรี
                                    </p>
                                    
                                </div>
                              </div>
                            </a>
                        </div>

                        <div class="col-md-6">
                          <a href="<?php echo $this->Html->url(array('controller' => 'specialandcooperatives','action' => 'list_result_manage_proposal',3)); ?>" >
                            <div class="offer offer-success" style="height: 152px;width:400px;" style="background-color: #FFCCE5!important;border-color: #FFCCE5!important">
                                <div class="rectangle4 pull-right" >
                                  <font color="black">  มคอ. 3-4  
                                        <?php if ($admins['Admincurriculumrequest']['employees_status_id'] == 0) { ?>
                                            <?php if ($countAllSpecialAndCooperatives3 == 0) { }else { ?>
                                              <button type="button" class="btn btn-danger btn-circle btn-lg"><?php echo $countAllSpecialAndCooperatives3 ?></button>															
                                            <?php } ?> 
                                        <?php }elseif ($admins['Admincurriculumrequest']['employees_status_id'] == 1) {?>
                                          
                                        <?php }elseif ($admins['Admincurriculumrequest']['employees_status_id'] == 2) {?>
                                          <?php if ($countAllSpecialAndCooperatives3 == 0) { }else { ?>
                                            <button type="button" class="btn btn-danger btn-circle btn-lg"><?php echo $countAllSpecialAndCooperatives3 ?></button>															
                                          <?php } ?> 
                                        <?php } ?> 
                                    </font>      
                                </div>
                              
                                <div class="offer-content font_panel2">
                                    <p style=" padding:25px 0 0;">
                                      
                                      <span class="glyphicon glyphicon-tasks" style="font-size:30px"></span> 
                                        เสนอ มคอ. 3-4 ปริญญาโท
                                    </p>
                                    
                                </div>
                              </div>
                            </a>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-md-6">
                          <a href="<?php echo $this->Html->url(array('controller' => 'specialandcooperatives','action' => 'list_result_manage_proposal',5)); ?>" >
                            <div class="offer offer-success" style="height: 152px;width:400px;" style="background-color: #FFCCE5!important;border-color: #FFCCE5!important">
                                <div class="rectangle4 pull-right" >
                                  <font color="black">มคอ. 3-4 
                                        <?php if ($admins['Admincurriculumrequest']['employees_status_id'] == 0) { ?>
                                          <?php if ($countAllSpecialAndCooperatives5 == 0) { }else { ?>
                                            <button type="button" class="btn btn-danger btn-circle btn-lg"><?php echo $countAllSpecialAndCooperatives5 ?></button>															
                                          <?php } ?> 
                                        <?php }elseif ($admins['Admincurriculumrequest']['employees_status_id'] == 1) {?>
                                          
                                        <?php }elseif ($admins['Admincurriculumrequest']['employees_status_id'] == 2) {?>
                                          <?php if ($countAllSpecialAndCooperatives5 == 0) { }else { ?>
                                            <button type="button" class="btn btn-danger btn-circle btn-lg"><?php echo $countAllSpecialAndCooperatives5 ?></button>															
                                          <?php } ?> 
                                        <?php } ?> 
                                        
                                       
                                        
                                    </font>      
                                </div>
                              
                                <div class="offer-content font_panel2">
                                    <p style=" padding:25px 0 0;">
                                      
                                      <span class="glyphicon glyphicon-tasks" style="font-size:30px"></span> 
                                      เสนอ มคอ. 3-4 ปริญญาเอก
                                    </p>
                                    
                                </div>
                              </div>
                            </a>
                        </div>
                        <div class="col-md-6">
                          <a href="<?php echo $this->Html->url(array('controller' => 'specialandcooperatives','action' => 'list_result_pass_proposal')); ?>" >
                            <div class="offer offer-success" style="height: 152px;width:400px;" >
                                <div class="rectangle pull-right" >
                                  <font color="black"> มคอ. 3-4   
                                        <!-- <?php if ($countproposals == 0) { }else { ?>
                                          <button type="button" class="btn btn-danger btn-circle btn-lg"><?php echo $countproposals ?></button>															
                                        <?php } ?> </b>  -->
                                        
                                    </font>      
                                </div>
                                <div class="offer-content font_panel3">
                                    <p style="font-size:26px; padding:25px 0 0;">
                                      
                                      <span class="glyphicon glyphicon-ok-sign" style="font-size:30px"></span> 
                                     มคอ. 3-4 <br>  ฉบับสมบูรณ์ 
                                    </p>
                                    
                                </div>
                                 
                              </div>
                            </a>
                        </div>
                         
                    </div>
                    <div class="row" >
                        <div class="col-md-6">
                          <a href="<?php echo $this->Html->url(array('controller' => 'specialandcooperatives','action' => 'info_page_full',226)); ?>" target="_blank">
                            <div class="offer offer-success" style="height: 152px;width:400px;" style="background-color: #99CCFF!important;border-color: #99CCFF!important">
                                <div class="rectangle5 pull-right" >
                                  <font color="black">ดาวน์โหลด
                                          
                                    </font>      
                                </div>
                              
                                <div class="offer-content font_panel2">
                                    <p style=" padding:25px 0 0;">
                                      
                                      <span class="glyphicon glyphicon-tasks" style="font-size:30px"></span> 
                                      ดาวน์โหลดแบบฟอร์ม มคอ.3-4
                                    </p>
                                    
                                </div>
                              </div>
                            </a>
                        </div>
                        <div class="col-md-6">
                          <!-- <a href="<?php echo $this->Html->url(array('controller' => 'specialandcooperatives','action' => 'list_result_pass_proposal')); ?>" >
                            <div class="offer offer-success" style="height: 152px;width:400px;" >
                                <div class="rectangle pull-right" >
                                  <font color="black"> มคอ. 3-4   
                                        
                                        
                                    </font>      
                                </div>
                                <div class="offer-content font_panel3">
                                    <p style="font-size:26px; padding:25px 0 0;">
                                      
                                      <span class="glyphicon glyphicon-ok-sign" style="font-size:30px"></span> 
                                     มคอ. 3-4 <br>  ฉบับสมบูรณ์ 
                                    </p>
                                    
                                </div>
                                 
                              </div>
                            </a> -->
                        </div>
                         
                    </div>
                      
                </center>
              </div>
            </div>
           
          <?php }elseif($admins['Admincurriculumrequest']['employee_status_id'] == 2){ ?>
            <div class="container-fluid">
               
                 
                <div class="col-md-12"> 
                
                  <center>
                      <div class="row" >
                          <div class="col-md-6">
                            <a href="<?php echo $this->Html->url(array('controller' => 'specialandcooperatives','action' => 'list_result_manage_proposal',1)); ?>" >
                              <div class="offer offer-success" style="height: 152px;width:400px;" style="background-color: #FFCCE5!important;border-color: #FFCCE5!important">
                                  <div class="rectangle4 pull-right" >
                                    <font color="black"> &nbsp;&nbsp;<b>มคอ. 3-4  
                                          <?php if ($countAllSpecialAndCooperatives1 == 0) { }else { ?>
                                            <button type="button" class="btn btn-danger btn-circle btn-lg"><?php echo $countAllSpecialAndCooperatives1 ?></button>															
                                          <?php } ?> 
                                          </b> 
                                          
                                      </font>      
                                  </div>
                                
                                  <div class="offer-content font_panel2">
                                      <p style=" padding:65px 0 0;">
                                        
                                        <span class="glyphicon glyphicon-tasks" style="font-size:36px"></span> 
                                        เสนอ มคอ. 3-4 ปริญญาตรี
                                      </p>
                                      
                                  </div>
                                </div>
                              </a>
                          </div>

                          <div class="col-md-6">
                            <a href="<?php echo $this->Html->url(array('controller' => 'specialandcooperatives','action' => 'list_result_manage_proposal',3)); ?>" >
                              <div class="offer offer-success" style="height: 152px;width:400px;" style="background-color: #FFCCE5!important;border-color: #FFCCE5!important">
                                  <div class="rectangle4 pull-right" >
                                    <font color="black"> &nbsp;&nbsp;<b>มคอ. 3-4  
                                          <?php if ($countAllSpecialAndCooperatives3 == 0) { }else { ?>
                                            <button type="button" class="btn btn-danger btn-circle btn-lg"><?php echo $countAllSpecialAndCooperatives3 ?></button>															
                                          <?php } ?> </b> 
                                          
                                      </font>      
                                  </div>
                                
                                  <div class="offer-content font_panel2">
                                      <p style=" padding:65px 0 0;">
                                        
                                        <span class="glyphicon glyphicon-tasks" style="font-size:36px"></span> 
                                        เสนอ มคอ. 3-4 ปริญญาโท
                                      </p>
                                      
                                  </div>
                                </div>
                              </a>
                          </div>
                      </div>
                      <div class="row" >
                          <div class="col-md-6">
                            <a href="<?php echo $this->Html->url(array('controller' => 'specialandcooperatives','action' => 'list_result_manage_proposal',5)); ?>" >
                              <div class="offer offer-success" style="height: 152px;width:400px;" style="background-color: #FFCCE5!important;border-color: #FFCCE5!important">
                                  <div class="rectangle4 pull-right" >
                                    <font color="black"> &nbsp;&nbsp;<b>มคอ. 3-4  
                                          <?php if ($countAllSpecialAndCooperatives5 == 0) { }else { ?>
                                            <button type="button" class="btn btn-danger btn-circle btn-lg"><?php echo $countAllSpecialAndCooperatives5 ?></button>															
                                          <?php } ?> </b> 
                                          
                                      </font>      
                                  </div>
                                
                                  <div class="offer-content font_panel2">
                                      <p style=" padding:65px 0 0;">
                                        
                                        <span class="glyphicon glyphicon-tasks" style="font-size:36px"></span> 
                                        เสนอ มคอ. 3-4 ปริญญาเอก
                                      </p>
                                      
                                  </div>
                                </div>
                              </a>
                          </div>
                          <div class="col-md-6">
                            <a href="<?php echo $this->Html->url(array('controller' => 'specialandcooperatives','action' => 'list_result_pass_proposal')); ?>" >
                              <div class="offer offer-success" style="height: 152px;width:400px;" >
                                  <div class="rectangle pull-right" >
                                    <font color="black"> &nbsp;&nbsp;มคอ. 3-4   
                                          <!-- <?php if ($countproposals == 0) { }else { ?>
                                            <button type="button" class="btn btn-danger btn-circle btn-lg"><?php echo $countproposals ?></button>															
                                          <?php } ?> </b>  -->
                                          
                                      </font>      
                                  </div>
                                  <div class="offer-content font_panel3">
                                      <p style="font-size:26px; padding:65px 0 0;">
                                        
                                        <span class="glyphicon glyphicon-ok-sign" style="font-size:36px"></span> 
                                        รายการ มคอ. 3-4 ฉบับสมบูรณ์ 
                                      </p>
                                      
                                  </div>
                                   
                                </div>
                              </a>
                          </div>
                           
                      </div>
                      <div class="row" >
                        <div class="col-md-6">
                          <a href="<?php echo $this->Html->url(array('controller' => 'specialandcooperatives','action' => 'info_page_full',226)); ?>" target="_blank">
                            <div class="offer offer-success" style="height: 152px;width:400px;" style="background-color: #99CCFF!important;border-color: #99CCFF!important">
                                <div class="rectangle5 pull-right" >
                                  <font color="black">ดาวน์โหลด
                                          
                                    </font>      
                                </div>
                              
                                <div class="offer-content font_panel2">
                                    <p style=" padding:25px 0 0;">
                                      
                                      <span class="glyphicon glyphicon-tasks" style="font-size:30px"></span> 
                                      ดาวน์โหลดแบบฟอร์ม มคอ.3-4
                                    </p>
                                    
                                </div>
                              </div>
                            </a>
                        </div>
                        <div class="col-md-6">
                          <!-- <a href="<?php echo $this->Html->url(array('controller' => 'specialandcooperatives','action' => 'list_result_pass_proposal')); ?>" >
                            <div class="offer offer-success" style="height: 152px;width:400px;" >
                                <div class="rectangle pull-right" >
                                  <font color="black"> มคอ. 3-4   
                                        
                                        
                                    </font>      
                                </div>
                                <div class="offer-content font_panel3">
                                    <p style="font-size:26px; padding:25px 0 0;">
                                      
                                      <span class="glyphicon glyphicon-ok-sign" style="font-size:30px"></span> 
                                     มคอ. 3-4 <br>  ฉบับสมบูรณ์ 
                                    </p>
                                    
                                </div>
                                 
                              </div>
                            </a> -->
                        </div>
                         
                    </div> 
                  </center>
                </div>
               
            </div>
          <?php }elseif($admins['Admincurriculumrequest']['employee_status_id'] == 3){ ?>
            <div class="container-fluid">
               
                 
                <div class="col-md-12"> 
                
                  <center>
                      <div class="row" >
                          <div class="col-md-6">
                            <a href="<?php echo $this->Html->url(array('controller' => 'specialandcooperatives','action' => 'list_result_manage_proposal',1)); ?>" >
                              <div class="offer offer-success" style="height: 152px;width:400px;" style="background-color: #FFCCE5!important;border-color: #FFCCE5!important">
                                  <div class="rectangle4 pull-right" >
                                    <font color="black"> &nbsp;&nbsp;<b>มคอ. 3-4  
                                          <?php if ($countAllSpecialAndCooperatives1 == 0) { }else { ?>
                                            <button type="button" class="btn btn-danger btn-circle btn-lg"><?php echo $countAllSpecialAndCooperatives1 ?></button>															
                                          <?php } ?> 
                                          </b> 
                                          
                                      </font>      
                                  </div>
                                
                                  <div class="offer-content font_panel2">
                                      <p style=" padding:65px 0 0;">
                                        
                                        <span class="glyphicon glyphicon-tasks" style="font-size:36px"></span> 
                                        เสนอ มคอ. 3-4 ปริญญาตรี
                                      </p>
                                      
                                  </div>
                                </div>
                              </a>
                          </div>

                          <div class="col-md-6">
                            <a href="<?php echo $this->Html->url(array('controller' => 'specialandcooperatives','action' => 'list_result_manage_proposal',3)); ?>" >
                              <div class="offer offer-success" style="height: 152px;width:400px;" style="background-color: #FFCCE5!important;border-color: #FFCCE5!important">
                                  <div class="rectangle4 pull-right" >
                                    <font color="black"> &nbsp;&nbsp;<b>มคอ. 3-4  
                                          <?php if ($countAllSpecialAndCooperatives3 == 0) { }else { ?>
                                            <button type="button" class="btn btn-danger btn-circle btn-lg"><?php echo $countAllSpecialAndCooperatives3 ?></button>															
                                          <?php } ?> </b> 
                                          
                                      </font>      
                                  </div>
                                
                                  <div class="offer-content font_panel2">
                                      <p style=" padding:65px 0 0;">
                                        
                                        <span class="glyphicon glyphicon-tasks" style="font-size:36px"></span> 
                                        เสนอ มคอ. 3-4 ปริญญาโท
                                      </p>
                                      
                                  </div>
                                </div>
                              </a>
                          </div>
                      </div>
                      <div class="row" >
                          <div class="col-md-6">
                            <a href="<?php echo $this->Html->url(array('controller' => 'specialandcooperatives','action' => 'list_result_manage_proposal',5)); ?>" >
                              <div class="offer offer-success" style="height: 152px;width:400px;" style="background-color: #FFCCE5!important;border-color: #FFCCE5!important">
                                  <div class="rectangle4 pull-right" >
                                    <font color="black"> &nbsp;&nbsp;<b>มคอ. 3-4  
                                          <?php if ($countAllSpecialAndCooperatives5 == 0) { }else { ?>
                                            <button type="button" class="btn btn-danger btn-circle btn-lg"><?php echo $countAllSpecialAndCooperatives5 ?></button>															
                                          <?php } ?> </b> 
                                          
                                      </font>      
                                  </div>
                                
                                  <div class="offer-content font_panel2">
                                      <p style=" padding:65px 0 0;">
                                        
                                        <span class="glyphicon glyphicon-tasks" style="font-size:36px"></span> 
                                        เสนอ มคอ. 3-4 ปริญญาเอก
                                      </p>
                                      
                                  </div>
                                </div>
                              </a>
                          </div>
                          <div class="col-md-6">
                            <a href="<?php echo $this->Html->url(array('controller' => 'specialandcooperatives','action' => 'list_result_pass_proposal')); ?>" >
                              <div class="offer offer-success" style="height: 152px;width:400px;" >
                                  <div class="rectangle pull-right" >
                                    <font color="black"> &nbsp;&nbsp;มคอ. 3-4   
                                          <!-- <?php if ($countproposals == 0) { }else { ?>
                                            <button type="button" class="btn btn-danger btn-circle btn-lg"><?php echo $countproposals ?></button>															
                                          <?php } ?> </b>  -->
                                          
                                      </font>      
                                  </div>
                                  <div class="offer-content font_panel3">
                                      <p style="font-size:26px; padding:65px 0 0;">
                                        
                                        <span class="glyphicon glyphicon-ok-sign" style="font-size:36px"></span> 
                                        รายการ มคอ. 3-4 ฉบับสมบูรณ์ 
                                      </p>
                                      
                                  </div>
                                   
                                </div>
                              </a>
                          </div>
                           
                      </div>
                      <div class="row" >
                        <div class="col-md-6">
                          <a href="<?php echo $this->Html->url(array('controller' => 'specialandcooperatives','action' => 'info_page_full',226)); ?>" target="_blank">
                            <div class="offer offer-success" style="height: 152px;width:400px;" style="background-color: #99CCFF!important;border-color: #99CCFF!important">
                                <div class="rectangle5 pull-right" >
                                  <font color="black">ดาวน์โหลด
                                          
                                    </font>      
                                </div>
                              
                                <div class="offer-content font_panel2">
                                    <p style=" padding:25px 0 0;">
                                      
                                      <span class="glyphicon glyphicon-tasks" style="font-size:30px"></span> 
                                      ดาวน์โหลดแบบฟอร์ม มคอ.3-4
                                    </p>
                                    
                                </div>
                              </div>
                            </a>
                        </div>
                        <div class="col-md-6">
                          <!-- <a href="<?php echo $this->Html->url(array('controller' => 'specialandcooperatives','action' => 'list_result_pass_proposal')); ?>" >
                            <div class="offer offer-success" style="height: 152px;width:400px;" >
                                <div class="rectangle pull-right" >
                                  <font color="black"> มคอ. 3-4   
                                        
                                        
                                    </font>      
                                </div>
                                <div class="offer-content font_panel3">
                                    <p style="font-size:26px; padding:25px 0 0;">
                                      
                                      <span class="glyphicon glyphicon-ok-sign" style="font-size:30px"></span> 
                                     มคอ. 3-4 <br>  ฉบับสมบูรณ์ 
                                    </p>
                                    
                                </div>
                                 
                              </div>
                            </a> -->
                        </div>
                         
                    </div> 
                  </center>
                </div>
               
            </div>    
          <?php } ?>

          </div>
      </div>
    </div>
  </div>
</div>

