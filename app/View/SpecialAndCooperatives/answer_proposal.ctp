<?php //echo debug($this->request->data)?>
<!-- start content here -->
<div class="container" style="padding:15px 0 0;">
  <div class="row">
  <div class="col-md-12">
      <div class="panel panel-primary">
				<div class="panel-heading"> 
					รายการที่เสนอหัวข้อโครงร่างวิทยานิพนธ์และการค้นคว้าอิสระ
				</div>
					<div class="panel-body">
						<a class="btn btn-default dropdown-toggle btn-primary" href="<?php echo $this->Html->url(array('action' => 'list_proposal_all')); ?>">
							ย้อนกลับ
							<span class="glyphicon glyphicon-home"></span>			
						</a>			
						
					</div>
				</div>
			</div>
      </div>
	<div class="panel panel-default" style="border-color: #F2F2F2!important">
		<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<div class="panel-title">
				<h3 style="margin:0px !important;">
				<center> 
						<div class="font_panel2">
							รายการที่เสนอหัวข้อโครงร่างวิทยานิพนธ์/การค้นคว้าอิสระ
						</div>
					</center>
				</h3> 
			</div>
		</div>	
		<div class="panel-body" style="padding-top: 15px; ">
			<div class="row" align="center"> 	
				<div class="font_panel5">	
				<h3>ระบบเสนอหัวข้อโครงร่างวิทยานิพนธ์/การค้นคว้าอิสระ</h3>
				
				<!-- <h4>18-21 พศจิกายน 2561 ณ โรงแรมเชียงใหม่แกรนด์วิว จังหวัดเชียงใหม่</h4> -->
				
				</div>
			</div> 
            <tbody>
              <?php	
                  $Students = $this->Session->read('Students');                     
                  $i=1;
                  foreach ($proposals as $proposal){ ?> 
                      <table class="table table-hovered">
                        <tr>
                          <td>ชื่อหัวข้อภาษาไทย </td>
                          <td><?php echo $proposal['Proposal']['title']; ?></td>
                        </tr>
                        <tr>
                          <td>ชื่อหัวข้อภาษาอังกฤษ</td>
                          <td> <?php echo $proposal['Proposal']['title_eng']; ?></td>
                        </tr>
                        <tr>
                          <td>วัตถุประสงค์</td>
                          <td><?php echo $proposal['Proposal']['objective']; ?></td>
                        </tr>
                        <tr>
                            <td>สถานะหัวข้อ</td>
                            <td>
                            <?php
                              if ($proposal['Proposal']['pass_id'] == 1) { ?>
                                <span class="label label-danger"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> <?php echo $proposal['Pass']['name']; ?> </span>	
                              <?php	}elseif ($proposal['Proposal']['pass_id'] == 4){ ?>			
                                <span class="label label-success"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>  <?php echo $proposal['Pass']['name']; ?></span> 
                              <?php	}else{ ?>		
                                <span class="label label-warning"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>  <?php echo $proposal['Pass']['name']; ?></span> 
                              <?php } ?><br>
                            </td>
                        </tr> 
                        <tr>
                            <td>สถานะการติดตาม</td>
                            <td>
                            <?php
                                  if ($proposal['Proposal']['proposal_follow_status_id'] == 1) { ?>
                                    <span class="label label-danger"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> <?php echo $proposal['ProposalFollowStatus']['name']; ?> </span>	
                            <?php	}elseif ($proposal['Proposal']['proposal_follow_status_id'] == 10){ ?>			
                                    <span class="label label-success"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>  <?php echo $proposal['ProposalFollowStatus']['name']; ?></span> 
                                                
                                  <?php	}else{ ?>			
                                    <span class="label label-warning"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span>  <?php echo $proposal['ProposalFollowStatus']['name']; ?></span> 
                                  
                            <?php } ?><br>
                            </td>
                        </tr>                                       
                        <tr>
                          <td>เอกสารแนบ</td>
                          <td> 
                            <table class="table table-hovered" border='1'>                    
                              <?php echo $output; ?>
                            </table>
                          </td>
                        </tr>
                        
                      </table> 
                       	                 
                      <?php  $i++;  } ?>
                      
                          <!-- <?php if ($proposal['Proposal']['pass_id'] == 1) { }else {  ?>
                                        <div class="col-sm-12">
                                          เพื่อตอบกลับ กรุณาอัพโหลดสามไฟล์ ดังนี้ <br>
                                          1. ไฟล์ที่แก้ไขแล้ว .docx <br>
                                          2. ไฟล์ที่แก้ไขแล้ว .pdf <br>                               
                                      
                                      
                                        </div> 
                                      <form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">	                              
                                      <?php 
                                          $Students = $this->Session->read('Students');
                                          $required = 'กรุณากรอกข้อมูล';
                                          
                                          echo $this->Form->create('Proposal',array('role' => 'form','data-toggle' => 'validator')); 
                                          //คอมเม้นตรงนี้
                                          echo $this->Form->hidden('Proposal.id', array(
                                            'value' => $proposal_id,
                                            'data-error' => $required,
                                            'label' => false,
                                            'class' => 'form-control'
                                          ));
                                      
                                      ?>  
                                      <div class="row">				  
                                        <div class="form-group col-md-12">
                                        
                                        <label>แนบไฟล์หัวข้อโครงร่างฯที่แก้ไข </label>
                                        <?php
                                            echo $this->Form->input('ProposalDocument.files.', array('type' => 'file', 'multiple','accept' => '.doc, .docx, .txt, .pdf, .zip, .rar'));
                                          ?>
                                        <span style="color: red;">* จำเป็นต้องกรอกให้ครบ</span>
                                        <div class="help-block with-errors"></div>	
                                        </div>				  
                                      </div>		
                                      
                                      <div class="row">
                                        <div class="col-md-12">
                                          <input type="submit" value="คลิกส่งเอกสารที่แก้ไข" class="btn btn-success btn-lg btn-block"> 
                                        </div>
                                      </div>
                                    </form>
                            <?php	} ?> -->
                           
                        </div>
                      </div>
                    

                   
                   <?php	
                          $i=1;
                          foreach ($answers as $answer){
                            ?>
                            
                            <?php
                              if ($answer['AnswerProposal']['status'] == 1) {
                                # code...
                              

                            ?>

                              <div class="panel panel-default" style="border-color: #d9edf7!important">
                                  <div class="panel-heading"  style="background-color: #d9edf7!important;border-color: #d9edf7!important"> 
                                        <h3 class="panel-title">ตอบกลับครั้งที่  <?php echo $i; ?></h3> 
                                      </div>
                                      <div class="panel-body">
                                        <tbody>
                                          <div class="col-sm-12">
                                              <div class="form-group has-feedback">      
                                                   <?php echo $answer['AnswerProposal']['Detail']; ?>
                                              </div>
                                              <div class="col-sm-12">
                                               
                                                <?php //debug($answer['AnswerProposalDocument']);

                                                if($answer['AnswerProposalDocument'] != array()){

                                                   echo '<br><label><span class="glyphicon glyphicon-list-alt"></span>เอกสารแนบ</label>';
  
                                                  foreach ($answer['AnswerProposalDocument'] as $key) {
                                                     $fileAction = $this->webroot.'files/students/proposals/'.$answer['AnswerProposal']['student_id'].'/'.$key['name'] ;
                                                    echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="'.$fileAction.'" class="btn btn-primary" target="_blank">'.$key['name'].' <span class="glyphicon glyphicon-download-alt"></span></a>';
                                                  }
                                                }


                                                 ?>
                                              </div>
                                          </div>                                          
                                      <div class="col-sm-12">
                                        <div class="form-group has-feedback">
                                                   <blockquote>
                                                   <h4><i class="fa fa-user-circle-o" aria-hidden="true"></i> <b>ตอบกลับโดย :</b><?php echo $answer['AnswerProposal']['PostName']; ?></h4>  <small><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $answer['AnswerProposal']['created']; ?></small>
                                                  </blockquote>      
                                        </div>
                                      </div>
                              </div>
                                                                                      
                                            
                      </div>
                             
                        <?php }else{
                          ?>
                      <div class="panel panel-default" style="border-color: #DABAFA!important">
                                  <div class="panel-heading" style="background-color: #DABAFA!important;border-color: #DABAFA!important"> 
                                        <h3 class="panel-title">ตอบกลับครั้งที่  <?php echo $i; ?></h3> 
                                      </div>
                                      <div class="panel-body">
                                        <tbody>                                           
                                         
                                             <div class="col-sm-12">
                                                   <div class="form-group has-feedback">      
                                                   <?php echo $answer['AnswerProposal']['Detail']; ?>
                                              </div>
                                          </div>                                         
                                           <div class="col-sm-12">
                                                   <div class="form-group has-feedback">
                                                   <blockquote>
                                                   <h4><i class="fa fa-user-circle-o" aria-hidden="true"></i> <b>ตอบกลับโดย :</b><?php echo $answer['AnswerProposal']['PostName']; ?></h4>  <small><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $answer['AnswerProposal']['created']; ?></small>
                                                  </blockquote>      
                                                   
                                                  </div>
                                      </div>
                              </div>
                                                                                      
                                            
                      </div>

                          <?php
                        }
                        $i++;  
                      } ?>
                                              


                  <?php  if ($proposal['Proposal']['proposal_follow_status_id'] != 10) { ?>
                        <!-- <center>
                          <a class="btn btn-success btn-lg" href="<?php echo $this->Html->url(array('action' => 'attach_proposal',
                            $proposal['Proposal']['id'])); ?>" role="button" >เพิ่มเอกสารเพิ่มเติมหรือที่ต้องการแก้ไข 
                            
                          </a>											
                      </center> -->
                  <?php } ?>
                  <?php  if ($proposals[0]['Proposal']['proposal_follow_status_id'] != 10) { ?>
                    <!-- <form method="post" accept-charset="utf-8">
                      <div class="panel panel-success">
                          <div class="panel-heading"> 
                            <h3 class="panel-title"><i class="glyphicon glyphicon-plus"></i> ตอบกลับไปยังเจ้าหน้าที่</h3> 
                          </div>
                          <div class="panel-body">
                            <tbody> 
                                  <?php 
                                        echo $this->Form->hidden('AnswerProposal.proposal_id', array(
                                          'value' => $proposal_id,
                                          'class' => 'form-control',
                                        ));

                                        echo $this->Form->hidden('AnswerProposal.status', array(
                                          'value' => 1,
                                          'class' => 'form-control',
                                        ));
                                  ?>    
                                <label class="col-sm-3 control-label">รายละเอียด <font color="red">*</font> </label>
                                    <div class="col-sm-7">
                                          <div class="form-group has-feedback">      
                                            <?php 
                                                  echo $this->Form->input('AnswerProposal.Detail', array(
                                                      'type' => 'textarea',
                                                      'label' => false,
                                                      'div' => false,
                                                      'class' => 'ckeditor',                                                           
                                                      'error' => false,
                                                      'required'
                                                      ));
                                              
                                                ?>
                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                </div>
                                <label class="col-sm-3 control-label">ชื่อผู้ใช้ที่ตอบกลับโดย  </label>
                                    <div class="col-sm-7">
                                          <div class="form-group has-feedback">      
                                          <?php
                                            $Students = $this->Session->read('Students');
                                            echo $Students['Student']['student_title'];?><?php echo $Students['Student']['student_firstname'];?> <?php echo $Students['Student']['student_surname'];?>
                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div>
                                </div> 
                                <div class="form-group">
                                        <label class="col-sm-3 control-label"></label>
                                        <div class="col-sm-7">
                                <input type="submit" class="btn btn-success" value="บันทึกข้อมูล" onclick="return confirm('ยืนยันการเพิ่มข้อมูล')">
                                        
                            </div>
                          </tbody>
                        </div>
                      </div>                 
                    </form>       -->
                  <?php }else {
                    echo '
                    <center>
                      <font class="text-success">
                        <span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span> หัวข้อของนักศึกษาได้รับการเสนอแต่งตั้งคณะกรรมการที่ปรึกษาเรียบร้อยแล้ว 
                      </font>
                    </center>
                    ';
                  } ?>
        
                
              </tbody>
            </div> 
            <!-- /container -->  
                                  
          

             </div>
          </div>
        </div>
      </div>
    </div>
  </div>   
<div style="clear: both;"></div>