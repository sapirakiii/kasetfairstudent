<?php //echo debug($listSearchStudents);?>strMinuteSpecialAndCooperative
<?php
	function DateThai($strDate)
		{

		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//, $strHour:$strMinute
	}
	?>

		
	<div class="container">
		<div class="row">
			<div class="col-md-12">
					<!-- Nav tabs -->
					<!-- <ul class="nav nav-tabs" role="tablist" >
						<li role="presentation" class="active " style="font-family:"baijam";">
						<a href="#news" aria-controls="news" role="tab" data-toggle="tab">
						<i class="glyphicon glyphicon-th-list"></i> <b>จำนวนผู้ที่เสนอหัวข้อโครงร่างฯ  </b></a></li>

					</ul> -->

				<!-- Tab panes -->
				<!-- <div class="tab-content" style="padding:10px;">
					<div role="tabpanel" class="tab-pane active" id="news">
						<div class="row">				
						    
							   	<table class="table table-bordered" >
									<tr>
										<td colspan="6" class="warning" >
											<center><b>รายการแสดงหัวข้อโครงร่างวิทยานิพนธ์/การค้นคว้าแบบอิสระสำหรับนักศึกษาระดับบัณฑิตศึกษา</b></center>
										</td>
									</tr>
									<tr>
										
										<td rowspan="2" style="text-align: center;padding-top: 2%;"><center>ปีการศึกษา</center></td>	
										<td colspan="2"><center><b>จำนวนหัวข้อระดับปริญญาโท</b></center></td>
										<td colspan="2"><center><b>จำนวนหัวข้อระดับปริญญาเอก</b></center></td>
										<td rowspan="2" style="text-align: center;padding-top: 2%;"><center>รวมจำนวนหัวข้อนักศึกษา</center></td>
									
									</tr>
									<tr>				
										<td ><center>วิทยานิพนธ์</center></td>
										<td ><center>การค้นคว้าอิสระ </center></td>
										<td ><center>วิทยานิพนธ์</center></td>
										<td ><center>การค้นคว้าอิสระ </center></td>				
									
									</tr>
								
									<?php echo $output; ?>
									
										
								</table>
							
							 

						</div>				
					</div>
				</div> -->
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist" style="margin-top:40px;">
					<li role="presentation" class="active " style="font-family:"baijam";">
					<a href="#news" aria-controls="news" role="tab" data-toggle="tab">
					<i class="glyphicon glyphicon-th-list"></i> <b>จำนวนนักศึกษาที่กำลังศึกษา ในภาคเรียนที่ <?php echo $lastId['Studentterm']['term_id'] ?> ปีการศึกษา <?php echo $yearNew['Studentterm']['term_year'] ?> </b></a></li>

				</ul>

				<!-- Tab panes -->
				<div class="tab-content" style="padding:10px;">
					<div role="tabpanel" class="tab-pane active" id="news">
						<div class="row">				
						<!-- code -->
						<!-- <div style="overflow-x:scroll;" > -->
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="padding:0px;">	
								<h4> ปรับปรุงข้อมูลล่าสุด ณ วันที่  <?php echo DateThai($lastYearTerms['Yearterm']['postdate']); ?></h4>
										
										 
									
								</div>	<br>
							<!-- </div> -->
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" style="padding:10px;">					
									<table class="table table-hover">
										<tr>
											<td>ปริญญา</td>				
											<td>จำนวนนักศึกษา</td>
											<td>ร้อยละ</td>								
										</tr>
										<?php 
											$total = 0;
											foreach ($students as $row) {
											$total += $row;

											}
																			
											$i = 0;
											foreach ($degrees as $degree) {
										?>
											<tr>
												<td><?php echo $degree['Degree']['degree_name'];?></td>
												<td><?php echo $students[$degree['Degree']['degree_name']]; ?></td>
												<td><?php echo round((($students[$degree['Degree']['degree_name']]/ $total)*100),2)?></td>
											</tr>
										<?php
											$i++;
											}
										?>
											<tr>
												<td colspan = '3'><strong> รวมนักศึกษาปัจจุบันทั้งหมด <?php echo $total;?> คน </strong></td>
											</tr>					
										</table>
									</div>			
							<!-- code -->
							<!-- code -->
							
							<!-- code -->

						</div>				
					</div>




				</div>
		</div>
	</div>

</div> <!-- /container -->

