<?php //echo debug($SpecialAndCooperatives);?>
<?php $paginator = $this->Paginator;?>
	<?php 
		date_default_timezone_set('Asia/Bangkok');
		$date = date("Y-m-d");
		$time = date("H:i:s");

		function DateDiff($strDate1,$strDate2){                             
		return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
		}


	?>
	<?php function DateThai($strDate)
		{

		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//, $strHour:$strMinute
	}
	?>
	
	<!-- class="container" -->
<div    style="padding:45px 0 0 0;">
	
			<div class="panel panel-default" style="border-color: #E5CCFF!important">
				<div class="panel-heading" style="background-color: #E5CCFF!important;border-color: #E5CCFF!important">
					เลือกมคอ.3-4 
					
					
				</div>
				<div class="panel-body">
					<div class="col-md-4">
						<a href="<?php echo $this->Html->url(array('action' => 'list_result_pass_proposal',1)); ?>" 
							class="btn btn-warning btn-block  ">                                
							มคอ.3-4 ระดับปริญญาตรี
						</a>
					</div>
					<div class="col-md-4">
						<a href="<?php echo $this->Html->url(array('action' => 'list_result_pass_proposal',3)); ?>" 
							class="btn btn-warning btn-block  ">                                
							มคอ.3-4 ระดับปริญญาโท
						</a>
					</div>
					
					<div class="col-md-4">
						<a href="<?php echo $this->Html->url(array('action' => 'list_result_pass_proposal',5)); ?>" 
							class="btn btn-warning btn-block  ">                                
							มคอ.3-4 ระดับปริญญาเอก
						</a>
					</div>
					
					
					
				</div>
			</div>	
 
		<?php if ($admins['Admincurriculumrequest']['employee_status_id'] == 1) { ?>
					
					
					
					<?php if ($id == null) { ?> 
						<div class="panel panel-primary">
							<div class="panel-heading"> 
								เพิ่ม มคอ.3-4 สมบูรณ์
							</div>
							<div class="panel-body">
									
									<center>
										<a class="btn btn-success btn-lg" href="<?php echo $this->Html->url(array('action' => 'add_pass_proposals',1)); ?>" role="button">											
											<i class="fa fa-plus" aria-hidden="true"></i> เพิ่ม มคอ.3-4 สมบูรณ์ที่มีผลบังคับใช้ ที่นี่
										</a>																	
									</center>
							</div>	
						</div>	
					<?php }else{ ?>
						<div class="panel panel-primary">
							<div class="panel-heading"> 
								เพิ่ม มคอ.3-4 สมบูรณ์
							</div>
							<div class="panel-body">
									
									<center>
										<a class="btn btn-success btn-lg" href="<?php echo $this->Html->url(array('action' => 'add_pass_proposals',$id)); ?>" role="button">											
											<i class="fa fa-plus" aria-hidden="true"></i> เพิ่ม มคอ.3-4 สมบูรณ์ที่มีผลบังคับใช้ ที่นี่
										</a>																	
									</center>
							</div>	
						</div>		
					<?php } ?>
		<?php } ?>
				<?php $admins = $this->Session->read('adminCurriculumRequests'); ?>
				<div id="no-more-tables">
				<?php if ($admins['Admincurriculumrequest']['employee_status_id'] == 3) { ?> 	
					<center>	 
					<font class="txt-content_SpecialAndCooperative26"><b> อาจารย์ผู้รับผิดชอบกระบวนวิชา</b></font>	 	
					</center>	 															
						 		
					<table class="table table-bordered table-striped" id="runner_table">
							<thead>
								<tr>
									<th style="text-align: center;">ลำดับ</th>
									<th style="width:5%">รหัสกระบวนวิชา</th>
									<th style="width:10%">ชื่อวิชาภาษาไทย</th>
									<th style="width:10%">ชื่อวิชาภาษาอังกฤษ</th>
						
									<th >สาขาวิชา</th>
									<th style="width:10%">ภาควิชา</th>
									<th style="width:10%">ระดับการศึกษา</th>
									<th style="width:20%">ภาคการศึกษาที่มีผลบังคับใช้</th> 
									<th>ไฟล์ มคอ.3-4 ฉบับสมบูรณ์ </th>
								
									
								</tr>
							</thead>
							<tbody>
								<?php
									$i = 0;
									if ($SpecialAndCooperatives == null) {
										echo '<tr>
											<td colspan = "11"> <center>
												ขออภัย ยังไม่พบข้อมูล </center><br>'; ?>

												
									<?php echo '</td>
										</tr>';
									}else {								
										foreach ($SpecialAndCooperatives as $SpecialAndCooperative){
											
										$i++;
									?>
										<tr>
											<td data-title="ลำดับ" align="center"><?php echo $i; ?></td>
											<td data-title="รหัสวิชา"> <?php echo $SpecialAndCooperative['SpecialAndCooperative']['code'];?> </td>
											<td data-title="ชื่อวิชาภาษาไทย"> <?php echo $SpecialAndCooperative['SpecialAndCooperative']['title'];?> </td>
											<td data-title="ชื่อวิชาภาษาอังกฤษ"> <?php echo $SpecialAndCooperative['SpecialAndCooperative']['title_eng'];?> </td>
										
											<td data-title="สาขาวิชา"> <?php echo $SpecialAndCooperative['Major']['major_name']; ?> </td>
											<td data-title="ภาควิชา"> <?php echo $SpecialAndCooperative['Organize']['name']; ?> </td>
											<td data-title="ภาควิชา"> <?php echo $SpecialAndCooperative['Degree']['degree_name']; ?> </td>
											<td data-title="สถานะ " >
												<?php echo $SpecialAndCooperative['Yearterm']['name']; ?>		 
											</td>
											
										
											<td data-title="ไฟล์ มคอ.3-4 ฉบับสมบูรณ์">
												<a class="btn btn-success" href="<?php echo $this->Html->url(array('action' => 'add_answer_pass_abstract',$SpecialAndCooperative['SpecialAndCooperative']['id'],$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'])); ?>"
															role="button" target="_blank"><i class="glyphicon glyphicon-tag" aria-hidden="true"></i>  ดูมคอ.3-4 สมบูรณ์ 
															</a>
												<?php if ($admins['Admincurriculumrequest']['employee_status_id'] == 1) { ?> 
													<a class="btn btn-warning" target="_blank" href="<?php echo $this->Html->url(array('action' => 'edit_proposal',$SpecialAndCooperative['SpecialAndCooperative']['id'])); ?>">
														แก้ไขข้อมูลกระบวนวิชา
													</a>
												<?php } ?>
											</td>
											
											
										
												
										</tr>
									<?php  } ?>						
								<?php  } ?>	
							</tbody>
						</table>
					<hr>
						<center>
							<font class="txt-content_SpecialAndCooperative26 "><b> อาจารย์ผู้สอน</b></font>	 														
						</center>
						<table class="table table-bordered table-striped" id="runner_table">
							<thead>
								<tr>
									<th style="text-align: center;">ลำดับ</th>
									<th style="width:5%">รหัสกระบวนวิชา</th>
									<th style="width:10%">ชื่อวิชาภาษาไทย</th>
									<th style="width:10%">ชื่อวิชาภาษาอังกฤษ</th> 
									<th >สาขาวิชา</th>
									<th style="width:10%">ภาควิชา</th>
									<th style="width:10%">ระดับการศึกษา</th>
									<th style="width:20%">ภาคการศึกษาที่มีผลบังคับใช้</th> 
									<th>ไฟล์ มคอ.3-4 ฉบับสมบูรณ์ </th>
								
									
								</tr>
							</thead>
							<tbody>
									<?php echo $outputSpecialAndCooperativeEmployee; ?>
								<!-- <?php
									$i = 0;
									if ($SpecialAndCooperativeEmployees == null) {
										echo '<tr>
											<td colspan = "11"> <center>
												ขออภัย ยังไม่พบข้อมูล </center><br>'; ?>

												
									<?php echo '</td>
										</tr>';
									}else {								
										foreach ($SpecialAndCooperativeEmployees as $SpecialAndCooperative){
											
										$i++;
									?>
										<tr>
											<td data-title="ลำดับ" align="center"><?php echo $i; ?></td>
											<td data-title="รหัสวิชา"> <?php echo $SpecialAndCooperative['SpecialAndCooperative']['code'];?> </td>
											<td data-title="ชื่อวิชาภาษาไทย"> <?php echo $SpecialAndCooperative['SpecialAndCooperative']['title'];?> </td>
											<td data-title="ชื่อวิชาภาษาอังกฤษ"> <?php echo $SpecialAndCooperative['SpecialAndCooperative']['title_eng'];?> </td>
										
											<td data-title="สาขาวิชา"> <?php echo $SpecialAndCooperative['Major']['major_name']; ?> </td>
											<td data-title="ภาควิชา"> <?php echo $SpecialAndCooperative['Organize']['name']; ?> </td>
											<td data-title="สถานะ " >
												<?php echo $SpecialAndCooperative['Yearterm']['name']; ?>		 
											</td>
											
										
												<td data-title="ไฟล์ มคอ.3-4 ฉบับสมบูรณ์">
													<a class="btn btn-success" href="<?php echo $this->Html->url(array('action' => 'add_answer_pass_abstract',$SpecialAndCooperative['SpecialAndCooperative']['id'],$SpecialAndCooperative['SpecialAndCooperative']['employee_id'])); ?>"
																role="button" target="_blank"><i class="glyphicon glyphicon-tag" aria-hidden="true"></i><font class="txt-content_SpecialAndCooperative26"> ดูมคอ.3-4 สมบูรณ์</font>
																</a>
															 
													
												</td>
											
											
										
												
										</tr>
									<?php  } ?>						
								<?php  } ?>	 -->
							</tbody>
						</table>
				<?php }else { ?>
						<table class="table table-bordered table-striped" id="runner_table">
							<thead>
								<tr>
									<th style="text-align: center;">ลำดับ</th>
									<th style="width:5%">รหัสกระบวนวิชา</th>
									<th style="width:10%">ชื่อวิชาภาษาไทย</th>
									<th style="width:10%">ชื่อวิชาภาษาอังกฤษ</th>
						
									<th >สาขาวิชา</th>
									<th style="width:10%">ภาควิชา</th>
									<th style="width:10%">ระดับการศึกษา</th>
									<th style="width:20%">ภาคการศึกษาที่มีผลบังคับใช้</th> 
									<th>ไฟล์ มคอ.3-4 ฉบับสมบูรณ์ </th>
								
									
								</tr>
							</thead>
							<tbody>
								<?php
									$i = 0;
									if ($SpecialAndCooperatives == null) {
										echo '<tr>
											<td colspan = "11"> <center>
												ขออภัย ยังไม่พบข้อมูล </center><br>'; ?>

												
									<?php echo '</td>
										</tr>';
									}else {								
										foreach ($SpecialAndCooperatives as $SpecialAndCooperative){
											
										$i++;
									?>
										<tr>
											<td data-title="ลำดับ" align="center"><?php echo $i; ?></td>
											<td data-title="รหัสวิชา"> <?php echo $SpecialAndCooperative['SpecialAndCooperative']['code'];?> </td>
											<td data-title="ชื่อวิชาภาษาไทย"> <?php echo $SpecialAndCooperative['SpecialAndCooperative']['title'];?> </td>
											<td data-title="ชื่อวิชาภาษาอังกฤษ"> <?php echo $SpecialAndCooperative['SpecialAndCooperative']['title_eng'];?> </td>
										
											<td data-title="สาขาวิชา"> <?php echo $SpecialAndCooperative['Major']['major_name']; ?> </td>
											<td data-title="ภาควิชา"> <?php echo $SpecialAndCooperative['Organize']['name']; ?> </td>
											<td data-title="ภาควิชา"> <?php echo $SpecialAndCooperative['Degree']['degree_name']; ?> </td>
											<td data-title="สถานะ " >
												<?php echo $SpecialAndCooperative['Yearterm']['name']; ?>		 
											</td>
											
										
											<td data-title="ไฟล์ มคอ.3-4 ฉบับสมบูรณ์">
												<a class="btn btn-success" href="<?php echo $this->Html->url(array('action' => 'add_answer_pass_abstract',$SpecialAndCooperative['SpecialAndCooperative']['id'],$SpecialAndCooperative['SpecialAndCooperative']['mis_employee_id'])); ?>"
															role="button" target="_blank"><i class="glyphicon glyphicon-tag" aria-hidden="true"></i>  ดูมคอ.3-4 สมบูรณ์ 
															</a>
												<?php if ($admins['Admincurriculumrequest']['employee_status_id'] == 1) { ?> 
													<a class="btn btn-warning" target="_blank" href="<?php echo $this->Html->url(array('action' => 'edit_proposal',$SpecialAndCooperative['SpecialAndCooperative']['id'])); ?>">
														แก้ไขข้อมูลกระบวนวิชา
													</a>
												<?php } ?>
											</td>
											
											
										
												
										</tr>
									<?php  } ?>						
								<?php  } ?>	
							</tbody>
						</table>
				<?php } ?>
			</div>
		
				
			</div>
	
		</div>
		<hr>
		<script type="text/javascript">
			$(document).ready(function() {
			$('#runner_table').DataTable({
				// "language": {
				// 	"search": "<?php //echo $search; ?>",
				"lengthMenu": [[100, 75, 50, -1], [100, 75, 50, "All"]]
				// 	"Question": "<?php //echo $Question; ?>",
				// 	"QuestionEmpty": "<?php //echo $QuestionEmpty; ?>",
				// 	"emptyTable": "<?php //echo $emptyTable; ?>",
				// 	"paginate": {
				// 		"next":       ">",
				// 		"previous":   "<"
				// 	}
				// }
			});
			} );
		</script>
		
 	
</div>
