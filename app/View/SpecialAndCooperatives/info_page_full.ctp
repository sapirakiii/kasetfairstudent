<?php //debug($new); ?>
<?php
	function DateThai($strDate)
		{

		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//, $strHour:$strMinute
	}
	?>
<div class="container" >	
	<div class="row">
        <div class="col-sm-12">           
        <div class="panel panel-default" style="border-color: #FFFFFF!important">
                <div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
                   
                    <div class="font_panel7">
                    <?php 
                        echo '<img  alt="" src="http://www.agri.cmu.ac.th/2017/files/Infopage/Logo_Thai.png" width="5%" />';
                     ?>
                   
                    <?php echo strip_tags($new['Infopage']['Title']);  ?></div>
			       
                </div>
                <div class="panel-body">
                                                            
                        <p><?php echo $new['Infopage']['Detail']; ?></p>
                        <hr>
                            <div style="padding-left: 10px;">
                            
                            <?php
                                    if ($new['InfoDocumentpage'] == null) {
                                        
                                    }else { ?>
                                    <b>เอกสารประกอบ :</b>
                                    <?php
                                        foreach ($new['InfoDocumentpage'] as $InfoDocument) {
                                            if ($InfoDocument['class'] == "file") { ?>
                                            
                                            <a href="<?php echo $this->Html->url('http://www.agri.cmu.ac.th/2017/files/Infopage/'.$InfoDocument['infopage_id'].'/').''. $InfoDocument['name'] ?>" class="btn btn-primary" target="_blank">Download <?php echo $InfoDocument['name_old']; ?> <span class="glyphicon glyphicon-cloud-download"></span></a>
                                <?php
                                        }
                                    }
                                }	
                                ?>
                            </div>
                        <br>
                        <div class="font_panel6"> <i class="fa fa-user-circle-o" aria-hidden="true"></i> : <?php echo $new['Infopage']['CountRead'] ?> ครั้ง </div> 
                        
                    </div>
                    </div>
        </div>
         
        <!-- <div class="col-sm-3">
         
          <div class="panel-group" id="accordion">
              <div class="font_panel2"> WEBSITE RELATED</div>
                        <div class="panel panel-default" style="border-color: #FFF595!important">
                            <div class="panel-heading" style="background-color: #FFF595!important;border-color: #FFF595!important">
                                <h4 class="panel-title">
                                <a href="<?php echo $this->Html->url(array('controller' => 'webs','action' => 'info_page','th',165)); ?>" ><div class="font_panel1"><i class="fa fa-globe" aria-hidden="true"></i> SMART KNOWLEDGE</div></a>
                                </h4>
                            </div>
                            
                        </div>
                        <div class="panel panel-default" style="border-color: #FFF595!important">
                            <div class="panel-heading" style="background-color: #FFF595!important;border-color: #FFF595!important">
                                <h4 class="panel-title">
                                <a href="<?php echo $this->Html->url(array('controller' => 'webs','action' => 'info_page','th',166)); ?>" ><div class="font_panel1"><i class="fa fa-globe" aria-hidden="true"></i> ประกาศแสดงเจตจำนงสุจริตการบริหารงาน</div></a>
                                </h4>
                            </div>
                            
                        </div>
                        
                    
            
            
              </div>
            </div>-->
          </div> 
            <!-- /.row -->


      <div class="container" style="">	
			<div class="row" >


            <div class="panel panel-default" style="border-color: #F2F2F2!important">
                <div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
                    <h3 class="panel-title"><div class="font_panel2"> ลิ้งค์ที่เกี่ยวข้อง</div></div>
			        </h3>
                </div>
                <div class="panel-body">
                                                            
                <?php
                                                if ($new['Infopage'] == null) {
                                                    
                                                }else { ?>                                   
                                                <?php
                                                    
                                                        ?>
                                                        <p><?php echo $new['Infopage']['link']; ?></p>
                                                        <!-- <a href="http://<?php //echo $new['Infopage']['link'] ?>" class="btn btn-primary" target="_blank"><?php //echo $new['Infopage']['link'] ?></a> -->
                                            <?php
                                                    
                                                
                                            }	
                                            ?>     
                        
                    </div>
                    </div>


           
                
           
          </div>
        </div>
      
<!-- 
        <div class="col-sm-4 my-4">
          <div class="card" style="border-color: #EEFA9D!important">
          <img class="card-img-top" src="<?php echo $this->Html->url('/files/Document/213_pic.JPG'); ?>" alt="">
          
            <div class="card-body" style="border-color: #EEFA9D!important">
              <h4 class="card-title"><div class="font_panel2"> สื่อวีดีโอ</div></h4>
              
            
            </div>
            <div class="card-footer" style="background-color: #EEFA9D!important;border-color: #EEFA9D!important">
              <a href="<?php echo $this->Html->url(array('controller' => 'main','action' => 'InfoActivityVideo')); ?>" class="btn btn-default"><div class="font_panel3"> View More</div></a>
            </div>
          </div>
        </div>
        <div class="col-sm-4 my-4">
          <div class="card" style="border-color: #EEFA9D!important">
            <img class="card-img-top" src="<?php echo $this->Html->url('/files/Document/213_pic.JPG'); ?>" alt="">
            <div class="card-body" style="border-color: #EEFA9D!important">
              <h4 class="card-title"><div class="font_panel2"> ประมวลภาพความประทับใจ</div></h4>
              
             
            </div>
            <div class="card-footer" style="background-color: #EEFA9D!important;border-color: #EEFA9D!important">
              <a href="<?php echo $this->Html->url(array('action' => 'InfoActivityFinish',70)); ?>" class="btn btn-default"><div class="font_panel3"> View More</div></a>
            </div>
          </div>
        </div>
        <div class="col-sm-4 my-4">
          <div class="card" style="border-color: #EEFA9D!important">
            <img class="card-img-top" src="<?php echo $this->Html->url('/img/5.jpg'); ?>" alt="">
            <div class="card-body" style="border-color: #EEFA9D!important">
              <h4 class="card-title"><div class="font_panel2"> สื่อประชาสัมพันธ์</div></h4>
              
              </div>
            <div class="card-footer" style="background-color: #EEFA9D!important;border-color: #EEFA9D!important">
              <a href="<?php echo $this->Html->url(array('controller' => 'main','action' => 'InfoActivityPromote')); ?>" class="btn btn-default"><div class="font_panel3"> View More</div></a>
            </div>
          </div>
        </div> -->
      
      <!-- /.row -->

    
    <!-- /.container -->

   