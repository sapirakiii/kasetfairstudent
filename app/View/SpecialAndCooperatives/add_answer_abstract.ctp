<?php //echo debug($this->request->data)?>
<!-- onclick="return confirm('ยืนยันการเพิ่มข้อมูล')" -->
<style>
.numberCircle {
    border-radius: 50%;
    behavior: url(PIE.htc); /* remove if you don't care about IE8 */
    width: 36px;
    height: 36px;
    padding: 8px;
    background: #fff;
    border: 2px solid black;
    color: black;
    text-align: center;
    font: 20px Arial, sans-serif;
    display: inline-block;
}
</style>
<style>
        u.dotted{
          border-bottom: 1px dashed #999;
          text-decoration: none; 
        }
</style>
<?php
	function DateThai($strDate)
		{

		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//, $strHour:$strMinute
	}
	?>
<!-- start content here -->

<div  style="padding:15px 0 0;">
  <div class="row">
      
    <div class="col-md-12">
      <div class="panel panel-primary">
				<div class="panel-heading"> 
          รายการที่ขอเปิดกระบวนวิชาใหม่/ปรับปรุงกระบวนวิชา
				</div>
					<div class="panel-body">
            
            <div class="col-md-12 panel with-nav-tabs panel-default" style="border-color: #F2F2F2!important;padding-left: 0px;padding-right: 0px;">
                  
                            <div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
                        <ul class="nav nav-tabs">
                          <li class="active"><a href="#tab8primary" data-toggle="tab"><i class="fa fa-university" aria-hidden="true"></i> ข้อมูลหลักสูตร</a></li>
                          <!-- <li><a href="#tab9primary" data-toggle="tab"> ประวัติการพิจารณา</a></li> -->
                          <!-- <li><a href="#tab10primary" data-toggle="tab"> ประวัติการพิจารณา</a></li> -->
                    
                        </ul>
                          </div>
                      <div class="panel-body">
                        <div class="tab-content">
                          <div class="tab-pane fade in active" id="tab8primary">
                            <div class="col-md-6"> 
                              <table class="table table-bordered" style="width: 100%;"> 
                                <tr>
                                  <td colspan="2">
                                    ข้อมูลผู้ร้องขอเปิด/ปรับปรุงกระบวนวิชา 
                                        
                                  </td>
                                </tr>
                                <tr>
                                  
                                  <td>
                                      ชื่อ-สกุล
                                        
                                  </td>
                                  <td>
                                        <?php  
                                            $sSubPre = '';
                                            if ($MisEmployeeRequests['MisSubPrename']['id'] != null) {
                                              $sSubPre = $sSubPre . $MisEmployeeRequests['MisSubPrename']['name_short_th'] . ' ';
                                            }
                                            if ($sSubPre == '') {
                                              $sSubPre = $sSubPre . $MisEmployeeRequests['MisPrename']['name_full_th'] . ' ';
                                            } 
                                          ?> 
                                        <?php echo $sSubPre.''.$MisEmployeeRequests['MisEmployee']['fname_th'].' '.$MisEmployeeRequests['MisEmployee']['lname_th']; ?>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                      หน่วยงาน
                                        
                                        
                                  </td>
                                  <td><?php echo  $MisOrganizes['MisOrganize']['name_full_th']; ?></td>
                                </tr>
                                <tr>
                                  <td><b>E-mail CMU ที่รับการแจ้งเตือน</b></td>
                                  <td><?php echo $MisEmployeeRequests['MisEmployee']['email_cmu']  ?></td>
                                </tr>
                                
                              </table>
                              <hr>  
                              
                              <div class="panel panel-warning">
                                <div class="panel-heading"> 
                                &emsp;<i class="glyphicon glyphicon-bookmark"></i>
                                <font class="txt-content_SpecialAndCooperative26">  รายละเอียดหลักสูตรกระบวนวิชา </font>
                                
                                </div>
                                  <div class="panel-body">
                                      
                                      <table class="table table-bordered">
                                          <tbody>
                                              <tr>
                                                  <td>รหัสกระบวนวิชา</td>
                                                  <td><?php echo $proposal['SpecialAndCooperative']['code']; ?></td>
                                              </tr> 
                                              <tr>
                                                  <td>ชื่อวิชาภาษาอังกฤษ</td>
                                                  <td><?php echo $proposal['SpecialAndCooperative']['title']; ?></td>
                                              </tr>  
                                              <tr>
                                                  <td>ชื่อวิชาภาษาอังกฤษ</td>
                                                  <td><?php echo $proposal['SpecialAndCooperative']['title_eng']; ?></td>
                                              </tr>
                                              <tr>
                                                  <td><b>ภาควิชา</b> </td>
                                                  <td>  <?php echo $proposal['Organize']['name'];?></td>

                                              </tr>  
                                              <?php if($proposal['SpecialAndCooperative']['major_id'] != null){ ?>
                                                <tr>
                                                    <td>สาขาวิชา</td>
                                                    <td><?php echo $proposal['Major']['major_name']; ?></td>
                                                </tr> 
                                              <?php }else{ ?>
                                                <tr>
                                                    <td>สาขาวิชา</td>
                                                    <td>ส่วนกลางภาควิชา</td>
                                                </tr>
                                              <?php } ?>
                                             
                                              <tr>
                                                  <td>ระดับ </td>
                                                  <td><?php echo $proposal['Degree']['degree_name']; ?></td>
                                              </tr>    
                                              <!-- <tr>
                                                  <td>สถานะ</td>
                                                  <td>
                                                    <?php
                                                      if ($proposal['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 1 || $proposal['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 2) { ?>
                                                        <font color="red"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> <?php echo $proposal['SpecialAndCooperativePass']['name']; ?> </font>	
                                                      <?php	}elseif ($proposal['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 3){ ?>			
                                                          <font color="orange"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>  <?php echo $proposal['SpecialAndCooperativePass']['name']; ?></font> 
                                                      <?php	}elseif ($proposal['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 4 || $proposal['SpecialAndCooperative']['special_and_cooperative_pass_id'] == 5){ ?>		
                                                          <font color="green"><span class="glyphicon glyphicon-ok-sign" aria-hidden="true"></span>  <?php echo $proposal['SpecialAndCooperativePass']['name']; ?></font> 
                                                      <?php } ?>
                                                  </td>
                                              </tr>  -->
                                              <tr>
                                                      <td><b>สถานะการติดตาม : </b></td>
                                                      <td> 
                                                        <?php if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 4){ ?>
                                                          <span class="label label-danger"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> 
                                                            <?php if($proposal['SpecialAndCooperative']['degree_id'] == 1 ){ ?>
                                                              <?php echo $proposal['SpecialAndCooperativeFollowStatus']['name']; ?>
                                                            <?php }else { ?>
                                                              <?php echo $proposal['SpecialAndCooperativeFollowStatus']['name_en']; ?>
                                                            <?php } ?>
                                                          </span>	
                                                        <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] >= 4 && $proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] <= 9){ ?>
                                                          <span class="label label-warning"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                                            <?php if($proposal['SpecialAndCooperative']['degree_id'] == 1 ){ ?>
                                                              <?php echo $proposal['SpecialAndCooperativeFollowStatus']['name']; ?>
                                                            <?php }else { ?>
                                                              <?php echo $proposal['SpecialAndCooperativeFollowStatus']['name_en']; ?>
                                                            <?php } ?>
                                                          </span>	
                                                        <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 10){ ?>
                                                          <span class="label label-success"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                                            <?php if($proposal['SpecialAndCooperative']['degree_id'] == 1 ){ ?>
                                                              <?php echo $proposal['SpecialAndCooperativeFollowStatus']['name']; ?>
                                                            <?php }else { ?>
                                                              <?php echo $proposal['SpecialAndCooperativeFollowStatus']['name_en']; ?>
                                                            <?php } ?>
                                                          </span>
                                                        <?php } ?>
                                                      </td>
                                              </tr> 
                                              <!-- <tr>
                                                
                                                <td colspan="2">
                                                  <table class="table table-bordered">
                                                      <tbody>
                                                        <tr>
                                                            <td >
                                                                <?php echo $output; ?>
                                                            </td>
                                                        </tr> 
                                                                                            
                                                      </tbody>
                                                  </table>  
                                                </td>
                                              </tr>                
                                              -->
                                              <tr>
                                                  <td>รูปแบบกระบวนวิชา </td>
                                                  <td><?php echo $proposal['SpecialAndCooperativeType']['name']; ?></td>
                                              </tr>
                                              <tr>
                                                  <td>ประเภทกระบวนวิชา </td>
                                                  <td>
                                                      <b><?php echo $proposal['Coursegroup']['name2']; ?></b>
                                                      <?php if($proposal['SpecialAndCooperative']['coursegroup_detail'] != null){ ?>
                                                        กลุ่มวิชา <u class="dotted"> <?php echo $proposal['SpecialAndCooperative']['coursegroup_detail']; ?></u>
                                                      <?php } ?>
                                                  </td>
                                              </tr>
                                              <tr>
                                                  <td>ประเภทการเข้าที่ประชุม/แจ้งเวียน </td>
                                                  <td>
                                                      <?php if($proposal['SpecialAndCooperative']['special_and_cooperative_meeting_type_id'] == 1){ ?>
                                                        เข้าที่ประชุมสาขา/ภาควิชา  
                                                        <b>ครั้งที่ <?php echo $proposal['SpecialAndCooperative']['special_and_cooperative_meeting_num']; ?></b> <br>
                                                        <b>วันที่ <?php echo DateThai($proposal['SpecialAndCooperative']['special_and_cooperative_meeting_postdate']) ; ?></b>
                                                      <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_meeting_type_id'] == 2){ ?>
                                                        แจ้งเวียน 
                                                        <b>วันที่ <?php echo DateThai($proposal['SpecialAndCooperative']['special_and_cooperative_meeting_postdate']) ; ?></b>
                                                      <?php } ?>
                                                  </td>
                                              </tr>
                                                          
                                                                  
                                                                  
                                          </tbody>
                                      </table>       
                                  
                                                                                
                                  </div>
                              </div> 
                              <div class="panel panel-primary">
                                <div class="panel-heading"> 
                                    &emsp; <i class="glyphicon glyphicon-user"></i>                          
                                    <font class="txt-content_SpecialAndCooperative26"> อาจารย์ผู้รับผิดชอบกระบวนวิชาและอาจารย์ผู้สอน</font>

                                    </div>
                                    <div class="panel-body">
                                        <table class="table table-bordered">
                                              <tbody>
                                                   
                                                  <tr>
                                                      <td><b>อาจารย์ผู้รับผิดชอบ </b></td>
                                                      <td>
                                                        <?php  
                                                          $sSubPre = '';
                                                          if ($employees['MisSubPrename']['id'] != null) {
                                                            $sSubPre = $sSubPre . $employees['MisSubPrename']['name_short_th'] . ' ';
                                                          }
                                                          if ($sSubPre == '') {
                                                            $sSubPre = $sSubPre . $employees['MisPrename']['name_full_th'] . ' ';
                                                          } 
                                                        ?> 
                                                      <?php echo $sSubPre.''.$employees['MisEmployee']['fname_th'].' '.$employees['MisEmployee']['lname_th']; ?>
                                                      
                                                      <?php //echo $employees['Prefix']['fullname']," ",$employees['Employee']['fname']," ",$employees['Employee']['lname'];?> </td>

                                                  </tr>  
                                                  <tr>
                                                      <td><b>อาจารย์ผู้สอน </b></td>
                                                      <td>
                                                        <table class="table table-bordered">
                                                            <tr>
                                                              <td>ลำดับ</td>
                                                              <td>รายนาม</td>
                                                              
                                                            </tr>
                                                          <?php echo  $outputSpecialAndCooperativeEmployee;  ?> 
                                                        </table>
                                                      </td>

                                                  </tr>                 
                                              </tbody>
                                          </table> 

                                    </div> 
                                </div>
                             
                             
                             
                              <?php	if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 10 ){ ?>
                                <?php	if($proposaldocumentdates != null){ ?>
                                  <div class="panel panel-success">
                                        <div class="panel-heading"> 
                                          <div class="panel-title">
                                              &emsp;<i class="glyphicon glyphicon-book"></i>
                                              <font class="txt-content_SpecialAndCooperative26"> ไฟล์ มคอ.3-4 สมบูรณ์ กระบวนวิชา <?php echo $proposal['SpecialAndCooperative']['code']; ?></font>
                                            </div> 
                                          </div>
                                          <div class="panel-body">
                                            <tbody>   
                                                <table class="table table-bordered">
                                                    <tbody>
                                                      <tr>
                                                          <td>
                                                              <?php echo $outputpass; ?>
                                                          </td>
                                                      </tr>                                
                                                    </tbody>
                                                </table>  
                                          </tbody>      
                        
                                        </div> 
                                      </div>      
                                
                                <?php	} ?>  
                              <?php	} ?>    
                              <div class="panel panel-primary">
                                <div class="panel-heading"> 
                                    &emsp; <i class="glyphicon glyphicon-signal"></i>
                                    <font class="txt-content_SpecialAndCooperative26"> สถานะการติดตามขอเปิด/ปรับปรุงกระบวนวิชา <?php echo $proposal['SpecialAndCooperative']['code'] ?></font>
  
                                    </div>
                                    <div class="panel-body">
                                      <?php if($proposal['SpecialAndCooperative']['degree_id'] == 1){ ?>
                                        <div class="row bs-wizard" style="border-bottom:0;">
                                          <?php  ?>
                                            <!-- <?php if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 1){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 1){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete">
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 1) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?>
                                              <div class="text-center bs-wizard-stepnum">Step 1</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center  ">อาจารย์</div>
                                            </div> -->
                                            
                                            <?php if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 1 || $proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 2){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 2){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 2) { ?>
                                                  <div class="col-xs-3 bs-wizard-step active">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 1</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center  ">อาจารย์ / ภาควิชา</div>
                                            </div>
  
                                            <?php if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 4){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 4){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 4) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 2</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">งานบริการการศึกษา</div>
                                            </div>
                                      
                                            <?php if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 5){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 5){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 5) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 3</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">ผู้บริหาร(ฝ่ายวิชาการ)</div>
                                            </div>  

                                            <?php if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 6){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 6){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 6) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 4</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">กรรมการวิชาการ <br> ระดับปริญญาตรี</div>
                                            </div>
                                        </div>
                                        <div class="row bs-wizard" style="border-bottom:0;"> 
                                            
                                            <?php if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 7){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 7){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 7) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 5</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">กรรมการบริหารประจำคณะ</div>
                                            </div>
                                            <?php if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 8){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 8){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 8) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 6</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">สำนักพัฒนาคุณภาพนักศึกษา</div>
                                            </div>
                                      
                                            <?php if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 9){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 9){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 9) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 7</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">กรรมการบริหารและ <br>ประสานงานวิชาการ	</div>
                                            </div>  
                                            <?php if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 10){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 10){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 10) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 8</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">มหาวิทยาลัย</div>
                                            </div>
                                        </div>
                                         
                                      <?php }else { ?>
                                        <div class="row bs-wizard" style="border-bottom:0;"> 
                                            <!-- <?php if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 1){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 1){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"> 
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 1) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 1</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center  ">อาจารย์</div>
                                            </div> -->
                                            
                                            <?php if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 2){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 2){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 2) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 1</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center  ">อาจารย์/ภาควิชา</div>
                                            </div>
                                            <?php if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 4){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 4){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 4) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 2</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">งานบริการการศึกษา</div>
                                            </div>
                                      
                                            <?php if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 5){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 5){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 5) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 3</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">กรรมการบัณฑิตศึกษา <br> ประจำคณะ</div>
                                            </div>  
                                            <?php if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 6){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 6){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 6) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 4</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">กรรมการวิชาการ <br> บัณฑิตวิทยาลัย</div>
                                            </div>
                                        </div>
                                        <div class="row bs-wizard" style="border-bottom:0;"> 
                                            
                                            <?php if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 7){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 7){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 7) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 5</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">กรรมการบริหารวิชาการ <br>ประจำบัณฑิตวิทยาลัย</div>
                                            </div>
                                            <?php if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 8){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 8){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 8) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 6</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">กรรมการบริหารมหาวิทยาลัย</div>
                                            </div>
                                            <?php if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 9){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 9){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 9) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 7</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">สภาวิชาการ</div>
                                            </div>  
                                            <?php if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 10){ ?> 
                                                <div class="col-xs-3 bs-wizard-step active">
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 10){ ?> 
                                                <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
                                              <?php }elseif($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] < 10) { ?>
                                                  <div class="col-xs-3 bs-wizard-step disabled">
                                              <?php } ?> 
                                              <div class="text-center bs-wizard-stepnum">Step 8</div>
                                              <div class="progress"><div class="progress-bar"></div></div>
                                              <a href="#" class="bs-wizard-dot"></a>
                                              <div class="bs-wizard-info text-center">สภามหาวิทยาลัย</div>
                                            </div>
                                      
                                        </div>
                                        <div class="row bs-wizard" style="border-bottom:0;"> 
                                            
                                              
                                        </div>
                                       
                                      <?php } ?>
  
                                    </div> 
                              </div>

                              <!-- ************** -->
                              </div>

                            </div>
                            <div class="col-md-6"> 
                              <?php  if ($admins['Admincurriculumrequest']['employee_status_id'] == 3) { ?>
                                
                                    <form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                      <div class="panel panel-success">
                                        <div class="panel-heading"> 
                                          <div class="panel-title">
                                              &emsp;<i class="glyphicon glyphicon-book"></i>
                                              <font class="txt-content_SpecialAndCooperative26"> การพิจารณาขอเปิด/ปรับปรุงกระบวนวิชา </font>
                                            </div> 
                                          </div>
                                          <div class="panel-body">
                                            <tbody>   
                                                                                      
                                                      
                                                  <?php	if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] >= 1 && $proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] <= 4){ ?>
                                                      <!-- ********** -->
                                                      <?php 
                                                            $required = 'กรุณากรอกข้อมูล';
                                                                  echo $this->Form->hidden('AnswerSpecialAndCooperative.special_and_cooperative_id', array(
                                                                    'value' => $proposal_id,
                                                                    'class' => 'form-control',
                                                                  ));
                                                                  echo $this->Form->hidden('AnswerSpecialAndCooperative.mis_employee_id', array(
                                                                    'value' => $mis_employee_id,
                                                                    'class' => 'form-control',
                                                                  ));
                                                                  echo $this->Form->hidden('AnswerSpecialAndCooperative.status', array(
                                                                    'value' => 1,
                                                                    'class' => 'form-control',
                                                                  ));
                                                            ?>
                                                          <div class="panel panel-default" style="border-color: #FFF595!important">
                                                            <div class="panel-heading" style="background-color: #FFF595!important;border-color: #FFF595!important">
                                                                <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                                    <div class="font_panel5">
                                                                    <div class="numberCircle">1</div>  หากมีคำอธิบายเพิ่มเติม (ถ้ามี) คลิ๊กเพิ่มคำอธิบายที่นี่</div></a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseTwo" class="panel-collapse  ">
                                                              <div class="panel-body">
                                                                <table>
                                                                  <tr>
                                                                    <td>รายละเอียด <font color="red">*</font></td>
                                                                      <td><div class="form-group has-feedback">      
                                                                              <?php 
                                                                                    echo $this->Form->input('AnswerSpecialAndCooperative.Detail', array(
                                                                                        'type' => 'textarea',
                                                                                        'label' => false,
                                                                                        'div' => false,
                                                                                        'class' => 'ckeditor',                                                           
                                                                                        'error' => false,
                                                                                        'required'
                                                                                        ));
                                                                                
                                                                                  ?>
                                                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                                      </div>
                                                                    </td>
                                                                  </tr>
                                                                  <!-- <tr>
                                                                    <td>อัพโหลดเอกสารประกอบการพิจารณา (ถ้ามี)</td>
                                                                      <td> <div class="form-group has-feedback">   
                                                                        <?php
                                                                            echo $this->Form->input('AnswerSpecialAndCooperativeDocument.files.', array('type' => 'file', 'multiple'));
                                                                            ?>
                                                                        </div>
                                                                    </td>
                                                                  </tr> -->
                                                                </table>
                                                                    <!-- ************************   -->
                                                              
                                                              </div>
                                                            </div>
                                                          </div>  
                                                          <table class="table">
                                                            <tr>
                                                              <td>
                                                                
                                                                  สถานะการแก้ไข  <img src="https://www1.reg.cmu.ac.th/ugradapply/images/update.gif">
                                                                    
                                                                    <?php 
                                                                      echo $this->Form->hidden('SpecialAndCooperative.id', array(
                                                                        'value' => $proposal_id,
                                                                        'class' => 'form-control',
                                                                      )); 
                                                                    ?>              
                                                                    <div class="form-group has-feedback">      
                                                                        
                                                                        <?php 
                                                                          $types = array( 
                                                                            '4' => '&nbsp; <font color="green"><b> ยืนยันการแก้ไข</b></font>'
                                                                           );	
                                                                          //  $passes 
                                                                          echo $this->Form->radio('SpecialAndCooperative.special_and_cooperative_pass_id', 
                                                                          $types, 
                                                                          array(
                                                                            'legend' => false,   
                                                                            'value' => 4,                                                          
                                                                            'separator' => '<br/>',
                                                                            'required',
                                                                            'style' => 'width: 25px; 
                                                                                  height: 25px; 
                                                                                  vertical-align:text-bottom;'));
                                                                        ?>
                                                                  <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                                </div>
                                                                
                                                                
                                                              </td>
                                                               
                                                            </tr>
                                                              
                                                          </table>
                                                          
                                                          <table class="table table-hover table-post" style="width: 100%;">  
                                                            <tr>
                                                                <td>                
                                                                    <div class="font_panel5">	
                                                                      <h3 ><div class="numberCircle">2</div> อัพโหลดไฟล์ มคอ.3/4 ที่แก้ไข</h3>
                                                                      
                                                                    </div>
                                                                </td>
                                                            </tr> 
                                                            <tr>
                                                              <td>
                                                                <label for="InfoDetail" class="addpost_input">อัปโหลดรูปภาพ และไฟล์เอกสารประกอบ</label> <font color="red">(กรณีเลือกมากกว่า 1 ไฟล์ ให้กด Ctrl หรือ Shift ค้างไว้และเลือกรูปภาพหรือไฟล์ที่ต้องการ)</font>
                                                                  <?php
                                                                      echo $this->Form->input('SpecialAndCooperativeDocument.files.', array('type' => 'file', 'multiple','accept' => '.xlsx,.xls,image/*, .doc, .docx, .ppt, .pptx, .txt, .pdf, .zip, .rar'));
                                                                  ?>
                                                              </td>
                                                            </tr>                                                       
                                                        
                                                            
                                                          </table>                          
                                                            <div class="form-group">
                                                                  
                                                              <div class="col-sm-12"> 
                                                                    <input type="submit" class="btn btn-success btn-lg btn-block" value="บันทึกข้อมูล" >                                                                
                                                              </div>
                                                          </div>
                                                  
                                                    
                                                    

                                                  <?php }else{  ?>
                                                            <?php 
                                                            $required = 'กรุณากรอกข้อมูล';
                                                                  echo $this->Form->hidden('AnswerSpecialAndCooperative.special_and_cooperative_id', array(
                                                                    'value' => $proposal_id,
                                                                    'class' => 'form-control',
                                                                  ));
                                                                  echo $this->Form->hidden('AnswerSpecialAndCooperative.mis_employee_id', array(
                                                                    'value' => $mis_employee_id,
                                                                    'class' => 'form-control',
                                                                  ));
                                                                  echo $this->Form->hidden('AnswerSpecialAndCooperative.status', array(
                                                                    'value' => 1,
                                                                    'class' => 'form-control',
                                                                  ));
                                                            ?>
                                                            <?php	if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] == 10){ ?>
                                                              <?php	if($proposaldocumentdates != null){ ?>
                                                                  <font color="green">เสร็จสิ้นกระบวนการ</font>
                                                                <?php	}else{ ?>
                                                                  <font color="green">เสร็จสิ้นการพิจารณา กรุณารอไฟล์ มคอ.3-4 สมบูรณ์ที่มีผลบังคับใช้</font>

                                                              <?php	} ?>
                                                            <?php	}else{ ?>
                                                              <font color="red">
                                                                กระบวนวิชา <?php echo $proposal['SpecialAndCooperative']['code']; ?> นี้ได้เสนอเข้าพิจารณาระดับคณะเรียบร้อยแล้ว กรุณารอการตอบกลับการพิจารณา <br>
                                                                *หมายเหตุ : การแก้ไขจะกระทำได้ ต่อเมื่ออยู่ในสถานะ อาจารย์/ภาควิชา
                                                              </font>

                                                            <?php	} ?>
                                                          <!-- 
                                                            <table>
                                                            <tr>
                                                                <td>รายละเอียด <font color="red">*</font></td>
                                                                  <td><div class="form-group has-feedback">      
                                                                          <?php 
                                                                                echo $this->Form->input('AnswerSpecialAndCooperative.Detail', array(
                                                                                    'type' => 'textarea',
                                                                                    'label' => false,
                                                                                    'div' => false,
                                                                                    'class' => 'ckeditor',                                                           
                                                                                    'error' => false,
                                                                                    'required'
                                                                                    ));
                                                                            
                                                                              ?>
                                                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                                  </div>
                                                                </td>
                                                              </tr>
                                                              <tr>
                                                                <td>อัพโหลดเอกสาร/รูปภาพแนบ (ถ้ามี)</td>
                                                                  <td> <div class="form-group has-feedback">   
                                                                    <?php
                                                                        echo $this->Form->input('AnswerSpecialAndCooperativeDocument.files.', array('type' => 'file', 'multiple'));
                                                                        ?>
                                                                    </div>
                                                                </td>
                                                              </tr> 
                                                              <tr>
                                                                <table class="table">
                                                                  <tr>
                                                                    <td>
                                                                      
                                                                        คลิ๊กเพื่อระบุสถานะ <br><font color="red">* ท่านได้ส่งขอ เปิด/ปรับปรุงกระบวนวิชา เรียบร้อยแล้ว กรุณารอการตอบกลับการพิจารณา</font>  
                                                                          
                                                                          <?php 
                                                                            echo $this->Form->hidden('SpecialAndCooperative.id', array(
                                                                              'value' => $proposal_id,
                                                                              'class' => 'form-control',
                                                                            )); 
                                                                          ?>              
                                                                          <div class="form-group has-feedback">      
                                                                              
                                                                              <?php  
                                                                                echo $this->Form->radio('SpecialAndCooperative.special_and_cooperative_pass_id', 
                                                                                $passes, 
                                                                                array(
                                                                                  'legend' => false,   
                                                                                  'value' => $proposal['SpecialAndCooperative']['special_and_cooperative_pass_id'],                                                          
                                                                                  'separator' => '<br/>',
                                                                                  'disabled',
                                                                                  // 'required',
                                                                                  'style' => 'width: 25px; 
                                                                                        height: 25px; 
                                                                                        vertical-align:text-bottom;'));
                                                                              ?>
                                                                          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                                        </div> 
                                                                      <label class="col-sm-3 control-label"> เสนอ/เห็นชอบ/มีการแก้ไข ครั้งที่ <br>
                                                                                </label>
                                                                                <div class="col-sm-7">
                                                                                  <div class="form-group has-feedback">      
                                                                                          <?php 
                                                                                                echo $this->Form->input('SpecialAndCooperative.num_id', array(
                                                                                                    'type' => 'text',
                                                                                                    'label' => false,
                                                                                                    'div' => false,
                                                                                                    'class' => array('form-control'),                                                          
                                                                                                    'error' => false,
                                                                                                    'readonly',
                                                                                                    ));
                                                                                            
                                                                                              ?>
                                                                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                                                  </div>
                                                                                </div>
                                                                                <label class="col-sm-3 control-label">วันที่  <br>
                                                                                </label>
                                                                                    <div class="col-sm-7">
                                                                                      <div class="input-group date">                        
                                                                                      <?php 
                                                                                      echo $this->Form->input('SpecialAndCooperative.postdate', array(                          
                                                                                            'type' => 'text',
                                                                                            'label' => false,  
                                                                                            'value' => date('d-m-Y'),                                                      
                                                                                            'class' => array('form-control'),                                                           
                                                                                            'error' => false,
                                                                                            'readonly',
                                                                                      ));
                                                                                      ?>    
                                                                                      <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span> <br>  
                                                                                      </div>     
                                                                                                      
                                                                                    </div>
                                                                                  </div>	
                                                                      
                                                                    </td>
                                                                    <td>
                                                                        คลิ๊กเพื่อระบุระดับกระบวนการติดตาม <br><font color="red">* ท่านได้ส่งขอ เปิด/ปรับปรุงกระบวนวิชา เรียบร้อยแล้ว กรุณารอการตอบกลับการพิจารณา</font> 
                                                                        
                                                                                    <?php 
                                                                                      echo $this->Form->hidden('SpecialAndCooperative.id', array(
                                                                                        'value' => $proposal_id,
                                                                                        'class' => 'form-control',
                                                                                      )); 
                                                                                    ?>              
                                                                                    <div class="form-group has-feedback">      
                                                                                        
                                                                                        <?php  
                                                                                          echo $this->Form->radio('SpecialAndCooperative.special_and_cooperative_follow_status_id', 
                                                                                          $SpecialAndCooperativeFollowpasses, 
                                                                                          array(
                                                                                            'legend' => false, 
                                                                                            'value' => $proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'],
                                                                                            'disabled',                                                            
                                                                                            'separator' => '<br/>',
                                                                                            // 'required',
                                                                                            'style' => 'width: 25px; 
                                                                                                  height: 25px; 
                                                                                                  vertical-align:text-bottom;'));
                                                                                        ?>
                                                                                  <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                                                </div> 
                                                                                        
                                                                    </td>
                                                                  </tr>
                                                                    
                                                                </table>
                                                              </tr>
                                                              
                                                            </table>
                                      
                                                              <div class="form-group">
                                                                    
                                                                <div class="col-sm-12"> 
                                                                      <input type="submit" class="btn btn-success btn-lg btn-block" value="บันทึกข้อมูล" onclick="return confirm('ยืนยันการเพิ่มข้อมูล')">                                                                
                                                                </div>
                                                            </div>
                                                          -->
                                                  <?php } ?>

                                                 
                                                              
                                                            
                                                    
                                          </tbody>      
                        
                                        </div> 
                                      </div>         
                                    </form>
                                                
                              <?php } ?>
                              <?php  if ($admins['Admincurriculumrequest']['employee_status_id'] == 2) { ?>
                                
                                <form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                  <div class="panel panel-success">
                                    <div class="panel-heading"> 
                                      <div class="panel-title">
                                          &emsp;<i class="glyphicon glyphicon-book"></i>
                                          <font class="txt-content_SpecialAndCooperative26"> การพิจารณาขอเปิด/ปรับปรุงกระบวนวิชา <?php echo $proposal['SpecialAndCooperative']['code'] ?></font>
                                        </div> 
                                      </div>
                                      <div class="panel-body">
                                        <tbody>   
                                            <?php	if($proposal['SpecialAndCooperative']['special_and_cooperative_pass_id'] != 5 ){ ?>                                      
                                                  
                                              <?php	if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] >= 1 && $proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] <= 4){ ?>
                                                  <!-- *******Save file *** -->
                                                    <?php 
                                                
                                                        $required = 'กรุณากรอกข้อมูล';
                                                        
                                                        echo $this->Form->create('SpecialAndCooperative',array('role' => 'form','data-toggle' => 'validator')); 
                                                      
                                                        echo $this->Form->hidden('SpecialAndCooperative.id', array(
                                                          'value' => $proposal_id,
                                                          'data-error' => $required,
                                                          'label' => false,
                                                          'class' => 'form-control'
                                                        ));

                                                        
                                                    
                                                    ?>  
                                                      <table class="table table-bordered">
                                                        <tbody>
                                                          <tr>
                                                              <td>    
                                                              <div class="font_panel5">	
                                                                <h3 ><div class="numberCircle">1</div> แก้ไขข้อมูลรหัสกระบวนวิชา <?php echo $proposal['SpecialAndCooperative']['code'] ?> </h3>
                                                                
                                                              </div> 
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                              <td>
                                                                  <label class="col-sm-3 control-label"> รหัสกระบวนวิชา <font color="red">*</font> </label> 
                                                                  <div class="col-sm-9">
                                                                            <div class="form-group has-feedback">      
                                                                              <?php 
                                                                                    echo $this->Form->input('SpecialAndCooperative.code', array(
                                                                                        'type' => 'text',
                                                                                        'label' => false,
                                                                                        'div' => false,
                                                                                        'value' => $proposal['SpecialAndCooperative']['code'],
                                                                                        'class' => array('form-control css-require'),                                                           
                                                                                        'error' => false,
                                                                                        'required'
                                                                                        ));
                                                                                
                                                                                  ?>
                                                                         
                                                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                                      </div>
                                                                  </div>    
                                                              
                                                        
                                                              </td>
                                                          </tr>
                                                          <tr>
                                                            <td>
                                                                <label class="col-sm-3 control-label"> ชื่อวิชาภาษาไทย <font color="red">*</font> </label> 
                                                                <div class="col-sm-9">
                                                                          <div class="form-group has-feedback">      
                                                                            <?php 
                                                                                  echo $this->Form->input('SpecialAndCooperative.title', array(
                                                                                      'type' => 'text',
                                                                                      'label' => false,
                                                                                      'div' => false,
                                                                                      'value' => $proposal['SpecialAndCooperative']['title'],
                                                                                      'class' => array('form-control css-require'),                                                           
                                                                                      'error' => false,
                                                                                      'required'
                                                                                      ));
                                                                              
                                                                                ?>
                                                                                                                      
                                                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                                    </div>
                                                                </div>    
                                                            
                                                      
                                                            </td>
                                                          </tr> 
                                                          <tr>
                                                            <td>
                                                                <label class="col-sm-3 control-label"> ชื่อวิชาภาษาอังกฤษ <font color="red">*</font> </label> 
                                                                <div class="col-sm-9">
                                                                          <div class="form-group has-feedback">      
                                                                            <?php 
                                                                                  echo $this->Form->input('SpecialAndCooperative.title_eng', array(
                                                                                      'type' => 'text',
                                                                                      'label' => false,
                                                                                      'div' => false,
                                                                                      'value' => $proposal['SpecialAndCooperative']['title_eng'],
                                                                                      'class' => array('form-control css-require'),                                                           
                                                                                      'error' => false,
                                                                                      'required'
                                                                                      ));
                                                                              
                                                                                ?>
                                                                     
                                                                      
                                                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                                    </div>
                                                                </div>    
                                                            
                                                      
                                                            </td>
                                                          </tr> 
                                                          <tr>
                                                            <td>
                                                              <table class="table table-hover table-post" style="width: 100%;">  
                                                                <tr>
                                                                    <td>                
                                                                        <div class="font_panel5">	
                                                                          <h3 ><div class="numberCircle">2</div> อัพโหลดไฟล์ มคอ.3/4 ที่แก้ไขล่าสุด</h3>
                                                                          ท่านสามารถแนบไฟล์ word หรือ ไฟล์ pdf /แนบได้ทั้ง 2 ไฟล์  
                                                                        </div>
                                                                    </td>
                                                                </tr> 
                                                                <tr>
                                                                  <td>
                                                                    <label for="InfoDetail" class="addpost_input">อัปโหลดรูปภาพ และไฟล์เอกสารประกอบ</label> <font color="red">(กรณีเลือกมากกว่า 1 ไฟล์ ให้กด Ctrl หรือ Shift ค้างไว้และเลือกรูปภาพหรือไฟล์ที่ต้องการ)</font>
                                                                      <?php
                                                                          echo $this->Form->input('SpecialAndCooperativeDocument.files.', array('type' => 'file', 'multiple','accept' => '.xlsx,.xls,image/*, .doc, .docx, .ppt, .pptx, .txt, .pdf, .zip, .rar'));
                                                                      ?>
                                                                  </td>
                                                                </tr>                                                       
                                                                
                                                                
                                                              </table>
                                                            </td>
                                                          </tr>
                                                          
                                                        </tbody>
                                                      </table>
                                                      <hr>
                                                  <!-- **************************comment********************************		 -->
                                                          <?php 
                                                              $required = 'กรุณากรอกข้อมูล';
                                                              echo $this->Form->hidden('AnswerSpecialAndCooperative.special_and_cooperative_id', array(
                                                                'value' => $proposal_id,
                                                                'class' => 'form-control',
                                                              ));
                                                              echo $this->Form->hidden('AnswerSpecialAndCooperative.mis_employee_id', array(
                                                                'value' => $mis_employee_id,
                                                                'class' => 'form-control',
                                                              ));
                                                              echo $this->Form->hidden('AnswerSpecialAndCooperative.status', array(
                                                                'value' => 1,
                                                                'class' => 'form-control',
                                                              ));
                                                          ?>
                                                              <div class="panel panel-default" style="border-color: #FFF595!important">
                                                                  <div class="panel-heading" style="background-color: #FFF595!important;border-color: #FFF595!important">
                                                                      <h4 class="panel-title">
                                                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                                          <div class="font_panel5">
                                                                          <div class="numberCircle">3</div>  หากมีคำอธิบายเพิ่มเติม (ถ้ามี) คลิ๊กเพิ่มคำอธิบายที่นี่</div></a>
                                                                      </h4>
                                                                  </div>
                                                                <div id="collapseTwo" class="panel-collapse ">
                                                                  <div class="panel-body">
                                                                    <table>
                                                                      <tr>
                                                                        <td>รายละเอียด <font color="red">*</font></td>
                                                                          <td><div class="form-group has-feedback">      
                                                                                  <?php 
                                                                                        echo $this->Form->input('AnswerSpecialAndCooperative.Detail', array(
                                                                                            'type' => 'textarea',
                                                                                            'label' => false,
                                                                                            'div' => false,
                                                                                            'class' => 'ckeditor',                                                           
                                                                                            'error' => false,
                                                                                            'required'
                                                                                            ));
                                                                                    
                                                                                      ?>
                                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                                          </div>
                                                                        </td>
                                                                      </tr>
                                                                      <!-- <tr>
                                                                        <td>อัพโหลดเอกสาร/รูปภาพแนบ (ถ้ามี)</td>
                                                                          <td> <div class="form-group has-feedback">   
                                                                            <?php
                                                                                echo $this->Form->input('AnswerSpecialAndCooperativeDocument.files.', array('type' => 'file', 'multiple'));
                                                                                ?>
                                                                            </div>
                                                                        </td>
                                                                      </tr> -->
                                                                    </table>
                                                                        <!-- ************************   -->
                                                                  
                                                                  </div>
                                                                </div>
                                                              </div>
                                                        
                                                          <table class="table table-bordered">
                                                              <tr>
                                                                  <td>    
                                                                  <div class="font_panel5">	
                                                                    <h3 ><div class="numberCircle">4</div> สถานะกระบวนวิชา<?php echo $proposal['SpecialAndCooperative']['code'] ?> </h3>
                                                                    
                                                                  </div> 
                                                                  </td>
                                                              </tr>
                                                              <tr>
                                                                <td>
                                                                  
                                                                    สถานะ <img src="https://www1.reg.cmu.ac.th/ugradapply/images/update.gif">
                                                                      
                                                                      <?php 
                                                                        echo $this->Form->hidden('SpecialAndCooperative.id', array(
                                                                          'value' => $proposal_id,
                                                                          'class' => 'form-control',
                                                                        )); 
                                                                      ?>              
                                                                        <div class="form-group has-feedback">      
                                                                            
                                                                            <?php  
                                                                              $types = array( 
                                                                                '4' => '&nbsp; <font color="green"><b> ยืนยันการแก้ไข</b></font>'
                                                                               );	
                                                                              //  $passes 
                                                                              echo $this->Form->radio('SpecialAndCooperative.special_and_cooperative_pass_id', 
                                                                              $types, 
                                                                              array(
                                                                                'legend' => false,   
                                                                                'value' => 4,                                                          
                                                                                'separator' => '<br/>',
                                                                                'required',
                                                                                'readonly',
                                                                                'style' => 'width: 25px; 
                                                                                      height: 25px; 
                                                                                      vertical-align:text-bottom;'));
                                                                            ?>
                                                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                                    </div>
                                                                    
                                                                    <!-- <label class="col-sm-3 control-label"> ระบุครั้งที่ </label> 
                                                                            <div class="col-sm-7">
                                                                              <div class="form-group has-feedback">      
                                                                                      <?php 
                                                                                            echo $this->Form->input('SpecialAndCooperative.num_id', array(
                                                                                                'type' => 'text',
                                                                                                'label' => false,
                                                                                                'div' => false,
                                                                                                'class' => array('form-control'),                                                          
                                                                                                'error' => false,
                                                                                                // 'required'
                                                                                                ));
                                                                                        
                                                                                          ?>
                                                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                                              </div>
                                                                            </div>
                                                                            <label class="col-sm-3 control-label">ระบุวันที่   <br>
                                                                            </label>
                                                                                <div class="col-sm-7">
                                                                                  <div class="input-group date">                        
                                                                                  <?php 
                                                                                  echo $this->Form->input('SpecialAndCooperative.postdate', array(                          
                                                                                        'type' => 'text',
                                                                                        'label' => false,  
                                                                                        'value' => date('d-m-Y'),                                                      
                                                                                        'class' => array('form-control'),                                                           
                                                                                        'error' => false,
                                                                                        'required'
                                                                                  ));
                                                                                  ?>    
                                                                                  <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span> <br>  
                                                                                  </div>     
                                                                                                  
                                                                                </div>
                                                                              </div>	 -->
                                                                  
                                                                </td>
                                                              
                                                            </tr>
                                                              
                                                          </table>

                                                        
                                                        <br>                           
                                                        <div class="form-group">
                                                              
                                                          <div class="col-sm-12"> 
                                                                <input type="submit" class="btn btn-success btn-lg btn-block" value="บันทึกข้อมูล"  >                                                                
                                                          </div>
                                                      </div>
                                              
                                                
                                                

                                              <?php }else{  ?>
                                                        <?php 
                                                        $required = 'กรุณากรอกข้อมูล';
                                                              echo $this->Form->hidden('AnswerSpecialAndCooperative.special_and_cooperative_id', array(
                                                                'value' => $proposal_id,
                                                                'class' => 'form-control',
                                                              ));
                                                              echo $this->Form->hidden('AnswerSpecialAndCooperative.mis_employee_id', array(
                                                                'value' => $mis_employee_id,
                                                                'class' => 'form-control',
                                                              ));
                                                              echo $this->Form->hidden('AnswerSpecialAndCooperative.status', array(
                                                                'value' => 1,
                                                                'class' => 'form-control',
                                                              ));
                                                        ?>
                                                        <font color="red">
                                                              กระบวนวิชา <?php echo $proposal['SpecialAndCooperative']['code']; ?> นี้ได้เสนอเข้าพิจารณาระดับคณะเรียบร้อยแล้ว กรุณารอการตอบกลับการพิจารณา <br>
                                                              *หมายเหตุ : การแก้ไขจะกระทำได้ ต่อเมื่ออยู่ในสถานะ อาจารย์/ภาควิชา
                                                        </font>
                                                      <!-- 
                                                        <table>
                                                        <tr>
                                                            <td>รายละเอียด <font color="red">*</font></td>
                                                              <td><div class="form-group has-feedback">      
                                                                      <?php 
                                                                            echo $this->Form->input('AnswerSpecialAndCooperative.Detail', array(
                                                                                'type' => 'textarea',
                                                                                'label' => false,
                                                                                'div' => false,
                                                                                'class' => 'ckeditor',                                                           
                                                                                'error' => false,
                                                                                'required'
                                                                                ));
                                                                        
                                                                          ?>
                                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                              </div>
                                                            </td>
                                                          </tr>
                                                          <tr>
                                                            <td>อัพโหลดเอกสาร/รูปภาพแนบ (ถ้ามี)</td>
                                                              <td> <div class="form-group has-feedback">   
                                                                <?php
                                                                    echo $this->Form->input('AnswerSpecialAndCooperativeDocument.files.', array('type' => 'file', 'multiple'));
                                                                    ?>
                                                                </div>
                                                            </td>
                                                          </tr> 
                                                          <tr>
                                                            <table class="table">
                                                              <tr>
                                                                <td>
                                                                  
                                                                    คลิ๊กเพื่อระบุสถานะ <br><font color="red">* ท่านได้ส่งขอ เปิด/ปรับปรุงกระบวนวิชา เรียบร้อยแล้ว กรุณารอการตอบกลับการพิจารณา</font>  
                                                                      
                                                                      <?php 
                                                                        echo $this->Form->hidden('SpecialAndCooperative.id', array(
                                                                          'value' => $proposal_id,
                                                                          'class' => 'form-control',
                                                                        )); 
                                                                      ?>              
                                                                      <div class="form-group has-feedback">      
                                                                          
                                                                          <?php  
                                                                            echo $this->Form->radio('SpecialAndCooperative.special_and_cooperative_pass_id', 
                                                                            $passes, 
                                                                            array(
                                                                              'legend' => false,   
                                                                              'value' => $proposal['SpecialAndCooperative']['special_and_cooperative_pass_id'],                                                          
                                                                              'separator' => '<br/>',
                                                                              'disabled',
                                                                              // 'required',
                                                                              'style' => 'width: 25px; 
                                                                                    height: 25px; 
                                                                                    vertical-align:text-bottom;'));
                                                                          ?>
                                                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                                    </div> 
                                                                  <label class="col-sm-3 control-label"> เสนอ/เห็นชอบ/มีการแก้ไข ครั้งที่ <br>
                                                                            </label>
                                                                            <div class="col-sm-7">
                                                                              <div class="form-group has-feedback">      
                                                                                      <?php 
                                                                                            echo $this->Form->input('SpecialAndCooperative.num_id', array(
                                                                                                'type' => 'text',
                                                                                                'label' => false,
                                                                                                'div' => false,
                                                                                                'class' => array('form-control'),                                                          
                                                                                                'error' => false,
                                                                                                'readonly',
                                                                                                ));
                                                                                        
                                                                                          ?>
                                                                                <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                                              </div>
                                                                            </div>
                                                                            <label class="col-sm-3 control-label">วันที่  <br>
                                                                            </label>
                                                                                <div class="col-sm-7">
                                                                                  <div class="input-group date">                        
                                                                                  <?php 
                                                                                  echo $this->Form->input('SpecialAndCooperative.postdate', array(                          
                                                                                        'type' => 'text',
                                                                                        'label' => false,  
                                                                                        'value' => date('d-m-Y'),                                                      
                                                                                        'class' => array('form-control'),                                                           
                                                                                        'error' => false,
                                                                                        'readonly',
                                                                                  ));
                                                                                  ?>    
                                                                                  <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span> <br>  
                                                                                  </div>     
                                                                                                  
                                                                                </div>
                                                                              </div>	
                                                                  
                                                                </td>
                                                                <td>
                                                                    คลิ๊กเพื่อระบุระดับกระบวนการติดตาม <br><font color="red">* ท่านได้ส่งขอ เปิด/ปรับปรุงกระบวนวิชา เรียบร้อยแล้ว กรุณารอการตอบกลับการพิจารณา</font> 
                                                                    
                                                                                <?php 
                                                                                  echo $this->Form->hidden('SpecialAndCooperative.id', array(
                                                                                    'value' => $proposal_id,
                                                                                    'class' => 'form-control',
                                                                                  )); 
                                                                                ?>              
                                                                                <div class="form-group has-feedback">      
                                                                                    
                                                                                    <?php  
                                                                                      echo $this->Form->radio('SpecialAndCooperative.special_and_cooperative_follow_status_id', 
                                                                                      $SpecialAndCooperativeFollowpasses, 
                                                                                      array(
                                                                                        'legend' => false, 
                                                                                        'value' => $proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'],
                                                                                        'disabled',                                                            
                                                                                        'separator' => '<br/>',
                                                                                        // 'required',
                                                                                        'style' => 'width: 25px; 
                                                                                              height: 25px; 
                                                                                              vertical-align:text-bottom;'));
                                                                                    ?>
                                                                              <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                                            </div> 
                                                                                    
                                                                </td>
                                                              </tr>
                                                                
                                                            </table>
                                                          </tr>
                                                          
                                                        </table>
                                  
                                                          <div class="form-group">
                                                                
                                                            <div class="col-sm-12"> 
                                                                  <input type="submit" class="btn btn-success btn-lg btn-block" value="บันทึกข้อมูล" onclick="return confirm('ยืนยันการเพิ่มข้อมูล')">                                                                
                                                            </div>
                                                        </div>
                                                      -->
                                              <?php } ?>

                                            <?php }else{ ?> 
                                              <?php  if($proposal['SpecialAndCooperative']['file'] != null){ ?>
                                                  <a href="<?php echo $this->Html->url('/files/students/SpecialAndCooperatives/'.$proposal['SpecialAndCooperative']['mis_employee_id'].'/') , $proposal['SpecialAndCooperative']['file']; ?>" 
                                                  class="btn btn-active" target="_blank">
                                                  <span class="glyphicon glyphicon-print" aria-hidden="true"></span> ไฟล์ มคอ.3-4 ฉบับสมบูรณ์</a>
                                                
                                              <?php } ?>

                                            <?php } ?>
                                                          
                                                        
                                                
                                      </tbody>      
                    
                                    </div> 
                                  </div>         
                                </form>
                                              
                              <?php } ?>

                              <?php if ($admins['Admincurriculumrequest']['employee_status_id'] == 1) { ?> 
                                <?php if ($admins['Admincurriculumrequest']['employees_status_id'] == 3) { ?> 
                                  <form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                    <div class="panel panel-success">
                                      <div class="panel-heading"> 
                                        <div class="panel-title">
                                            &emsp;<i class="glyphicon glyphicon-book"></i>
                                            <font class="txt-content_SpecialAndCooperative26"> การพิจารณาขอเปิด/ปรับปรุงกระบวนวิชา <?php echo $proposal['SpecialAndCooperative']['code'] ?></font>
                                          </div> 
                                        </div>
                                        <div class="panel-body">
                                          <tbody>   
                                              <?php	if($proposal['SpecialAndCooperative']['special_and_cooperative_pass_id'] != 5 ){ ?>                                      
                                                
                                                <?php	if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] >= 4 &&
                                                         $proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] <= 9){ ?>
                                                   
                                                    <!-- **************************comment********************************		 -->
                                                    <?php 
                                                        $required = 'กรุณากรอกข้อมูล';
                                                        echo $this->Form->hidden('AnswerSpecialAndCooperative.special_and_cooperative_id', array(
                                                          'value' => $proposal_id,
                                                          'class' => 'form-control',
                                                        ));
                                                        echo $this->Form->hidden('AnswerSpecialAndCooperative.mis_employee_id', array(
                                                          'value' => $mis_employee_id,
                                                          'class' => 'form-control',
                                                        ));
                                                        echo $this->Form->hidden('AnswerSpecialAndCooperative.status', array(
                                                          'value' => 1,
                                                          'class' => 'form-control',
                                                        ));
                                                    ?>
                                                    <?php	if ($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 9) { ?> 
                                                        <?php 
                                                          echo $this->Form->hidden('SpecialAndCooperative.id', array(
                                                            'value' => $proposal_id,
                                                            'class' => 'form-control',
                                                          )); 
                                                        ?>   
                                                        <table class="table table-hover table-post" style="width: 100%;">  
                                                          <tr>
                                                              <td>                
                                                                  <div class="font_panel5">	
                                                                    <h3 ><div class="numberCircle">1</div> อัพโหลดไฟล์ มคอ.3/4 ที่สมบูรณ์ <img src="https://www1.reg.cmu.ac.th/ugradapply/images/update.gif"></h3>
                                                                    ท่านสามารถแนบไฟล์ word หรือ ไฟล์ pdf /แนบได้ทั้ง 2 ไฟล์  
                                                                  </div>
                                                              </td>
                                                          </tr> 
                                                          <tr>
                                                            <td>
                                                              <label for="InfoDetail" class="addpost_input">อัปโหลดรูปภาพ และไฟล์เอกสารประกอบ</label> <font color="red">(กรณีเลือกมากกว่า 1 ไฟล์ ให้กด Ctrl หรือ Shift ค้างไว้และเลือกรูปภาพหรือไฟล์ที่ต้องการ)</font>
                                                                <?php
                                                                    echo $this->Form->input('SpecialAndCooperativePassDocument.files.', array('type' => 'file', 'multiple','accept' => '.xlsx,.xls,image/*, .doc, .docx, .ppt, .pptx, .txt, .pdf, .zip, .rar'));
                                                                ?>
                                                            </td>
                                                          </tr>                                                       
                                                          <!-- <tr>
                                                            <td>
                                                            <div class="row">	
                                                              <div class="form-group col-md-6">                                
                                                                <label>แนบไฟล์ Word</label>
                                                                <?php
                                                                    echo $this->Form->input('SpecialAndCooperativeDocument.fileDOC.', array(
                                                                      'type' => 'file',
                                                                      'accept' => '.doc , .docx',
                                                                      ));
                                                                  ?>
                                                              </div>
                                                              <div class="form-group col-md-6">                                
                                                                <label>หรือ แนบไฟล์ PDF</label>
                                                                <?php
                                                                    
                                                                    echo $this->Form->input('SpecialAndCooperativeDocument.filePDF.', array(
                                                                      'type' => 'file',
                                                                      'accept' => '.pdf',
                                                                      ));
                                                                  ?>
                                                              </div>			
  
                                                              
                                                            </td>
                                                          </tr> -->
                                                          
                                                        </table>
                                                    <?php }else { ?>  
                                                        <div class="panel panel-default" style="border-color: #FFF595!important">
                                                            <div class="panel-heading" style="background-color: #FFF595!important;border-color: #FFF595!important">
                                                                <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                                    <div class="font_panel5">
                                                                    <div class="numberCircle">1</div>  หากมีคำอธิบายเพิ่มเติม (ถ้ามี) คลิ๊กเพิ่มคำอธิบายที่นี่</div></a>
                                                                </h4>
                                                            </div>
                                                          <div id="collapseTwo" class="panel-collapse  ">
                                                            <div class="panel-body">
                                                              <table>
                                                                <tr>
                                                                  <td>รายละเอียด <font color="red">*</font></td>
                                                                    <td><div class="form-group has-feedback">      
                                                                            <?php 
                                                                                  echo $this->Form->input('AnswerSpecialAndCooperative.Detail', array(
                                                                                      'type' => 'textarea',
                                                                                      'label' => false,
                                                                                      'div' => false,
                                                                                      'class' => 'ckeditor',                                                           
                                                                                      'error' => false,
                                                                                      'required'
                                                                                      ));
                                                                              
                                                                                ?>
                                                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                                    </div>
                                                                  </td>
                                                                </tr>
                                                                <tr>
                                                                  <td>อัพโหลดเอกสารประกอบการพิจารณา (ถ้ามี)</td>
                                                                    <td> <div class="form-group has-feedback">   
                                                                      <?php
                                                                          echo $this->Form->input('AnswerSpecialAndCooperativeDocument.files.', array('type' => 'file', 'multiple'));
                                                                          ?>
                                                                      </div>
                                                                  </td>
                                                                </tr>
                                                              </table>
                                                                  <!-- ************************   -->
                                                            
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <table class="table">
                                                            <tr>
                                                              <td>
                                                                
                                                                  คลิ๊กเพื่อระบุสถานะ <br><font color="red">*</font> <img src="https://www1.reg.cmu.ac.th/ugradapply/images/update.gif">
                                                                    
                                                                    <?php 
                                                                      echo $this->Form->hidden('SpecialAndCooperative.id', array(
                                                                        'value' => $proposal_id,
                                                                        'class' => 'form-control',
                                                                      )); 
                                                                    ?>              
                                                                    <div class="form-group has-feedback">      
                                                                        
                                                                        <?php  
                                                                          echo $this->Form->radio('SpecialAndCooperative.special_and_cooperative_pass_id', 
                                                                          $passes, 
                                                                          array(
                                                                            'legend' => false,   
                                                                            // 'value' => $proposal['SpecialAndCooperative']['special_and_cooperative_pass_id'],                                                          
                                                                            'separator' => '<br/>',
                                                                            // 'required',
                                                                            'style' => 'width: 25px; 
                                                                                  height: 25px; 
                                                                                  vertical-align:text-bottom;'));
                                                                        ?>
                                                                  <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                                </div>
                                                                <label class="col-sm-12 control-label"> <font color="red">เลือกประเภทการเข้าที่ประชุม/แจ้งเวียน</font>  </label> <br>
                                                                  <?php  
                                                                      $types = array( 
                                                                        '1' => '&nbsp; <b> เข้าที่ประชุม</b>',
                                                                        '2' => '&nbsp; <b> แจ้งเวียน</b>',
                                                                        );	
                                                                      //  $passes 
                                                                      echo $this->Form->radio('SpecialAndCooperative.special_and_cooperative_meeting_type_id', 
                                                                      $types, 
                                                                      array(
                                                                        'legend' => false,                                    
                                                                        'separator' => '<br/>',
                                                                        'required',
                                                                        'readonly',
                                                                        'style' => 'width: 25px; 
                                                                              height: 25px; 
                                                                              vertical-align:text-bottom;'));
                                                                  ?>
                                                                 <br>
                                                                <label class="col-sm-3 control-label"> ครั้งที่ </label> 
                                                                          <div class="col-sm-7">
                                                                            <div class="form-group has-feedback">      
                                                                                    <?php 
                                                                                          echo $this->Form->input('SpecialAndCooperative.num_id', array(
                                                                                              'type' => 'text',
                                                                                              'label' => false,
                                                                                              'div' => false,
                                                                                              'class' => array('form-control'),                                                          
                                                                                              'error' => false,
                                                                                              
                                                                                              ));
                                                                                      
                                                                                        ?>
                                                                              <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                                            </div>
                                                                          </div>
                                                                          <label class="col-sm-3 control-label">วันที่  <br>
                                                                          </label>
                                                                              <div class="col-sm-7">
                                                                                <div class="input-group date">                        
                                                                                <?php 
                                                                                echo $this->Form->input('SpecialAndCooperative.postdate', array(                          
                                                                                      'type' => 'text',
                                                                                      'label' => false,  
                                                                                      'value' => date('d-m-Y'),                                                      
                                                                                      'class' => array('form-control'),                                                           
                                                                                      'error' => false,
                                                                                      'required'
                                                                                ));
                                                                                ?>    
                                                                                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span> <br>  
                                                                                </div>     
                                                                                                
                                                                              </div>
                                                                            </div>	
                                                                
                                                              </td>
                                                              <td>
                                                                  ระดับกระบวนการติดตาม <br><font color="red">*</font> <img src="https://www1.reg.cmu.ac.th/ugradapply/images/update.gif">
                                                                  
                                                                              <?php 
                                                                                echo $this->Form->hidden('SpecialAndCooperative.id', array(
                                                                                  'value' => $proposal_id,
                                                                                  'class' => 'form-control',
                                                                                )); 
                                                                              ?>              
                                                                              <div class="form-group has-feedback">      
                                                                                  
                                                                                  <?php  
                                                                                    echo $this->Form->radio('SpecialAndCooperative.special_and_cooperative_follow_status_id', 
                                                                                    $SpecialAndCooperativeFollowpasses, 
                                                                                    array(
                                                                                      'legend' => false, 
                                                                                      'value' => $proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'],                                                            
                                                                                      'separator' => '<br/>',
                                                                                      'required',
                                                                                      'readonly',
                                                                                      'style' => 'width: 25px; 
                                                                                            height: 25px; 
                                                                                            vertical-align:text-bottom;'));
                                                                                  ?>
                                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                                          </div> 
                                                                                  
                                                              </td>
                                                            </tr>
                                                              
                                                        </table>
                                                        <br> 
                                                        <!-- <table class="table table-hover table-post" style="width: 100%;">  
                                                          <tr>
                                                              <td>                
                                                                  <div class="font_panel5">	
                                                                    <h3 ><div class="numberCircle">2</div> อัพโหลดไฟล์ มคอ.3/4 (ถ้ามี)</h3>
                                                                     
                                                                  </div>
                                                              </td>
                                                          </tr> 
                                                          <tr>
                                                            <td>
                                                              <label for="InfoDetail" class="addpost_input">อัปโหลดรูปภาพ และไฟล์เอกสารประกอบ</label> <font color="red">(กรณีเลือกมากกว่า 1 ไฟล์ ให้กด Ctrl หรือ Shift ค้างไว้และเลือกรูปภาพหรือไฟล์ที่ต้องการ)</font>
                                                                <?php
                                                                    echo $this->Form->input('SpecialAndCooperativeDocument.files.', array('type' => 'file', 'multiple','accept' => '.xlsx,.xls,image/*, .doc, .docx, .ppt, .pptx, .txt, .pdf, .zip, .rar'));
                                                                ?>
                                                            </td>
                                                          </tr>                                                       
                                                      
                                                          
                                                        </table>                           -->
  
                                                    <?php } ?> 
                                                        <div class="form-group">
                                                                
                                                            <div class="col-sm-12"> 
                                                                  <input type="submit" class="btn btn-success btn-lg btn-block" value="บันทึกข้อมูล"  >                                                                
                                                            </div>
                                                        </div>
                                                
                                                  
                                                  
  
                                                <?php }else{  ?>
                                                          <?php 
                                                          $required = 'กรุณากรอกข้อมูล';
                                                                echo $this->Form->hidden('AnswerSpecialAndCooperative.special_and_cooperative_id', array(
                                                                  'value' => $proposal_id,
                                                                  'class' => 'form-control',
                                                                ));
                                                                echo $this->Form->hidden('AnswerSpecialAndCooperative.mis_employee_id', array(
                                                                  'value' => $mis_employee_id,
                                                                  'class' => 'form-control',
                                                                ));
                                                                echo $this->Form->hidden('AnswerSpecialAndCooperative.status', array(
                                                                  'value' => 1,
                                                                  'class' => 'form-control',
                                                                ));
                                                          ?>
                                                          <font color="red">ไม่สามารถปรับระบบได้ กรุณารอการตอบกลับจากอาจารย์/ภาควิชา</font>
                                                        
                                                <?php } ?> 
                                              <?php } ?>
                                                            
                                                          
                                                  
                                        </tbody>      
                      
                                      </div> 
                                    </div>         
                                  </form>
                                <?php }else{ ?>
                                  <form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">
                                    <div class="panel panel-success">
                                      <div class="panel-heading"> 
                                        <div class="panel-title">
                                            &emsp;<i class="glyphicon glyphicon-book"></i>
                                            <font class="txt-content_SpecialAndCooperative26"> การพิจารณาขอเปิด/ปรังปรุงกระบวนวิชา </font>
                                          </div> 
                                        </div>
                                        <div class="panel-body">
                                          <tbody>   
                                              <?php	if($proposal['SpecialAndCooperative']['special_and_cooperative_pass_id'] != 5 ){ ?>                                      
                                                
                                                <?php	if($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] >= 1 &&
                                                         $proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] <= 10){ ?>
                                                   
                                                    <!-- **************************comment********************************		 -->
                                                    <?php 
                                                        $required = 'กรุณากรอกข้อมูล';
                                                        echo $this->Form->hidden('AnswerSpecialAndCooperative.special_and_cooperative_id', array(
                                                          'value' => $proposal_id,
                                                          'class' => 'form-control',
                                                        ));
                                                        echo $this->Form->hidden('AnswerSpecialAndCooperative.mis_employee_id', array(
                                                          'value' => $mis_employee_id,
                                                          'class' => 'form-control',
                                                        ));
                                                        echo $this->Form->hidden('AnswerSpecialAndCooperative.status', array(
                                                          'value' => 1,
                                                          'class' => 'form-control',
                                                        ));
                                                    ?>
                                                    <?php	if ($proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'] > 9) { ?> 
                                                        <?php 
                                                          echo $this->Form->hidden('SpecialAndCooperative.id', array(
                                                            'value' => $proposal_id,
                                                            'class' => 'form-control',
                                                          )); 
                                                        ?>   
                                                        <table class="table table-hover table-post" style="width: 100%;">  
                                                          <tr>
                                                              <td>                
                                                                  <div class="font_panel5">	
                                                                    <h3 ><div class="numberCircle">1</div> อัพโหลดไฟล์ มคอ.3/4 ที่สมบูรณ์ <img src="https://www1.reg.cmu.ac.th/ugradapply/images/update.gif"></h3>
                                                                    ท่านสามารถแนบไฟล์ word หรือ ไฟล์ pdf /แนบได้ทั้ง 2 ไฟล์  
                                                                  </div>
                                                              </td>
                                                          </tr> 
                                                          <tr>
                                                            <td>
                                                              <label for="InfoDetail" class="addpost_input">อัปโหลดรูปภาพ และไฟล์เอกสารประกอบ</label> <font color="red">(กรณีเลือกมากกว่า 1 ไฟล์ ให้กด Ctrl หรือ Shift ค้างไว้และเลือกรูปภาพหรือไฟล์ที่ต้องการ)</font>
                                                                <?php
                                                                    echo $this->Form->input('SpecialAndCooperativePassDocument.files.', array('type' => 'file', 'multiple','accept' => '.xlsx,.xls,image/*, .doc, .docx, .ppt, .pptx, .txt, .pdf, .zip, .rar'));
                                                                ?>
                                                            </td>
                                                          </tr>                                                       
                                                          <!-- <tr>
                                                            <td>
                                                            <div class="row">	
                                                              <div class="form-group col-md-6">                                
                                                                <label>แนบไฟล์ Word</label>
                                                                <?php
                                                                    echo $this->Form->input('SpecialAndCooperativeDocument.fileDOC.', array(
                                                                      'type' => 'file',
                                                                      'accept' => '.doc , .docx',
                                                                      ));
                                                                  ?>
                                                              </div>
                                                              <div class="form-group col-md-6">                                
                                                                <label>หรือ แนบไฟล์ PDF</label>
                                                                <?php
                                                                    
                                                                    echo $this->Form->input('SpecialAndCooperativeDocument.filePDF.', array(
                                                                      'type' => 'file',
                                                                      'accept' => '.pdf',
                                                                      ));
                                                                  ?>
                                                              </div>			
  
                                                              
                                                            </td>
                                                          </tr> -->
                                                          
                                                        </table>
                                                    <?php }else { ?>  
                                                        <div class="panel panel-default" style="border-color: #FFF595!important">
                                                            <div class="panel-heading" style="background-color: #FFF595!important;border-color: #FFF595!important">
                                                                <h4 class="panel-title">
                                                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                                                    <div class="font_panel5">
                                                                    <div class="numberCircle">1</div>  หากมีคำอธิบายเพิ่มเติม (ถ้ามี) คลิ๊กเพิ่มคำอธิบายที่นี่</div></a>
                                                                </h4>
                                                            </div>
                                                          <div id="collapseTwo" class="panel-collapse  ">
                                                            <div class="panel-body">
                                                              <table>
                                                                <tr>
                                                                  <td>รายละเอียด <font color="red">*</font></td>
                                                                    <td><div class="form-group has-feedback">      
                                                                            <?php 
                                                                                  echo $this->Form->input('AnswerSpecialAndCooperative.Detail', array(
                                                                                      'type' => 'textarea',
                                                                                      'label' => false,
                                                                                      'div' => false,
                                                                                      'class' => 'ckeditor',                                                           
                                                                                      'error' => false,
                                                                                      'required'
                                                                                      ));
                                                                              
                                                                                ?>
                                                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                                    </div>
                                                                  </td>
                                                                </tr>
                                                                <tr>
                                                                  <td>อัพโหลดเอกสารประกอบการพิจารณา (ถ้ามี)</td>
                                                                    <td> <div class="form-group has-feedback">   
                                                                      <?php
                                                                          echo $this->Form->input('AnswerSpecialAndCooperativeDocument.files.', array('type' => 'file', 'multiple'));
                                                                          ?>
                                                                      </div>
                                                                  </td>
                                                                </tr>
                                                              </table>
                                                                  <!-- ************************   -->
                                                            
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <table class="table">
                                                            <tr>
                                                              <td>
                                                                
                                                                  คลิ๊กเพื่อระบุสถานะ <br><font color="red">*</font> <img src="https://www1.reg.cmu.ac.th/ugradapply/images/update.gif">
                                                                    
                                                                    <?php 
                                                                      echo $this->Form->hidden('SpecialAndCooperative.id', array(
                                                                        'value' => $proposal_id,
                                                                        'class' => 'form-control',
                                                                      )); 
                                                                    ?>              
                                                                    <div class="form-group has-feedback">      
                                                                        
                                                                        <?php  
                                                                          echo $this->Form->radio('SpecialAndCooperative.special_and_cooperative_pass_id', 
                                                                          $passes, 
                                                                          array(
                                                                            'legend' => false,   
                                                                            // 'value' => $proposal['SpecialAndCooperative']['special_and_cooperative_pass_id'],                                                          
                                                                            'separator' => '<br/>',
                                                                            // 'required',
                                                                            'style' => 'width: 25px; 
                                                                                  height: 25px; 
                                                                                  vertical-align:text-bottom;'));
                                                                        ?>
                                                                  <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                                </div>
                                                                <label class="col-sm-12 control-label"> <font color="red">เลือกประเภทการเข้าที่ประชุม/แจ้งเวียน</font>  </label> <br>
                                                                  <?php  
                                                                      $types = array( 
                                                                        '1' => '&nbsp; <b> เข้าที่ประชุม</b>',
                                                                        '2' => '&nbsp; <b> แจ้งเวียน</b>',
                                                                        );	
                                                                      //  $passes 
                                                                      echo $this->Form->radio('SpecialAndCooperative.special_and_cooperative_meeting_type_id', 
                                                                      $types, 
                                                                      array(
                                                                        'legend' => false,                                      
                                                                        'separator' => '<br/>',
                                                                        'required',
                                                                        'readonly',
                                                                        'style' => 'width: 25px; 
                                                                              height: 25px; 
                                                                              vertical-align:text-bottom;'));
                                                                  ?><br>
                                                                <label class="col-sm-3 control-label"> ครั้งที่ </label> 
                                                                          <div class="col-sm-7">
                                                                            <div class="form-group has-feedback">      
                                                                                    <?php 
                                                                                          echo $this->Form->input('SpecialAndCooperative.num_id', array(
                                                                                              'type' => 'text',
                                                                                              'label' => false,
                                                                                              'div' => false,
                                                                                              'class' => array('form-control'),                                                          
                                                                                              'error' => false,
                                                                                              // 'required'
                                                                                              ));
                                                                                      
                                                                                        ?>
                                                                              <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                                            </div>
                                                                          </div>
                                                                          <label class="col-sm-3 control-label">วันที่  <br>
                                                                          </label>
                                                                              <div class="col-sm-7">
                                                                                <div class="input-group date">                        
                                                                                <?php 
                                                                                echo $this->Form->input('SpecialAndCooperative.postdate', array(                          
                                                                                      'type' => 'text',
                                                                                      'label' => false,  
                                                                                      'value' => date('d-m-Y'),                                                      
                                                                                      'class' => array('form-control'),                                                           
                                                                                      'error' => false,
                                                                                      'required'
                                                                                ));
                                                                                ?>    
                                                                                <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span> <br>  
                                                                                </div>     
                                                                                                
                                                                              </div>
                                                                            </div>	
                                                                
                                                              </td>
                                                              <td>
                                                                  ระดับกระบวนการติดตาม <br><font color="red">*</font> <img src="https://www1.reg.cmu.ac.th/ugradapply/images/update.gif">
                                                                  
                                                                              <?php 
                                                                                echo $this->Form->hidden('SpecialAndCooperative.id', array(
                                                                                  'value' => $proposal_id,
                                                                                  'class' => 'form-control',
                                                                                )); 
                                                                              ?>              
                                                                              <div class="form-group has-feedback">      
                                                                                  
                                                                                  <?php  
                                                                                    echo $this->Form->radio('SpecialAndCooperative.special_and_cooperative_follow_status_id', 
                                                                                    $SpecialAndCooperativeFollowpasses, 
                                                                                    array(
                                                                                      'legend' => false, 
                                                                                      'value' => $proposal['SpecialAndCooperative']['special_and_cooperative_follow_status_id'],                                                            
                                                                                      'separator' => '<br/>',
                                                                                      'required',
                                                                                      'style' => 'width: 25px; 
                                                                                            height: 25px; 
                                                                                            vertical-align:text-bottom;'));
                                                                                  ?>
                                                                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                                          </div> 
                                                                                  
                                                              </td>
                                                            </tr>
                                                              
                                                        </table>
                                                        <br> 
                                                        <!-- <table class="table table-hover table-post" style="width: 100%;">  
                                                          <tr>
                                                              <td>                
                                                                  <div class="font_panel5">	
                                                                    <h3 ><div class="numberCircle">2</div> อัพโหลดไฟล์ มคอ.3/4 (ถ้ามี)</h3>
                                                                     
                                                                  </div>
                                                              </td>
                                                          </tr> 
                                                          <tr>
                                                            <td>
                                                              <label for="InfoDetail" class="addpost_input">อัปโหลดรูปภาพ และไฟล์เอกสารประกอบ</label> <font color="red">(กรณีเลือกมากกว่า 1 ไฟล์ ให้กด Ctrl หรือ Shift ค้างไว้และเลือกรูปภาพหรือไฟล์ที่ต้องการ)</font>
                                                                <?php
                                                                    echo $this->Form->input('SpecialAndCooperativeDocument.files.', array('type' => 'file', 'multiple','accept' => '.xlsx,.xls,image/*, .doc, .docx, .ppt, .pptx, .txt, .pdf, .zip, .rar'));
                                                                ?>
                                                            </td>
                                                          </tr>                                                       
                                                      
                                                          
                                                        </table>                           -->
  
                                                    <?php } ?> 
                                                        <div class="form-group">
                                                                
                                                            <div class="col-sm-12"> 
                                                                  <input type="submit" class="btn btn-success btn-lg btn-block" value="บันทึกข้อมูล"  >                                                                
                                                            </div>
                                                        </div>
                                                
                                                  
                                                  
  
                                                <?php }else{  ?>
                                                          <?php 
                                                          $required = 'กรุณากรอกข้อมูล';
                                                                echo $this->Form->hidden('AnswerSpecialAndCooperative.special_and_cooperative_id', array(
                                                                  'value' => $proposal_id,
                                                                  'class' => 'form-control',
                                                                ));
                                                                echo $this->Form->hidden('AnswerSpecialAndCooperative.mis_employee_id', array(
                                                                  'value' => $mis_employee_id,
                                                                  'class' => 'form-control',
                                                                ));
                                                                echo $this->Form->hidden('AnswerSpecialAndCooperative.status', array(
                                                                  'value' => 1,
                                                                  'class' => 'form-control',
                                                                ));
                                                          ?>
                                                          <font color="red">* ไม่สามารถปรับระบบได้ </font>
                                                        
                                                <?php } ?> 
                                              <?php } ?>
                                                            
                                                          
                                                  
                                        </tbody>      
                      
                                      </div> 
                                    </div>         
                                  </form>

                                <?php } ?>
                             
                                
                              <?php } ?>
                            </div>
                            <!-- ********** -->
                          </div> 
                          <div class="tab-pane fade" id="tab9primary">
                            
                            
                          </div> 
                          <div class="tab-pane fade" id="tab10primary">
                          
                          </div>
                    
                      
                        <!-- ถึงตรงนี้ -->
                      </div>
                    </div>
                  </div>

              <!-- ถึงตรงนี้ -->

              
            </div>
            
		</div>
	 
       
            
              
     
      <div class="col-md-6"> 
        <div class="panel panel-warning">
              <div class="panel-heading"> 
              &emsp; <i class="glyphicon glyphicon-list-alt"></i>
              <font class="txt-content_SpecialAndCooperative26"> ประวัติสถานะไฟล์แนบกระบวนวิชา <?php echo $proposal['SpecialAndCooperative']['code'] ?></font>

              </div>
              <div class="panel-body">
                <table class="table table-bordered">
                  <?php 
                    echo $outputlogs; 
                  ?>
                </table>				
                
              </div>
        </div>
      </div>
      <div class="col-md-6">
        
        <div class="panel panel-success">
          <div class="panel-heading"> 
          &emsp; <i class="glyphicon glyphicon-comment"></i>
          <font class="txt-content_SpecialAndCooperative26">  ประวัติการพิจารณาหลักสูตรกระบวนวิชา <?php echo $proposal['SpecialAndCooperative']['code'] ?></font>
          </div>
            <div class="panel-body">
                  <?php	  $i = 1; ?>
                          <?php  foreach ($answers as $answer){ ?>
                                
                                <?php if ($answer['AnswerSpecialAndCooperative']['status'] == 1) { ?>                        
                                      <?php if ($answer['AnswerSpecialAndCooperative']['show'] == 1) {  
                                          //****1 = show  2 = hide**** */  ?>
                                            <div class="panel panel-default" style="border-color: #C6E4AC!important">
                                              <div class="panel-heading"  style="background-color: #C6E4AC!important;border-color: #C6E4AC!important"> 
                                                    <h3 class="panel-title">ตอบกลับครั้งที่  <?php echo $i; ?>  
                                                       
                                                    </h3> 
                                                  </div>
                                                  <div class="panel-body">
                                                    <tbody>
                                                      <div class="col-sm-12">
                                                          <div class="form-group has-feedback">      
                                                              <?php echo $answer['AnswerSpecialAndCooperative']['Detail']; ?>
                                                              <?php  $admins = $this->Session->read('adminCurriculumRequests'); ?>
                                                              <?php if ($admins['Admincurriculumrequest']['employee_status_id'] == 4 || $admins['Admincurriculumrequest']['employee_status_id'] == 3 || $admins['Admincurriculumrequest']['employee_status_id'] == 2) {
                                                                # code...
                                                                  }else { ?>                                                   
                                                                  <?php if ($answer['AnswerSpecialAndCooperative']['employee_status_id'] == $admins['Admincurriculumrequest']['employee_status_id']) {                                                       
                                                                    ?>  
                                                                    <?php if ($admins['Admincurriculumrequest']['employee_status_id'] == 1   ) {                                                       
                                                                    ?>
                                                                    
                                                                    <a class="btn btn-warning pull-right" href="<?php echo $this->Html->url(array('action' => 'edit_answer_abstract',$answer['AnswerSpecialAndCooperative']['id'],1,$proposal_id,$mis_employee_id)); ?>">
                                                                      แก้ไข
                                                                    </a>
                                                                    <a class="btn btn-danger pull-right" href="<?php echo $this->Html->url(array('action' => 'delete_comment',$answer['AnswerSpecialAndCooperative']['id'],1,$proposal_id,$mis_employee_id)); ?>">
                                                                      ลบ
                                                                    </a>
                                                                    
                                                                    <?php } ?>
                                                                <?php  }else { ?>
                                                                
                                                                  <?php } ?>                                                   
                                                              
                                                              <?php } ?>
                                                          </div>
                                                          <div class="col-sm-12">
                                                          
                                                            <?php 

                                                            if($answer['AnswerSpecialAndCooperativeDocument'] != array()){

                                                              echo '<br><label><span class="glyphicon glyphicon-list-alt"></span>เอกสารแนบ</label>';

                                                              foreach ($answer['AnswerSpecialAndCooperativeDocument'] as $key) {
                                                                $fileAction = $this->webroot.'files/students/specialandcooperatives/'.$answer['AnswerSpecialAndCooperative']['special_and_cooperative_id'].'/'.$key['name_old'] ;
                                                                echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="'.$fileAction.'" class="btn btn-primary" target="_blank">'.$key['name_old'].' <span class="glyphicon glyphicon-download-alt"></span></a>';
                                                              }
                                                            }


                                                            ?>
                                                          </div>
                                                      </div>                                          
                                                  <div class="col-sm-12">
                                                    <div class="form-group has-feedback">
                                                              <blockquote>
                                                              <h4><i class="fa fa-user-circle-o" aria-hidden="true"></i> <b>ตอบกลับโดย :</b>
                                                              
                                                              <font class="txt-content_SpecialAndCooperative26"> <?php echo $answer['AnswerSpecialAndCooperative']['PostName']; ?></font></h4>  <small><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $answer['AnswerSpecialAndCooperative']['created']; ?></small>
                                                              </blockquote>    
  
                                                    </div>
                                                  </div>
                                          </div>            
                                        </div>
                                     
                                         
                                      <?php } ?> 
                                <?php }else{ ?>
                                  <div class="panel panel-default" style="border-color: #DABAFA!important">
                                            <div class="panel-heading" style="background-color: #DABAFA!important;border-color: #DABAFA!important"> 
                                                  <h3 class="panel-title">ตอบกลับครั้งที่  <?php echo $i; ?></h3> 
                                                </div>
                                                <div class="panel-body">
                                                  <tbody>                                           
                                                  
                                                      <div class="col-sm-12">
                                                            <div class="form-group has-feedback">      
                                                            <?php echo $answer['AnswerSpecialAndCooperative']['Detail']; ?>
                                                            <?php  $admins = $this->Session->read('adminCurriculumRequests'); ?>
                                                            <?php if ($answer['AnswerSpecialAndCooperative']['employee_status_id'] == $admins['Admincurriculumrequest']['employee_status_id']) {                                                       
                                                                  ?>  
                                                                  <a class="btn btn-danger pull-right" href="<?php echo $this->Html->url(array('action' => 'delete_comment',$answer['AnswerSpecialAndCooperative']['id'],1,$proposal_id,$mis_employee_id)); ?>">
                                                                    ลบ
                                                                  </a>                                                        
                                                              <?php  }else { ?>
                                                              
                                                                <?php } ?> 
                                                        </div>
                                                    </div>                                         
                                                    <div class="col-sm-12">
                                                            <div class="form-group has-feedback">
                                                            <blockquote>
                                                            <h4><i class="fa fa-user-circle-o" aria-hidden="true"></i> <b>ตอบกลับโดย :</b>
                                                            <font class="txt-content_SpecialAndCooperative26"> <b><?php echo $answer['AnswerSpecialAndCooperative']['PostName']; ?></b></font></h4>  <small><i class="fa fa-clock-o" aria-hidden="true"></i> <?php echo $answer['AnswerSpecialAndCooperative']['created']; ?></small>
                                                            </blockquote>      
                                                            
                                                            </div>
                                                </div>
                                        </div>
                                                      
                                  </div>

                                <?php } $i++; ?> 
                          <?php } ?>
                                                          
                                    
                                
                  <!-- **********          -->
              </div>
            </div>
        </div>
      
      </div>
     
  </div>   
</div>
