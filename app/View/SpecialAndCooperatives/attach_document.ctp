<?php //echo debug($this->request->data)?>
<!-- start content here -->
<div class="container" style="padding:15px 0 0;">
  <div class="row">
    <div class="col-md-12"> 
      <div class="panel panel-default" style="border-color: #F2F2F2!important">
        <div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
          <div class="panel-title">
            <h3 style="margin:0px !important;">
            <center> 
                <div class="font_panel2">
                อัพโหลดไฟล์กระบวนวิชา มคอ.3-4 ฉบับสมบูรณ์
                </div>
              </center>
            </h3> 
          </div>
        </div>	
        <div class="panel-body" style="padding-top: 15px; ">
          <div class="row" align="center"> 	
              <div class="font_panel5">	
                <h3>	กรุณาอัพโหลดไฟล์ที่ผ่านจากสภามหาวิทยาลัย ที่มีผลบังคับใช้แล้วเท่านั้น</h3> 
              </div>
            </div> 
              <tbody>     
                      <label class="col-sm-4 control-label">ชื่อหลักสูตร </label>
                          <div class="col-sm-8">
                                <div class="form-group has-feedback">      
                                <?php echo $SpecialAndCooperatives['SpecialAndCooperative']['title']; ?>               
                          </div>
                      </div>
                      <label class="col-sm-4 control-label">สาขาวิชา</label>
                          <div class="col-sm-8">
                                <div class="form-group has-feedback">     
                                <?php echo $SpecialAndCooperatives['Major']['major_name']; ?>

                          </div>
                      </div> 
                          </div>
                        </div>

                      
              </tbody>
                <!-- /container -->
            </div> 
          </div>
        </div>
      </div> 
    </div>
    <div class="col-md-6">                
        <form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">	                              
          <?php  
              $required = 'กรุณากรอกข้อมูล'; 
              echo $this->Form->create('SpecialAndCooperative',array('role' => 'form','data-toggle' => 'validator')); 
              //คอมเม้นตรงนี้
              echo $this->Form->hidden('SpecialAndCooperative.id', array(
                'value' => $proposal_id,
                'data-error' => $required,
                'label' => false,
                'class' => 'form-control'
              ));
          
          ?>      
                      
         
                      <h3>กรุณาอัพโหลดไฟล์ มคอ. ฉบับสมบูรณ์ ที่นี่</h3>
                    
                    </div> 
                  
                <div class="row">	
                  <div class="form-group">
                    <label class="col-sm-3 control-label">วันที่ผ่านการอนุมัติ  <br>
                    </label>
                      <div class="col-sm-7">
                        <div class="input-group date">                        
                        <?php 
                        echo $this->Form->input('SpecialAndCooperative.pass_special_and_cooperative_date', array(                          
                              'type' => 'text',
                              'label' => false,  
                              'value' => date('d-m-Y'),                                                      
                              'class' => array('form-control'),                                                           
                              'error' => false,
                              'required'
                        ));
                        ?>    
                        <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span> <br>  
                        </div>     
                        <font color="red">* ระบบจะดึงวันที่ให้อัตโนมัติ หากต้องการเปลี่ยนกรุณาเลือกวันที่ต้องการ ตัวอย่างวันที่ 11-01-2019</font>                
                      </div>
                    </div>	
                    <label class="col-sm-3 control-label">แนบไฟล์มคอ. ฉบับสมบูรณ์ </label>		  
                    <div class="form-group col-sm-7">
                    
                        <?php
                          echo $this->Form->input('SpecialAndCooperativePassDocument.files.', array('type' => 'file', 'multiple','accept' => '.xlsx,.xls,image/*, .doc, .docx, .ppt, .pptx, .txt, .pdf, .zip, .rar'));
                        ?>
                        <?php
                          //echo $this->Form->input('SpecialAndCooperative.files', array('type' => 'file','accept' => '.doc, .docx, .txt, .pdf, .zip, .rar, .jpg, .png'));
                        ?>
                    <span style="color: red;">* จำเป็นต้องกรอกให้ครบ</span>
                    <div class="help-block with-errors"></div>	
                    </div>				  
                  </div>		
                  
                  <div class="row">
                    <div class="col-md-12">
                      <input type="submit" value="บันทึกข้อมูล" class="btn btn-success btn-lg "> 
                      <a href="javascript:window.open('','_self');window.close()" class="btn btn-danger " >
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        ปิดหน้าต่างนี้
                      </a>
                    </div>
                  </div>
        </form>
               
    </div>
    <div class="col-md-6">
                  <h4>เอกสารแนบ</h4>
                  <table class="table table-bordered">
                      <tbody>
                        <tr>
                            <td >
                                <?php echo $output; ?>
                            </td>
                        </tr> 
                                                            
                      </tbody>
                  </table>  
    </div>
  </div>
</div>   
<div style="clear: both;"></div>