<?php //echo debug($this->request->data);?>
<div class="container" style="padding:0 15px 0;">	
	<div class="row">
		<div class="col-md-12">
        <div class="panel panel-primary">
						<div class="panel-heading"> 
							<h3 class="panel-title"><i class="glyphicon glyphicon-home"></i> ส่วนการจัดการฐานข้อมูลวิจัยของผลงานตีพิมพ์</h3> 
								</div>
									<div class="panel-body">
										         
				   	<a class="btn btn-default dropdown-toggle btn-primary" 
                       href="<?php echo $this->Html->url(array('action' => 'AddResearchPurpose/'.$researchid .'/'.$year .'/'.$degree .'/'.$researchtype.'')); ?>">
							<span class="glyphicon glyphicon-home"></span>			
							ย้อนกลับ
					</a>
						
						</div>
					</div>

                            <form method="post" accept-charset="utf-8">
                                  <div class="panel panel-success">
                                      <div class="panel-heading"> 
                                        <h3 class="panel-title"><i class="glyphicon glyphicon-plus"></i> แก้ไขวัตถุประสงค์ของการทำวิจัย</h3> 
                                      </div>
                                      <div class="panel-body">
                                        <tbody> 
                                            <table class="table table-hover" >
                                               <?php 
                                                    echo $this->Form->input('Researchpurpose.id', array(                                                                 
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control css-require'),                                                           
                                                                  'error' => false,                                                                  
                                                                  ));
                                                          
                                                            ?>
                                                <tr>
                                                    <td>วัตถุประสงค์ของการทำวิจัย</td>
                                                    <td>                                                    
                                                        <div class="col-sm-7">
                                                        <div class="form-group has-feedback">
                                                        <?php 
                                                                echo $this->Form->input('Researchpurpose.purpose', array(
                                                                    'type' => 'textarea',
                                                                    'label' => false,
                                                                    'div' => false,
                                                                    'class' => array('form-control css-require'),                                                           
                                                                    'error' => false,
                                                                    'required'                        
                                                                    ));
                                                            ?>
                                                        <span class="glyphicon form-control-feedback" aria-hidden="true"></span>    
                                                                                                
                                                    </div>
                                                </div>
                                                     
                                                     </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                     <td> <input type="submit" class="btn btn-success" value="แก้ไขข้อมูล" >
                                                    
                                                     </td>
                                                </tr>

                                            </table>   
                                            
                                        </tbody>
                                      </div>
                                          
                                    </form>
                                        
                                    


                                  <!-- /container -->  

                            </div>
                            
                    </div>
                </div>
       


</div>
</div>
</div>
<div style="clear: both;"></div>
