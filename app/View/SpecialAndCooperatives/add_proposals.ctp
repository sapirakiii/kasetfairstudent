<?php //debug(WWW_ROOT.'files'.DS.'Document');?>
<?php
	function DateThai($strDate)
		{

		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//, $strHour:$strMinute
	}
	?>
<style>
.numberCircle {
    border-radius: 50%;
    behavior: url(PIE.htc); /* remove if you don't care about IE8 */
    width: 36px;
    height: 36px;
    padding: 8px;
    background: #fff;
    border: 2px solid black;
    color: black;
    text-align: center;
    font: 20px Arial, sans-serif;
    display: inline-block;
}
</style>
 
<div class="container-fluid">
	<div class="row">
			 
        <form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">
          <div class="col-sm-6">
              <table class="table table-hover table-post" style="width: 100%;">
                  
                    <tr>
                      <td>
                        
                          <?php 
                              echo $this->Form->hidden('SpecialAndCooperative.mis_employee_id', array(                          
                                        'type' => 'text',
                                        'label' => false,                                                        
                                        'class' => array('form-control'),                                                           
                                        'error' => false,
                                        'value' => ''.$UserName['MisEmployee']['id'].'',
                                            
                              ));
                              
                              ?>   
                        
                      </td>
                    </tr>
                    <tr>
                      <td>
                            <label for="PreEngDetail" class="addpost_input">ชื่อ-สกุล </label>
                            <?php  
                                $sSubPre = '';
                                if ($UserName['MisSubPrename']['id'] != null) {
                                  $sSubPre = $sSubPre . $UserName['MisSubPrename']['name_short_th'] . ' ';
                                }
                                if ($sSubPre == '') {
                                  $sSubPre = $sSubPre . $UserName['MisPrename']['name_full_th'] . ' ';
                                } 
                              ?> 
                            <?php echo $sSubPre.''.$UserName['MisEmployee']['fname_th'].' '.$UserName['MisEmployee']['lname_th']; ?>
                      </td>
                    </tr>
                    <tr>
                      <td>
                          <label for="PreEngDetail" class="addpost_input">หน่วยงาน </label>
                          <?php echo  $admins['Organize']['name']; ?>
                          
                      </td>
                    </tr> 
                    <tr>
                        <td>
                            <label for="PreEngDetail" class="addpost_input">Email</label>
                          <?php 
                              echo $UserName['MisEmployee']['email_cmu']; 
                              
                              echo $this->Form->hidden('SpecialAndCooperative.email', array(
                                        'value' => ''.$UserName['MisEmployee']['email_cmu'].'',
                                            
                              ));
                              
                          ?>  
                          
                        </td>
                    </tr>
                  
                    <tr>
                        <td>
                          <label for="PreEngDetail" class="addpost_input">วันที่ยื่นเสนอ</label>
                          <?php  echo DateThai(date('Y-m-d'));  ?>  
                              
                        </td>
                    </tr>  
              </table>
              <hr>
              <table class="table table-hover table-post" style="width: 100%;">
                    <tr>
                        <td>    
                        <div class="font_panel5">	
                          <h3 ><div class="numberCircle">1</div> เพิ่มข้อมูลกระบวนวิชา </h3>
                          
                        </div> 
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="col-sm-3 control-label"> รหัสกระบวนวิชา <font color="red">*</font> </label> 
                            <div class="col-sm-9">
                                      <div class="form-group has-feedback">      
                                        <?php 
                                              echo $this->Form->input('SpecialAndCooperative.code', array(
                                                  'type' => 'text',
                                                  'label' => false,
                                                  'div' => false,
                                                  'class' => array('form-control css-require'),                                                           
                                                  'error' => false,
                                                  'required'
                                                  ));
                                          
                                            ?>
                                  ตัวอย่าง เช่น <font color="red"> 001101 </font>
                                  <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>
                            </div>    
                          
                  
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="col-sm-3 control-label"> ชื่อวิชาภาษาไทย <font color="red">*</font> </label> 
                            <div class="col-sm-9">
                                      <div class="form-group has-feedback">      
                                        <?php 
                                              echo $this->Form->input('SpecialAndCooperative.title', array(
                                                  'type' => 'text',
                                                  'label' => false,
                                                  'div' => false,
                                                  'class' => array('form-control css-require'),                                                           
                                                  'error' => false,
                                                  'required'
                                                  ));
                                          
                                            ?>
                                  ตัวอย่าง เช่น <font color="red"> ภาษาอังกฤษพื้นฐาน 1  </font>
                                  <!-- ตัวอย่าง เช่น <font color="red"> หลักสูตรวิทยาศาสตรบัณฑิต สาขาวิชา ... หลักสูตรปรับปรุง พ.ศ. ....... </font> -->
                                  <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>
                            </div>    
                          
                  
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label class="col-sm-3 control-label"> ชื่อวิชาภาษาอังกฤษ <font color="red">*</font> </label> 
                            <div class="col-sm-9">
                                      <div class="form-group has-feedback">      
                                        <?php 
                                              echo $this->Form->input('SpecialAndCooperative.title_eng', array(
                                                  'type' => 'text',
                                                  'label' => false,
                                                  'div' => false,
                                                  'class' => array('form-control css-require'),                                                           
                                                  'error' => false,
                                                  'required'
                                                  ));
                                          
                                            ?>
                                  ตัวอย่าง เช่น <font color="red"> Fundamental English 1 </font>
                                    
                                  <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                </div>
                            </div>    
                          
                  
                        </td>
                    </tr>
                    <tr>
                        <td>
                          <div class="form-group">
                            <label class="col-sm-3 control-label"> ภาควิชา</label>
                                  <div class="col-sm-9"> 
                                      <?php  
                                          
                                          echo $this->Form->radio('SpecialAndCooperative.organize_id', 
                                          $organizes, 
                                        array(
                                          'legend' => false,                                                                                                 
                                          'separator' => '<br/>',
                                          // 'disabled',
                                          'checked' => true,
                                          'required',
                                          'style' => 'width: 25px; 
                                                height: 25px; 
                                                vertical-align:text-bottom;'));
                                      ?>                                            
                                  </div>     
                              </div>
                            </div>
                             
                        </td>
                    </tr>
                    <tr>
                        <td>
                          <div class="form-group">
                            <label class="col-sm-3 control-label"> สาขาวิชาของกระบวนวิชานี้ </label>
                                  <div class="col-sm-9"> 
                                    <?php  
                                        echo $this->Form->radio('SpecialAndCooperative.major_id', 
                                      $majors, 
                                      array(
                                        'legend' => false,                                                                                                 
                                        'separator' => '<br/>',
                                        // 'disabled',
                                        //'required',
                                        'style' => 'width: 25px; 
                                              height: 25px; 
                                              vertical-align:text-bottom;'));
                                    ?>              

                                      <?php 
                                          // echo $this->Form->input('SpecialAndCooperative.major_id', array(                          
                                          //     'options' => $majors,
                                          //         'label' => false,                                                        
                                          //         'class' => array('form-control'),                                                           
                                          //         'error' => false,
                                          //         'required'
                                          // ));
                                      //debug($majors);
                                      ?>                                             
                                  </div>     
                              </div>
                            </div>
                            <font color="red"> * กรณีกระบวนวิชานี้เป็นวิชากลางของภาควิชา ไม่ต้องทำการเลือกสาขาวิชาใด ระบบจะนับเป็นวิชากลางอัตโนมัติ</font>
                        </td>
                    </tr>
                    <?php if($id == 1){ ?>
                      <tr>
                          <td>
                            <div class="form-group">
                              <label class="col-sm-3 control-label">เลือกระดับปริญญา</label>
                                    <div class="col-sm-9"> 
                                      <?php  
                                          $types = array('1' => ' ปริญญาตรี' );
                                          echo $this->Form->radio('SpecialAndCooperative.degree_id', 
                                        $types, 
                                        array(
                                          'legend' => false,                                                                                                 
                                          'separator' => '<br/>',
                                          // 'disabled',
                                          'checked' => true,
                                          'required',
                                          'style' => 'width: 25px; 
                                                height: 25px; 
                                                vertical-align:text-bottom;'));
                                      ?>                                          
                                    </div>     
                                </div>
                              </div>
                          </td>
                      </tr>
                    <?php }elseif($id == 3){ ?>
                      <tr>
                          <td>
                            <div class="form-group">
                              <label class="col-sm-3 control-label">เลือกระดับปริญญา</label>
                                    <div class="col-sm-9"> 
                                      <?php  
                                          $types = array('3' => ' ปริญญาโท' );
                                          echo $this->Form->radio('SpecialAndCooperative.degree_id', 
                                        $types, 
                                        array(
                                          'legend' => false,                                                                                                 
                                          'separator' => '<br/>',
                                          // 'disabled',
                                          'checked' => true,
                                          'required',
                                          'style' => 'width: 25px; 
                                                height: 25px; 
                                                vertical-align:text-bottom;'));
                                      ?>                                          
                                    </div>     
                                </div>
                              </div>
                          </td>
                      </tr>
                    <?php }elseif($id == 5){ ?>
                      <tr>
                          <td>
                            <div class="form-group">
                              <label class="col-sm-3 control-label">เลือกระดับปริญญา</label>
                                    <div class="col-sm-9"> 
                                      <?php  
                                          $types = array('5' => ' ปริญญาเอก' );
                                          echo $this->Form->radio('SpecialAndCooperative.degree_id', 
                                        $types, 
                                        array(
                                          'legend' => false,                                                                                                 
                                          'separator' => '<br/>',
                                          // 'disabled',
                                          'checked' => true,
                                          'required',
                                          'style' => 'width: 25px; 
                                                height: 25px; 
                                                vertical-align:text-bottom;'));
                                      ?>                                          
                                    </div>     
                                </div>
                              </div>
                          </td>
                      </tr>
                    <?php }else{ ?>
                      <tr>
                            <td>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">เลือกระดับปริญญา</label>
                                      <div class="col-sm-9"> 
                                        <?php  
                                            $types = array('3' => ' ปริญญาโท','5' => ' ปริญญาเอก' );
                                            echo $this->Form->radio('SpecialAndCooperative.degree_id', 
                                          $types, 
                                          array(
                                            'legend' => false,                                                                                                 
                                            'separator' => '<br/>',
                                            // 'disabled',
                                            'required',
                                            'style' => 'width: 25px; 
                                                  height: 25px; 
                                                  vertical-align:text-bottom;'));
                                        ?>                                          
                                      </div>     
                                  </div>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                    <tr>
                        <td>
                          <div class="form-group">
                            <label class="col-sm-3 control-label">รูปแบบของกระบวนวิชานี้</label>
                                  <div class="col-sm-9"> 
                                    <?php  
                                        
                                        echo $this->Form->radio('SpecialAndCooperative.special_and_cooperative_type_id', 
                                      $specialAndCooperativeTypes, 
                                      array(
                                        'legend' => false,                                                                                                 
                                        'separator' => '<br/>',
                                        // 'disabled',
                                        // 'checked' => true,
                                        'required',
                                        'style' => 'width: 25px; 
                                              height: 25px; 
                                              vertical-align:text-bottom;'));
                                    ?>                                          
                                  </div>     
                              </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                          <div class="form-group">
                            <label class="col-sm-3 control-label">เลือกประเภทกระบวนวิชานี้</label>
                                  <div class="col-sm-9"> 
                                       
                                      <input type="radio" id="1" name="data[SpecialAndCooperative][coursegroup_id]" value="1" style="width: 25px;height: 25px; vertical-align:text-bottom;" >วิชาศึกษาทั่วไป กลุ่มวิชา &nbsp;<input type="text" name="data[SpecialAndCooperative][coursegroup_detail]" /><br>
                                      <input type="radio" id="2" name="data[SpecialAndCooperative][coursegroup_id]" value="2" style="width: 25px;height: 25px; vertical-align:text-bottom;" >วิชาเฉพาะ<br>
                                      <input type="radio" id="3" name="data[SpecialAndCooperative][coursegroup_id]" value="3" style="width: 25px;height: 25px; vertical-align:text-bottom;" >วิชาเลือกเสรี <br>
                                       
                                    <?php  
                                        
                                      // echo $this->Form->radio('SpecialAndCooperative.special_and_cooperative_type_id', 
                                      // $SpecialAndCooperativeTypes, 
                                      // array(
                                      //   'legend' => false,                                                                                                 
                                      //   'separator' => '<br/>',
                                      //   // 'disabled',
                                      //   // 'checked' => true,
                                      //   'required',
                                      //   'style' => 'width: 25px; 
                                      //         height: 25px; 
                                      //         vertical-align:text-bottom;'));
                                    ?>                                          
                                  </div>     
                              </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                          <div class="form-group">
                            <label class="col-sm-3 control-label">เลือกประเภทการเข้าที่ประชุม/แจ้งเวียน</label>
                                  <div class="col-sm-9">
                                      <input type="radio" id="1" name="data[SpecialAndCooperative][special_and_cooperative_meeting_type_id]" value="1" style="width: 25px;height: 25px; vertical-align:text-bottom;" >เข้าที่ประชุมสาขา/ภาควิชา<br>
                                      
                                      <label class="col-sm-3 control-label"> ครั้งที่ </label> 
                                      <div class="col-sm-7">
                                        <div class="form-group has-feedback">      
                                        <?php 
                                                      echo $this->Form->input('SpecialAndCooperative.special_and_cooperative_meeting_num', array(
                                                          'type' => 'text',
                                                          'label' => false,
                                                          'div' => false,
                                                          'class' => array('form-control'),                                                          
                                                          'error' => false,
                                                          // 'required'
                                                          ));
                                                  
                                                    ?>
                                          <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                        </div>
                                      </div>
                                      <label class="col-sm-3 control-label">วันที่   <br>
                                      </label>
                                          <div class="col-sm-7">
                                            <div class="input-group date">                        
                                            <?php 
                                            echo $this->Form->input('SpecialAndCooperative.special_and_cooperative_meeting_postdate', array(                          
                                                  'type' => 'text',
                                                  'label' => false,  
                                                  'value' => date('d-m-Y'),                                                      
                                                  'class' => array('form-control'),                                                           
                                                  'error' => false,
                                                  'required'
                                            ));
                                            ?>    
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span> <br>  
                                            </div>     
                                                            
                                          </div>
                                        </div>	
                                                                        
                                  </div>     
                              </div>
                            </div>
                        </td>
                       
                    </tr>
                    <tr>
                        <td>
                          <div class="form-group">
                            <label class="col-sm-3 control-label"></label>
                                  <div class="col-sm-9">
                                      <input type="radio" id="2" name="data[SpecialAndCooperative][special_and_cooperative_meeting_type_id]" value="2" style="width: 25px;height: 25px; vertical-align:text-bottom;" >แจ้งเวียน<br>
                                       
                                      <label class="col-sm-3 control-label">วันที่   <br>
                                      </label>
                                          <div class="col-sm-7">
                                            <div class="input-group date">                        
                                            <?php 
                                            echo $this->Form->input('SpecialAndCooperative.special_and_cooperative_meeting_postdate', array(                          
                                                  'type' => 'text',
                                                  'label' => false,  
                                                  'value' => date('d-m-Y'),                                                      
                                                  'class' => array('form-control'),                                                           
                                                  'error' => false,
                                                  'required'
                                            ));
                                            ?>    
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span> <br>  
                                            </div>     
                                                            
                                          </div>
                                        </div>	
                                                                        
                                  </div>     
                              </div>
                            </div>
                        </td>
                       
                    </tr>
                    <!-- <tr>
                      <td>
                        <label for="DownloadDetail" class="addpost_input">ระบุภาคการศึกษาที่เสนอ</label>
                        <?php 
                                  
                          echo $this->Form->input('SpecialAndCooperative.yearterm_id', array(
                            'options' => $yearterms,
                            'class' => 'form-control',
                            
                            'label' => false,
                          ));
                        ?>
                        <br>
                      </td>
                    </tr>  -->
              </table>
              
              <hr>               
          </div>
          <div class="col-sm-6"> 
           
                
                <table class="table table-hover table-post" style="width: 100%;">
                  <tr>
                      <td>                
                          <div class="font_panel5">	
                            <h3 ><div class="numberCircle">2</div> เพิ่มรายชื่ออาจารย์ผู้รับผิดชอบและอาจารย์ผู้สอน</h3>
                          </div>
                      </td>
                  </tr>
                  <tr>
                        <td>
                            <label class="col-sm-5 control-label"> อาจารย์ผู้รับผิดชอบ <font color="red">*</font> </label> 
                              <div class="col-sm-7">
                                    <div class="form-group has-feedback">
                                      <?php if($admins['Admincurriculumrequest']['employee_status_id'] == 3){  ?>
                                          <?php 
                                              
                      
                                              $sSubPre = '';
                                              if ($UserName['MisSubPrename']['id'] != null) {
                                              $sSubPre = $sSubPre . $UserName['MisSubPrename']['name_short_th'] . ' ';
                                              }
                                              if ($sSubPre == '') {
                                              $sSubPre = $sSubPre . $UserName['MisPrename']['name_full_th'] . ' ';
                                              } 
                                              echo $sSubPre.''. $UserName['MisEmployee']['fname_th'].' '.$UserName['MisEmployee']['lname_th'];
                                              echo $this->Form->hidden('SpecialAndCooperative.mis_employee_id', array(
                                                        'value' => ''.$UserName['MisEmployee']['id'].'',
                                                            
                                              ));
                                              
                                          ?>  
                                       
                                      <?php }elseif ($admins['Admincurriculumrequest']['employee_status_id'] == 2) { ?>
                                        <select name="data[SpecialAndCooperative][mis_employee_id]" class="form-control js-example-basic-single" >
                                            <?php echo $outputadvisor; ?>
                                        </select>
                                        <font color="red">* เลือกอาจารย์ผู้รับผิดชอบกระบวนวิชา</font>
                                      <?php }else { ?>
                                        <select name="data[SpecialAndCooperative][mis_employee_id]" class="form-control js-example-basic-single" >
                                            <?php echo $outputadvisor; ?>
                                        </select>
                                        <font color="red">* เลือกอาจารย์ผู้รับผิดชอบกระบวนวิชา</font>

                                      <?php } ?>
                                                    
                                   
                                     
                                    <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                  </div>
                              </div>   
                        
                                                      
                        </td>
                      </tr>
                      <tr>
                          <td>                
                               
                                 เพิ่มรายชื่ออาจารย์ผู้สอน 
                               
                          </td>
                      </tr>
                  <tr>
                    <td>
                        <table class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th style="text-align: center;">ลำดับ</th>  
                                <th style="text-align: center;">รายชื่ออาจารย์ผู้สอน</th>
                                
                              
                              
                              </tr>
                            </thead>
                            <tbody>  
                              <!-- **************************ประธานกรรมการคนที่ 1********************************                 								 -->
                                <tr style="text-align: center;">	
                                  <td data-title="อันดับที่" align="center">1	</td>
                                  <td data-title="รายชื่อคณะกรรมการ">
                                    <div class="form-group has-feedback">
                                      <select name="data[SpecialAndCooperative][advisor_id1]" class="form-control js-example-basic-single" >
                                        <?php echo $outputadvisor; ?>
                                      </select>
                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>                                                 
                                    </div>
                                       
                                  </td>
                                   
                                </tr>
                                <!-- **************************กรรมการคนที่ 2********************************   -->
                                <tr style="text-align: center;">	
                                  <td data-title="อันดับที่" align="center">2	</td>
                                  <td data-title="รายชื่อคณะกรรมการ">
                                    <div class="form-group has-feedback">
                                      <select name="data[SpecialAndCooperative][advisor_id2]" class="form-control js-example-basic-single" >
                                        <?php echo $outputadvisor; ?>
                                      </select>
                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>                                                 
                                    </div>
                                    
                                  </td>
                                  
                                </tr>
                                <!-- **************************กรรมการคนที่ 3********************************                 								 -->
                                <tr style="text-align: center;">	
                                  <td data-title="อันดับที่" align="center">3	</td>
                                  <td data-title="รายชื่อคณะกรรมการ">
                                    <div class="form-group has-feedback">
                                      <select name="data[SpecialAndCooperative][advisor_id3]" class="form-control js-example-basic-single" >
                                        <?php echo $outputadvisor; ?>
                                      </select>
                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>                                                 
                                    </div>
                                    
                                  </td>
                                   
                                </tr>
                                <!-- **************************กรรมการคนที่ 4********************************                 								 -->
                                <tr style="text-align: center;">	
                                  <td data-title="อันดับที่" align="center">4	</td>
                                  <td data-title="รายชื่อคณะกรรมการ">
                                    <div class="form-group has-feedback">
                                      <select name="data[SpecialAndCooperative][advisor_id4]" class="form-control js-example-basic-single" >
                                        <?php echo $outputadvisor; ?>
                                      </select>
                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>                                                 
                                    </div>
                                    
                                  </td>
                                   
                                </tr>
                                <!-- **************************กรรมการคนที่ 5********************************                 								 -->
                                <tr style="text-align: center;">	
                                  <td data-title="อันดับที่" align="center">5	</td>
                                  <td data-title="รายชื่อคณะกรรมการ">
                                    <div class="form-group has-feedback">
                                      <select name="data[SpecialAndCooperative][advisor_id5]" class="form-control js-example-basic-single" >
                                        <?php echo $outputadvisor; ?>
                                      </select>
                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>                                                 
                                    </div>
                                    
                                  </td>
                                   
                                </tr>
                                <!-- **************************กรรมการคนที่ 6********************************                 								 -->
                                <tr style="text-align: center;">	
                                  <td data-title="อันดับที่" align="center">6	</td>
                                  <td data-title="รายชื่อคณะกรรมการ">
                                    <div class="form-group has-feedback">
                                      <select name="data[SpecialAndCooperative][advisor_id6]" class="form-control js-example-basic-single" >
                                        <?php echo $outputadvisor; ?>
                                      </select>
                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>                                                 
                                    </div>
                                    
                                  </td>
                                   
                                </tr>
                                <!-- **************************กรรมการคนที่ 7********************************                 								 -->
                                <tr style="text-align: center;">	
                                  <td data-title="อันดับที่" align="center">7	</td>
                                  <td data-title="รายชื่อคณะกรรมการ">
                                    <div class="form-group has-feedback">
                                      <select name="data[SpecialAndCooperative][advisor_id7]" class="form-control js-example-basic-single" >
                                        <?php echo $outputadvisor; ?>
                                      </select>
                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>                                                 
                                    </div>
                                    
                                  </td>
                                   
                                </tr>
                                <!-- **************************กรรมการคนที่ 8********************************                 								 -->
                                <tr style="text-align: center;">	
                                  <td data-title="อันดับที่" align="center">8	</td>
                                  <td data-title="รายชื่อคณะกรรมการ">
                                    <div class="form-group has-feedback">
                                      <select name="data[SpecialAndCooperative][advisor_id8]" class="form-control js-example-basic-single" >
                                        <?php echo $outputadvisor; ?>
                                      </select>
                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>                                                 
                                    </div>
                                    
                                  </td>
                                   
                                </tr>
                                <!-- **************************กรรมการคนที่ 9********************************                 								 -->
                                <tr style="text-align: center;">	
                                  <td data-title="อันดับที่" align="center">9	</td>
                                  <td data-title="รายชื่อคณะกรรมการ">
                                    <div class="form-group has-feedback">
                                      <select name="data[SpecialAndCooperative][advisor_id9]" class="form-control js-example-basic-single" >
                                        <?php echo $outputadvisor; ?>
                                      </select>
                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>                                                 
                                    </div>
                                    
                                  </td>
                                   
                                </tr>
                                <!-- **************************กรรมการคนที่ 10********************************                 								 -->
                                <tr style="text-align: center;">	
                                  <td data-title="อันดับที่" align="center">10	</td>
                                  <td data-title="รายชื่อคณะกรรมการ">
                                    <div class="form-group has-feedback">
                                      <select name="data[SpecialAndCooperative][advisor_id10]" class="form-control js-example-basic-single" >
                                        <?php echo $outputadvisor; ?>
                                      </select>
                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>                                                 
                                    </div>
                                    
                                  </td>
                                   
                                </tr>
                              </tbody>
                          </table>
                    </td>                
                  </tr>
                </table>
                <hr>
                
                <table class="table table-hover table-post" style="width: 100%;">  
                  <tr>
                      <td>                
                          <div class="font_panel5">	
                            <h3 ><div class="numberCircle">3</div> อัพโหลดไฟล์ มคอ.3-4</h3>
                            ท่านสามารถแนบไฟล์ word หรือ ไฟล์ pdf /แนบได้ทั้ง 2 ไฟล์  
                          </div>
                      </td>
                  </tr> 
                  <tr>
                    <td>
                      <label for="InfoDetail" class="addpost_input">อัปโหลดรูปภาพ และไฟล์เอกสารประกอบ</label> <font color="red">(กรณีเลือกมากกว่า 1 ไฟล์ ให้กด Ctrl หรือ Shift ค้างไว้และเลือกรูปภาพหรือไฟล์ที่ต้องการ)</font>
                        <?php
                            echo $this->Form->input('SpecialAndCooperativeDocument.files.', array('type' => 'file', 'multiple','accept' => '.xlsx,.xls,image/*, .doc, .docx, .ppt, .pptx, .txt, .pdf, .zip, .rar'));
                        ?>
                    </td>
                  </tr>                                                       
                  <!-- <tr>
                    <td>
                    <div class="row">	
                      <div class="form-group col-md-6">                                
                        <label>แนบไฟล์ Word</label>
                        <?php
                            echo $this->Form->input('SpecialAndCooperativeDocument.fileDOC.', array(
                              'type' => 'file',
                              'accept' => '.doc , .docx',
                              ));
                          ?>
                      </div>
                      <div class="form-group col-md-6">                                
                        <label>หรือ แนบไฟล์ PDF</label>
                        <?php
                            
                            echo $this->Form->input('SpecialAndCooperativeDocument.filePDF.', array(
                              'type' => 'file',
                              'accept' => '.pdf',
                              ));
                          ?>
                      </div>			

                      
                    </td>
                  </tr> -->
                  
                </table>
                <hr>
                <?php if($admins['Admincurriculumrequest']['employee_status_id'] == 1){ ?>
                  <table class="table table-hover table-post" style="width: 100%;">
                      <tr>
                          <td>                
                              <div class="font_panel5">	
                                <h3 ><div class="numberCircle">4</div> ระบุสถานะ มคอ.3-4 </h3>
                                
                              </div>
                          </td>
                      </tr>
                      <tr>
                        <td>
                          
                            คลิ๊กเพื่อระบุสถานะ <br><font color="red">*</font> <img src="https://www1.reg.cmu.ac.th/ugradapply/images/update.gif">
                              
                                           
                              <div class="form-group has-feedback">      
                                  
                                  <?php  
                                    echo $this->Form->radio('SpecialAndCooperative.special_and_cooperative_pass_id', 
                                    $passes, 
                                    array(
                                      'legend' => false,  
                                                                                      
                                      'separator' => '<br/>',
                                      // 'required',
                                      'style' => 'width: 25px; 
                                            height: 25px; 
                                            vertical-align:text-bottom;'));
                                  ?>
                            <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                          </div>
                           	
                          
                        </td>
                        <td>
                            ระดับกระบวนการติดตาม <br><font color="red">*</font> <img src="https://www1.reg.cmu.ac.th/ugradapply/images/update.gif">
                            
                                                   
                                        <div class="form-group has-feedback">      
                                            <?php if($id == 1){
                                                    $types = array(
                                                      '1' => 'อาจารย์ /ภาควิชา',
                                                      '4' => 'งานบริการการศึกษาฯ',
                                                      '5' => 'ผู้บริหาร(ฝ่ายวิชาการ)',
                                                      '6' => 'กรรมการวิชาการระดับปริญญาตรี',
                                                      '7' => 'กรรมการบริหารประจำคณะ',
                                                      '8' => 'สำนักพัฒนาคุณภาพนักศึกษา',
                                                      '9' => 'กรรมการบริหารและประสานงานวิชาการ',
                                                      '10' => 'มหาวิทยาลัย',
                                                      
                                                    );	
                                                  }else{
                                                    $types = array(
                                                      '1' => 'อาจารย์ /ภาควิชา',
                                                      '4' => 'งานบริการการศึกษาฯ',
                                                      '5' => 'กรรมการบัณฑิตศึกษาประจำคณะ',
                                                      '6' => 'กรรมการวิชาการบัณฑิตวิทยาลัย',
                                                      '7' => 'กรรมการบริหารวิชาการประจำบัณฑิตวิทยาลัย',
                                                      '8' => 'กรรมการบริหารมหาวิทยาลัย',
                                                      '9' => 'สภาวิชาการ',
                                                      '10' => 'สภามหาวิทยาลัย',
                                                      
                                                    );
                                                  }
                                            ?>
                                            <?php  
                                              echo $this->Form->radio('SpecialAndCooperative.special_and_cooperative_follow_status_id', 
                                              $types, 
                                              array(
                                                'legend' => false,                                       
                                                'separator' => '<br/>',
                                                'required',
                                                'style' => 'width: 25px; 
                                                      height: 25px; 
                                                      vertical-align:text-bottom;'));
                                            ?>
                                      <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                    </div> 
                                            
                        </td>
                      </tr>
                        
                  </table>

                <?php } ?>
                      <div style="margin-top: 15px;">						
                        <input type="submit" class="btn btn-success btn-lg btn-block" value="บันทึกข้อมูล มคอ.3-4"  >
                      </div>

                </div> 
              </div>
           
          </div>
        </form>
	</div>
</div> <!-- /container -->