<script >
window.print();
</script>
<?php 
      	//<script>window.jQuery || document.write('<script src="/assets/bootstrap/assets/js/vendor/jquery.min.js"><\/script>')</script>
      	echo $this->Html->script('/assets/bootstrap/assets/js/vendor/jquery.min'); 
    	?>
		<!-- Bootstrap core CSS -->
		<?php echo $this->Html->css('/assets/bootstrap/dist/css/bootstrap.min'); ?>

		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<?php echo $this->Html->css('/assets/bootstrap/assets/css/ie10-viewport-bug-workaround'); ?>

		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><?php echo $this->Html->script('/assets/bootstrap/assets/js/ie8-responsive-file-warning'); ?><![endif]-->
		<?php echo $this->Html->script('/assets/bootstrap/assets/js/ie-emulation-modes-warning'); ?>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<?php echo $this->Html->script('https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js'); ?>
			<?php echo $this->Html->script('https://oss.maxcdn.com/respond/1.4.2/respond.min.js'); ?>
		<![endif]-->
		<?php 
			//<script>window.jQuery || document.write('<script src="/assets/bootstrap/assets/js/vendor/jquery.min.js"><\/script>')</script>
			echo $this->Html->script('/assets/bootstrap/assets/js/vendor/jquery.min'); 
		?>
		
		<?php echo $this->Html->script('/assets/bootstrap/dist/js/bootstrap.min'); ?>
		<style type="text/css">
@font-face {
    font-family: 'baijam';
      src: url('/2017/files/fonts/th_baijam_0-webfont.eot');
    src: url('/2017/files/fonts/th_baijam_0-webfont.eot?#iefix') format('embedded-opentype'),
         url('/2017/files/fonts/th_baijam_0-webfont.woff') format('woff'),
         url('/2017/files/fonts/th_baijam_0-webfont.ttf') format('truetype'),
         url('/2017/files/fonts/th_baijam_0-webfont.svg#baijam') format('svg');
    font-weight: normal;
    font-style: normal;
	color:#000000;
	font-size:36px;
}
.font_panel1{
	font-family: 'baijam';
	color:#000000;
	font-size:26px;
	}
.font_panel2{
	font-family: 'baijam';
	color:#000000;
	font-size:34px;
	}
.font_panel3{
	font-family: 'baijam';
	color:#000000;
	font-size:26px;
	}
.font_panel4{
	font-family: 'baijam';
	color:#000000;
	font-size:22px;
	}
.font_panel5{
	font-family: 'baijam';
	color:#000000;
	font-size:18px;
	}
.font_panel6{
	font-family: 'baijam';
	color:#000000;
	font-size:16px;
	}	
</style>
<?php
	function DateThai($strDate)
		{

		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//, $strHour:$strMinute
	}
	?>
<div class="container" style="padding:15px 0 0;">

	<div class="panel">
		
	<div class="panel panel-default">
              <div class="panel-heading"> 
							<img class="img-responsive" alt="" src="http://www.agri.cmu.ac.th/2017/files/Infopage/46_0.jpg" style="padding-top:12px;width :60%;" align="right"/><br><br><br><br>
						
                   <h4 class="panel-title font_panel3"><?php echo $news[0]['Complaint']['Detail']; ?>
									 <small><?php 
                                          $time1 = strtotime($news[0]['Complaint']['postdate']);
		                                      $newformat1 = date('d-M-Y H.i', $time1);
                                          
                                          echo '<i class="glyphicon glyphicon-time" aria-hidden="true"></i> '.DateThai($newformat1).' '.date('H.i',strtotime($newformat1));  ?> น.
																					
																					</small></h4> 
              </div>
              <div class="panel-body">
                <tbody> 
                   <?php	
                      $i=1;
                       foreach ($news as $new){                                            
                    ?> 
                                        <table class="table table-hovered font_panel5">
                                            
                                            <tr>
                                              <td><b>หน่วยงานที่ให้ข้อเสนอแนะ/ข้อร้องเรียน</b></td>
                                              <td> <?php echo $new['Typeunitcomplaint']['namefull']; ?></td>
                                            </tr>
                                        		<tr>
                                              <td><b> ชื่อ-สกุลผู้แจ้ง</b></td>
                                              <td><?php echo $new['Complaint']['name']; ?></td>
                                            </tr>
                                            <tr>
                                              <td><b>โทรศัพท์</b></td>
                                              <td><?php echo $new['Complaint']['mobile']; ?></td>
                                            </tr>
                                            <tr>
                                              <td><b>อีเมล</b></td>
                                              <td><?php echo $new['Complaint']['mail']; ?></td>
                                            </tr>                                       
                                            <tr>
                                              <td><b>ประเภทบุคคล</b></td>
                                              <td>   <?php echo $new['Typecomplaint']['name']; ?></td>
                                            </tr>
																					
																					
                                        </table>  
																				<?php if ($new['ComplaintInfopage'] !=null) { ?>
																					<table>
                               <tr>
                                   <td colspan ='2'><h4>หลักฐาน/เอกสารแนบ </h4></td>
                               </tr>                         
                              <tr>
										                       
                                            <td>
                                                <?php
                                                        echo "<div class='list-group gallery'>";
                                                          foreach ($new['ComplaintInfopage'] as $img) {

                                                            if ($img['class'] == "img") {   ?>
                                                            <div class='col-sm-4'>
                                                                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $this->Html->url('/files/Complaint/').''. $img['name'] ?>">
                                                                    <img class="img-responsive" alt="" src="<?php echo $this->Html->url('/files/Complaint/').''. $img['name'] ?>" width="60%" />
                                                                    <div class='text-right'>
                                                                        <small class='text-muted'><?php //echo $new['Info']['Title']; ?></small>
                                                                    </div> 
                                                                </a>
                                                            </div>
                                                          <?php }else { ?>
                                                              <a href="<?php echo $this->Html->url('/files/Complaint/') , $img['name']; ?>" class="btn btn-primary" target="_blank">Download <?php echo $img['name_old']; ?> <span class="glyphicon glyphicon-cloud-download"></span></a>
                                                           
                                                          <?php  }
                                                          }
                                                          echo " </div>";

                                                      ?>

                                                 

                                            
                                            </td>
                                        </tr>  
                            </table>  
														<?php	} ?>
                                          <?php 
                                              $i++;  
                                              } ?>
                                               
							</div>
						</div>
						</tbody>
						</div> <!-- /container -->  
                     
					</div> 	
				</div>
			</div> 
		</div>
	</div>
	
</div>

