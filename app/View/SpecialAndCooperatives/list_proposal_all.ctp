<?php //debug($SpecialAndCooperatives) ?>
<style>
	.btn-circle {
	width: 30px;
	height: 30px;
	text-align: center;
	padding: 6px 0;
	font-size: 12px;
	line-height: 1.428571429;
	border-radius: 15px;
	}
	.btn-circle.btn-lg {
	width: 50px;
	height: 50px;
	padding: 10px 16px;
	font-size: 18px;
	line-height: 1.33;
	border-radius: 25px;
	}
	.btn-circle.btn-xl {
	width: 70px;
	height: 70px;
	padding: 10px 16px;
	font-size: 24px;
	line-height: 1.33;
	border-radius: 35px;
	}
</style>
<script type="text/javascript">
	function confirm_click()
	{
		return confirm("การลบหัวข้อโครงร่างฯนี้ จะเป็นการลบข้อมูลออกจากระบบ หากยืนยันว่าถูกต้องแล้ว โปรดยืนยันการลบข้อมูล ?");
	}

</script>
<?php 
		date_default_timezone_set('Asia/Bangkok');
		$date = date("Y-m-d");
		$time = date("H:i:s");

		function DateDiff($strDate1,$strDate2){                             
		return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
		}


	?>
	<?php
	function DateThai($strDate)
		{

		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//, $strHour:$strMinute
	}
	?>
 
<div class="container">
		<div class="row">
			<?php 
				$id = $this->Session->read('id');
				 
			 ?>
					 
			 
			
					<div class="col-md-12">
						<div class="panel panel-primary">
							<div class="panel-heading"> 
								ขอเปิดกระบวนวิชาใหม่/ปรับปรุงกระบวนวิชา
							</div>
							<div class="panel-body">
									
									<center>
										<a class="btn btn-success btn-lg" href="<?php echo $this->Html->url(array('action' => 'add_proposals')); ?>" role="button">											
											<i class="fa fa-plus" aria-hidden="true"></i> เพิ่มกระบวนวิชาใหม่/ปรับปรุงกระบวนวิชา ที่นี่
										</a>																	
									</center>
								
								
					</div>
								 
			
 		</div>
				

				<div class="panel panel-default" style="border-color: #F2F2F2!important">
					<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
							<h1 class="panel-title">รายการขอเปิดกระบวนวิชาใหม่/ปรับปรุงกระบวนวิชา</div></h1>
						</div>
						<div class="panel-body"> 
							<div id="no-more-tables" style="margin-top: 40px;"> 
								<table class="table table-striped table-bordered" id="runner_table">
									<thead>
										
										<tr >
											<th align="center">ลำดับ</th>
											<th style="width:10%">รหัสวิชา</th>
											<th style="width:20%">ชื่อวิชา</th>
											<!-- <th style="width:20%">ชื่อหลักสูตร</th> -->
											<th style="width:20%">สาขา</th>
											<th style="width:20%">ระดับปริญญา</th>
											<th style="width:20%">วันที่ยื่น</th>										 
											<th >สถานะ</th>
											<th >ระดับ</th>
											
											<th>การตอบกลับ</th>
											
										</tr>
									</thead>
									<tbody> 
										<?php echo $output;	 ?> 
									</tbody>
								</table>
							</div>
						</div>
						<hr>
						<div>						
						</div>
						<script type="text/javascript">
					$(document).ready(function() {
					$('#runner_table').DataTable({
						// "language": {
						// 	"search": "<?php //echo $search; ?>",
						// 	"lengthMenu": "<?php //echo $lengthMenu; ?>",
						// 	"Question": "<?php //echo $Question; ?>",
						// 	"QuestionEmpty": "<?php //echo $QuestionEmpty; ?>",
						// 	"emptyTable": "<?php //echo $emptyTable; ?>",
						// 	"paginate": {
						// 		"next":       ">",
						// 		"previous":   "<"
						// 	}
						// }
					});
					} );
				</script>	
				<hr>
			</div>
		</div>
	</div>
</div>
