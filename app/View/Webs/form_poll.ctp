<!-- <script >
window.print();
</script> -->
    <style type="text/css">
        @font-face {  
        font-family: sarabun ;  
        src: url(<?php echo $this->webroot.'files/fonts/THSarabun.ttf'; ?>) format("truetype");  
        }
            .txt-content{
                font-family: sarabun , verdana, helvetica, sans-serif; 
                /* font-family: 'TH Niramit AS'; */
                font-size: 28px;
            }
            .txt-content-indent{
                text-indent: 1cm;
                font-family: sarabun , verdana, helvetica, sans-serif; 
                /* font-family: 'TH Niramit AS'; */
                font-size: 28px;
            }
            .txt-content-indent2{
                text-indent: 1cm;
                font-family: sarabun , verdana, helvetica, sans-serif; 
                /* font-family: 'TH Niramit AS'; */
                font-size: 25px;
            }
            .txt-content-indent3{
                text-indent: 1cm;
                font-family: sarabun , verdana, helvetica, sans-serif; 
                /* font-family: 'TH Niramit AS'; */
                font-size: 18px;
            }
            .txt-content-command{
                text-indent: 1cm;
                font-family: sarabun , verdana, helvetica, sans-serif; 
                /* font-family: 'TH Niramit AS'; */
                font-size: 24px;
            }
            .txt-content-indent_eng{
                text-indent: 0.7cm;
                font-family: sarabun , verdana, helvetica, sans-serif; 
                /* font-family: 'TH Niramit AS'; */
                font-size: 22px;
            }
            .content-indent{
                margin-left: 1cm;
                font-family: sarabun , verdana, helvetica, sans-serif; 
                /* font-family: 'TH Niramit AS'; */
                font-size: 28px;
            }
            .br{
                margin-top: -6px;
            }
            .mr1{
                margin-top: 12px;
                line-height: 125%;
            }
            .mr2{
                margin-bottom: -6px;
                margin-top:-8px;
                line-height: 75%;
            }
    </style>
    <style>
        u.dotted{
          border-bottom: 1px dashed #999;
          text-decoration: none; 
        }
    </style>
    <!-- Bootstrap CSS -->
    <?php
	function DateThai($strDate)
		{

		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
        $strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฏาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//, $strHour:$strMinute
	}
?>
<?php
	function DateThais($strDate)
		{

		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน",
		"กรกฏาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//, $strHour:$strMinute
	}
?>
 
<style>
        
        .example2 {
        -webkit-print-color-adjust:exact;
        padding: 0px;
        background: url("https://www.agri.cmu.ac.th/smart_academic/img/draft.png");
        
        background-repeat: no-repeat;
        
        }
        
    
</style>
<!-- <script language="JavaScript1.2">
    function disableselect(e){
    return false
    }
    function reEnable(){
    return true
    }
    //if IE4+
    document.onselectstart=new Function ("return false")
    //if NS6
    if (window.sidebar){
    document.onmousedown=disableselect
    document.onclick=reEnable
    }
</script> -->
<script language=JavaScript>
    
    
    function clickIE() {if (document.all) {alert(message);return false;}}
    function clickNS(e) {if 
    (document.layers||(document.getElementById&&!document.all)) {
    if (e.which==2||e.which==3) {alert(message);return false;}}}
    if (document.layers) 
    {document.captureEvents(Event.MOUSEDOWN);document.onmousedown=clickNS;}
    else{document.onmouseup=clickNS;document.oncontextmenu=clickIE;}
    document.oncontextmenu=new Function("return false")
 
</script>
<?php 

$pageLength = 50;



date_default_timezone_set('Asia/Bangkok');
				$date = date("Y-m-d");
				$time = date("H:i:s");

//debug($this->base);
 //echo "<script>window.print();</script>";

function thainumDigit2($num){
return str_replace(array( '0' , '1' , '2' , '3' , '4' , '5' , '6' ,'7' , '8' , '9' ),
array( "๐" , "๑" , "๒" , "๓" , "๔" , "๕" , "๖" , "๗" , "๘" , "๙" ),
$num);
};
?>
<style type="text/css" media="print">
    @media print {
        Header {
        display: none !important;
        }
        Footer {
        display: none !important;
        }
        #lrno {
        font-size: 20pt;
        position: absolute!important;
        top: 25px;
        left: 30px;
        }
        #consignor {
        font-size: 30pt;
        position: relative;
        top: 70px;
        left: 0px;
        }
        #consignee {
        font-size: 33pt;
        position: relative;
        top: 9px;
        left: 590px;
        }
    }

</style>
<style type="text/css">   
  
	#printable { display: block; }  
  
@media print   
{   
     #non-printable { display: none; }   
     #printable { display: block; }   
}   
  
</style>  
<script>
function myFunction() {
    window.print();
}
</script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
<div class="container" style="padding-top: 0px;">
    <div class="row">
        <div class="col-md-12">
                <button onclick="myFunction()" class="btn btn-success pull-right" id="non-printable"><i class="fa fa-print" aria-hidden="true"></i> 
                    Print
                </button>
        
                <div class="col-md-12">
                    <!-- <p class="txt-content-indent text-right">
                        
                    </p> -->
                    <p class="txt-content mr1 ">
                        <b>
                            &emsp;&emsp;&emsp;&emsp; &emsp;&emsp;&emsp;&emsp; &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;คำขอทั่วไป 
                            &emsp;&emsp;&emsp;&emsp; &emsp;&emsp;&emsp;&emsp; &emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;  <b>มช.19</b>
                            <br>
                            &emsp;&emsp;&emsp;&emsp; &emsp;&emsp;&emsp;&emsp; &emsp;&emsp;&emsp;&emsp;&emsp;&emsp; General Request
                            
                        </b>
                       
                        <p class="txt-content-indent text-right mr1">
                            เขียนที่ คณะเกษตรศาสตร์<br>
                            วันที่ &nbsp;&nbsp;<?php  echo substr(DateThai($certStudents['CertStudent']['created']),0,2); ?>&nbsp;&nbsp;เดือน&nbsp;&nbsp; 
                            <?php 
                                if (substr($certStudents['CertStudent']['created'],5,2) == '01') {
                                    echo 'มกราคม';
                                }elseif (substr($certStudents['CertStudent']['created'],5,2) == '02') {
                                    echo 'กุมภาพันธ์';
                                }elseif (substr($certStudents['CertStudent']['created'],5,2) == '03') {
                                    echo 'มีนาคม';
                                }elseif (substr($certStudents['CertStudent']['created'],5,2) == '04') {
                                    echo 'เมษายน';
                                }elseif (substr($certStudents['CertStudent']['created'],5,2) == '05') {
                                    echo 'พฤษภาคม';
                                }elseif (substr($certStudents['CertStudent']['created'],5,2) == '06') {
                                    echo 'มิถุนายน';
                                }elseif (substr($certStudents['CertStudent']['created'],5,2) == '07') {
                                    echo 'กรกฏาคม';
                                }elseif (substr($certStudents['CertStudent']['created'],5,2) == '08') {
                                    echo 'สิงหาคม';
                                }elseif (substr($certStudents['CertStudent']['created'],5,2) == '09') {
                                    echo 'กันยายน';
                                }elseif (substr($certStudents['CertStudent']['created'],5,2) == '10') {
                                    echo 'ตุลาคม';
                                }elseif (substr($certStudents['CertStudent']['created'],5,2) == '11') {
                                    echo 'พฤศจิกายน';
                                }elseif (substr($certStudents['CertStudent']['created'],5,2) == '12') {
                                    echo 'ธันวาคม';
                                }
                            ?>
                            &nbsp;&nbsp;
                            พ.ศ. <?php  echo (substr($certStudents['CertStudent']['created'],0,4)+543);  ?>
                        </p>
                    </p>
                    <table border="0" style="font-family: sarabun , verdana, helvetica, sans-serif;font-size: 30px;">
                        <tr>
                            <td>
                                เรื่อง/<font class="txt-content-indent2"> Subject </font>
                                    <?php 
                                        if ($certStudents['CertType']['name'] == null ) {
                                            echo '<font color="red">(.....?....)</font>';
                                        } else {
                                            if ($certStudents['CertType']['id'] == 100) {
                                                echo ''.$certStudents['CertStudent']['cert_name'].'';
                                            
                                            } else {
                                                echo ''.$certStudents['CertType']['name'].'';
                                            }
                                            
                                        }
                                    ?>
                            </td>
                        </tr>
                        <?php   if ($certStudents['CertStudent']['cert19_leave_type_id'] != null ) {  ?>
                            <tr>
                                <td>
                                    ประเภทการลา/<font class="txt-content-indent2"> Type </font>
                                        <?php 
                                            if($certStudents['Cert19LeaveType']['name'] == null ){
                                                
                                            }else { 
                                                echo ''.$certStudents['Cert19LeaveType']['name'].'';                                                 
                                                
                                            }
                                        ?>
                                </td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td>
                                เรียน/<font class="txt-content-indent2">To </font>
                                คณบดีคณะเกษตรศาสตร์
                                <br>
                            </td>
                        </tr>
                    </table>
                    <p class="txt-content-indent mr1  ">
                      
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ข้าพเจ้า 
                    <u class="dotted">&nbsp;&nbsp; <b><?php echo $listStudents['Student']['student_title'].''.$listStudents['Student']['student_firstname'].' '.$listStudents['Student']['student_surname']; ?>

                    <font class="txt-content-indent2">  (<?php 
                            if ($listStudents['Student']['student_title'] == 'นาย') {
                                echo 'MR.';
                            } elseif ($listStudents['Student']['student_title'] == 'นางสาว') {
                                echo 'MS.';
                            }
                        ?>
                         <?php echo $listStudents['Student']['student_eng'];?>)</font> </b> &nbsp;&nbsp;</u>  
                    รหัสประจำตัว <u class="dotted">&nbsp;&nbsp;<b><?php echo $listStudents['Student']['id'] ?></b> &nbsp;&nbsp;</u>  <br>                    
                    สาขาวิชาเอก/<font class="txt-content-indent2">Major</font>  <u class="dotted">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b><?php echo $listStudents['Major']['major_name']  ?></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>
                    มีความประสงค์/<font class="txt-content-indent2"> I would like to request </font>
                    <u class="dotted"><?php  if ($certStudents['CertStudent']['detail_request'] == null ) { ?>
                        <font color="red">(.....?....)</font>
                    <?php } else { ?>
                        <?php  if (substr($certStudents['CertStudent']['detail_request'],0,3) == "<p>") { ?>
                            <table class="txt-content" border="0">
                                <tr>
                                    <td> <?php  echo $certStudents['CertStudent']['detail_request'];  ?>  </td>
                                </tr>
                            </table>
                        <?php }else { ?>	
                            <?php echo ' <b>'.$certStudents['CertStudent']['detail_request'].'</b> '; ?>
                        <?php } ?>

                    <?php } ?>
                    </u>
                    <?php  if ($certStudents['CertStudent']['cert_type_id'] != 100) { ?>
                        จำนวน 
                        <?php 
                        if ($certStudents['CertStudent']['num_request'] == null ) {
                            echo '<font color="red">.....?....</font>';
                        } else {
                            echo '<u class="dotted">&nbsp;&nbsp;<b>'.$certStudents['CertStudent']['num_request'].'</b>&nbsp;&nbsp;</u>';                            
                        }
                        ?> ชุด 
                    <?php } ?>
                     
                    กระบวนการวิชาที่ลงทะเบียนเรียนในภาคการศึกษานี้ / <font class="txt-content-indent2">Enrolled courses in this semester</font><br> 
                    <?php if($Studentgrades == null){ ?>   
                        1. กระบวนวิชา .............................. 2. กระบวนวิชา .............................. <br>
                       
                        
                    <?php }else{ ?>
                    <?php 
                    $i=0;
                    foreach ($Studentgrades as $key)  {
                        $i++;
                        if (($i%3)!=0) {
                            echo ''.$i.'. <small>กระบวนวิชา</small> <u class="dotted"> &nbsp;&nbsp;'.$key['Studentgrade']['code'].'&nbsp; <small>SEC</small> '.$key['Studentgrade']['SECLEC'].'/'.$key['Studentgrade']['SECLAB'].'   &nbsp; </u>';
                        } else {
                            echo ' &nbsp;&nbsp;'.$i.'. <small>กระบวนวิชา</small> <u class="dotted">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$key['Studentgrade']['code'].'&nbsp;<small>SEC</small> '.$key['Studentgrade']['SECLEC'].'/'.$key['Studentgrade']['SECLAB'].'&nbsp; </u><br>';
                        }
                        
                    } ?>
                    <?php } ?>
                    <!-- &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Course&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Section
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Course
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Section<br> -->
                    <br>&emsp;<small>course</small> &emsp;&emsp;&emsp;&emsp; <small>Section LEC/LAB</small> &emsp;   จึงเรียนมาเพื่อโปรดพิจารณาอนุมัติ/<small>For your consideration and approval</small>
                    </p>
                </div>
            
                <div class="col-md-12">
                    <table align="center" border="0">
                        <tr>
                            <td class="txt-content-blog">
                                <table border="black" width="650">
                                    
                                    <tr>
                                        <td  style="padding: 2px 5px;">
                                        <p class="txt-content-indent">
                                            <br>
                                            ที่อยู่ที่ติดต่อได้  : <?php  
                                            if ($StudentProfiles == null) {
                                                echo '<font color="red">ยังไม่พบข้อมูลที่อยู่</font> '; 
                                            } else {
                                                echo $StudentProfiles['StudentProfile']['address']; 
                                            }
                                            
                                            ?><br>
                                           
                                            โทรศัพท์  : <?php   echo $certStudents['Student']['student_mobile'];  ?> <br>
                                            
                                            Email : <?php   echo $certStudents['Student']['student_email'];   ?>
                                            </p>	
                                        </td>
                                    </tr>
                                </table>
                               
                            </td>
                            <td style="padding: 2px 2px;">
                                <p class="txt-content-indent text-left">
                                &emsp;ขอแสดงความนับถือ&nbsp;&nbsp;&nbsp;<br>
                                <?php //if($certStudents['CertStudent']['student_sign_id'] != null){ ?>
                                <?php if($studentSigns != null){ ?>
                                    &emsp;&nbsp;&nbsp;&nbsp;<img class="img-responsive" alt="" src="<?php echo $studentSigns['StudentSign']['sig'] ?>" width="60%"  /> <br>
                                <?php }else { ?>
                                    ..........................................<br>
                                <?php } ?>
                                <?php if($listStudents['Student']['student_title'] == 'นาย'){
                                    echo '&emsp;&nbsp;';
                                }else {
                                    echo '&emsp;';
                                } ?>   
                                (<?php echo $listStudents['Student']['student_title'].''.$listStudents['Student']['student_firstname'].' '.$listStudents['Student']['student_surname']; ?>)
                                </p>
                            </td>
                            
                        </tr>
                    </table>
                
                </div>
              
                <div class="col-md-12">
                    
                    <p class="txt-content-indent mr1">
                  
                    <?php 
                    // echo  $employeeSigns['EmployeeSign']['detail']
                    // echo  $certStudents['CertStudent']['comment_advisor']
                    ?>
                    ความคิดเห็นอาจารย์ที่ปรึกษา/Advisor's Comment
                    <?php if($certStudents['CertStudent']['employee_sign_id'] != null){ ?>
                        &emsp;<u class="dotted">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo  $certStudents['CertStudent']['comment_advisor'] ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>   <br>
                        
                    <?php }else { ?>
                        ........................................................................................................ <br>
                    <?php } ?>
                     
                    </p>
                     
                    <table  style="width:950px;font-family: sarabun , verdana, helvetica, sans-serif;font-size: 28px;"  border="0">
                        <tr>
                            <th style="width:540px">
                               
                            </th>
                            <?php if($certStudents['CertStudent']['employee_sign_id'] != null){ ?>
                                <th class="example2 "> 
                            <?php }else { ?>
                                <th> 
                            <?php } ?>
                            
                            <!-- class="example2 " -->
                                <!-- <div class="example2 "> -->
                                    <p class="  mr1" >
                                    <center>
                                        <?php if($certStudents['CertStudent']['employee_sign_id'] != null){ ?>
                                            &emsp;&nbsp;&nbsp;&nbsp;<img class="img-responsive" alt="" src="<?php echo $employeeSigns['EmployeeSign']['sig'] ?>" width="35%"  /> <br>
                                            <!-- &emsp;&nbsp;&nbsp;&nbsp;<img class="img-responsive" alt="" src=" https://www.agri.cmu.ac.th/smart_academic/img/draft.png" width="40%"  /> <br> -->
                                            
                                           
                                        <?php }else { ?>
                                            ..........................................................<br>
                                        <?php } ?>
                                        
                                        (<?php 
                                        if($advisorProposals == null){
                                            echo '..........................................................';
                                        }else{ ?> 
                                            <?php if($advisorProposals['Advisor']['level'] == 1){ ?>
                                            
                                                <?php echo $advisorProposals['Advisor']['advisor_fulltitle'];?><?php echo $advisorProposals['Advisor']['advisor_name'];?> <?php echo $advisorProposals['Advisor']['advisor_sname'];?>
                                            <?php }else { ?>
                                                
                                                <?php echo $advisorProposals['Advisor']['advisor_efulltitle'];?><?php echo $advisorProposals['Advisor']['advisor_name'];?> <?php echo $advisorProposals['Advisor']['advisor_sname'];?>
                                             <?php } ?>
                                        <?php } ?>
                                        ) 
                                        <br>
                                    อาจารย์ที่ปรึกษา/<font class="txt-content-indent2">Advisor </font><br>
                                    <?php if($certStudents['CertStudent']['date_approve'] != null){ ?>
                                        (<?php  echo DateThai($certStudents['CertStudent']['date_approve']); ?>)
                                    <?php } ?>
                                    </center>
                                    </p>
                                <!-- </div> -->
                            </th> 
                        </tr> 
                
                    </table>
                </div> 

                <?php //if($Students != null){ ?>  
                    <!-- <table  style="width:950px"   border="1">
                        <tr  class="txt-content text-center br">
                            <th style="width:70%">
                                ความเห็นงานบริการการศึกษา <font class="txt-content-indent_eng"> (Educational Service Section's Comment)</font> 
                            
                            </th>
                            <th>
                                คำสั่ง <font class="txt-content-indent_eng"> (Instruction)</font>   
                            
                            </th> 
                        </tr>
                        <tr>
                            <td style="height:170px" class="txt-content-indent2">
                                 
                            </td>
                            <td>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</td> 
                        </tr>
                    
                    
                
                    </table> -->
                <?php //}else { ?>  
                    
                    <table  style="width:100%"   border="1">
                        <tr  class="txt-content text-center br">
                            <th style="width:65%">
                                ความเห็นงานบริการการศึกษา <font class="txt-content-indent_eng"> (Educational Service Section's Comment)</font> 
                            
                            </th>
                            <th>
                                คำสั่ง <font class="txt-content-indent_eng"> (Instruction)</font>   
                            
                            </th> 
                        </tr>
                        <tr>
                            <td style="height:150px" class="txt-content-indent2">
                                <?php  if ($certStudents['CertStudent']['approve_id'] != 7) { ?>
                                 
                                    <?php if($certStudents['CertStudent']['comment_edu'] != null){ ?>
                                        <?php echo $certStudents['CertStudent']['comment_edu'] ?>  
                                        
                                    <?php } ?>
                                <?php } ?>
                            </td>
                            <!-- <td>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</td>  -->
                            <td class="txt-content-command">
                                <?php  if ($certStudents['CertStudent']['approve_id'] != 7) { ?>
                                    <?php if($certStudents['CertStudent']['command_edu'] != null){ ?>
                                        <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; ดำเนินการตามเสนอ</p>

                                        <p>&nbsp;</p>
                                        
                                        <p>&nbsp;</p>
                                        
                                        <p><br />
                                        &nbsp; &nbsp;(<?php echo $certStudents['CertStudent']['command_edu'] ?>)<br />
                                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <?php echo $certStudents['CertStudent']['command_edu_position'] ?> ปฏิบัติการแทน<br />
                                        &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp;คณบดีคณะเกษตรศาสตร์</p>
                                              
                                     
                                    <?php } ?>
                                <?php } ?>
                            </td>
                        </tr>
                    
                    
                
                    </table>
                    <p class="txt-content-indent3 mr1">
                        เอกสารฉบับนี้ใช้ลายมือชื่ออิเล็กทรอนิกส์ ตามพระราชบัญญัติ ว่าด้วยธุรกรรมทางอิเล็กทรอนิกส์ พ.ศ.2544
                        และข้อบังคับมหาวิทยาลัยเชียงใหม่ ว่าด้วยการใช้ลายมือชื่ออิเล็กทรอนิกส์ พ.ศ.2564    
                    </p>
                <?php //} ?>  

            <br><br>
        </div>
    </div>
</div>
 
