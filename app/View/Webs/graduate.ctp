
<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>
  .font-44{
    font-family: 'Mitr', sans-serif;
    font-size: 36px;
  }
  .font-28{
    font-family: 'Mitr', sans-serif;
    font-size: 28px;
  }
  .font-3{
    font-family: 'Mitr', sans-serif;
    font-size: 20px;
  }
  .font-18{
    font-family: 'Mitr', sans-serif;
    font-size: 16px;
  }
  .font-10{
    font-family: 'Mitr', sans-serif;
    font-size: 10px;
  }

    #btn-close-modal {
        width:100%;
        text-align: center;
        cursor:pointer;
        color:#fff;
    }

</style>
<style type="text/css">
  @import url("https://fonts.googleapis.com/css2?family=IBM+Plex+Sans+Thai:wght@200;300;400;500;600&display=swap");
  @import url("https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css");
            .txt-content{
               
                font-family: "IBM Plex Sans Thai", sans-serif;
                text-align: center;
            }
        @font-face {  
        font-family: sarabun ;  
        src: url(<?php echo $this->webroot.'files/fonts/THSarabun.ttf'; ?>) format("truetype");  
        }
            
            .txt-content-indent{
                text-indent: 1cm;
                font-family: sarabun , verdana, helvetica, sans-serif; 
                /* font-family: 'TH Niramit AS'; */
                font-size: 32px;
            }
            .content-indent{
                margin-left: 1cm;
                font-family: sarabun , verdana, helvetica, sans-serif; 
                /* font-family: 'TH Niramit AS'; */
                font-size: 32px;
            }
            .font-22{
              font-family: sarabun , verdana, helvetica, sans-serif;
            font-size: 26px;
            }
            .font-16{
                font-family: sarabun , verdana, helvetica, sans-serif;
                font-size: 32px;
            }
             
            
            .font-bold{
                font-weight: bold;
            }
            .br{
		          	margin-top: -16px;
            }
            .mr1{
                margin-top: 12px;
                line-height: 125%;
            }
            .mr2{
                margin-bottom: -6px;
                margin-top:-8px;
                line-height: 75%;
            }
            .textAlignVer{
                display:block;
                filter: flipv fliph;
                -webkit-transform: rotate(-90deg); 
                -moz-transform: rotate(-90deg); 
                transform: rotate(-90deg); 
                position:relative;
                width:20px;
                white-space:nowrap;
                font-size:16px;
                margin-bottom:10px;
                
            }
            .pd-facilities-in {
                padding: 20px;
            }
            
            .bg-fac3 {
              
              background: url(<?php echo $this->base.'/img/abstract-background-with-a-watercolor-texture_1048-2144.jpg' ?>);
              /* background: url(../../../images/img-facilities1.png); */
              background-size: cover;
              background-position: center center;
              width: 100%;
              height: 100%;
            } 
            .bg-fac4 {
              
              background: url(<?php echo $this->base.'/img/Grass.jpg' ?>);
              /* background: url(../../../images/img-facilities1.png); */
              background-size: cover;
              background-position: center center;
              width: 100%;
              height: 100%;
            }   
            .bg-facilities2 {
                background-color: #ffffffe0;
                margin-top: 25px;
                margin-bottom: 30px;
            }
            
            .pd-ct-contact {
                padding: 25px 0px 0px 0px;

            }
           
</style>
 
<link href="https://fonts.googleapis.com/css2?family=Mitr&display=swap" rel="stylesheet">
<style>
  
  /************************************
    * Close button
    ************************************/
    #closebt-container {
      position: relative;
      /* width:50%; */
      text-align:right;
      margin-top:20px;
      margin-right:20px;
      margin-bottom:10px;
    }

    .closebt {
      -webkit-transition: all 0.2s;
      -moz-transition: all 0.2s;
      -ms-transition: all 0.2s;
      -o-transition: all 0.2s;
      transition: all 0.2s;
      cursor:pointer;
    }

    .closebt:hover {
      transform:rotate(90deg);
    }

    #item > p  {
      margin:0px;
      color:#E74B3D;
    }
    /***************Timeline**************** */
    .timeline {
        position: relative;
        padding:4px 0 0 0;
        margin-top:22px;
        list-style: none;
    }

    .timeline>li:nth-child(even) {
        position: relative;
        margin-bottom: 50px;
        height: 180px;
        right:-100px;
    }

    .timeline>li:nth-child(odd) {
        position: relative;
        margin-bottom: 50px;
        height: 180px;
        left:-100px;
    }

    .timeline>li:before,
    .timeline>li:after {
        content: " ";
        display: table;
    }

    .timeline>li:after {
        clear: both;
        min-height: 170px;
    }

    .timeline > li .timeline-panel {
      position: relative;
      float: left;
      width: 41%;
      padding: 0 20px 20px 30px;
      text-align: right;
    }

    .timeline>li .timeline-panel:before {
        right: auto;
        left: -15px;
        border-right-width: 15px;
        border-left-width: 0;
    }

    .timeline>li .timeline-panel:after {
        right: auto;
        left: -14px;
        border-right-width: 14px;
        border-left-width: 0;
    }

    .timeline>li .timeline-image {
        z-index: 100;
        position: absolute;
        left: 50%;
        border: 7px solid #3b5998;
        border-radius: 100%;
        background-color: #3b5998;
        box-shadow: 0 0 5px #4582ec;
        width: 200px;
        height: 200px;
        margin-left: -100px;
    }

    .timeline>li .timeline-image h4 {
        margin-top: 12px;
        font-size: 10px;
        line-height: 14px;
    }

    .timeline>li.timeline-inverted>.timeline-panel {
        float: right;
        padding: 0 30px 20px 20px;
        text-align: left;
    }

    .timeline>li.timeline-inverted>.timeline-panel:before {
        right: auto;
        left: -15px;
        border-right-width: 15px;
        border-left-width: 0;
    }

    .timeline>li.timeline-inverted>.timeline-panel:after {
        right: auto;
        left: -14px;
        border-right-width: 14px;
        border-left-width: 0;
    }

    .timeline>li:last-child {
        margin-bottom: 0;
    }

    .timeline .timeline-heading h4 {
      margin-top:22px;
        margin-bottom: 4px;
        padding:0;
        color: #b3b3b3;
    }

    .timeline .timeline-heading h4.subheading {
      margin:0;
      padding:0;
        text-transform: none;
        font-size:18px;
        color:#333333;
    }

    .timeline .timeline-body>p,
    .timeline .timeline-body>ul {
        margin-bottom: 0;
        color:#808080;
    }
    /*Style for even div.line*/
    .timeline>li:nth-child(odd) .line:before {
        content: "";
        position: absolute;
        top: 60px;
        bottom: 0;
        left: 690px;
        width: 4px;
        height:340px;
        background-color: #3b5998;
        -ms-transform: rotate(-44deg); /* IE 9 */
        -webkit-transform: rotate(-44deg); /* Safari */
        transform: rotate(-44deg);
        box-shadow: 0 0 5px #4582ec;
    }
    /*Style for odd div.line*/
    .timeline>li:nth-child(even) .line:before  {
        content: "";
        position: absolute;
        top: 60px;
        bottom: 0;
        left: 450px;
        width: 4px;
        height:340px;
        background-color: #3b5998;
        -ms-transform: rotate(44deg); /* IE 9 */
        -webkit-transform: rotate(44deg); /* Safari */
        transform: rotate(44deg);
        box-shadow: 0 0 5px #4582ec;
    }
    /* Medium Devices, .visible-md-* */
    @media (min-width: 992px) and (max-width: 1199px) {
      .timeline > li:nth-child(even) {
        margin-bottom: 0px;
        min-height: 0px;
        right: 0px;
      }
      .timeline > li:nth-child(odd) {
        margin-bottom: 0px;
        min-height: 0px;
        left: 0px;
      }
      .timeline>li:nth-child(even) .timeline-image {
        left: 0;
        margin-left: 0px;
      }
      .timeline>li:nth-child(odd) .timeline-image {
        left: 690px;
        margin-left: 0px;
      }
      .timeline > li:nth-child(even) .timeline-panel {
        width: 76%;
        padding: 0 0 20px 0px;
        text-align: left;
      }
      .timeline > li:nth-child(odd) .timeline-panel {
        width: 70%;
        padding: 0 0 20px 0px;
        text-align: right;
      }
      .timeline > li .line {
        display: none;
      }
    }
    /* Small Devices, Tablets */
    @media (min-width: 768px) and (max-width: 991px) {
      .timeline > li:nth-child(even) {
        margin-bottom: 0px;
        min-height: 0px;
        right: 0px;
      }
      .timeline > li:nth-child(odd) {
        margin-bottom: 0px;
        min-height: 0px;
        left: 0px;
      }
      .timeline>li:nth-child(even) .timeline-image {
        left: 0;
        margin-left: 0px;
      }
      .timeline>li:nth-child(odd) .timeline-image {
        left: 520px;
        margin-left: 0px;
      }
      .timeline > li:nth-child(even) .timeline-panel {
        width: 70%;
        padding: 0 0 20px 0px;
        text-align: left;
      }
      .timeline > li:nth-child(odd) .timeline-panel {
        width: 70%;
        padding: 0 0 20px 0px;
        text-align: right;
      }
      .timeline > li .line {
        display: none;
      }
    }
    /* Custom, iPhone Retina */
    @media only screen and (max-width: 767px) {
      .timeline > li:nth-child(even) {
        margin-bottom: 0px;
        min-height: 0px;
        right: 0px;
      }
      .timeline > li:nth-child(odd) {
        margin-bottom: 0px;
        min-height: 0px;
        left: 0px;
      }
      .timeline>li .timeline-image {
        position: static;
        width: 150px;
        height: 150px;
        margin-bottom:0px;
      }
      .timeline>li:nth-child(even) .timeline-image {
        left: 0;
        margin-left: 0;
      }
      .timeline>li:nth-child(odd) .timeline-image {
        float:right;
        left: 0px;
        margin-left:0;
      }
      .timeline > li:nth-child(even) .timeline-panel {
        width: 100%;
        padding: 0 0 20px 14px;
      }
      .timeline > li:nth-child(odd) .timeline-panel {
        width: 100%;
        padding: 0 14px 20px 0px;
      }
      .timeline > li .line {
        display: none;
      }
    }

</style>
<?php echo $this->Html->css('/assets/webTcas65Agro/css/sheet.css'); ?>
  <!-- ======= Header ======= -->
    <header id="header" class="fixed-top ">
      <div class="container d-flex align-items-center">

        <h1 class="logo mr-auto">
          <!-- <a href="index.html">Tempo</a> -->
      </h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <a href="https://www.agri.cmu.ac.th/smart_academic/webs/indexfreshy" class="logo mr-auto"><img src="<?php echo $this->base.'/img/new_agri_logo.png' ?>" alt="" class="img-fluid"></a>

        <nav class="nav-menu d-none d-lg-block">
          <ul>
            <!-- <li class="active"><a href="http://www.agri.cmu.ac.th/smart_academic/webs/indextcas62">Home</a></li> -->
            <li class="active"><a href="#hero">Home</a></li>
            <li><a href="#about">โครงสร้างหลักสูตร</a></li>
            <!-- <li><a href="#advisor">อาจารย์ที่ปรึกษา</a></li> -->
            <li><a href="#services">บริการการศึกษา</a></li>
            <li><a href="#pricing">พัฒนาคุณภาพนักศึกษา</a></li>        
            <li><a href="#contact">ติดต่อสอบถาม</a></li>

          </ul>
        </nav><!-- .nav-menu -->

      </div>
    </header>
  <!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <!-- <section id="main" class="wow fadeIn" style="padding: 2cm 0cm  0cm 0cm;">
          
	   
    <img class="img-responsive" src="<?php echo $this->base.'/img/cmu1st.png' ?>"  width="100%">
     <center>
      
     
     
  </section> -->
  <section id="hero">
    <div class="hero-container" style="padding:570px 15px 0;">
      <!-- <h3>Welcome to <strong>Faculty of Agriculture</strong></h3> -->
     
      <!-- <h1><font style="color: #E1BB00;">CMU 1<sup>st</sup> YEAR </font><font style="color: #CCFF99;">STUDENTS 2023</font></h1> -->
      <h1><font class="txt-content" style="color: #C6A814;text-shadow: black 0.1em 0.1em 0.3em">GRADUATE </font><font style="color: #7150B2;">STUDENTS 2024</font></h1>
      <h2><font class="txt-content" style="text-shadow: black 0.1em 0.1em 0.3em" >ยินดีต้อนรับนักศึกษาใหม่ระดับบัณฑิตศึกษา คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่ ประจำปีการศึกษา <b>2567</b></font></h2>
      
    </div>
  </section>
  <!-- End Hero -->

  <main id="main">

  <!-- ======= About Section ======= -->
  <section id="account" class="account">
    <div class="container">

      <div class="section-title">
        <h2>Account</h2>
        <h3><font class="font-44" style="color: #E1BB00;">CMU</font><font class="font-44" > IT ACCOUNT</font> </h3>
        <p>บัญชีไอทีของมหาวิทยาลัยเชียงใหม่ CMU ACCOUNT กุญแจสำคัญในการระบุตัวตนสำหรับการเข้าใช้บริการไอทีของมหาวิทยาลัย</p>
      </div>

      <div class="row content">
        <div class="col-lg-12">
          <center>
            <a href="http://www.agri.cmu.ac.th/smart_academic/student_mains/login" class="btn-learn-more" target="_blank"> 
              STUDENT LOGIN <img src="https://www1.reg.cmu.ac.th/ugradapply/images/update.gif">
            </a>
            <br>
            <p> Click Login for use system</p>
            <br>    
            <img class="img-responsive" src="<?php echo $this->base.'/img/276.png' ?>"  width="100%"><br>
          </center>
        </div>
        <br><br>
        <div class="col-lg-6">
          <p>
            สำหรับนักศึกษาและบุคลากรมหาวิทยาลัยเชียงใหม่  @cmu.ac.th ชื่อผู้ใช้ (Username) และรหัสผ่าน(Password) เป็นกุญแจสำคัญในการเข้าใช้บริการไอทีต่าง ๆ ของมหาวิทยาลัยเชียงใหม่ 
            ที่พร้อมสนับสนุนการเรียนการสอน รวมไปถึงใช้ชีวิตในรั้วมหาวิทยาลัยเชียงใหม่ ให้สะดวกสบายยิ่งขึ้น
          </p>
          <p>
            <b>สำหรับนักศึกษาใหม่</b>
            <br>
            <ul>
              <li><i class="ri-check-double-line"></i> ระดับปริญญาตรีสามารถขอรับ CMU Account ได้ในขั้นตอนสุดท้ายของการรายงานตัวขึ้นทะเบียนนักศึกษาใหม่ของสำนักทะเบียนและประมวลผล หากขึ้นทะเบียนนักศึกษาใหม่เรียบร้อยแล้ว สามารถใช้งานได้ทันที</li>
              <li><i class="ri-check-double-line"></i> ระดับบัณฑิตศึกษา จะได้รับลิงค์สำหรับขอรับ CMU Account จากสำนักทะเบียนและประมวลผล เมื่อกระบวนการรายงานตัวเสร็จสิ้น และได้รับการตรวจสอบข้อมูลจากสำนักทะเบียนและประมวลผลแล้ว</li>
             
            </ul>
            <ul>
              <li>
              </li>
              <li></li>
            </ul>
          </p>
        
          
        </div>
        <div class="col-lg-6 pt-4 pt-lg-0">
          <p>
            &emsp;งานบริการการศึกษาและพัฒนาคุณภาพนักศึกษา จึงได้นำระบบสารสนเทศสำหรับนักศึกษา ที่ให้บริการสำหรับนักศึกษาภายในคณะ โดยใช้การยืนยันตัวตนผ่าน CMU IT Account 
          ซึ่งเน้นการใช้ 1 บัญชี ใช้งานได้ทุกบริการ (One account for all services) โดยนักศึกษาระดับปริญญาตรี ปริญญาโท และ ปริญญาเอก สามารถใช้งานได้
          </p>
          <p>
            &emsp; ทั้งนี้นักศึกษาสามารถอ่านคู่มือการใช้งาน CMU IT ACCOUNT สำหรับนักศึกษา ตามลิ้งค์ที่ปรากฏด้านล่าง
          </p>
          <a href="<?php echo $this->base.'/img/forgot password.jpg' ?>" target="_blank">
            <img class="img-responsive" src="<?php echo $this->base.'/img/forgot password.jpg' ?>"  width="100%" >
          </a><br>
          <p><a href="https://www.agri.cmu.ac.th/2017/webs/info_detail/20836" target="_blank">คู่มือการใช้ CMU IT ACCOUNT สำหรับนักศึกษา</a>  </p>
        </div>
      </div>

    </div>
  </section><!-- End About Section -->
  
  <!-- ======= Team Section ======= -->
  <section id="curriculum" class="curriculum">
    <div class="container">

      <div class="section-title">
        <h2>Curriculum</h2>
        <h3><font class="font-44" style="color: #E1BB00;"> โครงสร้าง</font><font class="font-44" >หลักสูตร</font> </h3>
           
      </div>

      <div class="row">

        <div class="col-lg-6 col-md-6 d-flex align-items-stretch">
          <div class="member">
            <div class="member-img">
              
              <a href="https://agri.cmu.ac.th/2017/webs/mainmenu_detail/3/434/1" target="_blank">
                <img src="<?php echo $this->base.'/img/graduate/1.png' ?>" class="img-fluid" alt="">
              </a>
                <div class="social">
                  
                  <a href="https://www.agri.cmu.ac.th/2017/webs/index_division/th/3"><i class="icofont-info"></i></a>
                  <a href="https://www.facebook.com/PrEconomyandDevelopment"><i class="icofont-facebook"></i></a>
               
                </div>
              </div>
              <div class="member-info">
                <h4>หลักสูตรปริญญาโท</h4>
                <span>หลักสูตรปริญญาโท</span>
              </div>
            </div>
        </div>

        <div class="col-lg-6 col-md-6 d-flex align-items-stretch">
          <div class="member">
            <div class="member-img">
              
              <a href="https://agri.cmu.ac.th/2017/webs/mainmenu_detail/3/435/1" target="_blank">
                <img src="<?php echo $this->base.'/img/graduate/2.png' ?>" class="img-fluid" alt="">
              </a>
                <div class="social">
                  
                  <a href="https://www.agri.cmu.ac.th/2017/webs/index_division/th/3"><i class="icofont-info"></i></a>
                  <a href="https://www.facebook.com/PrEconomyandDevelopment"><i class="icofont-facebook"></i></a>
               
                </div>
              </div>
              <div class="member-info">
                <h4>หลักสูตรปริญญาเอก</h4>
                <span>หลักสูตรปริญญาเอก</span>
              </div>
            </div>
        </div>

    </div>
  </section><!-- End Team Section -->

  <!--==========================
    รายชื่ออาจารย์ที่ปรึกษา
  ============================-->
  
    <!-- ======= Services Section ======= -->
    <section id="services" class="services">
      <div class="container">

        <div class="section-title">
          <h2>Education Student System</h2>
          <h3> <font class="font-44" style="color: #E1BB00;">บริการระบบสารสนเทศ</font><font class="font-44" >นักศึกษา</font> </h3>
          <!-- #C0C0C0 #E1BB00 -->
            <p>ระบบส่วนกลางของมหาวิทยาลัยเชียงใหม่</p>
        </div>
        <!-- <i class="bx bxl-dribbble"></i> -->
        <div class="row font-18">
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">          
            <div class="icon-box">
            <a href="https://agri.cmu.ac.th/2017/posts/post_pages/110" target="_blank">
              
                <img class="img-responsive" src="<?php echo $this->base.'/img/onedrive.png' ?>"  width="100%"><br>
                <!-- <h4 class="title"> &emsp;CMU OneDrive &emsp; </h4> -->
               </a>
                                     
            </div>
          </div>
 
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
            <div class="icon-box">
            <a href="https://agri.cmu.ac.th/2017/posts/post_pages/117" target="_blank">
            <img class="img-responsive" src="<?php echo $this->base.'/img/Picture5.png' ?>"  width="100%"><br>
              <!-- <h3 class="title"> MANGO CANVAS</h3> -->
            </a>
             
            </div>
          </div>
           
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
            <div class="icon-box">
            <a href="https://agri.cmu.ac.th/2017/posts/post_pages/118" target="_blank">
              <center>
                <img class="img-responsive" src="<?php echo $this->base.'/img/Canva-Logo-2013.png' ?>"  width="80%"><br>
                <!-- <h3 class="title"> MANGO CANVAS</h3> -->
              </center>
            </a>
             
            </div>
          </div>


          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
            <div class="icon-box">
            <a href="https://agri.cmu.ac.th/2017/posts/post_pages/119" target="_blank">
              
              <img class="img-responsive" src="<?php echo $this->base.'/img/outlook.png' ?>"  width="100%"><br>
              
             </a>
               
            </div>
          </div>


          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
            <div class="icon-box">
            <a href="https://itsc.cmu.ac.th/th/serviceview/b4e4af5f-97c7-44c2-b845-849eb3991538" target="_blank">
              
              <img class="img-responsive" src="<?php echo $this->base.'/img/Picture6.png' ?>"  width="100%"><br>
              
             </a>
               
            </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
            <div class="icon-box">
            <a href="https://itsc.cmu.ac.th/th/serviceview/aa7b0e89-dfba-4c90-ad44-ed6f58d5e3bb" target="_blank">
              
              <img class="img-responsive" src="<?php echo $this->base.'/img/Picture8.png' ?>"  width="100%"><br>
              
             </a>
               
            </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
            <div class="icon-box">
            <a href="https://agri.cmu.ac.th/2017/posts/post_pages/116" target="_blank">
              
              <img class="img-responsive" src="<?php echo $this->base.'/img/Picture9.png' ?>"  width="100%"><br>
              
             </a>
               
            </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
            <div class="icon-box">
            <a href="https://itsc.cmu.ac.th/th/serviceview/19245a2d-39c1-444a-ba40-2a5b6a3bb23c" target="_blank">
              
              <img class="img-responsive" src="<?php echo $this->base.'/img/Picture10.png' ?>"  width="100%"><br>
              
             </a>
               
            </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
            <div class="icon-box">
            <a href="https://itsc.cmu.ac.th/th/serviceview/2ab85bd2-2ce7-42e2-bb8f-86c27fdb98ef" target="_blank">
              
              <img class="img-responsive" src="<?php echo $this->base.'/img/Picture11.png' ?>"  width="100%"><br>
              
             </a>
               
            </div>
          </div>
        </div>
        <br>
        <div class="section-title"> 
          <p>ระบบสารสนเทศการศึกษาระดับคณะ</p>
        </div>
       
        <div class="row font-18">
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">    
            
          </div>
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">          
            <div class="icon-box">
            <a id="" href="https://agri.cmu.ac.th/2017/posts/post_pages/129" target="_blank">
            <img class="img-responsive" src="<?php echo $this->base.'/img/new_agri_logo.png' ?>"  width="100%"><br>
               
                <h4 class="title">  e-Thesis/I.S. Management System(New) <br> ระบบจัดการ<br>ปริญญานิพนธ์/การค้นคว้าอิสระ
                </h4>
               </a>
                                     
            </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">          
            <div class="icon-box">
            <a id="" href="https://agri.cmu.ac.th/2017/posts/post_pages/130" target="_blank">
            <img class="img-responsive" src="<?php echo $this->base.'/img/new_agri_logo.png' ?>"  width="100%"><br>
               
                <h4 class="title"> Graduate Graduation Enrollment System <br> ระบบการจัดทำ<br>ใบเสนอขออนุมัติปริญญา (มช. 22 บว.)
                </h4>
               </a>
                                     
            </div>
          </div>
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">          
        
          </div>
    </section>

    <!-- End Services Section -->


    <!-- ======= Cta Section ======= -->
    <section id="cta" class="cta">
      <div class="container">

        <div class="text-center">
          <h3><font class="font-44" >วิสัยทัศน์</font><font class="font-44" style="color: #E1BB00;">คณะเกษตรศาสตร์</font></h3>
          <p> Smart Agriculture towards Sustainable Development  ผู้นำทางวิชาการด้านเกษตรอัจฉริยะเพื่อสร้างและถ่ายทอดนวัตกรรมการเกษตรมุ่งสู่การพัฒนาอย่างยั่งยืน </p>
          <div class="row font-18">
          
           

        </div>
        
           
        </div>

      </div>
    </section><!-- End Cta Section -->
    
    <!-- ======= Services Section 1======= -->
    <section id="services" class="services">
      <div class="container">

        <div class="section-title">
          <h2>Education service</h2>
          <h3> <font class="font-44" style="color: #E1BB00;">บริการ</font><font class="font-44" >การศึกษา</font> </h3>
          <!-- #C0C0C0 #E1BB00
            <p>Ut possimus qui ut temporibus culpa velit eveniet modi omnis est adipisci expedita at voluptas atque vitae autem.</p> -->
        </div>
        <!-- <i class="bx bxl-dribbble"></i> -->
        <div class="row font-18">
          
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
            
          </div>
          
           
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
          <div class="icon-box">
            <a href="https://www.agri.cmu.ac.th/2017/webs/list_employee_office/3" target="_blank">
              <div class="icon"><i class="ri-user-fill" style="color: #322E44;"></i></div>
              <h4 class="title">แนะนำบุคลากรทางการศึกษา</h4></a>
              
            </div>
          </div>
          
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
           
           <div class="icon-box">
           <a href="https://www.reg.cmu.ac.th/webreg/th/calendar/" target="_blank"> 
           <!-- <a id="demo05" href="#modal-05"> -->
             <div class="icon">
             <i class="ri-calendar-2-line" style="color: #377011;" ></i></div>
             
             <h4 class="title"> ปฏิทินการศึกษา <br>ประจำปีการศึกษา 2567
              </h4>
              </a>
             
                                    
           </div>
            </div>
          </div>
          
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
          </div>

        </div>
          <!-- <i class="bx bx-tachometer"></i> -->
          <!-- <div class="col-md-6 col-lg-3 d-flex align-items-stretch mb-5 mb-lg-0">
            <div class="icon-box">
            <a id="demo07" href="#modal-07">
              <div class="icon"><i class="ri-dashboard-line" style="color: #452FB3;"  ></i></div>
              <h4 class="title"> แนะนำระบบสารสนเทศการศึกษาคณะเกษตรศาสตร์</h4></a>
             
            </div>
          </div> -->
          <!-- <i class="bx bx-world"></i> -->
        </div>

      </div>
       <!--DEMO01 ขึ้นทะเบียนรายงานตัวนักศึกษาใหม่-->
        <div id="animatedModal">
            
            
            <div id="closebt-container" class="close-animatedModal"> 
                <img class="closebt" src="<?php echo $this->Html->url('/img/close-cross-symbol-in-a-circle.svg'); ?>" >
            </div>
            <div id="modal-content" >
            
              <h3 >
              <center> 
                <font class="font-28"> การรายงานตัวขึ้นทะเบียนเป็นนักศึกษาใหม่</font></center> </h3> 
                <div id="particles-js"></div>
                <div class="container">
                  <div class="row">
                    
                    <div class="col-md-6 text-left font-18">
                      <img class="img-responsive" src="<?php echo $this->Html->url('/img/messageImage_1622723247007.jpg'); ?>" alt="Hero Imgs" title="" align="center">
                
                        
                              
                    </div> 
                    <div class="col-md-6 text-left font-18">
                      <img class="img-responsive" src="<?php echo $this->Html->url('/img/messageImage_1622723238026.jpg'); ?>" alt="Hero Imgs" title="" align="center">
                              

                        &emsp;การรายงานตัวขึ้นทะเบียนเป็นนักศึกษาเป็นกระบวนการที่ผู้มีสิทธิ์เข้าศึกษานำส่งเอกสารตามที่กำหนด 
                              ให้กับมหาวิทยาลัยเชียงใหม่ซึ่งสำนักทะเบียนและประมวลผลรจะรับรายงานตัวเพื่อขึ้นทะเบียนเป็นนักศึกษามหาวิทยาลัยเชียงใหม่ปีการศึกษา 2567 ผ่านระบบออนไลน์ดังนั้นจึงให้ผู้มีสิทธิ์รายงานตัวขึ้นทะเบียนเป็นนักศึกษามหาวิทยาลัยเชียงใหม่ทราบและถือปฏิบัติ
                        <hr>
                        &emsp; นักศึกษาใหม่ระดับปริญญาตรี จะต้องรายงานตัวขึ้นทะเบียนเป็นนักศึกษาใหม่ และดำเนินการส่งเอกสารการรายงานตัวให้ครบถ้วน 
                                ตามประกาศและระเบียบมหาวิทยาลัยเชียงใหม่ จึงจะถือว่าการรายงานตัวเป็นนักศึกษานั้นสมบูรณ์ <br><br>
                                สำหรับผู้ที่ไม่ไปรายงานตัวขึ้นทะเบียนเป็นนักศึกษาภายใน 10 วันทำการ นับจากวันที่กำหนดให้รายงานตัวขึ้นทะเบียนเป็นนักศึกษา 
                                จะถือว่าสละสิทธิ์การเข้าเป็นนักศึกษามหาวิทยาลัยเชียงใหม่ <br>
                                
                                <br>ทั้งนี้นักศึกษาใหม่ ระดับปริญญาตรี สามารถศึกษาข้อมูลเกี่ยวกับการรายงานตัวขึ้นทะเบียนเป็นนักศึกษาเพิ่มเติมได้ที่ 
                                &nbsp;
                                <a href="https://www3.reg.cmu.ac.th/stdinfo/undergraduate/"  target="_blank">
                                  <h3>  www3.reg.cmu.ac.th/stdinfo/undergraduate</h3>
                                </a>

                    </div>
                  
                  </div>
                </div>

                
            </div> 
           
        </div>
        <!--THIS IS IMPORTANT! to close the modal, the class name has to match the name given on the ID -->
        <!--DEMO02-->
        <div id="modal-02">
            <div id="closebt-container" class="close-modal-02">
                            
                <img class="closebt" src="<?php echo $this->Html->url('/img/close-cross-symbol-in-a-circle.svg'); ?>">
            </div>  
            
            <div id="modal-content">
              <h3><center> <font class="font-28"> การลงทะเบียนเรียน (ผ่านระบบออนไลน์)</font></center> </h3>
              
                  <div class="container">
                    <div class="row ">
                      <div class="col-md-6 text-left font-18">
                        <img class="img-responsive" src="<?php echo $this->Html->url('/img/messageImage_1622723755585.jpg'); ?>" alt="Hero Imgs" title="" align="center">
                  
                          
                                
                      </div> 
                      <div class="col-md-6 text-left font-18">
                        <img class="img-responsive" src="<?php echo $this->Html->url('/img/messageImage_1622723238026.jpg'); ?>" alt="Hero Imgs" title="" align="center">
                                

                          สำหรับนักศึกษาใหม่ ชั้นปีที่ 1 จะต้องลงทะเบียนเรียนแบบ (ตารางสำเร็จรูป package) ผ่านระบบออนไลน์ 
                          ของสำนักทะเบียนและประมวลผล ตามกำหนดการในปฏิทินการศึกษาจากสำนักทะเบียนและประมวลผล และชำระค่าธรรมเนียมการศึกษาให้เรียบร้อย 
                          จึงจะถือว่าการลงทะเบียนนั้นสมบูรณ์

                          <br>ทั้งนี้นักศึกษาสามารถเข้าศึกษาแยกตามหัวข้อได้ดังนี้
                            <ul>
                              <li style="padding: 10px 0px !important;">ค้นหากระบวนวิชาที่เปิดสอนได้ที่ &nbsp;
                                <a href="https://www1.reg.cmu.ac.th/registrationoffice/searchcourse.php" target="_blank" >
                                  <h3>  www1.reg.cmu.ac.th/registrationoffice/searchcourse.php</h3>

                                </a>
                              </li>
                              <li style="padding: 10px 0px !important;">เข้าระบบเพื่อลงทะเบียนออนไลน์ ได้ที่ &nbsp;
                                <a href="https://www1.reg.cmu.ac.th/webreg/th/undergraduate/?T=U" target="_blank" >
                                  <h3>https://www1.reg.cmu.ac.th/webreg/th/undergraduate/?T=U</h3>
                                </a>
                              </li>
                              <li style="padding: 10px 0px !important;">ดูรายละเอียดกระบวนวิชา (Courses Bulletin) 
                                  ได้ที่ &nbsp;
                                  <a href="https://mis.cmu.ac.th/tqf/coursepublic.aspx" target="_blank" >
                                    <h3>mis.cmu.ac.th/tqf/coursepublic.aspx</h3>
                                  </a>
                              </li>
                              <li style="padding: 10px 0px !important;">คู่มือการลงทะเบียนกระบวนวิชา 
                                ได้ที่ &nbsp;
                                <a href="https://www1.reg.cmu.ac.th/reg-ebook/views/index.php?v=2" target="_blank" >
                                  <h3>www1.reg.cmu.ac.th/reg-ebook/views/index.php?v=5</h3>
                                </a>
                              </li>
                            </ul>
                      </div>

                       
                     

                    </div>
                  </div>
             
            </div>
        </div>
        <!--DEMO03-->
        <div id="modal-03">
           
            <div id="closebt-container" class="close-modal-03">
                
                <img class="closebt" src="<?php echo $this->Html->url('/img/close-cross-symbol-in-a-circle.svg'); ?>">
            </div>  
            
            <div id="modal-content">
              <h3><center> <font class="font-28"> ข้อบังคับ /ประกาศ /แนวปฏิบัติ</font></center> </h3>
               
                  <div class="container">
                    <div class="row "> 
                      <div class="col-md-6 text-left font-18">
                       <img class="img-responsive" src="<?php echo $this->Html->url('/img/messageImage_1622725127184.jpg'); ?>" alt="Hero Imgs" title="" width="100%">
                        
                      </div>   
                      <div class="col-md-6 text-left font-18">
                         ข้อมูลเกี่ยวกับ ข้อบังคับ /ประกาศ /แนวปฏิบัติ / เป็นสิ่งที่นักศึกษาใหม่ต้องศึกษา และทำความเข้าใจตลอดระยะเวลาการศึกษา
                        เพื่อเป็นประโยชน์แก่ตัวนักศึกษา ซึ่งหัวข้อที่นักศึกษาควรทราบ มีดังนี้ 
                          <ul>
                            <li style="padding: 10px 0px !important;">ประกาศ/ข้อบังคับ มหาวิทยาลัยเชียงใหม่ &nbsp;
                            <a href="http://www.eqd.cmu.ac.th/Curr/rule_cmu.html" target="_blank" >www.eqd.cmu.ac.th/Curr/rule_cmu.html</a>
                            </li>
                            <li style="padding: 10px 0px !important;">แนวปฏิบัติ การลาพักการศึกษา &nbsp;
                            <a href="https://www1.reg.cmu.ac.th/web/reg-files/reg-absent.pdf" target="_blank" >
                            www1.reg.cmu.ac.th/web/reg-files/reg-absent.pdf</a></li><li style="padding: 10px 0px !important;">
                            แนวปฏิบัติ การลาออกจากการเป็นนักศึกษา &nbsp;
                            <a href="https://www1.reg.cmu.ac.th/web/reg-files/reg-resign.pdf" target="_blank" >
                            www1.reg.cmu.ac.th/web/reg-files/reg-resign.pdf</a></li><li style="padding: 10px 0px !important;">
                            แนวปฏิบัติ การถอนกระบวนวิชา (โดยได้รับอักษรลำดับขั้น W) &nbsp;
                            <a href="https://www1.reg.cmu.ac.th/registrationoffice/filesdownload/W-GuidelinesStudent.pdf" target="_blank" >
                            www1.reg.cmu.ac.th/registrationoffice/filesdownload/W-GuidelinesStudent.pdf</a></li><li style="padding: 10px 0px !important;">
                            แนวปฏิบัติ การขอเข้าร่วมศึกษากระบวนวิชา (โดยได้รับอักษรลำดับขั้น V) &nbsp;
                            <a href="https://www1.reg.cmu.ac.th/web/reg-files/reg-visiting.pdf" target="_blank" >
                            www1.reg.cmu.ac.th/web/reg-files/reg-visiting.pdf</a></li>
                          </ul>
                            
                             
                      </div>
                       
                    </div>
                  </div>
                 
                <!--Your modal content goes here-->
            </div>
        </div>
        <!--DEMO04-->
        <!-- <div id="modal-04">
            
           <div id="closebt-container" class="close-modal-04">
                <img class="closebt" src="<?php echo $this->Html->url('/img/close-cross-symbol-in-a-circle.svg'); ?>">
            </div>
            
            <div id="modal-content">
              <h3><center>  <font class="font-28"> คู่มือนักศึกษา</font></center> </h3>
               
                  <div class="container">
                    <div class="row ">
                      
                      <div class="col-md-6 pull-right">
                        <img class="img-responsive" src="<?php echo $this->Html->url('/img/messageImage_1622724789353.jpg'); ?>" alt="Hero Imgs" title="" width="70%">
                        
                      </div>
                      <div class="col-md-6 text-left font-18">
                          <p>ปีการศึกษา 2567</p>

                          <p><a href="http://www.agri.cmu.ac.th//2017/files/AgriPersonal/3725/บรรยาย คณะเกษตร.pdf">1. การให้คำปรึกษาด้านทะเบียนและประมวลผลการศึกษา&nbsp;ระดับปริญญาตรี</a>&nbsp;โดย รศ.ดร.ปิยะลักษณ์ พุทธวงศ์ รองผู้อำนวยการสำนักทะเบียนและประมวลผล</p>
  
                          <p><a href="http://www.agri.cmu.ac.th//2017/files/AgriPersonal/3725/Curriculum_2023.pdf">4. โครงสร้างหลักสูตร แผนการศึกษา</a> โดย ผศ.ดร.ฟ้าไพลิน ไชยวรรณ&nbsp;ผู้ช่วยคณบดีฝ่ายวิชาการ</p>

                          <p><a href="http://www.agri.cmu.ac.th//2017/files/AgriPersonal/3725/ปัญหางานทะเบียนที่พบบ่อย_66.pdf">5.ปัญหาการลงทะเบียนเรียน</a> โดย ผศ.ดร.ฟ้าไพลิน ไชยวรรณ ผู้ช่วยคณบดีฝ่ายวิชาการ</p>

                          
                          <p>&nbsp;</p>

                      </div>
                    
                    </div>
                  </div>
                   
                
            </div>
        </div> -->
        <!--DEMO05-->
        <!-- <div id="modal-05">
          
           <div id="closebt-container" class="close-modal-05">
                <img class="closebt" src="<?php echo $this->Html->url('/img/close-cross-symbol-in-a-circle.svg'); ?>">
            </div>
            
            <div id="modal-content">
              <h3><center> <font class="font-28" style="color: #E1BB00;">
              <img class="img-responsive" src="<?php echo $this->Html->url('/img/logo_small.png'); ?>" alt="">
                TCAS รอบที่ 4</font><br><font class="font-28"> รับตรงอิสระ (ทุนอุดมศึกษาเพื่อพัฒนาจังหวัดชายแดนภาคใต้)</font></center> </h3>
                
                  <div class="container">
                    <div class="row ">
                      
                     
                      <div class="col-md-12 text-left font-18">
                        
                      </div>
                    
                    </div>
                  </div>
                   
                <!--Your modal content goes here-->
            </div>
        </div>  
         <!--DEMO06-->
        <div id="modal-06">
          
           <div id="closebt-container" class="close-modal-06">
                <img class="closebt" src="<?php echo $this->Html->url('/img/close-cross-symbol-in-a-circle.svg'); ?>">
            </div>
            
            <div id="modal-content">
              <h3><center>  <font class="font-28"> ระบบงานทะเบียนการศึกษา </font></center> </h3>
               
                  <div class="container">
                    <div class="row ">
                      
                      <div class="col-md-6">
                        <img class="img-responsive" src="<?php echo $this->Html->url('/img/messageImage_1622724789353.jpg'); ?>" alt="Hero Imgs" title="" width="100%">
                      </div>
                      <div class="col-md-6 text-left font-18">
                        นักศึกษาใหม่ชั้นปีที่ 1 ควรจะต้องทราบ ระบบงานทะเบียนการศึกษา ซึ่งมีความสำคัญในการใช้บริการด้านต่างๆ เกี่ยวกับการศึกษา โดยที่ระบบงานทะเบียนการศึกษาให้บริการดังนี้
                        <ul>
                            <li style="padding: 10px 0px !important;">ระบบลงทะเบียน เพิ่ม-ถอน กระบวนวิชา</li>
                            <li style="padding: 10px 0px !important;">ระบบลงทะเบียนเพื่อใช้บริการมหาวิทยาลัย</li>
                            <li style="padding: 10px 0px !important;">ค้นหากระบวนวิชาที่เปิดสอน สรุปผลการลงทะเบียน </li>
                            <li style="padding: 10px 0px !important;">ข้อมูลส่วนบุคคล ดูผลการศึกษา(ดูเกรด)</li>
                            <li style="padding: 10px 0px !important;">ระบบลาพักการศึกษา ระบบลาออกจากการเป็นนักศึกษา</li>
                            <li style="padding: 10px 0px !important;">ระบบขอถอนกระบวนวิชาโดยได้รับอักษรลำดับขั้น W</li>
                            <li style="padding: 10px 0px !important;">ระบบขอเข้าร่วมศึกษากระบวนวิชาโดยได้รับอักษรลำดับขั้น V</li>
                            <li style="padding: 10px 0px !important;">ระบบขอวัดและประเมินผลโดยได้รับอักษรลำดับขั้น I</li>
                            <li style="padding: 10px 0px !important;">พิมพ์ใบแจ้งผลการลงทะเบียน(มชท.50)</li>
                            <li style="padding: 10px 0px !important;">พิมพ์ใบเสร็จรับเงิน กรณีชำระเงินผ่าน QR code</li>
                            <li style="padding: 10px 0px !important;">และบริการอื่นๆ เกี่ยวกับงานทะเบียนการศึกษา</li>
                            
                          </ul>
                          <hr>
                          นักศึกษาสามารถศึกษารายละเอียดเพิ่มเติมได้ที่ <a href="https://www1.reg.cmu.ac.th/webreg/th/undergraduate/?T=U" target="_blank" >
                          https://www1.reg.cmu.ac.th/webreg/th/undergraduate/?T=U</a>
                      </div>
                    
                    </div>
                  </div>
                   
                <!--Your modal content goes here-->
            </div>
        </div>   
     
        
    </section><!-- End Services Section -->


    <!-- ======= Services Section ======= -->
    <!-- End Services Section -->

  <!--==========================
    แผนผังคณะเกษตรศาสตร์
  ============================-->
 						
    <section id="advisor" class="padd-section text-center wow fadeInUp" >
      
      <div class="container">
        <div class="section-title  ">
        <h2>Faculty Area</h2>
        <h3> <font class="font-44" style="color: #E1BB00;">แผนผังอาคารเรียน</font><font class="font-44" >คณะเกษตรศาสตร์  </font> </h3>
             
             
        </div>
      </div>
      
      <img class="img-responsive" src="<?php echo $this->base.'/img/fac_map.jpg' ?>"width="100%">
      
    </section>  
    
    
    <!-- End Team Section -->


    <!--==========================
    CMU Mobile
  ============================-->
 	<hr>
</main><!-- End #main -->
