<div class="row">
    <div class="panel-header">
        <br>
        <div class="container" align="center">
            <h3>จำนวนนักศึกษาที่มีคุณลักษณะบัณฑิตตามที่มหาวิทยาลัยกำหนด (CMU Student DNA Blueprint)</h3>
        </div>
        <div class="panel-body">
            <div class="btn-group">
                <?php if($admins != null) { ?>
                <a class="btn btn-success" role="button" target="_blank" href="<?PHP echo $this->Html->url(array('action'=>'add_student_dna')); ?>">เพิ่มข้อมูล</a>
                <?php } ?>
            </div>
            <div class="btn-group">
                <form method="get">
                    <div class="dropdown">
                        <button class="btn btn-info btn-default dropdown-toggle" type="button" id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aira-expanded="false">
                            <?php echo $years['Yearterm']['year'];?>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu">
                            <?php foreach ($dropdowns as $dropdown) { ?>
                                <?php
                                    if ($years['Yearterm']['year'] == $dropdown['Yearterm']['year'])
                                    {
                                        $active = 'active';
                                    }
                                    else
                                    {
                                        $active = '';
                                    }
                                ?>
                                <li class="<?php echo $active;?>">
                                    <a href="<?php echo $this->Html->url(array('action' => 'student_dna',$dropdown['Yearterm']['year']));?>">
                                        <font color="black"><?php echo $dropdown['Yearterm']['year'];?></font>
                                    </a>
                                </li>
                                <?php } ?>
                        </ul>
                    </div>
                </form>
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th style="text-align:center" colspan="" rowspan="" width="1%">ลำดับ</th>
                        <th style="text-align:center" colspan="" rowspan="" width="30%">รหัสนักศึกษา</th>
                        <th style="text-align:center" colspan="" rowspan="" width="30%">ชื่อ - นามสกุล</th>
                        <th style="text-align:center" colspan="" rowspan="" width="10%">ระดับ</th>
                        <th style="text-align:center" colspan="" rowspan="" width="10%">เดือน</th>
                        <th style="text-align:center" colspan="" rowspan="" width="10%">ปี</th>
                        <?php if($admins != null) { ?>
                        <th style="text-align:center" colspan="" rowspan="" width="1%">แก้ไขข้อมูล</th>
                        <th style="text-align:center" colspan="" rowspan="" width="1%">ลบข้อมูล</th>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colspan="20">ไตรมาส1</td>
                    </tr>
                    <tr>
                        <?php
                            $i = 0;
                            foreach ($student_dna1 as $student_dna) {$i++;
                        ?>
                        <td><?php echo $i; ?></td>
                        <td style="text-align:center;"><?php echo $student_dna['StudentDna']['student_id']; ?></td>
                        <td style="text-align:center;"><?php echo $student_dna['StudentDna']['stu_name']; ?></td>
                        <td style="text-align:center;"><?php echo $student_dna['StudentDna']['degree_id']; ?></td>
                        <td style="text-align:center;"><?php echo $student_dna['StudentDna']['month']; ?></td>
                        <td style="text-align:center;"><?php echo $student_dna['StudentDna']['year']; ?></td>
                        <?php if($admins != null) { ?>
                        <td style="text-align:center;"><a class="btn btn-warning" role="button" target="_blank" href="<?php echo $this->Html->url(array('action'=>'edit_student_dna',$student_dna['StudentDna']['id'])); ?>">แก้ไขข้อมูล</a></td>
                        <td style="text-align:center;"><a onclick="return confirm('ยืนยันการลบข้อมูล')" class="btn btn-danger" role="button" href="<?php echo $this->Html->url(array('action'=>'remove_student_dna',$student_dna['StudentDna']['id'])); ?>">ลบข้อมูล</td>
                        <?php } ?>
                    </tr>
                        <?php } ?>
                        <tr>
                        <td colspan="20">ไตรมาส2</td>
                    </tr>
                    <tr>
                        <?php
                            foreach ($student_dna2 as $student_dna) {$i++;
                        ?>
                        <td><?php echo $i; ?></td>
                        <td style="text-align:center;"><?php echo $student_dna['StudentDna']['student_id']; ?></td>
                        <td style="text-align:center;"><?php echo $student_dna['StudentDna']['stu_name']; ?></td>
                        <td style="text-align:center;"><?php echo $student_dna['StudentDna']['degree_id']; ?></td>
                        <td style="text-align:center;"><?php echo $student_dna['StudentDna']['month']; ?></td>
                        <td style="text-align:center;"><?php echo $student_dna['StudentDna']['year']; ?></td>
                        <?php if($admins != null) { ?>
                        <td style="text-align:center;"><a class="btn btn-warning" role="button" target="_blank" href="<?php echo $this->Html->url(array('action'=>'edit_student_dna',$student_dna['StudentDna']['id'])); ?>">แก้ไขข้อมูล</a></td>
                        <td style="text-align:center;"><a onclick="return confirm('ยืนยันการลบข้อมูล')" class="btn btn-danger" role="button" href="<?php echo $this->Html->url(array('action'=>'remove_student_dna',$student_dna['StudentDna']['id'])); ?>">ลบข้อมูล</td>
                        <?php } ?>
                    </tr>
                        <?php } ?>
                        <tr>
                        <td colspan="20">ไตรมาส3</td>
                    </tr>
                    <tr>
                        <?php
                            foreach ($student_dna3 as $student_dna) {$i++;
                        ?>
                        <td><?php echo $i; ?></td>
                        <td style="text-align:center;"><?php echo $student_dna['StudentDna']['student_id']; ?></td>
                        <td style="text-align:center;"><?php echo $student_dna['StudentDna']['stu_name']; ?></td>
                        <td style="text-align:center;"><?php echo $student_dna['StudentDna']['degree_id']; ?></td>
                        <td style="text-align:center;"><?php echo $student_dna['StudentDna']['month']; ?></td>
                        <td style="text-align:center;"><?php echo $student_dna['StudentDna']['year']; ?></td>
                        <?php if($admins != null) { ?>
                        <td style="text-align:center;"><a class="btn btn-warning" role="button" target="_blank" href="<?php echo $this->Html->url(array('action'=>'edit_student_dna',$student_dna['StudentDna']['id'])); ?>">แก้ไขข้อมูล</a></td>
                        <td style="text-align:center;"><a onclick="return confirm('ยืนยันการลบข้อมูล')" class="btn btn-danger" role="button" href="<?php echo $this->Html->url(array('action'=>'remove_student_dna',$student_dna['StudentDna']['id'])); ?>">ลบข้อมูล</td>
                        <?php } ?>
                    </tr>
                        <?php } ?>
                        <tr>
                        <td colspan="20">ไตรมาส4</td>
                    </tr>
                    <tr>
                        <?php
                            foreach ($student_dna4 as $student_dna) {$i++;
                        ?>
                        <td><?php echo $i; ?></td>
                        <td style="text-align:center;"><?php echo $student_dna['StudentDna']['student_id']; ?></td>
                        <td style="text-align:center;"><?php echo $student_dna['StudentDna']['stu_name']; ?></td>
                        <td style="text-align:center;"><?php echo $student_dna['StudentDna']['degree_id']; ?></td>
                        <td style="text-align:center;"><?php echo $student_dna['StudentDna']['month']; ?></td>
                        <td style="text-align:center;"><?php echo $student_dna['StudentDna']['year']; ?></td>
                        <?php if($admins != null) { ?>
                        <td style="text-align:center;"><a class="btn btn-warning" role="button" target="_blank" href="<?php echo $this->Html->url(array('action'=>'edit_student_dna',$student_dna['StudentDna']['id'])); ?>">แก้ไขข้อมูล</a></td>
                        <td style="text-align:center;"><a onclick="return confirm('ยืนยันการลบข้อมูล')" class="btn btn-danger" role="button" href="<?php echo $this->Html->url(array('action'=>'remove_student_dna',$student_dna['StudentDna']['id'])); ?>">ลบข้อมูล</td>
                        <?php } ?>
                    </tr>
                        <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>