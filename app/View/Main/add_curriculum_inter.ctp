<div class="container">
      <div class="row">
            <div class="col-md-12">
                  
                        <form method="post" accept-charset="utf-8">
                        <div class="panel panel-success">
                              <div class="panel-heading">
                                    <h3 class="panel-title"><i class="glyphicon glyphicon-plus"></i>เพิ่มข้อมูล</h3>
                              </div>
                              <div class="panel-body">
                                    <label class="col-sm-4 control-label">รายชื่อวิชา</label>
                                    <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                                <?php
                                                      echo $this->Form->input('CurriculumInterInfo.title_th',array(
                                                            'type' => 'text',
                                                            'label' => false,
                                                            'div' => false,
                                                            'class' => array('form-control css-require'),
                                                            'error' => false,
                                                            'placeholder' => 'รายชื่อวิชา',
                                                            'required'
                                                      ));
                                                ?>
                                          </div>
                                    </div>
                                    <label class="col-sm-4 control-label">ประเภทหลักสูตร</label>
                                    <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                                <?php
                                                      echo $this->Form->input('CurriculumInterType.name',array(
                                                            'type' => 'text',
                                                            'label' => false,
                                                            'div' => false,
                                                            'class' => array('form-control css-require'),
                                                            'error' => false,
                                                            'placeholder' => 'ประเภทหลักสูตร',
                                                            'required'
                                                      ));
                                                ?>
                                          </div>
                                    </div>
                                    <label class="col-sm-4 control-label">เดือน</label>
                                    <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                                <?php
                                                $months = array(
                                                      '1' => '1',
                                                      '2' => '2',
                                                      '3' => '3',
                                                      '4' => '4',
                                                      '5' => '5',
                                                      '6' => '6',
                                                      '7' => '7',
                                                      '8' => '8',
                                                      '9' => '9',
                                                      '10' => '10',
                                                      '11' => '11',
                                                      '12' => '12',
                                                );
                                                echo $this->Form->input('CurriculumInterInfo.month', array(
                                                      'options' => $months,
                                                      'label' => false,
                                                      'div' => false,
                                                      'class' => array('form-control css-require'),                                                           
                                                      'error' => false,
                                                      'required'
                                                ));
                                                ?>
                                          </div>
                                    </div>
                                    <label class="col-sm-4 control-label">ปีที่กรอกข้อมูล</label>
                                    <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                                <?php
                                                      echo $this->Form->input('CurriculumInterInfo.year',array(
                                                            'type' => 'text',
                                                            'label' => false,
                                                            'div' => false,
                                                            'class' => array('form-control css-require'),
                                                            'error' => false,
                                                            'placeholder' => 'ปีที่กรอกข้อมูล',
                                                            'required'
                                                      ));
                                                ?>
                                          </div>
                                    </div>
                                    <label class="col-sm-4 control-label">แหล่งอ้างอิง</label>
                                    <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                                <?php
                                                      echo $this->Form->input('CurriculumInterInfo.link_th',array(
                                                            'type' => 'text',
                                                            'label' => false,
                                                            'div' => false,
                                                            'class' => array('form-control css-require'),
                                                            'error' => false,
                                                            'placeholder' => 'แหล่งอ้างอิง',
                                                            'required'
                                                      ));
                                                ?>
                                          </div>
                                    </div>
                                    <div class="form-group"><br>
                                          <input type="submit" class="btn btn-success" value="บันทึกข้อมูล" onclick="return confirm('ยืนยันการเพิ่มข้อมูล')">
                                    </div>
                              </div>
                        </div>
                        </form>
            </div>
      </div>
</div>