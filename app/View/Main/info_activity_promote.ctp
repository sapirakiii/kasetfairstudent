<?php //debug($InfoDocument) ?>
<?php
	function DateThai($strDate)
		{

		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//, $strHour:$strMinute
	}
	?>
<div class="container" style="padding:10px 0;">	
	<div class="row">
		<div class="col-md-12">
     
		<div class="panel panel-default" style="border-color: #F2F2F2!important">
		<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<h3 style="margin:0px !important;">
						<center>  <div class="font_panel2"><?php echo $new['Info']['Title'] ?></div>
            </center>
			</h3> 
		
		</div>
		<div class="panel-body">
			
			<?php
				$img = "";
				$img2 = "";
				$img3 = 0;

				$i = 0;

				echo "<div class='list-group gallery'>";
				
				foreach ($InfoDocuments as $InfoDocument) {
					
					if(($i%6) == 0)
					{	
						echo '<div class="row">';
					}

					if ($InfoDocument['InfoDocument']['class'] == "img") { ?>
					
            <div class='col-md-2' >
                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $this->Html->url('/files/Document/').''. $InfoDocument['InfoDocument']['name'] ?>">
                    <img class="img-responsive" alt="" src="<?php echo $this->Html->url('/files/Document/').''. $InfoDocument['InfoDocument']['name'] ?>" width="100%"  />
                    <div class='text-right'>
                        <small class='text-muted'><?php //echo $new['Info']['Title']; ?></small>
                    </div> 
                </a>
            </div>
					
				<?php }
					if(($i%6) == 5)
					{	
						echo '</div>';
					}
					++$i;
				}
				echo " </div>";
				echo "<br><hr>".$img2;
			?>
				<!-- col-sm-4 col-xs-6 col-md-3 col-lg-3	 -->
		</div>
	   <hr>
		<div style="padding-left: 10px;">
		<b>เอกสารเผยแพร่ :</b>
		
		<?php
				
				foreach ($new['InfoDocument'] as $InfoDocument) {
					if ($InfoDocument['class'] == "file") { ?>
			
						<a href="<?php echo $this->Html->url('/files/Document/') , $InfoDocument['name']; ?>" class="btn btn-primary" target="_blank">Download <?php echo $InfoDocument['name_old']; ?> <span class="glyphicon glyphicon-cloud-download"></span></a>
			<?php
					}
				}
				
			?>
		</div> 
		<hr>
		<div style="padding-left: 10px;">	
		
			<div class="font_panel6"> <span class="glyphicon glyphicon-tags" aria-hidden="true"></span> <b>หมวดหมู่: </b> <?php echo $new['Typenew']['TypeNews'] ?> </div> 
			<div class="font_panel6"> <span class="glyphicon glyphicon-time" aria-hidden="true"></span> <b>วันที่เผยแพร่ : </b> <?php echo DateThai($new['Info']['postdate'])  ?> </div> 
			
			<div class="font_panel6"> <span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> <b> เปิดอ่าน : </b> <?php echo $new['Info']['CountRead'] ?> ครั้ง </div> 
			
			
		</div>
	</div>

</div>

</div>
          </div>
        </div>
    </div>
 
<div style="clear: both;"></div>