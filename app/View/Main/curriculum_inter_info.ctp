<?php
date_default_timezone_set('Asia/Bangkok');
$date = date("Y-m-d");
$time = date("H:i:s");


function DateDiff($strDate1, $strDate2)
{
	return (strtotime($strDate2) - strtotime($strDate1)) /  (60 * 60 * 24);  // 1 day = 60*60*24
}

function TimeDiff($strTime1, $strTime2)
{
	return (strtotime($strTime2) - strtotime($strTime1)) /  (60 * 60); // 1 Hour =  60*60                           
}




?>
<div class="container-fluid" style="padding:15px 0 0;">
	<div class="panel panel-default" style="border-color: #F2F2F2!important">
		<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center>
						<div class="font_panel2">จำนวนหลักสูตรเชิงบูรณาการ/หลักสูตรนานาชาติ/Double degree (ปีการศึกษา <?php echo $lastyearterms['Yearterm']['year'] ?>)</div>
					</center>
				</h3>
				<?php if($admins != null) { ?>
				<a href="<?php echo $this->Html->url(array('action'=> 'add_curriculum_inter')); ?>" role="button" target="_blank" class="btn btn-success">เพิ่มข้อมูล</a>
				<?php } ?>
				<br>
				กรุณาเลือกปีการศึกษา
				<form name="frmselect" method="get"> 	
					<div class="dropdown"  style="float: left;">
						<button class="btn btn-default dropdown-toggle btn-info" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<?php echo $years['Yearterm']['year']; ?>
						</button>
						<ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
							<?php foreach ($dropdowns as $dropdown) { ?>
								<?php 
									if($years['Yearterm']['year'] == $dropdown['Yearterm']['year']){
										$active = "active";
									}else{
										$active = "";
									}
									// $yearList = $dropdown['Yearterm']['year'];
								?>
								<li class="<?php echo $active;?>">
									<a href="<?php echo $this->Html->url(array('action' => 'curriculum_inter_info',$dropdown['Yearterm']['year'])); ?>">
										<font color="black"><?php echo $dropdown['Yearterm']['year']; ?></font>
									</a>
								</li>
							<?php } ?>
						</ul>
					</div>
					
				</form>	



			</div>
		</div>
		<div id="no-more-tables">
			<table class="table table-bordered">
				<tr>
					<th style="text-align: center;" ><font size="3px" >ลำดับ</font></th>
					<th style="text-align: center;" ><font size="3px">รายชื่อวิชา</font></th> 
					<th style="text-align: center;" ><font size="3px">ประเภทหลักสูตร</font></th>
					<th style="text-align: center;" ><font size="3px">เดือนที่กรอกข้อมูล</font></th>
					<th style="text-align: center;" ><font size="3px">ปีที่กรอกข้อมูล</font></th>
					<th style="text-align: center;" ><font size="3px">แหล่งอ้างอิง</font></th>
					<?php if($admins != null) { ?>
					<th style="text-align: center;" width="1%"><font size="3px">แก้ไขข้อมูล</font></th>
					<th style="text-align: center;" width="1%"><font size="3px">ลบข้อมูล</font></th>
					<?php } ?>
				</tr>
				<tr>
					<td colspan="8">ไตรมาส 1</td>
				</tr>
					<?php
						$i = 0;
						foreach ($curriculum_inter_infos1 as $curriculum_inter_info){ $i++;
					?>
				<tr>
						<td style="text-align:center;"><?php echo $i ?></td>
						<td style="text-align:center;"><?php echo $curriculum_inter_info['CurriculumInterInfo']['title_th']?></td> 
						<td style="text-align:center;"><?php echo $curriculum_inter_info['CurriculumInterType']['name']?></td>
						<td style="text-align:center;"><?php echo $curriculum_inter_info['CurriculumInterInfo']['month']?></td>
						<td style="text-align:center;"><?php echo $curriculum_inter_info['CurriculumInterInfo']['year']?></td>
						<td style="text-align:center;">
							<a class="btn btn-info" target="_blank" href="<?php echo $curriculum_inter_info['CurriculumInterInfo']['link_th'] ?>">
								เอกสารประกอบหลักสูตร
							</a>
						</td>
						<?php if($admins != null) { ?>
						<td><a href="<?php echo $this->Html->url(array('action'=> 'edit_curriculum_inter',$curriculum_inter_info['CurriculumInterInfo']['id']));?>" role="button" target="_blank" class="btn btn-warning">แก้ไขข้อมูล</a></td>
						<td><a href="<?php echo $this->Html->url(array('action'=> 'remove_curriculum_inter',$curriculum_inter_info['CurriculumInterInfo']['id']));?>" role="button" class="btn btn-danger" onclick="return confirm('ยืนยันนการลบข้อมูล')">ลบข้อมูล</a></td>
						<?php } ?>
					</tr>
					<?php } ?>
				<tr>
					<td colspan="8">ไตรมาส 2</td>
				</tr>
					<?php
					 
						foreach ($curriculum_inter_infos2 as $curriculum_inter_info){ $i++;
					?>
				<tr>
						<td style="text-align:center;"><?php echo $i ?></td>
						<td style="text-align:center;"><?php echo $curriculum_inter_info['CurriculumInterInfo']['title_th']?></td> 
						<td style="text-align:center;"><?php echo $curriculum_inter_info['CurriculumInterType']['name']?></td>
						<td style="text-align:center;"><?php echo $curriculum_inter_info['CurriculumInterInfo']['month']?></td>
						<td style="text-align:center;"><?php echo $curriculum_inter_info['CurriculumInterInfo']['year']?></td>
						<td style="text-align:center;">
							<a class="btn btn-info" target="_blank" href="<?php echo $curriculum_inter_info['CurriculumInterInfo']['link_th'] ?>">
								เอกสารประกอบหลักสูตร
							</a>
						</td>
						<?php if($admins != null) { ?>
						<td><a href="<?php echo $this->Html->url(array('action'=> 'edit_curriculum_inter',$curriculum_inter_info['CurriculumInterInfo']['id']));?>" role="button" target="_blank" class="btn btn-warning">แก้ไขข้อมูล</a></td>
						<td><a href="<?php echo $this->Html->url(array('action'=> 'remove_curriculum_inter',$curriculum_inter_info['CurriculumInterInfo']['id']));?>" role="button" class="btn btn-danger" onclick="return confirm('ยืนยันนการลบข้อมูล')">ลบข้อมูล</a></td>
						<?php } ?>
					</tr>
					<?php } ?>
				<tr>
					<td colspan="8">ไตรมาส 3</td>
				</tr>
					<?php
						 
						foreach ($curriculum_inter_infos3 as $curriculum_inter_info){ $i++;
					?>
				<tr>
						<td style="text-align:center;"><?php echo $i ?></td>
						<td style="text-align:center;"><?php echo $curriculum_inter_info['CurriculumInterInfo']['title_th']?></td> 
						<td style="text-align:center;"><?php echo $curriculum_inter_info['CurriculumInterType']['name']?></td>
						<td style="text-align:center;"><?php echo $curriculum_inter_info['CurriculumInterInfo']['month']?></td>
						<td style="text-align:center;"><?php echo $curriculum_inter_info['CurriculumInterInfo']['year']?></td>
						<td style="text-align:center;">
							<a class="btn btn-info" target="_blank" href="<?php echo $curriculum_inter_info['CurriculumInterInfo']['link_th'] ?>">
								เอกสารประกอบหลักสูตร
							</a>
						</td>
						<?php if($admins != null) { ?>
						<td><a href="<?php echo $this->Html->url(array('action'=> 'edit_curriculum_inter',$curriculum_inter_info['CurriculumInterInfo']['id']));?>" role="button" target="_blank" class="btn btn-warning">แก้ไขข้อมูล</a></td>
						<td><a href="<?php echo $this->Html->url(array('action'=> 'remove_curriculum_inter',$curriculum_inter_info['CurriculumInterInfo']['id']));?>" role="button" class="btn btn-danger" onclick="return confirm('ยืนยันนการลบข้อมูล')">ลบข้อมูล</a></td>
						<?php } ?>
					</tr>
					<?php } ?>
				<tr>
					<td colspan="8">ไตรมาส 4</td>
				</tr>
					<?php
						 
						foreach ($curriculum_inter_infos4 as $curriculum_inter_info){ $i++;
					?>
				<tr>
						<td style="text-align:center;"><?php echo $i ?></td>
						<td style="text-align:center;"><?php echo $curriculum_inter_info['CurriculumInterInfo']['title_th']?></td> 
						<td style="text-align:center;"><?php echo $curriculum_inter_info['CurriculumInterType']['name']?></td>
						<td style="text-align:center;"><?php echo $curriculum_inter_info['CurriculumInterInfo']['month']?></td>
						<td style="text-align:center;"><?php echo $curriculum_inter_info['CurriculumInterInfo']['year']?></td>
						<td style="text-align:center;">
							<a class="btn btn-info" target="_blank" href="<?php echo $curriculum_inter_info['CurriculumInterInfo']['link_th'] ?>">
								เอกสารประกอบหลักสูตร
							</a>
						</td>
						<?php if($admins != null) { ?>
						<td><a href="<?php echo $this->Html->url(array('action'=> 'edit_curriculum_inter',$curriculum_inter_info['CurriculumInterInfo']['id']));?>" role="button" target="_blank" class="btn btn-warning">แก้ไขข้อมูล</a></td>
						<td><a href="<?php echo $this->Html->url(array('action'=> 'remove_curriculum_inter',$curriculum_inter_info['CurriculumInterInfo']['id']));?>" role="button" class="btn btn-danger" onclick="return confirm('ยืนยันนการลบข้อมูล')">ลบข้อมูล</a></td>
						<?php } ?>
				</tr>
				<?php } ?>
			</table>
			<div class="alert alert-info">
						KPI OWNER :กรศนันท์ สิทธิกุล
			</div>
		</div>
	</div>
</div>