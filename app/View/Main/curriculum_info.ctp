<div class="container">
    <div class="panel-header" align="center">
        <h3>จำนวนหลักสูตรที่สร้างบรรยากาศการเรียนรู้ในลักษณะ CWIE, Pi-shaped skills, Entrepreneurship</h3>
    </div>
</div>
<div class="panel-body">
<br>


    <div class="btn-group">
        <?php if($admins != null) { ?>
        <a class="btn btn-success" role="button" target="_blank" href="<?php echo $this->Html->url(array('action'=>'add_curriculum_info')); ?>">เพิ่มข้อมูล</a>
        <?php } ?>
    </div>
    กรุณาเลือกปีการศึกษา
        <form method="get">
            <div class="dropdown">
                <button class="btn btn-info btn-default dropdown-toggle" type="button" id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aira-expanded="false">
                    <?php echo $years['Yearterm']['year'];?>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu">
                    <?php foreach ($dropdowns as $dropdown) { ?>
                        <?php
                            if ($years['Yearterm']['year'] == $dropdown['Yearterm']['year'])
                            {
                                $active = 'active';
                            }
                            else
                            {
                                $active = '';
                            }
                        ?>
                        <li class="<?php echo $active;?>">
                            <a href="<?php echo $this->Html->url(array('action' => 'curriculum_info',$dropdown['Yearterm']['year']));?>">
                                <font color="black"><?php echo $dropdown['Yearterm']['year'];?></font>
                            </a>
                        </li>
                        <?php } ?>
                </ul>
            </div>
        </form>
    <table class="table table-bordered">
        <thead>
            <th style="text-align:center;" colspan="" rowspan="" width="1%">ลำดับ</th>
            <th style="text-align:center;" colspan="" rowspan="" width="20%">หลักสูตร</th>
            <th style="text-align:center;" colspan="" rowspan="" width="20%">หลักสูตร(ENG)</th>
            <th style="text-align:center;" colspan="" rowspan="" width="10%">ระดับ</th>
            <th style="text-align:center;" colspan="" rowspan="" width="10%">ลิ้งค์</th>
            <th style="text-align:center;" colspan="" rowspan="" width="10%">เดือน</th>
            <th style="text-align:center;" colspan="" rowspan="" width="10%">ปี</th>
            <?php if($admins != null) { ?>
            <th style="text-align:center;" colspan="" rowspan="" width="1%">แก้ไขข้อมูล</th>
            <th style="text-align:center;" colspan="" rowspan="" width="1%">ลบข้อมูล</th>
            <?php } ?>
        </thead>
        <tbody>
            <tr>
                <td colspan="20">ไตรมาส1</td>
            </tr>
            <tr>
                <?php
                    $i = 0;
                    foreach ($curriculum_infos1 as $curriculum_info){$i++;
                ?>
                <td style="text-align:center;"><?php echo $i; ?></td>
                <td style="text-align:center;"><?php echo $curriculum_info['CurriculumInfo']['title_th'] ?></td>
                <td style="text-align:center;"><?php echo $curriculum_info['CurriculumInfo']['title_en'] ?></td>
                <td style="text-align:center;"><?php echo $curriculum_info['CurriculumInfo']['degree_id'] ?></td>
                <td style="text-align:center;"><a class="btn-info btn" href="<?php echo $curriculum_info['CurriculumInfo']['link_th'] ?>" title="กดเพื่อเข้าดูข้อมูล">ลิ้งค์เข้าดูรายละเอียดเพิ่มเติม</a></td>
                <td style="text-align:center;"><?php echo $curriculum_info['CurriculumInfo']['month'] ?></td>
                <td style="text-align:center;"><?php echo $curriculum_info['CurriculumInfo']['year'] ?></td>
                <?php if($admins != null) { ?>
                <td style="text-align:center;"><a class="btn btn-warning" role="button" target="_blank" href="<?php echo $this->Html->url(array('action' => 'edit_curriculum_info',$curriculum_info['CurriculumInfo']['id'])); ?>" >แก้ไขข้อมูล</a></td>
                <td style="text-align:center;"><a class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')" href="<?php echo $this->Html->url(array('action'=>'remove_curriculum_info',$curriculum_info['CurriculumInfo']['id'])); ?>">ลบข้อมูล</a></td>
                <?php } ?>
            </tr>
                <?php } ?>
                <tr>
                <td colspan="20">ไตรมาส2</td>
            </tr>
            <tr>
                <?php
                    $i = 0;
                    foreach ($curriculum_infos2 as $curriculum_info){$i++;
                ?>
                <td style="text-align:center;"><?php echo $i; ?></td>
                <td style="text-align:center;"><?php echo $curriculum_info['CurriculumInfo']['title_th'] ?></td>
                <td style="text-align:center;"><?php echo $curriculum_info['CurriculumInfo']['title_en'] ?></td>
                <td style="text-align:center;"><?php echo $curriculum_info['CurriculumInfo']['degree_id'] ?></td>
                <td style="text-align:center;"><a class="btn-info btn" href="<?php echo $curriculum_info['CurriculumInfo']['link_th'] ?>" title="กดเพื่อเข้าดูข้อมูล">ลิ้งค์เข้าดูรายละเอียดเพิ่มเติม</a></td>
                <td style="text-align:center;"><?php echo $curriculum_info['CurriculumInfo']['month'] ?></td>
                <td style="text-align:center;"><?php echo $curriculum_info['CurriculumInfo']['year'] ?></td>
                <?php if($admins != null) { ?>
                <td style="text-align:center;"><a class="btn btn-warning" role="button" target="_blank" href="<?php echo $this->Html->url(array('action' => 'edit_curriculum_info',$curriculum_info['CurriculumInfo']['id'])); ?>" >แก้ไขข้อมูล</a></td>
                <td style="text-align:center;"><a class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')" href="<?php echo $this->Html->url(array('action'=>'remove_curriculum_info',$curriculum_info['CurriculumInfo']['id'])); ?>">ลบข้อมูล</a></td>
                <?php } ?>
            </tr>
                <?php } ?>
                <tr>
                <td colspan="20">ไตรมาส3</td>
            </tr>
            <tr>
                <?php
                    $i = 0;
                    foreach ($curriculum_infos3 as $curriculum_info){$i++;
                ?>
                <td style="text-align:center;"><?php echo $i; ?></td>
                <td style="text-align:center;"><?php echo $curriculum_info['CurriculumInfo']['title_th'] ?></td>
                <td style="text-align:center;"><?php echo $curriculum_info['CurriculumInfo']['title_en'] ?></td>
                <td style="text-align:center;"><?php echo $curriculum_info['CurriculumInfo']['degree_id'] ?></td>
                <td style="text-align:center;"><a class="btn-info btn" href="<?php echo $curriculum_info['CurriculumInfo']['link_th'] ?>" title="กดเพื่อเข้าดูข้อมูล">ลิ้งค์เข้าดูรายละเอียดเพิ่มเติม</a></td>
                <td style="text-align:center;"><?php echo $curriculum_info['CurriculumInfo']['month'] ?></td>
                <td style="text-align:center;"><?php echo $curriculum_info['CurriculumInfo']['year'] ?></td>
                <?php if($admins != null) { ?>
                <td style="text-align:center;"><a class="btn btn-warning" role="button" target="_blank" href="<?php echo $this->Html->url(array('action' => 'edit_curriculum_info',$curriculum_info['CurriculumInfo']['id'])); ?>" >แก้ไขข้อมูล</a></td>
                <td style="text-align:center;"><a class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')" href="<?php echo $this->Html->url(array('action'=>'remove_curriculum_info',$curriculum_info['CurriculumInfo']['id'])); ?>">ลบข้อมูล</a></td>
                <?php } ?>
            </tr>
                <?php } ?>
                <tr>
                <td colspan="20">ไตรมาส4</td>
            </tr>
            <tr>
                <?php
                    $i = 0;
                    foreach ($curriculum_infos4 as $curriculum_info){$i++;
                ?>
                <td style="text-align:center;"><?php echo $i; ?></td>
                <td style="text-align:center;"><?php echo $curriculum_info['CurriculumInfo']['title_th'] ?></td>
                <td style="text-align:center;"><?php echo $curriculum_info['CurriculumInfo']['title_en'] ?></td>
                <td style="text-align:center;"><?php echo $curriculum_info['CurriculumInfo']['degree_id'] ?></td>
                <td style="text-align:center;"><a class="btn-info btn" href="<?php echo $curriculum_info['CurriculumInfo']['link_th'] ?>" title="กดเพื่อเข้าดูข้อมูล">ลิ้งค์เข้าดูรายละเอียดเพิ่มเติม</a></td>
                <td style="text-align:center;"><?php echo $curriculum_info['CurriculumInfo']['month'] ?></td>
                <td style="text-align:center;"><?php echo $curriculum_info['CurriculumInfo']['year'] ?></td>
                <?php if($admins != null) { ?>
                <td style="text-align:center;"><a class="btn btn-warning" role="button" target="_blank" href="<?php echo $this->Html->url(array('action' => 'edit_curriculum_info',$curriculum_info['CurriculumInfo']['id'])); ?>" >แก้ไขข้อมูล</a></td>
                <td style="text-align:center;"><a class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')" href="<?php echo $this->Html->url(array('action'=>'remove_curriculum_info',$curriculum_info['CurriculumInfo']['id'])); ?>">ลบข้อมูล</a></td>
                <?php } ?>
            </tr>
                <?php } ?>
        </tbody>
    </table>
</div>