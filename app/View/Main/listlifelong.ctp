<?php
function DateThai($strDate)
{

	$strYear = date("Y", strtotime($strDate)) + 543;
	$strMonth = date("n", strtotime($strDate));
	$strDay = date("j", strtotime($strDate));
	$strHour = date("H", strtotime($strDate));
	$strMinute = date("i", strtotime($strDate));
	$strSeconds = date("s", strtotime($strDate));
	$strMonthCut = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
	$strMonthThai = $strMonthCut[$strMonth];
	return "$strDay $strMonthThai $strYear";
	//, $strHour:$strMinute
}
?>





<?php
date_default_timezone_set('Asia/Bangkok');
$date = date("Y-m-d");
$time = date("H:i:s");


function DateDiff($strDate1, $strDate2)
{
	return (strtotime($strDate2) - strtotime($strDate1)) /  (60 * 60 * 24);  // 1 day = 60*60*24
}

function TimeDiff($strTime1, $strTime2)
{
	return (strtotime($strTime2) - strtotime($strTime1)) /  (60 * 60); // 1 Hour =  60*60                           
}



?>
<div class="container-fluid" style="padding:15px 0 0;">
	<div class="panel panel-default" style="border-color: #F2F2F2!important">
		<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<div id="no-more-tables">
				<br>
				<center>
					<h3>หลักสูตร Life Long Learning (ตุลาคม2566 - มิถุนายน 2567)</h3>
				</center>
				<br>
				<div id="no-more-tables">
					<table class="table-responsive-full sort-table">
						<thead>
							<tr>
								<th style="text-align: center;" width="1%"><font size="3px">ลำดับ</font></th>
								<th style="text-align: center;" width="40%"><font size="3px">ชื่อหลักสูตร</font></th>
								<th style="text-align: center;" width="10%"><font size="3px">จำนวนผู้เรียนในหลักสูตร</font></th>
								<th style="text-align: center;" width="5%"><font size="3px">ประเภท</font></th>
								<th style="text-align: center;" width="5%"><font size="3px">รหัส</font></th>
								<th style="text-align: center;" width="5$"><font size="3px"></font>เดือน</th>
								<th style="text-align: center;" width="5$"><font size="3px"></font>ปี</th>
								<th style="text-align: center;" width="30%"><font size="3px">เว็บไซต์</font></th>
							</tr>
						</thead>
						<tbody>
							<?php
							$i = 0;
								foreach ($listlifelongs as $listlifelong){ $i++;
								?>
								<tr>
									<td style="text-align: center;"><?php echo $listlifelong['LifeLongInfo']['id'];?></td>
									<td style="text-align: center;"><?php echo $listlifelong['LifeLongInfo']['subject_name'];?></td>
									<td style="text-align: center;"><?php echo $listlifelong['LifeLongInfo']['count_student'];?></td>
									<td style="text-align: center;"><?php echo $listlifelong['LifeLongInfo']['type'];?></td>
									<td style="text-align: center;"><?php echo $listlifelong['LifeLongInfo']['code'];?></td>
									<td style="text-align: center;"><?php echo $listlifelong['LifeLongInfo']['month'];?></td>
									<td style="text-align: center;"><?php echo $listlifelong['LifeLongInfo']['year'];?></td>
									<td style="text-align: center;">
										<a href="<?php echo $listlifelong['LifeLongInfo']['website'];?>">
										<?php echo $listlifelong['LifeLongInfo']['website'];?></a>
									</td>
								</tr>
							<?php } ?>
						</tbody>
					</table>
		
		<div class="row">
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-info">
						ติดต่อสอบถาม : ฝ่ายกิจกรรมนักเรียน นักศึกษา โทรศัพท์ : 053-944641-4 ต่อ 117 (คุณสมโภชน์ อจิยจักร์) ในวันและเวลาราชการ
					</div>
				</div>
			</div>
		</div>

</div>

<form method="post" id="myForm">
<!-- <div class="g-recaptcha" data-sitekey = "keywebsite" -->
	<div class="g-recaptcha" data-sitekey="6LdFnBMqAAAAAIe15RPCFpfdaOgkvsGFZUM9VQ_y"></div>
    <button type="submit">ยืนยัน</button>
</form>

<!-- script for work -->
<script src="https://www.google.com/recaptcha/api.js" async defer></script>

<!-- insert this into database  -->
	<?php
		$secret= "6LdFnBMqAAAAAL3la12qnokvISo8it_983G2syay";
		if (isset($_POST['g-recaptcha-response']))
		{
			$captcha = $_POST['g-recaptcha-response'];
			$verify = file_get_contents('https://google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$captcha);
			$captcha_success = json_decode($verify);

// recheck if captcha work or not
		if (!$captcha)
			{
					echo 'คุณไม่ได้ป้อน reCAPTCHAอย่างถูกต้อง';
			}

		if ($captcha_success->success) 
			{
        		header('location: https://www.google.com');
				exit;
    		}
		}
	?>