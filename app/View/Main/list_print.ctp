<script >
window.print();
</script>
<?php 
      	//<script>window.jQuery || document.write('<script src="/assets/bootstrap/assets/js/vendor/jquery.min.js"><\/script>')</script>
      	echo $this->Html->script('/assets/bootstrap/assets/js/vendor/jquery.min'); 
    	?>
		<!-- Bootstrap core CSS -->
		<?php echo $this->Html->css('/assets/bootstrap/dist/css/bootstrap.min'); ?>

		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<?php echo $this->Html->css('/assets/bootstrap/assets/css/ie10-viewport-bug-workaround'); ?>

		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><?php echo $this->Html->script('/assets/bootstrap/assets/js/ie8-responsive-file-warning'); ?><![endif]-->
		<?php echo $this->Html->script('/assets/bootstrap/assets/js/ie-emulation-modes-warning'); ?>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<?php echo $this->Html->script('https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js'); ?>
			<?php echo $this->Html->script('https://oss.maxcdn.com/respond/1.4.2/respond.min.js'); ?>
		<![endif]-->
		<?php 
			//<script>window.jQuery || document.write('<script src="/assets/bootstrap/assets/js/vendor/jquery.min.js"><\/script>')</script>
			echo $this->Html->script('/assets/bootstrap/assets/js/vendor/jquery.min'); 
		?>
		
		<?php echo $this->Html->script('/assets/bootstrap/dist/js/bootstrap.min'); ?>
		<style type="text/css">
			@font-face {  
			font-family: baijam ;  
			src: url(<?php echo $this->webroot.'files/fonts/th_baijam_0-webfont.ttf'; ?>) format("truetype");  
			}
        .txt-content{
            font-family: baijam , verdana, helvetica, sans-serif; 
            /* font-family: 'TH Niramit AS'; */
            font-size: 18px;
        }
        .txt-content-indent{
            text-indent: 1cm;
            font-family: baijam , verdana, helvetica, sans-serif; 
            /* font-family: 'TH Niramit AS'; */
            font-size: 30px;
        }
        .content-indent{
            margin-left: 1cm;
            font-family: baijam , verdana, helvetica, sans-serif; 
            /* font-family: 'TH Niramit AS'; */
            font-size: 21px;
        }
    </style>
<div class="container" style="padding:15px 0 0;">

<div class="panel">
		
		<div class="panel-body" style="padding-top: 15px; ">

			<div align="center"> 	
			<center>
			<!-- <img class="img-responsive" alt="" src="<?php echo $this->Html->url('/img/vector1.png'); ?>" width="30%" /> -->
			<img class="img-responsive" alt="" src="http://www.agri.cmu.ac.th/kasetfairstudent/img/logo.jpg"  style="height: 4.6cm" />
		
			<div class="txt-content"><b>รายชื่อผู้ลงทะเบียน</b></div></center>
			<div class="txt-content"><b><?php echo $project['Project']['name']; ?></b></div></center>			
				
				
				<div class="txt-content"><b>ณ <?php echo $project['Project']['place']; ?><br> วันที่ <?php echo $project['Project']['date']; ?> เวลา <?php echo $project['Project']['time']; ?></b></div></center>	
			
				
			</div> 
			
			<br>
			<div class="row"> 	
				 <div class="col-md-12">
			
				 <div id="no-more-tables"> 				
				<table class="table table-bordered table-striped txt-content">
					<thead>
						<tr>
							<th style="text-align: center;">ลำดับ</th>
							 
							<th style="text-align: center;">ชื่อ - นามสกุล</th>
							<th style="text-align: center;" width="5%">ชื่อโรงเรียน</th>
							<th style="text-align: center;">ระดับ</th>
							<th style="text-align: center;">อาจารย์ผู้ควบคุม</th>
							<th style="text-align: center;">โทรศัพท์</th>
							<th style="text-align: center;" width="15%">ลายเซ็นต์</th>
							 
						</tr>
					</thead>
				 
					<tbody>

						<?php 
							$i=0;
							foreach($schools as $school){ 
						?>
							<tr>
								<td data-title="กลุ่มโรงเรียน" colspan="7">
									<b><?php echo $school['Member']['school']; ?></b>
								</td>
								 
							</tr>
							<?php
								foreach($students as $student){
									if($school['Member']['school'] == $student['Member']['school']){ 
									$i++
							?>
								<tr>
									<td data-title="ลำดับ" align="center"><?php echo $i; ?></td>
									 
									 
									<td data-title="ชื่อ - นามสกุล">
										<?php 
											echo $student['Prefix']['name'].$student['Member']['fname'].' '.$student['Member']['lname']; 
										?>
										<?php 
											if(isset($citizenId) || isset($admin)){
												echo $student['Member']['phone']; 

											}
										?>
									</td>
									<td data-title="ชื่อโรงเรียน">
										<?php 
											echo $student['Member']['school']; 
										?>
										
									</td>
									<td data-title="ระดับ">
										<?php 
											echo $student['Member']['level']; 
										?>
										
									</td>
									<td data-title="อาจารย์ผู้ควบคุม">
										<?php 
											echo $student['Member']['teacher']; 
										?>
										<?php 
											if(isset($citizenId) || isset($admin)){
												echo $student['Member']['phoneteacher']; 

											}
										?>
									</td>
									<td>
										<?php  if($UserName != null) { ?>
											<?php 
												echo $student['Member']['phoneteacher']; 
											?>  
										<?php } ?>
									</td>
									<td>
										 
									</td>
									 
									
									
								</tr>
						<?php 		
									}
								}
							} 
						?>
					</tbody>
				</table>
			</div>				
	
		</div>
		</div> 	
		</div>
		</div> 
		</div> 
	
	</div>
	
</div>

