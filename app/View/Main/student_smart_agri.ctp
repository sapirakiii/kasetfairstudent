<div class="container">
    <div class="panel-header" align="center">
        <h3>ระดับความพึงพอใจของผู้ใช้บัณฑิตต่อความรู้/ทักษะด้าน Smart Agriculture ( นิยามตามวิสัยทัศน์ของคณะฯ : สร้างและถ่ายทอดนวัตกรรมการเกษตร มุ่งสู่การพัฒนาอย่างยั่งยืน)</h3>
    </div>
</div>
<div class="panel-body">
<br>


    <div class="btn-group">
        <?php if($admins != null) { ?>
        <a class="btn btn-success" role="button" target="_blank" href="<?php echo $this->Html->url(array('action'=>'add_student_smart_agri')); ?>">เพิ่มข้อมูล</a>
        <?php } ?>
    </div>
    <div class="btn-group">
        <form method="get">
            <div class="dropdown">
                <button class="btn btn-info btn-default dropdown-toggle" type="button" id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aira-expanded="false">
                    <?php echo $years['Yearterm']['year'];?>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu">
                    <?php foreach ($dropdowns as $dropdown) { ?>
                        <?php
                            if ($years['Yearterm']['year'] == $dropdown['Yearterm']['year'])
                            {
                                $active = 'active';
                            }
                            else
                            {
                                $active = '';
                            }
                        ?>
                        <li class="<?php echo $active;?>">
                            <a href="<?php echo $this->Html->url(array('action' => 'student_smart_agri',$dropdown['Yearterm']['year']));?>">
                                <font color="black"><?php echo $dropdown['Yearterm']['year'];?></font>
                            </a>
                        </li>
                        <?php } ?>
                </ul>
            </div>
        </form>
    </div>
    <table class="table table-bordered">
        <thead>
            <th style="text-align:center;" colspan="" rowspan="" width="1%">ลำดับ</th>
            <th style="text-align:center;" colspan="" rowspan="" width="40%">หัวข้อ</th>
            <th style="text-align:center;" colspan="" rowspan="" width="10%">เดือน</th>
            <th style="text-align:center;" colspan="" rowspan="" width="10%">ปี</th>
            <th style="text-align:center;" colspan="" rowspan="" width="10%">ระดับความพึงพอใจ</th>
            <?php if($admins != null) { ?>
            <th style="text-align:center;" colspan="" rowspan="" width="1%">แก้ไขข้อมูล</th>
            <th style="text-align:center;" colspan="" rowspan="" width="1%">ลบข้อมูล</th>
            <?php } ?>
        </thead>
        <tbody>
            <tr>
                <td style="text-align:left;" colspan="100" rowspan="" width="1%">ไตรมาส1</td>
            </tr>
            <?PHP
                $i = 0;
                foreach ($student_smart_agri1 as $student_smart_agri) { $i++
            ?>
            <tr>
                <td style="text-align:center;"><?php echo $i; ?></td>
                <td style="text-align:center;"><?php echo $student_smart_agri['StudentSmartAgri']['name']; ?></td>
                <td style="text-align:center;"><?php echo $student_smart_agri['StudentSmartAgri']['month']; ?></td>
                <td style="text-align:center;"><?php echo $student_smart_agri['StudentSmartAgri']['year']; ?></td>
                <td style="text-align:center;"><?php echo $student_smart_agri['StudentSmartAgri']['average_point']; ?></td>
                <?php if($admins != null) { ?>
                <td style="text-align:center;"><a class="btn btn-warning" target="_blank" role="button" 
                    href="<?php echo $this->Html->url(array('action'=>'edit_student_smart_agri',$student_smart_agri['StudentSmartAgri']['id'])); ?>">แก้ไขข้อมูล</a>
                </td>
                <td style="text-align:center;"><a class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')" 
                    href="<?php echo $this->Html->url(array('action' => 'remove_student_smart_agri',$student_smart_agri['StudentSmartAgri']['id'])); ?>">ลบข้อมูล</a>
                </td>
                <?php } ?>
            </tr>
            <?php } ?>
            <tr>
                <td style="text-align:left;" colspan="100" rowspan="" width="1%">ไตรมาส2</td>
            </tr>
            <?PHP
                foreach ($student_smart_agri2 as $student_smart_agri) { $i++
            ?>
            <tr>
                <td style="text-align:center;"><?php echo $i; ?></td>
                <td style="text-align:center;"><?php echo $student_smart_agri['StudentSmartAgri']['name']; ?></td>
                <td style="text-align:center;"><?php echo $student_smart_agri['StudentSmartAgri']['month']; ?></td>
                <td style="text-align:center;"><?php echo $student_smart_agri['StudentSmartAgri']['year']; ?></td>
                <td style="text-align:center;"><?php echo $student_smart_agri['StudentSmartAgri']['average_point']; ?></td>
                <?php if($admins != null) { ?>
                <td style="text-align:center;"><a class="btn btn-warning" target="_blank" role="button" 
                    href="<?php echo $this->Html->url(array('action'=>'edit_student_smart_agri',$student_smart_agri['StudentSmartAgri']['id'])); ?>">แก้ไขข้อมูล</a>
                </td>
                <td style="text-align:center;"><a class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')" 
                    href="<?php echo $this->Html->url(array('action' => 'remove_student_smart_agri',$student_smart_agri['StudentSmartAgri']['id'])); ?>">ลบข้อมูล</a>
                </td>
                <?php } ?>
            </tr>
            <?php } ?>
            <tr>
                <td style="text-align:left;" colspan="100" rowspan="" width="1%">ไตรมาส3</td>
            </tr>
            <?PHP
                foreach ($student_smart_agri3 as $student_smart_agri) { $i++
            ?>
            <tr>
                <td style="text-align:center;"><?php echo $i; ?></td>
                <td style="text-align:center;"><?php echo $student_smart_agri['StudentSmartAgri']['name']; ?></td>
                <td style="text-align:center;"><?php echo $student_smart_agri['StudentSmartAgri']['month']; ?></td>
                <td style="text-align:center;"><?php echo $student_smart_agri['StudentSmartAgri']['year']; ?></td>
                <td style="text-align:center;"><?php echo $student_smart_agri['StudentSmartAgri']['average_point']; ?></td>
                <?php if($admins != null) { ?>
                <td style="text-align:center;"><a class="btn btn-warning" target="_blank" role="button" 
                    href="<?php echo $this->Html->url(array('action'=>'edit_student_smart_agri',$student_smart_agri['StudentSmartAgri']['id'])); ?>">แก้ไขข้อมูล</a>
                </td>
                <td style="text-align:center;"><a class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')" 
                    href="<?php echo $this->Html->url(array('action' => 'remove_student_smart_agri',$student_smart_agri['StudentSmartAgri']['id'])); ?>">ลบข้อมูล</a>
                </td>
                <?php } ?>
            </tr>
            <?php } ?>
            <tr>
                <td style="text-align:left;" colspan="100" rowspan="" width="1%">ไตรมาส4</td>
            </tr>
            <?PHP
                foreach ($student_smart_agri4 as $student_smart_agri) { $i++
            ?>
            <tr>
                <td style="text-align:center;"><?php echo $i; ?></td>
                <td style="text-align:center;"><?php echo $student_smart_agri['StudentSmartAgri']['name']; ?></td>
                <td style="text-align:center;"><?php echo $student_smart_agri['StudentSmartAgri']['month']; ?></td>
                <td style="text-align:center;"><?php echo $student_smart_agri['StudentSmartAgri']['year']; ?></td>
                <td style="text-align:center;"><?php echo $student_smart_agri['StudentSmartAgri']['average_point']; ?></td>
                <?php if($admins != null) { ?>
                <td style="text-align:center;"><a class="btn btn-warning" target="_blank" role="button" 
                    href="<?php echo $this->Html->url(array('action'=>'edit_student_smart_agri',$student_smart_agri['StudentSmartAgri']['id'])); ?>">แก้ไขข้อมูล</a>
                </td>
                <td style="text-align:center;"><a class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')" 
                    href="<?php echo $this->Html->url(array('action' => 'remove_student_smart_agri',$student_smart_agri['StudentSmartAgri']['id'])); ?>">ลบข้อมูล</a>
                </td>
                <?php } ?>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>