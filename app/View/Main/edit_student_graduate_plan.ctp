<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="glyphicon glyphicon-plus"></i> แก้ไขข้อมูล</h3> 
            </div>
            <div class="panel-body">
                <form method="post" accept-charset="utf-8">
                    <?php
                        echo $this->Form->input('StudentGraduatePlan.id',array(
                            'type' => 'hidden',
                            'label' => false,
                            'hidden',
                        ));
                    ?>
                    <label class="col-sm-4 control-label">กลุ่มรหัสนักศึกษา</label>
                    <div class="col-sm-8">
                        <div class="form-group has-feedback">
                            <?php
                                echo $this->Form->input('StudentGraduatePlan.name',array(
                                    'type' => 'text',
                                    'label' => false,
                                    'div' => false,
                                    'class' => array('form-control css-require'),
                                    'error' => false,
                                    'placeholder' => 'กรุณากรอกกลุ่มรหัสนักศึกษา',
                                    'required',
                                ));
                            ?>
                        </div>
                    </div>
                    <label class="col-sm-4 control-label">จำนวนนักศึกษาที่สมัคร</label>
                    <div class="col-sm-8">
                        <div class="form-group has-feedback">
                            <?php
                                echo $this->Form->input('StudentGraduatePlan.count_in',array(
                                    'type' => 'number',
                                    'label' => false,
                                    'div' => false,
                                    'class' => array('form-control css-require'),
                                    'error' => false,
                                    'placeholder' => 'จำนวนนักศึกษาที่สมัคร',
                                    'required',
                                ));
                            ?>
                        </div>
                    </div>
                    <label class="col-sm-4 control-label">จำนวนนักศึกษาที่จบ</label>
                    <div class="col-sm-8">
                        <div class="form-group has-feedback">
                            <?php
                                echo $this->Form->input('StudentGraduatePlan.count_grad',array(
                                    'type' => 'number',
                                    'label' => false,
                                    'div' => false,
                                    'class' => array('form-control css-require'),
                                    'error' => false,
                                    'placeholder' => 'จำนวนนักศึกษาที่จบ',
                                    'required',
                                ));
                            ?>
                        </div>
                    </div>
                    <label class="col-sm-4 control-label">จำนวนนักศึกษาที่ไม่จบ</label>
                    <div class="col-sm-8">
                        <div class="form-group has-feedback">
                            <?php
                                echo $this->Form->input('StudentGraduatePlan.count_not_grad',array(
                                    'type' => 'number',
                                    'label' => false,
                                    'div' => false,
                                    'class' => array('form-control css-require'),
                                    'error' => false,
                                    'placeholder' => 'จำนวนนักศึกษาที่ไม่จบ',
                                    'required',
                                ));
                            ?>
                        </div>
                    </div>
                    <label class="col-sm-4 control-label">เดือน</label>
                    <div class="col-sm-8">
                        <div class="form-group has-feedback">
                            <?php
                                $month = array(
                                    '1' => '1',
                                    '2' => '2',
                                    '3' => '3',
                                    '4' => '4',
                                    '5' => '5',
                                    '6' => '6',
                                    '7' => '7',
                                    '8' => '8',
                                    '9' => '9',
                                    '10' => '10',
                                    '11' => '11',
                                    '12' => '12',
                                );
                                echo $this->Form->input('StudentGraduatePlan.month', array(
                                    'options' => $month,
                                    'label' => false,
                                    'error' => false,
                                    'div' => false,
                                    'class' => array('form-control css-require'),
                                    'required',
                                ));
                            ?>
                        </div>
                    </div>
                    <label class="col-sm-4 control-label">ปี</label>
                    <div class="col-sm-8">
                        <div class="form-group has-feedback">
                            <?php
                                echo $this->Form->input('StudentGraduatePlan.year',array(
                                    'type' => 'number',
                                    'label' => false,
                                    'div' => false,
                                    'class' => array('form-control css-require'),
                                    'error' => false,
                                    'placeholder' => '2567',
                                    'required',
                                ));
                            ?>
                        </div>
                    </div>
                    <input type="submit" class="btn btn-success" value="บันทึกข้อมูล" onclick="return confirm('ยืนยันการเพิ่มข้อมูล')">
                </form>
            </div> <!-- class="panel-body" -->
            </div>
        </div>
    </div>
</div>