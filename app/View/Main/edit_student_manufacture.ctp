<div class="container" style="padding:0 15px 0;">	
	<div class="row">
		<div class="col-md-12">
                  <div class="well">

                        <tbody>
                              <form method="post" accept-charset="utf-8">
                                    <div class="panel panel-success">
                                          <div class="panel-heading"><br>
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-plus"></i>แก้ไขข้อมูล</h3>
                                          </div>
                                          <?php
                                                echo $this->Form->input('StudentManufacture.id',array(
                                                      'hidden',
                                                ));
                                          ?>
                                          <div class="panel-body">
                                                <label class="col-sm-4 control-label">หัวข้อ</label>
                                                <div class="col-sm-8">
                                                      <?php
                                                            echo $this->Form->input('StudentManufacture.name',array(
                                                                  'type' => 'text',
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'placeholder' => 'กรุณากรอกหัวข้อ',
                                                                  'required',
                                                            ));
                                                      ?>
                                                </div>
                                                <label class="col-sm-4 control-label">เดือน</label>
                                                <div class="col-sm-8">
                                                      <?php
                                                            $month = array(
                                                                  '1' => '1',
                                                                  '2' => '2',
                                                                  '3' => '3',
                                                                  '4' => '4',
                                                                  '5' => '5',
                                                                  '6' => '6',
                                                                  '7' => '7',
                                                                  '8' => '8',
                                                                  '9' => '9',
                                                                  '10' => '10',
                                                                  '11' => '11',
                                                                  '12' => '12',
                                                            );
                                                            echo $this->Form->input('StudentManufacture.month',array(
                                                                  'options' => $month,
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'required'
                                                            ));
                                                      ?>
                                                </div>
                                                <label class="col-sm-4 control-label">ปี</label>
                                                <div class="col-sm-8">
                                                      <?php
                                                            $year = array(
                                                                  '2569' => '2569',
                                                                  '2568' => '2568',
                                                                  '2567' => '2567',
                                                                  '2566' => '2566',
                                                                  '2565' => '2565',
                                                                  '2564' => '2564',
                                                                  '2563' => '2563',
                                                                  '2562' => '2562',
                                                                  '2561' => '2561',
                                                                  '2560' => '2560',
                                                                  '2559' => '2559',
                                                                  '2558' => '2558',
                                                                  '2557' => '2557',
                                                            );
                                                            echo $this->Form->input('StudentManufacture.year',array(
                                                                  'options' => $year,
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'required'
                                                            ));
                                                      ?>
                                                </div>
                                                <label class="col-sm-4 control-label">จำนวนนักศึกษา</label>
                                                <div class="col-sm-8">
                                                      <?php
                                                            echo $this->Form->input('StudentManufacture.total_stu',array(
                                                                  'type' => 'text',
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'required',
                                                                  'placeholder' => 'กรูณากรอกจำนวนนักศึกษา',
                                                            ));
                                                      ?>
                                                </div>
                                                <label class="col-sm-4 control-label">จำนวนนักศึกษาที่จบ</label>
                                                <div class="col-sm-8">
                                                      <?php
                                                            echo $this->Form->input('StudentManufacture.graduate_stu',array(
                                                                  'type' => 'text',
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'required',
                                                                  'placeholder' => 'กรูณากรอกจำนวนนักศึกษา',
                                                            ));
                                                      ?>
                                                </div>
                                          </div>
                                    </div>
                                    <br>
                                    <div>
                                          <input type="submit" class="btn btn-success" value="บันทึกข้อมูล" onclick="return confirm('กรุณายืนยันการบันทึกข้อมูล')"> 
                                    </div>
                              </form>
                        </tbody>

                  </div>
            </div>
      </div>
</div>
<div style="clear: both;"></div>