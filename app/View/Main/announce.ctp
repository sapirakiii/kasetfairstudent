<?php 
		date_default_timezone_set('Asia/Bangkok');
		$date = date("Y-m-d");
		$time = date("H:i:s");
	

		function DateDiff($strDate1,$strDate2){                             
		return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
		}

		function TimeDiff($strTime1,$strTime2) {                             
            return (strtotime($strTime2) - strtotime($strTime1))/  ( 60 * 60 ); // 1 Hour = 60*60
        }

	?>
	
<div class="container" style="padding:15px 0 0;">
	<table class="table table-striped table-bordered">
		<tr>
			<td colspan="7" class="warning">
			รายการ  <h4><?php echo $projects[0]['Project']['name']; ?></h4>
				<form name="frmselect" method="get"> 	
					<div class="dropdown"  style="float: left;">
						<button class="btn btn-default dropdown-toggle btn-info" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						<?php echo $projects[0]['Project']['name']; ?>
						<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
						<?php foreach ($dropdowns as $dropdown) { ?>
							<?php 
							if($projects[0]['Project']['id'] == $dropdown['Project']['id']){
								$active = "active";
							}else{
								$active = "";
							}
							// $yearList = $dropdown['Yearterm']['year'];
							?>
							<li class="<?php echo $active;?>">
								<a href="<?php echo $this->Html->url(array('action' => 'announce',$dropdown['Project']['id'])); ?>">
									<font color="black"><?php echo $dropdown['Project']['name']; ?></font>
								</a>
							</li>
						<?php } ?>
						</ul>
					</div>
				
				</form>
			</td>
			<td class="warning">
			
			</td>				
		</tr>
	</table> 
	<div class="panel panel-default" style="border-color: #F2F2F2!important">
		<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center><div class="font_panel2">ประกาศรายชื่อโรงเรียนที่เข้าแข่งขัน</div></center>
				</h3> 
			</div>
		</div>	
		<div class="panel-body" style="padding-top: 15px; ">
			<div align="center"> 	
				 
				
				<h3><?php echo $project['Project']['name']; ?></h3>
				<h3>วัน เวลาที่แข่งขัน : <?php echo $project['Project']['date']; ?><br><?php echo $project['Project']['time']; ?>
					 
				</h3>
				<h3>สถานที่แข่งขัน :
					<a href="<?php echo $project['Project']['link']; ?>" target="_blank">
						<span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>  <?php echo $project['Project']['place']; ?>
					</a>
				</h3>
			</div> 
			
			
			 
			<div id="no-more-tables"> 				
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th style="text-align: center;">ลำดับ</th>
							<th style="text-align: center;">โรงเรียน</th>
						</tr>
					</thead>
				
					<tbody>

						<?php 
							$i=0;
							foreach($schools as $school){ 
								$i++;
						?>
							<tr>
								<td data-title="ลำดับ" align="center"><?php echo $i; ?></td>
								<td data-title="โรงเรียน">
									<b><?php echo $school['Member']['school']; ?></b>
								</td>
								 
							</tr>
							 
						<?php 		
								 
							} 
						?>
					</tbody>
				</table>
			</div>			
	
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
			 
		</div>
		<div class="col-md-6">	
			
			<div class="alert alert-info" style="justify-content: center;">
				ติดต่อสอบถามเพิ่มเติมได้ที่ งานบริการการศึกษา และพัฒนาคุณภาพนักศึกษา
				<br>
				คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่ โทรศัพท์ 053 - 944641-2, 053 - 944644 โทรสาร 053 - 944666  (ในวันเวลาราชการ)
			</div>
			
		</div>
	</div>
	
</div>
