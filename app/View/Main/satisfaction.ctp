<?php
function DateThai($strDate)
{

	$strYear = date("Y", strtotime($strDate)) + 543;
	$strMonth = date("n", strtotime($strDate));
	$strDay = date("j", strtotime($strDate));
	$strHour = date("H", strtotime($strDate));
	$strMinute = date("i", strtotime($strDate));
	$strSeconds = date("s", strtotime($strDate));
	$strMonthCut = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
	$strMonthThai = $strMonthCut[$strMonth];
	return "$strDay $strMonthThai $strYear";
	//, $strHour:$strMinute
}
?>





<?php
date_default_timezone_set('Asia/Bangkok');
$date = date("Y-m-d");
$time = date("H:i:s");


function DateDiff($strDate1, $strDate2)
{
	return (strtotime($strDate2) - strtotime($strDate1)) /  (60 * 60 * 24);  // 1 day = 60*60*24
}

function TimeDiff($strTime1, $strTime2)
{
	return (strtotime($strTime2) - strtotime($strTime1)) /  (60 * 60); // 1 Hour =  60*60                           
}




?>
<div class="container-fluid" style="padding:15px 0 0;">
	<div class="panel panel-default" style="border-color: #F2F2F2!important">
		<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center>
						<div class="font_panel2">ความพึงพอใจของผู้เรียนต่อหลักสูตรฯของนักศึกษาปริญญาตรีชั้นปีที่ 4 (ปีการศึกษา <?php echo $lastyearterms['Yearterm']['year'] ?>)</div>
					</center>
				</h3>
			</div>
		</div>
		<?php if($admins != null){ ?>
		<a class="btn btn-success" href="<?php echo $this->Html->url(array('action' => 'add_satisfaction')); ?>" role="button" target="_blank">เพิ่มข้อมูล</a>
		<?php } ?>
		<br>
		กรุณาเลือกปีการศึกษา
		<form method="get" name="frmselect">
			<div class="dropdown" style="float: left;">
				<button class="btn btn-default dropdown-toggle btn-info" type="button" id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					<?php echo $lastyearterms['Yearterm']['year'] ?>
				</button>
				<ul class="dropdown-menu" aria-labelledby="dropdownMenu">
					<?php foreach ($dropdowns as $dropdown) { ?>
						<?php
							if($years['Yearterm']['year'] == $dropdown['Yearterm']['year']){
								$active = "active";
							}else{
								$active = "";
							}
						?>
						<li class="<?php echo $active; ?>">
							<a href="<?php echo $this->Html->url(array('action'=>'satisfaction',$dropdown['Yearterm']['year']));?>">
								<font color="black"><?php echo $dropdown['Yearterm']['year']; ?></font>
							</a>
						</li>
					<?php } ?>
				</ul>
			</div>
		</form><br><br>
		<div id="no-more-tables">
			<table class="table table-bordered">
				<tr>
					<th style="text-align: center;" width="5%"><font size="3px">ลำดับ</font></th>
					<th style="text-align: center;" width="20%"><font size="3px">แบบสำรวจ</font></th>
					<th style="text-align: center;" width="5%"><font size="3px">ค่าเฉลี่ย</font></th>
					<th style="text-align: center;" width="5%"><font size="3px">เดือนที่เก็บข้อมูล</font></th>
					<th style="text-align: center;" width="5%"><font size="3px">ปีที่เก็บข้อมูล</font></th>
					<th style="text-align: center;" width="10%"><font size="3px">เว็บไซต์</font></th>
					<?php if($admins != null){ ?>
					<th style="text-align: center;" width="1%"><font size="3px">แก้ไขข้อมูล</font></th>
					<th style="text-align: center;" width="1%"><font size="3px">ลบข้อมูล</font></th>
					<?php } ?>
				</tr>
				<tr>
					<td colspan="8">ไตรมาส 1</td>
				</tr>
				<tr>
					<?php 
					$i = 0;
						foreach ($satisfactions1 as $satisfaction){ $i++;?>
							<td style="text-align: center;"><?php echo $satisfaction['Satisfaction']['id'] ?></td>
							<td style="text-align: center;"><?php echo $satisfaction['Satisfaction']['name'] ?></td>
							<td style="text-align: center;"><?php echo $satisfaction['Satisfaction']['point'] ?></td>
								<td style="text-align: center;"><?php echo $satisfaction['Satisfaction']['month'] ?></td>
							<td style="text-align: center;"><?php echo $satisfaction['Satisfaction']['year'] ?></td>
							<td style="text-align: center;">
								<a href="<?php echo $satisfaction['Satisfaction']['link']?>" class="btn btn-primary" target="_blank">
									<?php echo $satisfaction['Satisfaction']['link'] ?>
								</a>
							</td>
							<?php if($admins != null){ ?>
							<td>
								<a class="btn btn-warning"
								href="<?php echo $this->Html->url(array('action' => 'edit_satisfaction',$satisfaction['Satisfaction']['id'])); ?>" role="button" target="_blank">แก้ไขข้อมูล</a>
							</td>
							<td>
								<a class="btn btn-danger"
								href="<?php echo $this->Html->url(array('action' => 'remove_satisfaction', $satisfaction['Satisfaction']['id'])); ?>" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')">ลบข้อมูล</a>
							</td>
							<?php } ?>
					
				</tr>
				<?php } ?>
				<tr>
					<td colspan="8">ไตรมาส 2</td>
				</tr>
				<tr>
					<?php 
					$i = 0;
						foreach ($satisfactions2 as $satisfaction){ $i++;?>
							<td style="text-align: center;"><?php echo $satisfaction['Satisfaction']['id'] ?></td>
							<td style="text-align: center;"><?php echo $satisfaction['Satisfaction']['name'] ?></td>
							<td style="text-align: center;"><?php echo $satisfaction['Satisfaction']['point'] ?></td>
								<td style="text-align: center;"><?php echo $satisfaction['Satisfaction']['month'] ?></td>
							<td style="text-align: center;"><?php echo $satisfaction['Satisfaction']['year'] ?></td>
							<td style="text-align: center;">
								<a href="<?php echo $satisfaction['Satisfaction']['link']?>" class="btn btn-primary" target="_blank">
									<?php echo $satisfaction['Satisfaction']['link'] ?>
								</a>
							</td>
							<?php if($admins != null){ ?>
							<td>
								<a class="btn btn-warning"
								href="<?php echo $this->Html->url(array('action' => 'edit_satisfaction',$satisfaction['Satisfaction']['id'])); ?>" role="button" target="_blank">แก้ไขข้อมูล</a>
							</td>
							<td>
								<a class="btn btn-danger"
								href="<?php echo $this->Html->url(array('action' => 'remove_satisfaction', $satisfaction['Satisfaction']['id'])); ?>" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')">ลบข้อมูล</a>
							</td>
							<?php } ?>
				</tr>
				<?php } ?>
				<tr>
					<td colspan="8">ไตรมาส 3</td>
				</tr>
				<tr>
					<?php 
					$i = 0;
						foreach ($satisfactions3 as $satisfaction){ $i++;?>
							<td style="text-align: center;"><?php echo $satisfaction['Satisfaction']['id'] ?></td>
							<td style="text-align: center;"><?php echo $satisfaction['Satisfaction']['name'] ?></td>
							<td style="text-align: center;"><?php echo $satisfaction['Satisfaction']['point'] ?></td>
								<td style="text-align: center;"><?php echo $satisfaction['Satisfaction']['month'] ?></td>
							<td style="text-align: center;"><?php echo $satisfaction['Satisfaction']['year'] ?></td>
							<td style="text-align: center;">
								<a href="<?php echo $satisfaction['Satisfaction']['link']?>" class="btn btn-primary" target="_blank">
									<?php echo $satisfaction['Satisfaction']['link'] ?>
								</a>
							</td>
							<?php if($admins != null){ ?>
							<td>
								<a class="btn btn-warning"
								href="<?php echo $this->Html->url(array('action' => 'edit_satisfaction',$satisfaction['Satisfaction']['id'])); ?>" role="button" target="_blank">แก้ไขข้อมูล</a>
							</td>
							<td>
								<a class="btn btn-danger"
								href="<?php echo $this->Html->url(array('action' => 'remove_satisfaction', $satisfaction['Satisfaction']['id'])); ?>" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')">ลบข้อมูล</a>
							</td>
							<?php } ?>
				</tr>
				<?php } ?>
				<tr>
					<td colspan="8">ไตรมาส 4</td>
				</tr>
				<tr>
					<?php 
					$i = 0;
						foreach ($satisfactions4 as $satisfaction){ $i++;?>
							<td style="text-align: center;"><?php echo $satisfaction['Satisfaction']['id'] ?></td>
							<td style="text-align: center;"><?php echo $satisfaction['Satisfaction']['name'] ?></td>
							<td style="text-align: center;"><?php echo $satisfaction['Satisfaction']['point'] ?></td>
								<td style="text-align: center;"><?php echo $satisfaction['Satisfaction']['month'] ?></td>
							<td style="text-align: center;"><?php echo $satisfaction['Satisfaction']['year'] ?></td>
							<td style="text-align: center;">
								<a href="<?php echo $satisfaction['Satisfaction']['link']?>" class="btn btn-primary" target="_blank">
									<?php echo $satisfaction['Satisfaction']['link'] ?>
								</a>
							</td>
							<?php if($admins != null){ ?>
							<td>
								<a class="btn btn-warning"
								href="<?php echo $this->Html->url(array('action' => 'edit_satisfaction',$satisfaction['Satisfaction']['id'])); ?>" role="button" target="_blank">แก้ไขข้อมูล</a>
							</td>
							<td>
								<a class="btn btn-danger"
								href="<?php echo $this->Html->url(array('action' => 'remove_satisfaction', $satisfaction['Satisfaction']['id'])); ?>" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')">ลบข้อมูล</a>
							</td>
							<?php } ?>
				</tr>
				<?php } ?>
			</table>
			<div class="alert alert-info">
						KPI OWNER :ทิพย์วิมล ระพินทร์วงศ์
			</div>
		</div>
	</div>
		 
</div>