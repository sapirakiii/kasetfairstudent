<?php //echo debug($this->request->data)?>
<!-- start content here -->
<div class="container" style="padding:0 15px 0;">	
	<div class="row">
		<div class="col-md-12">
                  <div class="well">

                        <tbody>
                              <form method="post" accept-charset="utf-8">
                                    <div class="panel panel-success">
                                          <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-plus"></i> แก้ไขข้อมูล</h3> 
                                          </div>
                                          <div class="panel-body">
                                                <?php
                                                      echo $this->Form->input('StudentSmartAgri.id',array(
                                                            'hidden'
                                                      ));
                                                ?>
                                                <label class="col-sm-4 control-label">หัวข้อ</label>
                                                <div class="col-sm-8">
                                                      <?php
                                                            echo $this->Form->input('StudentSmartAgri.name',array(
                                                                  'type' => 'text',
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'placeholder' => 'หัวข้อ',
                                                                  'required'
                                                            ));
                                                      ?>
                                                </div>
                                                <label class="col-sm-4 control-label">เดือนที่เก็บข้อมูล</label>
                                                <div class="col-sm-8">
                                                      <?php
                                                            $month = array(
                                                                  '1' => '1',
                                                                  '2' => '2',
                                                                  '3' => '3',
                                                                  '4' => '4',
                                                                  '5' => '5',
                                                                  '6' => '6',
                                                                  '7' => '7',
                                                                  '8' => '8',
                                                                  '9' => '9',
                                                                  '10' => '10',
                                                                  '11' => '11',
                                                                  '12' => '12',
                                                            );
                                                            echo $this->Form->input('StudentSmartAgri.month',array(
                                                                  'options' => $month,
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'placeholder' => 'กรอกชื่อหลักสูตร',
                                                                  'required'
                                                            ));
                                                      ?>
                                                </div>
                                                <label class="col-sm-4 control-label">ปีที่เก็บข้อมูล</label>
                                                <div class="col-sm-8">
                                                      <?php
                                                            echo $this->Form->input('StudentSmartAgri.year',array(
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'placeholder' => '2567',
                                                                  'required'
                                                            ));
                                                      ?>
                                                </div>
                                                <label class="col-sm-4 control-label">ระดับความพึงพอใจ</label>
                                                <div class="col-sm-8">
                                                      <?php
                                                            echo $this->Form->input('StudentSmartAgri.average_point',array(
                                                                  'type' => 'text',
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'placeholder' => 'ระดับความพึงพอใจ',
                                                                  'required'
                                                            ));
                                                      ?>
                                                </div>
                                                <br>
                                                <div class="form-group">
                                                      <input role="button" class="btn btn-success" type="submit" value="บันทึกข้อมูล" onclick="return confirm('ยืนยันการเพิ่มข้อมูล')">
                                                </div>
                                          </div>
                                    </div>
                              </form>
                        </tbody>

                  </div>
            </div>
      </div>
</div>
<div style="clear: both;"></div>