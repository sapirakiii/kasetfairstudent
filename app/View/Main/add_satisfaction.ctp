<div class="container">
      <div class="row">
            <div class="col-md-12">
                        <form method="post" accept-charset="utf-8">
                        <div class="panel panel-success">
                              <div class="panel-heading"> 
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-plus"></i> เพิ่มข้อมูล</h3> 
                                          </div>
                              <?php
                                    echo $this->Form->input('Satisfaction.id',array(
                                          'type' => 'hidden',
                                          'label' => false,
                                          'hidden'
                                    ));
                              ?>
                              <div class="panel-body">
                              <label class="col-sm-4 control-label">แบบสำรวจ</label>
                              <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                                <?php
                                                      echo $this->Form->input('Satisfaction.name',array(
                                                            'type' => 'text',
                                                            'label' => false,
                                                            'div' => false,
                                                            'class' => array('form-control css-require'),
                                                            'error' => false,
                                                            'placeholder' => 'แบบสำรวจ',
                                                            'required'
                                                      ));
                                                ?>
                                          </div>
                              </div>
                              <label class="col-sm-4 control-label">ค่าเฉลี่ย</label>
                              <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                                <?php
                                                      echo $this->Form->input('Satisfaction.point',array(
                                                            'type' => 'text',
                                                            'label' => false,
                                                            'div' => false,
                                                            'class' => array('form-control css-require'),
                                                            'error' => false,
                                                            'placeholder' => 'ค่าเฉลี่ย',
                                                            'required'
                                                      ));
                                                ?>
                                          </div>
                              </div>
                              <label class="col-sm-4 control-label">เดือนที่เก็บข้อมูล</label>
                              <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                                <?php
                                                      echo $this->Form->input('Satisfaction.month',array(
                                                            'type' => 'text',
                                                            'label' => false,
                                                            'div' => false,
                                                            'class' => array('form-control css-require'),
                                                            'error' => false,
                                                            'placeholder' => 'เดือนที่เก็บข้อมูล',
                                                            'required'
                                                      ));
                                                ?>
                                          </div>
                              </div>
                              <label class="col-sm-4 control-label">ปีที่เก็บข้อมูล</label>
                              <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                                <?php
                                                      echo $this->Form->input('Satisfaction.year',array(
                                                            'type' => 'number',
                                                            'label' => false,
                                                            'div' => false,
                                                            'class' => array('form-control css-require'),
                                                            'error' => false,
                                                            'placeholder' => 'ปีที่เก็บข้อมูล',
                                                            'required'
                                                      ));
                                                ?>
                                          </div>
                              </div>
                              <label class="col-sm-4 control-label">เว็บไซต์</label>
                              <div class="col-sm-8">
                                    <div class="form-group has-feedback">
                                                <?php
                                                      echo $this->Form->input('Satisfaction.link',array(
                                                            'type' => 'text',
                                                            'label' => false,
                                                            'div' => false,
                                                            'class' => array('form-control css-require'),
                                                            'error' => false,
                                                            'placeholder' => 'เว็บไซต์',
                                                      ));
                                                ?>
                                    </div>
                              </div>
                              <input type="submit" class="btn btn-success" value="บันทึกข้อมูล" onclick="return confirm('ยืนยันการเพิ่มข้อมูล')">
                        </div>
                        </form>
            </div>
      </div>
</div>