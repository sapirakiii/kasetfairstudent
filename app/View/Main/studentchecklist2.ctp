<div class="container" style="padding:15px 0 0;">
	
	<div class="panel panel-default" style="border-color: #F2F2F2!important">
		<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center><div class="font_panel2">รายชื่อผู้ลงทะเบียน</div></center>
				</h3> 
			</div>
		</div>	
		<div class="panel-body" style="padding-top: 15px; ">
			<div class="row">
				<div class="col-md-4">
					<a class="btn btn-primary " href="<?php echo $this->Html->url(array('action' => 'studentchecklist2',1)); ?>" 
					role="button">
						1.ประกวดโครงงานวิทยาศาสตร์ฯ					
					</a>	
				</div>
				<div class="col-md-4">
					<a class="btn btn-primary " href="<?php echo $this->Html->url(array('action' => 'studentchecklist2',2)); ?>" 
					role="button">
					2.ประกวด/แสดงผลงานสิ่งประดิษฐ์นวัตกรรมเกษตร					
					</a>
				</div>
				<div class="col-md-4">
					<a class="btn btn-primary " href="<?php echo $this->Html->url(array('action' => 'studentchecklist2',3)); ?>" 
					role="button">
					3.ประกวดวาดภาพระบายสี				
					</a>
				</div>
			</div><br>
			<div class="row">
				<div class="col-md-4">
					<a class="btn btn-primary " href="<?php echo $this->Html->url(array('action' => 'studentchecklist2',4)); ?>" 
					role="button">
					4.ประกวดแข่งขันจัดตู้พรรณไม้น้ำ					
					</a>	
				</div>
				<div class="col-md-4">
					<a class="btn btn-primary " href="<?php echo $this->Html->url(array('action' => 'studentchecklist2',5)); ?>" 
					role="button">
					5.ประกวดการจัดสวนถาด (ประเภทถาดแห้ง)				
					</a>
				</div>
				<div class="col-md-4">
					<a class="btn btn-primary " href="<?php echo $this->Html->url(array('action' => 'studentchecklist2',6)); ?>" 
					role="button">
					6.การแข่งขันการตอบปัญหาทางด้านวิทยาศาสตร์เกษตร		
					</a>
				</div>
			</div>
			<div align="center"> 	
				<h4><?php echo $project['Type']['name']; ?></h4>
				
				<h3><?php echo $project['Project']['name']; ?></h3>
			</div> 
			
			
			<?php
			if(!isset($admin)){
			?>
			<div class="row">
				<div class="col-md-12">
					<?php
					if(isset($citizenId)){
					?>
						<a class="btn btn-warning pull-right" href="<?php echo $this->Html->url(array('action' => 'logout',$project['Project']['id'])); ?>" role="button">ออกจากระบบ</a>	
					<?php
					}
					else{
					?>
						<!-- <a class="btn btn-danger pull-right" href="<?php echo $this->Html->url(array('action' => 'login',$project['Project']['id'])); ?>" role="button"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> ยกเลิกการลงทะเบียน</a>	 -->
					<?php
					}
					?>
					
				</div>
			</div>
			<?php
			}
			?>
			
					
					
				
			
				<!-- <button class="btn btn-danger pull-right" style="cursor: none;">
						<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 
						ปิดรับการลงทะเบียน						
					</button> -->
				<a class="btn btn-primary pull-right" href="<?php echo $this->Html->url(array('action' => 'register',$project['Project']['id'])); ?>"
					role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
					ลงทะเบียนเพิ่มเติม
				</a>
			 
			<div id="no-more-tables"> 				
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th style="text-align: center;">ลำดับ</th>
							<th style="text-align: center;">ชื่อ - นามสกุล</th>
							<th style="text-align: center;">ชื่อโรงเรียน</th>
							<th style="text-align: center;">ระดับ</th>
							<th style="text-align: center;">อาจารย์ผู้ควบคุม</th>
							<th style="text-align: center;">ใบสมัคร</th>
							<th style="text-align: center;">ยกเลิกการลงทะเบียน</th>
							
						</tr>
					</thead>
					<tbody>
						<?php
							echo $output;
						?>						
						
					</tbody>
				</table>
			</div>			
	
		</div>

	</div>
	
	<div class="row">
		<div class="col-md-6">
			<?php
			if($project['Type']['id'] == 2 && $project['Project']['price'] != 0){

			?>
									
				<div class="alert alert-warning">
					<span class="glyphicon glyphicon-stop" aria-hidden="true"></span>
					ชำระเงินค่าลงทะเบียนด้วยตนเอง ณ ศูนย์บริการวิชาการฯ ชั้น 2 อาคารเฉลิมพระเกียรติ   คณะเกษตรศาสตร์  มหาวิทยาลัยเชียงใหม่
					<br><br>
					<span class="glyphicon glyphicon-stop" aria-hidden="true"></span>
					โอนเงินเข้าบัญชีออมทรัพย์ ธนาคารกรุงไทย สาขามหาวิทยาลัยเชียงใหม่ 
					<br>
					ชื่อบัญชี: ศูนย์บริการวิชาการและถ่ายทอดเทคโนโลยีการเกษตร 
					<br>
					เลขที่บัญชี: 456-0-12117-6
					<br>
					พร้อมส่งหลักฐานการโอนเงิน มายัง Line Id . 083-6228414 
					หรือทางอีเมลล์ chaichet.agei@hotmail.com 

				</div>
			<?php
			}
			?>
		</div>
		<div class="col-md-6">	
			
		<div class="alert alert-info">
				ติดต่อสอบถามเพิ่มเติมได้ที่ งานบริการการศึกษา และพัฒนาคุณภาพนักศึกษา
				<br>
				คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่ โทรศัพท์ 053 - 944641-2, 053 - 944644 โทรสาร 053 - 944666  (ในวันเวลาราชการ)
			</div>
			
		</div>
	</div>
	
</div>
