<div class="container" style="padding:0 15px 0;">	
	<div class="row">
		<div class="col-md-12">
                  <div class="well">

                        <tbody>
                              <form method="post" accept-charset="utf-8">
                              <div class="panel panel-success">
                                    <div class="panel-heading">
                                          <h3 class="panel-title"><i class="glyphicon glyphicon-plus"></i>แก้ไขข้อมูล</h3>
                                    </div>
                                    <div class="panel-body">
                                          <?php
                                                echo $this->Form->input('CurriculumInfo.id',array(
                                                      'hidden'
                                                ));
                                          ?>
                                          <label class="col-sm-4 control-label">หลักสูตร</label>
                                          <div class="col-sm-8">
                                                <div class="form-group">
                                                <?php echo $this->Form->input('CurriculumInfo.title_th',array(
                                                      'type' => 'text',
                                                      'label' => false,
                                                      'div' => false,
                                                      'class' => array('form-control cssrequire'),
                                                      'error' => false,
                                                      'placeholder' => 'ชื่อหลักสูตรภาษาไทย',
                                                      'required'
                                                ));
                                                ?>
                                                </div>
                                          </div>
                                          <label class="col-sm-4 control-label">หลักสูตร(ENG)</label>
                                          <div class="col-sm-8">
                                                <div class="form-group">
                                                <?php echo $this->Form->input('CurriculumInfo.title_en',array(
                                                      'type' => 'text',
                                                      'label' => false,
                                                      'div' => false,
                                                      'class' => array('form-control cssrequire'),
                                                      'error' => false,
                                                      'placeholder' => 'ชื่อหลักสูตรภาษาอังกฤษ',
                                                ));
                                                ?>
                                                </div>
                                          </div>
                                          <label class="col-sm-4 control-label">ระดับ</label>
                                                <div class="col-sm-8">
                                                      <div class="form-group">
                                                      <?php
                                                            $degrees = array(
                                                                  '1' => '1',
                                                                  '3' => '3',
                                                                  '5' => '5',
                                                            );
                                                            echo $this->Form->input('CurriculumInfo.degree_id',array(
                                                                  'options' => $degrees,
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'placeholder' => '1 = ปริญญาตรี, 3 = ปริญญาโท, 5 = ปริญญาเอก',
                                                                  'required',
                                                            ));
                                                      ?>
                                                      </div>
                                                </div>
                                          <label class="col-sm-4 control-label">ลิ้งค์</label>
                                                <div class="col-sm-8">
                                                      <div class="form-group">
                                                      <?php
                                                            echo $this->Form->input('CurriculumInfo.link_th',array(
                                                                  'type' => 'text',
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'placeholder' => 'ระดับการศึกษา',
                                                                  'required'
                                                            ));
                                                      ?>
                                                      </div>
                                                </div>
                                          <label class="col-sm-4 control-label">เดือน</label>
                                                <div class="col-sm-8">
                                                      <div class="form-group">
                                                      <?php
                                                            $month = array(
                                                                  '1' => '1',
                                                                  '2' => '2',
                                                                  '3' => '3',
                                                                  '4' => '4',
                                                                  '5' => '5',
                                                                  '6' => '6',
                                                                  '7' => '7',
                                                                  '8' => '8',
                                                                  '9' => '9',
                                                                  '10' => '10',
                                                                  '11' => '11',
                                                                  '12' => '12',
                                                            );
                                                            echo $this->Form->input('CurriculumInfo.month',array(
                                                                  'options' => $month,
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'placeholder' => 'กรอกชื่อหลักสูตร',
                                                                  'required'
                                                            ));
                                                      ?>
                                                      </div>
                                                </div>
                                          <label class="col-sm-4 control-label">ปี</label>
                                                <div class="col-sm-8">
                                                      <div class="form-group">
                                                      <?php
                                                            $year = array(
                                                                  '2569' => '2569',
                                                                  '2568' => '2568',
                                                                  '2567' => '2567',
                                                                  '2566' => '2566',
                                                                  '2565' => '2565',
                                                                  '2564' => '2564',
                                                                  '2563' => '2563',
                                                                  '2562' => '2562',
                                                                  '2561' => '2561',
                                                                  '2560' => '2560',
                                                                  '2559' => '2559',
                                                                  '2558' => '2558',
                                                                  '2557' => '2557',
                                                            );
                                                            echo $this->Form->input('CurriculumInfo.year',array(
                                                                  'options' => $year,
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'required'
                                                            ));
                                                      ?>
                                                      </div>
                                                </div>
                                    </div>
                                    <div class="form-group">
                                          <input role="button" class="btn btn-success" type="submit" value="บันทึกข้อมูล" onclick="return confirm('ยืนยันการเพิ่มข้อมูล')">
                                    </div>
                              </div>
                              </form>
                        </tbody>
                  </div>
            </div>
      </div>
</div>
<div style="clear: both;"></div>