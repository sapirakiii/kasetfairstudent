<?php
function DateThai($strDate)
{

	$strYear = date("Y", strtotime($strDate)) + 543;
	$strMonth = date("n", strtotime($strDate));
	$strDay = date("j", strtotime($strDate));
	$strHour = date("H", strtotime($strDate));
	$strMinute = date("i", strtotime($strDate));
	$strSeconds = date("s", strtotime($strDate));
	$strMonthCut = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
	$strMonthThai = $strMonthCut[$strMonth];
	return "$strDay $strMonthThai $strYear";
	//, $strHour:$strMinute
}
?>





<?php
date_default_timezone_set('Asia/Bangkok');
$date = date("Y-m-d");
$time = date("H:i:s");


function DateDiff($strDate1, $strDate2)
{
	return (strtotime($strDate2) - strtotime($strDate1)) /  (60 * 60 * 24);  // 1 day = 60*60*24
}

function TimeDiff($strTime1, $strTime2)
{
	return (strtotime($strTime2) - strtotime($strTime1)) /  (60 * 60); // 1 Hour =  60*60                           
}




?>
<div class="container-fluid" style="padding:15px 0 0;">
	<div class="panel panel-default" style="border-color: #F2F2F2!important">
		<div class="panel-title">
			<h3 style="margin:0px !important;">
				<center>
					<div class="font_panel2">ร้อยละของผู้ตอบแบบสอบถาม และสถานภาพการทำงานของบัณฑิต<br>ปีการศึกษาที่<?php echo $years['Yearterm']['year'];?></div><br>
				</center>
			</h3>
		</div>
		<form method="get">
					<div class="dropdown">
						<button class="btn btn-default btn-info dropdown-toggle" type="button" id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aira-expanded="false">
							<?php echo $years['Yearterm']['year'];?>
						</button>
						<ul class="dropdown-menu" aria-labelleby="dropdownMenu1">
							<?php foreach($dropdowns as $dropdown) { ?>
								<?php
									if($years['Yearterm']['year'] == $dropdown['Yearterm']['year'])
								{
									$active = "active";
								}
								else
								{
									$active = "";
								}
								?>
								<li class="<?php echo $active; ?>">
									<a href="<?php echo $this->Html->url(array('action'=>'employment_data',$dropdown['Yearterm']['year'])); ?>">
										<font color="black"><?php echo $dropdown['Yearterm']['year']; ?></font>
									</a>
								</li>
								<?php } ?>
						</ul>
					</div>
				</form>
			<div class="container" align="center">
				<h3>ร้อยละของผู้ตอบแบบสอบถาม และสถานภาพการทำงานของบัณฑิต</h3>
			</div>
			<div id="no-more-tables">
				<div id="no-more-tables">
					<!-- <a class="btn btn-success" href="<?php echo $this->Html->url(array('action' => 'add_employment_data')); ?>" role="button" target="_blank">เพิ่มข้อมูล</a> -->
					<table class="table table-bordered">
							<tr>
								<th style="text-align: center;" rowspan="3">ลำดับ</th>
								<th style="text-align: center;" rowspan="3">ระดับการศึกษา</th>
								<th style="text-align: center;" rowspan="3">หลักสูตร</th>
								<th style="text-align: center;" rowspan="3">ปีการศึกษา</th>
								<th style="text-align: center;" rowspan="3">ผู้สำเร็จการศึกษา</th>
								<th style="text-align: center;" colspan="2">ร้อยละผู้ตอบแบบสอบถาม</th>
								<th style="text-align: center;" rowspan="3">จำนวนผู้ตอบแบบสอบถาม</th>
								<th style="text-align: center;" colspan="8">สถานภาพการทำงานของบัณฑิต</th>
								<!-- <th style="text-align: center;" rowspan="3" width="1%">แก้ไข</th>
								<th style="text-align: center;" rowspan="3" width="1%">ลบ</th> -->
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="2">จำนวนผู้ตอบแบบสอบถาม</th>
								<th style="text-align: center;" rowspan="2">ร้อยละ</th>
								<th style="text-align: center;" colspan="2">ทำงานแล้ว</th>
								<th style="text-align: center;" colspan="2">ทำงานแล้วและกำลังศึกษาต่อ</th>
								<th style="text-align: center;" colspan="2">กำลังศึกษาต่อ</th>
								<th style="text-align: center;" colspan="2">ยังไม่ได้ทำงานและไม่ได้ศึกษาต่อ</th>
							</tr>
							<tr>
								<th style="text-align: center;">จำนวน</th>
								<th style="text-align: center;">ร้อยละ</th>
								<th style="text-align: center;">จำนวน</th>
								<th style="text-align: center;">ร้อยละ</th>
								<th style="text-align: center;">จำนวน</th>
								<th style="text-align: center;">ร้อยละ</th>
								<th style="text-align: center;">จำนวน</th>
								<th style="text-align: center;">ร้อยละ</th>
							</tr>
						<tbody>
							<?php if($employment_datas == null){ ?>
								<tr>
									<td align="center" colspan="16">No Data </td>
								<tr>
							<?php }else { ?>
								<?php
									$i = 0;
									$count1 = 0;
									$count2 = 0;
									$count3 = 0;
									$count4 = 0;
									$count5 = 0;
									$count6 = 0;
									$count7 = 0;
									foreach ($employment_datas as $employment_data){
									 $i++;?>
									<tr>
										<td align="center"><?php echo $i;?></td>
										<td><?php echo $employment_data['Employment']['degree_name'];?></td>
										<td><?php echo $employment_data['Employment']['syllabus'];?></td>
										<td><?php echo $employment_data['Employment']['year'];?></td>
										<td>
											<?php 
												$count1 = $count1+$employment_data['Employment']['graduate_total'];
												echo $employment_data['Employment']['graduate_total'];
											?>
										</td>
										<td>
											<?php 
												$count2 = $count2+$employment_data['Employment']['respondents'];
												echo $employment_data['Employment']['respondents'];
											?>
										</td>
										<td>
											<?php $result1 = ($employment_data['Employment']['respondents'] * 100 / $employment_data['Employment']['graduate_total']);
											$rounded_number1 = round($result1, 2); echo $rounded_number1;?>
										</td>
										<td> 
											<?php 
												$count3 = $count3+$employment_data['Employment']['ws_respondents'];
												echo $employment_data['Employment']['ws_respondents'];
											?>
										</td>
										<td> 
											<?php
												$count4 = $count4+$employment_data['Employment']['ws_aw_qty'];
												 echo $employment_data['Employment']['ws_aw_qty'];
											?>
										</td>
										<td>
											<?php $result2 = $employment_data['Employment']['ws_aw_qty'] * 100 / $employment_data['Employment']['ws_respondents'];  
											$rounded_number2 = round($result2, 2); echo $rounded_number2;?>
										</td>
										<td> 
											<?php
												$count5 = $count5+$employment_data['Employment']['ws_was_qty'];
												echo $employment_data['Employment']['ws_was_qty'];
											?> 
										</td>
										<td>
											<?php $result3 = $employment_data['Employment']['ws_was_qty'] * 100 / $employment_data['Employment']['ws_respondents'];  
											$rounded_number3 = round($result3, 2); echo $rounded_number3;?>
										</td>
										<td>
											<?php
												$count6 = $count6+$employment_data['Employment']['ws_cs_qty'];
												 echo $employment_data['Employment']['ws_cs_qty'];
											?>
										</td>
										<td>
											<?php $result4 = $employment_data['Employment']['ws_cs_qty'] * 100 / $employment_data['Employment']['ws_respondents'];  
											$rounded_number4 = round($result4, 2); echo $rounded_number4;?>
										</td>
										<td>
											<?php
												$count7 = $count7+$employment_data['Employment']['ws_ue_qty'];
												echo $employment_data['Employment']['ws_ue_qty'];
											?>
										</td>
										<td>
											<?php $result5 = $employment_data['Employment']['ws_ue_qty'] * 100 / $employment_data['Employment']['ws_respondents']; 
											$rounded_number5 = round($result5, 2); echo  $rounded_number5;?>
										</td>
										<!-- <td align="center">
											<a class="btn btn-warning"
											href="<?php echo $this->Html->url(array('action' => 'edit_employment_data', $employment_data['Employment']['id'])); ?>" role="button" target="_blank">แก้ไขข้อมูล</a>
										</td>
										<td align="center">
											<a class="btn btn-danger" 
											href="<?php echo $this->Html->url(array('action' => 'remove_employment_data', $employment_data['Employment']['id'])); ?>" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')">ลบข้อมูล</a>
										</td> -->
									</tr>
								<?php } ?>
									<tr>
										<td colspan="4" style="text-align: center;"> รวม</td>
										<td><?php echo $count1; ?></td>
										<td><?php echo $count2; ?></td>
										<td><?php $average1 = $count2 * 100 / $count1; $average_total1 = round($average1,2);echo $average_total1; ?></td>
										<td><?php echo $count3; ?></td>
										<td><?php echo $count4; ?></td>
										<td><?php $average2 = $count4 * 100 / $count3; $average_total2 = round($average2,2);echo $average_total2; ?></td>
										<td><?php echo $count5; ?></td>
										<td><?php $average3 = $count5 * 100 / $count3; $average_total3 = round($average3,2);echo $average_total3; ?></td>
										<td><?php echo $count6; ?></td>
										<td><?php $average4 = $count6 * 100 / $count3; $average_total4 = round($average4,2);echo $average_total4; ?></td>
										<td><?php echo $count7; ?></td>
										<td><?php $average5 = $count7 * 100 / $count3; $average_total5 = round($average5,2);echo $average_total5; ?></td>
									</tr>	

							<?php } ?>
						</tbody>
					</table>
		 
				</div>
			</div>
			
			<div class="container" align="center">
				<h3>ประเภทของการมีงานทำ</h3>
			</div>
			<div id="no-more-tables">
				<div id="no-more-tables">
					<table class="table table-bordered">
						
							<tr>
								<th style="text-align: center;" rowspan="3">ลำดับ</th>
								<th style="text-align: center;" rowspan="3" colspan="">ระดับการศึกษา</th>
								<th style="text-align: center;" rowspan="3" colspan="">หลักสูตร</th>
								<th style="text-align: center;" rowspan="3" colspan="">ปีการศึกษา</th>
								<th style="text-align: center;" rowspan="3" colspan="">ผู้สำเร็จการศึกษา</th>
								
								<th style="text-align: center;" rowspan="" colspan="8">ประเภทของการมีงานทำ</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="2" colspan="">จำนวนผู้ตอบแบบสอบถาม</th>
								<th style="text-align: center;" rowspan="2" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="2">มีงานทำก่อนสำเร็จการศึกษา และอยู่ในสายงานเดิม</th>
								<th style="text-align: center;" rowspan="" colspan="2">มีงานทำก่อนสำเร็จการศึกษา แต่เปลี่ยนสายงาน</th>
								<th style="text-align: center;" rowspan="" colspan="2">มีงานทำก่อนสำเร็จการศึกษา และอยู่ในสายงานเดิมและเลื่อนระดับ</th>
								<th style="text-align: center;" rowspan="" colspan="2">มีงานทำหลังสำเร็จการศึกษา</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
							</tr>
							<?php if($employment_datas == null){ ?>
								<tr>
									<td align="center" colspan="16">No Data </td>
								<tr>
							<?php }else { ?>
							<tr>
								<?php
									$et_total = 0;
									$et_sum1 = 0;
									$et_sum2 = 0;
									$et_sum3 = 0;
									$et_sum4 = 0;
									$et_sum5 = 0;
									$et = 0;
									foreach ($employment_datas as $employment_data){
									$et++;
								?>
								<td align="center"><?php echo $et; ?></td>
								<td><?php echo $employment_data['Employment']['degree_name'];?></td>
								<td><?php echo $employment_data['Employment']['syllabus'];?></td>
								<td><?php echo $employment_data['Employment']['year'];?></td>
								<td><?php echo $employment_data['Employment']['graduate_total']; $et_total = $et_total+$employment_data['Employment']['graduate_total']; ?></td>
								<td><?php echo $employment_data['Employment']['et_respondents']; $et_sum1 = $et_sum1+$employment_data['Employment']['et_respondents'];?></td>
								<td><?php echo round(($employment_data['Employment']['et_respondents'] *100 / $employment_data['Employment']['graduate_total']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['et_bgol_qty'];  $et_sum2 = $et_sum2+$employment_data['Employment']['et_bgol_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['et_bgol_qty'] *100 / $employment_data['Employment']['et_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['et_bgcl_qty'];  $et_sum3 = $et_sum3+$employment_data['Employment']['et_bgcl_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['et_bgcl_qty'] *100 / $employment_data['Employment']['et_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['et_bgpol_qty'];  $et_sum4 = $et_sum4+$employment_data['Employment']['et_bgpol_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['et_bgpol_qty'] *100 / $employment_data['Employment']['et_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['et_ag_qty'];  $et_sum5 = $et_sum5+$employment_data['Employment']['et_ag_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['et_ag_qty'] *100 / $employment_data['Employment']['et_respondents']), 2); ?></td>
							</tr>
							<?php } ?>
							<tr>
								<td rowspan="" colspan="4">รวม</td>
								<td rowspan="" colspan=""><?php echo $et_total ?></td>
								<td rowspan="" colspan=""><?php echo $et_sum1 ?></td>
								<td><?php echo round(($et_sum1 * 100 / $et_total), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $et_sum2 ?></td>
								<td><?php echo round(($et_sum2 * 100 / $et_total), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $et_sum3 ?></td>
								<td><?php echo round(($et_sum3 * 100 / $et_total), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $et_sum4 ?></td>
								<td><?php echo round(($et_sum4 * 100 / $et_total), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $et_sum5 ?></td>
								<td><?php echo round(($et_sum5 * 100 / $et_total), 2); ?></td>
							</tr>
							<?php } ?>
							
					</table>
				</div>
			</div>
			
			<div class="container" align="center">
				<h3>ระยะเวลาในการได้งานทำหลังจากสำเร็จการศึกษา</h3>
			</div>
			<div id="no-more-tables">
				<div id="no-more-tables">
					<table class="table table-bordered">
							<tr>
								<th style="text-align: center;" rowspan="3">ลำดับ</th>
								<th style="text-align: center;" rowspan="3" colspan="">ระดับการศึกษา</th>
								<th style="text-align: center;" rowspan="3" colspan="">หลักสูตร</th>
								<th style="text-align: center;" rowspan="3" colspan="">ปีการศึกษา</th>
								<th style="text-align: center;" rowspan="3" colspan="">ผู้สำเร็จการศึกษา</th>
								
								<th style="text-align: center;" rowspan="" colspan="8">ระยะเวลาในการได้งานทำหลังจากสำเร็จการศึกษา</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="2" colspan="">จำนวนผู้ตอบแบบสอบถาม</th>
								<th style="text-align: center;" rowspan="2" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="2">ได้งานทันที</th>
								<th style="text-align: center;" rowspan="" colspan="2">1 - 2 เดือน</th>
								<th style="text-align: center;" rowspan="" colspan="2">3 - 6 เดือน</th>
								<th style="text-align: center;" rowspan="" colspan="2">7 - 9 เดือน</th>
								<th style="text-align: center;" rowspan="" colspan="2">10 - 12 เดือน</th>
								<th style="text-align: center;" rowspan="" colspan="2">มากกว่า 1 ปี</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
							</tr>

							<?php if($employment_datas == null){ ?>
								<tr>
									<td align="center" colspan="100">No Data </td>
								<tr>
							<?php }else { ?>
							<tr>
								<?php
									$tj_sum1 = 0;
									$tj_sum2 = 0;
									$tj_sum3 = 0;
									$tj_sum4 = 0;
									$tj_sum5 = 0;
									$tj_sum6 = 0;
									$tj_sum = 0;
									$tj_total = 0;
									$tj = 0;
									foreach ($employment_datas as $employment_data){
									$tj++;
								?>
								<td align="center"><?php echo $tj;?></td>
								<td><?php echo $employment_data['Employment']['degree_name'];?></td>
								<td><?php echo $employment_data['Employment']['syllabus'];?></td>
								<td><?php echo $employment_data['Employment']['year'];?></td>
								<td><?php echo $employment_data['Employment']['graduate_total']; $tj_total = $tj_total + $employment_data['Employment']['graduate_total']?></td>
								<td><?php echo $employment_data['Employment']['tj_respondents']; $tj_sum = $tj_sum + $employment_data['Employment']['tj_respondents'];?></td>
								<td><?php echo round(($employment_data['Employment']['tj_respondents'] *100 / $employment_data['Employment']['graduate_total']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['tj_im_qty']; $tj_sum1 = $tj_sum1 + $employment_data['Employment']['tj_im_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['tj_im_qty'] *100 / $employment_data['Employment']['tj_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['tj_12_qty']; $tj_sum2 = $tj_sum2 + $employment_data['Employment']['tj_12_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['tj_12_qty'] *100 / $employment_data['Employment']['tj_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['tj_36_qty']; $tj_sum3 = $tj_sum3 + $employment_data['Employment']['tj_36_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['tj_36_qty'] *100 / $employment_data['Employment']['tj_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['tj_79_qty']; $tj_sum4 = $tj_sum4 + $employment_data['Employment']['tj_79_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['tj_79_qty'] *100 / $employment_data['Employment']['tj_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['tj_ac_qty']; $tj_sum5 = $tj_sum5 + $employment_data['Employment']['tj_ac_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['tj_ac_qty'] *100 / $employment_data['Employment']['tj_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['tj_mc_qty']; $tj_sum6 = $tj_sum6 + $employment_data['Employment']['tj_mc_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['tj_mc_qty'] *100 / $employment_data['Employment']['tj_respondents']), 2); ?></td>
							</tr>
							<?php } ?>
							<tr>
								<td rowspan="" colspan="4">รวม</td>
								<td rowspan="" colspan=""><?php echo $tj_total ?></td>
								<td rowspan="" colspan=""><?php echo $tj_sum ?></td>
								<td><?php echo round(($tj_sum * 100 / $tj_total), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $tj_sum1 ?></td>
								<td><?php echo round(($tj_sum1 * 100 / $tj_sum), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $tj_sum2 ?></td>
								<td><?php echo round(($tj_sum2 * 100 / $tj_sum), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $tj_sum3 ?></td>
								<td><?php echo round(($tj_sum3 * 100 / $tj_sum), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $tj_sum4 ?></td>
								<td><?php echo round(($tj_sum4 * 100 / $tj_sum), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $tj_sum5 ?></td>
								<td><?php echo round(($tj_sum5 * 100 / $tj_sum), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $tj_sum6 ?></td>
								<td><?php echo round(($tj_sum6 * 100 / $tj_sum), 2); ?></td>
							</tr>
							<?php } ?>
					</table>
				</div>
			</div>

			<div class="container" align="center">
				<h3>ประเภทงานที่ทำ</h3>
			</div>
			<div id="no-more-tables">
				<div id="no-more-tables">
					<table class="table table-bordered">
							<tr>
								<th style="text-align: center;" rowspan="3">ลำดับ</th>
								<th style="text-align: center;" rowspan="3" colspan="">ระดับการศึกษา</th>
								<th style="text-align: center;" rowspan="3" colspan="">หลักสูตร</th>
								<th style="text-align: center;" rowspan="3" colspan="">ปีการศึกษา</th>
								<th style="text-align: center;" rowspan="3" colspan="">ผู้สำเร็จการศึกษา</th>
								<th style="text-align: center;" rowspan="" colspan="8">ประเภทงานที่ทำ</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="2" colspan="">จำนวนผู้ตอบแบบสอบถาม</th>
								<th style="text-align: center;" rowspan="2" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="2">หน่วยงานของรัฐ</th>
								<th style="text-align: center;" rowspan="" colspan="2">หน่วยงานรัฐวิสาหกิจ</th>
								<th style="text-align: center;" rowspan="" colspan="2">องค์กรธุรกิจ/เอกชน</th>
								<th style="text-align: center;" rowspan="" colspan="2">องค์การต่างประเทศ/ระหว่างประเทศ</th>
								<th style="text-align: center;" rowspan="" colspan="2">เจ้าของกิจการ/ธุรกิจส่วนตัว</th>
								<th style="text-align: center;" rowspan="" colspan="2">อาชีพอิสระ</th>
								<th style="text-align: center;" rowspan="" colspan="2">องค์กรไม่แสวงหาผลกำไร</th>
								<th style="text-align: center;" rowspan="" colspan="2">อื่น ๆ</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
							</tr>
							<?php if($employment_datas == null){ ?>
								<tr>
									<td align="center" colspan="100">No Data </td>
								<tr>
							<?php }else { ?>
							<tr>
								<?php
									$wt_total = 0;
									$wt_sum_main = 0;
									$wt_sum1 = 0;
									$wt_sum2 = 0;
									$wt_sum3 = 0;
									$wt_sum4 = 0;
									$wt_sum5 = 0;
									$wt_sum6 = 0;
									$wt_sum7 = 0;
									$wt_sum8 = 0;
									$wt = 0;
									foreach ($employment_datas as $employment_data){
									$wt++;
								?>
								<td align="center"><?php echo $wt;?></td>
								<td><?php echo $employment_data['Employment']['degree_name'];?></td>
								<td><?php echo $employment_data['Employment']['syllabus'];?></td>
								<td><?php echo $employment_data['Employment']['year'];?></td>
								<td><?php echo $employment_data['Employment']['graduate_total']; $wt_total = $wt_total + $employment_data['Employment']['graduate_total']?></td>
								<td><?php echo $employment_data['Employment']['wt_respondents']; $wt_sum_main = $wt_sum_main + $employment_data['Employment']['wt_respondents'];?></td>
								<td><?php echo round(($employment_data['Employment']['wt_respondents'] *100 / $employment_data['Employment']['graduate_total']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['wt_ga_qty']; $wt_sum1 = $wt_sum1 + $employment_data['Employment']['wt_ga_qty']?></td>
								<td><?php echo round(($employment_data['Employment']['wt_ga_qty'] *100 / $employment_data['Employment']['wt_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['wt_se_qty']; $wt_sum2 = $wt_sum2 + $employment_data['Employment']['wt_se_qty']?></td>
								<td><?php echo round(($employment_data['Employment']['wt_se_qty'] *100 / $employment_data['Employment']['wt_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['wt_pc_qty']; $wt_sum3 = $wt_sum3 + $employment_data['Employment']['wt_pc_qty']?></td>
								<td><?php echo round(($employment_data['Employment']['wt_pc_qty'] *100 / $employment_data['Employment']['wt_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['wt_io_qty']; $wt_sum4 = $wt_sum4 + $employment_data['Employment']['wt_io_qty']?></td>
								<td><?php echo round(($employment_data['Employment']['wt_io_qty'] *100 / $employment_data['Employment']['wt_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['wt_pb_qty']; $wt_sum5 = $wt_sum5 + $employment_data['Employment']['wt_pb_qty']?></td>
								<td><?php echo round(($employment_data['Employment']['wt_pb_qty'] *100 / $employment_data['Employment']['wt_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['wt_fl_qty']; $wt_sum6 = $wt_sum6 + $employment_data['Employment']['wt_fl_qty']?></td>
								<td><?php echo round(($employment_data['Employment']['wt_fl_qty'] *100 / $employment_data['Employment']['wt_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['wt_no_qty']; $wt_sum7 = $wt_sum7 + $employment_data['Employment']['wt_no_qty']?></td>
								<td><?php echo round(($employment_data['Employment']['wt_no_qty'] *100 / $employment_data['Employment']['wt_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['wt_ot_qty']; $wt_sum8 = $wt_sum8 + $employment_data['Employment']['wt_ot_qty']?></td>
								<td><?php echo round(($employment_data['Employment']['wt_ot_qty'] *100 / $employment_data['Employment']['wt_respondents']), 2); ?></td>
							</tr>
							<?php } ?>
							<tr>
								<td rowspan="" colspan="4">รวม</td>
								<td rowspan="" colspan=""><?php echo $wt_total ?></td>
								<td rowspan="" colspan=""><?php echo $wt_sum_main ?></td>
								<td><?php echo round(($wt_sum_main * 100 / $wt_total), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $wt_sum1 ?></td>
								<td><?php echo round(($wt_sum1 * 100 / $wt_sum_main), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $wt_sum2 ?></td>
								<td><?php echo round(($wt_sum2 * 100 / $wt_sum_main), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $wt_sum3 ?></td>
								<td><?php echo round(($wt_sum3 * 100 / $wt_sum_main), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $wt_sum4 ?></td>
								<td><?php echo round(($wt_sum4 * 100 / $wt_sum_main), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $wt_sum5 ?></td>
								<td><?php echo round(($wt_sum5 * 100 / $wt_sum_main), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $wt_sum6 ?></td>
								<td><?php echo round(($wt_sum6 * 100 / $wt_sum_main), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $wt_sum7 ?></td>
								<td><?php echo round(($wt_sum7 * 100 / $wt_sum_main), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $wt_sum8 ?></td>
								<td><?php echo round(($wt_sum8 * 100 / $wt_sum_main), 2); ?></td>
							</tr>
							<?php } ?>
					</table>
				</div>
			</div>

			<div class="container" align="center">
				<h3>การทำงานตรงสาขาวิชาที่สำเร็จการศึกษา</h3>
			</div>
			<div id="no-more-tables">
				<div id="no-more-tables">
					<table class="table table-bordered">
							<tr>
								<th style="text-align: center;" rowspan="3">ลำดับ</th>
								<th style="text-align: center;" rowspan="3" colspan="">ระดับการศึกษา</th>
								<th style="text-align: center;" rowspan="3" colspan="">หลักสูตร</th>
								<th style="text-align: center;" rowspan="3" colspan="">ปีการศึกษา</th>
								<th style="text-align: center;" rowspan="3" colspan="">ผู้สำเร็จการศึกษา</th>
								
								<th style="text-align: center;" rowspan="" colspan="8">การทำงานตรงสาขาวิชาที่สำเร็จการศึกษา</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="2" colspan="">จำนวนผู้ตอบแบบสอบถาม</th>
								<th style="text-align: center;" rowspan="2" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="2">ตรงสาขา</th>
								<th style="text-align: center;" rowspan="" colspan="2">ไม่ตรงสาขา</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
							</tr>
							<?php if($employment_datas == null){ ?>
								<tr>
									<td align="center" colspan="100">No Data </td>
								<tr>
							<?php }else { ?>
							<tr>
								<?php
									$wf = 0;
									$wf_total = 0;
									$wf_main = 0;
									$wf_sum1 = 0;
									$wf_sum2 = 0;
									foreach ($employment_datas as $employment_data){
									$wf++;
								?>
								<td align="center"><?php echo $wf;?></td>
								<td><?php echo $employment_data['Employment']['degree_name'];?></td>
								<td><?php echo $employment_data['Employment']['syllabus'];?></td>
								<td><?php echo $employment_data['Employment']['year'];?></td>
								<td><?php echo $employment_data['Employment']['graduate_total']; $wf_total = $wf_total + $employment_data['Employment']['graduate_total']?></td>
								<td><?php echo $employment_data['Employment']['wf_respondents']; $wf_main = $wf_main + $employment_data['Employment']['wf_respondents'];?></td>
								<td><?php echo round(($employment_data['Employment']['wf_respondents'] *100 / $employment_data['Employment']['graduate_total']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['wf_yes_qty']; $wf_sum1 = $wf_sum1 + $employment_data['Employment']['wf_yes_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['wf_yes_qty'] *100 / $employment_data['Employment']['graduate_total']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['wf_no_qty']; $wf_sum2 = $wf_sum2 + $employment_data['Employment']['wf_no_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['wf_no_qty'] *100 / $employment_data['Employment']['graduate_total']), 2); ?></td>
							</tr>
							<?php } ?>
							<tr>
								<td rowspan="" colspan="4">รวม</td>
								<td rowspan="" colspan=""><?php echo $wf_total ?></td>
								<td rowspan="" colspan=""><?php echo $wf_main ?></td>
								<td><?php echo round(($wf_main * 100 / $wf_total), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $wf_sum1 ?></td>
								<td><?php echo round(($wf_sum1 * 100 / $wf_main), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $wf_sum2 ?></td>
								<td><?php echo round(($wf_sum2 * 100 / $wf_main), 2); ?></td>
							</tr>
							<?php } ?>
					</table>
				</div>
			</div>

			<div class="container" align="center">
				<h3>การนำความรู้จากสาขาวิชาที่เรียนมาประยุกต์ใช้ในการทำงาน</h3>
			</div>
			<div id="no-more-tables">
				<div id="no-more-tables">
					<table class="table table-bordered">
							<tr>
								<th style="text-align: center;" rowspan="3">ลำดับ</th>
								<th style="text-align: center;" rowspan="3" colspan="">ระดับการศึกษา</th>
								<th style="text-align: center;" rowspan="3" colspan="">หลักสูตร</th>
								<th style="text-align: center;" rowspan="3" colspan="">ปีการศึกษา</th>
								<th style="text-align: center;" rowspan="3" colspan="">ผู้สำเร็จการศึกษา</th>
								<th style="text-align: center;" rowspan="" colspan="8">การนำความรู้จากสาขาวิชาที่เรียนมาประยุกต์ใช้ในการทำงาน</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="2" colspan="">จำนวนผู้ตอบแบบสอบถาม</th>
								<th style="text-align: center;" rowspan="2" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="2">น้อยที่สุด</th>
								<th style="text-align: center;" rowspan="" colspan="2">น้อย</th>
								<th style="text-align: center;" rowspan="" colspan="2">ปานกลาง</th>
								<th style="text-align: center;" rowspan="" colspan="2">มาก</th>
								<th style="text-align: center;" rowspan="" colspan="2">มากที่สุด</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
							</tr>
							<?php if($employment_datas == null){ ?>
								<tr>
									<td align="center">No Data </td>
								<tr>
							<?php }else { ?>
							<tr>
								<?php
									$kw = 0;
									$kw_main = 0;
									$kw_total = 0;
									$kw_sum1 = 0;
									$kw_sum2 = 0;
									$kw_sum3 = 0;
									$kw_sum4 = 0;
									$kw_sum5 = 0;
									foreach ($employment_datas as $employment_data){
									$kw++;
								?>
								<td align="center"><?php echo $kw;?></td>
								<td><?php echo $employment_data['Employment']['degree_name'];?></td>
								<td><?php echo $employment_data['Employment']['syllabus'];?></td>
								<td><?php echo $employment_data['Employment']['year'];?></td>
								<td><?php echo $employment_data['Employment']['graduate_total']; $kw_total = $kw_total + $employment_data['Employment']['graduate_total']?></td>
								<td><?php echo $employment_data['Employment']['kw_respondents']; $kw_main = $kw_main + $employment_data['Employment']['kw_respondents'];?></td>
								<td><?php echo round(($employment_data['Employment']['kw_respondents'] *100 / $employment_data['Employment']['graduate_total']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['kw_1_qty']; $kw_sum1 = $kw_sum1 + $employment_data['Employment']['kw_1_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['kw_1_qty'] *100 / $employment_data['Employment']['kw_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['kw_2_qty']; $kw_sum2 = $kw_sum2 + $employment_data['Employment']['kw_2_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['kw_2_qty'] *100 / $employment_data['Employment']['kw_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['kw_3_qty']; $kw_sum3 = $kw_sum3 + $employment_data['Employment']['kw_3_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['kw_3_qty'] *100 / $employment_data['Employment']['kw_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['kw_4_qty']; $kw_sum4 = $kw_sum4 + $employment_data['Employment']['kw_4_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['kw_4_qty'] *100 / $employment_data['Employment']['kw_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['kw_5_qty']; $kw_sum5 = $kw_sum5 + $employment_data['Employment']['kw_5_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['kw_5_qty'] *100 / $employment_data['Employment']['kw_respondents']), 2); ?></td>
							</tr>
							<?php } ?>
							<tr>
								<td rowspan="" colspan="4">รวม</td>
								<td rowspan="" colspan=""><?php echo $kw_total ?></td>
								<td rowspan="" colspan=""><?php echo $kw_main ?></td>
								<td><?php echo round(($kw_main * 100 / $kw_total), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $kw_sum1 ?></td>
								<td><?php echo round(($kw_sum1 * 100 / $tj_sum), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $kw_sum2 ?></td>
								<td><?php echo round(($kw_sum2 * 100 / $tj_sum), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $kw_sum3 ?></td>
								<td><?php echo round(($kw_sum3 * 100 / $tj_sum), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $kw_sum4 ?></td>
								<td><?php echo round(($kw_sum4 * 100 / $tj_sum), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $kw_sum5 ?></td>
								<td><?php echo round(($kw_sum5 * 100 / $tj_sum), 2); ?></td>
							</tr>
							<?php } ?>
					</table>
				</div>
			</div>

			<div class="container" align="center">
				<h3>สาเตุที่ยังไม่ได้ทำงาน</h3>
			</div>
			<div id="no-more-tables">
				<div id="no-more-tables">
					<table class="table table-bordered">
							<tr>
								<th style="text-align: center;" rowspan="3">ลำดับ</th>
								<th style="text-align: center;" rowspan="3" colspan="">ระดับการศึกษา</th>
								<th style="text-align: center;" rowspan="3" colspan="">หลักสูตร</th>
								<th style="text-align: center;" rowspan="3" colspan="">ปีการศึกษา</th>
								<th style="text-align: center;" rowspan="3" colspan="">ผู้สำเร็จการศึกษา</th>
								<th style="text-align: center;" rowspan="" colspan="8">สาเตุที่ยังไม่ได้ทำงาน</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="2" colspan="">จำนวนผู้ตอบแบบสอบถาม</th>
								<th style="text-align: center;" rowspan="2" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="2">ต้องการไปหาประสบการณ์ชีวิตหลัง</th>
								<th style="text-align: center;" rowspan="" colspan="2">ยังไมม่ประสงค์ทำงาน</th>
								<th style="text-align: center;" rowspan="" colspan="2">รอฟังคำตอบจากหน่วยงาน</th>
								<th style="text-align: center;" rowspan="" colspan="2">หางานทำไม่ได้</th>
								<th style="text-align: center;" rowspan="" colspan="2">อื่นๆ</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
							</tr>
							<?php if($employment_datas == null){ ?>
								<tr>
									<td align="center" colspan="100">No Data </td>
								<tr>
							<?php }else { ?>
							<tr>
								<?php
									$rnw = 0;
									$rnw_main = 0;
									$rnw_total = 0;
									$rnw_sum1 = 0;
									$rnw_sum2 = 0;
									$rnw_sum3 = 0;
									$rnw_sum4 = 0;
									$rnw_sum5 = 0;
									foreach ($employment_datas as $employment_data){
									$rnw++;
								?>
								<td align="center"><?php echo $rnw;?></td>
								<td><?php echo $employment_data['Employment']['degree_name'];?></td>
								<td><?php echo $employment_data['Employment']['syllabus'];?></td>
								<td><?php echo $employment_data['Employment']['year'];?></td>
								<td><?php echo $employment_data['Employment']['graduate_total']; $rnw_total = $rnw_total + $employment_data['Employment']['graduate_total']?></td>
								<td><?php echo $employment_data['Employment']['rnw_respondents']; $rnw_main = $rnw_main + $employment_data['Employment']['rnw_respondents'];?></td>
								<td><?php echo round(($employment_data['Employment']['rnw_respondents'] *100 / $employment_data['Employment']['graduate_total']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['rnw_el_qty']; $rnw_sum1 = $rnw_sum1 + $employment_data['Employment']['rnw_el_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['rnw_el_qty'] *100 / $employment_data['Employment']['rnw_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['rnw_nw_qty']; $rnw_sum2 = $rnw_sum2 + $employment_data['Employment']['rnw_nw_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['rnw_nw_qty'] *100 / $employment_data['Employment']['rnw_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['rnw_wr_qty']; $rnw_sum3 = $rnw_sum3 + $employment_data['Employment']['rnw_wr_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['rnw_wr_qty'] *100 / $employment_data['Employment']['rnw_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['rnw_oow_qty']; $rnw_sum4 = $rnw_sum4 + $employment_data['Employment']['rnw_oow_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['rnw_oow_qty'] *100 / $employment_data['Employment']['rnw_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['rnw_ot_qty']; $rnw_sum5 = $rnw_sum5 + $employment_data['Employment']['rnw_ot_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['rnw_ot_qty'] *100 / $employment_data['Employment']['rnw_respondents']), 2); ?></td>
							</tr>
							<?php } ?>
							<tr>
								<td rowspan="" colspan="4">รวม</td>
								<td rowspan="" colspan=""><?php echo $rnw_total ?></td>
								<td rowspan="" colspan=""><?php echo $rnw_main ?></td>
								<td><?php echo round(($rnw_main * 100 / $rnw_total), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $rnw_sum1 ?></td>
								<td><?php echo round(($rnw_sum1 * 100 / $rnw_main), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $rnw_sum2 ?></td>
								<td><?php echo round(($rnw_sum2 * 100 / $rnw_main), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $rnw_sum3 ?></td>
								<td><?php echo round(($rnw_sum3 * 100 / $rnw_main), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $rnw_sum4 ?></td>
								<td><?php echo round(($rnw_sum4 * 100 / $rnw_main), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $rnw_sum5 ?></td>
								<td><?php echo round(($rnw_sum5 * 100 / $rnw_main), 2); ?></td>
							</tr>
							<?php } ?>
					</table>
				</div>
			</div>

			<div class="container" align="center">
				<h3>ปัญหาในการหางานทำหลังสำเร็จการศึกษา</h3>
			</div>
			<div id="no-more-tables">
				<div id="no-more-tables">
					<table class="table table-bordered">
							<tr>
								<th style="text-align: center;" rowspan="4">ลำดับ</th>
								<th style="text-align: center;" rowspan="4" colspan="">ระดับการศึกษา</th>
								<th style="text-align: center;" rowspan="4" colspan="">หลักสูตร</th>
								<th style="text-align: center;" rowspan="4" colspan="">ปีการศึกษา</th>
								<th style="text-align: center;" rowspan="4" colspan="">ผู้สำเร็จการศึกษา</th>
								<th style="text-align: center;" rowspan="" colspan="20">ปัญหาในการหางานทำหลังสำเร็จการศึกษา</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="3" colspan="0">ไม่มีปัญหา</th>
								<th style="text-align: center;" rowspan="3" colspan="">จำนวนผู้ประสบปัญหาทั้ง</th>
								<th style="text-align: center;" rowspan="" colspan="18">มีปัญหา</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="" colspan="2">ไม่ทราบแหล่งงาน</th>
								<th style="text-align: center;" rowspan="" colspan="2">หางานที่ถูกใจไม่ได้</th>
								<th style="text-align: center;" rowspan="" colspan="2">ขาดคนหรือเงินค้ำประกัน</th>
								<th style="text-align: center;" rowspan="" colspan="2">หน่วยงานไม่ต้องการ</th>
								<th style="text-align: center;" rowspan="" colspan="2">สอบเข้าทำงานไม่ได้</th>
								<th style="text-align: center;" rowspan="" colspan="2">ปัญหาด้านสุขภาพ</th>
								<th style="text-align: center;" rowspan="" colspan="2">ขาดทัักษะที่จำเป็นสำหรับตำแหน่ง</th>
								<th style="text-align: center;" rowspan="" colspan="2">อื่นๆ</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
							</tr>
							<?php if($employment_datas == null){ ?>
								<tr>
									<td align="center" colspan="100">No Data </td>
								<tr>
							<?php }else { ?>
							<tr>
								<?php
									$pfw_respondent = 0;
									$pfw = 0;
									$pfw_none_problem = 0;
									$pfw_total = 0;
									$pfw_have_problem1 = 0;
									$pfw_have_problem2 = 0;
									$pfw_have_problem3 = 0;
									$pfw_have_problem4 = 0;
									$pfw_have_problem5 = 0;
									$pfw_have_problem6 = 0;
									$pfw_have_problem7 = 0;
									$pfw_have_problem8 = 0;
									$pfw_have_problem9 = 0;
									foreach ($employment_datas as $employment_data){
									$pfw++;
								?>
								<td align="center"><?php echo $pfw;?></td>
								<td><?php echo $employment_data['Employment']['degree_name'];?></td>
								<td><?php echo $employment_data['Employment']['syllabus'];?></td>
								<td><?php echo $employment_data['Employment']['year'];?></td>
								<td><?php echo $employment_data['Employment']['graduate_total']; $pfw_total = $pfw_total + $employment_data['Employment']['graduate_total'];?></td>
								<td><?php echo $employment_data['Employment']['pfw_np']; $pfw_none_problem = $pfw_none_problem + $employment_data['Employment']['pfw_np'];?></td>
								<td><?php echo $employment_data['Employment']['pfw_sum']; $pfw_have_problem9 = $pfw_have_problem9 + $employment_data['Employment']['pfw_sum'];?></td>
								<td><?php echo $employment_data['Employment']['pfw_losr_qty']; $pfw_have_problem1 = $pfw_have_problem1 + $employment_data['Employment']['pfw_losr_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['pfw_losr_qty'] *100 / $employment_data['Employment']['pfw_sum']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['pfw_uw_qty']; $pfw_have_problem2 = $pfw_have_problem2 + $employment_data['Employment']['pfw_uw_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['pfw_uw_qty'] *100 / $employment_data['Employment']['pfw_sum']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['pfw_log_qty']; $pfw_have_problem3 = $pfw_have_problem3 + $employment_data['Employment']['pfw_log_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['pfw_log_qty'] *100 / $employment_data['Employment']['pfw_sum']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['pfw_aa_qty']; $pfw_have_problem4 = $pfw_have_problem4 + $employment_data['Employment']['pfw_aa_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['pfw_aa_qty'] *100 / $employment_data['Employment']['pfw_sum']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['pfw_wt_qty']; $pfw_have_problem5 = $pfw_have_problem5 + $employment_data['Employment']['pfw_wt_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['pfw_wt_qty'] *100 / $employment_data['Employment']['pfw_sum']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['pfw_hp_qty']; $pfw_have_problem6 = $pfw_have_problem6 + $employment_data['Employment']['pfw_hp_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['pfw_hp_qty'] *100 / $employment_data['Employment']['pfw_sum']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['pfw_los_qty']; $pfw_have_problem7 = $pfw_have_problem7 + $employment_data['Employment']['pfw_los_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['pfw_los_qty'] *100 / $employment_data['Employment']['pfw_sum']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['pfw_ot_qty']; $pfw_have_problem8 = $pfw_have_problem8 + $employment_data['Employment']['pfw_ot_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['pfw_ot_qty'] *100 / $employment_data['Employment']['pfw_sum']), 2); ?></td>
							</tr>
							<?php } ?>
							<tr>
								<td colspan="4" rowspan="" style="text-align: center;">รวม</td>
								<td><?php echo $pfw_total ?></td>
								<td><?php echo $pfw_none_problem ?></td>
								<td><?php echo $pfw_have_problem9 ?></td>
								<td><?php echo $pfw_have_problem1 ?></td>
								<td><?php echo round(($pfw_have_problem1 * 100 / $pfw_have_problem9), 2); ?></td>
								<td><?php echo $pfw_have_problem2 ?></td>
								<td><?php echo round(($pfw_have_problem2 * 100 / $pfw_have_problem9), 2); ?></td>
								<td><?php echo $pfw_have_problem3 ?></td>
								<td><?php echo round(($pfw_have_problem3 * 100 / $pfw_have_problem9), 2); ?></td>
								<td><?php echo $pfw_have_problem4 ?></td>
								<td><?php echo round(($pfw_have_problem4 * 100 / $pfw_have_problem9), 2); ?></td>
								<td><?php echo $pfw_have_problem5 ?></td>
								<td><?php echo round(($pfw_have_problem5 * 100 / $pfw_have_problem9), 2); ?></td>
								<td><?php echo $pfw_have_problem6 ?></td>
								<td><?php echo round(($pfw_have_problem6 * 100 / $pfw_have_problem9), 2); ?></td>
								<td><?php echo $pfw_have_problem7 ?></td>
								<td><?php echo round(($pfw_have_problem7 * 100 / $pfw_have_problem9), 2); ?></td>
								<td><?php echo $pfw_have_problem8 ?></td>
								<td><?php echo round(($pfw_have_problem8 * 100 / $pfw_have_problem9), 2); ?></td>
							</tr>
							<?php } ?>
					</table>
				</div>
			</div>

			<div class="container" align="center">
				<h3>ความต้องการศึกษาต่อ</h3>
			</div>
			<div id="no-more-tables">
				<div id="no-more-tables">
					<table class="table table-bordered">
							<tr>
								<th style="text-align: center;" rowspan="4">ลำดับ</th>
								<th style="text-align: center;" rowspan="4" colspan="">ระดับการศึกษา</th>
								<th style="text-align: center;" rowspan="4" colspan="">หลักสูตร</th>
								<th style="text-align: center;" rowspan="4" colspan="">ปีการศึกษา</th>
								<th style="text-align: center;" rowspan="4" colspan="">ผู้สำเร็จการศึกษา</th>
								<th style="text-align: center;" rowspan="" colspan="10">ความต้องการศึกษาต่อ</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="" colspan="5">ทำงานแล้ว</th>
								<th style="text-align: center;" rowspan="" colspan="5">ยังไม่ได้ทำงาน</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="" colspan="2">ต้องการศึกษาต่อ</th>
								<th style="text-align: center;" rowspan="" colspan="2">ไม่ต้องการศึกษาต่อ</th>
								<th style="text-align: center;" rowspan="2" colspan="">จำนวนผู้ตอบแบบสอบถาม</th>
								<th style="text-align: center;" rowspan="" colspan="2">ต้องการศึกษาต่อ</th>
								<th style="text-align: center;" rowspan="" colspan="2">ไม่ต้องการศึกษาต่อ</th>
								<th style="text-align: center;" rowspan="2" colspan="">จำนวนผู้ตอบแบบสอบถาม</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
							</tr>
							<?php if($employment_datas == null){ ?>
								<tr>
									<td align="center" colspan="100">No Data </td>
								<tr>
							<?php }else { ?>
							<tr>
								<?php
									$ce = 0;
									$ce_study1 = 0;
									$ce_study2 = 0;
									$ce_none_study1 = 0;
									$ce_none_study2= 0;
									$ce_total1 = 0;
									$ce_total2 = 0;
									$ce_main = 0;
									foreach ($employment_datas as $employment_data){
									$ce++;
								?>
								<td align="center"><?php echo $ce;?></td>
								<td><?php echo $employment_data['Employment']['degree_name'];?></td>
								<td><?php echo $employment_data['Employment']['syllabus'];?></td>
								<td><?php echo $employment_data['Employment']['year'];?></td>
								<td><?php echo $employment_data['Employment']['graduate_total']; $ce_main = $ce_main + $employment_data['Employment']['graduate_total']?></td>
								<td><?php echo $employment_data['Employment']['ce_aw_wce_qty']; $ce_study1 = $ce_study1 + $employment_data['Employment']['ce_aw_wce_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['ce_aw_wce_qty'] *100 / $employment_data['Employment']['ce_aw_sum']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['ce_aw_nce_qty']; $ce_none_study1 = $ce_none_study1 + $employment_data['Employment']['ce_aw_nce_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['ce_aw_nce_qty'] *100 / $employment_data['Employment']['ce_aw_sum']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['ce_aw_sum']; $ce_total1 = $ce_total1 + $employment_data['Employment']['ce_aw_sum'];?></td>
								<td><?php echo $employment_data['Employment']['ce_nw_wce_qty']; $ce_study2 = $ce_study2 + $employment_data['Employment']['ce_nw_wce_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['ce_nw_wce_qty'] *100 / $employment_data['Employment']['ce_nw_sum']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['ce_nw_nce_qty']; $ce_none_study2 = $ce_none_study2 + $employment_data['Employment']['ce_nw_nce_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['ce_nw_nce_qty'] *100 / $employment_data['Employment']['ce_nw_sum']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['ce_nw_sum']; $ce_total2 = $ce_total2 + $employment_data['Employment']['ce_nw_sum'];?></td>
							</tr>
							<?php } ?>
							<tr>
								<td rowspan="" colspan="4" style="text-align:center;">รวม</td>
								<td rowspan="" colspan="" ><?php echo $ce_main ?></td>
								<td rowspan="" colspan="" ><?php echo $ce_study1 ?></td>
								<td rowspan="" colspan="" ><?php echo round(($ce_study1 * 100 / $ce_total1), 2); ?></td>
								<td rowspan="" colspan="" ><?php echo $ce_none_study1 ?></td>
								<td rowspan="" colspan="" ><?php echo round(($ce_none_study1 * 100 / $ce_total1), 2); ?></td>
								<td rowspan="" colspan="" ><?php echo $ce_total1 ?></td>
								<td rowspan="" colspan="" ><?php echo $ce_study2 ?></td>
								<td rowspan="" colspan="" ><?php echo round(($ce_study2 * 100 / $ce_total2), 2); ?></td>
								<td rowspan="" colspan="" ><?php echo $ce_none_study2 ?></td>
								<td rowspan="" colspan="" ><?php echo round(($ce_none_study2 * 100 / $ce_total2), 2); ?></td>
								<td rowspan="" colspan="" ><?php echo $ce_total2 ?></td>
							</tr>
							<?php } ?>
					</table>
				</div>
			</div>

			<div class="container" align="center">
				<h3>ระดับการศึกษาที่ต้องการศึกษาต่อ/กำลังศึกษาต่อ</h3>
			</div>
			<div id="no-more-tables">
				<div id="no-more-tables">
					<table class="table table-bordered">
							<tr>
								<th style="text-align: center;" rowspan="3">ลำดับ</th>
								<th style="text-align: center;" rowspan="3" colspan="">ระดับการศึกษา</th>
								<th style="text-align: center;" rowspan="3" colspan="">หลักสูตร</th>
								<th style="text-align: center;" rowspan="3" colspan="">ปีการศึกษา</th>
								<th style="text-align: center;" rowspan="3" colspan="">ผู้สำเร็จการศึกษา</th>
								<th style="text-align: center;" rowspan="" colspan="11">ระดับการศึกษาที่ต้องการศึกษาต่อ/กำลังศึกษาต่อ</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="2" colspan="">จำนวนผู้ตอบแบบสอบถาม</th>
								<th style="text-align: center;" rowspan="" colspan="2">ระดับปริญญาตรี</th>
								<th style="text-align: center;" rowspan="" colspan="2">ระดับปริญญาโท</th>
								<th style="text-align: center;" rowspan="" colspan="2">ระดับปริญญาเอก</th>
								<th style="text-align: center;" rowspan="" colspan="2">ระดับปริญญาบัณฑิต</th>
								<th style="text-align: center;" rowspan="" colspan="2">ระดับปริญญาบัณฑิตชั้นสูง</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
							</tr>
							
							<?php if($employment_datas == null){ ?>
								<tr>
									<td align="center" colspan="100">No Data </td>
								<tr>
							<?php }else { ?>
							<tr>
								<?php
									$elt = 0;
									$elt_total = 0;
									$elt_sum1 = 0;
									$elt_sum2 = 0;
									$elt_sum3 = 0;
									$elt_sum4 = 0;
									$elt_sum5 = 0;
									$elt_sum6 = 0;
									foreach ($employment_datas as $employment_data){
									$elt++;
								?>
								<td align="center"><?php echo $elt;?></td>
								<td><?php echo $employment_data['Employment']['degree_name'];?></td>
								<td><?php echo $employment_data['Employment']['syllabus'];?></td>
								<td><?php echo $employment_data['Employment']['year'];?></td>
								<td><?php echo $employment_data['Employment']['graduate_total']; $elt_total = $elt_total + $employment_data['Employment']['graduate_total']?></td>
								<td><?php echo $employment_data['Employment']['elt_respondents']; $elt_sum1 = $elt_sum1 + $employment_data['Employment']['elt_respondents'];?></td>
								<td><?php echo $employment_data['Employment']['elt_bd_qty']; $elt_sum2 = $elt_sum2 + $employment_data['Employment']['elt_bd_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['elt_bd_qty'] *100 / $employment_data['Employment']['elt_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['elt_md_qty']; $elt_sum2 = $elt_sum2 + $employment_data['Employment']['elt_md_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['elt_md_qty'] *100 / $employment_data['Employment']['elt_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['elt_dd_qty']; $elt_sum3 = $elt_sum3 + $employment_data['Employment']['elt_dd_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['elt_dd_qty'] *100 / $employment_data['Employment']['elt_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['elt_gd_qty']; $elt_sum4 = $elt_sum4 + $employment_data['Employment']['elt_gd_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['elt_gd_qty'] *100 / $employment_data['Employment']['elt_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['elt_hgd_qty']; $elt_sum6 = $elt_sum6 + $employment_data['Employment']['elt_hgd_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['elt_hgd_qty'] *100 / $employment_data['Employment']['elt_respondents']), 2); ?></td>
							</tr>
							<?php } ?>
							<tr>
								<td rowspan="" colspan="4" style="text-align:center;">รวม</td>
								<td rowspan="" colspan=""><?php echo $elt_total ?></td>
								<td rowspan="" colspan=""><?php echo $elt_sum1 ?></td>
								<td rowspan="" colspan=""><?php echo $elt_sum2 ?></td>
								<td><?php echo round(($elt_sum2 * 100 / $elt_total), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $elt_sum3 ?></td>
								<td><?php echo round(($elt_sum3 * 100 / $elt_total), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $elt_sum4 ?></td>
								<td><?php echo round(($elt_sum4 * 100 / $elt_total), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $elt_sum5 ?></td>
								<td><?php echo round(($elt_sum5 * 100 / $elt_total), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $elt_sum6 ?></td>
								<td><?php echo round(($elt_sum6 * 100 / $elt_total), 2); ?></td>
							</tr>
							<?php } ?>
					</table>
				</div>
			</div>

			<div class="container" align="center">
				<h3>ระดับการศึกษาที่ต้องการศึกษาต่อ</h3>
			</div>
			<div id="no-more-tables">
				<div id="no-more-tables">
					<table class="table table-bordered">
							<tr>
								<th style="text-align: center;" rowspan="3">ลำดับ</th>
								<th style="text-align: center;" rowspan="3" colspan="">ระดับการศึกษา</th>
								<th style="text-align: center;" rowspan="3" colspan="">หลักสูตร</th>
								<th style="text-align: center;" rowspan="3" colspan="">ปีการศึกษา</th>
								<th style="text-align: center;" rowspan="3" colspan="">ผู้สำเร็จการศึกษา</th>
								<th style="text-align: center;" rowspan="" colspan="8">ระดับการศึกษาที่ต้องการศึกษาต่อ</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="2" colspan="">จำนวนผู้ตอบแบบสอบถาม</th>
								<th style="text-align: center;" rowspan="2" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="2">สาขาวิชาเดิม</th>
								<th style="text-align: center;" rowspan="" colspan="2">สาขาอื่น</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
							</tr>
							<?php if($employment_datas == null){ ?>
								<tr>
									<td align="center" colspan="100">No Data </td>
								<tr>
							<?php }else { ?>
							<tr>
								<?php
									$fcs = 0;
									$fcs_sum1 = 0;
									$fcs_sum2 = 0;
									$fcs_main = 0;
									$fcs_total = 0;
									foreach ($employment_datas as $employment_data){
									$elt++;
								?>
								<td align="center"><?php echo $fcs;?></td>
								<td><?php echo $employment_data['Employment']['degree_name'];?></td>
								<td><?php echo $employment_data['Employment']['syllabus'];?></td>
								<td><?php echo $employment_data['Employment']['year'];?></td>
								<td><?php echo $employment_data['Employment']['graduate_total']; $fcs_total = $fcs_total + $employment_data['Employment']['graduate_total']?></td>
								<td><?php echo $employment_data['Employment']['fcs_respondents']; $fcs_main = $fcs_main + $employment_data['Employment']['fcs_respondents'];?></td>
								<td><?php echo round(($employment_data['Employment']['fcs_respondents'] *100 / $employment_data['Employment']['graduate_total']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['fcs_orfs_qty']; $fcs_sum1 = $fcs_sum1 + $employment_data['Employment']['fcs_orfs_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['fcs_orfs_qty'] *100 / $employment_data['Employment']['graduate_total']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['fcs_otfs_qty']; $fcs_sum2 = $fcs_sum2 + $employment_data['Employment']['fcs_otfs_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['fcs_otfs_qty'] *100 / $employment_data['Employment']['graduate_total']), 2); ?></td>
							</tr>
							<?php } ?>
							<tr>
								<td rowspan="" colspan="4" style="text-align:center;">รวม</td>
								<td rowspan="" colspan=""><?php echo $fcs_total ?></td>
								<td rowspan="" colspan=""><?php echo $fcs_main ?></td>
								<td><?php echo round(($fcs_main * 100 / $fcs_total), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $fcs_sum1 ?></td>
								<td><?php echo round(($fcs_sum1 * 100 / $elt_total), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $fcs_sum2 ?></td>
								<td><?php echo round(($fcs_sum2 * 100 / $elt_total), 2); ?></td>
							</tr>
							<?php } ?>
					</table>
				</div>
			</div>

			<div class="container" align="center">
				<h3>เหตุผลในการศึกษาต่อ</h3>
			</div>
			<div id="no-more-tables">
				<div id="no-more-tables">
					<table class="table table-bordered">
							<tr>
								<th style="text-align: center;" rowspan="3">ลำดับ</th>
								<th style="text-align: center;" rowspan="3" colspan="">ระดับการศึกษา</th>
								<th style="text-align: center;" rowspan="3" colspan="">หลักสูตร</th>
								<th style="text-align: center;" rowspan="3" colspan="">ปีการศึกษา</th>
								<th style="text-align: center;" rowspan="3" colspan="">ผู้สำเร็จการศึกษา</th>
								<th style="text-align: center;" rowspan="" colspan="12">เหตุผลในการศึกษาต่อ</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="2" colspan="">จำนวนผู้ตอบแบบสอบถาม</th>
								<th style="text-align: center;" rowspan="2" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="2">ความต้องการของบิดา/มารดา</th>
								<th style="text-align: center;" rowspan="" colspan="2">ความต้องการของตนเอง</th>
								<th style="text-align: center;" rowspan="" colspan="2">งานที่ต้องการต้องใช้วุฒิสูงกว่า</th>
								<th style="text-align: center;" rowspan="" colspan="2">ได้รับทุนการศึกษาต่อ</th>
								<th style="text-align: center;" rowspan="" colspan="2">อื่นๆ</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">จำนวน</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
							</tr>
							<?php if($employment_datas == null){ ?>
								<tr>
									<td align="center" colspan="100">No Data </td>
								<tr>
							<?php }else { ?>
							<tr>
								<?php
									$rce = 0;
									$rce_main = 0;
									$rce_total = 0;
									$rce_sum1 = 0;
									$rce_sum2 = 0;
									$rce_sum3 = 0;
									$rce_sum4 = 0;
									$rce_sum5 = 0;
									foreach ($employment_datas as $employment_data){
									$rce++;
								?>
								<td align="center"><?php echo $rce;?></td>
								<td><?php echo $employment_data['Employment']['degree_name'];?></td>
								<td><?php echo $employment_data['Employment']['syllabus'];?></td>
								<td><?php echo $employment_data['Employment']['year'];?></td>
								<td><?php echo $employment_data['Employment']['graduate_total']; $rce_total = $rce_total + $employment_data['Employment']['graduate_total']?></td>
								<td><?php echo $employment_data['Employment']['rce_respondents']; $rce_main = $rce_main + $employment_data['Employment']['rce_respondents'];?></td>
								<td><?php echo round(($employment_data['Employment']['rce_respondents'] *100 / $employment_data['Employment']['graduate_total']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['rce_pr_qty']; $rce_sum1 = $rce_sum1 + $employment_data['Employment']['rce_pr_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['rce_pr_qty'] *100 / $employment_data['Employment']['rce_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['rce_self_qty']; $rce_sum2 = $rce_sum2 + $employment_data['Employment']['rce_self_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['rce_self_qty'] *100 / $employment_data['Employment']['rce_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['rce_dj_qty']; $rce_sum3 = $rce_sum3 + $employment_data['Employment']['rce_dj_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['rce_dj_qty'] *100 / $employment_data['Employment']['rce_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['rce_ss_qty']; $rce_sum4 = $rce_sum4 + $employment_data['Employment']['rce_ss_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['rce_ss_qty'] *100 / $employment_data['Employment']['rce_respondents']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['rce_ot_qty']; $rce_sum5 = $rce_sum5 + $employment_data['Employment']['rce_ot_qty'];?></td>
								<td><?php echo round(($employment_data['Employment']['rce_ot_qty'] *100 / $employment_data['Employment']['rce_respondents']), 2); ?></td>
								
							</tr>
							<?php } ?>
							<tr>
								<td rowspan="" colspan="4" style="text-align:center;">รวม</td>
								<td rowspan="" colspan=""><?php echo $rce_total ?></td>
								<td rowspan="" colspan=""><?php echo $rce_main ?></td>
								<td><?php echo round(($rce_main * 100 / $rce_total), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $rce_sum1 ?></td>
								<td><?php echo round(($rce_sum1 * 100 / $rce_main), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $rce_sum2 ?></td>
								<td><?php echo round(($rce_sum2 * 100 / $rce_main), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $rce_sum3 ?></td>
								<td><?php echo round(($rce_sum3 * 100 / $rce_main), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $rce_sum4 ?></td>
								<td><?php echo round(($rce_sum4 * 100 / $rce_main), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $rce_sum5 ?></td>
								<td><?php echo round(($rce_sum5 * 100 / $rce_main), 2); ?></td>
							</tr>
							<?php } ?>
					</table>
				</div>
			</div>

			<div class="container" align="center">
				<h3>เงินเดือนหรือรายได้เฉลี่ยต่อเดือน</h3>
			</div>
			<div id="no-more-tables">
				<div id="no-more-tables">
					<table class="table table-bordered">
							<tr>
								<th style="text-align: center;" rowspan="2">ลำดับ</th>
								<th style="text-align: center;" rowspan="2" colspan="">ระดับการศึกษา</th>
								<th style="text-align: center;" rowspan="2" colspan="">หลักสูตร</th>
								<th style="text-align: center;" rowspan="2" colspan="">ปีการศึกษา</th>
								<th style="text-align: center;" rowspan="2" colspan="">ผู้สำเร็จการศึกษา</th>
								<th style="text-align: center;" rowspan="" colspan="10">เงินเดือนหรือรายได้เฉลี่ยต่อเดือน</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="" colspan="">จำนวนผู้ตอบแบบสอบถาม</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">เงินเดือนหรือรายได้รวม (บาท)</th>
								<th style="text-align: center;" rowspan="" colspan="">เงินเดือนหรือรายได้เฉลี่ย (บาท)</th>
								<th style="text-align: center;" rowspan="" colspan="">เงินเดือนหรือรายได้สูงสุด (บาท)</th>
								<th style="text-align: center;" rowspan="" colspan="">เงินเดือนหรือรายได้ต่ำสุด (บาท)</th>
							</tr>
							<?php if($employment_datas == null){ ?>
								<tr>
									<td align="center" colspan="100">No Data </td>
								<tr>
							<?php }else { ?>
							<tr>
								<?php
									$si = 0;
									$si_total = 0;
									$si_main = 0;
									$si_sum1 = 0;
									$si_sum2 = 0;
									$si_sum3 = 0;
									$si_sum4 = 0;
									foreach ($employment_datas as $employment_data){
									$si++;
								?>
								<td align="center"><?php echo $si;?></td>
								<td><?php echo $employment_data['Employment']['degree_name'];?></td>
								<td><?php echo $employment_data['Employment']['syllabus'];?></td>
								<td><?php echo $employment_data['Employment']['year'];?></td>
								<td><?php echo $employment_data['Employment']['graduate_total']; $si_total = $si_total + $employment_data['Employment']['graduate_total']?></td>
								<td><?php echo $employment_data['Employment']['si_respondents']; $si_main = $si_main + $employment_data['Employment']['si_respondents'];?></td>
								<td><?php echo round(($employment_data['Employment']['si_respondents'] *100 / $employment_data['Employment']['graduate_total']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['si_ti']; $si_sum1 = $si_sum1 + $employment_data['Employment']['si_ti'];?></td>
								<td><?php echo $employment_data['Employment']['si_ai']; $si_sum2 = $si_sum2 + $employment_data['Employment']['si_ai'];?></td>
								<td><?php echo $employment_data['Employment']['si_hi']; $si_sum3 = $si_sum3 + $employment_data['Employment']['si_hi'];?></td>
								<td><?php echo $employment_data['Employment']['si_li']; $si_sum4 = $si_sum4 + $employment_data['Employment']['si_li'];?></td>
							</tr>
							<?php } ?>
							<tr>
								<td rowspan="" colspan="4" style="text-align:center;">รวม</td>
								<td rowspan="" colspan=""><?php echo $si_total ?></td>
								<td rowspan="" colspan=""><?php echo $si_main ?></td>
								<td><?php echo round(($si_main * 100 / $si_total), 2); ?></td>
								<td rowspan="" colspan=""><?php echo $si_sum1 ?></td>
								<td rowspan="" colspan=""><?php echo $si_sum2 ?></td>
								<td rowspan="" colspan=""><?php echo $si_sum3 ?></td>
								<td rowspan="" colspan=""><?php echo $si_sum4 ?></td>
							</tr>
							<?php } ?>
					</table>
				</div>
			</div>

			<div class="container" align="center">
				<h3>ความพึงพอใจต่อหลักสูตรที่สำเร็จการศึกษา</h3>
			</div>
			<div id="no-more-tables">
				<div id="no-more-tables">
					<table class="table table-bordered">
							<tr>
								<th style="text-align: center;" rowspan="2">ลำดับ</th>
								<th style="text-align: center;" rowspan="2" colspan="">ระดับการศึกษา</th>
								<th style="text-align: center;" rowspan="2" colspan="">หลักสูตร</th>
								<th style="text-align: center;" rowspan="2" colspan="">ปีการศึกษา</th>
								<th style="text-align: center;" rowspan="2" colspan="">ผู้สำเร็จการศึกษา</th>
								<th style="text-align: center;" rowspan="" colspan="20">ความพึงพอใจต่อหลักสูตรที่สำเร็จการศึกษา</th>
							</tr>
							<tr>
								<th style="text-align: center;" rowspan="" colspan="">จำนวนผู้ตอบแบบสอบถาม</th>
								<th style="text-align: center;" rowspan="" colspan="">ร้อยละ</th>
								<th style="text-align: center;" rowspan="" colspan="">มีความทันสมัยทันต่อสภาวะการณ์ในปัจจุบัน</th>
								<th style="text-align: center;" rowspan="" colspan="">รายวิชามีความเหมาะสม และเพียงพอต่อการนำไปประยุกต์ใช้ในการปฏิบัติงานได้</th>
								<th style="text-align: center;" rowspan="" colspan="">ห้องปฏิบัติการและอุปกรณ์การเรียนการสอนมีความเหมาะสม และปริมาณที่เพียงพอ</th>
								<th style="text-align: center;" rowspan="" colspan="">การจัดกิจกรรมเสริมหลักสูตรสอดคล้องกับสาขาวิชาที่เรียน</th>
								<th style="text-align: center;" rowspan="" colspan="">อาจารย์มีคุณวุฒิเหมาะสมกับหลักสูตรที่สอน มีความรู้ความสามารถ ความเชี่ยวชาญ และประสบการณ์ในเนื้อหาวิชาที่สอน</th>
								<th style="text-align: center;" rowspan="" colspan="">อาจารย์มีเทคนิคการถ่ายทอดความรู้ และนำสื่อเทคโนโลยีมาใช้ประกอบการเรียนการสอน</th>
								<th style="text-align: center;" rowspan="" colspan="">อาจารย์ที่ปรึกษา ติดตาม ดูแลให้คำปรึกษาระหว่างเรียนในหลักสูตรอย่างใกล้ชิด</th>
								<th style="text-align: center;" rowspan="" colspan="">ระบบสนับสนุนช่วยเหลือและจัดสวัสดิการแก่นักศึกษา มีความเพียงพอ เหมาะสม</th>
								<th style="text-align: center;" rowspan="" colspan="">การสื่อสารและให้ข้อมูลที่จำเป็นแก่นักศึกษา มีความรวดเร็ว ทันสมัย เข้าถึงได้ง่าย ทั่วถึง</th>
								<th style="text-align: center;" rowspan="" colspan="">ระบบ SIS (Student Information System) และฐานข้อมูลต่าง ๆ สำหรับการส่งเสริมการจัดการเรียนการสอนที่มีประสิทธิภาพ</th>
								<th style="text-align: center;" rowspan="" colspan="">การจัดภูมิทัศน์สภาพแวดล้อมโดยรอบมหาวิทยาลัย มีความปลอดภัย ร่มรื่น สวยงาม สะอาด และเหมาะสม</th>
								<th style="text-align: center;" rowspan="" colspan="">ค่าเฉลี่ยรวม</th>
							</tr>
							<?php if($employment_datas == null){ ?>
								<tr>
									<td align="center" colspan="100">No Data </td>
								<tr>
							<?php }else { ?>
							<tr>
								<?php
									$gcs = 0;
									$gcs_total = 0;
									$gcs_main = 0;
									foreach ($employment_datas as $employment_data){
									$gcs++;
								?>
								<td align="center"><?php echo $gcs;?></td>
								<td><?php echo $employment_data['Employment']['degree_name'];?></td>
								<td><?php echo $employment_data['Employment']['syllabus'];?></td>
								<td><?php echo $employment_data['Employment']['year'];?></td>
								<td><?php echo $employment_data['Employment']['graduate_total']; $gcs_total = $gcs_total + $employment_data['Employment']['graduate_total']?></td>
								<td><?php echo $employment_data['Employment']['gcs_respondents']; $gcs_main = $gcs_main + $employment_data['Employment']['gcs_respondents'];?></td>
								<td><?php echo round(($employment_data['Employment']['gcs_respondents'] *100 / $employment_data['Employment']['graduate_total']), 2); ?></td>
								<td><?php echo $employment_data['Employment']['gcs_md'];?></td>
								<td><?php echo $employment_data['Employment']['gcs_ap'];?></td>
								<td><?php echo $employment_data['Employment']['gcs_eq'];?></td>
								<td><?php echo $employment_data['Employment']['gcs_at'];?></td>
								<td><?php echo $employment_data['Employment']['gcs_iq'];?></td>
								<td><?php echo $employment_data['Employment']['gcs_it'];?></td>
								<td><?php echo $employment_data['Employment']['gcs_av'];?></td>
								<td><?php echo $employment_data['Employment']['gcs_wf'];?></td>
								<td><?php echo $employment_data['Employment']['gcs_ci'];?></td>
								<td><?php echo $employment_data['Employment']['gcs_sis'];?></td>
								<td><?php echo $employment_data['Employment']['gcs_ev'];?></td>
								<td><?php echo $employment_data['Employment']['gcs_average'];?></td>
							</tr>
							<?php } ?>
							<?php } ?>
					</table>
				</div>
			</div>

	</div>
</div>