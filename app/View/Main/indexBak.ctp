<?php
function DateThai($strDate)
{

	$strYear = date("Y", strtotime($strDate)) + 543;
	$strMonth = date("n", strtotime($strDate));
	$strDay = date("j", strtotime($strDate));
	$strHour = date("H", strtotime($strDate));
	$strMinute = date("i", strtotime($strDate));
	$strSeconds = date("s", strtotime($strDate));
	$strMonthCut = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
	$strMonthThai = $strMonthCut[$strMonth];
	return "$strDay $strMonthThai $strYear";
	//, $strHour:$strMinute
}
?>





<?php
date_default_timezone_set('Asia/Bangkok');
$date = date("Y-m-d");
$time = date("H:i:s");


function DateDiff($strDate1, $strDate2)
{
	return (strtotime($strDate2) - strtotime($strDate1)) /  (60 * 60 * 24);  // 1 day = 60*60*24
}

function TimeDiff($strTime1, $strTime2)
{
	return (strtotime($strTime2) - strtotime($strTime1)) /  (60 * 60); // 1 Hour =  60*60                           
}




?>
<div class="container-fluid" style="padding:15px 0 0;">
	<div class="panel panel-default" style="border-color: #F2F2F2!important">
		<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center>
						<div class="font_panel2">กิจกรรมนักเรียนและนักศึกษา</div>
					</center>
				</h3>
			</div>
		</div>
		<div class="panel-body" style="padding-top: 15px; ">
			<div align="center">
				<h4>กำหนดการกิจกรรมนักเรียนและนักศึกษา</h4>
				<h4>ในงานวันเกษตรแห่งชาติ 2567</h4>
				<h4>ในระหว่างวันที่ 27 พฤศจิกายน - 8 ธันวาคม 2567</h4>
				<br>
			</div>
			<div id="no-more-tables">
				<center>
					<h3>กิจกรรมนักเรียน</h3>
				</center>
				<div id="no-more-tables">
					<table class="table-responsive-full sort-table">
						<thead>
							<tr>
								<th style="text-align: center;">ลำดับ</th>
								<th style="text-align: center;">ชื่อกิจกรรม</th>
								<th style="text-align: center;" width="8%">วันที่/เวลา</th>
								<th style="text-align: center;" width="15%">วันที่รับสมัคร</th>

								<th style="text-align: center;">สถานที่</th>
								<th style="text-align: center;">คงเหลือจำนวนที่รับสมัคร (ทีม)</th>
								<th style="text-align: center;">ลงทะเบียน</th>
								<th style="text-align: center;">ตรวจสอบการสมัคร</th>
								<?php
								if (!isset($UserName)) {
								?>
									<div class="row">
										<div class="col-md-12">
											<?php
											if (isset($UserName)) {
											?>
												<th style="text-align: center;">ปริ้นท์รายชื่อ </th>
											<?php
											} else {
											?>

											<?php
											}
											?>

										</div>
									</div>
								<?php
								}
								?>

							</tr>
						</thead>
						<tbody>
							<?php
							$i = 0;
							foreach ($projects as $project) {
								$i++;
							?>
								<tr>
									<td data-title="ลำดับ" data-label="ลำดับ"><?php echo $i; ?></td>
									<td data-title="ชื่อกิจกรรม" data-label="ชื่อกิจกรรม">
										<?php
										if ($project['Project']['file_name'] != null) {
										?>
											<a href="<?php echo $this->Html->url('/files/' . $project['Project']['file_name']); ?>" target="_blank">
												<?php echo $project['Project']['name']; ?>
											</a>
										<?php

										} else {
											echo $project['Project']['name'];
										}


										?>

									</td>
									<td data-title="วันที่/เวลา" data-label="วันที่/เวลา"><?php echo $project['Project']['date']; ?><br><?php echo $project['Project']['time']; ?></td>
									<?php if (DateDiff($date . " " . $time, $project['Project']['dateout'] . " " . $project['Project']['timestop']) > 0) { ?>
										<td data-title="วันที่รับสมัคร" data-label="วันที่รับสมัคร">รับสมัครวันนี้ จนถึง วันที่ <font color="red">
												<?php
												$time1 = strtotime($project['Project']['dateout']);
												$newformat1 = date('d-M-Y', $time1);

												echo DateThai($newformat1)  ?> เวลา <?php echo date('H.i', strtotime($project['Project']['timestop']));
																					?> น.</font>
										</td>
									<?php } else { ?>
										<td data-title="ลงทะเบียน" data-label="ลงทะเบียน">
											<button class="btn btn-danger " style="cursor: none;">
												<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
												ปิดรับการลงทะเบียน

											</button>
										</td>
									<?php } ?>

									<td data-title="สถานที่" data-label="สถานที่">
										<a href="<?php echo $project['Project']['link']; ?>" target="_blank">
											<span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?php echo $project['Project']['place']; ?>
										</a>
									</td>
									<td data-title="คงเหลือจำนวนที่รับสมัคร (ทีม)" data-label="คงเหลือจำนวนที่รับสมัคร (ทีม)">
										<?php if (DateDiff($date . " " . $time, $project['Project']['dateout'] . " " . $project['Project']['timestop']) > 0) { ?>

											<?php if ($project['Project']['id'] == 2) { ?>
												<h4 align="center">
													<font color="purple"><?php echo ($countMember2s); ?></font>
												</h4>
											<?php } else { ?>
												<?php if ($project['Project']['max'] == 0) { ?>
													<h4 align="center">
														<font color="green">ไม่จำกัดจำนวน</font>
													</h4>
												<?php } else if ($project['Project']['id'] == 6) { ?>
													<div align='center'>
														<h4 align="left">
															<font color="green">ม.ต้น : <?php echo ($project['Project']['max'] - ${'teamCount' . $project['Project']['id'] . '1s'}[0][0]["count(distinct created)"]); ?></font>
															<br>
															<!-- <font color="green">ม.ปลาย : <?php echo ($project['Project']['max'] - ${'teamCount' . $project['Project']['id'] . '2s'}[0][0]["count(distinct created)"]); ?></font> -->
															<font color="red">ม.ปลาย: เต็ม</font>
														</h4>
													</div>
												<?php } else { ?>
													<h4 align="center">
														<!-- <font color="green"><?php echo ($countMembersList[$i - 1]); ?></font> -->
														<!-- <?php debug($teamCounts[0][0]["count(distinct created)"]); ?> -->
														<!-- <?php debug(${'teamCount' . $project['Project']['id'] . 's'}[0][0]["count(distinct created)"]); ?> -->
														<font color="green"><?php echo ($project['Project']['max'] - ${'teamCount' . $project['Project']['id'] . 's'}[0][0]["count(distinct created)"]); ?></font>
													</h4>
											<?php }
											} ?>
										<?php } else { ?>
											<h4 align="center">
												<font color="red">เต็ม</font>
											</h4>
										<?php } ?>

									</td>
									<?php if (((DateDiff($date . " " . $time, $project['Project']['dateout'] . " " . $project['Project']['timestop']) > 0) &&
										($project['Project']['max'] - ${'teamCount' . $project['Project']['id'] . 's'}[0][0]["count(distinct created)"]) > 0) || 
										$project['Project']['max']==0
										
									) { ?>

										<?php
										//debug($date." ".$time);
										//debug($listActivityTopic['ActivityTopic']['topic_dateout']." ".$listActivityTopic['ActivityTopic']['topic_timestop']);
										//if($countMembersList[$i - 1] < $listActivityTopic['ActivityTopic']['max']){
										?>
										<td data-title="ลงทะเบียน" style="text-align: center;">
											<?php if ($project['Project']['id'] == 2) { ?>
												<a class="btn btn-success" href="<?php echo $this->Html->url('http://www.agri.cmu.ac.th/smart_academic/student_mains/login_member_student'); ?>" role="button" target="_blank">ลงทะเบียน</a>
											<?php } else { ?>
												<a class="btn btn-primary" href="
												<?php echo $this->Html->url(array('action' => 'register', $project['Project']['id'])); ?>" role="button" target="_blank">ลงทะเบียน
												</a>
											<?php } ?>
										</td>
										<!-- <?php //} else{ 
												?>
                                        <td data-title="ลงทะเบียน">                                    
                                          <button class="btn btn-danger" style="cursor: none;">
                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 
                                            เต็ม						
                                          </button>
                                        </td>
                                        <?php //} 
										?> -->
									
									<?php } else { ?>
										<td data-title="ลงทะเบียน" style="text-align: center;">
											<button class="btn btn-danger " style="cursor: none;">
												<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
												ปิดรับการลงทะเบียน

											</button>
										</td>
									<?php } ?>
									<td data-title="ตรวจสอบรายชื่อ" style="text-align: center;">

										<?php if ($project['Project']['id'] == 2) { ?>
											<a class="btn btn-success" href="<?php echo $this->Html->url(array('action' => 'member_studentchecklist')); ?>" role="button" target="_blank">ตรวจสอบรายชื่อ</a>
										<?php } else { ?>

											<?php if (!isset($UserName)) { ?>
												<a class="btn btn-primary" href="<?php echo $this->Html->url(array('action' => 'login_checklist', $project['Project']['id'])); ?>" role="button" target="_blank">ตรวจสอบรายชื่อ</a>
											<?php } else { ?>
												<a class="btn btn-primary" href="<?php echo $this->Html->url(array('action' => 'adminchecklist', $project['Project']['id'])); ?>" role="button" target="_blank">ตรวจสอบรายชื่อรวม</a>
											<?php } ?>

										<?php } ?>
									</td>
									<?php if ($UserName != null) { ?>
										<td data-title="ปริ้นท์รายชื่อ" style="text-align: center;">
											<a class="btn btn-warning pull-right" href="<?php echo $this->Html->url(array('action' => 'list_print', $project['Project']['id'])); ?>" role="button" target="_blank"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Print รายชื่อ</a>
										</td>
									<?php } ?>


								</tr>
							<?php
							}
							?>

						</tbody>
					</table>
				</div>
				<center>
					<h3>กิจกรรมนักศึกษา</h3>
				</center>
				<div id="no-more-tables">
					<table class="table-responsive-full sort-table">
						<thead>
							<tr>
								<th style="text-align: center;">ลำดับ</th>
								<th style="text-align: center;">ชื่อกิจกรรม</th>
								<th style="text-align: center;" width="8%">วันที่/เวลา</th>
								<th style="text-align: center;" width="15%">วันที่รับสมัคร</th>

								<th style="text-align: center;">สถานที่</th>
								<th style="text-align: center;">จำนวนผู้ลงทะเบียน</th>
								<th style="text-align: center;">ลงทะเบียน</th>
								<th style="text-align: center;">ตรวจสอบการสมัคร</th>

							</tr>
						</thead>
						<tbody>
							<?php
							$i = 5;
							foreach ($MemberProjects as $project) {
								$i++;
							?>
								<tr>
									<td data-title="ลำดับ" align="center"><?php echo $i; ?></td>
									<td data-title="ชื่อกิจกรรม">
										<?php
										if ($project['MemberProject']['file_name'] != null) {
										?>
											<a href="<?php echo $this->Html->url('https://www.agri.cmu.ac.th/smart_academic/files/students/agricultural_expo/' . $project['MemberProject']['file_name']); ?>" target="_blank">
												<?php echo $project['MemberProject']['name']; ?>
											</a>
										<?php

										} else {
											echo $project['MemberProject']['name'];
										}


										?>

									</td>
									<td data-title="วันที่/เวลา"><?php echo $project['MemberProject']['date']; ?><br><?php echo $project['MemberProject']['time']; ?></td>
									<?php if (DateDiff($date . " " . $time, $project['MemberProject']['dateout'] . " " . $project['MemberProject']['timestop']) > 0) { ?>
										<td>รับสมัครวันนี้ จนถึง วันที่ <font color="red">
												<?php
												$time1 = strtotime($project['MemberProject']['dateout']);
												$newformat1 = date('d-M-Y', $time1);

												echo DateThai($newformat1)  ?> เวลา <?php echo date('H.i', strtotime($project['MemberProject']['timestop']));
																					?> น.</font>
										</td>
									<?php } else { ?>
										<td data-title="ลงทะเบียน">
											<button class="btn btn-danger " style="cursor: none;">
												<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
												ปิดรับการลงทะเบียน

											</button>
										</td>
									<?php } ?>

									<td data-title="สถานที่">
										<a href="<?php echo $project['MemberProject']['link']; ?>" target="_blank">
											<span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> <?php echo $project['MemberProject']['place']; ?>
										</a>
									</td>
									<td data-title="สถานที่">

										<h4 align="center">
											<font color="purple"><?php echo ($countMember2s); ?></font>
										</h4>

									</td>
									<?php if (DateDiff($date . " " . $time, $project['MemberProject']['dateout'] . " " . $project['MemberProject']['timestop']) > 0) { ?>


										<td data-title="ลงทะเบียน" style="text-align: center;">
											<a class="btn btn-success" href="<?php echo $this->Html->url('https://www.agri.cmu.ac.th/smart_academic/student_mains/login_member_student'); ?>" role="button" target="_blank">
												ลงทะเบียน
											</a>

										</td>

									<?php } else { ?>
										<td data-title="ลงทะเบียน" style="text-align: center;">
											<button class="btn btn-danger " style="cursor: none;">
												<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
												ปิดรับการลงทะเบียน

											</button>
										</td>
									<?php } ?>
									<td data-title="ตรวจสอบรายชื่อ" style="text-align: center;">
										<!-- <a class="btn btn-success" href="<?php echo $this->Html->url(array('action' => 'member_studentchecklist')); ?>" role="button" target="_blank"> -->
										<!-- <a class="btn btn-primary" href="<?php echo $this->Html->url('https://www.agri.cmu.ac.th/smart_academic/student_mains/login_member_student'); ?>" role="button" target="_blank">ตรวจสอบรายชื่อ</a> -->

										<?php if (!isset($UserName)) { ?>
											<a class="btn btn-primary" href="<?php echo $this->Html->url('https://www.agri.cmu.ac.th/smart_academic/student_mains/login_member_student'); ?>" role="button" target="_blank">ตรวจสอบรายชื่อ</a>
										<?php } else { ?>
											<a class="btn btn-primary" href="<?php echo $this->Html->url('https://www.agri.cmu.ac.th/kasetfairstudent/main/list_member_student_checklist'); ?>" role="button" target="_blank">ตรวจสอบรายชื่อ</a>
										<?php } ?>

									</td>
								</tr>
							<?php
							}
							?>

						</tbody>
					</table>
				</div>

			</div>

		</div>

		<div class="row">
			<div class="row">
				<div class="col-md-12">
					<div class="alert alert-info">
						ติดต่อสอบถาม : ฝ่ายกิจกรรมนักเรียน นักศึกษา โทรศัพท์ : 053-944641-4 ต่อ 117 (คุณสมโภชน์ อจิยจักร์) ในวันและเวลาราชการ
					</div>
				</div>
			</div>
		</div>

	</div>