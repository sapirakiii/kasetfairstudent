<div class="container" style="padding:15px 0 0;">
	<div class="panel panel-primary" >
		<div class="panel-heading"> 
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center>ประชุมและเสวนา</center>
				</h3> 
			</div>
		</div>	
		<div class="panel-body" style="padding-top: 15px; ">
			<div align="center"> 	
				<h4>กำหนดกิจกรรมฝ่ายประชุมและเสวนา</h4>
				<h4>ในงานเกษตรภาคเหนือ ครั้งที่ 8 “เกษตรทันสมัย เศรษฐกิจก้าวไกล ใส่ใจชุมชน”</h4>
				<h4>ในระหว่างวันที่ 8-12 พฤศจิกายน 2560</h4>
				<br>
			<div> 
			<div id="no-more-tables"> 				
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th style="text-align: center;">ลำดับ</th>
							<th style="text-align: center;">โครงการประชุมและเสวนา</th>
							<th style="text-align: center;">วันที่/เวลา</th>
							<th style="text-align: center;">ทีมวิทยากร/ทีมนักวิจัย</th>
							<th style="text-align: center;"></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td data-title="ลำดับ" align="center">1</td>
							<td data-title="โครงการประชุมและเสวนา">
								จุดประกายฝันคนรุ่นใหม่ สู่เกษตรกรไทย 4.0<br/>
								โดย:- นักศึกษาสาขาธุรกิจเกษตร (ภาคพิเศษ)<br/>
								ภาควิชาพัฒนาเศรษฐกิจการเกษตร คณะเกษตรศาสตร์
							</td>
							<td data-title="วันที่/เวลา">ห้องประชุมบุญวาส ลำเพาพงศ์<br/>8 พ.ย. 2560<br>(08.30-12.15 น.)</td>
							<td data-title="ทีมวิทยากร/ทีมนักวิจัย">
								1) กลุ่มเกษตรกรรุ่นใหม่เชียงราย <br/>
								2) เจ้าของแบรนด์กิ๋นลำกิ๋นดี<br/>
								3) คุณชัยนันท์ หาญยุทธ (บริษัท ฟูด เมกเกอร์)<br/>
								4) คุณธีรพงศ์ ทาหล้า (ธุรกิจผักดอยโอเค)
							</td>
							<td style="text-align: center;">								
								<a class="btn btn-primary" href="https://goo.gl/forms/d0wPN1nEnNj8BDkY2" role="button" target="_blank">ลงทะเบียน</a>
								<br><br>
								<a class="btn btn-success" href="https://drive.google.com/open?id=1B-tKJfHnvU4Wn2k4oj2PnBC3ksEXfQK_AlDPVH-MmrM" role="button" target="_blank">ตรวจสอบรายชื่อ</a>								
							</td>
						</tr>
						<tr>
							<td data-title="ลำดับ" align="center">2</td>
							<td data-title="โครงการประชุมและเสวนา">
								<a href="<?php echo $this->Html->url('/files/เกษตรสุขภาพดีวิถีล้านนา.pdf'); ?>" target="_blank">
									เกษตรสุขภาพดีวิถีล้านนา <br/>(พร้อมบริการตรวจสารพิษตกค้างในเลือด : ฟรี)
								</a>
								
							</td>
							<td data-title="วันที่/เวลา">ห้องประชุมบุญวาส ลำเพาพงศ์<br/>9 พ.ย. 2560<br>(10.00-12.00 น.)</td>
							<td data-title="ทีมวิทยากร/ทีมนักวิจัย">อ.ดร.ธนะชัย พันธ์เกษมสุข</td>
							<td style="text-align: center;">								
								<a class="btn btn-primary" href="https://goo.gl/forms/dt9n3bYwy4JQBmLF2" role="button" target="_blank">ลงทะเบียน</a>
								<br><br>
								<a class="btn btn-success" href="https://drive.google.com/open?id=1_RGFdC4lyCIVFIlftOLzAvdMefPvaLF106dSQpzBL-o" role="button" target="_blank">ตรวจสอบรายชื่อ</a>								
							</td>
						</tr>
						<tr>
							<td data-title="ลำดับ" align="center">3</td>
							<td data-title="โครงการประชุมและเสวนา">
								การพัฒนาเกษตรกรผู้เลี้ยงจิ้งหรีดเข้าสู่ระบบการปฏิบัติการทางเกษตรที่ดี<br/>(พร้อมแชร์ประสบการณ์ตรงจากผู้เลี้ยงจิ้งหรีดมืออาชีพ)
							</td>
							<td data-title="วันที่/เวลา">ห้องประชุมบุญวาส ลำเพาพงศ์<br/>9 พ.ย. 2560<br>(08.30-16.30 น.)</td>
							<td data-title="ทีมวิทยากร/ทีมนักวิจัย">
								1) รศ.ดร.จิราพร  กุลสาริน<br>
								2) คุณฉัตรดนัย คงทัศน์ เจ้าของฟาร์ม C&W Farm-Cricket & Worms<br>
								3) รศ.ดร.ยุพา หาญบุญทรง<br>
								4) ผศ.ดร.โปรดปราน ทาเขียว อันเจลิ และอ.ดร.ปิยวรรณ สิมะไพศาล<br>
								5) วิทยากรรับเชิญจากสำนักงานมาตรฐานสินค้าเกษตรและอาหารแห่งชาติ
							</td>
							<td style="text-align: center;">								
								<a class="btn btn-primary" href="https://goo.gl/forms/cE5GP8xcvXPOLN9E3" role="button" target="_blank">ลงทะเบียน</a>
								<br><br>
								<a class="btn btn-success" href="https://drive.google.com/open?id=1gfHkXgpoXHbxxeVmkj8BHf8hkS9p_5UX6MlrcPxwAXM" role="button" target="_blank">ตรวจสอบรายชื่อ</a>								
							</td>
						</tr>
						<tr>
							<td data-title="ลำดับ" align="center">4</td>
							<td data-title="โครงการประชุมและเสวนา">
								<a href="<?php echo $this->Html->url('/files/เกษตรเพื่ออาหารสุขภาพดีวิถีล้านนา.pdf'); ?>" target="_blank">
									เกษตรเพื่ออาหารสุขภาพดีวิถีล้านนา <br/>
									1) นวัตกรรมใหม่การผลิตหอมแดงและดอกหอมแดงนอกฤดู<br/>
									2) เทคโนโลยีการผลิตเมล็ดพันธุ์พืชปลอดภัยและอินทรีย์ และผักไมโครกรีน
								</a>
								
							</td>
							<td data-title="วันที่/เวลา">ห้องประชุมบุญวาส ลำเพาพงศ์<br/>10 พ.ย. 2560<br>(09.00-12.00 น.)</td>
							<td data-title="ทีมวิทยากร/ทีมนักวิจัย">
								1) รศ.ดร.เกวลิน คุณาศักดากุล<br/>
								2) ผศ.ดร.ศิวาพร ธรรมดี<br/>
								3) อ.ดร.จุฑามาส  คุ้มชัย<br/>
								ผู้ดำเนินรายการ : คุณพัชนี สุวรรณวิศลกิจ
							</td>
							<td style="text-align: center;">								
								<a class="btn btn-primary" href="https://goo.gl/forms/dCuAsHKY1e9yPPPY2" role="button" target="_blank">ลงทะเบียน</a>
								<br><br>
								<a class="btn btn-success" href="https://drive.google.com/open?id=1MU2LKdm4jibFBzUMIG-7UtCNTszpXjUDviH6JcUQg8M" role="button" target="_blank">ตรวจสอบรายชื่อ</a>								
							</td>
						</tr>
						<tr>
							<td data-title="ลำดับ" align="center">5</td>
							<td data-title="โครงการประชุมและเสวนา">
								<a href="<?php echo $this->Html->url('/files/กาแฟล้านนาคุณภาพ.pdf'); ?>" target="_blank">
									กาแฟล้านนาคุณภาพ
								</a>
								 
							</td>
							<td data-title="วันที่/เวลา">ห้องประชุมบุญวาส ลำเพาพงศ์<br/>10 พ.ย. 2560<br>(13.30-16.00 น.)</td>
							<td data-title="ทีมวิทยากร/ทีมนักวิจัย">
								ผศ.ดร.เยาวลักษณ์ จันทร์บาง<br/>
								คุณพัชนี สุวรรณวิศลกิจ
							</td>
							<td style="text-align: center;">								
								<a class="btn btn-primary" href="https://goo.gl/forms/Ejw4ft0JS4R7BPW82" role="button" target="_blank">ลงทะเบียน</a>
								<br><br>
								<a class="btn btn-success" href="https://drive.google.com/open?id=1DEapLuFaZwUgpDJxdro5NAAeC_ErWg8ZQzj0cax5F88" role="button" target="_blank">ตรวจสอบรายชื่อ</a>								
							</td>
						</tr>
						<tr>
							<td data-title="ลำดับ" align="center">6</td>
							<td data-title="โครงการประชุมและเสวนา">
								<a href="<?php echo $this->Html->url('/files/เสวนาการชี้ช่องอนาคตธุรกิจเกษตรยุค 4.0.pdf'); ?>" target="_blank">
									ชี้ช่องอนาคตธุรกิจการเกษตรยุค 4.0<br/>
									โดย :- ฝ่ายกิจกรรมศิษย์เก่า (14,750)<br/>
									1)	ธกส. กับการสนับสนุนเกษตรยุค 4.0<br/>
									2)	การตลาดกับเกษตรยุค 4.0<br/>
									3)	ชาวสวนลำไยยุค 4.0
								</a>	
								
							</td>
							<td data-title="วันที่/เวลา">ห้องประชุมพิศิษฐ์ วรอุไร<br/>11 พ.ย. 2560<br>(09.00-12.00 น.)</td>
							<td data-title="ทีมวิทยากร/ทีมนักวิจัย">
								1) คุณอภิรมย์ สุขประเสริฐ (ผจก.ธ.ก.ส.)<br/>
								2) คุณอภิรักษ์ โกษะโยธิน<br/>
								3) คุณชยันต์ เนรัญชร<br/>
								ผู้ดำเนินรายการ : นายศิวัจน์ วิมลนพลักษณ์ 
							</td>
							<td style="text-align: center;">								
								<a class="btn btn-primary" href="https://docs.google.com/forms/d/e/1FAIpQLSd4b0qIKFqvVV46ahUXiy-4cB3CDL49p0plwt7oXf6_Gc95AA/viewform" role="button" target="_blank">ลงทะเบียน</a>
								<br><br>
								
								<a class="btn btn-success" href="https://docs.google.com/spreadsheets/d/1oUAtloL6wm5rjySjk02zFSJNb4q-QZywBctSNhTah3c/edit#gid=488439156" role="button" target="_blank">ตรวจสอบรายชื่อ</a>	
								
							</td>
						</tr>
						<tr>
							<td data-title="ลำดับ" align="center">7</td>
							<td data-title="โครงการประชุมและเสวนา">
								<a href="<?php echo $this->Html->url('/files/เสวนาการเกษตรยุคใหม่.pdf'); ?>" target="_blank">
									คนกล้าคืนถิ่น ....เกษตรรุ่นใหม่ยุคไทยแลนด์ 4.0
								</a>								
							</td>
							<td data-title="วันที่/เวลา">ห้องประชุมบุญญวาส ลำเพาพงศ์<br/>11 พ.ย. 2560<br>(13.30-15.30 น.)</td>
							<td data-title="ทีมวิทยากร/ทีมนักวิจัย">
								ร่วมกับศูนย์บริหารงานวิจัย(งานวิจัยรับใช้สังคม)  มช.<br/>
								1) นายพรรษวุฒิ   รุ่งรัศมี<br/>
								2) นายภูริช สิงห์ฆะราช<br/>
								3) นางดวงใจ สิริใจ<br>
								ผู้ดำเนินรายการ : คุณสุภาพรรณ ไกรฤกษ์
							</td>
							<td style="text-align: center;">								
								<a class="btn btn-primary" href="https://goo.gl/forms/5b7CHSh8WgtDv67H2" role="button" target="_blank">ลงทะเบียน</a>
								<br><br>
								<a class="btn btn-success" href="https://drive.google.com/open?id=1T6T4dkPNtV48y59jpbOLguHDE8ADSU8UBdv-XxBNT2g" role="button" target="_blank">ตรวจสอบรายชื่อ</a>								
							</td>
						</tr>
					</tbody>
				</table>
			</div>
	
		</div>

	</div>
	
</div>

<div class="modal fade" id="myModal" role="dialog">
	<div class="modal-dialog">
	  <!-- Modal content-->
	  <div class="modal-content">
		<div class="modal-body" style="padding: 0;">
		  <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="position: absolute; right: 5px; top: 5px;"><span aria-hidden="true">&times;</span></button>
		  <img id="modalpopup" src="" style="width: 100%;">
		</div>
	  </div>
	</div>
</div>