
<title>
รายชื่อนักศึกษา ปีการศึกษา <?php echo $term;?> / <?php echo $year;?> รหัส <?php echo substr($yearId1,0,2) ?>  สาขาวิชา<?php echo $major['Major']['major_name']?> ระดับ <?php echo $degree['Degree']['degree_name'] ?>
</title>
<!-- Bootstrap core CSS -->
<?php echo $this->Html->css('/assets/bootstrap-4.5.3/dist/css/bootstrap.min.css'); ?>
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script>
			window.jQuery || document.write('<script src="../assets/bootstrap-4.5.3/dist/js/vendor/jquery.slim.min.js"><\/script>')
		</script>
		<?php echo $this->Html->script('/assets/bootstrap-4.5.3/dist/js/bootstrap.bundle.min.js'); ?>
		<!-- close bootstrap -->
		
		<!-- Datatable -->
		<?php
		echo $this->Html->css('/assets/DataTables/datatables.min.css');
		echo $this->Html->script('/assets/DataTables/datatables.min.js');
		?>
		<!-- clsoe Datatable -->

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">

<style>
    th,
    td {
        vertical-align: top !important;
    }
</style>
	<?php 
		date_default_timezone_set('Asia/Bangkok');
		$date = date("Y-m-d");
		$time = date("H:i:s");

		function DateDiff($strDate1,$strDate2){                             
		return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
		}


	?>
  
	<?php //debug($degree) ?>
	<div class="container.fluid" style="padding:0 25px 0;">
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist" style="margin-top:40px;">
					<li role="presentation" class="active">
						<a href="#news" aria-controls="news" role="tab" data-toggle="tab">
							<i class="glyphicon glyphicon-th-list"></i> <b>แสดงรายชื่อนักศึกษาแข่งขันนำเสนอผลงานทางวิชาการภาคโปสเตอร์ของนักศึกษา </b>
						</a>
				
					</li>			
				</ul>
			</div>
		</div>
		<center>
			<h3>การแข่งขันนำเสนอผลงานทางวิชาการภาคโปสเตอร์ของนักศึกษา</h3> 
			<h4>ระดับชั้นปีที่ 3และปีที่ 4 คณะเกษตรศาสตร์</h4>
		</center>
		<br>
		 
	</div>
	<div id="no-more-tables">
		<table class="table table-bordered table-hover nowrap" id="myTable" style="width: 100%">
			<thead>
				<tr> 
					<th align="center">ลำดับ</th>
					
					<th style="width:5%">รหัสนักศึกษา</th>
					<th style="width:15%">ชื่อ-สกุล</th>
					<th style="width:25%">ชื่อหัวข้อภาษาไทย</th>
					<th style="width:25%">ชื่อหัวข้อภาษาอังกฤษ</th>
					<th style="width:10%">อาจารย์ที่ปรึกษา</th>
					<th style="width:10%">สาขา</th>
					<th style="width:10%">ภาควิชา</th>
						
					<th style="width:8%">ประทับตรา</th>		
					<th>ดูข้อมูล</th> 			 
				</tr>
				
			</thead>
			<tbody>
				<?php echo $output;  ?>		
				
			</tbody>
				
		</table>
	</div>

 
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable({
            responsive: {
                details: false
            },
			paging: false,
            scrollX: true,
            ordering: false,
            responsive: false,
            language: {
                searchPlaceholder: "ค้นหา"
            },
            dom: 'Bfrtip',
            buttons: ['copy', 'excel', 'print'],
        });
    });

     
</script>


 