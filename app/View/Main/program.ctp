<?php
	function DateThai($strDate)
		{

		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//, $strHour:$strMinute
	}
	?>
	<?php 
		date_default_timezone_set('Asia/Bangkok');
		$date = date("Y-m-d");
		$time = date("H:i:s");
	

		function DateDiff($strDate1,$strDate2){                             
		return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
		}

		function TimeDiff($strTime1,$strTime2) {                             
            return (strtotime($strTime2) - strtotime($strTime1))/  ( 60 * 60 ); // 1 Hour =  60*60                           
        }

	?>
<div class="container" style="padding:15px 0 0;">
	<div class="panel panel-default" style="border-color: #F2F2F2!important">
		<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
				<div class="panel-title">
					<h3 style="margin:0px !important;">
						<center><div class="font_panel2">ประกาศรายชื่อผู้เข้าแข่งขันกิจกรรมนักเรียนและนักศึกษา</div></center>
					</h3> 
				</div>
			</div>	
		<div class="panel-body" style="padding-top: 15px; ">
			<div align="center"> 	
				<h4>กิจกรรมนักเรียนและนักศึกษา</h4>
				<h4>ในงานเกษตรภาคเหนือ ครั้งที่ 10 “นวัตกรรมเกษตรอัจฉริยะเพื่อเป้าหมายการพัฒนาที่ยั่งยืน”</h4>
				<h4>ในระหว่างวันที่ 1-12 ธันวาคม 2565</h4>
				<br>
			</div> 
			<div id="no-more-tables"> 
				<div class="row">
					<div class="col-md-4 pull-right">
						<a href="<?php echo $this->Html->url(array('action' => 'studentchecklist',4)); ?>" target="_blank">
							<img src="<?php echo $this->Html->url('/img/messageImage_1668586606420.jpg' ); ?>" class="thumbnail" width="100%">
						</a>
					</div>
					<div class="col-md-4">
						<a href="<?php echo $this->Html->url(array('action' => 'studentchecklist',1)); ?>" target="_blank">
							<img src="<?php echo $this->Html->url('/img/messageImage_1668587491548.jpg' ); ?>" class="thumbnail" width="100%">
						</a>
					</div>
				</div>	
				<div class="row">
					<div class="col-md-4 pull-right">
						<a href="<?php echo $this->Html->url(array('action' => 'studentchecklist',3)); ?>" target="_blank">
							<img src="<?php echo $this->Html->url('/img/messageImage_1668587565114.jpg' ); ?>" class="thumbnail" width="100%">
						</a>
					</div>
					<div class="col-md-4">
						<a href="<?php echo $this->Html->url(array('action' => 'studentchecklist',5)); ?>" target="_blank">
							<img src="<?php echo $this->Html->url('/img/1668587606732.jpg' ); ?>" class="thumbnail" width="100%">
						</a>
					</div>
					<div class="col-md-4">
						<a href="<?php echo $this->Html->url(array('action' => 'studentchecklist',6)); ?>" target="_blank">
							<img src="<?php echo $this->Html->url('/img/1668587647514.jpg' ); ?>" class="thumbnail" width="100%">
						</a>
					</div>
				</div>				
				 
			</div>
	
		</div>

	</div>
	
	<div class="row">
		<div class="row">
			<div class="col-md-12">	
				<div class="alert alert-info">
					ติดต่อสอบถาม : ฝ่ายกิจกรรมนักเรียน นักศึกษา งานเกษตรภาคเหนือ ครั้งที่ 10 โทรศัพท์ : 053-944641-4 ต่อ 117 (คุณสมโภชน์ อจิยจักร์)ในวันและเวลาราชการ
				</div>
			</div>
		</div>
	</div>
	
</div>
