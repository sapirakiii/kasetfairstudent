<div class="container">
    <div class="panel-header" align="center">
        <h3>จำนวนนักศึกษาที่มีคุณลักษณะบัณฑิตตามที่มหาวิทยาลัยกำหนด (CMU Student DNA Blueprint)</h3>
    </div>
</div>
<div class="panel-body">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th style="text-align:center;" colspan="" rowspan="" width="1%">ลำดับ</th>
                <th style="text-align:center;" colspan="" rowspan="" width="20%">หัวข้อ</th>
                <th style="text-align:center;" colspan="" rowspan="" width="5%">จำนวนคน</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="text-align:center;">1</td>
                <td style="text-align:center;"><a href="<?php echo $this->Html->url(array('action' => 'student_dna')) ?>" target="_blank">จำนวนนักศึกษาที่มีคุณลักษณะบัณฑิตตามที่มหาวิทยาลัยกำหนด (CMU Student DNA Blueprint)</a></td>
                <td style="text-align:center;"><?php echo $student_dnas?></td>
            </tr>
        </tbody>
    </table>
</div>