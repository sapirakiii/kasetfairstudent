<div class="container" style="padding:0 15px 0;">	
	<div class="row">
		<div class="col-md-12">
                  <div class="well">

                        <tbody>
                              <form method="post" accept-charset="utf-8">
                                    <div class="panel panel-success">
                                          <div class="panel-heading">
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-plus"></i>แก้ไขข้อมูล</h3>
                                          </div>
                                          <?php
                                                echo $this->Form->input('StudentDna.id',array(
                                                      'hidden',
                                                ));
                                          ?>
                                          <div class="panel-body">
                                                <label class="col-sm-4 control-label">รหัสนักศึกษา</label>
                                                <div class="col-sm-8">
                                                      <?php
                                                            echo $this->Form->input('StudentDna.student_id',array(
                                                                  'type' => 'text',
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'placeholder' => 'รหัสนักศึกษา',
                                                                  'required',
                                                            ));
                                                      ?>
                                                </div>
                                                <label class="col-sm-4 control-label">ชื่อ - นามสกุล</label>
                                                <div class="col-sm-8">
                                                      <?php
                                                            echo $this->Form->input('StudentDna.stu_name',array(
                                                                  'type' => 'text',
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'placeholder' => 'ชื่อ - นามสกุล',
                                                                  'required',
                                                            ));
                                                      ?>
                                                </div>
                                                <label class="col-sm-4 control-label">ระดับการศึกษา</label>
                                                <div class="col-sm-8">
                                                      <?php
                                                            $degree = array(
                                                                  '1' => 'ปริญญาตรี',
                                                                  '3' => 'ปริญญาโท',
                                                                  '5' => 'ปริญญาเอก',
                                                            );
                                                            echo $this->Form->input('StudentDna.degree_id',array(
                                                                  'options' => $degree,
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'required'
                                                            ));
                                                      ?>
                                                </div>
                                                <label class="col-sm-4 control-label">เดือน</label>
                                                <div class="col-sm-8">
                                                      <?php
                                                            $month = array(
                                                                  '1' => '1',
                                                                  '2' => '2',
                                                                  '3' => '3',
                                                                  '4' => '4',
                                                                  '5' => '5',
                                                                  '6' => '6',
                                                                  '7' => '7',
                                                                  '8' => '8',
                                                                  '9' => '9',
                                                                  '10' => '10',
                                                                  '11' => '11',
                                                                  '12' => '12',
                                                            );
                                                            echo $this->Form->input('StudentDna.month',array(
                                                                  'options' => $month,
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'required'
                                                            ));
                                                      ?>
                                                </div>
                                                <label class="col-sm-4 control-label">ปี</label>
                                                <div class="col-sm-8">
                                                      <?php
                                                            $year = array(
                                                                  '2569' => '2569',
                                                                  '2568' => '2568',
                                                                  '2567' => '2567',
                                                                  '2566' => '2566',
                                                                  '2565' => '2565',
                                                                  '2564' => '2564',
                                                                  '2563' => '2563',
                                                                  '2562' => '2562',
                                                                  '2561' => '2561',
                                                                  '2560' => '2560',
                                                                  '2559' => '2559',
                                                                  '2558' => '2558',
                                                                  '2557' => '2557',
                                                            );
                                                            echo $this->Form->input('StudentDna.year',array(
                                                                  'options' => $year,
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'required'
                                                            ));
                                                      ?>
                                                </div>
                                          </div>
                                    </div>
                                    <div>
                                          <input type="submit" class="btn btn-success" value="บันทึกข้อมูล" onclick="return confirm('กรุณายืนยันการบันทึกข้อมูล')"> 
                                    </div>
                              </form>
                        </tbody>

                  </div>
            </div>
      </div>
</div>
<div style="clear: both;"></div>