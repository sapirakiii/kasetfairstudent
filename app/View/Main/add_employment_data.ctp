<div class="container">
      <div class="row">
            <div class="col-md-12">
                  <div class="panel-heading">
                        <h3>เพิ่มข้อมูล</h3>
                  </div>
                        <form method="post" accept-charset="utf-8">
                              <div class="panel-body">
                                    <label class="col-sm-4 control-label">ระดับการศึกษา</label>
                                    <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                                <?php
                                                      echo $this->Form->input('Employment.degree_name',array(
                                                            'type' => 'text',
                                                            'label' => false,
                                                            'div' => false,
                                                            'class' => array('form-control css-require'),
                                                            'error' => false,
                                                            'placeholder' => 'ระดับการศึกษา',
                                                            'required'
                                                      ));
                                                ?>
                                          </div>
                                    </div><label class="col-sm-4 control-label">ปีการศึกษา</label>
                                    <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                                <?php
                                                      echo $this->Form->input('Employment.year',array(
                                                            'type' => 'text',
                                                            'label' => false,
                                                            'div' => false,
                                                            'class' => array('form-control css-require'),
                                                            'error' => false,
                                                            'placeholder' => 'ปีการศึกษา',
                                                            'required'
                                                      ));
                                                ?>
                                          </div>
                                    </div>
                                    <label class="col-sm-4 control-label">หลักสูตร</label>
                                    <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                                <?php
                                                      echo $this->Form->input('Employment.syllabus',array(
                                                            'type' => 'text',
                                                            'label' => false,
                                                            'div' => false,
                                                            'class' => array('form-control css-require'),
                                                            'error' => false,
                                                            'placeholder' => 'หลักสูตร',
                                                            'required'
                                                      ));
                                                ?>
                                          </div>
                                    </div>
                                    <label class="col-sm-4 control-label">ผู้สำเร็จการศึกษา</label>
                                    <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                                <?php
                                                      echo $this->Form->input('Employment.graduate_total',array(
                                                            'type' => 'text',
                                                            'label' => false,
                                                            'div' => false,
                                                            'class' => array('form-control css-require'),
                                                            'error' => false,
                                                            'placeholder' => 'ผู้สำเร็จการศึกษา',
                                                            'required'
                                                      ));
                                                ?>
                                          </div>
                                    </div>
                                    <label class="col-sm-4 control-label">จำนวนผู้ตอบแบบสอบถาม</label>
                                    <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                                <?php
                                                      echo $this->Form->input('Employment.respondents',array(
                                                            'type' => 'text',
                                                            'label' => false,
                                                            'div' => false,
                                                            'class' => array('form-control css-require'),
                                                            'error' => false,
                                                            'placeholder' => 'จำนวนผู้ตอบแบบสอบถาม',
                                                            'required'
                                                      ));
                                                ?>
                                          </div>
                                    </div>

                                    <br><br>
                                    <h4>ตารางคำนวณร้อยละ ของผู้ตอบแแบบสอบถาม</h4>
                                    <label class="col-sm-4 control-label">จำนวนผู้ตอบแบบสอบถาม</label>
                                    <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                                <?php
                                                      echo $this->Form->input('Employment.ws_respondents',array(
                                                            'type' => 'text',
                                                            'label' => false,
                                                            'div' => false,
                                                            'class' => array('form-control css-require'),
                                                            'error' => false,
                                                            'placeholder' => 'จำนวนผู้ตอบแบบสอบถาม',
                                                      ));
                                                ?>
                                          </div>
                                    </div>
                                    <label class="col-sm-4 control-label">ทำงานแล้ว</label>
                                    <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                                <?php
                                                      echo $this->Form->input('Employment.ws_aw_qty',array(
                                                            'type' => 'text',
                                                            'label' => false,
                                                            'div' => false,
                                                            'class' => array('form-control css-require'),
                                                            'error' => false,
                                                            'placeholder' => 'ทำงานแล้ว',
                                                      ));
                                                ?>
                                          </div>
                                    </div>
                                    <label class="col-sm-4 control-label">ทำงานแล้วและกำลังศึกษาต่อ</label>
                                    <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                                <?php
                                                      echo $this->Form->input('Employment.ws_was_qty',array(
                                                            'type' => 'text',
                                                            'label' => false,
                                                            'div' => false,
                                                            'class' => array('form-control css-require'),
                                                            'error' => false,
                                                            'placeholder' => 'ทำงานแล้วและกำลังศึกษาต่อ',
                                                      ));
                                                ?>
                                          </div>
                                    </div>
                                    <label class="col-sm-4 control-label">กำลังศึกษาต่อ</label>
                                    <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                                <?php
                                                      echo $this->Form->input('Employment.ws_cs_qty',array(
                                                            'type' => 'text',
                                                            'label' => false,
                                                            'div' => false,
                                                            'class' => array('form-control css-require'),
                                                            'error' => false,
                                                            'placeholder' => 'กำลังศึกษาต่อ',
                                                      ));
                                                ?>
                                          </div>
                                    </div>
                                    <label class="col-sm-4 control-label">ยังไม่ได้ทำงานและไม่ได้ศึกษาต่อ</label>
                                    <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                                <?php
                                                      echo $this->Form->input('Employment.ws_ue_qty',array(
                                                            'type' => 'text',
                                                            'label' => false,
                                                            'div' => false,
                                                            'class' => array('form-control css-require'),
                                                            'error' => false,
                                                            'placeholder' => 'ยังไม่ได้ทำงานและไม่ได้ศึกษาต่อ',
                                                      ));
                                                ?>
                                          </div>
                                    </div>
                              </div>
                              <div class="form-group">
                                    <div class="col-sm-7">
                                          <input type="submit" class="btn btn-success" value="บันทึกข้อมูล" onclick="return confirm('ยืนยันการเพิ่มข้อมูล')">
                                    </div>
                              </div>
                        </form>
            </div>
      </div>
</div>