<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dependencies/JQL.min.js"></script>
<script type="text/javascript" src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>

<link rel="stylesheet" href="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dist/jquery.Thailand.min.css">
<script type="text/javascript" src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>


<?php
function DateThai($strDate)
{

	$strYear = date("Y", strtotime($strDate)) + 543;
	$strMonth = date("n", strtotime($strDate));
	$strDay = date("j", strtotime($strDate));
	$strHour = date("H", strtotime($strDate));
	$strMinute = date("i", strtotime($strDate));
	$strSeconds = date("s", strtotime($strDate));
	$strMonthCut = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
	$strMonthThai = $strMonthCut[$strMonth];
	return "$strDay $strMonthThai $strYear";
	//, $strHour:$strMinute
}
?>

<div class="container" style="padding:15px 0 0;">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center>ลงทะเบียนเข้าร่วมกิจกรรมงานเกษตรภาคเหนือ</center>
				</h3>
			</div>
		</div>
		<div class="panel-body" style="padding-top: 15px; ">
			<div align="center">

				<h3> ฝ่ายกิจกรรมนักเรียน นักศึกษา</h3> <br>
				<h3><?php echo $project['Project']['name']; ?></h3> <br>
				<h4>ณ <?php echo $project['Project']['place']; ?> วันที่ <?php echo $project['Project']['date']; ?> เวลา <?php echo $project['Project']['time']; ?> </h4>
				<br><b>
					รับสมัครตั้งแต่วันนี้ จนถึง วันที่
					<?php
					$time1 = strtotime($project['Project']['dateout']);
					$newformat1 = date('d-M-Y', $time1);

					echo DateThai($newformat1)  ?> เวลา <?php echo date('H.i', strtotime($project['Project']['timestop']));
														?> น.</b> <br>
				<a href="<?php echo $this->Html->url('/files/' . $project['Project']['file_name']); ?>" target="_blank" class="btn btn-primary btn-lg">
					Download <?php echo $project['Project']['name']; ?>
				</a>
			</div>

			<br>
			<?php
			date_default_timezone_set('Asia/Bangkok');
			$date = date("Y-m-d");
			$time = date("H:i:s");

			function DateDiff($strDate1, $strDate2)
			{
				return (strtotime($strDate2) - strtotime($strDate1)) /  (60 * 60 * 24);  // 1 day = 60*60*24
			}
			?>
			<?php if (DateDiff($project['Project']['dateout'] . " " . $project['Project']['timestop'], $date . " " . $time) > 0) { ?>
				<center>
					<h3>
						<font color="red"> ปิดรับการลงทะเบียน <br> คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่ ขอขอบพระคุณที่ท่านให้ความสนใจของงานเกษตรภาคเหนือมา ณ โอกาสนี้ </font>
					</h3>
				</center><br>
			<?php } else { ?>
				<form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">
					<?php
					//debug($this->request->data);
					$required = 'กรุณากรอกข้อมูล';

					echo $this->Form->create('Member', array('role' => 'form', 'data-toggle' => 'validator'));

					if($projectId==3){
						$types = array('ป.1' => 'ป.1', 'ป.2' => 'ป.2', 'ป.3' => 'ป.3', 'ป.4' => 'ป.4', 'ป.5' => 'ป.5', 'ป.6' => 'ป.6');
					} elseif($projectId==4){
						$types = array('ม.1' => 'ม.1', 'ม.2' => 'ม.2', 'ม.3' => 'ม.3', 'ม.4' => 'ม.4', 'ม.5' => 'ม.5', 'ม.6' => 'ม.6');
					} elseif($projectId==5){
						$types = array('ม.1' => 'ม.1', 'ม.2' => 'ม.2', 'ม.3' => 'ม.3', 'ม.4' => 'ม.4', 'ม.5' => 'ม.5', 'ม.6' => 'ม.6');
					} elseif($projectId==6){
						$types = array('ม.1' => 'ม.1', 'ม.2' => 'ม.2', 'ม.3' => 'ม.3', 'ม.4' => 'ม.4', 'ม.5' => 'ม.5', 'ม.6' => 'ม.6');
					} elseif($projectId==1){
						$types = array('ม.4' => 'ม.4', 'ม.5' => 'ม.5', 'ม.6' => 'ม.6', 'ปวช.1' => 'ปวช.1', 'ปวช.2' => 'ปวช.2', 'ปวช.3' => 'ปวช.3');
					}

					?>

					<table class="table borderless">
						<thead>
							<tr>
								<td colspan="3" , style="width:80%">
									<div class="form-group col-md-12" style="margin-bottom:30px;">
										<label>ชื่อโรงเรียน / สถาบันการศึกษา <span style="color: red;">*</span></label>
										<?php echo $this->Form->input('school', array(
											'type' => 'text',
											'label' => false,
											'class' => 'form-control',
											'data-error' => $required,
											'required' => true
										)); ?>
									</div>
								</td>
								<td style="width:20%">
									<div class="form-group col-md-12" style="margin-bottom:30px;">
										<label>ทีมลำดับที่ <span style="color: red;">*</span></label>
										<?php echo $this->Form->input('team_no', array(
											'type' => 'text',
											'label' => false,
											'class' => 'form-control',
											'data-error' => $required,
											'required' => true,
											'default' => '1'
										)); ?>
									</div>
								</td>

							</tr>
							<tr>
								<td colspan=4>
									<div class="form-group col-md-12" style="margin-bottom:30px;">
										<label>ที่อยู่โรงเรียน / สถาบันการศึกษา <span style="color: red;">*</span></label>
										<?php echo $this->Form->input('school_address', array(
											'type' => 'text',
											'label' => false,
											'class' => 'form-control',
											'data-error' => $required,
											'required' => true
										)); ?>
									</div>
								</td>
							<tr>
								<td>
									<div class="form-group col-md-12" style="margin-bottom:30px;">
										<label>ตำบล <span style="color: red;">*</span></label>
										<?php echo $this->Form->input('school_tambon', array(
											'id' => 'school_tambon',
											'type' => 'text',
											'label' => false,
											'class' => 'form-control',
											'data-error' => $required,
											'required' => true
										)); ?>
									</div>

								</td>
								<td>
									<div class="form-group col-md-12" style="margin-bottom:30px;">
										<label>อำเภอ <span style="color: red;">*</span></label>
										<?php echo $this->Form->input('school_amphoe', array(
											'id' => 'school_amphoe',
											'type' => 'text',
											'label' => false,
											'class' => 'form-control',
											'data-error' => $required,
											'required' => true
										)); ?>
									</div>

								</td>
								<td>
									<div class="form-group col-md-12" style="margin-bottom:30px;">
										<label>จังหวัด <span style="color: red;">*</span></label>
										<?php echo $this->Form->input('school_province', array(
											'id' => 'school_province',
											'type' => 'text',
											'label' => false,
											'class' => 'form-control',
											'data-error' => $required,
											'required' => true
										)); ?>
									</div>

								</td>
								<td>
									<div class="form-group col-md-12" style="margin-bottom:30px;">
										<label>รหัสไปรษณีย์ <span style="color: red;">*</span></label>
										<?php echo $this->Form->input('school_zipcode', array(
											'id' => 'school_zipcode',
											'type' => 'text',
											'label' => false,
											'class' => 'form-control',
											'data-error' => $required,
											'required' => true
										)); ?>
									</div>

								</td>
							</tr>

							<script>
								$.Thailand({
									$district: $('#school_tambon'), // input ของตำบล
									$amphoe: $('#school_amphoe'), // input ของอำเภอ
									$province: $('#school_province'), // input ของจังหวัด
									$zipcode: $('#school_zipcode'), // input ของรหัสไปรษณีย์
								});
							</script>
							
						</thead>
					</table>



					<br />




					<div style="margin-bottom:30px;"></div>
					<table class="table table-bordered table-striped">
						<thead>
							<tr>
								<th style="text-align: center;">ลำดับ</th>
								<th style="text-align: center;">คำนำหน้า</th>
								<th style="text-align: center;">ชื่อ </th>
								<th style="text-align: center;">สกุล</th>
								<th style="text-align: center;">อายุ</th>
								<th style="text-align: center;">โทรศัพท์</th>
								<th style="text-align: center;">ระดับการศึกษา</th>
							</tr>
						</thead>
						<tbody>
							<!-- ************************** 1********************************   -->
							<tr style="text-align: center;">
								<td data-title="อันดับที่" align="center">1</td>
								<td data-title="คำนำหน้า">
									<div class="form-group has-feedback">
										<?php
										$required = 'กรุณากรอกข้อมูล';
										echo $this->Form->input('Member.prefix_id1', array(
											'options' => $prefixes,
											'class' => 'form-control',
											'label' => false,
											'value' => "01",
											'required' => true,
											'data-error' => $required,
											'empty' => 'กรุณาเลือกคำนำหน้า'
										));
										?>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									</div>
								</td>
								</td>
								<td data-title="ชื่อ">
									<div class="form-group has-feedback">
										<?php
										echo $this->Form->input('Member.fname1', array(
											'type' => 'text',
											'label' => false,
											'div' => false,
											'class' => array('form-control css-require'),
											'error' => false,

										));
										?>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									</div>
								</td>
								<td data-title="สกุล">
									<div class="form-group has-feedback">
										<?php
										echo $this->Form->input('Member.lname1', array(
											'type' => 'text',
											'label' => false,
											'div' => false,
											'class' => array('form-control css-require'),
											'error' => false,

										));
										?>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									</div>
								</td>
								<td data-title="อายุ">
									<div class="form-group has-feedback">
										<?php
										echo $this->Form->input('Member.age1', array(
											'type' => 'number',
											'min' => '0',
											'label' => false,
											'div' => false,
											'class' => array('form-control css-require'),
											'error' => false,

										));
										?>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									</div>
								</td>
								<td data-title="เบอร์โทรศัพท์">
									<div class="form-group has-feedback">
										<?php
										echo $this->Form->input('Member.phone1', array(
											'type' => 'number',
											'min' => '0',
											'label' => false,
											'div' => false,
											'class' => array('form-control css-require'),
											'error' => false,

										));
										?>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									</div>
								</td>
								<td data-title="ระดับ">

									<?php
									// $types = array('ป.1-6' => 'ป.1-6', 'ม.1-3' => 'ม.1-3', 'ม.4-6' => 'ม.4-6');
									echo $this->Form->input('Member.level1', array(
										'options' => $types,
										'data-error' => $required,
										'label' => false,
										'class' => 'form-control',
										'required' => true
									)); ?>

									<div class="help-block with-errors"></div>
								</td>
							</tr>
							<!-- ************************** 2 ********************************   -->
							<tr style="text-align: center;">
								<td data-title="อันดับที่" align="center">2</td>
								<td data-title="คำนำหน้า">
									<div class="form-group has-feedback">
										<?php
										$required = 'กรุณากรอกข้อมูล';
										echo $this->Form->input('Member.prefix_id2', array(
											'options' => $prefixes,
											'class' => 'form-control',
											'label' => false,
											'value' => "01",
											'required' => true,
											'data-error' => $required,
											'empty' => 'กรุณาเลือกคำนำหน้า'
										));
										?>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									</div>
								</td>
								</td>
								<td data-title="ชื่อ">
									<div class="form-group has-feedback">
										<?php
										echo $this->Form->input('Member.fname2', array(
											'type' => 'text',
											'label' => false,
											'div' => false,
											'class' => array('form-control css-require'),
											'error' => false,
										));
										?>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									</div>
								</td>
								<td data-title="สกุล">
									<div class="form-group has-feedback">
										<?php
										echo $this->Form->input('Member.lname2', array(
											'type' => 'text',
											'label' => false,
											'div' => false,
											'class' => array('form-control css-require'),
											'error' => false,
										));
										?>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									</div>
								</td>
								<td data-title="อายุ">
									<div class="form-group has-feedback">
										<?php
										echo $this->Form->input('Member.age2', array(
											'type' => 'number',
											'min' => '0',
											'label' => false,
											'div' => false,
											'class' => array('form-control css-require'),
											'error' => false,
										));
										?>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									</div>
								</td>
								<td data-title="เบอร์โทรศัพท์">
									<div class="form-group has-feedback">
										<?php
										echo $this->Form->input('Member.phone2', array(
											'type' => 'number',
											'min' => '0',
											'label' => false,
											'div' => false,
											'class' => array('form-control css-require'),
											'error' => false,
										));
										?>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									</div>
								</td>
								<td data-title="ระดับ">

									<?php
									// $types = array('ป.1-6' => 'ป.1-6', 'ม.1-3' => 'ม.1-3', 'ม.4-6' => 'ม.4-6');
									echo $this->Form->input('Member.level2', array(
										'options' => $types,
										'data-error' => $required,
										'label' => false,
										'class' => 'form-control',
										'required' => true
									)); ?>

									<div class="help-block with-errors"></div>
								</td>
							</tr>
							<!-- ************************** 3 ********************************   -->
							<?php if ($project['Project']['id'] <> '5' && $project['Project']['id'] <> '6') { ?>
								<tr style="text-align: center;">
									<td data-title="อันดับที่" align="center">3</td>
									<td data-title="คำนำหน้า">
										<div class="form-group has-feedback">
											<?php
											$required = 'กรุณากรอกข้อมูล';
											echo $this->Form->input('Member.prefix_id3', array(
												'options' => $prefixes,
												'class' => 'form-control',
												'label' => false,
												'value' => "01",
												'required' => true,
												'data-error' => $required,
												'empty' => 'กรุณาเลือกคำนำหน้า'
											));
											?>
											<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
										</div>
									</td>
									</td>
									<td data-title="ชื่อ">
										<div class="form-group has-feedback">
											<?php
											echo $this->Form->input('Member.fname3', array(
												'type' => 'text',
												'label' => false,
												'div' => false,
												'class' => array('form-control css-require'),
												'error' => false,
											));
											?>
											<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
										</div>
									</td>
									<td data-title="สกุล">
										<div class="form-group has-feedback">
											<?php
											echo $this->Form->input('Member.lname3', array(
												'type' => 'text',
												'label' => false,
												'div' => false,
												'class' => array('form-control css-require'),
												'error' => false,
											));
											?>
											<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
										</div>
									</td>
									<td data-title="อายุ">
										<div class="form-group has-feedback">
											<?php
											echo $this->Form->input('Member.age3', array(
												'type' => 'number',
												'min' => '0',
												'label' => false,
												'div' => false,
												'class' => array('form-control css-require'),
												'error' => false,
											));
											?>
											<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
										</div>
									</td>
									<td data-title="เบอร์โทรศัพท์">
										<div class="form-group has-feedback">
											<?php
											echo $this->Form->input('Member.phone3', array(
												'type' => 'number',
												'min' => '0',
												'label' => false,
												'div' => false,
												'class' => array('form-control css-require'),
												'error' => false,
											));
											?>
											<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
										</div>
									</td>
									<td data-title="ระดับ">

										<?php
										// $types = array('ป.1-6' => 'ป.1-6', 'ม.1-3' => 'ม.1-3', 'ม.4-6' => 'ม.4-6');
										echo $this->Form->input('Member.level3', array(
											'options' => $types,
											'data-error' => $required,
											'label' => false,
											'class' => 'form-control',
											'required' => true
										)); ?>

										<div class="help-block with-errors"></div>
									</td>
								</tr>
							<?php } ?>
							<!-- ************************** 4 ********************************   -->
							<!-- <tr style="text-align: center;">	
										<td data-title="อันดับที่" align="center">4</td>
											<td data-title="คำนำหน้า">
												<div class="form-group has-feedback">  
													<?php
													$required = 'กรุณากรอกข้อมูล';
													echo $this->Form->input('Member.prefix_id4', array(
														'options' => $prefixes,
														'class' => 'form-control',
														'label' => false,
														'value' => "01",
														'required' => true,
														'data-error' => $required,
														'empty' => 'กรุณาเลือกคำนำหน้า'
													));
													?>
													<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
												</div>                                         
											</td>
										</td>
										<td data-title="ชื่อ"> 
											<div class="form-group has-feedback">      
												<?php
												echo $this->Form->input('Member.fname4', array(
													'type' => 'text',
													'label' => false,
													'div' => false,
													'class' => array('form-control css-require'),
													'error' => false,
												));
												?>
												<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
											</div>	
										</td>
										<td data-title="สกุล"> 
											<div class="form-group has-feedback">      
												<?php
												echo $this->Form->input('Member.lname4', array(
													'type' => 'text',
													'label' => false,
													'div' => false,
													'class' => array('form-control css-require'),
													'error' => false,
												));
												?>
												<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
											</div>
										</td>
										<td data-title="อายุ"> 
											<div class="form-group has-feedback">      
												<?php
												echo $this->Form->input('Member.age4', array(
													'type' => 'number',
													'min' => '0',
													'label' => false,
													'div' => false,
													'class' => array('form-control css-require'),
													'error' => false,
												));
												?>
												<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
											</div>
										</td>
										<td data-title="เบอร์โทรศัพท์"> 
											<div class="form-group has-feedback">      
												<?php
												echo $this->Form->input('Member.phone4', array(
													'type' => 'number',
													'min' => '0',
													'label' => false,
													'div' => false,
													'class' => array('form-control css-require'),
													'error' => false,
												));
												?>
												<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
											</div>
										</td>
										<td data-title="ระดับ"> 
											 
											<?php
											// $types = array('ป.1-6' => 'ป.1-6', 'ม.1-3' => 'ม.1-3', 'ม.4-6' => 'ม.4-6');
											echo $this->Form->input('Member.level4', array(
												'options' => $types,
												'data-error' => $required,
												'label' => false,
												'class' => 'form-control',
												'required' => true
											)); ?>				
																
											<div class="help-block with-errors"></div>
										</td>
									</tr> -->


						</tbody>
					</table>




					<div class="form-group col-md-4">
						<label> ชื่อ-สกุล อาจารย์ผู้ควบคุม <span style="color: red;">*</span></label>

						<?php echo $this->Form->input('teacher', array('type' => 'text', 'label' => false, 'class' => 'form-control', 'data-error' => $required, 'required' => true)); ?>

						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group col-md-4">
						<label>เบอร์โทรอาจารย์ผู้ควบคุม </label>

						<?php echo $this->Form->input('phoneteacher', array('type' => 'text', 'label' => false, 'class' => 'form-control', 'data-error' => $required)); ?>

						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group col-md-4">
						<label> อีเมล์อาจารย์ผู้ควบคุม <span style="color: red;">*</span></label>

						<?php echo $this->Form->input('emailteacher', array('type' => 'email', 'label' => false, 'class' => 'form-control', 'data-error' => $required)); ?>

						<div class="help-block with-errors"></div>
					</div>
					<div class="row">
						<div class="form-group col-md-6">

							<div style="padding: 0 20px;">
								<table class="table table-bordered table-striped">
									<tbody>
										<tr>
											<td><label>แนบใบสมัครแบบไฟล์รูปภาพหรือไฟล์เอกสาร (WORD,PDF,JPG)</label></td>
											<td>
												<?php
												echo $this->Form->input('Member.files.', array('type' => 'file', 'accept' => '.xlsx,.xls,image/*, .doc, .docx, .ppt, .pptx, .txt, .pdf, .zip, .rar'));
												?>
												* เพิ่มได้มากกว่า 1 ไฟล์
											</td>
										</tr>

									</tbody>
								</table>
							</div>
						</div>

					</div>

					<br>
					<div class="row">
						<?php if ($projectId == 1) { ?>
							<div class="form-group col-md-9">
								<div style="padding: 0 20px;">
									<table class="table table-bordered table-striped">
										<tbody>
											<tr>
												<td><label>แนบไฟล์เอกสารประกวด (WORD,PDF,JPG)</label></td>
												<td>
													<?php
													echo $this->Form->input(
														'Member.file1s.',
														array(
															'type' => 'file',
															'accept' => '.xlsx,.xls,image/*, .doc, .docx, .ppt, .pptx, .txt, .pdf, .zip, .rar'
														)
													);
													?>
													* เพิ่มได้มากกว่า 1 ไฟล์
												</td>
											</tr>

										</tbody>
									</table>
								</div>
							</div>
						<?php  } ?>

					</div>



					<div class="row">
						<div class="col-md-12">
							<input type="submit" value="คลิกส่งข้อมูลการลงทะเบียน" class="btn btn-success btn-lg btn-block">
						</div>
					</div>
				</form>

			<?php } ?>

		</div>

	</div>

</div>