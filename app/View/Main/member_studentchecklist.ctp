<?php 
		date_default_timezone_set('Asia/Bangkok');
		$date = date("Y-m-d");
		$time = date("H:i:s");
	

		function DateDiff($strDate1,$strDate2){                             
		return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
		}

		function TimeDiff($strTime1,$strTime2) {                             
            return (strtotime($strTime2) - strtotime($strTime1))/  ( 60 * 60 ); // 1 Hour = 60*60
        }

	?>
	
<div class="container.fluid" style="padding:15px 0 0;">
	<div class="panel panel-default" style="border-color: #F2F2F2!important">
		<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center><div class="font_panel2">รายชื่อผู้ลงทะเบียน</div></center>
				</h3> 
			</div>
		</div>	
		<div class="panel-body" style="padding-top: 15px; ">
			<div align="center"> 	
				<h3>การแข่งขันนำเสนอผลงานทางวิชาการภาคโปสเตอร์ของนักศึกษา</h3>

				<h4>ระดับชั้นปีที่ 3และปีที่ 4 คณะเกษตรศาสตร์</h4>
			</div> 
			
			<?php
			if(!isset($admin)){
			?>
			<div class="row">
				<div class="col-md-12">
					<?php
					if(isset($citizenId)){
					?>
						<a class="btn btn-warning pull-right" href="<?php echo $this->Html->url(array('action' => 'logout',$project['Project']['id'])); ?>" role="button">ออกจากระบบ</a>	
					<?php
					}
					else{
					?> 
						<!-- <a class="btn btn-danger pull-right" href="<?php echo $this->Html->url(array('action' => 'login',$project['Project']['id'])); ?>" role="button"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> ยกเลิกการลงทะเบียน</a>	 -->
					<?php
					}
					?>
					
				</div>
			</div>
			<?php
			}
			?>
			
			
				
					
					<a class="btn btn-warning pull-left" href="<?php echo $this->Html->url(array('controller' => 'main','action' => 'index')); ?>" role="button">
					
					<span class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span>ย้อนกลับ</a>	
				
			
				<!-- <button class="btn btn-danger pull-right" style="cursor: none;">
						<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 
						ปิดรับการลงทะเบียน						
					</button> -->

					 <?php  if(DateDiff($date." ".$time,$project['Project']['dateout']." ".$project['Project']['timestop'] ) > 0 ) { ?>
						<a class="btn btn-primary pull-right" href="<?php echo $this->Html->url(array('action' => 'register',$project['Project']['id'])); ?>"
							role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
							ลงทะเบียนเพิ่มเติม
						</a>
					<?php }else { ?>
						
					<?php } ?> 
					<?php  if($UserName != null) { ?>
						<a class="btn btn-primary btn-lg pull-right" href="<?php echo $this->Html->url(array('action' => 'list_print',$project['Project']['id'])); ?>"
						target="_blank">
							<span class="glyphicon glyphicon-print" aria-hidden="true"></span> Print รายชื่อ
						</a>
					 
					<?php } ?>
			 
			<div id="no-more-tables"> 				
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th style="text-align: center;">ลำดับ</th>
							<th style="text-align: center;">รหัสนักศึกษา</th>
							<th style="text-align: center;">ชื่อ - นามสกุล</th>
							<th style="text-align: center;">สาขาวิชา</th>
							<th style="text-align: center;">หัวข้อภาษาไทย</th>
							<th style="text-align: center;">หัวข้อภาษาอังกฤษ</th>
							<th style="text-align: center;">ประเภท/หัวข้อ</th>
							<?php
							if($project['Type']['id'] == 2 && $project['Project']['price'] != 0){
							?>
								<th style="text-align: center;">สถานะ</th>
							<?php
							}
							
							if(isset($citizenId) || isset($admin)){
							?>
								<th style="text-align: center;">ยกเลิกการลงทะเบียน</th>
							<?php
							}
							
							?>
						</tr>
					</thead>
				 
					<tbody>
						<?php 
							$i=0;
							foreach($majordegrees as $major){ 
						?>
							<tr>
								<td data-title="กลุ่มโรงเรียน">
									<?php if($students != null){ ?>
										<b><?php echo $major['Majordegree']['major_name']; ?></b>
									<?php } ?>
								</td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<?php
								foreach($students as $student){
									if($major['Majordegree']['major_id'] == $student['MemberStudent']['major_id']){ 
									$i++
							?>
								<tr>
									<td data-title="ลำดับ" align="center"><?php echo $i; ?></td>
									<td data-title="รหัสนักศึกษา">
										<?php 
											echo $student['MemberStudent']['student_id']; 
										?>
									</td>
									<td data-title="ชื่อ - นามสกุล">
										<?php 
											echo $student['Prefix']['name'].$student['MemberStudent']['student_firstname'].' '.$student['MemberStudent']['student_lastname']; 
										?>
									</td>
									<td data-title="สาขาวิชา">
										<?php 
											echo $student['Major']['major_name']; 
										?>
										
									</td>
									<td data-title="หัวข้อภาษาไทย">
										<?php 
											echo $student['MemberStudent']['title']; 
										?>
										
									</td>
									<td data-title="หัวข้อภาษาอังกฤษ">
										<?php 
											echo $student['MemberStudent']['title_eng']; 
										?>
										
									</td>
									<td data-title="ประเภท/หัวข้อ">
										<?php 
											echo $student['Paper']['name']; 
										?>
										<?php 
											if(isset($citizenId) || isset($admin)){
												echo $student['Member']['phoneteacher']; 

											}
										?>
									</td>
									<?php  if($UserName != null) { ?>
										<td data-title="ประเภท/หัวข้อ">
											<?php foreach($studentDocuments as $key){
													if($student['MemberStudent']['student_id'] == $key['MemberStudentDocument']['student_id']){ 
													
											?>
												 
												<a href="<?php echo $this->Html->url('/files/paper/'.$key['MemberStudentDocument']['name']); ?>" class="btn btn-primary pull-right" target="_blank">
													<?php 
														echo $key['MemberStudentDocument']['name']; 
													?><br>
												</a>
												
												<?php } ?>
											<?php } ?>
										</td> 
									<?php } ?>
									 
									
									
								</tr>
						<?php 		
									}
								}
							} 
						?>
					</tbody>
				</table>
			</div>			
	
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
			 
		</div>
		<div class="col-md-6">	
			
			<div class="alert alert-info">
				ติดต่อสอบถามเพิ่มเติมได้ที่ งานบริการการศึกษา และพัฒนาคุณภาพนักศึกษา
				<br>
				คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่ โทรศัพท์ 053 - 944641-2, 053 - 944644 โทรสาร 053 - 944666  (ในวันเวลาราชการ)
			</div>
			
		</div>
	</div>
	
</div>
