<?php //echo debug($this->request->data)?>
<!-- start content here -->
<div class="container" style="padding:0 15px 0;">	
	<div class="row">
		<div class="col-md-12">
                  <div class="well">

                        <tbody>
                              <form method="post" accept-charset="utf-8">
                                    <div class="panel panel-success">
                                          <div class="panel-heading"> <br>
                                                <h3 class="panel-title"><i class="glyphicon glyphicon-plus"></i> เพิ่มข้อมูล</h3> 
                                          </div>
                                          <div class="panel-body">
                                                <?php
                                                      echo $this->Form->input('StudentTravel.id',array(
                                                            'hidden',
                                                            'label' => false,
                                                            'type' => 'text',
                                                      ));
                                                ?>
                                                <label class="col-sm-4 control-label">รหัสนักศึกษา</label>
                                                <div class="col-sm-8">
                                                      <?php
                                                            echo $this->Form->input('StudentTravel.student_id',array(
                                                                  'type' => 'text',
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'placeholder' => 'กรุณากรอกรหัสนักศึกษา',
                                                                  'required',
                                                            ));
                                                      ?>
                                                </div>
                                                <label class="col-sm-4 control-label">ชื่อ - นามสกุล</label>
                                                <div class="col-sm-8">
                                                      <?php
                                                            echo $this->Form->input('StudentTravel.stu_name',array(
                                                                  'type' => 'text',
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'placeholder' => 'ชื่อ นามสกุล',
                                                                  'required',
                                                            ));
                                                      ?>
                                                </div>
                                                <label class="col-sm-4 control-label">สาขา</label>
                                                <div class="col-sm-8">
                                                      <?php
                                                            echo $this->Form->input('StudentTravel.class',array(
                                                                  'type' => 'text',
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'placeholder' => 'สาขาที่เรียน',
                                                                  'required',
                                                            ));
                                                      ?>
                                                </div>
                                                <label class="col-sm-4 control-label">เดินทางไปยัง</label>
                                                <div class="col-sm-8">
                                                      <?php
                                                            echo $this->Form->input('StudentTravel.country',array(
                                                                  'type' => 'text',
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'placeholder' => 'ประเทศที่นักศึกษาที่เดินทางไปทำกิจกกรม',
                                                                  'required',
                                                            ));
                                                      ?>
                                                </div>
                                                <label class="col-sm-4 control-label">วันที่เดินทาง</label>
                                                <div class="col-sm-8">
                                                      <?php
                                                            echo $this->Form->input('StudentTravel.travel',array(
                                                                  'type' => 'text',
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'placeholder' => 'ปี-เดือน-วันที่',
                                                                  'required',
                                                            ));
                                                      ?>
                                                </div>
                                                <label class="col-sm-4 control-label">ระดับ</label>
                                                <div class="col-sm-8">
                                                      <?php
                                                            $degrees = array(
                                                                  '1' => '1',
                                                                  '3' => '3',
                                                                  '5' => '5',
                                                            );
                                                            echo $this->Form->input('StudentTravel.degree_id',array(
                                                                  'options' => $degrees,
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'placeholder' => '1 = ปริญญาตรี, 3 = ปริญญาโท, 5 = ปริญญาเอก',
                                                                  'required',
                                                            ));
                                                      ?>
                                                </div>
                                                <label class="col-sm-4 control-label">เดือน</label>
                                                <div class="col-sm-8">
                                                      <?php
                                                            $months = array(
                                                                  '1' => '1',
                                                                  '2' => '2',
                                                                  '3' => '3',
                                                                  '4' => '4',
                                                                  '5' => '5',
                                                                  '6' => '6',
                                                                  '7' => '7',
                                                                  '8' => '8',
                                                                  '9' => '9',
                                                                  '10' => '10',
                                                                  '11' => '11',
                                                                  '12' => '12',
                                                            );
                                                            echo $this->Form->input('StudentTravel.month',array(
                                                                  'options' => $months,
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'required',
                                                            ));
                                                      ?>
                                                </div>
                                                <label class="col-sm-4 control-label">ปี</label>
                                                <div class="col-sm-8">
                                                      <?php
                                                            echo $this->Form->input('StudentTravel.year',array(
                                                                  'type' => 'text',
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control cssrequire'),
                                                                  'error' => false,
                                                                  'placeholder' => '2567',
                                                                  'required',
                                                            ));
                                                      ?>
                                                </div>
                                                <br>
                                                <div class="form-group">
                                                      <input role="button" class="btn btn-success" type="submit" value="บันทึกข้อมูล" onclick="return confirm('ยืนยันการเพิ่มข้อมูล')">
                                                </div>
                                          </div>
                                    </div>
                              </form>
                        </tbody>

                  </div>
            </div>
      </div>
</div>
<div style="clear: both;"></div>