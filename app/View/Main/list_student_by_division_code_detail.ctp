
<title>
รายชื่อนักศึกษา ปีการศึกษา <?php echo $term;?> / <?php echo $year;?> รหัส <?php echo substr($yearId1,0,2) ?>  สาขาวิชา<?php echo $major['Major']['major_name']?> ระดับ <?php echo $degree['Degree']['degree_name'] ?>
</title>
<!-- Bootstrap core CSS -->
<?php echo $this->Html->css('/assets/bootstrap-4.5.3/dist/css/bootstrap.min.css'); ?>
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script>
			window.jQuery || document.write('<script src="../assets/bootstrap-4.5.3/dist/js/vendor/jquery.slim.min.js"><\/script>')
		</script>
		<?php echo $this->Html->script('/assets/bootstrap-4.5.3/dist/js/bootstrap.bundle.min.js'); ?>
		<!-- close bootstrap -->
		
		<!-- Datatable -->
		<?php
		echo $this->Html->css('/assets/DataTables/datatables.min.css');
		echo $this->Html->script('/assets/DataTables/datatables.min.js');
		?>
		<!-- clsoe Datatable -->

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.dataTables.min.css">

<style>
    th,
    td {
        vertical-align: top !important;
    }
</style>
	<?php 
		date_default_timezone_set('Asia/Bangkok');
		$date = date("Y-m-d");
		$time = date("H:i:s");

		function DateDiff($strDate1,$strDate2){                             
		return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
		}


	?>
<?php 
	$UserName = $this->Session->read('UserName');
	$admins = $this->Session->read('adminStudents');
	if ($admins == null) { ?>
		<div class="hero-container">
			<!-- <h1>Welcome to Faculty of Agriculture</h1> -->	
			<center style="margin-top: 12px;">
			<font class="font-44 ">
			ข้อตกลงและเงื่อนไขการใช้บริการเว็บไซต์ และนโยบายข้อมูลส่วนบุคคล</font> <br>
				
				<a href="http://www.ratchakitcha.soc.go.th/DATA/PDF/2560/A/010/24.PDF" target="_blank">
				พระราชบัญญัติ ว่าด้วยการกระทําความผิดเกี่ยวกับคอมพิวเตอร์ (ฉบับที่ 2) พ.ศ. 2560</a> <br>
				<a href="http://www.ratchakitcha.soc.go.th/DATA/PDF/2562/A/069/T_0052.PDF" target="_blank">
				พระราชบัญญัติ คุ้มครองข้อมูลส่วนบุคคล พ.ศ. 2562</a> 
			</center>
			
			<div class="container">
			
			<br>
				<div class="col-md-12 font-22">
					&emsp;&emsp;&emsp;เนื่องจากระบบฐานข้อมูลนักศึกษา คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่ เป็นระบบฐานข้อมูลที่เกี่ยวข้องกับข้อมูลส่วนบุคคลของนักศึกษา ซึ่งมีความเกี่ยวข้องกับ
					พระราชบัญญัติคุ้มครองข้อมูลส่วนบุคคล พ.ศ. 2562 และพระราชบัญญัติ ว่าด้วยการกระทําความผิดเกี่ยวกับคอมพิวเตอร์ (ฉบับที่ 2) พ.ศ. 2560 ตามประกาศในราชกิจจานุเบกษา
					จึงมีความจำเป็นที่จะต้องให้ผู้มีสิทธิ์ใช้งานระบบ ได้รับสิทธิ์ในการเข้าระบบเท่านั้น  <br><br>
					
				</div>
				
				
				</div>
			<br>
		</div>
<?php }else { ?>

 
	<?php //debug($degree) ?>
	<div class="container.fluid" style="padding:0 25px 0;">
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-tabs" role="tablist" style="margin-top:40px;">
					<li role="presentation" class="active">
						<a href="#news" aria-controls="news" role="tab" data-toggle="tab">
							<i class="glyphicon glyphicon-th-list"></i> <b>แสดงรายชื่อนักศึกษาที่กำลังศึกษาแยกตามสาขาวิชา </b>
						</a>
				
					</li>			
				</ul>
			</div>
		</div>
		<center>
			<?php if ($admins['Adminstudent']['employee_status_id'] == 1 || $admins['Adminstudent']['employee_status_id'] == 0 || $admins['Adminstudent']['employee_status_id'] == 5) { ?>				
				รายชื่อนักศึกษา ปีการศึกษา <?php echo $term;?> / <?php echo $year;?>   <br>
				<h4><b> รหัส <?php echo substr($yearId1,0,2) ?>  สาขาวิชา<?php echo $major['Major']['major_name']?> ระดับ <?php echo $degree['Degree']['degree_name'] ?></b>
				จำนวนนักศึกษา <b><?php echo $countAllListStudents ?></b> คน </h4>
			<?php }else{ ?> 
				รายชื่อนักศึกษา ปีการศึกษา <?php echo $term;?> / <?php echo $year;?>   <br>
				<h4><b> รหัส <?php echo substr($yearId1,0,2) ?>  สาขาวิชา<?php echo $major['Major']['major_name']?> ระดับ <?php echo $degree['Degree']['degree_name'] ?></b>
				<br>มีจำนวนนักศึกษาในภาควิชาทั้งหมด <b><?php echo $row ?></b> คน 
				<br><font color="red">ระบบกรองแสดงรายชื่อตามภาควิชาเท่านั้น ไม่สามารถดูรายชื่อภาควิชาอื่นได้</font>
					
				
			<?php } ?> 
		</center>
		<br>
		 
	</div>
	<div id="no-more-tables">
		<table class="table table-bordered table-hover nowrap" id="myTable" style="width: 100%">
			<thead>
				<tr> 
					<th align="center">ลำดับ</th>
					<th >รหัสนักศึกษา</th>
														
					<th >ชื่อ-นามสกุล</th>
					 
					<th >สาขาวิชา</th>
					<?php if($degree['Degree']['id'] == 3 || $degree['Degree']['id'] == 5) {?> <th>แผนการเรียน</th>  <?php } ?>
					<th >อาจารย์ที่ปรึกษา</th>
					<th >ปีการศึกษาที่เข้าศึกษา</th>
					<?php foreach ($yearterms as $key): ?>
								<td    style="text-align: center;padding-top: 2%;">
									<center>
										<?php  echo $key['Yearterm']['term'];  ?>/<?php  echo $key['Yearterm']['year'];  ?>
										 
									</center>
								</td>
					<?php endforeach ?>
					<?php if($admins['Adminstudent']['employee_status_id'] == 0 ||
									$admins['Adminstudent']['employee_status_id'] == 1 ||
									$admins['Adminstudent']['employee_status_id'] == 2 ||
									$admins['Adminstudent']['employee_status_id'] == 3 ||
									$admins['Adminstudent']['employee_status_id'] == 5 ){ ?>
								<th >ดูข้อมูลนักศึกษา</th>
								<th >เกรดนักศึกษา</th>
					<?php } ?>
				</tr>
				
			</thead>
			<tbody>
				<?php echo $output;  ?>		
			</tbody>
				
		</table>
	</div>

<?php } ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#myTable').DataTable({
            responsive: {
                details: false
            },
			paging: false,
            scrollX: true,
            ordering: false,
            responsive: false,
            language: {
                searchPlaceholder: "ค้นหา"
            },
            dom: 'Bfrtip',
            buttons: ['copy', 'excel', 'print'],
        });
    });

     
</script>