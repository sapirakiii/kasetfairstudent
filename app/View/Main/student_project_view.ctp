<div class="container">
    <div class="panel-header" align="center">
        <h3>จำนวนโครงการ Pitching/Start up ของนักศึกษาระดับปริญญาตรีและบัณฑิตศึกษา</h3>
    </div>
</div>
<div class="panel-body">
    <br>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th style="text-align:center;" colspan="" rowspan="" width="1%">ลำดับ</th>
                <th style="text-align:center;" colspan="" rowspan="" width="20%">หัวข้อ</th>
                <th style="text-align:center;" colspan="" rowspan="" width="5%">จำนวนโปรเจ็ค</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="text-align:center;">1</td>
                <td style="text-align:center;"><a target="_blank" href="<?php echo $this->Html->url(array('action' => 'student_project')); ?>">จำนวนโครงการ Pitching/Start up ของนักศึกษาระดับปริญญาตรีและบัณฑิตศึกษา</a></td>
                <td style="text-align:center;"><?php echo $student_projects; ?></td>
            </tr>
        </tbody>
    </table>
</div>