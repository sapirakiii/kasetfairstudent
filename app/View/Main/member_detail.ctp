<div class="container" style="padding:15px 0 0;">
	<div class="panel panel-primary" >
		<div class="panel-heading"> 
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center>ข้อมูลผู้ลงทะเบียน</center>
				</h3> 
			</div>
		</div>	
		<div class="panel-body" style="padding-top: 15px; ">
			<div align="center"> 	
				<h4><?php echo $project['Type']['name']; ?></h4>
				
				<h3><?php echo $project['Project']['name']; ?></h3> 
				<h3>ณ <?php echo $project['Project']['place']; ?> วันที่ <?php echo $project['Project']['date']; ?> เวลา <?php echo $project['Project']['time']; ?></h3>
			</div> 
			

			 
			<br>
			<a class="btn btn-warning pull-left" href="<?php echo $this->Html->url(array('action' => 'checklist',$project['Project']['id'])); ?>" role="button">			
				<span class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span>ย้อนกลับ
			</a>


			 				
			<table class="table table-bordered table-striped">
				<tbody>
					<tr>
						<td style="text-align: right; font-weight: bold;">ชื่อ - นามสกุล : </td>					
						<td><?php echo $member['Prefix']['name'].$member['Member']['fname'].' '.$member['Member']['lname']; ?></td>
					</tr>
					<?php 
					if ($project['Type']['id'] == 2) {
					?>
					<tr>
						<td style="text-align: right; font-weight: bold;">หมายเลขบัตรประชาชน : </td>					
						<td><?php echo $member['Member']['citizen_id']; ?></td>
					</tr>
					<?php 
					}
					?>
					<tr>
						<td style="text-align: right; font-weight: bold;">อายุ : </td>					
						<td><?php echo $member['Member']['age']; ?></td>
					</tr>
					<tr>
						<td style="text-align: right; font-weight: bold;">เบอร์โทรศัพท์ : </td>					
						<td><?php echo $member['Member']['phone']; ?></td>
					</tr>
					<tr>
						<td style="text-align: right; font-weight: bold;">อาชีพ : </td>					
						<td><?php echo $member['Member']['career']; ?></td>
					</tr>
					<tr>
						<td style="text-align: right; font-weight: bold;">E-mail : </td>					
						<td><?php echo $member['Member']['email']; ?></td>
					</tr>
				</tbody>
			</table>
						
	
		</div>

	</div>
	
</div>

