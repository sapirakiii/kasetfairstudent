<div class="container" style="padding:15px 0 0;">
	<div class="panel panel-primary" >
		<div class="panel-heading"> 
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center>ลงทะเบียน</center>
				</h3> 
			</div>
		</div>	
		<div class="panel-body" style="padding-top: 15px; ">
			<div align="center"> 	
				<h4><?php echo $project['Type']['name']; ?></h4>
				
				<h3><?php echo $project['Project']['name']; ?></h3>
			</div> 
			
			<br>
			
			
			
			<?php 
			//debug($this->request->data);
			$required = 'กรุณากรอกข้อมูล';
			
			echo $this->Form->create('Member',array('role' => 'form','data-toggle' => 'validator')); 
			
			?>
				<div class="row">
				  <div class="form-group col-md-4">
					<label>คำนำหน้า <span style="color: red;">*</span></label>

					<?php
				
					echo $this->Form->input('prefix_id', array(
						'options' => $prefixes,
						'class' => 'form-control',
						'label' => false,
						'required' => true,
						'data-error' => $required,
						'empty' => 'กรุณาเลือกคำนำหน้า'
					));
					
					?>
					
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-md-4">
					<label>ชื่อ <span style="color: red;">*</span></label>

					<?php echo $this->Form->input('fname', array('type' => 'text','label' => false,'class' => 'form-control','data-error' => $required,'required' => true)); ?>
					
					<div class="help-block with-errors"></div>
				  </div>
				  <div class="form-group col-md-4">
					<label>นามสกุล <span style="color: red;">*</span></label>

					<?php echo $this->Form->input('lname', array('type' => 'text','label' => false,'class' => 'form-control','data-error' => $required,'required' => true)); ?>
					
					<div class="help-block with-errors"></div>					
				  </div>
				</div>
				
				
				<div class="row">
				<?php 
				if ($project['Type']['id'] == 2) {
				?>
					 
					
				  <div class="form-group col-md-4">
					<label>หมายเลขบัตรประชาชน <span style="color: red;">*</span></label>

					<?php echo $this->Form->input('citizen_id', array('type' => 'text','data-error' => $required,'label' => false,'class' => 'form-control','required' => true,'maxlength' => '13')); ?>
					
					<div class="help-block with-errors"></div>
				  </div>
				<?php	
				} 
				
				if ($project['Type']['id'] == 7) {
				?>
				  <div class="form-group col-md-8">
					<label>ที่อยู่ <span style="color: red;">*</span></label>

					<?php echo $this->Form->input('address', array('type' => 'text','label' => false,'class' => 'form-control','data-error' => $required,'required' => true)); ?>
					
					<div class="help-block with-errors"></div>	
				  </div>
				<?php	
				} 
				else{
				?>
				  <div class="form-group col-md-4">
					<label>อายุ <span style="color: red;">*</span></label>

					<?php echo $this->Form->input('age', array('type' => 'number','min' => '0','label' => false,'class' => 'form-control','data-error' => $required,'required' => true)); ?>
					
					<div class="help-block with-errors"></div>	
				  </div>
				<?php	
					
				}
				?>
				 
				  
				  <div class="form-group col-md-4">
					<label>เบอร์โทรศัพท์ </label>

					<?php echo $this->Form->input('phone', array('type' => 'number','min' => '0','label' => false,'class' => 'form-control','data-error' => $required)); ?>
					
					<div class="help-block with-errors"></div>					
				  </div>
				  
				</div>
				
				<div class="row">	
				<?php 
				if ( $project['Type']['id'] == 3) { 
				?>
						<div class="form-group col-md-6">
						<label>โรงเรียน <span style="color: red;">*</span></label>

						<?php echo $this->Form->input('school', array('type' => 'text','label' => false,'class' => 'form-control','data-error' => $required,'required' => true)); ?>
						
						<div class="help-block with-errors"></div>
						</div>
				<?php 
				}
				else if ( $project['Type']['id'] != 7) { 
				?>	
				
				  <div class="form-group col-md-6">
					<label>อาชีพ <span style="color: red;">*</span></label>

					<?php echo $this->Form->input('career', array('type' => 'text','label' => false,'class' => 'form-control','data-error' => $required,'required' => true)); ?>
					
					<div class="help-block with-errors"></div>
				  </div>			
				<?php	
				} 
				?>
					
					<?php if ( $project['Type']['id'] == 3) { ?>
						<div class="form-group col-md-6">
					<label>ระดับ <span style="color: red;">*</span></label>
						
					<?php 
					$types = array('ป.1-6' => 'ป.1-6', 'ม.1-3' => 'ม.1-3', 'ม.4-6' => 'ม.4-6');
					echo $this->Form->input('level', array(
						'options' => $types,
						'data-error' => $required,
						'label' => false,
						'class' => 'form-control',
						'required' => true)); ?>				
										
					<div class="help-block with-errors"></div>
				  </div>
				</div>
				<div class="form-group col-md-6">
					<label>อาจารย์ ผู้ควบคุม <span style="color: red;">*</span></label>

					<?php echo $this->Form->input('teacher', array('type' => 'text','label' => false,'class' => 'form-control','data-error' => $required,'required' => true)); ?>
					
					<div class="help-block with-errors"></div>
				  </div>
					<div class="form-group col-md-6">
					<label>เบอร์โทรอาจารย์ ผู้ควบคุม </label>

					<?php echo $this->Form->input('phoneteacher', array('type' => 'text','label' => false,'class' => 'form-control','data-error' => $required)); ?>
					
					<div class="help-block with-errors"></div>
				  </div>
						<?php }else { ?>	 
							<div class="form-group col-md-6">
							<label>E-mail</label>
								
							<?php echo $this->Form->input('email', array('type' => 'email','data-error' => $required,'label' => false,'class' => 'form-control')); ?>						
												
							<div class="help-block with-errors"></div>
							</div>
						</div>
				<?php	} ?>
				
				
				<?php	
				if ($project['Type']['id'] == 9) {
				
					$category = 'ประเภทประกวด';
					// if ($project['Project']['id'] == 32){
					// 	$category = 'ประเภทประกวด';
					// }
						
				?>
					<div class="row">				  
					  <div class="form-group col-md-12">
						
						<label><?php echo $category; ?></label>
						
						<div style="padding: 0 20px;">
							<table class="table table-bordered table-striped">
								<tbody>
									<?php
									
									foreach ($AquaticGroups as $AquaticGroup){
											
									?>
											
										
										<tr>
											<td><?php echo $AquaticGroup['AquaticGroup']['name']; ?></td>	
											<td>
												<table>
													<tbody>
														<tr>
															<td>จำนวน</td>
															<td width="250px">
																<?php echo $this->Form->input('aquatic_group_id.'.$AquaticGroup['AquaticGroup']['id'], array('type' => 'number','min' => '0','label' => false,'class' => 'form-control','value' => 0)); ?>
															</td>
															<td>ตัว</td>
														</tr>
													</tbody>
												</table>
												
											</td>	
										</tr>
										

									<?php
										
									}
											
									?>
								</tbody>
							</table>
						</div>	
					  </div>				  
					</div>
				<?php	
				} 
				?>
				
				<br>
				<div class="row">				  
				  <div class="form-group col-md-6">
					
					<label>ท่านทราบข่าวสารจากแหล่งไหน <span style="color: red;">*</span><span style="font-weight:normal;"> (สามารถตอบมากกว่า 1 ข้อ)</span></label>
					
					<?php
						
					foreach ($sources as $source){
							
					?>
					
					<div class="checkbox" style="margin-left:20px;">
						<label>
							<?php echo $this->Form->checkbox('source_id.'.$source['Source']['id'], array('value' => $source['Source']['id'],'hiddenField' => false,'data-error' => $required,'required' => true)); ?>
						</label>
						
						<span style="vertical-align: top;"><?php echo $source['Source']['name']; ?></span>
						
					</div>	
					
					<?php
						
					}
							
					?>

					
					<div class="help-block with-errors"></div>
				  </div>				  
				</div>
				

				<div class="row">
					<div class="col-md-12">
						<input type="submit" value="คลิกส่งข้อมูลการลงทะเบียน" class="btn btn-success btn-lg btn-block"> 
					</div>
				</div>
			</form>
	
		</div>

	</div>
	
</div>

<script type="text/javascript">
	$('.form-group').on("click",'input:checkbox',function(){          
		checkboxValidate($(this).attr('name'));
	});
	function checkboxValidate(name){
		var min = 1 //minumum number of boxes to be checked for this form-group
		if($('input:checked').length<min){
			$('input:checkbox').prop('required',true);
		}
		else{
			$('input:checkbox').prop('required',false);
		}
		
		$('#MemberRegisterForm').validator('validate');
		
		// if($('input[name="'+name+'"]:checked').length<min){
			// $('input[name="'+name+'"]').prop('required',true);
		// }
		// else{
			// $('input[name="'+name+'"]').prop('required',false);
		// }
	}
</script>