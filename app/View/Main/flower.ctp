<?php //echo debug($projects);?>
<div class="container" style="padding:15px 0 0;">
<div class="panel panel-default" style="border-color: #F2F2F2!important">
	<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center><div class="font_panel2">ประกวดไม้ดอกไม้ประดับ</div></center>
				</h3> 
			</div>
		</div>	
		<div class="panel-body" style="padding-top: 15px; ">
			<div align="center"> 	
				<h4>งานเกษตรภาคเหนือ ครั้งที่ 8 วันที่ 8-12 พฤศจิกายน 2560</h4>
				<br>
			</div> 
			<div id="no-more-tables"> 				
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th style="text-align: center;">ลำดับ</th>
							<th style="text-align: center;">ประเภทไม้ประกวด</th>
			
							<th style="text-align: center;">ลงทะเบียน</th>
							<!-- <th style="text-align: center;">ตรวจสอบรายชื่อ</th> -->
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 0;
							foreach ($projects as $project){
								$i++;
							?>
								<tr>
									<td data-title="ลำดับ" align="center"><?php echo $i; ?></td>
									<td data-title="ประเภทไม้ประกวด">
										<?php 
										if($project['Project']['file_name'] != null){
										?>
											<a href="<?php echo $this->Html->url('/files/'.$project['Project']['file_name']); ?>" target="_blank">
												<?php echo $project['Project']['name']; ?>
											</a>
										<?php 
											
										}
										else{
											echo $project['Project']['name']; 
										}
										
											
										?>
										
									</td>
									
									<td data-title="ลงทะเบียน" style="text-align: center;">	
									<button class="btn btn-danger" style="cursor: none;">
												<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 
												ปิดการลงทะเบียน						
										</button>							
										<!-- <a class="btn btn-primary" href="<?php echo $this->Html->url(array('action' => 'register',$project['Project']['id'])); ?>" role="button" target="_blank">ลงทะเบียน</a>					 -->
									</td>
									<!-- <td data-title="ตรวจสอบรายชื่อ" style="text-align: center;">								
										<a class="btn btn-success" href="<?php echo $this->Html->url(array('action' => 'checklist',$project['Project']['id'])); ?>" role="button" target="_blank">ตรวจสอบรายชื่อ</a>								
									</td> -->
								</tr>
							<?php
							}
						?>						
						
					</tbody>
				</table>
			</div>
	
		</div>

	</div>
	
	
</div>
