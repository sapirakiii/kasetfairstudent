<div class="container">	
	<div class="row">
		<div class="col-md-12">
                  <div class="panel panel-success">
                        <div class="panel-heading">
                              <h3 class="panel-title"><i class="glyphicon glyphicon-plus"></i> เพิ่มรายการ</h3>
                        </div>
                        <div class="panel-body">
                              <form method="post">
                                          <?php
                                                echo $this->Form->input('Notification.id',array(
                                                      'hidden',
                                                      'type' => 'hidden',
                                                ));
                                          ?>
                                    <label class="control-label">หัวข้อ</label>
                                    <div class="form-group has-feedback">
                                         <?php
                                                echo $this->Form->input('Notification.title',array(
                                                      'type' => 'textarea',
                                                      'div' => false,
                                                      'label' => false,
                                                      'class' => array('form-control css-require'),
                                                      'placeholder' => 'หัวข้อข่าว',
                                                      'required'
                                                ));
                                         ?>
                                    </div>
                                    <label class="control-label">รายละเอียดข่าว</label>
                                    <div class="form-group has-feedback">
                                         <?php
                                                echo $this->Form->input('Notification.name',array(
                                                      'type' => 'textarea',
                                                      'div' => false,
                                                      'label' => false,
                                                      'class' => array('form-control css-require'),
                                                      'placeholder' => 'รายละเอียดข่าว',
                                                      'required'
                                                ));
                                         ?>
                                    </div>
                                    <label class="control-label">ตั้งค่าแสดงผล</label>
                                    <div class="form-group has-feeback">
                                          <?php
                                                $level = array(
                                                      '1' => 'แสดงผล',
                                                      '0' => 'ไม่แสดงผล',
                                                );
                                                echo $this->Form->input('Notification.level',array(
                                                      'options' => $level,
                                                      'div' => false,
                                                      'label' => false,
                                                      'class' => array('form-control css-require'),
                                                ));
                                          ?>
                                    </div>
                                    <div class="btn-group">
                                          <input type="submit" value="บันทึก" class="btn btn-success" onclick="return confirm('ยืนยันการบันทึกข้อมูล')">
                                    </div>
                                    <div class="btn-group">
                                          <a class="btn btn-danger" role="button" href="<?php echo $this->Html->url(array('action'=>'notification')); ?>">ยกเลิก</a>
                                    </div>
                              </form>
                        </div>
                  </div>
            </div>
      </div>
</div>
<div style="clear: both;"></div>