<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta content="Faculty of Agriculture , Chiangmai University" name="author" />
	<meta content="" name="keywords" />
	<meta content="" name="keywords" lang="th"/>
	<meta content="" name="description" />
	<meta name="robots" content="noindex">
	<meta name="googlebot" content="noindex">
	<link href="/smart_academic/img/Logo_ENG.png" type="image/x-icon" rel="icon" /><link href="/smart_academic/img/Logo_ENG.png" type="image/x-icon" rel="shortcut icon" /><link rel="stylesheet" type="text/css" href="/smart_academic/css/agri.css" />
	
	<title>Faculty of Agriculture , Chiangmai University</title>

	<!-- Bootstrap core CSS -->
	<link rel="stylesheet" type="text/css" href="/smart_academic/assets/bootstrap/dist/css/bootstrap.min.css" />
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<link rel="stylesheet" type="text/css" href="/smart_academic/assets/bootstrap/assets/css/ie10-viewport-bug-workaround.css" />		
	<!-- Custom styles for this template -->
	<link rel="stylesheet" type="text/css" href="/smart_academic/assets/bootstrap/sticky-footer-navbar.css" />
	<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
	<!--[if lt IE 9]><script type="text/javascript" src="/smart_academic/assets/bootstrap/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
	<script type="text/javascript" src="/smart_academic/assets/bootstrap/assets/js/ie-emulation-modes-warning.js"></script>
	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script type="text/javascript" src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>			<script type="text/javascript" src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>		<![endif]-->
	
	<script type="text/javascript" src="/smart_academic/assets/bootstrap/assets/js/vendor/jquery.min.js"></script>			
		
	<script type="text/javascript" src="/smart_academic/assets/countdown_v5.3/countdown.js"></script>			
	<script>
		(function(i, s, o, g, r, a, m) {
			i['GoogleAnalyticsObject'] = r;
			i[r] = i[r] || function() {
				(i[r].q = i[r].q || []).push(arguments)
			}, i[r].l = 1 * new Date();
			a = s.createElement(o),
				m = s.getElementsByTagName(o)[0];
			a.async = 1;
			a.src = g;
			m.parentNode.insertBefore(a, m)
		})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

		ga('create', 'UA-54232567-2', 'auto');
		ga('send', 'pageview');
	</script>
</head>
<body style="margin-bottom: 160px;">
	<div class="container-fluid">
		<!-- Fixed navbar -->
		<div class="row">			
			<nav class="navbar navbar-default" style="min-height:auto; margin-bottom:0; border:0; border-radius:0;">
				<div class="container">
					<div class="navbar-header col-md-7">    
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="/smart_academic/internships" style="height:90px; padding:15px 25px;">
							<img src="/smart_academic/img/sys_6.png" id="logo" class="img-responsive" width="100%"/>
						</a>	
					</div>	
					<ul class="nav navbar-nav navbar-right navbar-nav2">
					</ul>
				</div>
			</nav>			
		</div>
		<div class="row">
			<nav class="navbar navbar-default" style="background-color:#7a52b5; margin-bottom:0; min-height:35px; border:0; border-radius:0;">
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-menu" aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div id="navbar-menu" class="collapse navbar-collapse">
						<ul class="nav navbar-nav navbar-nav3">			
							<li><a href="/smart_academic/internships">กำหนดการ</a></li>
							<li><a href="/smart_academic/internships/project">หัวข้อและรายละเอียดโครงการ</a></li>
							<li><a href="/smart_academic/internships/register">ลงชื่อเลือกโครงการ</a></li>
							<li><a href="/smart_academic/internships/checklist">ตรวจสอบรายชื่อโครงการที่เลือก</a></li>
							<li> 
								<a href="/smart_academic/files/รูปแบบรายงานการฝึกงาน 2 กระบวนวิชา 400290 .pdf" target="_blank"> รูปแบบรายงานการฝึกงาน <span class="glyphicon glyphicon-cloud-download"></span></a>
							</li>
						</ul>
					</div><!--/.nav-collapse -->
				</div>
			</nav>
		</div>
			
	</div>	
	<!-- Begin page content -->

	<div class="container"   style="padding-top: 0;">
		<style>
			p {
			text-align: center;
			font-size: 60px;
			margin-top: 0px;
			}
		</style>
		<font  class="font_panel2"><center>  นับถอยหลังเปิดลงทะเบียนฝึกงาน 2 กระบวนวิชา 400290 </center></font >
		<!-- Display the countdown timer in an element -->
		<p id="demo"></p>
		<script>
			// Set the date we're counting down to
			var countDownDate = new Date("Jun 02, 2022 13:30:00").getTime();

			// Update the count down every 1 second
			var x = setInterval(function() {

			// Get today's date and time
			var now = new Date().getTime();

			// Find the distance between now and the count down date
			var distance = countDownDate - now;

			// Time calculations for days, hours, minutes and seconds
			var days = Math.floor(distance / (1000 * 60 * 60 * 24));
			var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			var seconds = Math.floor((distance % (1000 * 60)) / 1000);

			// Display the result in the element with id="demo"
			document.getElementById("demo").innerHTML = days + "วัน " + hours + "ชั่วโมง "
			+ minutes + "นาที " + seconds + "วินาที ";

			// If the count down is finished, write some text
			if (distance < 0) {
				clearInterval(x);
				document.getElementById("demo").innerHTML = "EXPIRED";
			}
			}, 1000);
		</script>
	
		<div class="page-header">
			<h4>โครงการฝึกงานเกษตรแบบครบวงจร ประจำปีการศึกษา 2565</h4>
			โปรดศึกษาโครงการฝึกงานที่ต้องการล่วงหน้าให้ถี่ถ้วน เพื่อเตรียมความพร้อมก่อนลงทะเบียนฝึกงาน 2 กระบวนวิชา 400290
		</div>	
		โครงการที่เข้าร่วมมีทั้งหมด  27  โครงการ  :  ฝึกงาน  100  ชั่วโมง
		<div class="table-responsive"> 
			<table class="table table-bordered table-striped"> 
				<thead> 
					<tr> 
						<th class="text-center" rowspan="4" style="vertical-align: middle;" >ลำดับ</th> 
						<th class="text-center" rowspan="4" style="vertical-align: middle;" width="20%">หน่วยงาน</th> 
						<th class="text-center" rowspan="4" style="vertical-align: middle;" width="30%">ชื่อโครงการ</th> 
						<th class="text-center" rowspan="4" style="vertical-align: middle;" width="10%">รูปแบบการฝึกงาน</th> 
						<th class="text-center" colspan="9" style="vertical-align: middle;" width="40%">ช่วงเวลาฝึก / จำนวนรับ (คน) </th> 
					</tr> 
					<tr> 
						<th class="text-center" colspan="3"  width="10%">ภาคเรียนที่ 1</th> 
						<th class="text-center" colspan="3"  width="10%">ปิดภาคเรียนที่ 1 </th> 
						<th class="text-center" colspan="3"   width="10%">ภาคเรียนที่ 2</th> 
					</tr> 
				</thead> 
				<tbody> 
					<tr> 
						<td class="text-center" >1</td>
						<td >ภาควิชากีฏวิทยาและโรคพืช</td>
						<td >โครงการ “เรียนรู้โลกแมลง”</td> 
						<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 
					<tr> 
						<td class="text-center" >2</td>
						<td >ภาควิชากีฏวิทยาและโรคพืช</td>
						<td >โครงการ “เรียนรู้เชื่อมโยงถึงความสำคัญของหมอพืชและงานด้านโรคพืชต่อการเกษตร”  </td> 
						<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 

						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 
					<tr> 
						<td class="text-center" rowspan = "1">3</td>
						<td rowspan = "1">ภาควิชาพัฒนาเศรษฐกิจการเกษตร</td>
						<td rowspan = "1">โครงการ  "เสริมสร้างทักษะการเรียนรู้และทำงานร่วมกับชุมชน”</td>
						<td rowspan = "1"><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td>  

						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr>
					<tr> 
						<td class="text-center" >4</td>
						<td >ภาควิชาพัฒนาเศรษฐกิจการเกษตร</td>
						<td >โครงการ "การประกอบธุรกิจเกี่ยวข้องกับสินค้าเกษตร ในยุค New Normal"</td> 
						<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 
							
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 
					<tr> 
						<td class="text-center" >5</td>
						<td >ภาควิชาสัตวศาสตร์และสัตวน้ำ</td>
						<td >โครงการ “การจัดการและการผลิตสุกร" <br><font color="red">หมายเหตุ : ปฏิบัติงานในวันพุธ หรือ วันเสาร์ และอาทิตย์</font></td> 
						<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 
							
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 
					<tr> 
						<td class="text-center" >6</td>
						<td >ภาควิชาสัตวศาสตร์และสัตวน้ำ</td>
						<td >โครงการ “การจัดการและการผลิตสัตว์ปีก"</td> 
						<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 
							
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 
					<tr> 
						<td class="text-center" >7</td>
						<td >ภาควิชาสัตวศาสตร์และสัตวน้ำ</td>
						<td >โครงการ “การจัดการและการผลิตโคนม โคเนื้อ แปลงหญ้า แพะ แกะ และกระต่าย" <br><font color="red">หมายเหตุ : เวียนหมวดสำหรับการฝึก ปฏิบัติงานในวันพุธ วันเสาร์และอาทิตย์</font></td> 
						<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 
							
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 
					<tr> 
						<td class="text-center" >8</td>
						<td >ภาควิชาสัตวศาสตร์และสัตวน้ำ</td>
						<td >โครงการ “การจัดการและการผลิตสัตว์น้ำ"</td> 
						<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 
							
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 
					<tr> 
							<td class="text-center" >9</td>
							<td >ภาควิชาพืชศาสตร์และปฐพีศาสตร์</td>
							<td >โครงการ "การผลิตไม้ดอกเป็นการค้า"</td> 
							<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 
								
							<td class="text-center" colspan="3"><a href="">Link</a></td>
							<td class="text-center" colspan="3"><a href="">Link</a></td> 
							<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 
					<tr> 
						<td class="text-center" rowspan = "1">10</td>
						<td rowspan = "1">ภาควิชาพืชศาสตร์และปฐพีศาสตร์</td>
						<td rowspan = "1">โครงการ "การผลิตกล้วยไม้คุณภาพ"</td>
						<td rowspan = "1"><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td>  
						
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr>
					<tr> 
						<td class="text-center" >11</td>
						<td >ภาควิชาพืชศาสตร์และปฐพีศาสตร์</td>
						<td >โครงการ "การผลิตเมลอนและเห็ดครบวงจร"  <br><font color="red">หมายเหตุ : ถ้าจำนวนนักศึกษาต่ำกว่า 3 คน (ไม่สามารถรับจัดฝึกงานได้)</font></td> 
						<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 
							
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 
					<tr> 
						<td class="text-center" >12</td>
						<td >ภาควิชาพืชศาสตร์และปฐพีศาสตร์</td>
						<td >โครงการ "การสำรวจและการศึกษาการใช้ประโยชน์สมุนไพรพื้นบ้านในภาคเหนือ"</td> 
						<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 
							
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 
					<tr> 
						<td class="text-center" >13</td>
						<td >ภาควิชาพืชศาสตร์และปฐพีศาสตร์</td>
						<td >โครงการ "การปลูกและการผลิตต้นพันธุ์สตรอว์เบอรีปลอดโรค"</td> 
						<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 
							
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 
					<tr> 
						<td class="text-center" >14</td>
						<td >ภาควิชาพืชศาสตร์และปฐพีศาสตร์</td>
						<td >โครงการ "ปฐพีศาสตร์ครบวงจรสำหรับสวนไม้ผลผสมผสาน"</td> 
						<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 
							
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 
					<tr> 
						<td class="text-center" >15</td>
						<td >ภาควิชาพืชศาสตร์และปฐพีศาสตร์</td>
						<td >โครงการ "การผลิตพืชไร่แบบครบวงจร"</td> 
						<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 
							
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 
					<tr> 
						<td class="text-center" >16</td>
						<td >ภาควิชาเกษตรที่สูงและทรัพยากรธรรมชาติ</td>
						<td >โครงการ  "ภาคเกษตรที่สูงและทรัพยากรธรรมชาติ"</td> 
						<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 
							
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 
					<tr> 
						<td class="text-center" rowspan = "1">17</td>
						<td rowspan = "1">ศูนย์วิจัยข้าวล้านนา</td>
						<td rowspan = "1">โครงการ  "นวัตกรรมการผลิตและเพิ่มมูลค่าข้าวพื้นเมืองคุณภาพสูงแบบครบวงจร "				</td>
						<td rowspan = "1"><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td>  
						
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr>
					<tr> 
						<td class="text-center" >18</td>
						<td >งานบริหารงานวิจัยและวิเทศสัมพันธ์</td>
						<td >โครงการ “นักวิทยาศาสตร์เกษตร” </td> 
						<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 
							
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 
					<tr> 
						<td class="text-center" >19</td>
						<td >ศูนย์บริการวิชาการและถ่ายทอดเทคโนโลยีการเกษตร</td>
						<td >โครงการ "ธุรกิจเกษตร :  ร้าน CMU Steak and Coffee -  ร้านเกษตร มช. (ฝึกงานวันพุธ)" </td> 
						<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 
							
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 
					<tr> 
						<td class="text-center" >20</td>
						<td >ศูนย์บริการวิชาการและถ่ายทอดเทคโนโลยีการเกษตร</td>
						<td >โครงการ “การบริการวิชาการองค์ความรู้ทางการเกษตรสู่ชุมชน"	เน้นการเขียนบทความ		</td> 
						<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 
							
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 
					<tr> 
						<td class="text-center" >21</td>
						<td >ศูนย์วิจัยระบบทรัพยากรเกษตร</td>
						<td >โครงการ “การผลิตพืชผักปลอดสารพิษแบบครบวงจร"</td> 
						<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 
							
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 
					<tr> 
						<td class="text-center" >22</td>
						<td >ศูนย์วิจัยระบบทรัพยากรเกษตร</td>
						<td >โครงการ "ไม้ผล"</td> 
						<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 
							
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 
					<tr> 
						<td class="text-center" >23</td>
						<td >ศูนย์ วิจัย สาธิตและฝึกอบรมการเกษตรแม่เหียะ</td>
						<td >โครงการ “การผลิตปุ๋ยอินทรีย์ – ชีวภาพและวัสดุปลูกสำหรับใช้ในระบบการผลิตพืชที่เป็นมิตรสิ่งแวดล้อมแบบครบวงจร"</td> 
						<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 
							
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 
					<tr> 
						<td class="text-center" >24</td>
						<td >ศูนย์ วิจัย สาธิตและฝึกอบรมการเกษตรแม่เหียะ</td>
						<td >โครงการ "เทคนิคเบื้องต้นในการใช้เครื่องจักรกลเกษตรแบบครบวงจร"</td> 
						<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 
							
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 
					<tr> 
						<td class="text-center" >25</td>
						<td >ศูนย์ วิจัย สาธิตและฝึกอบรมการเกษตรแม่เหียะ</td>
						<td >โครงการ  “การจัดการการผลิตลำไยและมะม่วงแบบครบวงจร"</td> 
						<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 
							
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 
					<tr> 
						<td class="text-center" >26</td>
						<td >ศูนย์วิจัยและฝึกอบรมที่สูง</td>
						<td >โครงการ  “การปลูกและผลิตกาแฟอะราบิกาแบบครบวงจร” <br><font color="red"> ฝึกงานวันที่ 7 -19 พฤศจิกายน 2565</font></td> 
						<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 
							
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 
					<tr> 
						<td class="text-center" >27</td>
						<td >ศูนย์วิจัยเทคโนโลยีหลังการเก็บเกี่ยว</td>
						<td >โครงการ " การจัดการผลิตผลเกษตรด้วยเทคโนโลยีหลังการเก็บเกี่ยว"</td> 
						<td ><span class="glyphicon glyphicon-road" aria-hidden="true"></span> on-site</td> 
							
						<td class="text-center" colspan="3"><a href="">Link</a></td>
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
						<td class="text-center" colspan="3"><a href="">Link</a></td> 
					</tr> 			
				</tbody> 
			</table> 	
		</div>
	</div>
</body>
<footer class="footer" style="height: auto; background-color: #7a52b5; position: inherit;">
	<nav class="navbar navbar-default navbar-nav3 " style="background-color:#E1BB00; margin-bottom:0; min-height:8px; border:0; border-radius:0;">
	</nav>
	<div id="cmu_global_foot_info" class="container-fluid">
		<div class="container">
			<div id="cmu_global_logo" class="pull-left"> 
				<a href="/smart_academic/mains/login" >
					<img src="/smart_academic/img/Logo_Thai.png" title="AGRI Logo"/> 	
				</a>	
			</div>	
			<div class="pull-left col-lg-3">
				<h1 style="color:#FFF !important;">AGRI CMU</h1>
				<h5 style="color:#FFF !important;">	  				
						คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่			
				</h5>
			</div>
			<div id="cmu_global_info" class="pull-left">
				<p> &copy; <script>document.write(new Date().getFullYear())</script>  Practical Training II System  v. 4.0   All rights reserved.<br />

				</p>
				<p>Faculty of Agriculture , Chiang Mai University<br />
					239, Huay Kaew Road, Muang District, Chiang Mai, Thailand, 50200 <br />
					Tel : 0-5394-4641 ,Fax : 0-5394-4666 <br>
					contact : General officer Tel : 0-5394-4641-4 ext 111  <br>
					contact : Programme Scientist Tel : 0-5394-4641-4 ext 119  <br>
				</p>
			</div>
			<div id="cmu_global_info" class="pull-right">

				<p> <a href="http://www.agri.cmu.ac.th/main/" target="_blank" class="pull-right" 
				style="color:#FFF !important;">
			
					</a> 
				</p>

			</div>
		</div>
	</div>
	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<script type="text/javascript" src="/smart_academic/assets/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script type="text/javascript" src="/smart_academic/assets/bootstrap/assets/js/ie10-viewport-bug-workaround.js"></script>
</footer>
</html>
