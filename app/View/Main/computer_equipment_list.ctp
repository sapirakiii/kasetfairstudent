<div style="margin-top: 15px">
    <center><h3>สำรวจอุปกรณ์คอมพิวเตอร์</h3></center>
</div>
<div align="center" style="margin: 20px">
	<a class="btn btn-lg btn-primary" href="<?php echo $this->Html->url(array('action' => 'computer_equipment_form')); ?>" role="button" target="_blank">แบบสำรวจอุปกรณ์คอมพิวเตอร์</a>					
</div>
<div class="container">
    <table class="table table-bordered table-striped">
        <thead style="text-align:center">
            <th>ลำดับ</th>
            <th>ประเภทคอม</th>
            <th>ห้องเรียน</th>
            <th>CPU</th>
            <th>HDD</th>
            <th>SSD</th>
            <th>รหัสครุภัณฑ์</th>
            <th>ประเภทคอมที่ใช้</th>
            <th>ผู้ใช้งาน</th>
        </thead>
        <?php
            foreach($ComputerEquipments as $ComputerEquipment) {
        ?>
        <tr>
            <td><?php echo $ComputerEquipment['ComputerEquipment']['id'] ?></td>
            <td><?php echo $ComputerEquipment['ComType']['name'] ?></td>
            <td><?php echo $ComputerEquipment['ComputerEquipment']['room'] ?></td>
            <td><?php echo $ComputerEquipment['ComputerEquipment']['cpu'] ?></td>
            <td><?php echo $ComputerEquipment['ComputerEquipment']['HDD'] ?></td>
            <td><?php echo $ComputerEquipment['ComputerEquipment']['SSD'] ?></td>
            <td><?php echo $ComputerEquipment['ComputerEquipment']['code'] ?></td>
            <td><?php echo $ComputerEquipment['ComUse']['name'] ?></td>
            <td><?php echo $ComputerEquipment['MisEmployee']['fname_th']." ".$ComputerEquipment['MisEmployee']['lname_th'] ?></td>
        </tr>
        <?php } ?>
    </table>
</div>