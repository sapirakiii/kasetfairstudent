<div class="container">
    <div class="panel-header" align="center">
        <h3>จำนวนโครงการ Pitching/Start up ของนักศึกษาระดับปริญญาตรีและบัณฑิตศึกษา</h3>
    </div>
</div>
<div class="panel-body">
<br>

		<?php if($admins != null){ ?>
		<a class="btn btn-success" href="<?php echo $this->Html->url(array('action' => 'add_student_project'));?>" role="button" target="_blank">เพิ่มข้อมูล</a><br>
        <?php } ?>
		กรุณาเลือกปีการศึกษา
		<form name="frmselect" method="get"> 	
					<div class="dropdown"  style="float: left;">
						<button class="btn btn-default dropdown-toggle btn-info" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<?php echo $years['Yearterm']['year']; ?>
						</button>
						<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
							<?php foreach ($dropdowns as $dropdown) { ?>
								<?php 
									if($years['Yearterm']['year'] == $dropdown['Yearterm']['year']){
										$active = "active";
									}else{
										$active = "";
									}
									// $yearList = $dropdown['Yearterm']['year'];
								?>
								<li class="<?php echo $active;?>">
									<a href="<?php echo $this->Html->url(array('action' => 'student_project',$dropdown['Yearterm']['year'])); ?>">
										<font color="black"><?php echo $dropdown['Yearterm']['year']; ?></font>
									</a>
								</li>
							<?php } ?>
						</ul>
					</div>
		</form><br><br>
    <table class="table table-bordered">
        <thead>
            <th style="text-align:center;" colspan="" rowspan="" width="1%">ลำดับ</th>
            <th style="text-align:center;" colspan="" rowspan="" width="40%">ชื่อโปรเจ็ค</th>
            <th style="text-align:center;" colspan="" rowspan="" width="10%">ลิ้งค์ข้อมูล</th>
            <th style="text-align:center;" colspan="" rowspan="" width="10%">เดือน</th>
            <th style="text-align:center;" colspan="" rowspan="" width="10%">ปี</th>
            <?php if($admins != null){ ?>
            <th style="text-align:center;" colspan="" rowspan="" width="1%">แก้ไขข้อมูล</th>
            <th style="text-align:center;" colspan="" rowspan="" width="1%">ลบข้อมูล</th>
            <?php } ?>
        </thead>
        <tbody>
            <tr>
                <td style="text-align:left;" colspan="100" rowspan="" width="1%">ไตรมาส1</td>
            </tr>
            <?PHP
                $i = 0;
                foreach ($student_projects1 as $student_satisfaction_course_moral) { $i++
            ?>
            <tr>
                <td style="text-align:center;"><?php echo $i; ?></td>
                <td style="text-align:center;"><?php echo $student_satisfaction_course_moral['StudentProject']['project_name']; ?></td>
                <td style="text-align:center;"><a class="btn btn-info" target="_blank" href="<?php echo $student_satisfaction_course_moral['StudentProject']['link']; ?>">ข้อมูลเพิ่มเติม</a></td>
                <td style="text-align:center;"><?php echo $student_satisfaction_course_moral['StudentProject']['month']; ?></td>
                <td style="text-align:center;"><?php echo $student_satisfaction_course_moral['StudentProject']['year']; ?></td>
                <?php if($admins != null){ ?>
                <td style="text-align:center;"><a class="btn btn-warning" target="_blank" role="button" 
                    href="<?php echo $this->Html->url(array('action'=>'edit_student_project',$student_satisfaction_course_moral['StudentProject']['id'])); ?>">แก้ไขข้อมูล</a>
                </td>
                <td style="text-align:center;"><a class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')" 
                    href="<?php echo $this->Html->url(array('action' => 'remove_student_project',$student_satisfaction_course_moral['StudentProject']['id'])); ?>">ลบข้อมูล</a>
                </td>
                <?php } ?>
            </tr>
            <?php } ?>
            <tr>
                <td style="text-align:left;" colspan="100" rowspan="" width="1%">ไตรมาส2</td>
            </tr>
            <?PHP
                foreach ($student_projects2 as $student_satisfaction_course_moral) { $i++
            ?>
            <tr>
            <td style="text-align:center;"><?php echo $i; ?></td>
                <td style="text-align:center;"><?php echo $student_satisfaction_course_moral['StudentProject']['project_name']; ?></td>
                <td style="text-align:center;"><a class="btn btn-info" target="_blank" href="<?php echo $student_satisfaction_course_moral['StudentProject']['link']; ?>">ข้อมูลเพิ่มเติม</a></td>
                <td style="text-align:center;"><?php echo $student_satisfaction_course_moral['StudentProject']['month']; ?></td>
                <td style="text-align:center;"><?php echo $student_satisfaction_course_moral['StudentProject']['year']; ?></td>
                <?php if($admins != null){ ?>
                <td style="text-align:center;"><a class="btn btn-warning" target="_blank" role="button" 
                    href="<?php echo $this->Html->url(array('action'=>'edit_student_project',$student_satisfaction_course_moral['StudentProject']['id'])); ?>">แก้ไขข้อมูล</a>
                </td>
                <td style="text-align:center;"><a class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')" 
                    href="<?php echo $this->Html->url(array('action' => 'remove_student_project',$student_satisfaction_course_moral['StudentProject']['id'])); ?>">ลบข้อมูล</a>
                </td>
                <?php } ?>
            </tr>
            <?php } ?>
            <tr>
                <td style="text-align:left;" colspan="100" rowspan="" width="1%">ไตรมาส3</td>
            </tr>
            <?PHP
                foreach ($student_projects3 as $student_satisfaction_course_moral) { $i++
            ?>
            <tr>
            <td style="text-align:center;"><?php echo $i; ?></td>
                <td style="text-align:center;"><?php echo $student_satisfaction_course_moral['StudentProject']['project_name']; ?></td>
                <td style="text-align:center;"><a class="btn btn-info" target="_blank" href="<?php echo $student_satisfaction_course_moral['StudentProject']['link']; ?>">ข้อมูลเพิ่มเติม</a></td>
                <td style="text-align:center;"><?php echo $student_satisfaction_course_moral['StudentProject']['month']; ?></td>
                <td style="text-align:center;"><?php echo $student_satisfaction_course_moral['StudentProject']['year']; ?></td>
                <?php if($admins != null){ ?>
                <td style="text-align:center;"><a class="btn btn-warning" target="_blank" role="button" 
                    href="<?php echo $this->Html->url(array('action'=>'edit_student_project',$student_satisfaction_course_moral['StudentProject']['id'])); ?>">แก้ไขข้อมูล</a>
                </td>
                <td style="text-align:center;"><a class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')" 
                    href="<?php echo $this->Html->url(array('action' => 'remove_student_project',$student_satisfaction_course_moral['StudentProject']['id'])); ?>">ลบข้อมูล</a>
                </td>
                <?php } ?>
            </tr>
            <?php } ?>
            <tr>
                <td style="text-align:left;" colspan="100" rowspan="" width="1%">ไตรมาส4</td>
            </tr>
            <?PHP
                foreach ($student_projects4 as $student_satisfaction_course_moral) { $i++
            ?>
            <tr>
            <td style="text-align:center;"><?php echo $i; ?></td>
                <td style="text-align:center;"><?php echo $student_satisfaction_course_moral['StudentProject']['project_name']; ?></td>
                <td style="text-align:center;"><a class="btn btn-info" target="_blank" href="<?php echo $student_satisfaction_course_moral['StudentProject']['link']; ?>">ข้อมูลเพิ่มเติม</a></td>
                <td style="text-align:center;"><?php echo $student_satisfaction_course_moral['StudentProject']['month']; ?></td>
                <td style="text-align:center;"><?php echo $student_satisfaction_course_moral['StudentProject']['year']; ?></td>
                <?php if($admins != null){ ?>
                <td style="text-align:center;"><a class="btn btn-warning" target="_blank" role="button" 
                    href="<?php echo $this->Html->url(array('action'=>'edit_student_project',$student_satisfaction_course_moral['StudentProject']['id'])); ?>">แก้ไขข้อมูล</a>
                </td>
                <td style="text-align:center;"><a class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')" 
                    href="<?php echo $this->Html->url(array('action' => 'remove_student_project',$student_satisfaction_course_moral['StudentProject']['id'])); ?>">ลบข้อมูล</a>
                </td>
                <?php } ?>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>