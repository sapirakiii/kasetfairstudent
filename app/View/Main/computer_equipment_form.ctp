<div class="container" style="padding:15px 0 0; width:60%;">
	<div class="panel panel-primary" >
		<div class="panel-heading"> 
			<div class="panel-title">
				<h3 style="margin:0px !important;" >
					<center>แบบสำรวจอุปกรณ์คอมพิวเตอร์</center>
				</h3> <br />
			</div>
			<?php 
				$required = 'กรุณากรอกข้อมูล'; 
				// debug($this->request->data);
			?>
			
			<form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">

				<div  style="margin-top:10px">
					<label for=""  class="col-sm-3 col-form-label">1.ประเภทคอม</label>
						<?php echo $this->Form->input('com_type_id', array(
							'type'=>'radio',
							'options'=>array('1'=>' คอมชุด  ','2'=>' คอมประกอบ  '),
							'label'=>false,
							'value' => '',
							'required' => true,
							'data-error' => $required,
							'hiddenField'=>false,
							'div' => false,
							'legend'=>false,
							'style' => 'width: 15px; 
							height: 15px; 
							vertical-align:text-bottom;',
						));
						?>
				</div>
				<div class="form-group row"  style="margin-top:10px">
					<label for="" class="col-sm-3 col-form-label">2.ยี่ห้อ<font color="red">(กรณีคอมชุด)</font></label>
					<div class="col-sm-5">
						<?php echo $this->Form->input('brand', array(
							'type' => 'text',
							'label' => false,
							'class' => 'form-control',
							'required' => true
						));?>
					</div>
				</div>
				<div  style="margin-top:10px">
					<label for=""  class="col-sm-3 col-form-label">3.หน่วยความจำ</label>
					<div class="form-group row"  style="margin-top:10px">
						<label for="" class="col-sm-3 col-form-label">3.1.Harddisk(HDD)</label>
						<div class="col-sm-5">
							<?php echo $this->Form->input('HDD', array(
								'type' => 'number',
								'label' => false,
								'class' => 'form-control',
								'required' => true
							));?>
						</div>
						<label for="" class="col-sm-2 col-form-label">GB</label>
					</div>
					<div class="form-group row"  style="margin-top:10px">
						<label for="" class="col-sm-3 col-form-label">3.2.Solid State Drive(SSD)</label>
						<div class="col-sm-5">
							<?php echo $this->Form->input('SSD', array(
								'type' => 'number',
								'label' => false,
								'class' => 'form-control',
								'required' => true
							));?>
						</div>
						<label for="" class="col-sm-2 col-form-label">GB</label>
					</div>
				</div>
				<div class="form-group row"  style="margin-top:10px">
					<label for="" class="col-sm-3 col-form-label">4.ห้องเรียน</label>
					<div class="col-sm-5">
						<?php echo $this->Form->input('room', array(
							'type' => 'text',
							'label' => false,
							'class' => 'form-control',
							'required' => true
						));?>
					</div>
				</div>
				<div class="form-group row"  style="margin-top:10px">
					<label for="" class="col-sm-3 col-form-label">5.CPU</label>
					<div class="col-sm-5">
						<?php echo $this->Form->input('cpu', array(
							'type' => 'text',
							'label' => false,
							'class' => 'form-control',
							'required' => true
						));?>
					</div>
				</div>
				<div class="form-group row" style="margin-top:10px">
					<label for="" class="col-sm-3 col-form-label">6.Ram</label>
					<div class="col-sm-5">
						<?php echo $this->Form->input('ram', array(
							'type' => 'text',
							'label' => false,
							'class' => 'form-control',
							'required' => true
						));?>
					</div>
				</div>
				<div class="form-group row" style="margin-top:10px">
					<label for="" class="col-sm-3 col-form-label">7.รหัสครุภัณฑ์</label>
					<div class="col-sm-5">
						<?php echo $this->Form->input('code', array(
							'type' => 'text',
							'label' => false,
							'class' => 'form-control',
							'required' => true
						));?>
					</div>
				</div>

				<div class="form-group row"  style="margin-top:10px">
					<label for="" class="col-sm-3 col-form-label">8.ผู้ใช้งาน</label>
					<div class="col-sm-5">
						<label for="singleselect"></label>
                        <select name="data[mis_employee_id]" id="singleselect"  class="form-control js-example-basic-single" >
                            <?php echo $outputstudent; ?>
                        </select>
					</div>
				</div>

				<div style="margin-top:10px">
					<label for=""  class="col-sm-3 col-form-label">9.ประเภทคอมที่ใช้</label>
						<?php echo $this->Form->input('com_use_id', array(
							'type'=>'radio',
							'options'=>array('1'=>' คอมสำนักงานคณะ  ','2'=>' คอมห้องเรียน  ','3'=>' คอมห้องประชุม  '),
							'label'=>false,
							'value' => '',
							'hiddenField'=>false,
							'div' => false,
							'legend'=>false,
							'style' => 'width: 15px; 
							height: 15px; 
							vertical-align:text-bottom;',
						));?>
				</div>
				<div style="margin-top:10px">
					<label for=""  class="col-sm-3 col-form-label">10.อุปกรณ์การเรียนการสอนอื่นๆ</label>
					<?php
						foreach ($teachings as $teaching){
					?>
						<div class="form-check" style="padding-left:10px;">
							<label for="">
								<?php echo $this->Form->checkbox('teaching_id.'.$teaching['TeachingAid']['teaching_id'], array(
													'value' => $teaching['TeachingAid']['teaching_id'],
													'hiddenField' => false,
													'required' => false)); 
								?>
							</label>
							<?php echo $teaching['TeachingAid']['name']; ?>
						</div>
					<?php
						}			
					?>
				</div>
				<div style="margin-top:10px">
                      <label for="InfoDetail" class="addpost_input"> 11.อัปโหลดรูปภาพ และไฟล์เอกสารประกอบ</label> 
                        <?php
                            echo $this->Form->input('ComputerEquipmentDocument.files.', array(
                              'type' => 'file',
                              'multiple',
                              'accept' => '.xlsx,.xls,image/*, .doc, .docx, .ppt, .pptx, .txt, .pdf, .zip, .rar'
                            ));
                        ?>
                </div>

				<div style="margin-top:20px">
					<center><button type="submit" class="btn btn-success">บันทึกข้อมูล</button></center>
				</div>
			</form>
		</div>	
		
	</div>
</div>

