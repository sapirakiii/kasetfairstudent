<div class="container">
    <div class="panel-header">
        <h3 align="center">KPI ONLINE การพัฒนาการเรียนการสอนมุ่งสู่ Smart Agriculture</h3><br>
    </div>
</div>
<div class="panel-body">
    <table class="table table-bordered">
        <thead>
            <tr>
                <td colspan="" rowspan="" width="1%" style="text-align:center;">ลำดับ</td>
                <td colspan="" rowspan="" width="" style="text-align:center;">หัวข้อ</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="" rowspan="" width="" style="text-align:center;">1</td>
                <td colspan="" rowspan="" width="" >
                    <a target="_blank" href="<?php echo $this->Html->url(array('action' => 'curriculum_inter_info')); ?>">
                        จำนวนหลักสูตรเชิงบูรณาการ/หลักสูตรนานาชาติ/Double degree (นิยาม จำนวนหลักสูตรที่มี /ไม่นับระหว่างดำเนินการ นิยามหลักสูตรบูรณาการ: บูรณาการข้ามสาขา)
                    </a>
            </tr>
            <tr>
                <td colspan="" rowspan="" width="" style="text-align:center;">2</td>
                <td colspan="" rowspan="" width="" >
                    <a target="_blank" href="<?php echo $this->Html->url(array('action' => 'list_life_long')); ?>">
                        หลักสูตร Life Long Learning (นิยาม จำนวนหลักสูตรระยะสั้น รวม MOOC)
                    </a>
            </tr>
            <tr>
                <td colspan="" rowspan="" width="" style="text-align:center;">3</td>
                <td colspan="" rowspan="" width="" >
                    <a target="_blank" href="<?php echo $this->Html->url(array('action' => 'list_life_long')); ?>">
                        จำนวนผู้เรียนในหลักสูตร Life Long Learning (นิยาม จำนวนคนที่เรียนในหลักสูตรระยะสั้น MOOC /ThaiMOOC/ LE)
                    </a>
            </tr>
            <tr>
                <td colspan="" rowspan="" width="" style="text-align:center;">4</td>
                <td colspan="" rowspan="" width="" >
                    <a target="_blank" href="<?php echo $this->Html->url(array('action' => 'student_training')); ?>">
                        จำนวนผู้เข้ารับการถ่ายทอดความรู้และเรียนรู้ ผ่านการเข้าร่วมกิจกรรม/ฝึกอบรม/แหล่งเรียนรู้ ด้านการเรียนการสอน (นิยาม : ผู้เข้าร่วมกิจกรรม: เด็ก เยาวชน และประชาชนทั่วไปที่เข้าร่วมกิจกรรม ผู้เข้าร่วมการฝึกอบรม : เยาวชน นักเรียน นักศึกษา ประชาชนทั่วไป รวมทั้งผู้ปฏิบัติงานจากทุกภาคส่วน)
                    </a>
            </tr>
            <tr>
                <td colspan="" rowspan="" width="" style="text-align:center;">5</td>
                <td colspan="" rowspan="" width="" >
                    <a target="_blank" href="<?php echo $this->Html->url(array('action' => 'employment_data')); ?>">
                        ร้อยละของบัณฑิตระดับปริญญาตรีที่มีงานทำหรือประกอบอาชีพอิสระ ภายใน 1 ปีหลังสำเร็จการศึกษา
                    </a>
            </tr>
            <tr>
                <td colspan="" rowspan="" width="" style="text-align:center;">6</td>
                <td colspan="" rowspan="" width="" >
                    <a target="_blank" href="<?php echo $this->Html->url(array('action' => 'satisfaction')); ?>">
                        ความพึงพอใจของผู้เรียนต่อหลักสูตรฯของนักศึกษาปริญญาตรีชั้นปีที่ 4
                    </a>
            </tr>
            <tr>
                <td colspan="" rowspan="" width="" style="text-align:center;">7</td>
                <td colspan="" rowspan="" width="" >
                    <a target="_blank" href="<?php echo $this->Html->url(array('action' => 'life_long_learning')); ?>">
                        จำนวนกระบวนวิชาที่รองรับ lifelong learning ในแต่ละภาคการศึกษา
                    </a>
            </tr>
            <tr>
                <td colspan="" rowspan="" width="" style="text-align:center;">8</td>
                <td colspan="" rowspan="" width="" >
                    <a target="_blank" href="<?php echo $this->Html->url(array('action' => 'student_graduate_plan')); ?>">
                        ร้อยละผู้สำเร็จการศึกษาจบการศึกษาตามหลักสูตรภายในระยะเวลาที่กำหนด (นิยาม นับเฉพาะนักศึกษาปริญญาตรี )
                    </a>
            </tr>
            <tr>
                <td colspan="" rowspan="" width="" style="text-align:center;">8</td>
                <td colspan="" rowspan="" width="" >
                    <a target="_blank" href="<?php echo $this->Html->url(array('action' => 'curriculum_info')); ?>">
                        จำนวนหลักสูตรที่สร้างบรรยากาศการเรียนรู้ในลักษณะ CWIE, Pi-shaped skills, Entrepreneurship
                    </a>
            </tr>
            <tr>
                <td colspan="" rowspan="" width="" style="text-align:center;">9</td>
                <td colspan="" rowspan="" width="" >
                    <a target="_blank" href="<?php echo $this->Html->url(array('action' => 'student_common_european')); ?>">
                        ร้อยละของนักศึกษาระดับปริญญาตรีที่มีผลการสอบวัดความรู้และทักษะภาษาอังกฤษก่อนสำเร็จการศึกษา ตามมาตรฐาน Common European Framework of Reference for Language อยู่ในระดับ B1 ขึ้นไป (นักศึกษาปริญญาตรีชั้นปีที่ 4)
                    </a>
            </tr>
            <tr>
                <td colspan="" rowspan="" width="" style="text-align:center;">10</td>
                <td colspan="" rowspan="" width="" >
                    <a target="_blank" href="<?php echo $this->Html->url(array('action' => 'student_satisfaction_course_moral')); ?>">
                        ระดับความพึงพอใจของผู้ใช้บัณฑิตต่อทักษะด้านคุณธรรม จริยธรรม และทักษะการเป็นพลเมืองโลก
                    </a>
            </tr>
            <tr>
                <td colspan="" rowspan="" width="" style="text-align:center;">11</td>
                <td colspan="" rowspan="" width="" >
                    <a target="_blank" href="<?php echo $this->Html->url(array('action' => 'student_smart_agri')); ?>">
                        ระดับความพึงพอใจของผู้ใช้บัณฑิตต่อความรู้/ทักษะด้าน Smart Agriculture ( นิยามตามวิสัยทัศน์ของคณะฯ : สร้างและถ่ายทอดนวัตกรรมการเกษตร มุ่งสู่การพัฒนาอย่างยั่งยืน)
                    </a>
            </tr>
            <tr>
                <td colspan="" rowspan="" width="" style="text-align:center;">12</td>
                <td colspan="" rowspan="" width="" >
                    <a target="_blank" href="<?php echo $this->Html->url(array('action' => 'student_project')); ?>">
                        จำนวนโครงการ Pitching/Start up ของนักศึกษาระดับปริญญาตรีและบัณฑิตศึกษา
                    </a>
            </tr>
            <tr>
                <td colspan="" rowspan="" width="" style="text-align:center;">13</td>
                <td colspan="" rowspan="" width="" >
                    <a target="_blank" href="<?php echo $this->Html->url(array('action' => 'student_active')); ?>">
                        จำนวนนักศึกษาที่ไปดำเนินกิจกรรม ในต่างประเทศ (นิยาม แลกเปลี่ยนประสบการณ์ สหกิจ ฝึกงาน ในต่างประเทศและทุนต่างประเทศ)
                    </a>
            </tr>
            <tr>
                <td colspan="" rowspan="" width="" style="text-align:center;">14</td>
                <td colspan="" rowspan="" width="" >
                    <a target="_blank" href="<?php echo $this->Html->url(array('action' => 'student_manufacture')); ?>">
                        สัดส่วนการผลิตบัณฑิต สายวิทย์ : สายอื่นในสถาบันอุดมศึกษา (ข้อมูลนักศึกษาจบ ต่อ นักศึกษาที่มีอยู่ทั้งหมด)
                    </a>
            </tr>
            <tr>
                <td colspan="" rowspan="" width="" style="text-align:center;">15</td>
                <td colspan="" rowspan="" width="" >
                    <a target="_blank" href="<?php echo $this->Html->url(array('action' => 'student_dna')); ?>">
                        จำนวนนักศึกษาที่มีคุณลักษณะบัณฑิตตามที่มหาวิทยาลัยกำหนด (CMU Student DNA Blueprint)
                    </a>
            </tr>
        </tbody>
    </table>
</div>