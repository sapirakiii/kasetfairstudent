<div class="container" style="padding:15px 0 0;">
<div class="panel panel-default" style="border-color: #F2F2F2!important">
	<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center><div class="font_panel2">เบอร์โทรแต่ละฝ่ายการจัดงาน</div></center>
				</h3> 
			</div>
		</div>	
		<div class="panel-body" style="padding-top: 15px; ">
			<div align="center"> 	
				<h4><div class="font_panel7">งานเกษตรภาคเหนือ ครั้งที่ 8</div></h4>
				
				<h3><div class="font_panel7">ระหว่างวันที่ 8-12 พ.ย. 2560 </div></h3> 
				
				
			</div> 
			
			<br>
			
			<div id="no-more-tables"> 				
				<table class="table table-bordered table-striped">
					<thead>
						<tr style="text-align: center;">
							<th rowspan="2"  style="text-align: center;">ลำดับ</th>
							<td colspan="3">ฝ่ายต่างๆ</td>
						</tr>
						<tr >						
							<th style="text-align: center;">ชื่อฝ่าย</th>
							<th style="text-align: center;">ประธานกรรมการ</th>
							<th style="text-align: center;">กรรมการและเลขานุการ</th>							
						</tr>
					</thead>
					<tbody>						
							<tr align="center">
									<td data-title="ลำดับ" align="center">1</td>
									<td data-title="ฝ่าย">ฝ่ายต้อนรับและพิธีการ</td>
									<td data-title="ประธานกรรมการ">	ผศ.ดร. ดรุณี นาพรหม <br>โทร. 085-0407747 </td>
									<td data-title="กรรมการและเลขานุการ">นาง สุดใจ สันธทรัพย์ <br>โทร.081-1113103	</td>
									
							</tr>
							<tr align="center">
									<td data-title="ลำดับ" align="center">2</td>
									<td data-title="ฝ่าย">ฝ่ายประชุมและเสวนา</td>
									<td data-title="ประธานกรรมการ">	รศ.ดร. เกวลิน	คุณาศักดากุล <br>โทร. 084-1712567 </td>
									<td data-title="กรรมการและเลขานุการ">นาง ลาลิตยา นุ่มมีศรี  <br>โทร.089-6323047	</td>
									
							</tr>
												
						
					</tbody>
				</table>
			</div>			
	
		</div>

	</div>
	
	

