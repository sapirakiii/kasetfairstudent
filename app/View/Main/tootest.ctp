<form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">
    <label>1=</label>
        <?php echo $this->Form->input('name', array(
            'type' => 'text',
            'value' => '0' ,
            'min' => '0',
            'label' => false,
            'class' => 'form-control')); 
        ?>
    <div class="help-block with-errors"></div>
    <?php
        foreach ($testoos as $testoo){
    ?>
        <div class="checkbox" style="margin-left:20px;">
            <label>
                <?php echo $this->Form->checkbox('id_test.'.$testoo['TestToo']['id'], array(
                    'value' => $testoo['TestToo']['id'],
                    'hiddenField' => false,
                    'data-error' => $required,
                    // 'required' => true 
                    )); 
                ?>
            </label>
            <?php echo $testoo['TestToo']['name']; ?>
        </div>	
    <?php
        }
    ?>
    <div style="margin-top:10px">
        <label for="InfoDetail" class="addpost_input"> 9.อัปโหลดรูปภาพ และไฟล์เอกสารประกอบ</label> 
            <?php
                echo $this->Form->input('ComputerEquipmentDocument.files.', array(
                    'type' => 'file',
                    'multiple',
                    'accept' => '.xlsx,.xls,image/*, .doc, .docx, .ppt, .pptx, .txt, .pdf, .zip, .rar'
                    ));
            ?>
    </div>
    <div class="row" style="margin:30px 0px 30px; ">
		<div class="col-md-12">
			<center><input type="submit" value="คลิกส่งข้อมูลการลงทะเบียน" class="btn btn-success btn-lg btn-block"></center> 
		</div>
	</div>
</form>