<div class="container">
    <div class="panel-header" align="center">
        <h3>จำนวนนักศึกษาที่ไปดำเนินกิจกรรม ในต่างประเทศ ระดับปริญญาตรี</h3>
    </div>
</div>
<div class="panel-body">
    <br>
    <div class="btn-group">
        <a class="btn btn-danger" role="button" href="<?php echo $this->Html->url(array('action'=>'student_active')); ?>">หน้าแรก</a>
    </div>
    <div class="btn-group">
        <?php if($admins != null){ ?>
        <a class="btn btn-success" role="button" target="_blank" href="<?php echo $this->Html->url(array('action'=>'add_student_active1')); ?>">เพิ่มข้อมูล</a>
        <?php } ?>
    </div>
    <div class="btn-group">
        <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aira-expanded="false">
                ระดับการศึกษา
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="<?php echo $this->Html->url(array('action'=>'student_active_degree1')); ?>">ปริญญาตรี</a>
                <a class="dropdown-item" href="<?php echo $this->Html->url(array('action'=>'student_active_degree3')); ?>">ปริญญาโท</a>
                <a class="dropdown-item" href="<?php echo $this->Html->url(array('action'=>'student_active_degree5')); ?>">ปริญญาเอก</a>
            </div>
        </div>
    </div>
    <div class="btn-group">
        <form method="get">
            <div class="dropdown">
                <button class="btn btn-info btn-default dropdown-toggle" type="button" id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aira-expanded="false">
                    <?php echo $years['Yearterm']['year'];?>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu">
                    <?php foreach ($dropdowns as $dropdown) { ?>
                        <?php
                            if ($years['Yearterm']['year'] == $dropdown['Yearterm']['year'])
                            {
                                $active = 'active';
                            }
                            else
                            {
                                $active = '';
                            }
                        ?>
                        <li class="<?php echo $active;?>">
                            <a href="<?php echo $this->Html->url(array('action' => 'student_active_degree1',$dropdown['Yearterm']['year']));?>">
                                <font color="black"><?php echo $dropdown['Yearterm']['year'];?></font>
                            </a>
                        </li>
                        <?php } ?>
                </ul>
            </div>
        </form>
    </div>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th style="text-align:center;" colspan="" rowspan="" width="1%">ลำดับ</th>
                <th style="text-align:center;" colspan="" rowspan="" width="10%">รหัสนักศึกษา</th>
                <th style="text-align:center;" colspan="" rowspan="" width="10%">ชื่อ - นามสกุล</th>
                <th style="text-align:center;" colspan="" rowspan="" width="5%">สาขา</th>
                <th style="text-align:center;" colspan="" rowspan="" width="5%">เดินทางไปที่</th>
                <th style="text-align:center;" colspan="" rowspan="" width="5%">วันที่เดินทาง</th>
                <th style="text-align:center;" colspan="" rowspan="" width="5%">ระดับ</th>
                <th style="text-align:center;" colspan="" rowspan="" width="5%">เดือน</th>
                <th style="text-align:center;" colspan="" rowspan="" width="5%">ปี</th>
                <?php if($admins != null){ ?>
                <th style="text-align:center;" colspan="" rowspan="" width="1%">แก้ไขข้อมูล</th>
                <th style="text-align:center;" colspan="" rowspan="" width="1%">ลบข้อมูล</th>
                <?php } ?>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="12" rowspan="">ไตรมาส1</td>
            </tr>
            <tr>
            <?php
                $i = 0;
                foreach ($student_active1 as $student_active){$i++;
            ?>
                <td style="text-align:center;"><?php echo $i; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['student_id']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['stu_name']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['class']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['country']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['travel']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['degree_id']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['month']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['year']; ?></td>
                <?php if($admins != null){ ?>
                <td style="text-align:center;"><a class="btn btn-warning" role="button" target="_blank" href="<?php echo $this->Html->url(array('action'=>'edit_student_active1',$student_active['StudentTravel']['id'])); ?>">แก้ไขข้อมูล</a></td>
                <td style="text-align:center;"><a class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')" href="<?php echo $this->Html->url(array('action'=>'remove_student_active1',$student_active['StudentTravel']['id'])); ?>">ลบข้อมูล</a></td>
                <?php } ?>
            </tr>
            <?php } ?>
            <tr>
                <td colspan="12" rowspan="">ไตรมาส2</td>
            </tr>
            <tr>
            <?php
                $i = 0;
                foreach ($student_active2 as $student_active){$i++;
            ?>
                <td style="text-align:center;"><?php echo $i; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['student_id']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['stu_name']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['class']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['country']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['travel']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['degree_id']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['month']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['year']; ?></td>
                <?php if($admins != null){ ?>
                <td style="text-align:center;"><a class="btn btn-warning" role="button" target="_blank" href="<?php echo $this->Html->url(array('action'=>'edit_student_active1',$student_active['StudentTravel']['id'])); ?>">แก้ไขข้อมูล</a></td>
                <td style="text-align:center;"><a class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')" href="<?php echo $this->Html->url(array('action'=>'remove_student_active1',$student_active['StudentTravel']['id'])); ?>">ลบข้อมูล</a></td>
                <?php } ?>
            </tr>
            <?php } ?>
            <tr>
                <td colspan="12" rowspan="">ไตรมาส3</td>
            </tr>
            <tr>
            <?php
                $i = 0;
                foreach ($student_active3 as $student_active){$i++;
            ?>
                <td style="text-align:center;"><?php echo $i; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['student_id']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['stu_name']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['class']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['country']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['travel']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['degree_id']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['month']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['year']; ?></td>
                <?php if($admins != null){ ?>
                <td style="text-align:center;"><a class="btn btn-warning" role="button" target="_blank" href="<?php echo $this->Html->url(array('action'=>'edit_student_active1',$student_active['StudentTravel']['id'])); ?>">แก้ไขข้อมูล</a></td>
                <td style="text-align:center;"><a class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')" href="<?php echo $this->Html->url(array('action'=>'remove_student_active1',$student_active['StudentTravel']['id'])); ?>">ลบข้อมูล</a></td>
                <?php } ?>
            </tr>
            <?php } ?>
            <tr>
                <td colspan="12" rowspan="">ไตรมาส4</td>
            </tr>
            <tr>
            <?php
                $i = 0;
                foreach ($student_active4 as $student_active){$i++;
            ?>
                <td style="text-align:center;"><?php echo $i; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['student_id']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['stu_name']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['class']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['country']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['travel']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['degree_id']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['month']; ?></td>
                <td style="text-align:center;"><?php echo $student_active['StudentTravel']['year']; ?></td>
                <?php if($admins != null){ ?>
                <td style="text-align:center;"><a class="btn btn-warning" role="button" target="_blank" href="<?php echo $this->Html->url(array('action'=>'edit_student_active1',$student_active['StudentTravel']['id'])); ?>">แก้ไขข้อมูล</a></td>
                <td style="text-align:center;"><a class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')" href="<?php echo $this->Html->url(array('action'=>'remove_student_active1',$student_active['StudentTravel']['id'])); ?>">ลบข้อมูล</a></td>
                <?php } ?>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>