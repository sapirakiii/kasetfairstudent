<div class="container">
    <div class="panel-header" align="center">
        <h3>จำนวนนักศึกษาที่ไปดำเนินกิจกรรม ในต่างประเทศ <br>(นิยาม แลกเปลี่ยนประสบการณ์ สหกิจ ฝึกงาน ในต่างประเทศและทุนต่างประเทศ)</h3>
    </div>
</div>
<div class="panel-body">
    <br>
    <div class="btn-group">
        <!-- <form method="get">
            <div class="dropdown">
                <button class="btn btn-info btn-default dropdown-toggle" type="button" id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aira-expanded="false">
                    <?php echo $years['Yearterm']['year'];?>
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu">
                    <?php foreach ($dropdowns as $dropdown) { ?>
                        <?php
                            if ($years['Yearterm']['year'] == $dropdown['Yearterm']['year'])
                            {
                                $active = 'active';
                            }
                            else
                            {
                                $active = '';
                            }
                        ?>
                        <li class="<?php echo $active;?>">
                            <a href="<?php echo $this->Html->url(array('action' => 'student_satisfaction_course_moral',$dropdown['Yearterm']['year']));?>">
                                <font color="black"><?php echo $dropdown['Yearterm']['year'];?></font>
                            </a>
                        </li>
                        <?php } ?>
                </ul>
            </div>
        </form> -->
    </div>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th style="text-align:center;" colspan="" rowspan="" width="1%">ลำดับ</th>
                <th style="text-align:center;" colspan="" rowspan="" width="20%">หัวข้อ</th>
                <th style="text-align:center;" colspan="" rowspan="" width="5%">ระดับ</th>
                <th style="text-align:center;" colspan="" rowspan="" width="5%">จำนวนคน</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="text-align:center;">1</td>
                <td style="text-align:center;"><a href="<?php echo $this->Html->url(array('action' => 'student_active_degree1')) ?>" target="_blank">จำนวนนักศึกษาที่ไปดำเนินกิจกรรม ในต่างประเทศ </a></td>
                <td style="text-align:center;">ปริญญาตรี</td>
                <td style="text-align:center;"><?php echo $student_actives1?></td>
            </tr>
            <tr>
                <td style="text-align:center;">2</td>
                <td style="text-align:center;"><a href="<?php echo $this->Html->url(array('action' => 'student_active_degree3')) ?>" target="_blank">จำนวนนักศึกษาที่ไปดำเนินกิจกรรม ในต่างประเทศ </a></td>
                <td style="text-align:center;">ปริญญาโท</td>
                <td style="text-align:center;"><?php echo $student_actives2?></td>
            </tr>
            <tr>
                <td style="text-align:center;">3</td>
                <td style="text-align:center;"><a href="<?php echo $this->Html->url(array('action' => 'student_active_degree5')) ?>" target="_blank">จำนวนนักศึกษาที่ไปดำเนินกิจกรรม ในต่างประเทศ </a></td>
                <td style="text-align:center;">ปริญญาเอก</td>
                <td style="text-align:center;"><?php echo $student_actives3?></td>
            </tr>
        </tbody>
    </table>
</div>