<?php
function DateThai($strDate)
{

	$strYear = date("Y", strtotime($strDate)) + 543;
	$strMonth = date("n", strtotime($strDate));
	$strDay = date("j", strtotime($strDate));
	$strHour = date("H", strtotime($strDate));
	$strMinute = date("i", strtotime($strDate));
	$strSeconds = date("s", strtotime($strDate));
	$strMonthCut = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
	$strMonthThai = $strMonthCut[$strMonth];
	return "$strDay $strMonthThai $strYear";
	//, $strHour:$strMinute
}
?>





<?php
date_default_timezone_set('Asia/Bangkok');
$date = date("Y-m-d");
$time = date("H:i:s");


function DateDiff($strDate1, $strDate2)
{
	return (strtotime($strDate2) - strtotime($strDate1)) /  (60 * 60 * 24);  // 1 day = 60*60*24
}

function TimeDiff($strTime1, $strTime2)
{
	return (strtotime($strTime2) - strtotime($strTime1)) /  (60 * 60); // 1 Hour =  60*60                           
}



?>
<div class="container-fluid" style="padding:15px 0 0;">
	<div class="panel panel-default" style="border-color: #F2F2F2!important">
		<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<div id="no-more-tables">
				<br>
				<center>
					<h3>ร้อยละของนักศึกษาระดับปริญญาตรีที่มีผลการสอบวัดความรู้และทักษะภาษาอังกฤษก่อนสำเร็จการศึกษา ตามมาตรฐาน Common European Framework of Reference for Language อยู่ในระดับ B1 (ปีการศึกษาที่ <?php echo $lastyearterms['Yearterm']['year']; ?>)</h3>
				</center>
				<br>
				<?php if($admins != null) { ?>
				<a class="btn btn-success" role="button" target="_blank" href="<?php echo $this->Html->url(array('action'=>'add_student_common_european')); ?>">เพิ่มข้อมูล</a>
				<?php } ?>
				กรุณาเลือกปีการศึกษา
				<form method="get" name="frmselect">
					<div class="dropdown" style="float:left">
						<button class="btn btn-default btn-info dropdown-toggle" type="button" id="dropdownMenu" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown">
							<?php echo $lastyearterms['Yearterm']['year']; ?>
						</button>
						<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
							<?php foreach($dropdowns as $dropdown) { ?>
								<?php
									if($years['Yearterm']['year'] == $dropdown['Yearterm']['year'])
									{
										$active = "active";
									}else{
										$active = "";
									}
								?>
								<li class="<?php echo $active;?>">
									<a href="<?php echo $this->Html->url(array('action' => 'student_common_european',$dropdown['Yearterm']['year'])); ?>">
										<font color="black"><?php echo $dropdown['Yearterm']['year']; ?></font>
									</a>
								</li>
								<?php } ?>
						</ul>
						
					</div>
				</form>
				<br><br>
				<div id="no-more-tables">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th style="text-align: center;" width="5%"><font size="3px">ลำดับ</font></th>
								<th style="text-align: center;" width="40%"><font size="3px">ชื่อหลักสูตร</font></th>
								<th style="text-align: center;" width="10%"><font size="3px">เดือน</font></th>
								<th style="text-align: center;" width="10%"><font size="3px">ปี</font></th>
								<th style="text-align: center;" width="10%"><font size="3px">จำนวนผู้เข้าสอบทั้งหมด</font></th>
								<th style="text-align: center;" width="10%"><font size="3px">จำนวนผู้เข้าสอบที่ผ่าน</font></th>
								<th style="text-align: center;" width="10%"><font size="3px">ร้อยละ</font></th>
								<?php if($admins != null) { ?>
								<th style="text-align: center;" width="5%"><font size="3px">แก้ไขข้อมูล</font></th>
								<th style="text-align: center;" width="5%"><font size="3px">ลบข้อมูล</font></th>
								<?php } ?>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th colspan="10">ไตรมาส1</th>
							</tr>
							<tr>
								<?php
									$a = 0;
									foreach ($student_common_european1 as $student_common_european){$a++;
								?>
									<td style="text-align: center;"><?php  echo $a; ?></td>
									<td align='center'><?php echo $student_common_european['StudentCommonEuropean']['course_name'] ?></td>
									<td align='center'><?php echo $student_common_european['StudentCommonEuropean']['month'] ?></td>
									<td align='center'><?php echo $student_common_european['StudentCommonEuropean']['year'] ?></td>
									<td align='center'><?php echo $student_common_european['StudentCommonEuropean']['take_exam_total'] ?></td>
									<td align='center'><?php echo $student_common_european['StudentCommonEuropean']['pass_exam'] ?></td>
									<td align='center'><?php echo round(($student_common_european['StudentCommonEuropean']['pass_exam']*100)/$student_common_european['StudentCommonEuropean']['take_exam_total'], 2) ?></td>
									<?php if($admins != null) { ?>
									<td align='center'>
										<a role="button" class="btn btn-warning" target="_blank" href="<?php echo $this->Html->url(array('action' => 'edit_student_common_european',$student_common_european['StudentCommonEuropean']['id'])); ?>">
											แก้ไขข้อมูล
										</a>
									</td>
									<td align='center'>
									<a role="button" class="btn btn-danger" onclick="return confirm('ยืนยันการลบข้อมูล')" href="<?php echo $this->Html->url(array('action' => 'remove_student_common_european',$student_common_european['StudentCommonEuropean']['id'])); ?>">
											ลบข้อมูล
										</a>
									</td>
									<?php } ?>
							</tr>
							<?php } ?>
							<tr>
								<th colspan="10">ไตรมาส2</th>
							</tr>
							<tr>
								<?php
									$a = 0;
									foreach ($student_common_european2 as $student_common_european){$a++;
								?>
									<td style="text-align: center;"><?php  echo $a; ?></td>
									<td align='center'><?php echo $student_common_european['StudentCommonEuropean']['course_name'] ?></td>
									<td align='center'><?php echo $student_common_european['StudentCommonEuropean']['month'] ?></td>
									<td align='center'><?php echo $student_common_european['StudentCommonEuropean']['year'] ?></td>
									<td align='center'><?php echo $student_common_european['StudentCommonEuropean']['take_exam_total'] ?></td>
									<td align='center'><?php echo $student_common_european['StudentCommonEuropean']['pass_exam'] ?></td>
									<td align='center'><?php echo round(($student_common_european['StudentCommonEuropean']['pass_exam']*100)/$student_common_european['StudentCommonEuropean']['take_exam_total'], 2) ?></td>
									<?php if($admins != null) { ?>
									<td align='center'>
										<a role="button" class="btn btn-warning" target="_blank" href="<?php echo $this->Html->url(array('action' => 'edit_student_common_european',$student_common_european['StudentCommonEuropean']['id'])); ?>">
											แก้ไขข้อมูล
										</a>
									</td>
									<td align='center'>
									<a role="button" class="btn btn-danger" onclick="return confirm('ยืนยันการลบข้อมูล')" href="<?php echo $this->Html->url(array('action' => 'remove_student_common_european',$student_common_european['StudentCommonEuropean']['id'])); ?>">
											ลบข้อมูล
										</a>
									</td>
									<?php } ?>
							</tr>
							<?php } ?>
							<tr>
								<th colspan="10">ไตรมาส3</th>
							</tr>
							<tr>
								<?php
									$a = 0;
									foreach ($student_common_european3 as $student_common_european){$a++;
								?>
									<td style="text-align: center;"><?php  echo $a; ?></td>
									<td align='center'><?php echo $student_common_european['StudentCommonEuropean']['course_name'] ?></td>
									<td align='center'><?php echo $student_common_european['StudentCommonEuropean']['month'] ?></td>
									<td align='center'><?php echo $student_common_european['StudentCommonEuropean']['year'] ?></td>
									<td align='center'><?php echo $student_common_european['StudentCommonEuropean']['take_exam_total'] ?></td>
									<td align='center'><?php echo $student_common_european['StudentCommonEuropean']['pass_exam'] ?></td>
									<td align='center'><?php echo round(($student_common_european['StudentCommonEuropean']['pass_exam']*100)/$student_common_european['StudentCommonEuropean']['take_exam_total'], 2) ?></td>
									<?php if($admins != null) { ?>
									<td align='center'>
										<a role="button" class="btn btn-warning" target="_blank" href="<?php echo $this->Html->url(array('action' => 'edit_student_common_european',$student_common_european['StudentCommonEuropean']['id'])); ?>">
											แก้ไขข้อมูล
										</a>
									</td>
									<td align='center'>
									<a role="button" class="btn btn-danger" onclick="return confirm('ยืนยันการลบข้อมูล')" href="<?php echo $this->Html->url(array('action' => 'remove_student_common_european',$student_common_european['StudentCommonEuropean']['id'])); ?>">
											ลบข้อมูล
										</a>
									</td>
									<?php } ?>
							</tr>
							<?php } ?>
							<tr>
								<th colspan="10">ไตรมาส4</th>
							</tr>
							<tr>
								<?php
									$a = 0;
									foreach ($student_common_european4 as $student_common_european){$a++;
								?>
									<td style="text-align: center;"><?php  echo $a; ?></td>
									<td align='center'><?php echo $student_common_european['StudentCommonEuropean']['course_name'] ?></td>
									<td align='center'><?php echo $student_common_european['StudentCommonEuropean']['month'] ?></td>
									<td align='center'><?php echo $student_common_european['StudentCommonEuropean']['year'] ?></td>
									<td align='center'><?php echo $student_common_european['StudentCommonEuropean']['take_exam_total'] ?></td>
									<td align='center'><?php echo $student_common_european['StudentCommonEuropean']['pass_exam'] ?></td>
									<td align='center'><?php echo round(($student_common_european['StudentCommonEuropean']['pass_exam']*100)/$student_common_european['StudentCommonEuropean']['take_exam_total'], 2) ?></td>
									<?php if($admins != null) { ?>
									<td align='center'>
										<a role="button" class="btn btn-warning" target="_blank" href="<?php echo $this->Html->url(array('action' => 'edit_student_common_european',$student_common_european['StudentCommonEuropean']['id'])); ?>">
											แก้ไขข้อมูล
										</a>
									</td>
									<td align='center'>
									<a role="button" class="btn btn-danger" onclick="return confirm('ยืนยันการลบข้อมูล')" href="<?php echo $this->Html->url(array('action' => 'remove_student_common_european',$student_common_european['StudentCommonEuropean']['id'])); ?>">
											ลบข้อมูล
										</a>
									</td>
									<?php } ?>
							</tr>
							<?php } ?>
						</tbody>
						<tbody
					</table>
				</div>
			</div>
		</div>
	</div>
</div>