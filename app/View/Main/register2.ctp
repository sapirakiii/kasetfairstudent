<?php
	function DateThai($strDate)
		{

		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//, $strHour:$strMinute
	}
	?>
<div class="container" style="padding:15px 0 0;">
	<div class="panel panel-primary" >
		<div class="panel-heading"> 
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center>ลงทะเบียนเข้าร่วมกิจกรรมงานเกษตรภาคเหนือ</center>
				</h3> 
			</div>
		</div>	
		<div class="panel-body" style="padding-top: 15px; ">
			<div align="center"> 	
			 
			  	<h3> ฝ่ายกิจกรรมนักเรียน นักศึกษา</h3> <br>
				<h3><?php echo $project['Project']['name']; ?></h3> <br>
				<h4>ณ <?php echo $project['Project']['place']; ?> วันที่ <?php echo $project['Project']['date']; ?> เวลา <?php echo $project['Project']['time']; ?> </h4>
				<br><b>
				รับสมัครตั้งแต่วันนี้ จนถึง วันที่ 
				<?php 
                    $time1 = strtotime($project['Project']['dateout']);
                    $newformat1 = date('d-M-Y', $time1);
                        
					echo DateThai($newformat1)  ?> เวลา <?php echo date('H.i',strtotime($project['Project']['timestop'])); 
					?> น.</b> <br>
			    <a href="<?php echo $this->Html->url('/files/'.$project['Project']['file_name']); ?>" target="_blank" class="btn btn-primary btn-lg">
					Download <?php echo $project['Project']['name']; ?>
				</a>
			</div> 
			
			<br>
			<?php
              date_default_timezone_set('Asia/Bangkok');
              $date = date("Y-m-d");
              $time = date("H:i:s");
            
            function DateDiff($strDate1,$strDate2){                             
              return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
			}
			?>
			<?php if(DateDiff($project['Project']['dateout']." ".$project['Project']['timestop'] , $date." ".$time) > 0 ) { ?>
				<?php echo '<center><h4><font color="red">หมดช่วงเวลาการลงทะเบียน ขอขอบพระคุณที่ท่านให้ความสนใจของงานเกษตรภาคเหนือมา ณ โอกาสนี้ด้วยครับ</font></h4></center><br> '; ?>
			<?php }else { ?>
				<form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">
					<?php 
					//debug($this->request->data);
					$required = 'กรุณากรอกข้อมูล';
					
					echo $this->Form->create('Member',array('role' => 'form','data-toggle' => 'validator')); 
					
					?>
					<?php if ($projectId == 1 || $projectId == 2) { ?>

						<div class="row">
						<div class="form-group col-md-4">
							<label>คำนำหน้า </label>
							<?php
								echo $this->Form->input('prefix_id', array(
									'options' => $prefixes,
									'class' => 'form-control',
									'label' => false,
									'empty' => 'กรุณาเลือกคำนำหน้า'
								));
							
							?>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group col-md-4">
							<label>ชื่อ </label>
		
							<?php echo $this->Form->input('fname', array('type' => 'text',
								'label' => false,
								'class' => 'form-control',
								'data-error' => $required
								)); 
							?>
							
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group col-md-4">
							<label>นามสกุล </label>
		
							<?php echo $this->Form->input('lname',
								array('type' => 'text',
								'label' => false,
								'class' => 'form-control',
								'data-error' => $required
								)); 
							 ?>
							
							<div class="help-block with-errors"></div>					
						</div>
						</div>
						
						
						<div class="row">
						<div class="form-group col-md-4">
							<label>อายุ </label>
		
							<?php echo $this->Form->input('age', array('type' => 'number','min' => '0','label' => false,
							'class' => 'form-control','data-error' => $required)); ?>
							
							<div class="help-block with-errors"></div>	
						</div>
						
						
						
						<div class="form-group col-md-4">
							<label>เบอร์โทรศัพท์ </label>
		
							<?php echo $this->Form->input('phone', array('type' => 'number','min' => '0','label' => false,'class' => 'form-control','data-error' => $required)); ?>
							
							<div class="help-block with-errors"></div>					
						</div>
						
						</div>
						
						<div class="row">	
								<div class="form-group col-md-6">
								<label>โรงเรียน </label>
		
								<?php echo $this->Form->input('school', array('type' => 'text','label' => false,'class' => 'form-control',
								'data-error' => $required)); ?>
								
								<div class="help-block with-errors"></div>
						</div>
					
						<div class="form-group col-md-6">
							<label>ระดับ </label>
							<?php 
							$types = array('ป.1-6' => 'ป.1-6', 'ม.1-3' => 'ม.1-3', 'ม.4-6' => 'ม.4-6');
							echo $this->Form->input('level', array(
								'options' => $types,
								'data-error' => $required,
								'label' => false,
								'class' => 'form-control',
								)); ?>				
												
							<div class="help-block with-errors"></div>
						</div>
						</div>
						<div class="form-group col-md-6">
							<label>อาจารย์ ผู้ควบคุม </label>
		
							<?php echo $this->Form->input('teacher', array('type' => 'text','label' => false,
							'class' => 'form-control','data-error' => $required)); ?>
							
							<div class="help-block with-errors"></div>
						</div>
							<div class="form-group col-md-6">
							<label>เบอร์โทรอาจารย์ ผู้ควบคุม </label>
		
							<?php echo $this->Form->input('phoneteacher', array('type' => 'text','label' => false,'class' => 'form-control','data-error' => $required)); ?>
							
							<div class="help-block with-errors"></div>
						</div>
						<div class="row">				  
							<div class="form-group col-md-6">
								
									<div style="padding: 0 20px;">
										<table class="table table-bordered table-striped">
											<tbody>
													<tr>
														<td><label>แนบใบสมัครแบบไฟล์รูปภาพหรือไฟล์เอกสาร (WORD,PDF,JPG)</label></td>	
														<td>
															<?php
																	echo $this->Form->input('MemberDocument.files.', array('type' => 'file', 'multiple','accept' => '.xlsx,.xls,image/*, .doc, .docx, .ppt, .pptx, .txt, .pdf, .zip, .rar'));
															?>
															* เพิ่มได้มากกว่า 1 ไฟล์
														</td>	
													</tr>
												
												</tbody>
											</table>
										</div>	
							</div>
												
						</div>
					<?php }else{ ?>

						<div class="row">
						<div class="form-group col-md-4">
							<label>คำนำหน้า <span style="color: red;">*</span></label>


							<?php
								echo $this->Form->input('prefix_id', array(
									'options' => $prefixes,
									'class' => 'form-control',
									'label' => false,
									'required' => true,
									'data-error' => $required,
									'empty' => 'กรุณาเลือกคำนำหน้า'
								));
							
							?>
							
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group col-md-4">
							<label>ชื่อ <span style="color: red;">*</span></label>
		
							<?php echo $this->Form->input('fname', array('type' => 'text',
							'label' => false,'class' => 'form-control','data-error' => $required,'required' => true)); ?>
							
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group col-md-4">
							<label>นามสกุล <span style="color: red;">*</span></label>
		
							<?php echo $this->Form->input('lname', array('type' => 'text','label' => false,'class' => 'form-control','data-error' => $required,'required' => true)); ?>
							
							<div class="help-block with-errors"></div>					
						</div>
						</div>
						
						
						<div class="row">
						<div class="form-group col-md-4">
							<label>อายุ <span style="color: red;">*</span></label>
		
							<?php echo $this->Form->input('age', array('type' => 'number','min' => '0','label' => false,'class' => 'form-control','data-error' => $required,'required' => true)); ?>
							
							<div class="help-block with-errors"></div>	
						</div>
						
						
						
						<div class="form-group col-md-4">
							<label>เบอร์โทรศัพท์ </label>
		
							<?php echo $this->Form->input('phone', array('type' => 'number','min' => '0','label' => false,'class' => 'form-control','data-error' => $required)); ?>
							
							<div class="help-block with-errors"></div>					
						</div>
						
						</div>
						
						<div class="row">	
								<div class="form-group col-md-6">
								<label>โรงเรียน <span style="color: red;">*</span></label>
		
								<?php echo $this->Form->input('school', array('type' => 'text','label' => false,'class' => 'form-control','data-error' => $required,'required' => true)); ?>
								
								<div class="help-block with-errors"></div>
						</div>
					
						<div class="form-group col-md-6">
							<label>ระดับ <span style="color: red;">*</span></label>
							<?php 
							$types = array('ป.1-6' => 'ป.1-6', 'ม.1-3' => 'ม.1-3', 'ม.4-6' => 'ม.4-6');
							echo $this->Form->input('level', array(
								'options' => $types,
								'data-error' => $required,
								'label' => false,
								'class' => 'form-control',
								'required' => true)); ?>				
												
							<div class="help-block with-errors"></div>
						</div>
						</div>
						<div class="form-group col-md-6">
							<label>อาจารย์ ผู้ควบคุม <span style="color: red;">*</span></label>
		
							<?php echo $this->Form->input('teacher', array('type' => 'text','label' => false,'class' => 'form-control','data-error' => $required,'required' => true)); ?>
							
							<div class="help-block with-errors"></div>
						</div>
							<div class="form-group col-md-6">
							<label>เบอร์โทรอาจารย์ ผู้ควบคุม </label>
		
							<?php echo $this->Form->input('phoneteacher', array('type' => 'text','label' => false,'class' => 'form-control','data-error' => $required)); ?>
							
							<div class="help-block with-errors"></div>
						</div>
						<div class="row">				  
							<div class="form-group col-md-6">
								
									<div style="padding: 0 20px;">
										<table class="table table-bordered table-striped">
											<tbody>
													<tr>
														<td><label>แนบใบสมัครแบบไฟล์รูปภาพหรือไฟล์เอกสาร (WORD,PDF,JPG)</label></td>	
														<td>
															<?php
																	echo $this->Form->input('MemberDocument.files.', array('type' => 'file', 'multiple','accept' => '.xlsx,.xls,image/*, .doc, .docx, .ppt, .pptx, .txt, .pdf, .zip, .rar'));
															?>
															* เพิ่มได้มากกว่า 1 ไฟล์
														</td>	
													</tr>
												
												</tbody>
											</table>
										</div>	
							</div>
												
						</div>
					<?php } ?>
					<br>
					<div class="row">	
						<?php  if ($projectId == 1) { ?>
							<div class="form-group col-md-9">
									<div style="padding: 0 20px;">
										<table class="table table-bordered table-striped">
											<tbody>
													<tr>
														<td><label>แนบไฟล์เอกสารประกวด (WORD,PDF,JPG)</label></td>	
														<td>
															<?php
																	echo $this->Form->input('MemberDocument.file1s.',
																	array('type' => 'file',
																	'multiple','accept' => '.xlsx,.xls,image/*, .doc, .docx, .ppt, .pptx, .txt, .pdf, .zip, .rar'));
															?>
															* เพิ่มได้มากกว่า 1 ไฟล์
														</td>	
													</tr>
												
												</tbody>
											</table>
										</div>	
							</div>	
						<?php  }elseif ($projectId == 2) { ?>	
							<div class="form-group col-md-12">
									<div style="padding: 0 20px;">
										<table class="table table-bordered table-striped">
											<tbody>
													<tr>
														<td><label>แนบคลิปนำเสนอสิ่งประดิษฐ์ฯ จากYoutube <br>
															- การส่งคลิปนำเสนอสิ่งประดิษฐ์ฯ ให้บันทึกคลิปและอัพโหลดคลิปลงบนเว็บไซต์ YouTube
															จากนั้นให้แนบลิ้งค์หรือระบุลิ้งค์มาในขั้นตอนการสมัคร
															</label>
														</td>	
														<td>
															<?php
																	echo $this->Form->input('Member.link', 
																	array('type' => 'textarea',
																	'label' => false,
																	'class' => 'form-control',
																	'data-error' => $required,
																	));
															?>
															ตัวอย่าง: <a href="https://www.youtube.com/watch?v=bjBgJysk6a4" target="_blank">
															https://www.youtube.com/watch?v=bjBgJysk6a4</a> เป็นต้น
														</td>	
													</tr>
												
												</tbody>
											</table>
										</div>	
							</div>	
						<?php  }	?>			  
							  
					</div>
					<div class="row">				  
					  <div class="form-group col-md-6">
						
						<label>ท่านทราบข่าวสารจากแหล่งไหน <span style="color: red;">*</span><span style="font-weight:normal;"> (สามารถตอบมากกว่า 1 ข้อ)</span></label>
						
						<?php
							
						foreach ($sources as $source){
								
						?>
						
						<div class="checkbox" style="margin-left:20px;">
							<label>
								<?php echo $this->Form->checkbox('source_id.'.$source['Source']['id'], array('value' => $source['Source']['id'],'hiddenField' => false,'data-error' => $required,'required' => true)); ?>
							</label>
							
							<span style="vertical-align: top;"><?php echo $source['Source']['name']; ?></span>
							
						</div>	
						
						<?php
							
						}
								
						?>
						<div class="help-block with-errors"></div>
					  </div>				  
					</div>
					
	
					<div class="row">
						<div class="col-md-12">
							<input type="submit" value="คลิกส่งข้อมูลการลงทะเบียน" class="btn btn-success btn-lg btn-block"> 
						</div>
					</div>
				</form>
			
			<?php } ?>
	
		</div>

	</div>
	
</div>

<script type="text/javascript">
	$('.form-group').on("click",'input:checkbox',function(){          
		checkboxValidate($(this).attr('name'));
	});
	function checkboxValidate(name){
		var min = 1 //minumum number of boxes to be checked for this form-group
		if($('input:checked').length<min){
			$('input:checkbox').prop('required',true);
		}
		else{
			$('input:checkbox').prop('required',false);
		}
		
		$('#MemberRegisterForm').validator('validate');
		
		// if($('input[name="'+name+'"]:checked').length<min){
			// $('input[name="'+name+'"]').prop('required',true);
		// }
		// else{
			// $('input[name="'+name+'"]').prop('required',false);
		// }
	}
</script>