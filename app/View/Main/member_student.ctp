<div class="container" style="padding:15px 0 0; width:65%;">
    <div class="row">
        <h3 style="margin:0px !important;">
            <center><b> การแข่งขันนำเสนอผลงานทางวิชาการภาคโปสเตอร์ของนักศึกษา ระดับชั้นปีที่ 3และปีที่ 4 คณะเกษตรศาสตร์ <br> งานวันเกษตรภาคเหนือ ครั้งที่ 10  </b></center>
        </h3>
        <hr>
        <?php 
            $required = 'กรุณากรอกข้อมูล'; 
            // debug($this->request->data);
        ?>
        <form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8"> 

            <div class="row">
                <div class="form-group col-md-12" style="margin-top:10px; margin-bottom:20px">
                    <label>1) รหัสนักศึกษา </label>
                    <?php echo $this->Form->input('student_id', array(
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true
                    )); ?>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group col-md-2" style="margin-bottom:10px">
					<label>2) คำนำหน้า</label>
                        <?php
							echo $this->Form->input('prefix_id', array(
								'options' => $prefixes,
								'class' => 'form-control',
								'label' => false,
								'empty' => 'คำนำหน้า'
							));
					    ?>
				</div>
                <div class="form-group col-md-5">
                    <label>3) ชื่อ </label>
                    <?php echo $this->Form->input('student_firstname', array(
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true
                    )); ?>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group col-md-5">
                    <label>4) นามสกุล </label>
                    <?php echo $this->Form->input('student_lastname', array(
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true
                    )); ?>
                    <div class="help-block with-errors"></div>					
                </div>
            </div>
            <div class="row" style="margin-top:20px; margin-bottom:20px">
                <div class="form-group col-md-4">
                    <label>5) สาขาวิชา </label>
                    <?php
						echo $this->Form->input('major_id', array(
							'options' => $majordegrees,
							'class' => 'form-control',
							'label' => false,
							'empty' => 'กรุณาเลือกสาขาวิชา'
						));
					?>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group col-md-4" style="margin:0px">
                    <label style="margin-bottom:3px">6) ประเภท/หัวข้อ </label> <br />
                    <?php 
                        foreach($papers as $paper){
                            echo $this->Form->input('paper_id', array(
                                'type'=>'radio',
                                'options'=>array($paper['Paper']['id'] => ' '.$paper['Paper']['name'].' '),
                                'label'=>false,
                                'value' => '',
                                'class' => 'form-check-input',
                                'required' => true,
                                'data-error' => $required,
                                'hiddenField'=>false,
                                'div' => false,
                                'legend'=>false,
                                'style' => 'width: 15px; 
                                height: 15px; 
                                vertical-align:text-bottom;',
                            ));
                        }
					?>
                    <div class="help-block with-errors"></div>					
                </div>
            </div>
            <div class="row" style="margin-top:20px; margin-bottom:20px">

                <div class="form-group col-md-12">
                    <label>7) ชื่อผลงานภาษาไทย </label>
                    <?php echo $this->Form->input('title', array(
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true
                    )); ?>
                    <div class="help-block with-errors"></div>
                </div>
                <div class="form-group col-md-12">
                    <label>8) ชื่อผลงานภาษาอังกฤษ </label>
                    <?php echo $this->Form->input('title_eng', array(
                        'type' => 'text',
                        'label' => false,
                        'class' => 'form-control',
                        'required' => true
                    )); ?>
                    <div class="help-block with-errors"></div>
                </div>
            <div class="mb-3" style="margin-top:20px">
                <label for="InfoDetail" class="form-label"  style="margin:0px"> 9) เอกสารแนบ (* PDF file)</label> 
                    <?php echo $this->Form->input('MemberStudentDocument.files.', array(
                        'type' => 'file',
                        'multiple',
                        'class' => 'form-control',
                        'style' => 'margin-top: 0px;',
                        'accept' => '.xlsx,.xls,image/*, .doc, .docx, .ppt, .pptx, .txt, .pdf, .zip, .rar'
                    )); ?>
            </div>

            <div class="row" style="margin:30px 0px 30px;" >
                <div class="col-md-12">
                    <center><input type="submit" value="คลิกส่งข้อมูลการลงทะเบียน" class="btn btn-success btn-lg btn-block"></center> 
                </div>
            </div>
        </form>
    </div>
</div>