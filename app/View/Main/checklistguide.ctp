<div class="container" style="padding:15px 0 0;">
	<div class="panel panel-primary" >
		<div class="panel-heading"> 
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center>รายชื่อผู้ลงทะเบียน มัคคุเทศก์เกษตร</center>
				</h3> 
			</div>
		</div>	
		<div class="panel-body" style="padding-top: 15px; ">
			<div align="center"> 	
				
				
				<h3><?php echo $Dateguide['Dateguide']['name']; ?></h3> 
				
				
			</div> 
			
			<br>
			<a class="btn btn-primary pull-right" href="<?php echo $this->Html->url(array('action' => 'registerguide',$Dateguide['Dateguide']['id'])); ?>"
			 role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> ลงทะเบียนเพิ่มเติม</a>
			 
			
			<a class="btn btn-warning pull-left" href="<?php echo $this->Html->url(array('action' => 'ListDateguide')); ?>" role="button">					
				<span class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span>ย้อนกลับ
			</a>
			
			
			<div id="no-more-tables"> 				
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th style="text-align: center;">ลำดับ</th>
							<th style="text-align: center;">ชื่อหน่วยงาน</th>
							<th style="text-align: center;">ช่วงเวลา</th>
							<th style="text-align: center;">นักเรียน</th>
							<th style="text-align: center;">อาจารย์</th>
							<th style="text-align: center;">เกษตรกร</th>
							<th style="text-align: center;">บุคลากร</th>
							<th style="text-align: center;">อื่นๆ</th>
							<th style="text-align: center;">รวม</th>
							<th style="text-align: center;">การจัดมัคคุเทศก์</th>
							<th style="text-align: center;">ผู้ควบคุม</th>
							
							
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 0;
							foreach ($Guides as $Guide){
								$i++;
							?>
								<tr>
									<td data-title="ลำดับ" align="center"><?php echo $i; ?></td>
									<td data-title="ชื่อหน่วยงาน"><?php echo $Guide['Guide']['organize'] ?></td>
									<td data-title="ชื่อหน่วยงาน"><?php echo $Guide['Guide']['start'] ?>-<?php echo $Guide['Guide']['end'] ?></td>
									<td data-title="ชื่อหน่วยงาน"><?php echo $Guide['Guide']['studentnum'] ?></td>
									<td data-title="ชื่อหน่วยงาน"><?php echo $Guide['Guide']['teachernum'] ?></td>
									<td data-title="ชื่อหน่วยงาน"><?php echo $Guide['Guide']['farmernum'] ?></td>
									<td data-title="ชื่อหน่วยงาน"><?php echo $Guide['Guide']['staffnum'] ?></td>
									<td data-title="ชื่อหน่วยงาน"><?php echo $Guide['Guide']['othernum'] ?></td>
									<?php $total=0;
										  $total = (((($Guide['Guide']['studentnum']+$Guide['Guide']['teachernum'])+$Guide['Guide']['farmernum'])+$Guide['Guide']['staffnum'])+$Guide['Guide']['othernum']); ?>
									<td data-title="ชื่อหน่วยงาน"><?php echo $total ?></td>
									
									<td data-title="ชื่อหน่วยงาน">
										<?php if ($Guide['Guide']['visit']) {
											echo 'ประสงค์';
										}else {
											echo 'ไม่ประสงค์';
										} ?>
									
									 </td>
									 <td data-title="ชื่อหน่วยงาน">
										 <?php 
										$memberName = $Guide['Prefix']['name'].$Guide['Guide']['fname']; 
										
										echo $memberName.'<br>';
										echo 'tel:'.$Guide['Guide']['reftel'].'<br>';
										// echo 'line:'.$Guide['Guide']['refline'];
										   ?>
									 
									 </td>
									
								</tr>
							<?php
							}
						?>						
						
					</tbody>
				</table>
			</div>			
	
		</div>

	</div>
	
	<div class="row">
		
		<div class="col-md-8">	
			
				<div class="alert alert-info">
					ติดต่อสอบถามเพิ่มเติมได้ที่ ฝ่ายประชาสัมพันธ์งานเกษตรภาคเหนือ ครั้งที่ 8 (คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่)
					<br>
					หมายเลขโทรศัพท์ภายใน 053-944088 มือถือหมายเลข 084-6089915, 089-4296229 (ในวันเวลาราชการ)
					<br>
					<!-- อีเมลล์ chaichet.agei@hotmail.com Line Id . 083-6228414 -->
				</div>
		
				
		</div>
	</div>
	
</div>

