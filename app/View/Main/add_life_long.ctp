<?php //echo debug($this->request->data)?>
<!-- start content here -->
<div class="container" style="padding:0 15px 0;">	
	<div class="row">
		<div class="col-md-12">
         <div class="well">
            
				 

                                <form method="post" accept-charset="utf-8">
                                  <div class="panel panel-success">
                                      <div class="panel-heading"> 
                                        <h3 class="panel-title"><i class="glyphicon glyphicon-plus"></i> เพิ่มรายการ</h3> 
                                      </div>
                                      <div class="panel-body">
                                        <tbody> 
                                                
                                            <label class="col-sm-4 control-label"> หัวข้อ</label>
                                               <div class="col-sm-8">
                                                     <div class="form-group has-feedback">      
                                                        <?php 
                                                              echo $this->Form->input('LifeLongInfo.subject_name', array(
                                                                  'type' => 'text',
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control css-require'),                                                           
                                                                  'error' => false,
                                                                  'placeholder' => 'กรุณากรอกหัวข้อ',
                                                                  'required'
                                                                  ));
                                                          
                                                            ?>
                                                  <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                </div>
                                            </div>
                                            <label class="col-sm-4 control-label">รายละเอียด</label>
                                               <div class="col-sm-8">
                                                     <div class="form-group has-feedback">      
                                                        <?php 
                                                              echo $this->Form->input('LifeLongInfo.type', array(
                                                                  'type' => 'text',
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control css-require'),                                                           
                                                                  'error' => false,
                                                                  'placeholder' => 'กรุณากรอกรายละเอียด',
                                                                  'required'
                                                                  ));
                                                          
                                                            ?>
                                                  <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                </div>
                                            </div>
                                            <label class="col-sm-4 control-label">Code </label>
                                               <div class="col-sm-8">
                                                     <div class="form-group has-feedback">      
                                                        <?php 
                                                              echo $this->Form->input('LifeLongInfo.code', array(
                                                                  'type' => 'text',
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'placeholder' => 'กรุณากรอกรายละเอียด',
                                                                  'class' => array('form-control css-require'),                                                           
                                                                  'error' => false,
                                                                  'required'
                                                                  ));
                                                          
                                                            ?>
                                                  <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                </div>
                                            </div>
                                            <label class="col-sm-4 control-label">Website </label>
                                               <div class="col-sm-8">
                                                     <div class="form-group has-feedback">      
                                                        <?php 
                                                              echo $this->Form->input('LifeLongInfo.website', array(
                                                                  'type' => 'text',
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'placeholder' => 'กรุณากรอกรายละเอียด',
                                                                  'class' => array('form-control css-require'),                                                           
                                                                  'error' => false,
                                                                  'required'
                                                                  ));
                                                          
                                                            ?>
                                                  <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                </div>
                                            </div>
                                             
                                         
                                         <label class="col-sm-4 control-label">เดือน <font color="red">*</font> </label>
                                               <div class="col-sm-8">
                                                     <div class="form-group has-feedback">      
                                                        <?php 
                                                               $types = array(
                                                                              '1' => '1',
                                                                              '2' => '2',
                                                                              '3' => '3',
                                                                              '4' => '4',
                                                                              '5' => '5',
                                                                              '6' => '6',
                                                                              '7' => '7',
                                                                              '8' => '8',
                                                                              '9' => '9',
                                                                              '10' => '10',
                                                                              '11' => '11',
                                                                              '12' => '12',
                                                                             
                                                                        );                                                       
                                                              echo $this->Form->input('LifeLongInfo.month', array(
                                                                  'options' => $types,
                                                                  'label' => false,
                                                                  'div' => false,
                                                                   
                                                                  'class' => array('form-control css-require'),                                                           
                                                                  'error' => false,
                                                                  'required'
                                                                  ));
                                                          
                                                            ?>
                                                           
                                                  <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                </div>
                                            </div>
                                            <label class="col-sm-4 control-label">ปี </label>
                                               <div class="col-sm-8">
                                                     <div class="form-group has-feedback">      
                                                        <?php 
                                                              echo $this->Form->input('LifeLongInfo.year', array(
                                                                  'type' => 'number',
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'value' => 2567,
                                                                  'class' => array('form-control css-require'),                                                           
                                                                  'error' => false,
                                                                  'placeholder' => 'กรุณากรอกปีการศึกษา',
                                                                  'required'
                                                                  ));
                                                          
                                                            ?>
                                                  <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                </div>
                                            </div>
                                            <label class="col-sm-4 control-label">จำนวนนักศึกษา</label>
                                               <div class="col-sm-8">
                                                     <div class="form-group has-feedback">      
                                                        <?php 
                                                              echo $this->Form->input('LifeLongInfo.count_student', array(
                                                                  'type' => 'text',
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'class' => array('form-control css-require'),  
                                                                  'placeholder' => 'กรุณากรอกจำนวนนักศึกษา',                                                         
                                                                  'error' => false,
                                                                  'required'
                                                                  ));
                                                          
                                                            ?>
                                                  <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                </div>
                                            </div>
                                            
                                         
                                                <div class="form-group">
                                                            <input type="submit" class="btn btn-success" value="บันทึกข้อมูล" onclick="return confirm('ยืนยันการเพิ่มข้อมูล')">
                                                </div>
                                            </div>
                                          </div>
                                              
                                            
                                    </form>
                        </tbody>




                   </div> <!-- /container --> 
            </div>
          </div>
        </div>
    </div>
  </div>   
<div style="clear: both;"></div>