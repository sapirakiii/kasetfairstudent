<?php //debug($news) ?>
<?php 
		date_default_timezone_set('Asia/Bangkok');
		$date = date("Y-m-d");
		$time = date("H:i:s");

		function DateDiff($strDate1,$strDate2){                             
		return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
		}


	?>
<div class="container">

<!-- <div class="portlet light ">
	<div class="portlet-title">
		<div class="caption">
			<?php echo $this->element('Infos/menu'); ?>
		</div>
	</div>
	//col-md-9
</div> -->
<div class="col-md-12">
<br>
		<div class="panel panel-default" style="border-color: #F2F2F2!important">
			<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			    <h1 class="panel-title"><div class="font_panel2">VIDEO</div></h1>
            </div>
            <div class="panel-body">

            <div class="row">
            <div class="col-lg-6 portfolio-item">
              <div class="card h-100">
              <iframe width="506" height="289" src="https://www.youtube.com/embed/M3j2VNVm0N4" frameborder="0" allowfullscreen></iframe>
                <!-- <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a> -->
                <div class="card-body">
                  <h4 class="card-title">
                    <a href="#"><div class="font_panel2">งานเกษตรภาคเหนือ ครั้งที่ 8</div></a>
                  </h4>
                  <p class="card-text">ขอเชิญเที่ยวงานเกษตรภาคเหนือ ครั้งที่ 8 ณ คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่</p>
                </div>
              </div>
            </div>
             <div class="col-lg-6 portfolio-item">
              <div class="card h-100">
                <iframe width="506" height="289" src="https://www.youtube.com/embed/8n7UyC_jTzA" frameborder="0" allowfullscreen></iframe>
                <div class="card-body">
                  <h4 class="card-title">
                    <a href="#"><div class="font_panel2">เชิญชวนเข้ารับฟังงานเสวนาชี้ช่องอนาคตธุรกิจเกษตรยุค 4.0</div></a>
                  </h4>
                  <p class="card-text">งานเสวนาชี้ช่องอนาคตธุรกิจเกษตรยุค 4.0</p>
                </div>
              </div>
            </div>
            <!--<div class="col-lg-6 portfolio-item">
              <div class="card h-100">
                <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                <div class="card-body">
                  <h4 class="card-title">
                    <a href="#">Project Three</a>
                  </h4>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-6 portfolio-item">
              <div class="card h-100">
                <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                <div class="card-body">
                  <h4 class="card-title">
                    <a href="#">Project Four</a>
                  </h4>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit aliquam aperiam nulla perferendis dolor nobis numquam, rem expedita, aliquid optio, alias illum eaque. Non magni, voluptates quae, necessitatibus unde temporibus.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-6 portfolio-item">
              <div class="card h-100">
                <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                <div class="card-body">
                  <h4 class="card-title">
                    <a href="#">Project Five</a>
                  </h4>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-6 portfolio-item">
              <div class="card h-100">
                <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                <div class="card-body">
                  <h4 class="card-title">
                    <a href="#">Project Six</a>
                  </h4>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit aliquam aperiam nulla perferendis dolor nobis numquam, rem expedita, aliquid optio, alias illum eaque. Non magni, voluptates quae, necessitatibus unde temporibus.</p>
                </div>
              </div>
            </div> -->
          </div>
          <!-- /.row -->
                
           <br>
		<div class="panel panel-default" style="border-color: #F2F2F2!important">
			<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			    <h1 class="panel-title"><div class="font_panel2">KasetFair ON TV </div></h1>
            </div>
            <div class="panel-body">
            <div class="row">
            <div class="col-lg-6 portfolio-item">
            <div class="card h-100">
            <iframe width="506" height="289" src="https://www.youtube.com/embed/PN-Y6jTguAc" frameborder="0" allowfullscreen></iframe>
              <!-- <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a> -->
              <div class="card-body">
                <h4 class="card-title">
                  <a href="#"><div class="font_panel2">[WETV] คณะเกษตรศาสตร์ มช แถลงข่าว “งานเกษตรภาคเหนือ ครั้งที่ 8</div></a>
                </h4>
                <p class="card-text"></p>
              </div>
            </div>
          </div>
          <div class="col-lg-6 portfolio-item">
          <div class="card h-100">
          <iframe width="506" height="289" src="https://www.youtube.com/embed/H__bVNYj8A8" frameborder="0" allowfullscreen></iframe>
            <!-- <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a> -->
            <div class="card-body">
              <h4 class="card-title">
                <a href="#"><div class="font_panel2">[NBT] เกษตร มช เตรียมจัดงานเกษตรภาคเหนือครั้งที่8 19 ตุลาคม 2560</div></a>
              </h4>
              <p class="card-text"></p>
            </div>
          </div>
        </div>
            <!-- <div class="col-lg-6 portfolio-item">
              <div class="card h-100">
                <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                <div class="card-body">
                  <h4 class="card-title">
                    <a href="#">Project Three</a>
                  </h4>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-6 portfolio-item">
              <div class="card h-100">
                <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                <div class="card-body">
                  <h4 class="card-title">
                    <a href="#">Project Four</a>
                  </h4>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit aliquam aperiam nulla perferendis dolor nobis numquam, rem expedita, aliquid optio, alias illum eaque. Non magni, voluptates quae, necessitatibus unde temporibus.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-6 portfolio-item">
              <div class="card h-100">
                <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                <div class="card-body">
                  <h4 class="card-title">
                    <a href="#">Project Five</a>
                  </h4>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam viverra euismod odio, gravida pellentesque urna varius vitae.</p>
                </div>
              </div>
            </div>
            <div class="col-lg-6 portfolio-item">
              <div class="card h-100">
                <a href="#"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                <div class="card-body">
                  <h4 class="card-title">
                    <a href="#">Project Six</a>
                  </h4>
                  <p class="card-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugit aliquam aperiam nulla perferendis dolor nobis numquam, rem expedita, aliquid optio, alias illum eaque. Non magni, voluptates quae, necessitatibus unde temporibus.</p>
                </div>
              </div>
            </div> -->
          </div>
          <!-- /.row -->
       </div>
    </div>
</div>
