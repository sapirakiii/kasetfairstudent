<?php //debug(WWW_ROOT.'files'.DS.'Document');?>
<?php
	function DateThai($strDate)
		{

		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//, $strHour:$strMinute
	}
	?>
  <style type="text/css">
			input.largerCheckbox
			{
				width: 30px;
				height: 30px;
			}
			input.largerCheckbox2
			{
				width: 27px;
				height: 27px;
			}
		</style>
<style>
.numberCircle {
    border-radius: 50%;
    behavior: url(PIE.htc); /* remove if you don't care about IE8 */
    width: 36px;
    height: 36px;
    padding: 8px;
    background: #fff;
    border: 2px solid black;
    color: black;
    text-align: center;
    font: 20px Arial, sans-serif;
    display: inline-block;
}
</style>
 
<div class="container-fluid">
	<div class="row">
   
        <form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">
          <div class="col-sm-6">
             
            <table class="table table-hover table-post" style="width: 100%;">
                      <tr>
                          <td>    
                          <div class="font_panel5">	
                            <h3 ><div class="numberCircle">1</div> ข้อมูลผู้สมัคร</h3>
                            
                          </div> 
                          </td>
                      </tr>
                      <tr>
                        <td>
                            <label class="col-sm-3 control-label"> รหัสนักศึกษา  </label> 
                            <div class="col-sm-9"> 
                                        <?php echo $memberStudents['MemberStudent']['student_id']; ?> 
                            </div>                        
                        </td>
                      </tr>
                      <tr>
                        <td>
                            <label class="col-sm-3 control-label"> ชื่อ-สกุล </label> 
                            <div class="col-sm-9"> 
                              <?php echo $memberStudents['MemberStudent']['student_firstname'];?> <?php echo $memberStudents['MemberStudent']['student_lastname'];?><br>
										 	 
                            </div>                        
                        </td>
                      </tr>
                      <tr>
                        <td>
                            <label class="col-sm-3 control-label"> เบอร์โทรศัพท์</label> 
                            <div class="col-sm-9"> 
                               <?php echo $memberStudents['MemberStudent']['student_mobile'];?>
										 	 
                            </div>                        
                        </td>
                      </tr>
                      <tr>
                        <td>
                            <label class="col-sm-3 control-label">สาขาวิชา </label> 
                            <div class="col-sm-9"> 
                              <?php echo $memberStudents['Major']['major_name']; ?>
                            </div>                        
                        </td>
                      </tr>
                      <tr>
                        <td>
                            <label class="col-sm-3 control-label">ภาควิชา / หน่วยงาน </label> 
                            <div class="col-sm-9"> 
                              <?php echo $Organizes['Organize']['name']; ?>
                            </div>                        
                        </td>
                      </tr>
                      <tr>
                          <td>
                              <label class="col-sm-3 control-label"> ชื่อหัวข้อภาษาไทย <font color="red">*</font> </label> 
                              <div class="col-sm-9">
                                <?php echo $memberStudents['MemberStudent']['title']; ?>     
                              </div>  
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <label class="col-sm-3 control-label"> ชื่อหัวข้อภาษาอังกฤษ <font color="red">*</font> </label> 
                              <div class="col-sm-9">
                                <?php echo $memberStudents['MemberStudent']['title_eng']; ?>        
                              </div>  
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <label class="col-sm-3 control-label"> ชื่อหัวข้อภาษาอังกฤษ <font color="red">*</font> </label> 
                              <div class="col-sm-9">
                                <?php echo $memberStudents['MemberStudent']['title_eng']; ?>        
                              </div>  
                          </td>
                      </tr>
                      <tr>
                          <td>
                              <label class="col-sm-3 control-label"> ประเภท <font color="red">*</font> </label> 
                              <div class="col-sm-9">
                                <?php echo $memberStudents['SpecialAndCooperativeType']['name']; ?>        
                              </div>  
                          </td>
                      </tr>
                   
                      <tr>
                          <td>
                              <label class="col-sm-3 control-label"> สำเนาบัตรนักศึกษา </label> 
                              <div class="col-sm-9">
                                <a href="https://www.agri.cmu.ac.th/smart_academic/files/students/agricultural_expo/<?php echo $student_id; ?>/<?php echo $memberStudents['MemberStudent']['files']; ?>" class="btn btn-primary" target="_blank">
                                  สำเนาบัตรนักศึกษา
                                </a>
                              </div>  
                          </td>
                      </tr>
                   
                      
                     
                </table>
                <hr>
          </div>
          <div class="col-sm-6"> 
                <table class="table table-hover table-post" style="width: 100%;">
                  <tr>
                      <td>                
                          <div class="font_panel5">	
                            <h3 ><div class="numberCircle">2</div> 
                              <font color="red"> อาจารย์ที่ปรึกษา </font>
                            </h3>
                          </div>
                      </td>
                  </tr>
                  <tr>
                    <td>อาจารย์ที่ปรึกษา</td>
                    <td>
                      <?php if($memberStudents['MemberStudent']['advisor_id'] != null){  ?>  
                        <?php echo $memberStudents['Advisor']['advisor_name'].' '.$memberStudents['Advisor']['advisor_sname']; ?> 
                      <?php }else { ?> 
                        <font color="red"> ยังไม่ได้ระบุอาจารย์ที่ปรึกษา </font> 
                      <?php } ?> 
                    </td>
                  </tr>
                  
                </table>
                <hr>
                
                <table class="table table-hover table-post" style="width: 100%;">  
                  <tr>
                      <td>                
                          <div class="font_panel5">	
                            <h3 ><div class="numberCircle">3</div> ไฟล์เอกสารประกอบ </h3>
                
                          </div>
                      </td>
                  </tr> 
                  <tr>
                    <td>
                      <table class="table table-bordered">
                          <tbody>
                            <tr>
                                <td>
                                    <?php echo $output; ?>
                                </td>
                            </tr>                                
                          </tbody>
                      </table>  
                    </td>
                  </tr>  
                </table>
                
                   
                </div> 
              </div>
          
          </div>
        </form>
	</div>
</div> <!-- /container -->