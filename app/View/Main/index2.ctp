<?php
	function DateThai($strDate)
		{

		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//, $strHour:$strMinute
	}
	?>
	<?php 
		date_default_timezone_set('Asia/Bangkok');
		$date = date("Y-m-d");
		$time = date("H:i:s");
	

		function DateDiff($strDate1,$strDate2){                             
		return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
		}

		function TimeDiff($strTime1,$strTime2) {                             
            return (strtotime($strTime2) - strtotime($strTime1))/  ( 60 * 60 ); // 1 Hour =  60*60                           
        }

	?>
<div class="container-fluid" style="padding:15px 0 0;">
	<div class="panel panel-default" style="border-color: #F2F2F2!important">
		<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
				<div class="panel-title">
					<h3 style="margin:0px !important;">
						<center><div class="font_panel2">กิจกรรมนักเรียนและนักศึกษา</div></center>
					</h3> 
				</div>
			</div>	
		<div class="panel-body" style="padding-top: 15px; ">
			<div align="center"> 	
				<h4>กำหนดการกิจกรรมนักเรียนและนักศึกษา</h4>
				<h4>ในงานเกษตรภาคเหนือ ครั้งที่ 10 “นวัตกรรมเกษตรอัจฉริยะเพื่อเป้าหมายการพัฒนาที่ยั่งยืน”</h4>
				<h4>ในระหว่างวันที่ 1-12 ธันวาคม 2565</h4>
				<br>
			</div> 
			<div id="no-more-tables"> 				
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th style="text-align: center;">ลำดับ</th>
							<th style="text-align: center;">ชื่อกิจกรรม</th>
							<th style="text-align: center;" width="10%">วันที่/เวลา</th>
							<th style="text-align: center;">วันที่รับสมัคร</th>
							<th style="text-align: center;">สถานที่</th>
							<th style="text-align: center;">จำนวนผู้ลงทะเบียน</th>
							<th style="text-align: center;">ลงทะเบียน</th>
							<th style="text-align: center;">ตรวจสอบรายชื่อ</th>
							<?php
								if(!isset($admin)){
								?>
								<div class="row">
									<div class="col-md-12">
										<?php
										if(isset($admin)){
										?>
											<th style="text-align: center;">ปริ้นท์รายชื่อ </th>
										<?php
										}
										else{
										?>
											
										<?php
										}
										?>
										
									</div>
								</div>
								<?php
								}
								?>
							
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 0;
							foreach ($projects as $project){
								$i++;
							?>
								<tr>
									<td data-title="ลำดับ" align="center"><?php echo $i; ?></td>
									<td data-title="ชื่อกิจกรรม">
										<?php 
										if($project['Project']['file_name'] != null){
										?>
											<a href="<?php echo $this->Html->url('/files/'.$project['Project']['file_name']); ?>" target="_blank">
												<?php echo $project['Project']['name']; ?>
											</a>
										<?php 
											
										}
										else{
											echo $project['Project']['name']; 
										}
										
											
										?>
										
									</td>
									<td data-title="วันที่/เวลา"><?php echo $project['Project']['date']; ?><br><?php echo $project['Project']['time']; ?></td>
									<?php  if(DateDiff($date." ".$time,$project['Project']['dateout']." ".$project['Project']['timestop'] ) > 0 ) { ?>
										<td>รับสมัครวันนี้ จนถึง วันที่ <font color="red">
											<?php 
												$time1 = strtotime($project['Project']['dateout']);
												$newformat1 = date('d-M-Y', $time1);
													
												echo DateThai($newformat1)  ?> เวลา <?php echo date('H.i',strtotime($project['Project']['timestop'])); 
												?> น.</font>
										</td>
									<?php }else { ?>
										<td data-title="ลงทะเบียน">
											<button class="btn btn-danger " style="cursor: none;">
												<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 
												ปิดรับการลงทะเบียน						
												
											</button>
										</td>
                                  <?php } ?> 
									
									<td data-title="สถานที่">
										<?php echo $project['Project']['place']; ?>
									</td>
									<td data-title="สถานที่">
										<h4 align="center"><font color="green"><?php echo ($countMembersList[$i - 1]); ?></font></h4>
									</td>
									<?php  if(DateDiff($date." ".$time,$project['Project']['dateout']." ".$project['Project']['timestop'] ) > 0 ) { ?>
                                    
                                    <?php 
                                    	//debug($date." ".$time);
                                    	//debug($listActivityTopic['ActivityTopic']['topic_dateout']." ".$listActivityTopic['ActivityTopic']['topic_timestop']);
                                        //if($countMembersList[$i - 1] < $listActivityTopic['ActivityTopic']['max']){
                                        ?>
                                        <td data-title="ลงทะเบียน">
											<a class="btn btn-primary" href="<?php echo $this->Html->url(array('action' => 'register',$project['Project']['id'])); ?>" role="button" target="_blank">ลงทะเบียน</a>					
																	
                                        </td>
                                        <!-- <?php //} else{ ?>
                                        <td data-title="ลงทะเบียน">                                    
                                          <button class="btn btn-danger" style="cursor: none;">
                                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 
                                            เต็ม						
                                          </button>
                                        </td>
                                        <?php //} ?> -->
                                  <?php }else { ?>
                                    <td data-title="ลงทะเบียน">
                                      <button class="btn btn-danger " style="cursor: none;">
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 
                                        ปิดรับการลงทะเบียน						
                                        
                                      </button>
                                    </td>
                                  <?php } ?> 
									<td data-title="ตรวจสอบรายชื่อ" style="text-align: center;">								
										<a class="btn btn-success" href="<?php echo $this->Html->url(array('action' => 'studentchecklist',$project['Project']['id'])); ?>" role="button" >ตรวจสอบรายชื่อ</a>								
									</td>
									<?php
										if(!isset($admin)){
										?>
										<div class="row">
											<div class="col-md-12">
												<?php
												if(isset($admin)){
												?>
													<td data-title="ปริ้นท์รายชื่อ" style="text-align: center;">
														<a class="btn btn-warning pull-right" href="<?php echo $this->Html->url(array('action' => 'ListPrint',$project['Project']['id'])); ?>"
															role="button" target="_blank"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Print หน้าลงทะเบียน</a>
													</td>
												<?php
												}
												else{
												?>
													
												<?php
												}
												?>
												
											</div>
										</div>
										<?php
										}
										?>
									
								</tr>
							<?php
							}
						?>						
						
					</tbody>
				</table>
			</div>
	
		</div>

	</div>
	
	<div class="row">
		<div class="col-md-6">
					
			

		</div>
		<div class="col-md-6">	
			<div class="alert alert-info">
				ติดต่อสอบถามเพิ่มเติมได้ที่ งานบริการการศึกษา และพัฒนาคุณภาพนักศึกษา
				<br>
				คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่ โทรศัพท์ 053 - 944641-2, 053 - 944644 โทรสาร 053 - 944666  (ในวันเวลาราชการ)
			</div>
		</div>
	</div>
	
</div>
