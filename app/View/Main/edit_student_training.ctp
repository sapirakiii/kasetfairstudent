<div class="container">
      <div class="row">
            <div class="col-md-12">
                  <div class="panel panel-success">
                  <div class="panel-heading">
                        <h3 class="panel-title"><i class="glyphicon glyphicon-plus"></i> แก้ไขข้อมูล</h3> 
                  </div>
                  <div class="panel-body">
                        <div>
                              <form method="post" accept-charset="utf-8">
                                    <?php
                                          echo $this->Form->input('StudentTraining.id',array(
                                                'type'=>'hidden',
                                                'label'=>false,
                                                'hidden',
                                          ));
                                    ?>
                                    <label class="col-sm-4 control-label">กิจกรรม</label>
                                    <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                          <?php
                                                echo $this->Form->input('StudentTraining.name',array(
                                                      'type'=>'text',
                                                      'label'=>false,
                                                      'div'=>false,
                                                      'class'=>array('form-control css-require'),
                                                      'error'=>false,
                                                      'placeholder'=>'กิจกรรม',
                                                      'required'
                                                ));
                                          ?>
                                          </div>
                                    </div>
                                    <label class="col-sm-4 control-label">วันที่จัดกิจกรรม</label>
                                    <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                          <?php
                                                echo $this->Form->input('StudentTraining.postdate',array(
                                                      'type'=>'text',
                                                      'label'=>false,
                                                      'div'=>false,
                                                      'class'=>array('form-control css-require'),
                                                      'error'=>false,
                                                      'placeholder'=>'2024-06-24',
                                                      'required'
                                                ));
                                          ?>
                                          </div>
                                    </div>
                                    <label class="col-sm-4 control-label">เดือน</label>
                                    <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                          <?php
                                                $months = array(
                                                      '1' => '1',
                                                      '2' => '2',
                                                      '3' => '3',
                                                      '4' => '4',
                                                      '5' => '5',
                                                      '6' => '6',
                                                      '7' => '7',
                                                      '8' => '8',
                                                      '9' => '9',
                                                      '10' => '10',
                                                      '11' => '11',
                                                      '12' => '12',
                                                );
                                                echo $this->Form->input('StudentTraining.month', array(
                                                      'options' => $months,
                                                      'label' => false,
                                                      'div' => false,
                                                      'class' => array('form-control css-require'),                                                           
                                                      'error' => false,
                                                      'required'
                                                ));
                                          ?>
                                          </div>
                                    </div>
                                    <label class="col-sm-4 control-label">ปีที่เก็บข้อมูล</label>
                                    <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                          <?php
                                                echo $this->Form->input('StudentTraining.year',array(
                                                      'type'=>'number',
                                                      'label' =>false,
                                                      'div'=>false,
                                                      'class'=>array('form-control css-require'),
                                                      'error' => false,
                                                      'required',
                                                      'placeholder' => '2567'
                                                ));
                                          ?>
                                          </div>
                                    </div>
                                    <label class="col-sm-4 control-label">จำนวนผู้เข้าร่วม</label>
                                    <div class="col-sm-8">
                                          <div class="form-group has-feedback">
                                          <?php
                                                echo $this->Form->input('StudentTraining.count_student',array(
                                                      'type'=>'text',
                                                      'label' =>false,
                                                      'div'=>false,
                                                      'class'=>array('form-control css-require'),
                                                      'error' => false,
                                                      'required',
                                                      'placeholder' => 'จำนวนผู้เข้าร่วม'
                                                ));
                                          ?>
                                          </div>
                                    </div><br>
                                    <input type="submit" role="button" onclick="return confirm('ยืนยันการเพิ่มข้อมูล')" class="btn btn-success" value="บันทึกข้อมูล">
                              </form>
                        </div>
                        </div>
                  </div>
            </div>
      </div>
</div>