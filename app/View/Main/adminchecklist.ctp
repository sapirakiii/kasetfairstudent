<?php 
		date_default_timezone_set('Asia/Bangkok');
		$date = date("Y-m-d");
		$time = date("H:i:s");
	

		function DateDiff($strDate1,$strDate2){                             
		return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
		}

		function TimeDiff($strTime1,$strTime2) {                             
            return (strtotime($strTime2) - strtotime($strTime1))/  ( 60 * 60 ); // 1 Hour = 60*60
        }

	?>
	
<div class="container" style="padding:15px 0 0;">
	<div class="panel panel-default" style="border-color: #F2F2F2!important">
		<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center><div class="font_panel2">รายชื่อผู้ลงทะเบียน</div></center>
				</h3> 
			</div>
		</div>	
		<div class="panel-body" style="padding-top: 15px; ">
			<div align="center"> 	
				<h4><?php echo $project['Type']['name']; ?></h4>
				
				<h3><?php echo $project['Project']['name']; ?></h3>
				<h3>วัน เวลาที่แข่งขัน : <?php echo $project['Project']['date']; ?><br><?php echo $project['Project']['time']; ?>
					 
				</h3>
				<h3>สถานที่แข่งขัน :
					<a href="<?php echo $project['Project']['link']; ?>" target="_blank">
						<span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>  <?php echo $project['Project']['place']; ?>
					</a>
				</h3>
			</div> 
			
			<?php
			if(!isset($admin)){
			?>
			<div class="row">
				<div class="col-md-12">
					<?php
					if(isset($citizenId)){
					?>
						<a class="btn btn-warning pull-right" href="<?php echo $this->Html->url(array('action' => 'logout',$project['Project']['id'])); ?>" role="button">ออกจากระบบ</a>	
					<?php
					}
					else{
					?> 
						<!-- <a class="btn btn-danger pull-right" href="<?php echo $this->Html->url(array('action' => 'login',$project['Project']['id'])); ?>" role="button"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> ยกเลิกการลงทะเบียน</a>	 -->
					<?php
					}
					?>
					
				</div>
			</div>
			<?php
			}
			?>
			
			
				
					
					 
			
				<!-- <button class="btn btn-danger pull-right" style="cursor: none;">
						<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 
						ปิดรับการลงทะเบียน						
					</button> -->

					 <?php  if(DateDiff($date." ".$time,$project['Project']['dateout']." ".$project['Project']['timestop'] ) > 0 ) { ?>
						<!-- <a class="btn btn-primary pull-right" href="<?php echo $this->Html->url(array('action' => 'register',$project['Project']['id'])); ?>"
							role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> 
							ลงทะเบียนเพิ่มเติม
						</a> -->
					<?php }else { ?>
						
					<?php } ?> 
					<?php  if($UserName != null) { ?>
						<!-- <a class="btn btn-primary btn-lg pull-right" href="<?php echo $this->Html->url(array('action' => 'list_print',$project['Project']['id'])); ?>"
						target="_blank">
							<span class="glyphicon glyphicon-print" aria-hidden="true"></span> Print รายชื่อ
						</a>
					  -->
					<?php } ?>
			 
			<div id="no-more-tables"> 				
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th style="text-align: center;">ลำดับ</th>
							<th style="text-align: center;">ชื่อ - นามสกุล</th>
							<th style="text-align: center;">ชื่อโรงเรียน</th>
							<th style="text-align: center;">จังหวัด</th>
							<th style="text-align: center;">ระดับ</th>
							<th style="text-align: center;">อาจารย์ผู้ควบคุม</th>
							<th style="text-align: center;">เบอร์โทรศัพท์ติดต่อ</th>
							<?php
							if($project['Type']['id'] == 2 && $project['Project']['price'] != 0){
							?>
								<th style="text-align: center;">สถานะ</th>
							<?php
							}
							
							if(isset($citizenId) || isset($admin)){
							?>
								<th style="text-align: center;">ยกเลิกการลงทะเบียน</th>
							<?php
							}
							
							?>
						</tr>
					</thead>
					<!-- <tbody>
						<?php
							$i = 0;
							foreach ($members as $member){
								$i++;
							?>
								<tr>
									<td data-title="ลำดับ" align="center"><?php echo $i; ?></td>
									<td data-title="ชื่อ - นามสกุล">
										<?php 
											echo $member['Prefix']['name'].$member['Member']['fname'].' '.$member['Member']['lname']; 
										?>
										<?php 
											if(isset($citizenId) || isset($admin)){
												echo $member['Member']['phone']; 

											}
										?>
									</td>
									<td data-title="ชื่อโรงเรียน">
										<?php 
											echo $member['Member']['school']; 
										?>
										
									</td>
									<td data-title="ระดับ">
										<?php 
											echo $member['Member']['level']; 
										?>
										
									</td>
									<td data-title="อาจารย์ผู้ควบคุม">
										<?php 
											echo $member['Member']['teacher']; 
										?>
										<?php 
											if(isset($citizenId) || isset($admin)){
												echo $member['Member']['phoneteacher']; 

											}
										?>
									</td>
									
									<?php
									if($project['Type']['id'] == 2 && $project['Project']['price'] != 0){
										$statusColor = 'red';
										if($member['Status']['id'] == 2){
											$statusColor = 'green';										
										}
									?>
										<td data-title="สถานะ" align="center">
											<?php
											if(isset($admin)){
												$this->request->data = $member;
											?>
												<?php
												echo $this->Form->create('Member',array('class' => 'form-inline')); 	
												echo $this->Form->input('id'); 
												?>

												  <div class="form-group">
													<?
													
													echo $this->Form->input('status_id', array(
														'options' => $statuses,
														'class' => 'form-control',
														'label' => false
													));
													
													?>
												  </div>				  
												  <button type="submit" class="btn btn-primary">บันทึก</button>
												</form>
											<?php
											}
											else{
											?>
											
												<span style="color: <?php echo $statusColor; ?>;"><?php echo $member['Status']['name']; ?></span>
											
											<?php	
											}
											?>											
											
										</td>
									<?php
									}
									
									if(isset($citizenId) || isset($admin)){
									?>
										<td data-title="ยกเลิกการลงทะเบียน" style="text-align: center;">	
											<a href="<?php echo $this->Html->url(array('action' => 'delete_member',$project['Project']['id'],$member['Member']['id'])); ?>" class="btn btn-danger" onclick="return confirm('คุณแน่ใจหรือไม่?')">
												<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
												ยกเลิก
											</a>
															
										</td>
									<?php
									}
									
									?>
									
									
								</tr>
							<?php
							}
						?>						
						
					</tbody> -->
					<!-- ส่วนที่แก้ไข -->
					<tbody>

						<?php 
							$i=0;
							foreach($schools as $school){ 
						?>
							<tr>
								<td data-title="กลุ่มโรงเรียน" colspan="7">
									<b><?php echo $school['Member']['school']; ?></b>
								</td>
								<!-- <td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td></td> -->
							</tr>
							<?php
								foreach($students as $student){
									if($school['Member']['school'] == $student['Member']['school']){ 
									$i++
							?>
								<tr>
									<td data-title="ลำดับ" align="center"><?php echo $i; ?></td>
									 
									<td data-title="ชื่อ - นามสกุล">
										<?php 
											echo $student['Prefix']['name'].$student['Member']['fname'].' '.$student['Member']['lname']; 
										?>
										<?php 
											if(isset($citizenId) || isset($admin)){
												echo $student['Member']['phone']; 

											}
										?>
									</td>
									<td data-title="ชื่อโรงเรียน">
										<?php 
											echo $student['Member']['school']; 
										?>
										
									</td>
									<td data-title="จังหวัด">
										<?php 
											echo $student['Member']['school_province']; 
										?>
										
									</td>
									<td data-title="ระดับ">
										<?php 
											echo $student['Member']['level']; 
										?>
										
									</td>
									<td data-title="อาจารย์ผู้ควบคุม">
										<?php 
											echo $student['Member']['teacher']; 
										?>
										<?php 
											if(isset($citizenId) || isset($admin)){
												echo $student['Member']['phoneteacher']; 

											}
										?>
									</td>
									<td>
										<?php  if($UserName != null) { ?>
											<?php 
												echo $student['Member']['phoneteacher']; 
											?>  
										<?php } ?>
									</td>
									<!-- <td>
										<?php  if($UserName != null) { ?>
											<?php 
												echo $student['Member']['phoneteacher']; 
											?>  
										<?php } ?>
									</td> -->
									<?php
									if($project['Type']['id'] == 2 && $project['Project']['price'] != 0){
										$statusColor = 'red';
										if($student['Status']['id'] == 2){
											$statusColor = 'green';										
										}
									?>
										<td data-title="สถานะ" align="center">
											<?php
											if(isset($admin)){
												$this->request->data = $student;
											?>
												<?php
												echo $this->Form->create('Member',array('class' => 'form-inline')); 	
												echo $this->Form->input('id'); 
												?>

												  <div class="form-group">
													<?
													
													echo $this->Form->input('status_id', array(
														'options' => $statuses,
														'class' => 'form-control',
														'label' => false
													));
													
													?>
												  </div>				  
												  <button type="submit" class="btn btn-primary">บันทึก</button>
												</form>
											<?php
											}
											else{
											?>
											
												<span style="color: <?php echo $statusColor; ?>;"><?php echo $student['Status']['name']; ?></span>
											
											<?php	
											}
											?>											
											
										</td>
									<?php
									}
									
									if(isset($citizenId) || isset($admin)){
									?>
										<td data-title="ยกเลิกการลงทะเบียน" style="text-align: center;">	
											<a href="<?php echo $this->Html->url(array('action' => 'delete_member',$project['Project']['id'],$student['Member']['id'])); ?>" class="btn btn-danger" onclick="return confirm('คุณแน่ใจหรือไม่?')">
												<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
												ยกเลิก
											</a>
															
										</td>
									<?php
									}
									
									?>
									
									
								</tr>
						<?php 		
									}
								}
							} 
						?>
					</tbody>
				</table>
			</div>			
	
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-6">
			 
		</div>
		<div class="col-md-6">	
			
			<div class="alert alert-info" style="justify-content: center;">
				ติดต่อสอบถามเพิ่มเติมได้ที่ งานบริการการศึกษา และพัฒนาคุณภาพนักศึกษา
				<br>
				คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่ โทรศัพท์ 053 - 944641-2, 053 - 944644 โทรสาร 053 - 944666  (ในวันเวลาราชการ)
			</div>
			
		</div>
	</div>
	
</div>
