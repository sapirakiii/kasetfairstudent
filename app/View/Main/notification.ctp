<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div align="center">
                <h3>Notification</h3>
            </div><br>
            <?php if($admins != null) { ?>
            <a class="btn btn-success" role="button" target="_blank" href="<?php echo $this->Html->url(array('action' => 'add_notification'));?>">เพิ่มข้อมูล</a>
            <a class="btn btn-info" role="button" target="_blank" href="<?php echo $this->Html->url(array('action' => 'notification_view'));?>">ดูข้อมูลทั้งหมด</a>
            <?php } ?>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="20%" colspan="" rollspan="" style="text-align:center">หัวข้อข่าว</th>
                        <th width="20%" colspan="" rollspan="" style="text-align:center">รายละเอียด</th>
                        <?php if($admins != null) { ?>
                        <th width="5%" colspan="" rollspan="" style="text-align:center">แก้ไขข้อมูล</td>
                        <th width="5%" colspan="" rollspan="" style="text-align:center">ลบข้อมูล</td>
                        <?php } ?>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    foreach ($notifications as $notification){
                    ?>
                    <tr>
                        <?php
                            if($notification['Notification']['level'] != 0){
                        ?>
                        <td style="text-align:center"><?php echo $notification['Notification']['title'];?></td>
                        <td style="text-align:center"><?php echo $notification['Notification']['name'];?></td>
                        <?php if($admins != null) { ?>
                        <td style="text-align:center"><a class="btn btn-warning" target="_blank" href="<?php echo $this->Html->url(array('action'=>'edit_notification',$notification['Notification']['id'])); ?>">แก้ไขข้อมูล</a></td>
                        <td style="text-align:center"><a class="btn btn-danger" onclick="return confirm('ยืนยันการลบข้อมูล')" href="<?php echo $this->Html->url(array('action'=>'remove_notification',$notification['Notification']['id'])); ?>">ลบข้อมูล</a></td>
                        <?php } ?>
                        <?php } ?>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>