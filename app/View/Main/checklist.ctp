<div class="container" style="padding:15px 0 0;">
<div class="panel panel-default" style="border-color: #F2F2F2!important">
	<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center><div class="font_panel2">รายชื่อผู้ลงทะเบียน</div></center>
				</h3> 
			</div>
		</div>	
		<div class="panel-body" style="padding-top: 15px; ">
			<div align="center"> 	
				<h4><div class="font_panel7"><?php echo $project['Type']['name']; ?></div></h4>
				
				<h3><div class="font_panel7"><?php echo $project['Project']['name']; ?></div></h3> 
				
				<?php
				if($project['Type']['id'] != 7){
				?>
				<h3><div class="font_panel7">ณ <?php echo $project['Project']['place']; ?> วันที่ <?php echo $project['Project']['date']; ?> เวลา <?php echo $project['Project']['time']; ?>
				</div></h3>
				<?php
				}
				?>
			</div> 
			
			<br>
			<div class="row"> 	
				<div class="col-md-12">
				
					 
						<a class="btn btn-primary pull-right" href="<?php echo $this->Html->url(array('action' => 'register',$project['Project']['id'])); ?>"
					 role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> ลงทะเบียนเพิ่มเติม</a>
					 
					 
						<a class="btn btn-warning pull-left" href="<?php echo $this->Html->url(array('action' => 'index' )); ?>" role="button">					
							<span class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span>ย้อนกลับ
						</a>
					 
				</div> 	
			</div> 				
			
			<div id="no-more-tables"> 				
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th style="text-align: center;">ลำดับ</th>
							<th style="text-align: center;">ชื่อ - นามสกุล</th>
							<?php
							if($project['Project']['id'] == 38 || $project['Project']['id'] == 37 || $project['Project']['id'] == 36){ ?>
								<th style="text-align: center;">เบอร์โทรศัพท์</th>
								<?php
							if($project['Project']['id'] == 38){ ?>
								<th style="text-align: center;">ประเภทการเข้าแข่งขัน</th>
								<?php } ?>
							<?php } ?>
							<?php
							if(isset($admin) && $project['Type']['id'] == 7){
								foreach ($flowerGroups as $flowerGroup){
								?>
									<th style="text-align: center;"><?php echo $flowerGroup['FlowerGroup']['name']; ?></th>
								<?php
								}
							}

							if($project['Type']['id'] == 9 || $project['Type']['id'] == 2 && $project['Project']['price'] != 0){
							?>
								<th style="text-align: center;">สถานะ</th>
							<?php
							}
							
							if(isset($citizenId) || isset($admin)){
							?>
								<th style="text-align: center;">เบอร์โทรศัพท์</th>
								<th style="text-align: center;">ยกเลิกการลงทะเบียน</th>
							<?php
							}
							
							?>
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 0;
							$fname = '';
							$lname = '';
							foreach ($members as $member){
																
								if($fname != trim($member['Member']['fname']) || $lname != trim($member['Member']['lname'])){
									$i++;
									$fname = trim($member['Member']['fname']);
									$lname = trim($member['Member']['lname']);
								?>
									<tr>
										<td data-title="ลำดับ" align="center"><?php echo $i; ?></td>
										<td data-title="ชื่อ - นามสกุล">
											<?php 
											$memberName = $member['Prefix']['name'].$member['Member']['fname'].' '.$member['Member']['lname']; 
											if(isset($admin)){
											?>
												<a href="<?php echo $this->Html->url(array('action' => 'member_detail',$member['Member']['id'])); ?>" target="_blank"><?php echo $memberName; ?></a>
											<?php 	
											}
											else{
												echo $memberName;
											}
											?>
											
										</td>
										<?php
										if($project['Project']['id'] == 38 || $project['Project']['id'] == 37 || $project['Project']['id'] == 36){ ?>
											<td data-title="เบอร์โทรศัพท์" align="center"><?php echo $member['Member']['phone'] ?></td>
											<?php
											if($project['Project']['id'] == 38){ ?>
											<?php if ($member['Member']['order'] == 1) {
												echo '<td data-title="ประเภทการเข้าแข่งชัน" align="center" class="warning">'.$member['Member']['singing_type'].'</td>';
											}else {
												echo '<td data-title="ประเภทการเข้าแข่งชัน" align="center" class="success">'.$member['Member']['singing_type'].'</td>';
											} ?>
											<?php } ?>
								
										<?php } ?>
										<?php
										if(isset($admin) && $project['Type']['id'] == 7){
											foreach ($flowerGroups as $flowerGroup){
											?>
												<td data-title="<?php echo $flowerGroup['FlowerGroup']['name']; ?>" align="center"><?php echo $memberFlowerGroupAmounts[$member['Member']['id']][$flowerGroup['FlowerGroup']['id']]; ?></td>
											<?php
											}
										}
								
										if($project['Type']['id'] == 9 || $project['Type']['id'] == 2 && $project['Project']['price'] != 0){
											$statusColor = 'red';
											if($member['Status']['id'] == 2){
												$statusColor = 'green';										
											}
										?>
											<td data-title="สถานะ" align="center">
												<?php
												if(isset($admin)){
													$this->request->data = $member;
												?>
													<?php
													echo $this->Form->create('Member',array('class' => 'form-inline')); 	
													echo $this->Form->input('id'); 
													?>

													  <div class="form-group">
														<?php
														
														echo $this->Form->input('status_id', array(
															'options' => $statuses,
															'class' => 'form-control',
															'label' => false
														));
														
														?>
													  </div>				  
													  <button type="submit" class="btn btn-primary">บันทึก</button>
													</form>
												<?php
												}
												else{
												?>
												
													<span style="color: <?php echo $statusColor; ?>;"><?php echo $member['Status']['name']; ?></span>
												
												<?php	
												}
												?>											
												
											</td>
										<?php
										}
										
										if(isset($citizenId) || isset($admin)){
										?>
										<td data-title="เบอร์โทรศัพท์" style="text-align: center;">	
														<?php echo $member['Member']['phone']; ?>
											
																		
											<td data-title="ยกเลิกการลงทะเบียน" style="text-align: center;">	
												<a href="<?php echo $this->Html->url(array('action' => 'delete_member',$project['Project']['id'],$member['Member']['id'])); ?>" class="btn btn-danger" onclick="return confirm('คุณแน่ใจหรือไม่?')">
													<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
													ยกเลิก
												</a>
																
											</td>
										<?php
										}
										
										?>
										
										
									</tr>
								<?php
								}
							}
						?>						
						
					</tbody>
				</table>
			</div>			
	
		</div>

	</div>
	
	<div class="row">
		<div class="col-md-6">
			<?php
			if($project['Type']['id'] == 2 && $project['Project']['price'] != 0){

			?>
									
				<div class="alert alert-warning">
					<span class="glyphicon glyphicon-stop" aria-hidden="true"></span>
					ชำระเงินค่าลงทะเบียนด้วยตนเอง ณ ศูนย์บริการวิชาการฯ ชั้น 2 อาคารเฉลิมพระเกียรติ   คณะเกษตรศาสตร์  มหาวิทยาลัยเชียงใหม่
					<br><br>
					<span class="glyphicon glyphicon-stop" aria-hidden="true"></span>
					โอนเงินเข้าบัญชีออมทรัพย์ ธนาคารกรุงไทย สาขามหาวิทยาลัยเชียงใหม่ 
					<br>
					ชื่อบัญชี: ศูนย์บริการวิชาการและถ่ายทอดเทคโนโลยีการเกษตร 
					<br>
					เลขที่บัญชี: 456-0-12117-6
					<br>
					พร้อมส่งหลักฐานการโอนเงิน มายัง Line Id . 083-6228414 
					หรือทางอีเมลล์ chaichet.agei@hotmail.com 

					<br><br>
					<div class="alert alert-danger">
						<span class="glyphicon glyphicon-stop" aria-hidden="true"></span>
						ผู้สมัครชำระค่าลงทะเบียนได้จนถึงวันที่ 25 ตุลาคม 2560 เวลา 16.00 น. หากพ้นกำหนดดังกล่าว ฝ่ายฝึกอบรมวิชาชีพระยะสั้น ขอให้สิทธิการลงทะเบียนสำหรับท่านอื่นที่จองไว้เเต่ไม่สามารถลงทะเบียนได้ต่อไป โดยไม่ต้องเเจ้งล่วงหน้า
					</div>
				</div>
			<?php
			}elseif ($project['Type']['id'] == 9 && $project['Project']['price'] != 0) { 	?>
				<div class="alert alert-warning">
					<span class="glyphicon glyphicon-stop" aria-hidden="true"></span>
					รับสมัครตั้งแต่วันนี้ - วันอังคารที่ 31 ตุลาคม 2560  หรือจนกว่าตู้ที่จัดไว้จะเต็ม <br>
					การแข่งขันการประกวดปลากัด   <br>
						ติดต่อ  คุณจักรินทร์  ใหม่วัน   โทร. 083-948-6773  <br><br>
					การแข่งขันการประกวดปลาหมอครอสบรีด <br>  
						ติดต่อ  คุณเอนก. บุญแก้ว.  โทร.084-040-0738	 <br><br>
					การแข่งขันการประกวดสัตว์น้ำสวยงาม  (กุ้งเครฟิชสวยงาม) <br>
						ติดต่อ  ณัฐวุฒิ วงศ์วิชัยแก้ว  โทร. 082-895-8056 <br><br>
					<br><br>
					<span class="glyphicon glyphicon-stop" aria-hidden="true"></span>
					<b>สำหรับการแข่งขันการประกวดปลากัด</b> รับสมัครทางระบบออนไลน์และชำระค่าสมัครหน้างาน ในวันที่ 6 พย. ตั้งแต่เวลา 9:00-18:00 น.  <br><br>
				    <b> อัตราค่าสมัครของปลาแต่ละประเภท </b><br>
					1.  ปลากัด					ค่าสมัครตัวละ  	150.-บาท <br>
					2.  ปลาหมอครอสบรีด  		ค่าสมัครตัวละ  	300.-บาท <br>
					3.  กุ้งเครฟิชสวยงาม		   ค่าสมัครตัวละ    200.-บาท <br>


					<br><br>

				</div>
			<?php
			}
			?>
		</div>
		<div class="col-md-6">	
			 
				<div class="alert alert-info">
				ติดต่อสอบถามเพิ่มเติมได้ที่ งานบริการการศึกษา และพัฒนาคุณภาพนักศึกษา
				<br>
				คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่ โทรศัพท์ 053 - 944641-4 ต่อ 117 (คุณสมโภชน์ อริยจักร์) ในวันและเวลาราชการ
				</div>
			 
		</div>
	</div>
	
</div>

