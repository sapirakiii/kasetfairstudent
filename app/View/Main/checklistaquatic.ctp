<div class="container" style="padding:15px 0 0;">
	<div class="panel panel-primary" >
		<div class="panel-heading"> 
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center>รายชื่อผู้ลงทะเบียน</center>
				</h3> 
			</div>
		</div>	
		<div class="panel-body" style="padding-top: 15px; ">
		
			<div align="center"> 	
				<h4><?php echo $project['Type']['name']; ?></h4>
				
				<h3><?php echo $project['Project']['name']; ?></h3> 
				
				<?php
				if($project['Type']['id'] != 9){
				?>
				<h3>ณ <?php echo $project['Project']['place']; ?> วันที่ <?php echo $project['Project']['date']; ?> เวลา <?php echo $project['Project']['time']; ?></h3>
				<?php
				}
				?>
			</div> 
			
			<br>
			<div class="row"> 

					<div class="col-md-12"> 	
						<!-- <a class="btn btn-primary pull-right" href="<?php echo $this->Html->url(array('action' => 'registeraquatic',$project['Project']['id'])); ?>"
						role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> ลงทะเบียนเพิ่มเติม</a> -->
						
						<?php
						if(!isset($admin)){

							if(isset($citizenId)){
							?>
								<a class="btn btn-warning pull-right" href="<?php echo $this->Html->url(array('action' => 'logout',$project['Project']['id'])); ?>" role="button">ออกจากระบบ</a>	
							<?php
							}
							else{
								
								if($project['Type']['id'] == 2){
								?>
									<a class="btn btn-danger pull-right" href="<?php echo $this->Html->url(array('action' => 'login',$project['Project']['id'])); ?>" role="button"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> ยกเลิกการลงทะเบียน</a>
								<?php
								}
							}

						}
						
						
						if(isset($project['Type']['action'])){
						?>
							<a class="btn btn-warning pull-left" href="<?php echo $this->Html->url(array('action' => $project['Type']['action'])); ?>" role="button">					
								<span class="glyphicon glyphicon-triangle-left" aria-hidden="true"></span>ย้อนกลับ
							</a>
						<?php
						}			
						?>
					</div> 	
				</div> 	

				<div class="table-responsive">
					<div id="no-more-tables"> 				
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
									<th style="text-align: center;">ลำดับ</th>
									<th style="text-align: center;">ชื่อ - นามสกุล</th>
									<?php
									if(isset($admin) && $project['Type']['id'] == 9){
										foreach ($aquaticGroups as $aquaticGroup){
										?>
											<th style="text-align: center;"><?php echo $aquaticGroup['AquaticGroup']['name']; ?></th>
										<?php
										}
									}

									if($project['Type']['id'] == 9 || $project['Type']['id'] == 2 && $project['Project']['price'] != 0){
									?>
										<th style="text-align: center;">สถานะ</th>
									<?php
									}
									
									if(isset($citizenId) || isset($admin)){
									?>
										<th style="text-align: center;">เบอร์โทรศัพท์</th>
										<th style="text-align: center;">ยกเลิกการลงทะเบียน</th>
									<?php
									}
									
									?>
								</tr>
							</thead>
							<tbody>
								<?php
									$i = 0;
									foreach ($members as $member){
										$i++;
									?>
										<tr>
											<td data-title="ลำดับ" align="center"><?php echo $i; ?></td>
											<td data-title="ชื่อ - นามสกุล">
												<?php 
												$memberName = $member['Prefix']['name'].$member['Member']['fname'].' '.$member['Member']['lname']; 
												if(isset($admin)){
												?>
													<a href="<?php echo $this->Html->url(array('action' => 'member_detail',$member['Member']['id'])); ?>" target="_blank"><?php echo $memberName; ?></a>
												<?php 	
												}
												else{
													echo $memberName;
												}
												?>
												
											</td>
											<?php
											if(isset($admin) && $project['Type']['id'] == 9){
												foreach ($aquaticGroups as $aquaticGroup){
												?>
													<td data-title="<?php echo $aquaticGroup['AquaticGroup']['name']; ?>" align="center"><?php echo $memberAquaticGroupAmounts[$member['Member']['id']][$aquaticGroup['AquaticGroup']['id']]; ?></td>
												<?php
												}
											}
									
											if($project['Type']['id'] == 9 || $project['Type']['id'] == 2 && $project['Project']['price'] != 0){
												$statusColor = 'red';
												if($member['Status']['id'] == 2){
													$statusColor = 'green';										
												}
											?>
												<td data-title="สถานะ" align="center">
													<?php
													if(isset($admin)){
														$this->request->data = $member;
													?>
														<?php
														echo $this->Form->create('Member',array('class' => 'form-inline')); 	
														echo $this->Form->input('id'); 
														?>

														<div class="form-group">
															<?php
															
															echo $this->Form->input('status_id', array(
																'options' => $statuses,
																'class' => 'form-control',
																'label' => false
															));
															
															?>
														</div>				  
														<button type="submit" class="btn btn-primary">บันทึก</button>
														</form>
													<?php
													}
													else{
													?>
													
														<span style="color: <?php echo $statusColor; ?>;"><?php echo $member['Status']['name']; ?></span>
													
													<?php	
													}
													?>											
													
												</td>
											<?php
											}
											
											if(isset($citizenId) || isset($admin)){
											?>
											<td data-title="เบอร์โทรศัพท์" style="text-align: center;">	
													<?php echo $member['Member']['phone']; ?>
										
																	
												</td>
												<td data-title="ยกเลิกการลงทะเบียน" style="text-align: center;">	
													<a href="<?php echo $this->Html->url(array('action' => 'delete_member',$project['Project']['id'],$member['Member']['id'])); ?>" class="btn btn-danger" onclick="return confirm('คุณแน่ใจหรือไม่?')">
														<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
														ยกเลิก
													</a>
																	
												</td>
											<?php
											}
											
											?>
											
											
										</tr>
									<?php
									}
								?>						
								
							</tbody>
						</table>
					</div>			
				</div>
		</div>

	</div>
	
	<div class="row">
		<div class="col-md-6">
			<?php
			if($project['Type']['id'] == 2 && $project['Project']['price'] != 0){

			?>
									
				<div class="alert alert-warning">
					<span class="glyphicon glyphicon-stop" aria-hidden="true"></span>
					ชำระเงินค่าลงทะเบียนด้วยตนเอง ณ ศูนย์บริการวิชาการฯ ชั้น 2 อาคารเฉลิมพระเกียรติ   คณะเกษตรศาสตร์  มหาวิทยาลัยเชียงใหม่
					<br><br>
					<span class="glyphicon glyphicon-stop" aria-hidden="true"></span>
					โอนเงินเข้าบัญชีออมทรัพย์ ธนาคารกรุงไทย สาขามหาวิทยาลัยเชียงใหม่ 
					<br>
					ชื่อบัญชี: ศูนย์บริการวิชาการและถ่ายทอดเทคโนโลยีการเกษตร 
					<br>
					เลขที่บัญชี: 456-0-12117-6
					<br>
					พร้อมส่งหลักฐานการโอนเงิน มายัง Line Id . 083-6228414 
					หรือทางอีเมลล์ chaichet.agei@hotmail.com 

					<br><br>
					<div class="alert alert-danger">
						<span class="glyphicon glyphicon-stop" aria-hidden="true"></span>
						ผู้สมัครชำระค่าลงทะเบียนได้จนถึงวันที่ 25 ตุลาคม 2560 เวลา 16.00 น. หากพ้นกำหนดดังกล่าว ฝ่ายฝึกอบรมวิชาชีพระยะสั้น ขอให้สิทธิการลงทะเบียนสำหรับท่านอื่นที่จองไว้เเต่ไม่สามารถลงทะเบียนได้ต่อไป โดยไม่ต้องเเจ้งล่วงหน้า
					</div>
				</div>
			<?php
			}elseif ($project['Type']['id'] == 9 && $project['Project']['price'] != 0) { 	?>
				<div class="alert alert-warning">
					<span class="glyphicon glyphicon-stop" aria-hidden="true"></span>
					รับสมัครตั้งแต่วันนี้ - วันอังคารที่ 31 ตุลาคม 2560  หรือจนกว่าตู้ที่จัดไว้จะเต็ม <br>
					การแข่งขันการประกวดปลากัด   <br>
						ติดต่อ  คุณจักรินทร์  ใหม่วัน   โทร. 083-948-6773  <br><br>
					การแข่งขันการประกวดปลาหมอครอสบรีด <br>  
						ติดต่อ  คุณเอนก. บุญแก้ว.  โทร.084-040-0738	 <br><br>
					การแข่งขันการประกวดสัตว์น้ำสวยงาม  (กุ้งเครฟิชสวยงาม) <br>
						ติดต่อ  ณัฐวุฒิ วงศ์วิชัยแก้ว  โทร. 082-895-8056 <br><br>
					<br><br>
					<span class="glyphicon glyphicon-stop" aria-hidden="true"></span>
					<b>สำหรับการแข่งขันการประกวดปลากัด</b> รับสมัครทางระบบออนไลน์และชำระค่าสมัครหน้างาน ในวันที่ 6 พย. ตั้งแต่เวลา 9:00-18:00 น.  <br><br>
				    <b> อัตราค่าสมัครของปลาแต่ละประเภท </b><br>
					1.  ปลากัด					ค่าสมัครตัวละ  	150.-บาท <br>
					2.  ปลาหมอครอสบรีด  		ค่าสมัครตัวละ  	300.-บาท <br>
					3.  กุ้งเครฟิชสวยงาม		   ค่าสมัครตัวละ    200.-บาท <br>


					<br><br>

				</div>
			<?php
			}
			?>
		</div>
		<div class="col-md-6">	
			<?php
			if($project['Type']['id'] == 2){

			?>
				<div class="alert alert-info">
					ติดต่อสอบถามเพิ่มเติมได้ที่ นายชัยเชษฐ์  โกฎธิ (คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่)
					<br>
					หมายเลขโทรศัพท์ภายใน 053-944088 มือถือหมายเลข 083-6228414 (ในวันเวลาราชการ)
					<br>
					อีเมลล์ chaichet.agei@hotmail.com Line Id . 083-6228414
				</div>
			<?php
			}
			else if($project['Type']['id'] == 1){
			?>
				<div class="alert alert-info">
					ติดต่อสอบถามเพิ่มเติมได้ที่ งานบริหารงานวิจัยและวิเทศสัมพันธ์ 
					<br>
					คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่ โทร. 053-944090 (ในวันเวลาราชการ)
				</div>
				<?php	
			}else if($project['Type']['id'] == 3){
			?>
				<div class="alert alert-info">
				ติดต่อสอบถามเพิ่มเติมได้ที่ งานบริการการศึกษา และพัฒนาคุณภาพนักศึกษา
				<br>
				คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่ โทรศัพท์ 053 - 944641 - 3 โทรสาร 053 - 944642  (ในวันเวลาราชการ)
				</div>
				<?php	
			}
			?>
		</div>
	</div>
	
</div>

