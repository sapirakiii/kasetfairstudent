<?php
function DateThai($strDate)
{

	$strYear = date("Y", strtotime($strDate)) + 543;
	$strMonth = date("n", strtotime($strDate));
	$strDay = date("j", strtotime($strDate));
	$strHour = date("H", strtotime($strDate));
	$strMinute = date("i", strtotime($strDate));
	$strSeconds = date("s", strtotime($strDate));
	$strMonthCut = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
	$strMonthThai = $strMonthCut[$strMonth];
	return "$strDay $strMonthThai $strYear";
	//, $strHour:$strMinute
}
?>





<?php
date_default_timezone_set('Asia/Bangkok');
$date = date("Y-m-d");
$time = date("H:i:s");


function DateDiff($strDate1, $strDate2)
{
	return (strtotime($strDate2) - strtotime($strDate1)) /  (60 * 60 * 24);  // 1 day = 60*60*24
}

function TimeDiff($strTime1, $strTime2)
{
	return (strtotime($strTime2) - strtotime($strTime1)) /  (60 * 60); // 1 Hour =  60*60                           
}




?>
<div class="container-fluid" style="padding:15px 0 0;">
	<div class="panel panel-default" style="border-color: #F2F2F2!important">
		<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center>
						<div class="font_panel2">จำนวนกระบวนวิชา Life Long Learning ในแต่ละภาคการศึกษา (ปีการศึกษา <?php echo $lastyearterms['Yearterm']['year'] ?>)</div>
					</center>
				</h3>
			</div>
		</div>
		<br>
		กรุณาเลือกปีการศึกษา
		<?php if($admins != null){ ?>
		<a class="btn btn-success" href="<?php echo $this->Html->url(array('action' => 'add_life_long_learning'));?>" role="button" target="_blank">เพิ่มข้อมูล</a>
		<?php } ?>
		<form name="frmselect" method="get"> 	
					<div class="dropdown"  style="float: left;">
						<button class="btn btn-default dropdown-toggle btn-info" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<?php echo $years['Yearterm']['year']; ?>
						</button>
						<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
							<?php foreach ($dropdowns as $dropdown) { ?>
								<?php 
									if($years['Yearterm']['year'] == $dropdown['Yearterm']['year']){
										$active = "active";
									}else{
										$active = "";
									}
									// $yearList = $dropdown['Yearterm']['year'];
								?>
								<li class="<?php echo $active;?>">
									<a href="<?php echo $this->Html->url(array('action' => 'life_long_learning',$dropdown['Yearterm']['year'])); ?>">
										<font color="black"><?php echo $dropdown['Yearterm']['year']; ?></font>
									</a>
								</li>
							<?php } ?>
						</ul>
					</div>
					
					</form>
					<br><br>
		<div id="no-more-tables">
			<table class="table table-bordered">
				<tr>
					<th style="text-align: center;" width="1%" rowspan="2"><font size="3px" >ลำดับ</font></th>
					<th style="text-align: center;" width="20%" colspan="12"><font size="3px">กระบวนวิชา</font></th>
				</tr>
				<tr>
					<th tyle="text-align: center;" style="text-align:center;" width="1%">รหัส</th>
					<th tyle="text-align: center;" style="text-align:center;" width="10%">ชื่อ</th>
					<th tyle="text-align: center;" style="text-align:center;" width="5%">หมวด</th>
					<th tyle="text-align: center;" style="text-align:center;" width="5%">วันเรียน</th>
					<th tyle="text-align: center;" style="text-align:center;" width="5%">เวลาเรียน</th>
					<th tyle="text-align: center;" style="text-align:center;" width="5%">ห้อง</th>
					<th tyle="text-align: center;" style="text-align:center;" width="5%">เดือน</th>
					<th tyle="text-align: center;" style="text-align:center;" width="5%">ปี</th>
					<th tyle="text-align: center;" style="text-align:center;" width="10%">อาจารย์ผู้สอน</th>
					<th tyle="text-align: center;" style="text-align:center;" width="10%">จำนวนผู้สมัครเข้าเรียนหลักสูตร</th>
					<?php if($admins != null){ ?>
					<th tyle="text-align: center;" style="text-align:center;" width="1%">แก้ไขข้อมูล</th>
					<th tyle="text-align: center;" style="text-align:center;" width="1%">ลบข้อมูล</th>
					<?php } ?>
				</tr>
				<tr>
					<td colspan="13">ไตรมาส 1</td>
				</tr>
				<?php
					$i = 0;
					foreach ($life_long_learnings1 as $life_long_learning){ $i++;?>
					<tr>
						
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['id']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['code']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['name']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['section']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['study_day']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['study_time']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['classroom']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['month']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['year']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['lecturer']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['total_student_apply']?></td>
							<?php if($admins != null){ ?>
							<td>
								<a class="btn btn-warning" href="<?php echo $this->Html->url(array('action'=>'edit_life_long_learning',$life_long_learning['LifeLongCurriculum']['id']));?>" role="button" target="_blank">แก้ไขข้อมูล</a>
							</td>
							<?php } ?>
							<?php if($admins != null){ ?>
							<td>
								<a class="btn btn-danger" href="<?php echo $this->Html->url(array('action'=>'remove_life_long_learning',$life_long_learning['LifeLongCurriculum']['id']));?>" role="button" onclick="return confirm('ยืนยังการลบข้อมูล')">ลบข้อมูล</a>
							</td>
							<?php } ?>
					</tr>
				<?php } ?>
				<tr>
					<td colspan="13">ไตรมาส 2</td>
				</tr>
				<?php
					$i = 0;
					foreach ($life_long_learnings2 as $life_long_learning){ $i++;?>
					<tr>
						
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['id']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['code']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['name']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['section']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['study_day']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['study_time']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['classroom']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['month']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['year']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['lecturer']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['total_student_apply']?></td>
							<?php if($admins != null){ ?>
							<td>
								<a class="btn btn-warning" href="<?php echo $this->Html->url(array('action'=>'edit_life_long_learning',$life_long_learning['LifeLongCurriculum']['id']));?>" role="button" target="_blank">แก้ไขข้อมูล</a>
							</td>
							<?php } ?>
							<?php if($admins != null){ ?>
							<td>
								<a class="btn btn-danger" href="<?php echo $this->Html->url(array('action'=>'remove_life_long_learning',$life_long_learning['LifeLongCurriculum']['id']));?>" role="button" onclick="return confirm('ยืนยังการลบข้อมูล')">ลบข้อมูล</a>
							</td>
							<?php } ?>
					</tr>
				<?php } ?>
				<tr>
					<td colspan="13">ไตรมาส 3</td>
				</tr>
				<?php
					$i = 0;
					foreach ($life_long_learnings3 as $life_long_learning){ $i++;?>
					<tr>
						
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['id']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['code']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['name']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['section']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['study_day']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['study_time']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['classroom']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['month']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['year']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['lecturer']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['total_student_apply']?></td>
							<?php if($admins != null){ ?>
							<td>
								<a class="btn btn-warning" href="<?php echo $this->Html->url(array('action'=>'edit_life_long_learning',$life_long_learning['LifeLongCurriculum']['id']));?>" role="button" target="_blank">แก้ไขข้อมูล</a>
							</td>
							<?php } ?>
							<?php if($admins != null){ ?>
							<td>
								<a class="btn btn-danger" href="<?php echo $this->Html->url(array('action'=>'remove_life_long_learning',$life_long_learning['LifeLongCurriculum']['id']));?>" role="button" onclick="return confirm('ยืนยังการลบข้อมูล')">ลบข้อมูล</a>
							</td>
							<?php } ?>
					</tr>
				<?php } ?>
				<tr>
					<td colspan="13">ไตรมาส 4</td>
				</tr>
				<?php
					$i = 0;
					foreach ($life_long_learnings4 as $life_long_learning){ $i++;?>
					<tr>
						
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['id']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['code']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['name']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['section']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['study_day']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['study_time']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['classroom']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['month']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['year']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['lecturer']?></td>
							<td align="center"><?php echo $life_long_learning['LifeLongCurriculum']['total_student_apply']?></td>
							<?php if($admins != null){ ?>
							<td>
								<a class="btn btn-warning" href="<?php echo $this->Html->url(array('action'=>'edit_life_long_learning',$life_long_learning['LifeLongCurriculum']['id']));?>" role="button" target="_blank">แก้ไขข้อมูล</a>
							</td>
							<?php } ?>
							<?php if($admins != null){ ?>
							<td>
								<a class="btn btn-danger" href="<?php echo $this->Html->url(array('action'=>'remove_life_long_learning',$life_long_learning['LifeLongCurriculum']['id']));?>" role="button" onclick="return confirm('ยืนยังการลบข้อมูล')">ลบข้อมูล</a>
							</td>
							<?php } ?>
					</tr>
				<?php } ?>
			</table>
			<div class="alert alert-info">
						KPI OWNER :กรศนันท์ สิทธิกุล
			</div>
		 
		</div>
	</div>
</div>