<?php
function DateThai($strDate)
{

	$strYear = date("Y", strtotime($strDate)) + 543;
	$strMonth = date("n", strtotime($strDate));
	$strDay = date("j", strtotime($strDate));
	$strHour = date("H", strtotime($strDate));
	$strMinute = date("i", strtotime($strDate));
	$strSeconds = date("s", strtotime($strDate));
	$strMonthCut = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
	$strMonthThai = $strMonthCut[$strMonth];
	return "$strDay $strMonthThai $strYear";
	//, $strHour:$strMinute
}
?>





<?php
date_default_timezone_set('Asia/Bangkok');
$date = date("Y-m-d");
$time = date("H:i:s");


function DateDiff($strDate1, $strDate2)
{
	return (strtotime($strDate2) - strtotime($strDate1)) /  (60 * 60 * 24);  // 1 day = 60*60*24
}

function TimeDiff($strTime1, $strTime2)
{
	return (strtotime($strTime2) - strtotime($strTime1)) /  (60 * 60); // 1 Hour =  60*60                           
}




?>
<div class="container">
	<h3>จำนวนรับนักศึกษาช่วยงานเกษตรแห่งชาติ 2567</h3>
	<div class="row">
		<div class="col-md-12">
			<form method="post" accept-charset="utf-8">
				<div class="panel-heading">
					<div class="btn-group">
						<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							วันที่
						</button>
						<div class="dropdown-menu">
							<a class="dropdown-item" href="#">Action</a>
							<a class="dropdown-item" href="#">Another action</a>
							<a class="dropdown-item" href="#">Something else here</a>
							<div class="dropdown-divider"></div>
							<a class="dropdown-item" href="#">Separated link</a>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
				<hr>
				<div class="panel-body">
					<table class="table table-bordered">
						<tr>
							<th style="text-align:center;" width="1%" colspan="" rowspan="2">ลำดับ</th>
							<th style="text-align:center;" width="20%" colspan="" rowspan="2">ชื่อฝ่าย</th>
							<!-- <th style="text-align:center;" width="10%" colspan="" rowspan="2">เบอร์โทรผู้ประสานงาน</th>
							<th style="text-align:center;" width="10%" colspan="" rowspan="2">ลิ้งค์ไลน์กลุ่ม (ถ้ามี)</th>
							<th style="text-align:center;" width="10%" colspan="" rowspan="2">คุณสมบัติเฉพาะ (ถ้ามี)</th> -->
							<th style="text-align:center;" width="5%" colspan="3" rowspan="">27 พ.ย. 67</th>
							<th style="text-align:center;" width="5%" colspan="3" rowspan="">28 พ.ย. 67</th>
							<th style="text-align:center;" width="5%" colspan="3" rowspan="">29 พ.ย. 67</th>
							<th style="text-align:center;" width="5%" colspan="3" rowspan="">30 พ.ย. 67</th>
							<th style="text-align:center;" width="5%" colspan="3" rowspan="">1 ธ.ค. 67</th>
							<th style="text-align:center;" width="5%" colspan="3" rowspan="">2 ธ.ค. 67</th>
							<th style="text-align:center;" width="5%" colspan="3" rowspan="">3 ธ.ค. 67</th>
							<th style="text-align:center;" width="5%" colspan="3" rowspan="">4 ธ.ค. 67</th>
							<th style="text-align:center;" width="5%" colspan="3" rowspan="">5 ธ.ค. 67</th>
							<th style="text-align:center;" width="5%" colspan="3" rowspan="">6 ธ.ค. 67</th>
							<th style="text-align:center;" width="5%" colspan="3" rowspan="">7 ธ.ค. 67</th>
							<th style="text-align:center;" width="5%" colspan="3" rowspan="">8 ธ.ค. 67</th>
						</tr>
						<tr>
							<th style="text-align:center;" width="" colspan="" rowspan="">เช้า</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">บ่าย</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">รวม</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">เช้า</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">บ่าย</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">รวม</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">เช้า</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">บ่าย</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">รวม</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">เช้า</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">บ่าย</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">รวม</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">เช้า</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">บ่าย</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">รวม</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">เช้า</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">บ่าย</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">รวม</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">เช้า</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">บ่าย</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">รวม</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">เช้า</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">บ่าย</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">รวม</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">เช้า</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">บ่าย</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">รวม</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">เช้า</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">บ่าย</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">รวม</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">เช้า</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">บ่าย</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">รวม</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">เช้า</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">บ่าย</th>
							<th style="text-align:center;" width="" colspan="" rowspan="">รวม</th>
						</tr>
						<!-- ลำดับที่ 1 -->
						<tr>
							<td style="text-align:center;">1</td>
							<td style="text-align:center;">ฝ่ายจัดหารายได้</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.1_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.1_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.1_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.21_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.21_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.21_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.41_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.41_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.41_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.61_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.61_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.61_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.81_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.81_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.81_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.101_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.101_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.101_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.121_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.121_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.121_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.141_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.141_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.141_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.161_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.161_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.161_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.181_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.181_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.181_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.201_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.201_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.201_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.221_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.221_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.221_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
						</tr>
						<!-- ลำดับที่ 2 -->
						<tr>
							<td style="text-align:center;">2</td>
							<td style="text-align:center;">ฝ่ายพิธีการและต้อนรับ</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.2_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.2_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.2_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.22_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.22_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.22_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.42_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.42_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.42_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.62_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.62_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.62_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.82_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.82_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.82_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.102_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.102_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.102_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.122_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.122_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.122_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.142_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.142_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.142_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.162_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.162_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.162_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.182_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.182_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.182_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.202_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.202_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.202_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.222_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.222_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.222_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
						</tr>
						<!-- ลำดับที่ 3 -->
						<tr>
							<td style="text-align:center;">3</td>
							<td style="text-align:center;">ฝ่ายประชุม เสวนาวิชาการและนิทรรศการนานาชาติ</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.3_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.3_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.3_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.23_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.23_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.23_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.43_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.43_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.43_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.63_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.63_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.63_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.83_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.83_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.83_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.103_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.103_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.103_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.123_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.123_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.123_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.143_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.143_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.143_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.163_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.163_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.163_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.183_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.183_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.183_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.203_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.203_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.203_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.223_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.223_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.223_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
						</tr>
						<!-- ลำดับที่ 4 -->
						<tr>
							<td style="text-align:center;">4</td>
							<td style="text-align:center;">ฝ่ายนิทรรศการเอกชนด้านนวัตกรรมเทคโนโลยีเกษตรสมัยใหม่</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.4_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.4_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.4_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.24_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.24_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.24_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.44_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.44_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.44_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.64_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.64_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.64_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.84_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.84_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.84_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.104_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.104_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.104_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.124_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.124_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.124_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.144_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.144_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.144_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.164_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.164_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.164_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.184_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.184_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.184_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.204_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.204_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.204_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.224_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.224_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.224_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
						</tr>
						<!-- ลำดับที่ 5 -->
						<tr>
							<td style="text-align:center;">5</td>
							<td style="text-align:center;">"ฝ่ายนิทรรศการคณะเกษตรศาสตร์และหน่วยงานในมหาวิทยาลัยเชียงใหม่"</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.5_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.5_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.5_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.25_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.25_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.25_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.45_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.45_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.45_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.65_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.65_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.65_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.85_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.85_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.85_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.105_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.105_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.105_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.125_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.125_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.125_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.145_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.145_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.145_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.165_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.165_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.165_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.185_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.185_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.185_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.205_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.205_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.205_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.225_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.225_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.225_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
						</tr>
						<!-- ลำดับที่ 6 -->
						<tr>
							<td style="text-align:center;">6</td>
							<td style="text-align:center;">ฝ่ายนิทรรศการส่วนราชการ รัฐวิสาหกิจ ฯ</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.6_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.6_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.6_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.26_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.26_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.26_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.46_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.46_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.46_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.66_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.66_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.66_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.86_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.86_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.86_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.106_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.106_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.106_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.126_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.126_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.126_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.146_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.146_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.146_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.166_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.166_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.166_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.186_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.186_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.186_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.206_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.206_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.206_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.226_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.226_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.226_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
						</tr>
						<!-- ลำดับที่ 7 -->
						<tr>
							<td style="text-align:center;">7</td>
							<td style="text-align:center;">ฝ่ายนิทรรศการองค์กรอิสระและเครือข่ายเกษตรกร</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.7_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.7_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.7_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.27_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.27_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.27_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.47_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.47_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.47_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.67_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.67_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.67_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.87_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.87_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.87_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.107_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.107_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.107_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.127_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.127_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.127_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.147_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.147_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.147_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.167_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.167_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.167_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.187_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.187_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.187_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.207_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.207_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.207_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.227_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.227_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.227_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
						</tr>
						<!-- ลำดับที่ 8 -->
						<tr>
							<td style="text-align:center;">8</td>
							<td style="text-align:center;">ฝ่ายฝึกอบรมวิชาชีพระยะสั้น</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.8_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.8_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.8_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.28_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.28_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.28_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.48_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.48_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.48_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.68_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.68_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.68_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.88_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.88_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.88_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.108_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.108_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.108_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.128_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.128_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.128_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.148_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.148_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.148_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.16_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.16_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.16_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.188_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.188_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.188_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.208_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.208_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.208_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.228_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.228_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.228_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
						</tr>
						<!-- ลำดับที่ 9 -->
						<tr>
							<td style="text-align:center;">9</td>
							<td style="text-align:center;">ฝ่ายประกวดพืช</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.9_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.9_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.9_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.29_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.29_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.29_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.49_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.49_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.49_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.69_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.69_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.69_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.89_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.89_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.89_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.109_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.109_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.109_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.129_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.129_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.129_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.149_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.149_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.149_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.169_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.169_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.169_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.189_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.189_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.189_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.209_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.209_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.209_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.229_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.229_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.229_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
						</tr>
						<!-- ลำดับที่ 10 -->
						<tr>
							<td style="text-align:center;">10</td>
							<td style="text-align:center;">ฝ่ายประกวดสัตว์</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.10_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.10_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.10_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.30_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.30_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.30_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.50_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.50_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.50_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.70_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.70_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.70_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.90_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.90_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.90_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.110_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.110_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.110_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.130_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.130_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.130_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.150_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.150_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.150_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.170_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.170_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.170_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.190_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.190_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.190_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.210_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.210_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.210_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.230_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.230_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.230_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
						</tr>
						<!-- ลำดับที่ 11 -->
						<tr>
							<td style="text-align:center;">11</td>
							<td style="text-align:center;">"ฝ่ายกิจกรรมนักเรียน นักศึกษา และกิจกรรมศิษย์เก่า - มัคคุเทศก์เกษตร"</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.11_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.11_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.11_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.31_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.31_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.31_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.51_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.51_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.51_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.71_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.71_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.71_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.91_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.91_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.91_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.111_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.111_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.111_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.121_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.121_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.121_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.151_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.151_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.151_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.171_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.171_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.171_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.191_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.191_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.191_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.211_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.211_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.211_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.231_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.231_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.231_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
						</tr>
						<!-- ลำดับที่ 12 -->
						<tr>
							<td style="text-align:center;">12</td>
							<td style="text-align:center;">ฝ่ายนันทนาการและบันเทิง</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.12_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.12_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.12_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.32_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.32_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.32_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.52_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.52_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.52_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.72_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.72_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.72_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.92_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.92_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.92_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.112_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.112_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.112_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.132_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.132_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.132_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.152_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.152_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.152_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.172_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.172_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.172_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.192_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.192_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.192_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.212_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.212_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.212_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.232_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.232_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.232_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
						</tr>
						<!-- ลำดับที่ 13 -->
						<tr>
							<td style="text-align:center;">13</td>
							<td style="text-align:center;">ฝ่ายร้านค้าเกษตร ตลาดนัดสินค้าและมหกรรมอาหาร</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.13_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.13_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.13_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.33_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.33_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.33_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.53_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.53_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.53_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.73_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.73_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.73_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.93_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.93_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.93_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.113_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.113_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.113_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.133_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.133_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.133_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.153_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.153_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.153_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.173_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.173_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.173_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.193_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.193_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.193_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.213_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.213_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.213_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.233_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.233_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.233_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
						</tr>
						<!-- ลำดับที่ 14 -->
						<tr>
							<td style="text-align:center;">14</td>
							<td style="text-align:center;">ฝ่ายประชาสัมพันธ์</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.14_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.14_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.14_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.34_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.34_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.34_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.54_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.54_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.54_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.74_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.74_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.74_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.94_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.94_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.94_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.114_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.114_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.114_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.134_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.134_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.134_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.154_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.154_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.154_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.174_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.174_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.174_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.194_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.194_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.194_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.214_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.214_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.214_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.234_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.234_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.234_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
						</tr>
						<!-- ลำดับที่ 15 -->
						<tr>
							<td style="text-align:center;">15</td>
							<td style="text-align:center;">ฝ่ายสถานที่ ภูมิทัศน์ รักษาความสะอาดยานพาหนะ จราจร รักษาความปลอดภัย</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.15_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.15_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.15_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.35_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.35_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.35_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.55_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.55_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.55_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.75_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.75_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.75_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.95_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.95_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.95_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.115_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.115_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.115_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.135_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.135_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.135_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.155_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.155_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.155_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.175_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.175_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.175_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.195_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.195_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.195_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.215_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.215_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.215_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.235_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.235_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.235_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
						</tr>
						<!-- ลำดับที่ 16 -->
						<tr>
							<td style="text-align:center;">16</td>
							<td style="text-align:center;">ฝ่ายแปลงสาธิต</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.16_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.16_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.16_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.36_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.36_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.36_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.56_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.56_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.56_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.76_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.76_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.76_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.96_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.96_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.96_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.116_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.116_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.116_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.136_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.136_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.136_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.156_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.156_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.156_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.176_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.176_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.176_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.196_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.196_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.196_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.216_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.216_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.216_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.236_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.236_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.236_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
						</tr>
						<!-- ลำดับที่ 17 -->
						<tr>
							<td style="text-align:center;">17</td>
							<td style="text-align:center;">ฝ่ายสวัสดิการและพยาบาล</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.17_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.17_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.17_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.37_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.37_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.37_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.57_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.57_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.57_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.77_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.77_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.77_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.97_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.97_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.97_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.117_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.117_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.117_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.137_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.137_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.137_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.157_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.157_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.157_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.177_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.177_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.177_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.197_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.197_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.197_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.217_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.217_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.217_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.237_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.237_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.237_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
						</tr>
						<!-- ลำดับที่ 18 -->
						<tr>
							<td style="text-align:center;">18</td>
							<td style="text-align:center;">ฝ่ายประเมินผล</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.18_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.18_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.18_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.38_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.38_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.38_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.58_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.58_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.58_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.78_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.78_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.78_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.98_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.98_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.98_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.118_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.118_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.118_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.138_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.138_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.138_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.158_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.158_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.158_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.178_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.178_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.178_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.198_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.198_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.198_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.218_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.218_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.218_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.238_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.238_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.238_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
						</tr>
						<!-- ลำดับที่ 19 -->
						<tr>
							<td style="text-align:center;">19</td>
							<td style="text-align:center;">ฝ่ายการเงิน</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.19_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.19_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.19_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.39_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.39_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.39_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.59_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.59_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.59_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.79_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.79_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.79_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.99_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.99_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.99_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.119_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.119_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.119_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.139_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.139_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.139_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.159_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.159_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.159_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.179_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.179_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.179_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.199_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.199_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.199_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.219_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.219_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.219_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.239_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.239_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.239_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
						</tr>
						<!-- ลำดับที่ 20 -->
						<tr>
							<td style="text-align:center;">20</td>
							<td style="text-align:center;">ฝ่ายเลขานุการและประสานงาน</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.20_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.20_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.20_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.40_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.40_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.40_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.60_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.60_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.60_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.80_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.80_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.80_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.100_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.100_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.100_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.120_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.120_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.120_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.140_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.140_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.140_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.160_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.160_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.160_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.180_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.180_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.180_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.200_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.200_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.200_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.220_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.220_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.220_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.240_1',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.240_2',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
							<td style="text-align:center;">
								<?php
									echo $this->Form->input('StudentAssistantReceive.240_3',array(
										'type' => 'text', 'label' => false,
									));
								?>
							</td>
						</tr>
					</table>
				</div>
			</form>
			