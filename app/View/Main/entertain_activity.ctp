<div class="container" style="padding:15px 0 0;">
<div class="panel panel-default" style="border-color: #F2F2F2!important">
	<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center><div class="font_panel2">กิจกรรมนันทนาการและบันเทิง</div></center>
				</h3> 
			</div>
		</div>	
		<div class="panel-body" style="padding-top: 15px; ">
			<div align="center"> 	
				<h4>กำหนดการกิจกรรมนันทนาการและบันเทิง</h4>
				<h4>ในงานเกษตรภาคเหนือ ครั้งที่ 8 “เกษตรทันสมัย เศรษฐกิจก้าวไกล ใส่ใจชุมชน”</h4>
				<h4>ในระหว่างวันที่ 8-12 พฤศจิกายน 2560</h4>
				<br>
				<h4>ขณะนี้จำนวนผู้สมัครร้องเพลงไทยลูกทุ่งเต็มแล้ว หากประสงค์ต้องการลงทะเบียนเหลือประเภทบุคลากรเท่านั้น ติดต่อ ณ จุดบริการเวทีลานเกษตรรวมใจ</h4>
			</div> 
			<a class="btn btn-primary btn-lg btn-block" href="<?php echo $this->Html->url('/files/กำหนดการและกิจกรรมการแสดงประจำเว.pdf'); ?>" role="button" target="_blank">
				<span class="glyphicon glyphicon-download" aria-hidden="true"></span>
				กำหนดการ
			</a>
			<br>
			<div id="no-more-tables"> 				
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th style="text-align: center;">ลำดับ</th>
							<th style="text-align: center;">ชื่อกิจกรรม</th>
							<th style="text-align: center;">วันที่/เวลา</th>
							<th style="text-align: center;">สถานที่</th>
							<th style="text-align: center;">ลงทะเบียน</th>
							<th style="text-align: center;">ตรวจสอบรายชื่อ</th>
							<th style="text-align: center;">ปริ้นท์รายชื่อ </th>
						</tr>
					</thead>
					<tbody>
						<?php
							$i = 0;
							foreach ($projects as $project){
								$i++;
							?>
								<tr>
									<td data-title="ลำดับ" align="center"><?php echo $i; ?></td>
									<td data-title="ชื่อกิจกรรม">
										<?php 
										if($project['Project']['file_name'] != null){
										?>
											<a href="<?php echo $this->Html->url('/files/'.$project['Project']['file_name']); ?>" target="_blank">
												<?php echo $project['Project']['name']; ?>
											</a>
										<?php 
											
										}
										else{
											echo $project['Project']['name']; 
										}
										
											
										?>
										
									</td>
									<td data-title="วันที่/เวลา"><?php echo $project['Project']['date']; ?><br><?php echo $project['Project']['time']; ?></td>
									<td data-title="สถานที่">
										<?php echo $project['Project']['place']; ?>
									</td>
									<td data-title="ลงทะเบียน" style="text-align: center;">
									<?php
										if($project['Project']['id'] == 6){
										?>
										
											<button class="btn btn-danger" style="cursor: none;">
												<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 
												เต็ม						
											</button>
										
										<?php
										}elseif ($project['Project']['id'] == 36 || $project['Project']['id'] == 37 || $project['Project']['id'] == 38) {
											echo '<button class="btn btn-danger" style="cursor: none;">
											<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 
											ปิดรับการลงทะเบียน						
										</button>';
										}
										else{
										?>								
										<a class="btn btn-primary" href="<?php echo $this->Html->url(array('action' => 'register',$project['Project']['id'])); ?>" role="button" target="_blank">ลงทะเบียน</a>					
										<?php	
										}
										?>
									</td>
									<td data-title="ตรวจสอบรายชื่อ" style="text-align: center;">								
										<a class="btn btn-success" href="<?php echo $this->Html->url(array('action' => 'checklist',$project['Project']['id'])); ?>" role="button" >ตรวจสอบรายชื่อ</a>								
									</td>
									<td data-title="ปริ้นท์รายชื่อ" style="text-align: center;">
										<a class="btn btn-warning pull-right" href="<?php echo $this->Html->url(array('action' => 'ListPrint',$project['Project']['id'])); ?>"
											role="button" target="_blank"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Print หน้าลงทะเบียน</a>
									</td>
								</tr>
							<?php
							}
						?>						
						
					</tbody>
				</table>
			</div>
	
		</div>

	</div>
	
	<div class="row">
		<div class="col-md-6">
					
			

		</div>
		<div class="col-md-6">	
			<div class="alert alert-info">
				ติดต่อสอบถามเพิ่มเติมได้ที่ นางวนิดา พวงพยอม
				<br>
				หน่วยสารบรรณ คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่ โทรศัพท์ 053 - 944009 ต่อ 105  (ในวันเวลาราชการ)
			</div>
		</div>
	</div>
	
</div>
