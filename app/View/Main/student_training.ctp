<?php
function DateThai($strDate)
{

	$strYear = date("Y", strtotime($strDate)) + 543;
	$strMonth = date("n", strtotime($strDate));
	$strDay = date("j", strtotime($strDate));
	$strHour = date("H", strtotime($strDate));
	$strMinute = date("i", strtotime($strDate));
	$strSeconds = date("s", strtotime($strDate));
	$strMonthCut = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
	$strMonthThai = $strMonthCut[$strMonth];
	return "$strDay $strMonthThai $strYear";
	//, $strHour:$strMinute
}
?>





<?php
date_default_timezone_set('Asia/Bangkok');
$date = date("Y-m-d");
$time = date("H:i:s");


function DateDiff($strDate1, $strDate2)
{
	return (strtotime($strDate2) - strtotime($strDate1)) /  (60 * 60 * 24);  // 1 day = 60*60*24
}

function TimeDiff($strTime1, $strTime2)
{
	return (strtotime($strTime2) - strtotime($strTime1)) /  (60 * 60); // 1 Hour =  60*60                           
}




?>
<div class="container-fluid" style="padding:15px 0 0;">
	<div class="panel panel-default" style="border-color: #F2F2F2!important">
		<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center>
						<div class="font_panel2">จำนวนผู้เข้ารับการถ่ายทอดความรู้และเรียนรู้ ผ่านการเข้าร่วมกิจกรรม/ฝึกอบรม/แหล่งเรียนรู้ ด้านการเรียนการสอน (ปีการศึกษา <?php echo $lastyearterms['Yearterm']['year'] ?>)</div>
					</center>
				</h3>
				<?php if($admins != null) { ?>
				<a class="btn btn-success" role="button" target="_blank"
				href="<?php echo $this->Html->url(array('action'=>'add_student_training'));?>">
					เพิ่มข้อมูล</a>
					<?php } ?>
					กรุณาเลือกปีการศึกษา
				<form method="get">
					<div class="dropdown">
						<button class="btn btn-default btn-info dropdown-toggle" type="button" id="dropdownMenu" data-toggle="dropdown" aria-haspopup="true" aira-expanded="false">
							<?php echo $years['Yearterm']['year']; ?>
						</button>
						<ul class="dropdown-menu" aria-labelleby="dropdownMenu1">
							<?php foreach($dropdowns as $dropdown) { ?>
								<?php
									if($years['Yearterm']['year'] == $dropdown['Yearterm']['year'])
								{
									$active = "active";
								}
								else
								{
									$active = "";
								}
								?>
								<li class="<?php echo $active; ?>">
									<a href="<?php echo $this->Html->url(array('action'=>'student_training',$dropdown['Yearterm']['year'])); ?>">
										<font color="black"><?php echo $dropdown['Yearterm']['year']; ?></font>
									</a>
								</li>
								<?php } ?>
						</ul>
					</div>
				</form>
			</div>
		</div>
		<div>
			<table class="table table-bordered">
				<thead>
					<tr>
						<td align="center" width="1%" style="background-color:#ededed;">ลำดับ</td>
						<td align="center" width="49%" style="background-color:#ededed;">กิจกรรม</td>
						<td align="center" width="10%" style="background-color:#ededed;">วันที่จัดกิจกรรม</td>
						<td align="center" width="10%" style="background-color:#ededed;">เดือนที่เก็บข้อมูล</td>
						<td align="center" width="10%" style="background-color:#ededed;">ปีที่เก็บข้อมูล</td>
						<td align="center" width="10%" style="background-color:#ededed;">จำนวนผู้เข้าร่วม</td>
						<?php if($admins != null) { ?>
						<td align="center" width="1%" style="background-color:#ededed;">แก้ไขข้อมูล</td>
						<td align="center" width="1%" style="background-color:#ededed;">ลบข้อมูล</td>
						<?php } ?>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td colspan="8">ไตรมาส 1</td>
					</tr>
					<?php
						$i = 0;
						$total1 = 0;
							foreach ($student_trainings1 as $student_training){ $i++;?>
						<tr>
							<td align="center"><?php echo $student_training['StudentTraining']['id'];?></td>
							<td><?php echo $student_training['StudentTraining']['name'];?></td>
							<td align="center"><?php echo $student_training['StudentTraining']['postdate'];?></td>
							<td align="center"><?php echo $student_training['StudentTraining']['month'];?></td>
							<td align="center"><?php echo $student_training['StudentTraining']['year'];?></td>
							<td align="center">
								<?php
									$total1 = $total1+$student_training['StudentTraining']['count_student'];
									echo $student_training['StudentTraining']['count_student'];
								?>
							</td>
							<?php if($admins != null) { ?>
							<td><a href="<?php echo $this->Html->url(array('action' =>'edit_student_training',$student_training['StudentTraining']['id'])); ?>" class="btn btn-warning" role="button" target="_blank">แก้ไขข้อมูล</a></td>
							<td><a href="<?php echo $this->Html->url(array('action'=>'remove_student_training',$student_training['StudentTraining']['id'])); ?>" class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')">ลบข้อมูล</a></td>
							<?php } ?>
						</tr>
					<?php } ?>
					<tr>
						<td colspan="8">ไตรมาส 2</td>
					</tr>
					<?php
						$i = 0;
						$total2 = 0;
							foreach ($student_trainings2 as $student_training){ $i++;?>
						<tr>
							<td align="center"><?php echo $student_training['StudentTraining']['id'];?></td>
							<td><?php echo $student_training['StudentTraining']['name'];?></td>
							<td align="center"><?php echo $student_training['StudentTraining']['postdate'];?></td>
							<td align="center"><?php echo $student_training['StudentTraining']['month'];?></td>
							<td align="center"><?php echo $student_training['StudentTraining']['year'];?></td>
							<td align="center">
								<?php
									$total2 = $total2+$student_training['StudentTraining']['count_student'];
									echo $student_training['StudentTraining']['count_student'];
								?>
							</td>
							<?php if($admins != null) { ?>
							<td><a href="<?php echo $this->Html->url(array('action' =>'edit_student_training',$student_training['StudentTraining']['id'])); ?>" class="btn btn-warning" role="button" target="_blank">แก้ไขข้อมูล</a></td>
							<td><a href="<?php echo $this->Html->url(array('action'=>'remove_student_training',$student_training['StudentTraining']['id'])); ?>" class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')">ลบข้อมูล</a></td>
							<?php } ?>
						</tr>
					<?php } ?>
					<tr>
						<td colspan="8">ไตรมาส 3</td>
					</tr>
					<?php
						$i = 0;
						$total3 = 0;
							foreach ($student_trainings3 as $student_training){ $i++;?>
						<tr>
							<td align="center"><?php echo $student_training['StudentTraining']['id'];?></td>
							<td><?php echo $student_training['StudentTraining']['name'];?></td>
							<td align="center"><?php echo $student_training['StudentTraining']['postdate'];?></td>
							<td align="center"><?php echo $student_training['StudentTraining']['month'];?></td>
							<td align="center"><?php echo $student_training['StudentTraining']['year'];?></td>
							<td align="center">
								<?php
									$total3 = $total3+$student_training['StudentTraining']['count_student'];
									echo $student_training['StudentTraining']['count_student'];
								?>
							</td>
							<?php if($admins != null) { ?>
							<td><a href="<?php echo $this->Html->url(array('action' =>'edit_student_training',$student_training['StudentTraining']['id'])); ?>" class="btn btn-warning" role="button" target="_blank">แก้ไขข้อมูล</a></td>
							<td><a href="<?php echo $this->Html->url(array('action'=>'remove_student_training',$student_training['StudentTraining']['id'])); ?>" class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')">ลบข้อมูล</a></td>
							<?php } ?>
					<?php } ?>
					<tr>
						<td colspan="8">ไตรมาส 4</td>
					</tr>
					<?php
						$i = 0;
						$total4 = 0;
							foreach ($student_trainings4 as $student_training){ $i++;?>
						<tr>
							<td align="center"><?php echo $student_training['StudentTraining']['id'];?></td>
							<td><?php echo $student_training['StudentTraining']['name'];?></td>
							<td align="center"><?php echo $student_training['StudentTraining']['postdate'];?></td>
							<td align="center"><?php echo $student_training['StudentTraining']['month'];?></td>
							<td align="center"><?php echo $student_training['StudentTraining']['year'];?></td>
							<td align="center">
								<?php
									$total4 = $total4+$student_training['StudentTraining']['count_student'];
									echo $student_training['StudentTraining']['count_student'];
								?>
							</td>
							<?php if($admins != null) { ?>
							<td><a href="<?php echo $this->Html->url(array('action' =>'edit_student_training',$student_training['StudentTraining']['id'])); ?>" class="btn btn-warning" role="button" target="_blank">แก้ไขข้อมูล</a></td>
							<td><a href="<?php echo $this->Html->url(array('action'=>'remove_student_training',$student_training['StudentTraining']['id'])); ?>" class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')">ลบข้อมูล</a></td>
							<?php } ?>
						</tr>
					<?php } ?>
					<tr>
						<td colspan="5" align="center" style="background-color:#ededed;">รวมจำนวนผู้เข้าร่วม</td>
						<td align="center" style="background-color:#ededed;"><?php echo ($total1+$total2+$total3+$total4);?></td>
					</tr>
				</tbody>
			</table>
		</div>
			<div class="alert alert-info">
				KPI OWNER :ศิริขวัญ ใจสม
			</div>
	</div>
</div>