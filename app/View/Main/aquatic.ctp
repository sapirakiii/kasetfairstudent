<?php //debug($projects);?>
<div class="container" style="padding:15px 0 0;">
<div class="panel panel-default" style="border-color: #F2F2F2!important">
	<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center><div class="font_panel2">การประกวดสัตว์น้ำ</div></center>
				</h3> 
			</div>
		</div>	
		<div class="panel-body" style="padding-top: 15px; ">
			<div align="center"> 	
				<h4>การประกวดสัตว์น้ำ ในงานเกษตรภาคเหนือ ครั้งที่ 8</h4>
				<h4>ระหว่างวันที่ 8-12 พฤศจิกายน 2560</h4>
				<br>
			</div> 
			<div id="no-more-tables"> 				
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th style="text-align: center;">ลำดับ</th>
							<th style="text-align: center;">หัวข้อ</th>
							<th style="text-align: center;" width="80px">วัน เดือน ปี</th>
							<th style="text-align: center;">เวลา</th>
							<th style="text-align: center;">สถานที่</th>
						
							<!-- <th style="text-align: center;">รับจำนวน</th>
							<th style="text-align: center;">ค่าลงทะเบียน</th> -->
							<th style="text-align: center;">ลงทะเบียน</th>
							<th style="text-align: center;">ตรวจสอบรายชื่อ</th>
							<th style="text-align: center;">ปริ้นท์รายชื่อ  </th>
						</tr>
					</thead>
					<tbody>
						
						<?php
							//debug($countMembersList);
							
							$i = 0;
							foreach ($projects as $project){
								$i++;
							?>
								<tr>
									<td data-title="ลำดับ" align="center"><?php echo $i; ?></td>
									<td data-title="หัวข้อ">
									<?php 
										if($project['Project']['file_name'] != null){
										?>
											<a href="<?php echo $this->Html->url('/files/'.$project['Project']['file_name']); ?>" target="_blank">
												<?php echo $project['Project']['name']; ?>
											</a>
										<?php 
											
										}
										else{
											echo $project['Project']['name']; 
										}
										
											
										?>
										
									</td>
									<td data-title="วัน เดือน ปี"><?php echo $project['Project']['date']; ?></td>
									<td data-title="เวลา"><?php echo $project['Project']['time']; ?></td>
									<td data-title="สถานที่"><?php echo $project['Project']['place']; ?></td>
									<!-- <td data-title="วิทยากร">
										<?php echo $project['Project']['lecturer']; ?>
									</td> -->
									<!-- <td data-title="รับจำนวน">
										<?php echo $project['Project']['max'].' ตัว'; ?>
									</td>
									<td data-title="ค่าลงทะเบียน">
										<?php 
										if($project['Project']['price'] == 0){
											echo 'ฟรี'; 
										}
										else{
											echo $project['Project']['price'].' บาท'; 
										}
										
										?>
									</td> -->
									<?php 
										if($project['Project']['id'] == 42){
										?>
										<td data-title="ลงทะเบียน" style="text-align: center;">	
											<button class="btn btn-danger" style="cursor: none;">
															<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 
															ปิดการลงทะเบียน								
											</button>
										</td>
								<?php 
										}else { ?>
											
										<td data-title="ลงทะเบียน" style="text-align: center;">		
										<button class="btn btn-danger" style="cursor: none;">
															<span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 
															ปิดการลงทะเบียน						
											</button>						
											<!-- <a class="btn btn-primary" href="<?php echo $this->Html->url(array('action' => 'registeraquatic',$project['Project']['id'])); ?>" role="button" target="_blank">ลงทะเบียน</a>					 -->
										</td>
									
										<?php 	}	?>
									<td data-title="ตรวจสอบรายชื่อ" style="text-align: center;">								
										<a class="btn btn-success" href="<?php echo $this->Html->url(array('action' => 'checklistaquatic',$project['Project']['id'])); ?>" role="button" target="_blank">ตรวจสอบรายชื่อ</a>								
									</td>
									<td>
										<a class="btn btn-warning pull-right" href="<?php echo $this->Html->url(array('action' => 'ListPrint',$project['Project']['id'])); ?>"
											role="button" target="_blank"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> Print หน้าลงทะเบียน</a>
									</td>
								</tr>
							<?php
							}
						?>						
						
					</tbody>
				</table>
			</div>
	
		</div>

	</div>
	
	<div class="row">
		<div class="col-md-6">
					
			<div class="alert alert-warning">
				<span class="glyphicon glyphicon-stop" aria-hidden="true"></span>
				สำหรับการแข่งขันประกวดปลากัด รับสมัครทางระบบออนไลน์และชำระค่าสมัครหน้างาน วันที่ 6 พย. ตั้งแต่เวลา 9:00-18:00 น.
				<br><br>
				<span class="glyphicon glyphicon-stop" aria-hidden="true"></span>
				รับสมัครตั้งแต่วันนี้ - วันอังคารที่ 31 ตุลาคม 2560  หรือจนกว่าตู้ที่จัดไว้จะเต็ม

				<br><br>
				<!-- <div class="alert alert-danger">
					<span class="glyphicon glyphicon-stop" aria-hidden="true"></span>
					ผู้สมัครชำระค่าลงทะเบียนได้จนถึงวันที่ 25 ตุลาคม 2560 เวลา 16.00 น. หากพ้นกำหนดดังกล่าว ฝ่ายฝึกอบรมวิชาชีพระยะสั้น ขอให้สิทธิการลงทะเบียนสำหรับท่านอื่นที่จองไว้เเต่ไม่สามารถลงทะเบียนได้ต่อไป โดยไม่ต้องเเจ้งล่วงหน้า
				</div> -->
			</div>

		</div>
		<div class="col-md-6">	
			<div class="alert alert-info">
				ติดต่อสอบถามเพิ่มเติม  <br>
				ประเภทการแข่งขันประกวดปลากัด   <br>
				ติดต่อ  คุณจักรินทร์  ใหม่วัน   โทร. 083-948-6773 <br> <br>
				ประเภทการแข่งขันปลาหมอครอสบรีด   <br>
				ติดต่อ  คุณเอนก. บุญแก้ว.  โทร.084-040-0738 <br> <br>	
				ประเภทการแข่งขันสัตว์น้ำสวยงาม  (กุ้งเครฟิชสวยงาม) <br>
				ติดต่อ  ณัฐวุฒิ วงศ์วิชัยแก้ว  โทร. 082-895-8056
		
			</div>
		</div>
	</div>
	
</div>