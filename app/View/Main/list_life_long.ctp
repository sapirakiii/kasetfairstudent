<?php
function DateThai($strDate)
{

	$strYear = date("Y", strtotime($strDate)) + 543;
	$strMonth = date("n", strtotime($strDate));
	$strDay = date("j", strtotime($strDate));
	$strHour = date("H", strtotime($strDate));
	$strMinute = date("i", strtotime($strDate));
	$strSeconds = date("s", strtotime($strDate));
	$strMonthCut = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
	$strMonthThai = $strMonthCut[$strMonth];
	return "$strDay $strMonthThai $strYear";
	//, $strHour:$strMinute
}
?>





<?php
date_default_timezone_set('Asia/Bangkok');
$date = date("Y-m-d");
$time = date("H:i:s");


function DateDiff($strDate1, $strDate2)
{
	return (strtotime($strDate2) - strtotime($strDate1)) /  (60 * 60 * 24);  // 1 day = 60*60*24
}

function TimeDiff($strTime1, $strTime2)
{
	return (strtotime($strTime2) - strtotime($strTime1)) /  (60 * 60); // 1 Hour =  60*60                           
}




?>
<div class="container-fluid" style="padding:15px 0 0;">
	<!-- Dropdown -->
<!-- <div class="dropdown">
  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Dropdown button
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="#">Action</a>
    <a class="dropdown-item" href="#">Another action</a>
    <a class="dropdown-item" href="#">Something else here</a>
  </div>
</div> -->
				<center>
					<h3>หลักสูตร Life Long Learning (ปีการศึกษา <?php echo $lastyearterms['Yearterm']['year'] ?>)</h3>
				</center>
				<br>

				<div id="no-more-tables">
				<?php if($admins != null){ ?>
					<a class="btn btn-success" 
						href="<?php echo $this->Html->url(array('action' => 'add_life_long')); ?>" role="button" target="_blank">
						เพิ่มข้อมูล
					</a>
					<?php } ?>
					กรุณาเลือกปีการศึกษา
				<form name="frmselect" method="get">
					<div class="dropdown"  style="float: left;">
						<button class="btn btn-default dropdown-toggle btn-info" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<?php echo $years['Yearterm']['year']; ?>
						</button>
						<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
							<?php foreach ($dropdowns as $dropdown) { ?>
								<?php 
									if($years['Yearterm']['year'] == $dropdown['Yearterm']['year']){
										$active = "active";
									}else{
										$active = "";
									}
									// $yearList = $dropdown['Yearterm']['year'];
								?>
								<li class="<?php echo $active;?>">
									<a href="<?php echo $this->Html->url(array('action' => 'list_life_long',$dropdown['Yearterm']['year'])); ?>">
										<font color="black"><?php echo $dropdown['Yearterm']['year']; ?></font>
									</a>
								</li>
							<?php } ?>
						</ul>
					</div>
					
					</form>	
					<br><br>
					<table class="table table-bordered">
						<thead>
							<tr>
								<th style="text-align: center;" width="1%"><font size="3px">ลำดับ</font></th>
								<th style="text-align: center;" width="40%"><font size="3px">ชื่อหลักสูตร</font></th>
								<th style="text-align: center;" width="5%"><font size="3px">ประเภท</font></th>
								<th style="text-align: center;" width="5%"><font size="3px">รหัส</font></th>
								<th style="text-align: center;" width="30%"><font size="3px">เว็บไซต์</font></th>
								<th style="text-align: center;" width="10%"><font size="3px">จำนวนผู้เรียนในหลักสูตร</font></th>
								<th style="text-align: center;" width="5%"><font size="3px">เดือนที่เก็บข้อมูล</font></th>
								<th style="text-align: center;" width="5%"><font size="3px">ปีการศึกษาที่เก็บข้อมูล</font></th>
								<?php if($admins != null){ ?>
								<th style="text-align: center;" width="5%"><font size="3px">แก้ไขข้อมูล</font></th>
								<th style="text-align: center;" width="5%"><font size="3px">ลยข้อมูล</font></th>
								<?php } ?>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan="10">ไตรมาส 1</td>
							</tr>
							<?php
							$i = 0;
							foreach ($listlifelongs1 as $listlifelong){ $i++;?> 
								<tr>
									<td style="text-align: center;"><?php  echo $i; ?></td>
									<td style="text-align: center;">
										<?php echo $listlifelong['LifeLongInfo']['subject_name'];?>
									</td>
									
									<td> 
										<?php echo $listlifelong['LifeLongInfo']['type'];?>
									</td>
									<td>
									<?php echo $listlifelong['LifeLongInfo']['code'];?>
									</td>
									<td>
										<a class="btn btn-info" href="<?php echo $listlifelong['LifeLongInfo']['website'];?>">
											<?php echo $listlifelong['LifeLongInfo']['website'];?>
										</a>
									</td>
									<td><?php echo $listlifelong['LifeLongInfo']['count_student'];?></td>
									<td><?php echo $listlifelong['LifeLongInfo']['month'];?></td>
									<td><?php echo $listlifelong['LifeLongInfo']['year'];?></td>
									<?php if($admins != null){ ?>
									<td>
										<a class="btn btn-warning" 
											href="<?php echo $this->Html->url(array('action' => 'edit_life_long', $listlifelong['LifeLongInfo']['id'])); ?>" role="button" target="_blank">
											แก้ไข
										</a>
									</td>
									<?php } ?>
									<?php if($admins != null){ ?>
									<td>
										<a class="btn btn-danger" 
											href="<?php echo $this->Html->url(array('action' => 'remove_life_long', $listlifelong['LifeLongInfo']['id'])); ?>" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')">
											ลบ
										</a>
									</td>
									<?php } ?>
								</tr>
							<?php } ?>
							<tr>
								<td colspan="10">ไตรมาส 2</td>
							</tr>
							<?php foreach ($listlifelongs2 as $listlifelong){ $i++;?> 
								<tr>
									<td><?php  echo $i; ?></td>
									<td>
										<?php echo $listlifelong['LifeLongInfo']['subject_name'];?>
									</td>
									
									<td>
										<?php echo $listlifelong['LifeLongInfo']['type'];?>
									</td>
									<td>
									<?php echo $listlifelong['LifeLongInfo']['code'];?>
									</td>
									<td>
										<a class="btn btn-info" target="_blank" href="<?php echo $listlifelong['LifeLongInfo']['website'];?>">
											<?php echo $listlifelong['LifeLongInfo']['website'];?>
										</a>
									</td>
									<td><?php echo $listlifelong['LifeLongInfo']['count_student'];?></td>
									<td><?php echo $listlifelong['LifeLongInfo']['month'];?></td>
									<td><?php echo $listlifelong['LifeLongInfo']['year'];?></td>
									<?php if($admins != null){ ?>
									<td>
										<a class="btn btn-warning" 
											href="<?php echo $this->Html->url(array('action' => 'edit_life_long', $listlifelong['LifeLongInfo']['id'])); ?>" role="button" target="_blank">
											แก้ไข
										</a>
									</td>
									<?php } ?>
									<?php if($admins != null){ ?>
									<td>
										<a class="btn btn-danger" 
											href="<?php echo $this->Html->url(array('action' => 'remove_life_long', $listlifelong['LifeLongInfo']['id'])); ?>" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')">
											ลบ
										</a>
									</td>
									<?php } ?>
								</tr>
							<?php } ?>
							<tr>
								<td colspan="10">ไตรมาส 3</td>
							</tr>
							<?php foreach ($listlifelongs3 as $listlifelong){ $i++;?> 
								<tr>
									<td align="center"><?php  echo $i; ?></td>
									<td>
										<?php echo $listlifelong['LifeLongInfo']['subject_name'];?>
									</td>
									
									<td>
										<?php echo $listlifelong['LifeLongInfo']['type'];?>
									</td>
									<td>
									<?php echo $listlifelong['LifeLongInfo']['code'];?>
									</td>
									<td>
										<a class="btn btn-info" target="_blank" href="<?php echo $listlifelong['LifeLongInfo']['website'];?>">
											<?php echo $listlifelong['LifeLongInfo']['website'];?>
										</a>
									</td>
									<td><?php echo $listlifelong['LifeLongInfo']['count_student'];?></td>
									<td><?php echo $listlifelong['LifeLongInfo']['month'];?></td>
									<td><?php echo $listlifelong['LifeLongInfo']['year'];?></td>
									<?php if($admins != null){ ?>
									<td>
										<a class="btn btn-warning" 
											href="<?php echo $this->Html->url(array('action' => 'edit_life_long', $listlifelong['LifeLongInfo']['id'])); ?>" role="button" target="_blank">
											แก้ไข
										</a>
									</td>
									<?php } ?>
									<?php if($admins != null){ ?>
									<td>
										<a class="btn btn-danger" 
											href="<?php echo $this->Html->url(array('action' => 'remove_life_long', $listlifelong['LifeLongInfo']['id'])); ?>" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')">
											ลบ
										</a>
									</td>
									<?php } ?>
								</tr>
							<?php } ?>
							<tr>
								<td colspan="10">ไตรมาส 4</td>
							</tr>
							<?php foreach ($listlifelongs4 as $listlifelong){ $i++;?> 
								<tr>
									<td align="center"><?php  echo $i; ?></td>
									<td>
										<?php echo $listlifelong['LifeLongInfo']['subject_name'];?>
									</td>
									
									<td>
										<?php echo $listlifelong['LifeLongInfo']['type'];?>
									</td>
									<td>
									<?php echo $listlifelong['LifeLongInfo']['code'];?>
									</td>
									<td>
										<a class="btn btn-info" target="_blank" href="<?php echo $listlifelong['LifeLongInfo']['website'];?>">
											<?php echo $listlifelong['LifeLongInfo']['website'];?>
										</a>
									</td>
									<td><?php echo $listlifelong['LifeLongInfo']['count_student'];?></td>
									<td><?php echo $listlifelong['LifeLongInfo']['month'];?></td>
									<td><?php echo $listlifelong['LifeLongInfo']['year'];?></td>
									<?php if($admins != null){ ?>
									<td>
										<a class="btn btn-warning" 
											href="<?php echo $this->Html->url(array('action' => 'edit_life_long', $listlifelong['LifeLongInfo']['id'])); ?>" role="button" target="_blank">
											แก้ไข
										</a>
									</td>
									<?php } ?>
									<?php if($admins != null){ ?>
									<td>
										<a class="btn btn-danger" 
											href="<?php echo $this->Html->url(array('action' => 'remove_life_long', $listlifelong['LifeLongInfo']['id'])); ?>" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')">
											ลบ
										</a>
									</td>
									<?php } ?>
								</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>	
			<div class="alert alert-info">
						KPI OWNER :กรศนันท์ สิทธิกุล
			</div>
		</div>
	</div> 
</div>