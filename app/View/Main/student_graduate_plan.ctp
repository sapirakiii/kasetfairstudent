<?php
function DateThai($strDate)
{

	$strYear = date("Y", strtotime($strDate)) + 543;
	$strMonth = date("n", strtotime($strDate));
	$strDay = date("j", strtotime($strDate));
	$strHour = date("H", strtotime($strDate));
	$strMinute = date("i", strtotime($strDate));
	$strSeconds = date("s", strtotime($strDate));
	$strMonthCut = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
	$strMonthThai = $strMonthCut[$strMonth];
	return "$strDay $strMonthThai $strYear";
	//, $strHour:$strMinute
}
?>





<?php
date_default_timezone_set('Asia/Bangkok');
$date = date("Y-m-d");
$time = date("H:i:s");


function DateDiff($strDate1, $strDate2)
{
	return (strtotime($strDate2) - strtotime($strDate1)) /  (60 * 60 * 24);  // 1 day = 60*60*24
}

function TimeDiff($strTime1, $strTime2)
{
	return (strtotime($strTime2) - strtotime($strTime1)) /  (60 * 60); // 1 Hour =  60*60                           
}



?>
<div class="container-fluid" style="padding:15px 0 0;">
	<div class="panel panel-default" style="border-color: #F2F2F2!important">
		<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<div id="no-more-tables">
				<br>
				<center>
					<h3>ร้อยละผู้สำเร็จการศึกษาจบการศึกษาตามหลักสูตรภายในระยะเวลาที่กำหนด (ปีการศึกษาที่ <?php echo $lastyearterms['Yearterm']['year']; ?>)</h3>
				</center>
				<br>
				<?php if($admins != null) { ?>
				<a class="btn btn-success" role="button" target="_blank" href="<?php echo $this->Html->url(array('action'=>'add_student_graduate_plan')); ?>">เพิ่มข้อมูล</a>
				<?php } ?>
				กรุณาเลือกปีการศึกษา
				<form method="get" name="frmselect">
					<div class="dropdown" style="float:left">
						<button class="btn btn-default btn-info dropdown-toggle" type="button" id="dropdownMenu" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown">
							<?php echo $lastyearterms['Yearterm']['year']; ?>
						</button>
						<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
							<?php foreach($dropdowns as $dropdown) { ?>
								<?php
									if($years['Yearterm']['year'] == $dropdown['Yearterm']['year'])
									{
										$active = "active";
									}else{
										$active = "";
									}
								?>
								<li class="<?php echo $active;?>">
									<a href="<?php echo $this->Html->url(array('action' => 'student_graduate_plan',$dropdown['Yearterm']['year'])); ?>">
										<font color="black"><?php echo $dropdown['Yearterm']['year']; ?></font>
									</a>
								</li>
								<?php } ?>
						</ul>
						
					</div>
				</form>
				<br><br>
				<div id="no-more-tables">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th style="text-align: center;" width="5%"><font size="3px">ลำดับ</font></th>
								<th style="text-align: center;" width="40%"><font size="3px">กลุ่มรหัสนักศึกษา</font></th>
								<th style="text-align: center;" width="10%"><font size="3px">เดือน</font></th>
								<th style="text-align: center;" width="10%"><font size="3px">ปี</font></th>
								<th style="text-align: center;" width="10%"><font size="3px">จำนวนนักศึกษาที่สมัคร</font></th>
								<th style="text-align: center;" width="10%"><font size="3px">จำนวนนักศึกษาที่จบ</font></th>
								<th style="text-align: center;" width="10%"><font size="3px">จำนวนนักศึกษาที่ไม่จบ</font></th>
								<th style="text-align: center;" width="10%"><font size="3px">ร้อยละ</font></th>
								<?php if($admins != null) { ?>
								<th style="text-align: center;" width="5%"><font size="3px">แก้ไขข้อมูล</font></th>
								<th style="text-align: center;" width="5%"><font size="3px">ลบข้อมูล</font></th>
								<?php } ?>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th colspan="10">ไตรมาส1</th>
							</tr>
							<tr>
								<?php
									$i = 0;
									foreach ($student_graduate_plans1 as $student_graduate_plan){$i++;
								?>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['id'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['name'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['month'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['year'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['count_in'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['count_grad'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['count_not_grad'] ?></td>
									<td align='center'><?php echo round((($student_graduate_plan['StudentGraduatePlan']['count_grad']*100)/$student_graduate_plan['StudentGraduatePlan']['count_in']) , 2)?></td>
									<?php if($admins != null) { ?>
									<td><a href="<?php echo $this->Html->url(array('action'=>'edit_student_graduate_plan',$student_graduate_plan['StudentGraduatePlan']['id'])); ?>" class="btn btn-warning" role="button" target="_blank">แก้ไขข้อมูล</a></td>
									<td><a href="<?php echo $this->Html->url(array('action'=>'remove_student_graduate_plan',$student_graduate_plan['StudentGraduatePlan']['id']));?>"class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')">ลบข้อมูล</a></td>
									<?php } ?>
							</tr>
							<?php } ?>
							<tr>
								<th colspan="10">ไตรมาส2</th>
							</tr>
							<tr>
								<?php
									$i = 0;
									foreach ($student_graduate_plans2 as $student_graduate_plan){$i++;
								?>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['id'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['name'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['month'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['year'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['count_in'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['count_grad'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['count_not_grad'] ?></td>
									<td align='center'><?php echo round((($student_graduate_plan['StudentGraduatePlan']['count_grad']*100)/$student_graduate_plan['StudentGraduatePlan']['count_in']) , 2)?></td>
									<?php if($admins != null) { ?>
									<td><a href="<?php echo $this->Html->url(array('action'=>'edit_student_graduate_plan',$student_graduate_plan['StudentGraduatePlan']['id'])); ?>" class="btn btn-warning" role="button" target="_blank">แก้ไขข้อมูล</a></td>
									<td><a href="<?php echo $this->Html->url(array('action'=>'remove_student_graduate_plan',$student_graduate_plan['StudentGraduatePlan']['id']));?>"class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')">ลบข้อมูล</a></td>
									<?php } ?>
							</tr>
							<?php } ?>
							<tr>
								<th colspan="10">ไตรมาส3</th>
							</tr>
							<tr>
								<?php
									$i = 0;
									foreach ($student_graduate_plans3 as $student_graduate_plan){$i++;
								?>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['id'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['name'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['month'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['year'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['count_in'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['count_grad'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['count_not_grad'] ?></td>
									<td align='center'><?php echo round((($student_graduate_plan['StudentGraduatePlan']['count_grad']*100)/$student_graduate_plan['StudentGraduatePlan']['count_in']) , 2)?></td>
									<?php if($admins != null) { ?>
									<td><a href="<?php echo $this->Html->url(array('action'=>'edit_student_graduate_plan',$student_graduate_plan['StudentGraduatePlan']['id'])); ?>" class="btn btn-warning" role="button" target="_blank">แก้ไขข้อมูล</a></td>
									<td><a href="<?php echo $this->Html->url(array('action'=>'remove_student_graduate_plan',$student_graduate_plan['StudentGraduatePlan']['id']));?>"class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')">ลบข้อมูล</a></td>
									<?php } ?>
							</tr>
							<?php } ?>
							<tr>
								<th colspan="10">ไตรมาส4</th>
							</tr>
							<tr>
								<?php
									$i = 0;
									foreach ($student_graduate_plans4 as $student_graduate_plan){$i++;
								?>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['id'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['name'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['month'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['year'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['count_in'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['count_grad'] ?></td>
									<td align='center'><?php echo $student_graduate_plan['StudentGraduatePlan']['count_not_grad'] ?></td>
									<td align='center'><?php echo round((($student_graduate_plan['StudentGraduatePlan']['count_grad']*100)/$student_graduate_plan['StudentGraduatePlan']['count_in']) , 2)?></td>
									<?php if($admins != null) { ?>
									<td><a href="<?php echo $this->Html->url(array('action'=>'edit_student_graduate_plan',$student_graduate_plan['StudentGraduatePlan']['id'])); ?>" class="btn btn-warning" role="button" target="_blank">แก้ไขข้อมูล</a></td>
									<td><a href="<?php echo $this->Html->url(array('action'=>'remove_student_graduate_plan',$student_graduate_plan['StudentGraduatePlan']['id']));?>"class="btn btn-danger" role="button" onclick="return confirm('ยืนยันการลบข้อมูล')">ลบข้อมูล</a></td>
									<?php } ?>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>