<div class="container" style="padding:15px 0 0;">
	<div class="panel panel-primary" >
		<div class="panel-heading"> 
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center>รายชื่อผู้ลงทะเบียน</center>
				</h3> 
			</div>
		</div>	
		<div class="panel-body" style="padding-top: 15px; ">
			<div align="center"> 	
				<h4><?php echo $project['Type']['name']; ?></h4>
				
				<h3><?php echo $project['Project']['name']; ?></h3>
			</div> 
			
			<br>
			
			<div class="row">
				<div class="col-md-6 col-md-offset-3" style="padding: 0;">
					<a class="btn btn-warning pull-right" href="<?php echo $this->Html->url(array('action' => 'checklist',$project['Project']['id'])); ?>" role="button">ย้อนกลับ</a>	
					
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-6 col-md-offset-3 well">
					<?php 
					//debug($this->request->data);
					$required = 'กรุณากรอกข้อมูล';
					
					echo $this->Form->create('Member',array('role' => 'form','data-toggle' => 'validator')); 
					
					?>

						<div class="row">
						  <div class="form-group col-md-12" style="margin-bottom: 0;">
							<label>หมายเลขบัตรประชาชน <span style="color: red;">*</span></label>

							<?php echo $this->Form->input('citizen_id', array('type' => 'text','data-error' => $required,'label' => false,'class' => 'form-control','required' => true,'maxlength' => '13')); ?>
							
							<div class="help-block with-errors"></div>
						  </div>
						  
						</div>
						
						
						
						
						<div class="row">
							<div class="col-md-12">
								<input type="submit" value="เข้าสู่ระบบ" class="btn btn-success btn-lg btn-block"> 
							</div>
						</div>
					</form>
				</div>	
			</div>
		</div>

	</div>
	
</div>