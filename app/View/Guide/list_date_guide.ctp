<?php //echo debug($projects);
?>
<div class="container" style="padding:15px 0 0;">
	<div class="panel panel-default" style="border-color: #F2F2F2!important">
		<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
			<div class="panel-title">

			</div>
		</div>
		<div class="panel-body" style="padding-top: 15px; ">
			<div align="center">
				<img src="<?php
							// echo $this->Html->url('/img/A4.png'); 
							?>"> <br>
				<h4>ลงทะเบียน Open House งานเกษตรแห่งชาติ</h4>
				<!-- <h4>ในงานเกษตรภาคเหนือ ครั้งที่ 10 “นวัตกรรมเกษตรอัจฉริยะเพื่อเป้าหมายการพัฒนาที่ยั่งยืน”</h4> -->
				<h4>ในระหว่างวันที่ 2-4 ธันวาคม 2567</h4>
				<br>
			</div>
			<div align="center">
				<a class="btn btn-lg btn-primary" href="<?php echo $this->Html->url(array('action' => 'registerguide')); ?>" role="button" target="_blank">ลงทะเบียน Open House งานเกษตรภาคเหนือ</a>
			</div>
			<div id="no-more-tables">
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th style="text-align: center;">ลำดับ</th>
							<th style="text-align: center;">ชื่อหน่วยงาน</th>
							<th style="text-align: center;">วันที่</th>
							<th style="text-align: center;">รอบที่</th>
							<!-- <th style="text-align: center;">เวลา</th> -->
							<th style="text-align: center;">จำนวนผู้เข้าร่วม</th>
							<th style="text-align: center;">อาจารย์</th>
							<?php if ($UserName != null) { ?>
								<th style="text-align: center;">เบอร์ติดต่อ</th>
								<!-- <th style="text-align: center;">ID LINE</th> -->
							<?php } ?>
							<th style="text-align: center;">ไฟล์รายชื่อผู้เข้าร่วม</th>

							<!-- <th style="text-align: center;">การจัดมัคคุเทศก์</th> -->
						</tr>
					</thead>

					<!-- <tbody>
						<?php
						$countall = 0;
						$i = 0;
						foreach ($Guides as $Guide) {
							$i++;
						?>
							<?php if ($Guide['Guide']['visit'] == 1) { ?>
								<tr class="success">
							<?php } else { ?>
								<tr class="danger">
							<?php } ?>
									<td data-title="ลำดับ" align="center"><?php echo $i; ?></td>
									<td data-title="ชื่อหน่วยงาน"><?php echo $Guide['Guide']['organize'] ?></td>
									<td data-title="วันที่"  align="center"><?php echo $Guide['Dateguide']['name'] ?></td>
									<?php if ($Guide['Guide']['round'] == 1) {
										echo '<td data-title="รอบที่" class="success" align="center"> รอบที่ 1 (เวลา 09.30-10.30)  </td>';
									} else {
										echo '<td data-title="รอบที่" class="danger" align="center"> รอบที่ 2 (เวลา 11.00-12.00)  </td>';
									}
									?>
									<td><?php echo $Guide['Prefix']['name'] . " " . $Guide['Guide']['fname'] . " " . $Guide['Guide']['lname'] ?></td>
									<td><?php echo $Guide['Guide']['prefix_teacher_id'] . " " . $Guide['Guide']['fname_teacher'] ?></td>
									<td  align="center"><?php echo $Guide['Guide']['reftel'] ?></td>
									<td  align="center"><?php echo $Guide['Guide']['refline'] ?></td>
									<td data-title="เวลา"><?php echo $Guide['Guide']['start'] ?>-<?php echo $Guide['Guide']['end'] ?></td> 
									<td data-title="นักเรียน"><?php echo $Guide['Guide']['studentnum'] ?></td>
									<td data-title="อาจารย์"><?php echo $Guide['Guide']['teachernum'] ?></td>
									<td data-title="เกษตรกร"><?php echo $Guide['Guide']['farmernum'] ?></td>
									<td data-title="บุคลากร"><?php echo $Guide['Guide']['staffnum'] ?></td>
									<td data-title="อื่นๆ"><?php echo $Guide['Guide']['othernum'] ?></td> 
									<?php
									$total = 0;
									$total = (((($Guide['Guide']['studentnum'] + $Guide['Guide']['teachernum']) + $Guide['Guide']['farmernum']) + $Guide['Guide']['staffnum']) + $Guide['Guide']['othernum']);
									?> 
									<td data-title="รวม" align="center"><b style="font-size:16px;"> <?php echo $total ?></b></td>
									<?php $countall = $countall + $total; ?> 
									<?php if ($Guide['Guide']['visit'] == 1) {
										echo '<td data-title="การจัดมัคคุเทศก์" class="success" align="center"> ประสงค์  </td>';
									} else {
										echo '<td data-title="การจัดมัคคุเทศก์" class="danger" align="center"> ไม่ประสงค์  </td>';
									}
									?>  
								</tr>
							<?php
						}
							?>						
						<tr>
							<td colspan ="9" data-title="" align="center">รวม</td>
							<td  data-title="" align="center"><p style="font-size:18px;"> <?php echo $countall ?></p></td>
							<td colspan ="2" data-title="" align="center"> คน</td>
						</tr>
					</tbody> -->
					<tbody>
						<?php
						$i = 0;
						foreach ($schools as $school) {
						?>
							<tr>
								<td data-title="กลุ่มโรงเรียน" colspan="8">
									<b><?php echo $school['Guide']['organize']; ?></b>
								</td>
							</tr>
							<?php
							foreach ($Guides as $Guide) {
								if ($school['Guide']['organize'] == $Guide['Guide']['organize']) {
									$i++
							?>
									<tr>
										<td data-title="ลำดับ" align="center"><?php echo $i; ?></td>
										<td data-title="ชื่อหน่วยงาน"><?php echo $Guide['Guide']['organize'] ?></td>
										<td data-title="วันที่" align="center"><?php echo $Guide['Dateguide']['name'] ?></td>
										<?php if ($Guide['Guide']['round'] == 1) {
											echo '<td data-title="รอบที่" class="success" align="center"> รอบที่ 1 (เวลา 09.30-10.30)  </td>';
										} else {
											echo '<td data-title="รอบที่" class="danger" align="center"> รอบที่ 2 (เวลา 11.00-12.00)  </td>';
										}
										?>
										<td data-title="จำนวนผู้เข้าร่วม" align="center"><?php echo $Guide['Guide']['total_attendees'] ?></td>
										<td data-title="อาจารย์" align="center"><?php echo $Guide['Guide']['prefix_teacher_id'] . " " . $Guide['Guide']['fname_teacher'] ?></td>
										<?php if ($UserName != null) { ?>
											<td data-title="เบอร์ติดต่อ" align="center"><?php echo $Guide['Guide']['reftel'] ?></td>
										<?php } ?>

										<td data-title="ไฟล์รายชื่อ" align="center">
											<?= $this->Html->link(
												'ไฟล์รายชื่อ',
												'/files/guide/' . $Guide['Guide']['file'],
												array('target' => '_blank')
											) ?>
										</td>
									</tr>
						<?php
								}
							}
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>



</div>