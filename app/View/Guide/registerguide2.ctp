<div class="container" style="padding:15px 0 0; width:65%;">
	<div class="panel panel-primary" >
		<div class="panel-heading"> 
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center>ลงทะเบียน</center>
				</h3> 
			</div>
		</div>	
		<div class="panel-body" style="padding-top: 15px; ">
			<div align="center"> 	
				<h3><div class="font_panel2">การทัศนศึกษาดูงาน (กิจกรรมมัคคุเทศก์เกษตร)</div></h3>
				
				<h4>งานเกษตรภาคเหนือ  ครั้งที่ 10 ระหว่างวันที่ 6-8 ธ.ค. 2565</h4>
			</div> 

			<br>

			<?php 
			// debug($this->request->data);
				$required = 'กรุณากรอกข้อมูล';
				echo $this->Form->create('Guide',array('role' => 'form','data-toggle' => 'validator')); 
			?>
				<div class="row">
					<div class="help-block with-errors"></div>
				</div>

				<div class="form-group col-md-12" style="margin-bottom:30px;">
				  <label>ชื่อโรงเรียน/มหาวิทยาลัย/หน่วยงาน<span style="color: red;">*</span></label>
					<?php echo $this->Form->input('organize', array(
						'type' => 'text',
						'label' => false,
						'class' => 'form-control',
						'data-error' => $required,
						'required' => true
						)); ?>
					<div class="help-block with-errors"></div>

					<label>ที่อยู่<span style="color: red;">*</span></label>
						<?php echo $this->Form->input('address', array(
							'type' => 'textarea',
						'value' => '
								เลขที่  ...........
								ถนน  ...........
								ตำบล/แขวง  ...........
								อำเภอ/เขต  ...........
								จังหวัด  ............
								รหัสไปรษณีย์  ............โทรศัพท์  ............' ,
						'label' => false,
						'class' => 'form-control ckeditor',
						'data-error' => $required,
						'required' => true,
							)); ?>
					<div class="help-block with-errors"></div>
				</div>
				  
				<label>1) จำนวนผู้ทัศนศึกษาดูงาน <font color="red">(กรณีไม่เติมตัวเลข ระบบจะแสดงจำนวนเป็นค่า 0 โดยอัตโนมัติ)</font> </label>
					<div class="row">
						<div class="form-group col-md-3">
							<label>1.1) นักเรียน </label>
							<?php echo $this->Form->input('studentnum', array('type' => 'number','value' => '0' ,'data-error' => $required,'label' => false,'class' => 'form-control','required' => true,'maxlength' => '13')); ?>
							<div class="help-block with-errors"></div>
						</div>
						<div class="form-group col-md-3">
							<label>1.2) อาจารย์ </label>
							<?php echo $this->Form->input('teachernum', array('type' => 'number','value' => '0' ,'min' => '0','label' => false,'class' => 'form-control')); ?>
						<div class="help-block with-errors"></div>					
					</div>
					<div class="form-group col-md-3" >
						<label>1.3) เกษตรกร</label>
						<?php echo $this->Form->input('farmernum', array('type' => 'number','value' => '0' ,'min' => '0','label' => false,'class' => 'form-control')); ?>
						<div class="help-block with-errors"></div>					
					</div>
					<div class="form-group col-md-3">
						<label>1.4) บุคลากร/เจ้าหน้าที่ </label>
						<?php echo $this->Form->input('staffnum', array('type' => 'number','value' => '0' ,'min' => '0','label' => false,'class' => 'form-control')); ?>
						<div class="help-block with-errors"></div>					
					</div>
					<div class="form-group col-md-6">
						<label>1.5) อื่นๆ (ระบุ) <font color="red">(ไม่บังคับ)</font> </label>
						<?php echo $this->Form->input('otherdetail', array('type' => 'text','label' => false,'class' => 'form-control')); ?>
						<div class="help-block with-errors"></div>
					</div>
					<div class="form-group col-md-6" style="margin-bottom:30px;">
					<label>จำนวน</label>
						<?php echo $this->Form->input('othernum', array('type' => 'number','value' => '0' ,'min' => '0','label' => false,'class' => 'form-control')); ?>
						<div class="help-block with-errors"></div>
					</div>
	
				<label>2) ช่วงเวลาทัศนศึกษาดูงาน</label>
				<div class="row">
				<label class="col-sm-4 control-label">กรุณาเลือกวันที่ต้องการทัศนศึกษาดูงาน <font color="red">*</font> </label>
                    <div class="col-sm-6">
                        <div class="form-group has-feedback">      
                            <?php 
                                echo $this->Form->input('dateguide_id', array(
                                    'options' => $listDateguides,
                                    'label' => false,
                                    'div' => false,
                                    'class' => array('form-control css-require'),                                                           
                                    'error' => false,
                                    'required'
                                ));
                            ?>
							<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                        </div>
                    </div> 
				<label class="col-sm-4 control-label">ช่วงเวลา<font color="red">*</font> </label>
				  	<div class="form-group col-md-6" style="margin-bottom:30px;">  
						<?php 
							echo $this->Form->input('round', array(
								'type'=>'radio',
								'options'=>array('1'=>' รอบที่ 1 (เวลา 09.30-10.30) ','2'=>'รอบที่ 2 (เวลา 11.00-12.00) '),
								'label'=>false,
								'value' => '1',
								'hiddenField'=>false,
								'div' => false,
								'legend'=>false,
								'style' => 'width: 15px; 
								height: 15px; 
								vertical-align:text-bottom;',
							));
						?>
						 <div class="help-block with-errors"></div>					
					 </div>

					<!-- <label class="col-sm-3 control-label">ช่วงเวลาเริ่ม <font color="red">*</font> </label>
                                               <div class="col-sm-7">
                                                     <div class="form-group has-feedback">      
                                                        <?php 
                                                               $types = array('06:00' => '06:00',
                                                                              '06:30' => '06:30',
                                                                              '07:00' => '07:00',
                                                                              '07:30' => '07:30',
                                                                              '08:00' => '08:00',
                                                                              '08:30' => '08:30',
                                                                              '09:00' => '09:00',
                                                                              '09:30' => '09:30',
                                                                              '10:00' => '10:00',
                                                                              '10:30' => '10:30',
                                                                              '11:00' => '11:00',
                                                                              '11:30' => '11:30',
                                                                              '12:00' => '12:00',
                                                                              '12:30' => '12:30',
                                                                              '13:00' => '13:00',
                                                                              '13:30' => '13:30',
                                                                              '14:00' => '14:00',
                                                                              '14:30' => '14:30',
                                                                              '15:00' => '15:00',
                                                                              '15:30' => '15:30',
                                                                              '16:00' => '16:00',
                                                                              '16:30' => '16:30',
                                                                              '17:00' => '17:00',
                                                                              '17:30' => '17:30',
                                                                              '18:00' => '18:00',
                                                                              '18:30' => '18:30',
                                                                              '19:00' => '19:00',
                                                                              '19:30' => '19:30',
                                                                              '20:00' => '20:00',
                                                                              '20:30' => '20:30',
                                                                              '21:00' => '21:00',
                                                                              '21:30' => '21:30',
                                                                              '22:00' => '22:00',
                                                                              '22:30' => '22:30',
                                                                              '23:00' => '23:00',
                                                                              '23:30' => '23:30',
                                                                              '24:00' => '24:00');                                                       
                                                              echo $this->Form->input('start', array(
                                                                  'options' => $types,
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'value' => '00:00',
                                                                  'class' => array('form-control css-require'),                                                           
                                                                  'error' => false,
                                                                  'required'
                                                                  ));
                                                          
                                                            ?>
                                                           
                                                  <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                </div>
                                            </div> -->
                                            <!-- <label class="col-sm-3 control-label">ช่วงเวลาสิ้นสุด <font color="red">*</font> </label>
                                               <div class="col-sm-7">
                                                     <div class="form-group has-feedback">      
                                                        <?php 
                                                              $types = array('06:00' => '06:00',
                                                                              '06:30' => '06:30',
                                                                              '07:00' => '07:00',
                                                                              '07:30' => '07:30',
                                                                              '08:00' => '08:00',
                                                                              '08:30' => '08:30',
                                                                              '09:00' => '09:00',
                                                                              '09:30' => '09:30',
                                                                              '10:00' => '10:00',
                                                                              '10:30' => '10:30',
                                                                              '11:00' => '11:00',
                                                                              '11:30' => '11:30',
                                                                              '12:00' => '12:00',
                                                                              '12:30' => '12:30',
                                                                              '13:00' => '13:00',
                                                                              '13:30' => '13:30',
                                                                              '14:00' => '14:00',
                                                                              '14:30' => '14:30',
                                                                              '15:00' => '15:00',
                                                                              '15:30' => '15:30',
                                                                              '16:00' => '16:00',
                                                                              '16:30' => '16:30',
                                                                              '17:00' => '17:00',
                                                                              '17:30' => '17:30',
                                                                              '18:00' => '18:00',
                                                                              '18:30' => '18:30',
                                                                              '19:00' => '19:00',
                                                                              '19:30' => '19:30',
                                                                              '20:00' => '20:00',
                                                                              '20:30' => '20:30',
                                                                              '21:00' => '21:00',
                                                                              '21:30' => '21:30',
                                                                              '22:00' => '22:00',
                                                                              '22:30' => '22:30',
                                                                              '23:00' => '23:00',
                                                                              '23:30' => '23:30',
                                                                              '24:00' => '24:00'); 
                                                              echo $this->Form->input('end', array(
                                                                   'options' => $types,
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'value' => '00:00',
                                                                  'class' => array('form-control css-require'),                                                           
                                                                  'error' => false,
                                                                  'required'
                                                                  ));
                                                          
                                                            ?>  
														</div>
													</div>
					</div>-->
					<!-- 
					<label>3) การจัดมัคคุเทศก์เกษตรพาเที่ยวชมงาน </label>
					<div class="row">
						<div class="help-block with-errors"></div>
				  	</div>
				  	<div class="form-group col-md-12">  
						<?php 
							echo $this->Form->input('visit', array(
								'type'=>'radio',
								'options'=>array('1'=>' ประสงค์ให้คณะจัดมัคคุเทศก์ ','2'=>' ไม่ประสงค์ '),
								'label'=>false,
								'value' => '1',
								'hiddenField'=>false,
								'div' => false,
								'legend'=>false,
								'style' => 'width: 15px; 
								height: 15px; 
								vertical-align:text-bottom;',
							));
						?><br
						 <div class="help-block with-errors"></div>					
					 </div>	 -->

					<label>3) ผู้ควบคุม/ผู้ประสานงาน </label>
					<div class="row">
						<div class="help-block with-errors"></div>
				  	</div>
					<div class="form-group col-md-3">
						<label>คำนำหน้า <span style="color: red;">*</span></label>
							<?php
								echo $this->Form->input('prefix_id', array(
									'options' => $prefixes,
									'class' => 'form-control',
									'label' => false,
									'required' => true,
									'data-error' => $required,
									'empty' => 'กรุณาเลือกคำนำหน้า'
								));
							?>
					<div class="help-block with-errors"></div>
				  	</div>

				  	<div class="form-group col-md-9">
						<label>ชื่อ-สกุล <span style="color: red;">*</span></label>
							<?php echo $this->Form->input('fname', array(
								'type' => 'text',
								'label' => false,
								'class' => 'form-control',
								'data-error' => $required,
								'required' => true
								)); 
							?>
						<div class="help-block with-errors"></div>
					</div>

					<div class="form-group col-md-6">
						<label>โทรศัพท์มือถือ <span style="color: red;">*</span></label>
							<?php echo $this->Form->input('reftel', array('type' => 'text','label' => false,'class' => 'form-control','data-error' => $required,'required' => true,'maxlength' => '10')); ?>
						<div class="help-block with-errors"></div>
				  	</div>

					<div class="form-group col-md-6">
						<label>Line ID </label>
							<?php echo $this->Form->input('refline', array('type' => 'text','label' => false,'class' => 'form-control','data-error' => $required)); ?>
						<div class="help-block with-errors"></div>
					</div> 
	
				<div class="row">				  
				  <div class="form-group col-md-6">
					
					<label>ท่านทราบข่าวสารจากแหล่งไหน <span style="color: red;">*</span><span style="font-weight:normal;"> (สามารถตอบมากกว่า 1 ข้อ)</span></label>
					
					<?php
						
					foreach ($sources as $source){
							
					?>
					
					<div class="checkbox" style="margin-left:20px;">
						<label>
							<?php echo $this->Form->checkbox('source_id.'.$source['Source']['id'], array(
								'value' => $source['Source']['id'],
								'hiddenField' => false,
								'data-error' => $required,
								// 'required' => true
								)); ?>
						</label>
						<?php echo $source['Source']['name']; ?>
					</div>	
					
					<?php
						
					}
							
					?>

					
					<div class="help-block with-errors"></div>
				  </div>				  
				</div>
		
				
				<div class="row" style="margin:30px 0px 30px;" >
					<div class="col-md-12">
						<center><input type="submit" value="คลิกส่งข้อมูลการลงทะเบียน" class="btn btn-success btn-lg btn-block"></center> 
					</div>
				</div>
				</form>
			</div>
		</div>
	</div>
	

	<!-- <script type="text/javascript">
		$('.form-group').on("click",'input:checkbox',function(){          
			checkboxValidate($(this).attr('name'));
		});
		function checkboxValidate(name){
			var min = 1 //minumum number of boxes to be checked for this form-group
			if($('input:checked').length<min){
				$('input:checkbox').prop('required',true);
			}
			else{
				$('input:checkbox').prop('required',false);
			}
			
			$('#MemberRegisterForm').validator('validate');
			
			// if($('input[name="'+name+'"]:checked').length<min){
				// $('input[name="'+name+'"]').prop('required',true);
			// }
			// else{
				// $('input[name="'+name+'"]').prop('required',false);
			// }
		}
	</script> -->
