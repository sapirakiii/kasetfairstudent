<style>
	@CHARSET "UTF-8";

	* {
		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
				box-sizing: border-box;
		outline: none;
	}
	body {
		background: url(https://www.agri.cmu.ac.th/smart_academic/img/bg_login.jpg) no-repeat center center fixed;
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
	}

	.login-form {
		font: 16px/2em Lato, serif;
		margin: 0px auto;
		max-width: 400px;
	}

	form[role=login] {
		color: #5d5d5d;
		background: #f2f2f2;
		padding: 26px;
		border-radius: 10px;
		-moz-border-radius: 10px;
		-webkit-border-radius: 10px;
	}
	form[role=login] img {
		display: block;
		margin: 0 auto;
		margin-bottom: 35px;
	}
	form[role=login] input,
	form[role=login] button {
		font-size: 18px;
		margin: 16px 0;
	}
	form[role=login] > div {
		text-align: center;
	}
	
	.form-links {
		text-align: center;
		margin-top: 1em;
		margin-bottom: 50px;
	}
	.form-links a {
		color: #fff;
	}

</style>


  <!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=1,initial-scale=1,user-scalable=1" />
	<title>Insert title here</title>
	
	<link href="http://fonts.googleapis.com/css?family=Lato:100italic,100,300italic,300,400italic,400,700italic,700,900italic,900" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="assets/css/styles.css" />
	
	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

	<section class="container">
			<section class="login-form">
				<form method="post" action="" role="login">
					<img src="<?php echo $this->Html->url('/img/746.jpg'); ?>"  class="img-responsive" alt="" style='height: 100%; width: 100%; object-fit: contain'/>
					<center>
						<h4>Certificate สำหรับผู้เข้าร่วมกิจกรรม <br> Open house งานเกษตรแห่งชาติ 2567 <br> คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่<br> </h4>
						<h4>กรุณากรอกชื่อและนามสกุลผู้เข้าร่วม</h4> <h6>(ไม่ต้องใส่คำนำหน้าชื่อ)</h6>
					
					<input type="text" name="data[name]" placeholder="ชื่อ นามสกุล ผู้เข้าร่วมกิจกรรม" required class="form-control input-lg" />
					<button type="submit" name="go" class="btn btn-lg btn-primary btn-block">Login</button>
					</center>
				</form>
				<div class="form-links">
					<!-- <a href="#">www.website.com</a> -->
					<P><font color="white">© 2024 Faculty of Agriculture</font></P>
				</div>
			</section>
	</section>
	
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>
</html>


<!-- /container -->