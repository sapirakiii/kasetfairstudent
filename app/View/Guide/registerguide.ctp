<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dependencies/JQL.min.js"></script>
<script type="text/javascript" src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dependencies/typeahead.bundle.js"></script>

<link rel="stylesheet" href="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dist/jquery.Thailand.min.css">
<script type="text/javascript" src="https://earthchie.github.io/jquery.Thailand.js/jquery.Thailand.js/dist/jquery.Thailand.min.js"></script>

<div class="container" style="padding:15px 0 0; width:65%;">
	<div class="panel-heading">
		<div class="panel-title">
			<!-- <h3 style="margin:0px !important;">
				<center>ลงทะเบียน Open House ระหว่างวันที่ 2-4 พฤศจิกายน 2567</center>
			</h3> -->
			<h2>
				<center>ลงทะเบียน Open House งานเกษตรแห่งชาติ</center>
			</h2>
			<h2>
				<center>ในระหว่างวันที่ 2-4 ธันวาคม 2567</center>
			</h2>

		</div>
	</div>

	<!-- <div align="center" style="padding-top: 15px; ">
		<h3>
			<div class="font_panel2"> ลงทะเบียน Open House ระหว่างวันที่ 2-4 พฤศจิกายน 2567</div>
		</h3>
	</div> -->
	<br />
	<form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">
		<?php
		//debug($this->request->data);
		$required = 'กรุณากรอกข้อมูล';
		echo $this->Form->create('Guide', array('role' => 'form', 'data-toggle' => 'validator'));
		?>
		<div class="form-group col-md-12" style="margin-bottom:20px;">
			<label>ชื่อโรงเรียน/มหาวิทยาลัย/หน่วยงาน<span style="color: red;">*</span></label>
			<?php echo $this->Form->input('organize', array(
				'type' => 'text',
				'label' => false,
				'class' => 'form-control',
				'data-error' => $required,
				'required' => true
			)); ?>
		</div>
		<div class="form-group col-md-12" style="margin-bottom:20px;">
			<label>ที่อยู่โรงเรียน/มหาวิทยาลัย/หน่วยงาน <span style="color: red;">*</span></label>
			<?php echo $this->Form->input('school_address', array(
				'type' => 'text',
				'label' => false,
				'class' => 'form-control',
				'data-error' => $required,
				'required' => true
			)); ?>
		</div>
		<div class="row">
			<div class="col-sm-3" style="margin-bottom:20px;">
				<label>ตำบล <span style="color: red;">*</span></label>
				<div class="form-group has-feedback">
					<?php echo $this->Form->input('school_tambon', array(
						'id' => 'school_tambon',
						'type' => 'text',
						'label' => false,
						'class' => 'form-control',
						'data-error' => $required,
						'required' => true
					)); ?>
				</div>
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
			</div>

			<div class="col-sm-3">
				<label>อำเภอ <span style="color: red;">*</span></label>
				<div class="form-group has-feedback">
					<?php echo $this->Form->input('school_amphoe', array(
						'id' => 'school_amphoe',
						'type' => 'text',
						'label' => false,
						'class' => 'form-control',
						'data-error' => $required,
						'required' => true
					)); ?>
				</div>
			</div>

			<div class="col-sm-3">
				<label>จังหวัด <span style="color: red;">*</span></label>
				<div class="form-group has-feedback">
					<?php echo $this->Form->input('school_province', array(
						'id' => 'school_province',
						'type' => 'text',
						'label' => false,
						'class' => 'form-control',
						'data-error' => $required,
						'required' => true
					)); ?>
				</div>
			</div>

			<div class="col-sm-3">
				<label>รหัสไปรษณีย์ <span style="color: red;">*</span></label>
				<div class="form-group has-feedback">
					<?php echo $this->Form->input('school_zipcode', array(
						'id' => 'school_zipcode',
						'type' => 'text',
						'label' => false,
						'class' => 'form-control',
						'data-error' => $required,
						'required' => true
					)); ?>
				</div>
			</div>
		</div>

		<script>
			$.Thailand({
				$district: $('#school_tambon'), // input ของตำบล
				$amphoe: $('#school_amphoe'), // input ของอำเภอ
				$province: $('#school_province'), // input ของจังหวัด
				$zipcode: $('#school_zipcode'), // input ของรหัสไปรษณีย์
			});
		</script>

		<div class="row">
			<div class="col-md-6" style="margin-bottom:20px;">
				<label>จำนวนนักเรียน/นักศึกษาที่เข้าร่วม <span style="color: red;">*</span></label>
				<div class="form-group has-feedback">
					<?php echo $this->Form->input('total_attendees', array(
						'id' => 'total_attendees',
						'type' => 'text',
						'label' => false,
						'class' => 'form-control',
						'data-error' => $required,
						'required' => true
					)); ?>
				</div>
			</div>

			<div class="form-group col-md-6">
				<label>แนบใบรายชื่อนักเรียน/นักศึกษาที่เข้าร่วม (WORD,EXEL,PDF,JPG)</label>
				<?php
				echo $this->Form->input(
					'Guide.files.',
					array(
						'type' => 'file',
						'accept' => '.xlsx,.xls,image/*, .doc, .docx, .ppt, .pptx, .txt, .pdf, .zip, .rar'
					)
				);
				?>
			</div>

		</div>


		<br>


		<label>2) ช่วงเวลาทัศนศึกษาดูงาน</label>
		<br />
		<div class="row">
			<div class="col-sm-6" style="margin-bottom:20px;">
				<label>กรุณาเลือกวันที่ต้องการทัศนศึกษาดูงาน <font color="red">*</font> </label>
				<div class="form-group has-feedback">
					<?php
					echo $this->Form->input('dateguide_id', array(
						'options' => $listDateguides,
						'label' => false,
						'div' => false,
						'class' => array('form-control css-require'),
						'error' => false,
						'required'
					)); ?>
					<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
				</div>
			</div>



			<div class="col-md-6">
				<label>ช่วงเวลา<font color="red">*</font> </label>
				<div class="form-group has-feedback">
					<?php
					echo $this->Form->input('round', array(
						'type' => 'radio',
						'options' => array('1' => ' รอบเช้า (เวลา 10:00-11:00) ', '2' => 'รอบบ่าย (เวลา 13:30-14:30) '),
						'label' => false,
						'value' => '1',
						'hiddenField' => false,
						'div' => false,
						'legend' => false,
						'style' => 'width: 15px; 
						height: 15px; 
						vertical-align:text-bottom;',
					));
					?>
					<div class="help-block with-errors"></div>
				</div>
			</div>
		</div>

		<br>

		<div class="row">

			<label>3) อาจารย์ผู้ควบคุม/ผู้ประสานงาน</label>
			<div class="row">
				<div class="help-block with-errors"></div>
			</div>
			<div class="form-group col-md-3" style="margin-bottom:20px;">
				<label>คำนำหน้า <span style="color: red;">*</span></label>
				<?php
				echo $this->Form->input('prefix_teacher_id', array(
					'options' => $prefixes,
					'class' => 'form-control',
					'label' => false,
					'required' => true,
					'data-error' => $required,
					'empty' => 'กรุณาเลือกคำนำหน้า'
				)); ?>
				<div class="help-block with-errors"></div>
			</div>
			<div class="form-group col-md-6">
				<label>ชื่อ-สกุล <span style="color: red;">*</span></label>
				<?php echo $this->Form->input('fname_teacher', array(
					'type' => 'text',
					'label' => false,
					'class' => 'form-control',
					'data-error' => $required,
					'required' => true
				));
				?>
				<div class="help-block with-errors"></div>
			</div>
			<div class="form-group col-md-3">
				<label>โทรศัพท์มือถือ <span style="color: red;">*</span></label>
				<?php echo $this->Form->input('reftel', array('type' => 'text', 'label' => false, 'class' => 'form-control', 'data-error' => $required, 'required' => true, 'maxlength' => '10')); ?>
				<div class="help-block with-errors"></div>
			</div>
		</div>

		<div class="row" style="margin:30px 0px 30px;">
			<div class="col-md-12">
				<center><input type="submit" value="คลิกส่งข้อมูลการลงทะเบียน" class="btn btn-success btn-lg btn-block"></center>
			</div>
		</div>
	</form>
</div>