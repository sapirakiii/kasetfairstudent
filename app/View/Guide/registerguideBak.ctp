<div class="container" style="padding:15px 0 0; width:65%;" >
	<div class="panel-heading"> 
		<div class="panel-title">
			<h3 style="margin:0px !important;">
				<center>ลงทะเบียน</center>
			</h3> 
		</div>
	</div>	

	<div align="center" style="padding-top: 15px; "> 	
		<h3><div class="font_panel2"> ลงทะเบียน Open House งานเกษตรภาคเหนือ</div></h3>
		
		<h4>งานเกษตรภาคเหนือ  ครั้งที่ 10 ระหว่างวันที่ 6-8 ธ.ค. 2565</h4>
	</div> <br />

	<?php 
	//debug($this->request->data);
		$required = 'กรุณากรอกข้อมูล';
		echo $this->Form->create('Guide',array('role' => 'form','data-toggle' => 'validator')); 
	?>
		<div class="form-group col-md-12" style="margin-bottom:30px;">
			<label>ชื่อโรงเรียน/มหาวิทยาลัย/หน่วยงาน<span style="color: red;">*</span></label>
				<?php echo $this->Form->input('organize', array(
					'type' => 'text',
					'label' => false,
					'class' => 'form-control',
					'data-error' => $required,
					'required' => true
				)); ?>

			<br />

			<label>ที่อยู่<span style="color: red;">*</span></label>
				<?php echo $this->Form->input('address', array(
					'type' => 'textarea',
					'value' => '
							เลขที่  ...........
							ถนน  ...........
							ตำบล/แขวง  ...........
							อำเภอ/เขต  ...........
							จังหวัด  ............
							รหัสไปรษณีย์  ............โทรศัพท์  ............' ,
					'label' => false,
					'class' => 'form-control ckeditor',
					'data-error' => $required,
					'required' => true,
				)); ?>
		</div>

		<div style="margin-bottom:30px;"></div>
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th style="text-align: center;">ลำดับ</th>  
							<th style="text-align: center;">คำนำหน้า</th>
							<th style="text-align: center;">ชื่อ </th>
							<th style="text-align: center;">สกุล</th>
						</tr>
					</thead>
					<tbody>  
						<!-- ************************** 1********************************   -->
						<tr style="text-align: center;">	
							<td data-title="อันดับที่" align="center">1</td>
								<td data-title="คำนำหน้า">
									<div class="form-group has-feedback">  
										<?php	
											$required = 'กรุณากรอกข้อมูล';			
											echo $this->Form->input('Guide.prefix_id1', array(
											'options' => $prefixes,
											'class' => 'form-control',
											'label' => false,
											'value' => "01",
											'required' => true,
											'data-error' => $required,
											'empty' => 'กรุณาเลือกคำนำหน้า'
											)); 
										?>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									</div>                                         
								</td>
							</td>
							<td data-title="ชื่อ"> 
								<div class="form-group has-feedback">      
									<?php 
										echo $this->Form->input('Guide.name1', array(
										'type' => 'text',
										'label' => false,
										'div' => false,
										'class' => array('form-control css-require'),                                                           
										'error' => false,
										));
									?>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								</div>	
							</td>
							<td data-title="สกุล"> 
								<div class="form-group has-feedback">      
									<?php 
										echo $this->Form->input('Guide.lname1', array(
										'type' => 'text',
										'label' => false,
										'div' => false,
										'class' => array('form-control css-require'),                                                           
										'error' => false,
										));
									?>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								</div>
							</td>
						</tr>
						<!-- ************************** 2 ********************************   -->
						<tr style="text-align: center;">	
							<td data-title="อันดับที่" align="center">2</td>
								<td data-title="คำนำหน้า">
									<div class="form-group has-feedback">  
										<?php	
											$required = 'กรุณากรอกข้อมูล';			
											echo $this->Form->input('Guide.prefix_id2', array(
											'options' => $prefixes,
											'class' => 'form-control',
											'label' => false,
											'value' => "01",
											'required' => true,
											'data-error' => $required,
											'empty' => 'กรุณาเลือกคำนำหน้า'
											)); 
										?>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									</div>                                         
								</td>
							</td>
							<td data-title="ชื่อ"> 
								<div class="form-group has-feedback">      
									<?php 
										echo $this->Form->input('Guide.name2', array(
										'type' => 'text',
										'label' => false,
										'div' => false,
										'class' => array('form-control css-require'),                                                           
										'error' => false,
										));
									?>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								</div>	
							</td>
							<td data-title="สกุล"> 
								<div class="form-group has-feedback">      
									<?php 
										echo $this->Form->input('Guide.lname2', array(
										'type' => 'text',
										'label' => false,
										'div' => false,
										'class' => array('form-control css-require'),                                                           
										'error' => false,
										));
									?>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								</div>
							</td>
						</tr>
						<!-- ************************** 3 ********************************   -->
						<tr style="text-align: center;">	
							<td data-title="อันดับที่" align="center">3</td>
								<td data-title="คำนำหน้า">
									<div class="form-group has-feedback">  
										<?php	
											$required = 'กรุณากรอกข้อมูล';			
											echo $this->Form->input('Guide.prefix_id3', array(
											'options' => $prefixes,
											'class' => 'form-control',
											'label' => false,
											'value' => "01",
											'required' => true,
											'data-error' => $required,
											'empty' => 'กรุณาเลือกคำนำหน้า'
											)); 
										?>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									</div>                                         
								</td>
							</td>
							<td data-title="ชื่อ"> 
								<div class="form-group has-feedback">      
									<?php 
										echo $this->Form->input('Guide.name3', array(
										'type' => 'text',
										'label' => false,
										'div' => false,
										'class' => array('form-control css-require'),                                                           
										'error' => false,
										));
									?>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								</div>	
							</td>
							<td data-title="สกุล"> 
								<div class="form-group has-feedback">      
									<?php 
										echo $this->Form->input('Guide.lname3', array(
										'type' => 'text',
										'label' => false,
										'div' => false,
										'class' => array('form-control css-require'),                                                           
										'error' => false,
										));
									?>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								</div>
							</td>
						</tr>
						<!-- ************************** 4 ********************************   -->
						<tr style="text-align: center;">	
							<td data-title="อันดับที่" align="center">3</td>
								<td data-title="คำนำหน้า">
									<div class="form-group has-feedback">  
										<?php	
											$required = 'กรุณากรอกข้อมูล';			
											echo $this->Form->input('Guide.prefix_id4', array(
											'options' => $prefixes,
											'class' => 'form-control',
											'label' => false,
											'value' => "01",
											'required' => true,
											'data-error' => $required,
											'empty' => 'กรุณาเลือกคำนำหน้า'
											)); 
										?>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									</div>                                         
								</td>
							</td>
							<td data-title="ชื่อ"> 
								<div class="form-group has-feedback">      
									<?php 
										echo $this->Form->input('Guide.name4', array(
										'type' => 'text',
										'label' => false,
										'div' => false,
										'class' => array('form-control css-require'),                                                           
										'error' => false,
										));
									?>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								</div>	
							</td>
							<td data-title="สกุล"> 
								<div class="form-group has-feedback">      
									<?php 
										echo $this->Form->input('Guide.lname4', array(
										'type' => 'text',
										'label' => false,
										'div' => false,
										'class' => array('form-control css-require'),                                                           
										'error' => false,
										));
									?>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								</div>
							</td>
						</tr>
						<!-- ************************** 5 ********************************   -->
						<tr style="text-align: center;">	
							<td data-title="อันดับที่" align="center">5</td>
								<td data-title="คำนำหน้า">
									<div class="form-group has-feedback">  
										<?php	
											$required = 'กรุณากรอกข้อมูล';			
											echo $this->Form->input('Guide.prefix_id5', array(
											'options' => $prefixes,
											'class' => 'form-control',
											'label' => false,
											'value' => "01",
											'required' => true,
											'data-error' => $required,
											'empty' => 'กรุณาเลือกคำนำหน้า'
											)); 
										?>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									</div>                                         
								</td>
							</td>
							<td data-title="ชื่อ"> 
								<div class="form-group has-feedback">      
									<?php 
										echo $this->Form->input('Guide.name5', array(
										'type' => 'text',
										'label' => false,
										'div' => false,
										'class' => array('form-control css-require'),                                                           
										'error' => false,
										));
									?>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								</div>	
							</td>
							<td data-title="สกุล"> 
								<div class="form-group has-feedback">      
									<?php 
										echo $this->Form->input('Guide.lname5', array(
										'type' => 'text',
										'label' => false,
										'div' => false,
										'class' => array('form-control css-require'),                                                           
										'error' => false,
										));
									?>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								</div>
							</td>
						</tr>
						<!-- ************************** 6 ********************************   -->
						<tr style="text-align: center;">	
							<td data-title="อันดับที่" align="center">6</td>
								<td data-title="คำนำหน้า">
									<div class="form-group has-feedback">  
										<?php	
											$required = 'กรุณากรอกข้อมูล';			
											echo $this->Form->input('Guide.prefix_id6', array(
											'options' => $prefixes,
											'class' => 'form-control',
											'label' => false,
											'value' => "01",
											'required' => true,
											'data-error' => $required,
											'empty' => 'กรุณาเลือกคำนำหน้า'
											)); 
										?>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									</div>                                         
								</td>
							</td>
							<td data-title="ชื่อ"> 
								<div class="form-group has-feedback">      
									<?php 
										echo $this->Form->input('Guide.name6', array(
										'type' => 'text',
										'label' => false,
										'div' => false,
										'class' => array('form-control css-require'),                                                           
										'error' => false,
										));
									?>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								</div>	
							</td>
							<td data-title="สกุล"> 
								<div class="form-group has-feedback">      
									<?php 
										echo $this->Form->input('Guide.lname6', array(
										'type' => 'text',
										'label' => false,
										'div' => false,
										'class' => array('form-control css-require'),                                                           
										'error' => false,
										));
									?>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								</div>
							</td>
						</tr>
						<!-- ************************** 7 ********************************   -->
						<tr style="text-align: center;">	
							<td data-title="อันดับที่" align="center">7</td>
								<td data-title="คำนำหน้า">
									<div class="form-group has-feedback">  
										<?php	
											$required = 'กรุณากรอกข้อมูล';			
											echo $this->Form->input('Guide.prefix_id7', array(
											'options' => $prefixes,
											'class' => 'form-control',
											'label' => false,
											'value' => "01",
											'required' => true,
											'data-error' => $required,
											'empty' => 'กรุณาเลือกคำนำหน้า'
											)); 
										?>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									</div>                                         
								</td>
							</td>
							<td data-title="ชื่อ"> 
								<div class="form-group has-feedback">      
									<?php 
										echo $this->Form->input('Guide.name7', array(
										'type' => 'text',
										'label' => false,
										'div' => false,
										'class' => array('form-control css-require'),                                                           
										'error' => false,
										));
									?>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								</div>	
							</td>
							<td data-title="สกุล"> 
								<div class="form-group has-feedback">      
									<?php 
										echo $this->Form->input('Guide.lname7', array(
										'type' => 'text',
										'label' => false,
										'div' => false,
										'class' => array('form-control css-require'),                                                           
										'error' => false,
										));
									?>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								</div>
							</td>
						</tr>
						<!-- ************************** 8 ********************************   -->
						<tr style="text-align: center;">	
							<td data-title="อันดับที่" align="center">8</td>
								<td data-title="คำนำหน้า">
									<div class="form-group has-feedback">  
										<?php	
											$required = 'กรุณากรอกข้อมูล';			
											echo $this->Form->input('Guide.prefix_id8', array(
											'options' => $prefixes,
											'class' => 'form-control',
											'label' => false,
											'value' => "01",
											'required' => true,
											'data-error' => $required,
											'empty' => 'กรุณาเลือกคำนำหน้า'
											)); 
										?>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									</div>                                         
								</td>
							</td>
							<td data-title="ชื่อ"> 
								<div class="form-group has-feedback">      
									<?php 
										echo $this->Form->input('Guide.name8', array(
										'type' => 'text',
										'label' => false,
										'div' => false,
										'class' => array('form-control css-require'),                                                           
										'error' => false,
										));
									?>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								</div>	
							</td>
							<td data-title="สกุล"> 
								<div class="form-group has-feedback">      
									<?php 
										echo $this->Form->input('Guide.lname8', array(
										'type' => 'text',
										'label' => false,
										'div' => false,
										'class' => array('form-control css-require'),                                                           
										'error' => false,
										));
									?>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								</div>
							</td>
						</tr>
						<!-- ************************** 9 ********************************   -->
						<tr style="text-align: center;">	
							<td data-title="อันดับที่" align="center">9</td>
								<td data-title="คำนำหน้า">
									<div class="form-group has-feedback">  
										<?php	
											$required = 'กรุณากรอกข้อมูล';			
											echo $this->Form->input('Guide.prefix_id9', array(
											'options' => $prefixes,
											'class' => 'form-control',
											'label' => false,
											'value' => "01",
											'required' => true,
											'data-error' => $required,
											'empty' => 'กรุณาเลือกคำนำหน้า'
											)); 
										?>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									</div>                                         
								</td>
							</td>
							<td data-title="ชื่อ"> 
								<div class="form-group has-feedback">      
									<?php 
										echo $this->Form->input('Guide.name9', array(
										'type' => 'text',
										'label' => false,
										'div' => false,
										'class' => array('form-control css-require'),                                                           
										'error' => false,
										));
									?>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								</div>	
							</td>
							<td data-title="สกุล"> 
								<div class="form-group has-feedback">      
									<?php 
										echo $this->Form->input('Guide.lname9', array(
										'type' => 'text',
										'label' => false,
										'div' => false,
										'class' => array('form-control css-require'),                                                           
										'error' => false,
										));
									?>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								</div>
							</td>
						</tr>
						<!-- ************************** 10 ********************************   -->
						<tr style="text-align: center;">	
							<td data-title="อันดับที่" align="center">10</td>
								<td data-title="คำนำหน้า">
									<div class="form-group has-feedback">  
										<?php	
											$required = 'กรุณากรอกข้อมูล';			
											echo $this->Form->input('Guide.prefix_id10', array(
											'options' => $prefixes,
											'class' => 'form-control',
											'label' => false,
											'value' => "01",
											'required' => true,
											'data-error' => $required,
											'empty' => 'กรุณาเลือกคำนำหน้า'
											)); 
										?>
										<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
									</div>                                         
								</td>
							</td>
							<td data-title="ชื่อ"> 
								<div class="form-group has-feedback">      
									<?php 
										echo $this->Form->input('Guide.name10', array(
										'type' => 'text',
										'label' => false,
										'div' => false,
										'class' => array('form-control css-require'),                                                           
										'error' => false,
										));
									?>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								</div>	
							</td>
							<td data-title="สกุล"> 
								<div class="form-group has-feedback">      
									<?php 
										echo $this->Form->input('Guide.lname10', array(
										'type' => 'text',
										'label' => false,
										'div' => false,
										'class' => array('form-control css-require'),                                                           
										'error' => false,
										));
									?>
									<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
								</div>
							</td>
						</tr>
												
					</tbody>
                </table>

		<label>2) ช่วงเวลาทัศนศึกษาดูงาน</label>
		<div class="row">
			<label class="col-sm-4 control-label">กรุณาเลือกวันที่ต้องการทัศนศึกษาดูงาน <font color="red">*</font> </label>
			<div class="col-sm-6">
				<div class="form-group has-feedback">      
					<?php 
						echo $this->Form->input('dateguide_id', array(
							'options' => $listDateguides,
							'label' => false,
							'div' => false,
							'class' => array('form-control css-require'),                                                           
							'error' => false,
							'required'
						));?>
				<span class="glyphicon form-control-feedback" aria-hidden="true"></span>
				</div>
			</div> 
			<label class="col-sm-4 control-label">ช่วงเวลา<font color="red">*</font> </label>
			<div class="form-group col-md-6" style="margin-bottom:30px;">  
				<?php 
					echo $this->Form->input('round', array(
						'type'=>'radio',
						'options'=>array('1'=>' รอบที่ 1 (เวลา 09.30-10.30) ','2'=>'รอบที่ 2 (เวลา 11.00-12.00) '),
						'label'=>false,
						'value' => '1',
						'hiddenField'=>false,
						'div' => false,
						'legend'=>false,
						'style' => 'width: 15px; 
						height: 15px; 
						vertical-align:text-bottom;',
					));
				?>
			<div class="help-block with-errors"></div>					
		</div>
		 	
		<label>3) อาจารย์ผู้ควบคุม/ผู้ประสานงาน</label>
		<div class="row">
			<div class="help-block with-errors"></div>
		</div>
		<div class="form-group col-md-3">
			<label>คำนำหน้า <span style="color: red;">*</span></label>
				<?php
					echo $this->Form->input('prefix_teacher_id', array(
						'options' => $prefixes,
						'class' => 'form-control',
						'label' => false,
						'required' => true,
						'data-error' => $required,
						'empty' => 'กรุณาเลือกคำนำหน้า'
					)); ?>
			<div class="help-block with-errors"></div>
		</div>
		<div class="form-group col-md-9">
			<label>ชื่อ-สกุล <span style="color: red;">*</span></label>
				<?php echo $this->Form->input('fname_teacher', array(
					'type' => 'text',
					'label' => false,
					'class' => 'form-control',
					'data-error' => $required,
					'required' => true
					)); 
				?>
			<div class="help-block with-errors"></div>
		</div>
		<div class="form-group col-md-6">
			<label>โทรศัพท์มือถือ <span style="color: red;">*</span></label>
				<?php echo $this->Form->input('reftel', array('type' => 'text','label' => false,'class' => 'form-control','data-error' => $required,'required' => true,'maxlength' => '10')); ?>
			<div class="help-block with-errors"></div>
		</div>
		<div class="form-group col-md-6">
			<label>Line ID </label>
				<?php echo $this->Form->input('refline', array('type' => 'text','label' => false,'class' => 'form-control','data-error' => $required)); ?>
			<div class="help-block with-errors"></div>
		</div> 

		<div class="row" style="margin:30px 0px 30px;" >
			<div class="col-md-12">
				<center><input type="submit" value="คลิกส่งข้อมูลการลงทะเบียน" class="btn btn-success btn-lg btn-block"></center> 
			</div>
		</div>
	</form>
</div>

 