<?php //debug($Events);?>
<script type="text/javascript">
function confirm_click()
{
return confirm("ยืนยันการลบข้อมูล ?");
}

</script>
<div class="container">
	<div class="row">
		<div class="col-sm-12 md-12 xs-12 offset-1" style="background-color: #FFFFFF!important;border-color: #FFFFFF!important">
	<a href="<?php echo $this->Html->url(array('action' => 'AddEvent')); ?>" class="btn btn-success">
		<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
		เพิ่ม
	</a>
	<table class="table table-hover table-post" style="width: 100%;">
		<tr>
		    <!-- <th>ลำดับ</th> -->
		    <th>วันที่กิจกรรมที่กำลังจะเกิดขึ้น</th>
			<th>เวลา</th>
		    <th style="min-width: 350px;width: auto;">หัวข้อกิจกรรมที่กำลังจะเกิดขึ้น</th> 
		    <!--<th>ประเภท</th>-->
		    <!--<th>ทั่วไป</th>
		    <th>ภายใน</th>-->
		   
		    <!-- <th>รายละเอียด</th> -->
		   
			
		    <th>แก้ไข/ลบ</th>
	  	</tr>
	  	
	  	<?php
			foreach ($events as $event):
		?>
	  	<tr>
			<!-- <td>
				
				<?php
					echo $event['Event']['id'];
				?>
			</td> -->
			<td>
				<?php
					echo $event['Event']['postdate'];
					
				?>
			</td>
			<td>
				<?php echo '<i class="glyphicon glyphicon-time" aria-hidden="true"></i>'.date('H.i',strtotime( $event['Event']['timestart'])).'-'.date('H.i',strtotime( $event['Event']['timeend'])).' น.'; ?>
			</td>
			<td>
				<a href="<?php echo $this->Html->url(array('action' => 'detail',$event['Event']['id'])); ?>" target="_blank">
					<?php
			  			echo $event['Event']['Title'];
					?>
				</a>
			</td>
			<!-- <td>
				<?php
					echo $event['Event']['Detail'];
				?>
			</td> -->
			
			
			<td>
				<a href="<?php echo $this->Html->url(array('action' => 'EditEvent',$event['Event']['id'])); ?>" class="btn btn-warning">
					<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
					แก้ไข
				</a>
				<a href="<?php echo $this->Html->url(array('action' => 'DeleteEvent',$event['Event']['id'])); ?>" class="btn btn-danger" onclick="return confirm_click();">
					<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
					ลบ
				</a>
			</td>
	  	</tr>
	  	<?php	
			endforeach;
		?>
	  	<tr>
	  		<?php echo $this->Paginator->numbers(array(
		  			'before' => '<div class="pagination"><ul>',
				    'separator' => '',
				    'currentClass' => 'active',
				    'tag' => 'li class="btn btn-default"',
				    'after' => '</ul></div>'
			    )); 
			?>
	  	</tr>
	</table>
	<?php 
		echo $this->Paginator->numbers(array(
			'before' => '<div class="pagination"><ul>',
		    'separator' => '',
		    'currentClass' => 'active',
		    'tag' => 'li class="btn btn-default"',
		    'after' => '</ul></div>'
		)); 
	?>
	<hr>
		</div>
 	</div>
</div> <!-- /container -->