<?php //debug($TypeNew);?>

<div class="container">
	
	<form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">

		<table class="table table-hover table-post" style="width: 100%;">
			<tr>
				<td>
				<?php 
                             echo $this->Form->input('Projectvdo.id', array(
                                  'type' => 'text',
                                  'label' => false,
                                   'hidden'      
                            ));
                     ?> 
	  						
					<?php
		  				

						$name = $UserName['Employee']['fname']." ".$UserName['Employee']['lname'];
						echo $this->Form->hidden('Projectvdo.PostName', array(
		  						'value' => $name,
		  						'class' => 'form-control',
						));
					?>
					
				</td>
		  	</tr>
			  <tr>
			  <td>
				  <label for="ProjectvdoDetail" class="addpost_input">ประเภทภาษา</label>
				<?php 
					 $types = array('1' => 'วีดีโอภาษาไทย (สำหรับแสดงเว็บไซต์ ภาษาไทย)', '2' => 'วีดีโอภาษาอังกฤษ (สำหรับแสดงเว็บไซต์ ภาษาอังกฤษ)');						 
					echo $this->Form->input('Projectvdo.language_id', array(
						'options' => $types,
						  'class' => 'form-control',
						  'label' => false,
					  ));
				?>
				  <br>
			  </td>
		  </tr>
			<tr>
			  <td>
			  <div class="form-group">
                                          <label class="col-sm-3 control-label">วันที่ลงข่าว </label>
                                                <div class="col-sm-7">
                                                   <div class="input-group date">                        
                                                  <?php 
                                                  echo $this->Form->input('Projectvdo.postdate', array(                          
                                                             'type' => 'text',
                                                             'label' => false,                                                        
                                                             'class' => array('form-control'),                                                           
                                                             'error' => false,
                                                             'required'
                                                  ));
                                                  ?>    
                                                  <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span> 
                                                                                                  
                                                    </div>     
                                             </div>
                                         </div>
			  </td>
			</tr>
			
			<tr>
			<td>
				  <label for="ProjectvdoDetail" class="addpost_input">หัวข้อวีดีโอ  <font color="red">(กรณีเลือกประเภทข่าวเป็นภาษาอังกฤษ กรุณาพิมพ์หัวข้อข่าว เป็นภาษาอังกฤษ)</font></label>
				<?php echo $this->Form->textarea('Projectvdo.Title',array('class'=>'ckeditor'))?>
			</td>
		</tr>
		  <tr>
			  <td>
				  <label for="ProjectvdoDetail" class="addpost_input">เนื้อหา <font color="red"> (กรณีเลือกประเภทข่าวเป็นภาษาอังกฤษ กรุณาพิมพ์เนื้อหาข่าว เป็นภาษาอังกฤษ)</font></label>
				<?php echo $this->Form->textarea('Projectvdo.Detail',array('class'=>'ckeditor','width' => '100%'))?>

			</td>
		  </tr>	
		<tr>
			  <td>
				<label class="col-md-12 control-label">ลิ้งค์จาก Youtube <font color="red">เอา https:// มาด้วย เช่น https://www.youtube.com/watch?v=8rRfqWcz-mw</font> </label>
					
						<?php 
								echo $this->Form->input('Projectvdo.link', array(
									'type' => 'text',
									'label' => false,
									'div' => false,
									'class' => array('form-control css-require'),                                                           
									'error' => false,
									'required'
									));
							
							?>
					
					
		   </td>
		  </tr>  
				<td>
					<div style="margin-top: 15px;">
						<a href="<?php echo $this->Html->url(array('action' => 'index')); ?>" class="btn btn-Projectvdo">
							<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
							กลับสู่เมนูหลัก
						</a>
						<input type="submit" class="btn btn-warning" value="แก้ไขข้อมูล" onclick="return confirm('ยืนบันการแก้ไขข้อมูล')">
					</div>
				</td>
			</tr>
		</table>
	</form>
</div> <!-- /container -->