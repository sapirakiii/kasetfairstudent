<div class="container">

  <form class="form-signin" method="post">
  
	<div style="text-align: center;">
		<img src="<?php echo $this->Html->url('/img/Logo_Thai.png'); ?>" class="img-responsive" style="display: block; width:80%; margin: auto;"/>
		<h2 class="form-signin-heading">เข้าสู่ระบบ</h2>
	</div>
	
	
	<input type="text" name="data[UserName]" class="form-control" placeholder="UserName" required autofocus>
	
	<input type="password" name="data[Password]" class="form-control" placeholder="Password" required>
	
	<button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
  </form>

</div> <!-- /container -->