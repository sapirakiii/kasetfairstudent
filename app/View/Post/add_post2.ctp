<?php //debug(WWW_ROOT.'files'.DS.'Document');?>


<div class="container" style="background-color: #FFFFFF!important;border-color: #FFFFFF!important">
	
	<form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">

		<table class="table table-hover table-post" style="width: 100%;" >
			
		  		  	  	
			<tr>
				<td>
					<label for="InfoDetail" class="addpost_input">อัปโหลดรูปภาพ และไฟล์เอกสารประกอบ</label> <font color="red">(กรณีเลือกมากกว่า 1 ไฟล์ ให้กด Ctrl หรือ Shift ค้างไว้และเลือกรูปภาพหรือไฟล์ที่ต้องการ)</font>
				    <?php
				        echo $this->Form->input('InfoDocument.files.', array('type' => 'file', 'multiple','accept' => '.xlsx,.xls,image/*, .doc, .docx, .ppt, .pptx, .txt, .pdf, .zip, .rar'));
				    ?>
				</td>
			</tr>
			<tr>
				<td>
					<div style="margin-top: 15px;">
						<a href="<?php echo $this->Html->url(array('action' => 'index')); ?>" class="btn btn-info">
							<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
							กลับสู่เมนูหลัก
						</a>
						<input type="submit" class="btn btn-success" value="เพิ่มข้อมูล" onclick="return confirm('ยืนบันการเพิ่มข้อมูล')">
					</div>
				</td>
			</tr>
		</table>
	</form>
	<div class="panel panel-default" style="border-color: #F2F2F2!important">
			<div class="panel-heading" style="background-color: #F2F2F2!important;border-color: #F2F2F2!important">
				<h3 class="panel-title">แสดงรูปภาพในหน้านี้</h3>
			</div>
			<div class="panel-body">
				<div class='list-group gallery'>
			
				   
					<table class="table table-hover table-post" style="width: 100%;">
		<tr>
		    <th>ลำดับ</th>
		    <th style="min-width: 350px;width: auto;">ชื่อรูป</th> 
		    <th>URL</th>
		
	  	</tr>
	  	<?php
				  
				   foreach ($infodocs as $infodoc) { 
					
					
					   ?>
	  	<tr>
			<td>
				
				<?php
					echo $infodoc['InfoDocument']['id'];
				?>
			</td>
			<td>
				<img class="img-responsive" alt="" 
				src="<?php echo $this->Html->url('/files/Document/').''. $infodoc['InfoDocument']['name'] ?>" width="20%"  />
			</td>
			<td>
				
				<?php echo 'http://www.kasetfair2017.agri.cmu.ac.th'.$this->Html->url('/files/Document/').''. $infodoc['InfoDocument']['name'] ?>
			</td>
	  	</tr>
				<?php } ?>
	</table> 
						
				</div>
					  
				
		</div>

	</div>

	</div>											  


</div> <!-- /container -->