<?php //debug($TypeNew);?>

<div class="container">
	
	<form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">

		<table class="table table-hover table-post" style="width: 100%;">
			<tr>
				<td>
					<?php	
						// if ($LevelApp['Level']['name'] == "admin") {
						?>
							<label for="EventDetail" class="addpost_input">หมวดหมู่</label><br>
						<?php
							echo $this->Form->hidden('Event.id');
		  					// echo $this->Form->input('Event.typenew_id', array(
			  				// 	'options' => $TypeNew,
							// 	'class' => 'form-control',
							// 	'label' => false,
							// ));
							echo 'ข่าวกิจกรรม';
		  				// }else{
		  				// 	echo $this->Form->hidden('Event.type_new_id', array(
		  				// 		'value' => $group_app['TypeNew']['id'],
		  				// 		'class' => 'form-control',
						// 	  ));
						// 	}
	  						?>
	  						
					
					<!--<label for="EventTInter">ภายใน</label>
						<?php 
							echo $this->Form->checkbox('Event.TInter', array(
							));
						?>
						<br><label for="EventTIntra">ภายนอก</label>-->
					<?php
							echo $this->Form->hidden('Event.TIntra', array(
								'value' => 1,
							));
					?>
				</td>
		  	</tr>
			  <!-- <?php 
								$adminposts = $this->Session->read('adminposts');
								if ($adminposts!=null) {          
								
									if (
									$adminposts['Adminpost']['employee_status_id'] == 2 ||
									$adminposts['Adminpost']['employee_status_id'] == 3) {
								
								?>
		  	<tr>
		  		<td>
			  		<label for="EventDetail" class="addpost_input">ข่าวเด่น</label>
					<?php 
						echo $this->Form->checkbox('Event.THilight', array(
						));
					?>
			  		<br>
			  	</td>
		  	</tr>
			  <?php 
								}
							} ?> -->
			
			<tr>
			  <td>
			  <div class="form-group">
                                          <label class="col-sm-3 control-label">วันที่กิจกรรมที่กำลังจะเกิดขึ้น </label>
                                                <div class="col-sm-7">
                                                   <div class="input-group date">                        
                                                  <?php 
                                                  echo $this->Form->input('Event.postdate', array(                          
                                                             'type' => 'text',
                                                             'label' => false,                                                        
                                                             'class' => array('form-control'),                                                           
                                                             'error' => false,
                                                             'required'
                                                  ));
                                                  ?>    
                                                  <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span> 
                                                                                                  
                                                    </div>     
                                             </div>
                                         </div>
			  </td>
			</tr>
			<tr>
				<td>
				<label class="col-sm-3 control-label">เวลาเริ่ม <font color="red">*</font> </label>
                                               <div class="col-sm-7">
                                                     <div class="form-group has-feedback">      
                                                        <?php 
                                                               $types = array('06:00:00' => '06:00',
                                                                              '06:30:00' => '06:30',
                                                                              '07:00:00' => '07:00',
                                                                              '07:30:00' => '07:30',
                                                                              '08:00:00' => '08:00',
                                                                              '08:30:00' => '08:30',
                                                                              '09:00:00' => '09:00',
                                                                              '09:30:00' => '09:30',
                                                                              '10:00:00' => '10:00',
                                                                              '10:30:00' => '10:30',
                                                                              '11:00:00' => '11:00',
                                                                              '11:30:00' => '11:30',
                                                                              '12:00:00' => '12:00',
                                                                              '12:30:00' => '12:30',
                                                                              '13:00:00' => '13:00',
                                                                              '13:30:00' => '13:30',
                                                                              '14:00:00' => '14:00',
                                                                              '14:30:00' => '14:30',
                                                                              '15:00:00' => '15:00',
                                                                              '15:30:00' => '15:30',
                                                                              '16:00:00' => '16:00',
                                                                              '16:30:00' => '16:30',
                                                                              '17:00:00' => '17:00',
                                                                              '17:30:00' => '17:30',
                                                                              '18:00:00' => '18:00',
                                                                              '18:30:00' => '18:30',
                                                                              '19:00:00' => '19:00',
                                                                              '19:30:00' => '19:30',
                                                                              '20:00:00' => '20:00',
                                                                              '20:30:00' => '20:30',
                                                                              '21:00:00' => '21:00',
                                                                              '21:30:00' => '21:30',
                                                                              '22:00:00' => '22:00',
                                                                              '22:30:00' => '22:30',
                                                                              '23:00:00' => '23:00',
                                                                              '23:30:00' => '23:30',
                                                                              '24:00:00' => '24:00');                                                       
                                                              echo $this->Form->input('Event.timestart', array(
																   'options' => $types,
																//   'type' => 'text',
                                                                  'label' => false,
                                                                  'div' => false,
                                                                 
                                                                  'class' => array('form-control css-require'),                                                           
                                                                  'error' => false,
                                                                  'required'
                                                                  ));
                                                          
                                                            ?>
                                                           
                                                  <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                </div>
                                            </div>
				</td>
			</tr>
			<tr>
				 <td>
				 <label class="col-sm-3 control-label">เวลาเสร็จสิ้น <font color="red">*</font> </label>
                                               <div class="col-sm-7">
                                                     <div class="form-group has-feedback">      
                                                        <?php 
                                                              $types = array('06:00:00' => '06:00',
																			'06:30:00' => '06:30',
																			'07:00:00' => '07:00',
																			'07:30:00' => '07:30',
																			'08:00:00' => '08:00',
																			'08:30:00' => '08:30',
																			'09:00:00' => '09:00',
																			'09:30:00' => '09:30',
																			'10:00:00' => '10:00',
																			'10:30:00' => '10:30',
																			'11:00:00' => '11:00',
																			'11:30:00' => '11:30',
																			'12:00:00' => '12:00',
																			'12:30:00' => '12:30',
																			'13:00:00' => '13:00',
																			'13:30:00' => '13:30',
																			'14:00:00' => '14:00',
																			'14:30:00' => '14:30',
																			'15:00:00' => '15:00',
																			'15:30:00' => '15:30',
																			'16:00:00' => '16:00',
																			'16:30:00' => '16:30',
																			'17:00:00' => '17:00',
																			'17:30:00' => '17:30',
																			'18:00:00' => '18:00',
																			'18:30:00' => '18:30',
																			'19:00:00' => '19:00',
																			'19:30:00' => '19:30',
																			'20:00:00' => '20:00',
																			'20:30:00' => '20:30',
																			'21:00:00' => '21:00',
																			'21:30:00' => '21:30',
																			'22:00:00' => '22:00',
																			'22:30:00' => '22:30',
																			'23:00:00' => '23:00',
																			'23:30:00' => '23:30',
																			'24:00:00' => '24:00'); 
                                                              echo $this->Form->input('Event.timeend', array(
                                                                   'options' => $types,
                                                                  'label' => false,
                                                                  'div' => false,
                                                                 
                                                                  'class' => array('form-control css-require'),                                                           
                                                                  'error' => false,
                                                                  'required'
                                                                  ));
                                                          
                                                            ?>
                                                            
                                                  <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                </div>
				 </td>
			</tr>
			<!-- <tr>
			  <td>
			  <div class="form-group">
                                          <label class="col-sm-3 control-label">กำหนดวันที่สิ้นสุด ไอคอน HOT!  <img src="<?php echo $this->Html->url('/img/post/new_dark.gif') ?>"></label>
                                                <div class="col-sm-7">
                                                   <div class="input-group date">                        
                                                  <?php 
                                                  echo $this->Form->input('Event.postend', array(                          
                                                             'type' => 'text',
                                                             'label' => false,                                                        
                                                             'class' => array('form-control'),                                                           
                                                             'error' => false,
                                                             'required'
                                                  ));
                                                  ?>    
                                                  <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span> 
                                                                                                  
                                                    </div>     
                                             </div>
                                         </div>
			  </td>
			</tr> -->
			<!-- <tr>
				<td>
					<label for="EventDetail" class="addpost_input">ภาพตัวอย่าง</label><br>
		  			<?php
		  				if ($LevelApp['Level']['name'] != "admin") {
		  					?>
		  						<div style="display: inline-block;margin:10px 10px 15px 15px">
			  					<input type="radio" name="data[Event][file_Picture]" id="EventFilePictureValue" value="<?php echo $group_app['TypeNew']['id'] ?>" checked>
								<img src="<?php echo $this->Html->url('/img/post/') , $group_app['TypeNew']['id'] , ".jpg" ?>" alt="" style="margin-left: 5px;margin-right:10px;"></div>
						<?php
		  				}else{
		  					for ($i=1; $i < 10; $i++) {
		  					?>
		  						<div style="display: inline-block;margin:10px 10px 15px 15px">
									<?php if ($i == 1){ ?>
			  							<input type="radio" name="data[Event][file_Picture_select]" id="EventFilePictureValue" value="<?php echo $i ;?>" checked>
									<?php 
										}else{ 
									?>
										<input type="radio" name="data[Event][file_Picture_select]" id="EventFilePictureValue" value="<?php echo $i ;?>">					
									<?php 
										} 
									?>
		  							<img src="<?php echo $this->Html->url('/img/post/') , $i , ".jpg" ?>" alt="" style="margin-left: 5px;margin-right:10px;">
								</div>
						<?php
		  					}
		  				}
		  				for ($i=1; $i < 10; $i++) {
	  						?>
	  						<div style="display: inline-block;margin:10px 10px 15px 15px">
								<?php if ($i == 1){ ?>
		  							<input type="radio" name="data[Event][file_Picture_select]" id="EventFilePictureValue" value="<?php echo $i ;?>" checked>
								<?php 
									}else{ 
								?>
									<input type="radio" name="data[Event][file_Picture_select]" id="EventFilePictureValue" value="<?php echo $i ;?>">					
								<?php 
									} 
								?>
	  							<img src="<?php echo $this->Html->url('/img/post/') , $i , ".jpg" ?>" alt="" style="margin-left: 5px;margin-right:10px;">
							</div>
							<?php
						}
		  					$i = 99;
		  				?>
		  				<div style="display: inline-block;margin:10px 10px 15px 15px">
	  						<input type="radio" name="data[Event][file_Picture_select]" id="EventFilePictureValue" value="<?php echo $i ?>"> อัปโหลดภาพตัวอย่าง
	  					<?php
				        	echo $this->Form->file('Event.file_Picture',array('type' => 'file','accept' => 'image/*'));
				    ?>
						</div>
				</td>
			</tr> -->
		  	<tr>
				<td>
		  			<label for="EventDetail" class="addpost_input">หัวข้อกิจกรรมที่กำลังจะเกิดขึ้น</label>
					  <?php 
				 
					 echo $this->Form->input('Event.Title', array(                          
								'type' => 'text',
								'label' => false,                                                        
								'class' => array('form-control'),                                                           
								'error' => false,
								'required'
					 ));
					
					?>
				</td>
			</tr>
		  	<tr>
		  		<td>
				  <label for="EventDetail" class="addpost_input">รายละเอียดโดยย่อของกิจกรรมที่กำลังจะเกิดขึ้น <font color="red">(ควรเพิ่มรายละเอียดโดยย่อ ไม่ควรมีเนื้อหายาวจนเกินไป เพื่อความสวยงามต่อการแสดงผล)</font> </label>
					<?php echo $this->Form->textarea('Event.Detail',array('class'=>'ckeditor','width' => '100%'))?>

				</td>
		  	</tr>
			  	
			<tr>
				<td>
					<div style="margin-top: 15px;">
						<a href="<?php echo $this->Html->url(array('action' => 'index')); ?>" class="btn btn-Event">
							<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
							กลับสู่เมนูหลัก
						</a>
						<input type="submit" class="btn btn-warning" value="แก้ไขข้อมูล" onclick="return confirm('ยืนยันการแก้ไขข้อมูล')">
					</div>
				</td>
			</tr>
		</table>
	</form>
</div> <!-- /container -->