<?php //debug($this->request->data);?>
<?php //debug($LevelApp);?>
<div class="container">
	

	<table class="table table-hover table-post" style="width: 100%;">
		<tr>
		    <th>Username</th> 
		    <th>Level</th>
		    <th>group</th>
	  	</tr>
	  	<form class="form-control" method="post">
	  	<?php
	  		echo $this->Form->input('LevelApp.id');
			echo $this->Form->hidden('LevelApp.employee_id'); 

		?>
	  	<tr>
			<td>
				<?php 
					echo $this->Form->input('Employee.UserName', array(
						'class' => 'form-control',
						'label' => false,
						'readonly'
					));
				?>
			</td>
				<?php 
					echo $this->Form->hidden('LevelApp.Application_id', array(
						'options' => $Application,
						'class' => 'form-control',
						'label' => false,
						'readonly'
					));
				?>
			<td>
				<?php
					echo $this->Form->input('LevelApp.level_id', array(
						'options' => $level,
						'class' => 'form-control',
						'label' => false,
						'readonly'
					));
				?>
			</td>
			<td>
				<?php
					echo $this->Form->input('EmployeeTypeNew.type_new_id', array(
						'options' => $TypeNew,
						'class' => 'form-control',
						'label' => false,
						'value' => $EmployeeTypeNew['EmployeeTypeNew']['type_new_id'],
						'readonly'
					));
				?>
			</td>
	  	</tr>

	  	
	</table>
	<a href="<?php echo $this->Html->url(array('action' => 'member')); ?>" class="btn btn-info">
		<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
		กลับสู่เมนูหลัก
	</a>
	<input type="submit" class="btn btn-danger" value="ลบข้อมูล" onclick="return confirm('ยืนบันการแก้ไขข้อมูล')">
	</form>
</div> <!-- /container -->