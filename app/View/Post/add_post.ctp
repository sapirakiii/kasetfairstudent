<?php //debug(WWW_ROOT.'files'.DS.'Document');?>


<div class="container" style="background-color: #FFFFFF!important;border-color: #FFFFFF!important">
	
	<form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">

		<table class="table table-hover table-post" style="width: 100%;" >
			<tr>
				<td>ผู้ดูแลระบบข่าวจากหน่วยงาน : <?php echo $listorganizes[0]['Organize']['name']?>
				</td>
			</tr>
			<tr>
				
				<td>
					<?php	
						// if ($LevelApp['Level']['name'] == "admin") {
						?>
							<label for="InfoDetail" class="addpost_input">หมวดหมู่</label><br>
							
							<select name="data[Info][typenew_id]" class="form-control js-example-basic-single" >
								<?php echo $output; ?>
							</select>
		
						<?php
		  					

						$name = $UserName['Employee']['fname']." ".$UserName['Employee']['lname'];
						echo $this->Form->hidden('Info.PostName', array(
		  						'value' => $name,
		  						'class' => 'form-control',
						));
					?>
					<!--<label for="InfoTInter">ภายใน</label>
						<?php 
							echo $this->Form->checkbox('Info.TInter', array(
							));
						?>
						<br><label for="InfoTIntra">ภายนอก</label>-->
					<?php
							echo $this->Form->hidden('Info.TIntra', array(
								'value' => 1,
							));
					?>
					<br>
				</td>
		  	</tr>
			  <?php 
								$adminposts = $this->Session->read('adminposts');
								if ($adminposts!=null) {          
								
									if (
									$adminposts['Adminpost']['employee_status_id'] == 2 ||
									$adminposts['Adminpost']['employee_status_id'] == 3) {
								
								?>
		  	<tr>
		  		<td>
			  		<label for="InfoDetail" class="addpost_input">แสดงในข่าวเด่นหรือไม่</label>
					<?php 
						echo $this->Form->checkbox('Info.THilight', array(
						));
					?>
			  		<br>
			  	</td>
		  	</tr>
			  <?php 
								}
							} ?>
			  <tr>
		  		<td>
			  		<label for="InfoDetail" class="addpost_input">ประเภทข่าว</label>
					<?php 
					 	$types = array('1' => 'ข่าวภาษาไทย (สำหรับแสดงเว็บไซต์ ภาษาไทย)', '2' => 'ข่าวภาษาอังกฤษ (สำหรับแสดงเว็บไซต์ ภาษาอังกฤษ)');						 
						echo $this->Form->input('Info.language_id', array(
							'options' => $types,
						  	'class' => 'form-control',
						  	'label' => false,
						  ));
					?>
			  		<br>
			  	</td>
		  	</tr>
			<tr>
			  <td>
			  <div class="form-group">
                                          <label class="col-sm-3 control-label">วันที่ลงข่าว </label>
                                                <div class="col-sm-7">
                                                   <div class="input-group date">                        
                                                  <?php 
                                                  echo $this->Form->input('Info.postdate', array(                          
                                                             'type' => 'text',
                                                             'label' => false,                                                        
                                                             'class' => array('form-control'),                                                           
                                                             'error' => false,
                                                             'required'
                                                  ));
                                                  ?>    
                                                  <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span> 
                                                                                                  
                                                    </div>     
                                             </div>
                                         </div>
			  </td>
			</tr>
			<tr>
			  <td>
			  <div class="form-group">
                                          <label class="col-sm-3 control-label">กำหนดวันที่สิ้นสุด ไอคอน HOT!  <img src="<?php echo $this->Html->url('/img/post/new_dark.gif') ?>"></label>
                                                <div class="col-sm-7">
                                                   <div class="input-group date">                        
                                                  <?php 
                                                  echo $this->Form->input('Info.postend', array(                          
                                                             'type' => 'text',
                                                             'label' => false,                                                        
                                                             'class' => array('form-control'),                                                           
                                                             'error' => false,
                                                             'required'
                                                  ));
                                                  ?>    
                                                  <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span> 
                                                                                                  
                                                    </div>     
                                             </div>
                                         </div>
			  </td>
			</tr>
			<!-- <tr>
				<td>
					<label for="InfoDetail" class="addpost_input">ภาพตัวอย่าง</label><br>
		  			<?php
		  				/*if ($LevelApp['Level']['name'] != "admin") {
		  					?>
		  						<div style="display: inline-block;margin:10px 10px 15px 15px">
			  					<input type="radio" name="data[Info][file_Picture]" id="InfoFilePictureValue" value="<?php echo $group_app['TypeNew']['id'] ?>" checked>
								<img src="<?php echo $this->Html->url('/img/post/') , $group_app['TypeNew']['id'] , ".jpg" ?>" alt="" style="margin-left: 5px;margin-right:10px;"></div>
						<?php
		  				}else{
		  					for ($i=1; $i < 10; $i++) {
		  					?>
		  						<div style="display: inline-block;margin:10px 10px 15px 15px">
									<?php if ($i == 1){ ?>
			  							<input type="radio" name="data[Info][file_Picture_select]" id="InfoFilePictureValue" value="<?php echo $i ?>" checked>
									<?php 
										}else{ 
									?>
										<input type="radio" name="data[Info][file_Picture_select]" id="InfoFilePictureValue" value="<?php echo $i ?>">					
									<?php 
										} 
									?>
		  							<img src="<?php echo $this->Html->url('/img/post/') , $i , ".jpg" ?>" alt="" style="margin-left: 5px;margin-right:10px;">
								</div>
						<?php
		  					}
		  				}*/
		  				for ($i=1; $i < 10; $i++) {
	  						?>
	  						<div style="display: inline-block;margin:10px 10px 15px 15px">
								<?php if ($i == 1){ ?>
		  							<input type="radio" name="data[Info][file_Picture_select]" id="InfoFilePictureValue" value="<?php echo $i ;?>" checked>
								<?php 
									}else{ 
								?>
									<input type="radio" name="data[Info][file_Picture_select]" id="InfoFilePictureValue" value="<?php echo $i ;?>">					
								<?php 
									} 
								?>
	  							<img src="<?php echo $this->Html->url('/img/post/') , $i , ".jpg" ?>" alt="" style="margin-left: 5px;margin-right:10px;">
							</div>
							<?php
						}
	  					$i = 99;
		  				?>
		  				<div style="display: inline-block;margin:10px 10px 15px 15px">
	  						<input type="radio" name="data[Info][file_Picture_select]" id="InfoFilePictureValue" value="<?php echo $i ?>"> อัปโหลดภาพตัวอย่าง
	  					<?php
				        	echo $this->Form->file('Info.file_Picture',array('type' => 'file','accept' => 'image/*'));
				    ?>
						</div>
				</td>
			</tr> -->
		  	<tr>
				<td>
		  			<label for="InfoDetail" class="addpost_input">หัวข้อข่าว  <font color="red">(กรณีเลือกประเภทข่าวเป็นภาษาอังกฤษ กรุณาพิมพ์หัวข้อข่าว เป็นภาษาอังกฤษ)</font></label>
					<?php echo $this->Form->textarea('Info.Title',array('class'=>'ckeditor'))?>
				</td>
			</tr>
		  	<tr>
		  		<td>
		  			<label for="InfoDetail" class="addpost_input">เนื้อหา <font color="red"> (กรณีเลือกประเภทข่าวเป็นภาษาอังกฤษ กรุณาพิมพ์เนื้อหาข่าว เป็นภาษาอังกฤษ)</font></label>
					<?php echo $this->Form->textarea('Info.Detail',array('class'=>'ckeditor','width' => '100%'))?>

				</td>
		  	</tr>	  	
			<tr>
				<td>
					<label for="InfoDetail" class="addpost_input">อัปโหลดรูปภาพ และไฟล์เอกสารประกอบ</label> <font color="red">(กรณีเลือกมากกว่า 1 ไฟล์ ให้กด Ctrl หรือ Shift ค้างไว้และเลือกรูปภาพหรือไฟล์ที่ต้องการ)</font>
				    <?php
				        echo $this->Form->input('InfoDocument.files.', array('type' => 'file', 'multiple','accept' => '.xlsx,.xls,image/*, .doc, .docx, .ppt, .pptx, .txt, .pdf, .zip, .rar'));
				    ?>
				</td>
			</tr>
			<tr>
				<td>
					<div style="margin-top: 15px;">
						<a href="<?php echo $this->Html->url(array('action' => 'index')); ?>" class="btn btn-info">
							<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
							กลับสู่เมนูหลัก
						</a>
						<input type="submit" class="btn btn-success" value="เพิ่มข้อมูล" onclick="return confirm('ยืนบันการเพิ่มข้อมูล')">
					</div>
				</td>
			</tr>
		</table>
	</form>
</div> <!-- /container -->