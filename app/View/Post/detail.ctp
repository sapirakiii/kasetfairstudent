<?php //debug($InfoDocument) ?>

<div class="container">
	<div class="panel panel-primary" style="border-color: #7a52b5!important">
		<div class="panel-heading" style="background-color: #7a52b5!important;border-color: #7a52b5!important">
			<h3 class="panel-title"><?php echo $new['Info']['Title'] ?></h3>
		</div>
		<div class="panel-body">
			<?php
				if($new['Info']['Detail']){
					?><p><?php echo $new['Info']['Detail']; ?></p><hr><br>
			<?php
				}
			?>
			<?php
				$img = "";
				$img2 = "";
				$img3 = 0;
				echo "<div class='list-group gallery'>";
				foreach ($new['InfoDocument'] as $InfoDocument) {
					if ($InfoDocument['class'] == "img") { ?>
					
            <div class='col-sm-12'>
                <a class="thumbnail fancybox" rel="ligthbox" href="<?php echo $this->Html->url('/files/Document/').''. $InfoDocument['name'] ?>">
                    <img class="img-responsive" alt="" src="<?php echo $this->Html->url('/files/Document/').''. $InfoDocument['name'] ?>" width="100%" />
                    <div class='text-right'>
                        <small class='text-muted'><?php //echo $new['Info']['Title']; ?></small>
                    </div> 
                </a>
            </div>
					
				<?php }
				}
				echo " </div>";
				echo "<br><hr>".$img2;
			?>
				<!-- col-sm-4 col-xs-6 col-md-3 col-lg-3	 -->
		</div>
		
		<hr>
		<div style="padding-left: 10px;">
		<b>เอกสารประกอบ :</b>
		<?php
				if ($new['InfoDocument'] == null) {
					echo 'ไม่มีเอกสารประกอบ';
				}else {
					foreach ($new['InfoDocument'] as $InfoDocument) {
						if ($InfoDocument['class'] == "file") { ?>
			
						<a href="<?php echo $this->Html->url('/files/Document/') , $InfoDocument['name']; ?>" class="btn btn-primary" target="_blank">Download <?php echo $InfoDocument['name_old']; ?> <span class="glyphicon glyphicon-cloud-download"></span></a>
			<?php
					}
				}
			}	
			?>
		</div>
		<hr>
		<div style="padding-left: 10px;">			
			<b>กลุ่มข่าว :</b> <?php echo $new['Typenew']['TypeNews'] ?><br>
			<b>วันที่เผยแพร่ :</b> <?php echo $new['Info']['created'] ?><br>
			<b>เผยแพร่โดย :</b> <?php echo $new['Info']['PostName'] ?><br>
			<b>เปิดอ่าน :</b> <?php echo $new['Info']['CountRead'] ?> ครั้ง<br></p>
		</div>
	</div>

</div>