<?php //debug($last_order); ?>
<div class="container">
	<?php
		echo $this->Form->create('Banner', array('type' => 'file'));
		echo $this->Form->input('Banner.name', array(
						'class' => 'form-control',
						'label' => 'ชื่อไฟล์',
					));
		echo $this->Form->input('Banner.url', array(
						'class' => 'form-control',
						'label' => 'ลิงค์ปลายทางที่ต้องการเชื่อมต่อ',
					));
		$last_order['0']['Banner']['order']++;
		echo $this->Form->hidden('Banner.order', array(
						'value' => $last_order['0']['Banner']['order'],
					));
		echo $this->Form->hidden('Banner.status', array(
						'value' => 1,
					));
	?>
	<div style="margin:15px;">โปรดกำหนดไฟล์ Banner ขนาด (1170x300 px) เพื่อพอดีกับหน้าจอ</div>
	<?php
		echo $this->Form->file('image_file', array('accept' => 'image/*'));
	?>
	<div style="margin:15px;"></div>
	<a href="<?php echo $this->Html->url(array('action' => 'banner')); ?>" class="btn btn-info">
		<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
		กลับสู่เมนูหลัก
	</a>
	<input type="submit" class="btn btn-success" value="เพิ่ม" onclick="return confirm('ยืนยันการกรอกข้อมูล')">
	<?php 
		echo $this->Form->end();
	?>

</div>

