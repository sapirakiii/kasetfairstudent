<?php //debug($Banner);?>
<div class="container">
	<a href="<?php echo $this->Html->url(array('action' => 'add_banner')); ?>" class="btn btn-success">
		<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
		เพิ่ม
	</a>
	<hr>

	<?php
		$last = count($Banners);
		$n = 1;
		foreach ($Banners as $Banner):

	?>
	<div class="banner col-md-1">
		<?php echo $n;$n++; ?>
	</div>
	<div class="banner col-md-9" id="<?php echo $Banner['Banner']['order']; ?>" >
	<?php echo $Banner['Banner']['name']; ?>
	<img class="img-responsive" alt="" src="<?php echo $this->Html->url('/img/banner/') , $Banner['Banner']['image_name']; ?>" width="100%"  />
						
	</div>
	<div class="col-md-2">
		<?php
			if ($Banner['Banner']['order'] != 1) {

				##### upper #####
				echo $this->Form->create('Banner',array('class' => 'form-horizontal')); 
				
				echo $this->Form->hidden('id',array('value' => $Banner['Banner']['id']));
				echo $this->Form->hidden('Banner.order', array('value' => $Banner['Banner']['order']));
				echo $this->Form->hidden('Banner.activity', array(
								'value' => 'plus',
							));
				##### upper #####
			
		?>
				<button formaction="banner/#<?php $Banner['Banner']['order']--; echo $Banner['Banner']['order']; $Banner['Banner']['order']++; ?>" type="submit" class="btn btn-info"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></button>

		<?php
				echo $this->Form->end();
			}
			#####	show/hide   ######
			echo $this->Form->create('Banner',array('class' => 'form-horizontal')); 
				
			echo $this->Form->hidden('id',array('value' => $Banner['Banner']['id']));
			echo $this->Form->hidden('Banner.activity', array(
							'value' => 'show',
						));
			#####	show/hide   ######

				
		?>
			<?php
				if ($Banner['Banner']['status'] == 0) {
					?><button type="submit" formaction="banner/#<?php echo $Banner['Banner']['order']; ?>" class="btn btn-primary" onclick="return confirm('ต้องการแก้ไขการแสดง')" ><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span><?php	
				}else{
					?><button type="submit" formaction="banner/#<?php echo $Banner['Banner']['order']; ?>" class="btn btn-default" onclick="return confirm('ต้องการแก้ไขการแสดง')" ><span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span><?php
				}


			?>
			</button>

		<?php
			echo $this->Form->end();
			#####	delete   ######
			echo $this->Form->create('Banner',array('class' => 'form-horizontal')); 
				
			echo $this->Form->hidden('id',array('value' => $Banner['Banner']['id']));
			echo $this->Form->hidden('image_name',array('value' => $Banner['Banner']['image_name']));
			echo $this->Form->hidden('Banner.order', array('value' => $Banner['Banner']['order']));
			echo $this->Form->hidden('Banner.activity', array(
							'value' => 'delete',
						));
			#####	delete   ######

				
		?>
			<button type="submit" formaction="banner/#<?php echo $Banner['Banner']['order']; ?>" class="btn btn-danger" onclick="return confirm('ยืนบันการลบข้อมูล')" ><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>

				
		<?php
			echo $this->Form->end();


			if ($Banner['Banner']['order'] != $last) {
				#####	dropdown   ######
				echo $this->Form->create('Banner',array('class' => 'form-horizontal')); 
				
				echo $this->Form->hidden('id',array('value' => $Banner['Banner']['id']));
				echo $this->Form->hidden('Banner.order', array('value' => $Banner['Banner']['order']));
				echo $this->Form->hidden('Banner.activity', array(
								'value' => 'bate',
							));
				#####	dropdown   ######
				
			?>
			<button type="submit" formaction="banner/#<?php echo $Banner['Banner']['order']; ?>" class="btn btn-warning"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></button>
				
			<?php
				echo $this->Form->end();
			}
		?>
	</div>

	<div style="clear:both"></div>
	<hr style="margin-bottom: 20px;">
	<?php endforeach; ?>

</div> <!-- /container -->