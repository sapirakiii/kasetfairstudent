<?php //debug($Employee) ?>
<script type="text/javascript">
function confirm_click()
{
return confirm("ยืนยันการลบข้อมูล ?");
}

</script>
<div class="container" style="padding:0 15px 0;">	
	<div class="row">
		<div class="col-md-12">
	<table class="table table-hover table-post" style="width: 100%;">
		<tr>
		    <th class="col-md-4">Name - Lastname</th> 
		    <!-- <th class="col-md-4">Level</th> -->
		    <th class="col-md-4">Group</th>
	  	</tr>
	  	<form class="form-control" method="post">
	  	<tr>
			<td >
          
				<select name="data[Adminpost][employee_id]" id="singleselect" class="form-control js-example-basic-single" >
					<?php echo $outputEmployee; ?>
				</select>
			</td>			
			<td>
                <?php 
                    $types = array('1' => 'กลุ่มข่าว', '2' => 'กลุ่มข่าว กิจกรรม ', '3' => 'กลุ่มข่าว กิจกรรม แบนเนอร์',);	
					echo $this->Form->input('Adminpost.employee_status_id', array(
						'options' => $types,
						'class' => 'form-control',
						'label' => false,
					));
				?>
			</td>
	  	</tr>
	</table>
	<a href="<?php echo $this->Html->url(array('action' => 'member')); ?>" class="btn btn-info">
		<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
		กลับสู่เมนูหลัก
	</a>
	<input type="submit" class="btn btn-success" value="เพิ่มสิทธิ์" onclick="return confirm('ยืนยันการให้สิทธิ์')">
	</form>
	</div>
	</div>

</div>
</div>
</div>
</div>

<div class="container">
<div class="row">
		<div class="col-md-12">
	
	<table class="table table-hover table-post table_center">
		<th>ID_	Employee</th>
		<th>Username</th>
		<th>Name - Lastname</th>
		<!-- <th>Level</th> -->
		<th>group</th>
		<th>แก้ไขสิทธิ์</th>
		<?php
			echo $output;
		?>
		<tr>
			<?php echo $this->Paginator->numbers(array(
		  			'before' => '<div class="pagination"><ul>',
				    'separator' => '',
				    'currentClass' => 'active',
				    'tag' => 'li class="btn btn-default"',
				    'after' => '</ul></div>'
			    )); 
			?>
		</tr>
	</table>
	<?php echo $this->Paginator->numbers(array(
  			'before' => '<div class="pagination"><ul>',
		    'separator' => '',
		    'currentClass' => 'active',
		    'tag' => 'li class="btn btn-default"',
		    'after' => '</ul></div>'
	    )); 
	?>

</div> <!-- /container -->
</div>
</div>