<?php //debug($Employee) ?>
<script type="text/javascript">
function confirm_click()
{
return confirm("ยืนยันการลบข้อมูล ?");
}

</script>
<div class="container">
	
	<a href="<?php echo $this->Html->url(array('action' => 'add_member')); ?>" class="btn btn-success">
		<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
		เพิ่ม
	</a>
	<hr>
	<a href="<?php echo $this->Html->url(array('action' => 'AddMemberStatus')); ?>" class="btn btn-success">
		<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
		เพิ่มกลุ่มการเข้าถึงข่าว
	</a>
	<hr>
	
	<table class="table table-hover table-post table_center">
		<th>ID_	Employee</th>
		<th>Username</th>
		<th>Name - Lastname</th>
		<!-- <th>Level</th> -->
		<th>group</th>
		<th>แก้ไขสิทธิ์</th>
		<?php
			echo $output;
		?>
		<tr>
			<?php echo $this->Paginator->numbers(array(
		  			'before' => '<div class="pagination"><ul>',
				    'separator' => '',
				    'currentClass' => 'active',
				    'tag' => 'li class="btn btn-default"',
				    'after' => '</ul></div>'
			    )); 
			?>
		</tr>
	</table>
	<?php echo $this->Paginator->numbers(array(
  			'before' => '<div class="pagination"><ul>',
		    'separator' => '',
		    'currentClass' => 'active',
		    'tag' => 'li class="btn btn-default"',
		    'after' => '</ul></div>'
	    )); 
	?>

</div> <!-- /container -->