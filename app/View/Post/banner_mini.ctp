<?php //debug($Banners);?>
<div class="container">
	<a href="<?php echo $this->Html->url(array('action' => 'add_banner_mini')); ?>" class="btn btn-success">
		<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
		เพิ่ม
	</a>
	<hr>

	<?php
		$last = count($Banners);
		foreach ($Banners as $Banner):
	?>
	<div class="banner col-lg-6 col-md-6 col-sm-4 col-xs-6" id="<?php echo $Banner['Banner']['order']; ?>" style="height: 50px;background-position: right;">
		<img src="<?php echo $this->Html->url('/img/banner/') , $Banner['Banner']['image_name']; ?>" alt="">
	</div>
	<div class="col-md-2">
		<?php
			if ($Banner['Banner']['order'] != 1) {

				##### upper #####
				echo $this->Form->create('Banner',array('class' => 'form-horizontal')); 
				
				echo $this->Form->hidden('id',array('value' => $Banner['Banner']['id']));
				echo $this->Form->hidden('Banner.order', array('value' => $Banner['Banner']['order']));
				echo $this->Form->hidden('Banner.activity', array(
								'value' => 'plus',
							));
				##### upper #####
			
		?>
				<button formaction="banner_mini/#<?php $Banner['Banner']['order']--; echo $Banner['Banner']['order']; $Banner['Banner']['order']++; ?>" type="submit" class="btn btn-info"><span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span></button>

		<?php
				echo $this->Form->end();
			}
			#####	show/hide   ######
			echo $this->Form->create('Banner',array('class' => 'form-horizontal')); 
				
			echo $this->Form->hidden('id',array('value' => $Banner['Banner']['id']));
			echo $this->Form->hidden('Banner.activity', array(
							'value' => 'show',
						));
			#####	show/hide   ######

				
		?>
			<?php
				if ($Banner['Banner']['status'] == 0) {
					?><button type="submit" formaction="banner_mini/#<?php echo $Banner['Banner']['order']; ?>" class="btn btn-primary" onclick="return confirm('ต้องการแก้ไขการแสดง')" ><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span><?php	
				}else{
					?><button type="submit" formaction="banner_mini/#<?php echo $Banner['Banner']['order']; ?>" class="btn btn-default" onclick="return confirm('ต้องการแก้ไขการแสดง')" ><span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span><?php
				}


			?>
			</button>

		<?php
			echo $this->Form->end();
			#####	delete   ######
			echo $this->Form->create('Banner',array('class' => 'form-horizontal')); 
				
			echo $this->Form->hidden('id',array('value' => $Banner['Banner']['id']));
			echo $this->Form->hidden('Banner.order', array('value' => $Banner['Banner']['order']));
			echo $this->Form->hidden('Banner.activity', array(
							'value' => 'delete',
						));
			#####	delete   ######

				
		?>
			<button type="submit" formaction="banner_mini/#<?php echo $Banner['Banner']['order']; ?>" class="btn btn-danger" onclick="return confirm('ยืนบันการลบข้อมูล')" ><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>

				
		<?php
			echo $this->Form->end();


			if ($Banner['Banner']['order'] != $last) {
				#####	dropdown   ######
				echo $this->Form->create('Banner',array('class' => 'form-horizontal')); 
				
				echo $this->Form->hidden('id',array('value' => $Banner['Banner']['id']));
				echo $this->Form->hidden('Banner.order', array('value' => $Banner['Banner']['order']));
				echo $this->Form->hidden('Banner.activity', array(
								'value' => 'bate',
							));
				#####	dropdown   ######
				
			?>
			<button type="submit" formaction="banner_mini/#<?php echo $Banner['Banner']['order']; ?>" class="btn btn-warning"><span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span></button>
				
			<?php
				echo $this->Form->end();
			}
		?>
	</div>
	<div style="clear:both"></div>
	<hr style="margin-bottom: 20px;">
	<?php endforeach; ?>

</div> <!-- /container -->