<?php //debug($this->request->data);?>
<?php //debug($Employees);?>
<div class="container" style="padding:0 15px 0;">	
	<div class="row">
		<div class="col-md-12">
	<table class="table table-hover table-post" style="width: 100%;">
		<tr>
		    <th class="col-md-4">Name - Lastname</th> 
		    <!-- <th class="col-md-4">Level</th> -->
		    <th class="col-md-4">Group</th>
	  	</tr>
	  	<form class="form-control" method="post">
	  	<tr>
			<td >
				<select name="data[Employee][id]" class="form-control js-example-basic-single" >
					<?php echo $output; ?>
				</select>
			</td>
				<?php 
					echo $this->Form->hidden('LevelApp.Application_id', array(
						'class' => 'form-control',
						'label' => false,
						'value' => 1,
						'readonly'
					));
				?>
			<!-- <td>
				<?php
					echo $this->Form->input('LevelApp.level_id', array(
						'class' => 'form-control',
						'label' => false,
					));
				?>
			</td> -->
			<td>
				<?php 
					echo $this->Form->input('EmployeeTypeNew.typenew_id', array(
						'options' => $TypeNew,
						'class' => 'form-control',
						'label' => false,
					));
				?>
			</td>
	  	</tr>
	</table>
	<a href="<?php echo $this->Html->url(array('action' => 'member')); ?>" class="btn btn-info">
		<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
		กลับสู่เมนูหลัก
	</a>
	<input type="submit" class="btn btn-success" value="เพิ่มสิทธิ์" onclick="return confirm('ยืนบันการให้สิทธิ์')">
	</form>
	</div>
	</div>

</div>
</div>

</div>
<div style="clear: both;"></div>