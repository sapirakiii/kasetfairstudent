<?php //debug($output);?>

<div class="container" style="background-color: #FFFFFF!important;border-color: #FFFFFF!important">
	
	<form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">

		<table class="table table-hover table-post" style="width: 100%;" >
			
			
			  
			  <!-- <tr>
		  		<td>
			  		<label for="EventDetail" class="addpost_input">ประเภทภาษา</label>
					<?php 
					 	$types = array('1' => 'วีดีโอภาษาไทย (สำหรับแสดงเว็บไซต์ ภาษาไทย)', '2' => 'วีดีโอภาษาอังกฤษ (สำหรับแสดงเว็บไซต์ ภาษาอังกฤษ)');						 
						echo $this->Form->input('Event.language_id', array(
							'options' => $types,
						  	'class' => 'form-control',
						  	'label' => false,
						  ));
					?>
			  		<br>
			  	</td>
		  	</tr> -->
			<tr>
			  <td>
			  <div class="form-group">
                      <label class="col-sm-3 control-label">วันที่กิจกรรมที่กำลังจะเกิดขึ้น </label>
                            <div class="col-sm-7">
                               <div class="input-group date">                        
                              <?php 
                              echo $this->Form->input('Event.postdate', array(                          
                                         'type' => 'text',
                                         'label' => false,                                                        
                                         'class' => array('form-control'),                                                           
                                         'error' => false,
                                         'required'
                              ));
                              ?>    
                              <span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span> 
                                                                              
                                </div>     
                         </div>
                     </div>
			  </td>
			</tr>
			<tr>
				<td>
				<label class="col-sm-3 control-label">เวลาเริ่ม <font color="red">*</font> </label>
                                               <div class="col-sm-7">
                                                     <div class="form-group has-feedback">      
                                                        <?php 
                                                               $types = array('06:00' => '06:00',
                                                                              '06:30' => '06:30',
                                                                              '07:00' => '07:00',
                                                                              '07:30' => '07:30',
                                                                              '08:00' => '08:00',
                                                                              '08:30' => '08:30',
                                                                              '09:00' => '09:00',
                                                                              '09:30' => '09:30',
                                                                              '10:00' => '10:00',
                                                                              '10:30' => '10:30',
                                                                              '11:00' => '11:00',
                                                                              '11:30' => '11:30',
                                                                              '12:00' => '12:00',
                                                                              '12:30' => '12:30',
                                                                              '13:00' => '13:00',
                                                                              '13:30' => '13:30',
                                                                              '14:00' => '14:00',
                                                                              '14:30' => '14:30',
                                                                              '15:00' => '15:00',
                                                                              '15:30' => '15:30',
                                                                              '16:00' => '16:00',
                                                                              '16:30' => '16:30',
                                                                              '17:00' => '17:00',
                                                                              '17:30' => '17:30',
                                                                              '18:00' => '18:00',
                                                                              '18:30' => '18:30',
                                                                              '19:00' => '19:00',
                                                                              '19:30' => '19:30',
                                                                              '20:00' => '20:00',
                                                                              '20:30' => '20:30',
                                                                              '21:00' => '21:00',
                                                                              '21:30' => '21:30',
                                                                              '22:00' => '22:00',
                                                                              '22:30' => '22:30',
                                                                              '23:00' => '23:00',
                                                                              '23:30' => '23:30',
                                                                              '24:00' => '24:00');                                                       
                                                              echo $this->Form->input('Event.timestart', array(
                                                                  'options' => $types,
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'value' => '00:00',
                                                                  'class' => array('form-control css-require'),                                                           
                                                                  'error' => false,
                                                                  'required'
                                                                  ));
                                                          
                                                            ?>
                                                           
                                                  <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                </div>
                                            </div>
				</td>
			</tr>
			<tr>
				 <td>
				 <label class="col-sm-3 control-label">เวลาเสร็จสิ้น <font color="red">*</font> </label>
                                               <div class="col-sm-7">
                                                     <div class="form-group has-feedback">      
                                                        <?php 
                                                              $types = array('06:00' => '06:00',
                                                                              '06:30' => '06:30',
                                                                              '07:00' => '07:00',
                                                                              '07:30' => '07:30',
                                                                              '08:00' => '08:00',
                                                                              '08:30' => '08:30',
                                                                              '09:00' => '09:00',
                                                                              '09:30' => '09:30',
                                                                              '10:00' => '10:00',
                                                                              '10:30' => '10:30',
                                                                              '11:00' => '11:00',
                                                                              '11:30' => '11:30',
                                                                              '12:00' => '12:00',
                                                                              '12:30' => '12:30',
                                                                              '13:00' => '13:00',
                                                                              '13:30' => '13:30',
                                                                              '14:00' => '14:00',
                                                                              '14:30' => '14:30',
                                                                              '15:00' => '15:00',
                                                                              '15:30' => '15:30',
                                                                              '16:00' => '16:00',
                                                                              '16:30' => '16:30',
                                                                              '17:00' => '17:00',
                                                                              '17:30' => '17:30',
                                                                              '18:00' => '18:00',
                                                                              '18:30' => '18:30',
                                                                              '19:00' => '19:00',
                                                                              '19:30' => '19:30',
                                                                              '20:00' => '20:00',
                                                                              '20:30' => '20:30',
                                                                              '21:00' => '21:00',
                                                                              '21:30' => '21:30',
                                                                              '22:00' => '22:00',
                                                                              '22:30' => '22:30',
                                                                              '23:00' => '23:00',
                                                                              '23:30' => '23:30',
                                                                              '24:00' => '24:00'); 
                                                              echo $this->Form->input('Event.timeend', array(
                                                                   'options' => $types,
                                                                  'label' => false,
                                                                  'div' => false,
                                                                  'value' => '00:00',
                                                                  'class' => array('form-control css-require'),                                                           
                                                                  'error' => false,
                                                                  'required'
                                                                  ));
                                                          
                                                            ?>
                                                            
                                                  <span class="glyphicon form-control-feedback" aria-hidden="true"></span>
                                                </div>
				 </td>
			</tr>
		  	<tr>
				<td>
		  			<label for="EventDetail" class="addpost_input">หัวข้อกิจกรรมที่กำลังจะเกิดขึ้น  </label>
					<?php 
				 
					 echo $this->Form->input('Event.Title', array(                          
								'type' => 'text',
								'label' => false,                                                        
								'class' => array('form-control'),                                                           
								'error' => false,
								'required'
					 ));
					
					?>
				</td>
			</tr>			
		  	<tr>
		  		<td>
		  			<label for="EventDetail" class="addpost_input">รายละเอียดโดยย่อของกิจกรรมที่กำลังจะเกิดขึ้น <font color="red">(ควรเพิ่มรายละเอียดโดยย่อ ไม่ควรมีเนื้อหายาวจนเกินไป เพื่อความสวยงามต่อการแสดงผล)</font> </label>
					<?php echo $this->Form->textarea('Event.Detail',array('class'=>'ckeditor','width' => '100%'))?>

				</td>
		  	</tr>
			
			<!-- <tr>
		  		<td>
					<label class="col-md-12 control-label">ลิ้งค์จาก Youtube <font color="red">เอา https:// มาด้วย เช่น https://www.youtube.com/watch?v=8rRfqWcz-mw</font> </label>
					    
							<?php 
									echo $this->Form->input('Event.link', array(
										'type' => 'text',
										'label' => false,
										'div' => false,
										'class' => array('form-control css-require'),                                                           
										'error' => false,
										'required'
										));
								
								?>
						
						
			   </td>
		  	</tr>  	 -->
			
			<tr>
				<td>
					<div style="margin-top: 15px;">
						
						<input type="submit" class="btn btn-success" value="เพิ่มข้อมูล" onclick="return confirm('ยืนยันการเพิ่มข้อมูล')">
					</div>
				</td>
			</tr>
		</table>
	</form>
</div> <!-- /container -->