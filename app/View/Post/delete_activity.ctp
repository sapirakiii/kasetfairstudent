<?php //debug($UserName);?>

<div class="container">
	
	<form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8">

		<table class="table table-hover table-post" style="width: 100%;">
			<tr>
				<td>
					<?php	
						if ($LevelApp['Level']['name'] == "admin") {
						?>
							<label for="InfoDetail" class="addpost_input">หมวดหมู่</label><br>
						<?php
							echo $this->Form->hidden('Info.id');
		  					echo $this->Form->input('Info.type_new_id', array(
			  					'options' => $TypeNew,
								'class' => 'form-control',
								'label' => false,
								'readonly'
							));
		  				}else{
		  					echo $this->Form->hidden('Info.type_new_id', array(
		  						'value' => $group_app['TypeNew']['id'],
		  						'class' => 'form-control',
		  						'readonly'
	  						));
	  						?>
	  						
					<?php
		  				}

						$name = $UserName['Employee']['fname']." ".$UserName['Employee']['lname'];
						echo $this->Form->hidden('Info.PostName', array(
		  						'value' => $name,
		  						'class' => 'form-control',
		  						'readonly'
						));
					?>
					<!--<label for="InfoTInter">ภายใน</label>
						<?php 
							echo $this->Form->checkbox('Info.TInter', array(
							));
						?>
						<br><label for="InfoTIntra">ภายนอก</label>-->
					<?php
							echo $this->Form->hidden('Info.TIntra', array(
								'value' => 1,
							));
					?>
				</td>
		  	</tr>
		  	<tr>
		  		<td>
			  		<label for="InfoDetail" class="addpost_input">ข่าวเด่น</label>
					<?php 
						echo $this->Form->checkbox('Info.THilight', array(
							'readonly'
						));
					?>
			  		<br>
			  	</td>
		  	</tr>
			<tr>
				<td>
					<label for="InfoDetail" class="addpost_input">ภาพตัวอย่าง</label><br>
		  			<?php
		  				/*if ($LevelApp['Level']['name'] != "admin") {
		  					?>
		  						<div style="display: inline-block;margin:10px 10px 15px 15px">
			  					<input type="radio" name="data[Info][file_Picture]" id="InfoFilePictureValue" value="<?php echo $group_app['TypeNew']['id'] ?>" checked>
								<img src="<?php echo $this->Html->url('/img/post/') , $group_app['TypeNew']['id'] , ".jpg" ?>" alt="" style="margin-left: 5px;margin-right:10px;"></div>
						<?php
		  				}else{
		  					for ($i=1; $i < 10; $i++) {
		  					?>
		  						<div style="display: inline-block;margin:10px 10px 15px 15px">
									<?php if ($i == 1){ ?>
			  							<input type="radio" name="data[Info][file_Picture_select]" id="InfoFilePictureValue" value="<?php echo $i ;?>" checked>
									<?php 
										}else{ 
									?>
										<input type="radio" name="data[Info][file_Picture_select]" id="InfoFilePictureValue" value="<?php echo $i ;?>">					
									<?php 
										} 
									?>
		  							<img src="<?php echo $this->Html->url('/img/post/') , $i , ".jpg" ?>" alt="" style="margin-left: 5px;margin-right:10px;">
								</div>
						<?php
		  					}
		  				}*/
		  				for ($i=1; $i < 10; $i++) {
	  						?>
	  						<div style="display: inline-block;margin:10px 10px 15px 15px">
								<?php if ($i == 1){ ?>
		  							<input type="radio" name="data[Info][file_Picture_select]" id="InfoFilePictureValue" value="<?php echo $i ;?>" checked>
								<?php 
									}else{ 
								?>
									<input type="radio" name="data[Info][file_Picture_select]" id="InfoFilePictureValue" value="<?php echo $i ;?>">					
								<?php 
									} 
								?>
	  							<img src="<?php echo $this->Html->url('/img/post/') , $i , ".jpg" ?>" alt="" style="margin-left: 5px;margin-right:10px;">
							</div>
							<?php
						}
				    ?>
				</td>
			</tr>
		  	<tr>
				<td>
		  			<label for="InfoDetail" class="addpost_input">ชื่อเรื่อง</label>
					<?php echo $this->Form->textarea('Info.Title',array('class'=>'ckeditor','readonly'))?>
				</td>
			</tr>
		  	<tr>
		  		<td>
		  			<label for="InfoDetail" class="addpost_input">เนื้อหา</label>
					<?php echo $this->Form->textarea('Info.Detail',array('class'=>'ckeditor','width' => '100%','readonly'))?>

				</td>
		  	</tr>	  	
			<tr>
			<tr>
				<td>
					<div style="margin-top: 15px;">
						<a href="<?php echo $this->Html->url(array('action' => 'index')); ?>" class="btn btn-info">
							<span class="glyphicon glyphicon-home" aria-hidden="true"></span>
							กลับสู่เมนูหลัก
						</a>
						<input type="submit" class="btn btn-danger" value="ลบข้อมูล" onclick="return confirm('ยืนบันการลบข้อมูล')">
					</div>
				</td>
			</tr>
		</table>
	</form>
</div> <!-- /container -->