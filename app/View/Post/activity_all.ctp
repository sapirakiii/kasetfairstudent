<?php //debug($news);?>
<script type="text/javascript">
function confirm_click()
{
return confirm("ยืนยันการลบข้อมูล ?");
}

</script>
<div class="container" style="background-color: #FFFFFF!important;border-color: #FFFFFF!important">

	<a href="<?php echo $this->Html->url(array('action' => 'AddActivity',1)); ?>" class="btn btn-success">
		<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
		เพิ่ม
	</a>
	<table class="table table-hover table-post" style="width: 100%;">
		<tr>
		    <th>ลำดับ</th>
		    <th style="min-width: 350px;width: auto;">หัวข้อข่าว</th> 
		    <!--<th>ประเภท</th>-->
		    <!--<th>ทั่วไป</th>
		    <th>ภายใน</th>-->
		   
		    <th>อ่าน</th>
		    <th>วันที่เผยแพร่</th>
		    <!-- <th>ข่าวเด่น</th> -->
		    <th>ข่าวของ</th>
			<th>language id</th>
		    <th>แก้ไข/ลบ</th>
	  	</tr>
	  	
	  	<?php
			foreach ($news as $new):
		?>
	  	<tr>
			<td>
				
				<?php
					echo $new['Info']['id'];
				?>
			</td>
			<td>
				<a href="<?php echo $this->Html->url(array('controller' => 'main','action' => 'InfoActivityDetail',$new['Info']['id'])); ?>" target="_blank">
					<?php
			  			echo $new['Info']['Title'];
					?>
				</a>
			</td>
			<!--<td>
				<?php
					if ($new['Info']['TNews'] == '0') {
						$new['Info']['TNews'] = "เนื้อหา";
					}
					elseif ($new['Info']['TNews'] == '1') {
						$new['Info']['TNews'] = "ลิงค์";
					}
					elseif ($new['Info']['TNews'] == '2') {
						$new['Info']['TNews'] = "โปรเตอร์";
					}
		  			echo $new['Info']['TNews'];
				?>
			</td>-->
			<!--<td>
				<?php
					if ($new['Info']['TInter'] == "Y" or $new['Info']['TInter'] == "1") {
						$new['Info']['TInter'] = "ใช้";
					}else{
						$new['Info']['TInter'] = "";
					}
					echo $new['Info']['TInter'];
				?>
			</td>
			<td>
				<?php
					if ($new['Info']['TIntra'] == "Y" or $new['Info']['TIntra'] == "1") {
						$new['Info']['TIntra'] = "ใช้";
					}
					else{
						$new['Info']['TIntra'] = "";
					}
					echo $new['Info']['TIntra'];

				?>
			</td>-->
			
			<td>
				<?php
					echo $new['Info']['CountRead'];
				?>
			</td>
			<td>
				<?php
					echo $new['Info']['postdate'];
					
				?>
			</td>
			<!-- <td>
				<?php
					if ($new['Info']['THilight'] == "1") {
						$new['Info']['THilight'] = "ข่าวเด่น";
					}elseif ($new['Info']['THilight'] == "0"){
						$new['Info']['THilight'] = " ";
					}
					echo $new['Info']['THilight'];
				?>
			</td> -->
			<td>
				<?php
					echo $new['Typenew']['TypeNews'];
				?>
			</td>
			<td>
				<?php
					echo $new['Info']['language_id'];
				?>
			</td>
			<td>
				<a href="<?php echo $this->Html->url(array('action' => 'edit_activity',$new['Info']['id'])); ?>" class="btn btn-warning">
					<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
					แก้ไข
				</a>
				<a href="<?php echo $this->Html->url(array('action' => 'delete_activity',$new['Info']['id'])); ?>" class="btn btn-danger" onclick="return confirm_click();">
					<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
					ลบ
				</a>
			</td>
	  	</tr>
	  	<?php	
			endforeach;
		?>
	  	<tr>
	  		<?php echo $this->Paginator->numbers(array(
		  			'before' => '<div class="pagination"><ul>',
				    'separator' => '',
				    'currentClass' => 'active',
				    'tag' => 'li class="btn btn-default"',
				    'after' => '</ul></div>'
			    )); 
			?>
	  	</tr>
	</table>
	<?php 
		echo $this->Paginator->numbers(array(
			'before' => '<div class="pagination"><ul>',
		    'separator' => '',
		    'currentClass' => 'active',
		    'tag' => 'li class="btn btn-default"',
		    'after' => '</ul></div>'
		)); 
	?>
	<hr>

</div> <!-- /container -->