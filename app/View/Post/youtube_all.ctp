<?php //debug($Projectvdos);?>
<script type="text/javascript">
function confirm_click()
{
return confirm("ยืนยันการลบข้อมูล ?");
}

</script>
<div class="container">

	<a href="<?php echo $this->Html->url(array('action' => 'AddProjectvdo')); ?>" class="btn btn-success">
		<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
		เพิ่ม
	</a>
	<table class="table table-hover table-post" style="width: 100%;">
		<tr>
		    <th>ลำดับ</th>
		    <th style="min-width: 350px;width: auto;">หัวข้อข่าว</th> 
		    <!--<th>ประเภท</th>-->
		    <!--<th>ทั่วไป</th>
		    <th>ภายใน</th>-->
		   
		    <th>รายละเอียด</th>
		    <th>วันที่เผยแพร่</th>
		    <!-- <th>ข่าวเด่น</th> -->
		    <!-- <th>ข่าวของ</th> -->
			<th>language id</th>
		    <th>แก้ไข/ลบ</th>
	  	</tr>
	  	
	  	<?php
			foreach ($Projectvdos as $Projectvdo):
		?>
	  	<tr>
			<td>
				
				<?php
					echo $Projectvdo['Projectvdo']['id'];
				?>
			</td>
			<td>
				<a href="<?php echo $this->Html->url(array('action' => 'detail',$Projectvdo['Projectvdo']['id'])); ?>" target="_blank">
					<?php
			  			echo $Projectvdo['Projectvdo']['Title'];
					?>
				</a>
			</td>
			<td>
				<?php
					echo $Projectvdo['Projectvdo']['Detail'];
				?>
			</td>
			<td>
				<?php
					echo $Projectvdo['Projectvdo']['postdate'];
					
				?>
			</td>
			
			<td>
				<?php
					echo $Projectvdo['Projectvdo']['language_id'];
				?>
			</td>
			<td>
				<a href="<?php echo $this->Html->url(array('action' => 'EditProjectvdo',$Projectvdo['Projectvdo']['id'])); ?>" class="btn btn-warning">
					<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
					แก้ไข
				</a>
				<a href="<?php echo $this->Html->url(array('action' => 'DeleteProjectvdo',$Projectvdo['Projectvdo']['id'])); ?>" class="btn btn-danger" onclick="return confirm_click();">
					<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
					ลบ
				</a>
			</td>
	  	</tr>
	  	<?php	
			endforeach;
		?>
	  	<tr>
	  		<?php echo $this->Paginator->numbers(array(
		  			'before' => '<div class="pagination"><ul>',
				    'separator' => '',
				    'currentClass' => 'active',
				    'tag' => 'li class="btn btn-default"',
				    'after' => '</ul></div>'
			    )); 
			?>
	  	</tr>
	</table>
	<?php 
		echo $this->Paginator->numbers(array(
			'before' => '<div class="pagination"><ul>',
		    'separator' => '',
		    'currentClass' => 'active',
		    'tag' => 'li class="btn btn-default"',
		    'after' => '</ul></div>'
		)); 
	?>
	<hr>

</div> <!-- /container -->