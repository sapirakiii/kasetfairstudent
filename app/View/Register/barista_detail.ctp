<div class="container" style="padding:15px 0 0; width:60%;">
    <div class="row">
        <div class="col-md-12">
            <div class="panel-title">
                <h3 style="margin:0px !important;">
                 <center><h3><b> การแข่งขันประกวด Latte art smack down งานวันเกษตรภาคเหนือ ครั้งที่ 10  </b></h3> </center>
                </h3> 
            </div>

            <div align="center">
                <a class="btn btn-lg btn btn-secondary" href="<?php echo $this->Html->url(array('action' => 'barista_list')); ?>" role="button">กลับ</a>								
            </div><br />

           
                <table class="table table-bordered" width="100%" >
                    <tr>
                        <td class="border border-dark p-2 text-dark" align="right" bgcolor="#f7f1b9" width="20%">
                            <strong>ชื่อ :</strong>
                        </td>
                        <td class="border border-dark text-dark"  >
                            <?php echo $barista['Barista']['Fname']." ".$barista['Barista']['Lname'] ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="border border-dark p-2 text-dark" align="right" bgcolor="#f7f1b9" width="20%">
                            <strong>ชื่อเล่น :</strong>
                        </td>
                        <td class="border border-dark text-dark"  >
                            <?php echo $barista['Barista']['Nname']?>
                        </td>
                    </tr>
                    <tr>
                        <td class="border border-dark p-2 text-dark" align="right" bgcolor="#f7f1b9" width="20%">
                            <strong>อายุ :</strong>
                        </td>
                        <td class="border border-dark text-dark"  >
                            <?php echo $barista['Barista']['age']?>
                        </td>
                    </tr>
                    <tr>
                        <td class="border border-dark p-2 text-dark" align="right" bgcolor="#f7f1b9" width="20%">
                            <strong>สังกัด/ร้านที่ทำงานอยู่ :</strong>
                        </td>
                        <td class="border border-dark text-dark"  >
                            <?php echo $barista['Barista']['affiliation']?>
                        </td>
                    </tr>
                    <tr>
                        <td class="border border-dark p-2 text-dark" align="right" bgcolor="#f7f1b9" width="20%">
                            <strong>เบอร์โทรติดต่อ :</strong>
                        </td>
                        <td class="border border-dark text-dark"  >
                            <?php echo $barista['Barista']['phone']?>
                        </td>
                    </tr>
                    <tr>
                        <td class="border border-dark p-2 text-dark" align="right" bgcolor="#f7f1b9" width="20%">
                            <strong>Line ID :</strong>
                        </td>
                        <td class="border border-dark text-dark"  >
                            <?php echo $barista['Barista']['line']?>
                        </td>
                    </tr>
                    <?php if($baristadocument != null){?>
                    <tr>
                        <td class="border border-dark p-2 text-dark" align="right" bgcolor="#f7f1b9" width="20%">
                            <strong>รูป :</strong>
                        </td>
                        <td class="border border-dark text-dark"  >
                            <?php foreach ($baristadocument as $key) { ?>
                                <a href="<?php echo $this->Html->url('/files/barista/'.$barista['Barista']['id'].'/'.$key['BaristaDocument']['name']); ?>" target="_blank">
                                    <img src="<?php echo $this->Html->url('/files/barista/'.$barista['Barista']['id'].'/'.$key['BaristaDocument']['name']); ?>" width="50%">
                                </a>
                                
                            <?php } ?>
                        </td>
                    </tr>
                    <?php } else{ ?>
                        <td class="border border-dark p-2 text-dark" align="right" bgcolor="#f7f1b9" width="20%">
                            <strong>รูป :</strong>
                        </td>
                        <td class="border border-dark text-dark"  >
                            &nbsp;&nbsp;&nbsp;&nbsp;-
                        </td>
                    <?php } ?>
                </table>
            
         
        </div>
    </div>
</div>