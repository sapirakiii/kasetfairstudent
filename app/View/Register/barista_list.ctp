<div class="container" style="padding:15px 0 0; width:60%;">
    <div class="row">
        <div class="col-md-12">
            <div class="panel-title">
                <h3 style="margin:0px !important;">
                   
                    <center>
                        <img src="<?php echo $this->Html->url('/img/เชิญเข้าร่วมแข่งขัน (5).jpg'); ?>" width="50%">
                    </center><br />
                </h3> 
            </div>

            <div align="center">
                <center><h3><b> การแข่งขันประกวด Latte art smack down งานวันเกษตรภาคเหนือ ครั้งที่ 10  </b></h3> </center>
                <?php if ($countbaristas >= $max_barista) { ?> 
                    <button class="btn btn-danger " style="cursor: none;">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 
                        ปิดรับการลงทะเบียน 					
                        
                    </button>
                    <center><h3><font color="green"> จำนวนที่นั่งการลงทะเบียนเต็มเรียบร้อยแล้ว </font> <br>   คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่ ขอขอบพระคุณทุกท่านที่ให้ความสนใจมา ณ โอกาสนี้ </h3>
                    </center>
                <?php } else { ?>
                    
                    <a class="btn btn-lg btn-primary" href="<?php echo $this->Html->url(array('action' => 'barista_register')); ?>" role="button" target="_blank">ลงทะเบียนการเเข่งขันบาริสต้า</a>								
                    <center><h3>
                        เปิดระบบรับจำนวนผู้ลงทะเบียนสำรอง <br>
                        ระบบเหลือจำนวนที่นั่งลงทะเบียนสำรอง <font color="red"><b> <?php echo ($max_barista - $countbaristas)  ?></b></font> ที่นั่งสุดท้าย <img src="https://www.agri.cmu.ac.th/smart_academic/img/post/new_dark.gif"> </h3>
                    </center>
                <?php } ?>
            </div><br />

            
                <table class="table table-bordered table-striped">
                    <thead style="text-align:center">
                        <th>ลำดับ</th>
                        <th>ชื่อ</th>
                        <th>สังกัด/ร้านที่ทำงานอยู่</th>
                    </thead>
                    <?php
                        $i = 0;
                        foreach($baristas as $barista) {
                        $i++;
                    ?>
                    <?php  if($barista['Barista']['level'] == 2) { ?>
                        <tr class="warning">
                    <?php }else { ?>
                        <tr >
                    <?php } ?>
                     
                        <td width=10%><?php echo $i; ?></td>
                        <td width=50%>
                            <?php  if($UserName != null) { ?> 
                                <a href="<?php echo $this->Html->url(array('action' => 'barista_detail',$barista['Barista']['id'])); ?>"><?php echo $barista['Barista']['Fname']." ".$barista['Barista']['Lname'] ?></a>
                            <?php }else { ?>
                                <?php echo  $barista['Barista']['Fname']." ".$barista['Barista']['Lname'] ?>
                     
                            <?php } ?>
                        </td>
                        <td><?php echo $barista['Barista']['affiliation'] ?></td>
                    </tr>
                    <?php } ?>
                </table>
             
         
        </div>
    </div>
</div>