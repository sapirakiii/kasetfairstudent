<div class="container" style="padding:15px 0 0; width:65%;">
    <div class="row">
        <h3 style="margin:0px !important;">
            <center><b> ลงทะเบียนการแข่งขันประกวด Latte art smack down <br> งานวันเกษตรภาคเหนือ ครั้งที่ 10 <br>
                วันที่ 10 ธันวาคม 2565 เวลา 09.00-12.00 น.  <br>
                ชิงเงินรางวัลมากกว่า 22,000 บาท ใบประกาศและโล่ห์รางวัล <br>
                ณ ศูนย์วิจัย สาธิต และฝึกอบรมการเกษตรแม่เหียะ
                </b>
            </center>
        </h3>
        <hr>
        <?php 
            $required = 'กรุณากรอกข้อมูล'; 
            // debug($this->request->data);
        ?>
        <?php if ($countbaristas >= $max_barista) { ?> 
            <button class="btn btn-danger " style="cursor: none;">
                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> 
                ปิดรับการลงทะเบียน					
                
            </button>
            <center><h3> คณะเกษตรศาสตร์ มหาวิทยาลัยเชียงใหม่ ขอขอบพระคุณทุกท่านที่ให้ความสนใจมา ณ โอกาสนี้ </h3>
            </center>
        <?php } else { ?>
            
            <form multiple="multiple" enctype="multipart/form-data" method="post" accept-charset="utf-8"> 
    
                <div class="row">
                        <div class="form-group col-md-4">
                        <label>1) คำนำหน้า </label>
                        <?php
                            echo $this->Form->input('prefix_id', array(
                                'options' => $prefixes,
                                'class' => 'form-control',
                                'label' => false,
                                'empty' => 'คำนำหน้า'
                            ));
                        ?>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label>2) ชื่อ </label>
                        <?php echo $this->Form->input('Fname', array(
                            'type' => 'text',
                            'label' => false,
                            'class' => 'form-control',
                            'required' => true
                        )); ?>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label>3) นามสกุล </label>
                        <?php echo $this->Form->input('Lname', array(
                            'type' => 'text',
                            'label' => false,
                            'class' => 'form-control',
                            'required' => true
                        )); ?>
                        <div class="help-block with-errors"></div>					
                    </div>
                </div>
    
                <div class="row" style="margin-top:20px; margin-bottom:20px">
                    <div class="form-group col-md-3">
                        <label>4) ชื่อเล่น </label>
                        <?php echo $this->Form->input('Nname', array(
                            'type' => 'text',
                            'label' => false,
                            'class' => 'form-control',
                            'required' => false
                        )); ?>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-md-3" style="margin:0px">
                        <label>5) อายุ </label>
                        <?php echo $this->Form->input('age', array(
                            'type' => 'number',
                            'label' => false,
                            'class' => 'form-control',
                            'required' => false
                        )); ?>
                        <div class="help-block with-errors"></div>					
                    </div>
                    <div class="form-group col-md-6" >
                        <label>6) สังกัด/ร้านที่ทำงานอยู่</label>
                        <?php echo $this->Form->input('affiliation', array(
                            'type' => 'text',
                            'label' => false,
                            'class' => 'form-control',
                            'required' => true
                        )); ?>
                        <div class="help-block with-errors"></div>					
                    </div>
                </div>
    
                <div class="row">
                    <div class="form-group col-md-6">
                        <label>7) เบอร์โทรติดต่อ </label>
                        <?php echo $this->Form->input('phone', array(
                            'type' => 'text',
                            'label' => false,
                            'class' => 'form-control',
                            'required' => true
                        )); ?>
                        <div class="help-block with-errors"></div>
                    </div>
                    <div class="form-group col-md-6">
                        <label>8) Line ID </label>
                        <?php echo $this->Form->input('line', array(
                            'type' => 'text',
                            'label' => false,
                            'class' => 'form-control',
                            'required' => false
                        )); ?>
                        <div class="help-block with-errors"></div>					
                    </div>
                </div>
    
                <div class="mb-3" style="margin-top:20px">
                    <label for="InfoDetail" class="form-label"  style="margin:0px">
                        9) กรุณาอัปโหลด<font color="red">รูปภาพผู้เข้าเเข่งขัน</font> 
                        <br> หมายเหตุ การลงทะเบียนนี้คือการบันทึกรายชื่อผู้เข้าแข่งขันสำรองไว้ก่อนเท่านั้น กรณีทางผู้จัดการได้มีการติดต่อกลับไปเพื่อเป็นตัวจริงในการแข่งขัน ทางผู้จัดงานจะติดต่อรายบุคคลเพื่อทำการชำระเงินต่อไป
                        <!-- และอัพโหลด<font color="red">สลิปการโอนเงินลงทะเบียน </font> -->
                        <!-- <font color="red"> (555บาท) * ระบบสามารถเลือกรูปได้มากกว่า 1 ไฟล์ </font> <br>
                        หมายเลขบัญชี : ธนาคารไทยพาณิชย์ <br>
                        ชื่อบัญชี : งานวันเกษตรภาคเหนือ <br>
                        เลขที่บัญชี : 667-412969-8 -->
                    </label> 
                        <?php echo $this->Form->input('BaristaDocument.files.', array(
                            'type' => 'file',
                            'multiple',
                            'class' => 'form-control',
                            'style' => 'margin-top: 0px;',
                            'accept' => '.xlsx,.xls,image/*, .doc, .docx, .ppt, .pptx, .txt, .pdf, .zip, .rar'
                        )); ?>
                </div>
    
                <div class="row" style="margin:30px 0px 30px;" >
                    <div>
                        <center><input type="submit" value="คลิกส่งข้อมูลการลงทะเบียน" class="btn-success"></center> 
                    </div>
                </div>
            </form>
                                        
        <?php } ?>
    </div>
</div>