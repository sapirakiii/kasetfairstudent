<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
 
<!-- Modal animate -->   
<?php echo $this->Html->css('/assets/animatedModal.js-master/css/normalize.min.css'); ?>
<?php echo $this->Html->css('/assets/animatedModal.js-master/css/animate.min.css'); ?>



<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
	  <script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
	  <?php echo $this->Html->script('jquery.dataTables.min'); ?>
	  <?php echo $this->Html->script('dataTables.bootstrap.min'); ?>
<!-- =======================================================
* Template Name: Tempo - v2.1.0
* Template URL: https://bootstrapmade.com/tempo-free-onepage-bootstrap-theme/
* Author: BootstrapMade.com
* License: https://bootstrapmade.com/license/
======================================================== -->
</head>
<style>
.font-44{
  font-family: 'Mitr', sans-serif;
  font-size: 36px;
}
.font-28{
  font-family: 'Mitr', sans-serif;
  font-size: 28px;
}
.font-22{
  font-family: 'Mitr', sans-serif;
  font-size: 22px;
}
.font-10{
  font-family: 'Mitr', sans-serif;
  font-size: 10px;
}

  #btn-close-modal {
	  width:100%;
	  text-align: center;
	  cursor:pointer;
	  color:#fff;
  }

</style>
<link href="https://fonts.googleapis.com/css2?family=Mitr&display=swap" rel="stylesheet">
<style>
  
  /************************************
  * Close button
  ************************************/
  #closebt-container {
    position: relative;
    width:95%;
    text-align:right;
    margin-top:40px;
    margin-right:40px;
    margin-bottom:20px;
  }

  .closebt {
    -webkit-transition: all 0.2s;
    -moz-transition: all 0.2s;
    -ms-transition: all 0.2s;
    -o-transition: all 0.2s;
    transition: all 0.2s;
    cursor:pointer;
  }

  .closebt:hover {
    transform:rotate(90deg);
  }

  #item > p  {
    margin:0px;
    color:#E74B3D;
  }
  /***************Timeline**************** */
  .timeline {
      position: relative;
      padding:4px 0 0 0;
      margin-top:22px;
      list-style: none;
  }

  .timeline>li:nth-child(even) {
      position: relative;
      margin-bottom: 50px;
      height: 180px;
      right:-100px;
  }

  .timeline>li:nth-child(odd) {
      position: relative;
      margin-bottom: 50px;
      height: 180px;
      left:-100px;
  }

  .timeline>li:before,
  .timeline>li:after {
      content: " ";
      display: table;
  }

  .timeline>li:after {
      clear: both;
      min-height: 170px;
  }

  .timeline > li .timeline-panel {
    position: relative;
    float: left;
    width: 41%;
    padding: 0 20px 20px 30px;
    text-align: right;
  }

  .timeline>li .timeline-panel:before {
      right: auto;
      left: -15px;
      border-right-width: 15px;
      border-left-width: 0;
  }

  .timeline>li .timeline-panel:after {
      right: auto;
      left: -14px;
      border-right-width: 14px;
      border-left-width: 0;
  }

  .timeline>li .timeline-image {
      z-index: 100;
      position: absolute;
      left: 50%;
      border: 7px solid #3b5998;
      border-radius: 100%;
      background-color: #3b5998;
      box-shadow: 0 0 5px #4582ec;
      width: 200px;
      height: 200px;
      margin-left: -100px;
  }

  .timeline>li .timeline-image h4 {
      margin-top: 12px;
      font-size: 10px;
      line-height: 14px;
  }

  .timeline>li.timeline-inverted>.timeline-panel {
      float: right;
      padding: 0 30px 20px 20px;
      text-align: left;
      margin: 30px 0 0 0;
  }

  .timeline>li.timeline-inverted>.timeline-panel:before {
      right: auto;
      left: -15px;
      border-right-width: 15px;
      border-left-width: 0;
  }

  .timeline>li.timeline-inverted>.timeline-panel:after {
      right: auto;
      left: -14px;
      border-right-width: 14px;
      border-left-width: 0;
  }

  .timeline>li:last-child {
      margin-bottom: 0;
  }

  .timeline .timeline-heading h4 {
    margin-top:22px;
      margin-bottom: 4px;
      padding:0;
      color: #b3b3b3;
  }

  .timeline .timeline-heading h4.subheading {
    margin:0;
    padding:0;
      text-transform: none;
      font-size:18px;
      color:#333333;
  }

  .timeline .timeline-body>p,
  .timeline .timeline-body>ul {
      margin-bottom: 0;
      color:#808080;
  }
  /*Style for even div.line*/
  .timeline>li:nth-child(odd) .line:before {
      content: "";
      position: absolute;
      top: 60px;
      bottom: 0;
      left: 690px;
      width: 4px;
      height:340px;
      background-color: #3b5998;
      -ms-transform: rotate(-44deg); /* IE 9 */
      -webkit-transform: rotate(-44deg); /* Safari */
      transform: rotate(-44deg);
      box-shadow: 0 0 5px #4582ec;
  }
  /*Style for odd div.line*/
  .timeline>li:nth-child(even) .line:before  {
      content: "";
      position: absolute;
      top: 60px;
      bottom: 0;
      left: 450px;
      width: 4px;
      height:340px;
      background-color: #3b5998;
      -ms-transform: rotate(44deg); /* IE 9 */
      -webkit-transform: rotate(44deg); /* Safari */
      transform: rotate(44deg);
      box-shadow: 0 0 5px #4582ec;
  }
  /* Medium Devices, .visible-md-* */
  @media (min-width: 992px) and (max-width: 1199px) {
    .timeline > li:nth-child(even) {
      margin-bottom: 0px;
      min-height: 0px;
      right: 0px;
    }
    .timeline > li:nth-child(odd) {
      margin-bottom: 0px;
      min-height: 0px;
      left: 0px;
    }
    .timeline>li:nth-child(even) .timeline-image {
      left: 0;
      margin-left: 0px;
    }
    .timeline>li:nth-child(odd) .timeline-image {
      left: 690px;
      margin-left: 0px;
    }
    .timeline > li:nth-child(even) .timeline-panel {
      width: 76%;
      padding: 0 0 20px 0px;
      text-align: left;
    }
    .timeline > li:nth-child(odd) .timeline-panel {
      width: 70%;
      padding: 0 0 20px 0px;
      text-align: right;
    }
    .timeline > li .line {
      display: none;
    }
  }
  /* Small Devices, Tablets */
  @media (min-width: 768px) and (max-width: 991px) {
    .timeline > li:nth-child(even) {
      margin-bottom: 0px;
      min-height: 0px;
      right: 0px;
    }
    .timeline > li:nth-child(odd) {
      margin-bottom: 0px;
      min-height: 0px;
      left: 0px;
    }
    .timeline>li:nth-child(even) .timeline-image {
      left: 0;
      margin-left: 0px;
    }
    .timeline>li:nth-child(odd) .timeline-image {
      left: 520px;
      margin-left: 0px;
    }
    .timeline > li:nth-child(even) .timeline-panel {
      width: 70%;
      padding: 0 0 20px 0px;
      text-align: left;
    }
    .timeline > li:nth-child(odd) .timeline-panel {
      width: 70%;
      padding: 0 0 20px 0px;
      text-align: right;
    }
    .timeline > li .line {
      display: none;
    }
  }
  /* Custom, iPhone Retina */
  @media only screen and (max-width: 767px) {
    .timeline > li:nth-child(even) {
      margin-bottom: 120px;
      min-height: 0px;
      right: 0px;
    }
    .timeline > li:nth-child(odd) {
      margin-bottom: 80px;
      min-height: 0px;
      left: 0px;
    }
    .timeline>li .timeline-image {
      position: static;
      width: 150px;
      height: 150px;
      margin-bottom:-180px;
    }
    .timeline>li.timeline-inverted>.timeline-panel {
        float: right;
        padding: 0 30px 20px 20px;
        text-align: left;
        margin:  0px -140px 0 0;
    }
    .timeline>li:nth-child(even) .timeline-image {
      left: 0;
      margin-left: 0;
    }
    .timeline>li:nth-child(odd) .timeline-image {
      float:right;
      left: 0px;
      margin-left:0;
    }
    .timeline > li:nth-child(even) .timeline-panel {
      width: 60%;
      padding: 0 0 20px 14px;
      margin: 0px -40px 0 0px;
    }
    .timeline > li:nth-child(odd) .timeline-panel {
      width: 55%;
      padding: 0 14px 20px 0px;
      margin: -20px 20px 0 -25px;
    }
    .timeline > li .line {
      display: none;
    }
  }

</style>

<script>
$("#demo01").animatedModal();
</script>



<script type="text/javascript">
function confirm_click()
{
return confirm("ข้าพเจ้าได้รับทราบหลักเกณฑ์การเข้าสังกัดสาขาวิชาเอก ?");
}

</script>
<?php 
		date_default_timezone_set('Asia/Bangkok');
		$date = date("Y-m-d");
		$time = date("H:i:s");

		function DateDiff($strDate1,$strDate2){                             
		return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
		}
		$currentdate= $this->Session->read('currentdate');
		$stringdate = $this->Session->read('stringdate');
		$stringstuid = $this->Session->read('stringstuid');
		$no1 = $this->Session->read('no1');
	?>
	<?php
	function DateThai($strDate)
		{

		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("n",strtotime($strDate));
		$strDay= date("j",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		$strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		$strMonthThai=$strMonthCut[$strMonth];
		return "$strDay $strMonthThai $strYear";
		//, $strHour:$strMinute
	}
	?>
	<style>
.numberCircle {
    border-radius: 50%;
    behavior: url(PIE.htc); /* remove if you don't care about IE8 */
    width: 36px;
    height: 36px;
    padding: 8px;
    background: #fff;
    border: 2px solid black;
    color: black;
    text-align: center;
    font: 20px Arial, sans-serif;
    display: inline-block;
}
</style>

<div class="container">
		<div class="row">
			<center>
                <p><strong>ตารางหลักสูตรการอบรมวิชาชีพระยะสั้น งานเกษตรภาคเหนือครั้งที่ 10</strong></p>
			</center>
			<br>
			

                <table cellspacing="0" style="border-collapse:collapse; width:100%">
                    <tbody>
                        <tr>
                            <td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:1px solid black; height:1px; vertical-align:top; width:378px">
                            <p><strong>หลักสูตร</strong></p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:1px solid black; height:1px; vertical-align:top; width:177px">
                            <p><strong>วันอบรม</strong></p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:1px solid black; height:1px; vertical-align:top; width:130px">
                            <p><strong>ช่วงเช้า</strong></p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:1px solid black; height:1px; vertical-align:top; width:154px">
                            <p><strong>ช่วงบ่าย</strong></p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:1px solid black; height:1px; vertical-align:top; width:201px">
                            <p><strong>สถานที่อบรม</strong></p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:1px solid black; height:1px; vertical-align:top; width:154px">
                            <p><strong>ค่าลงทะเบียน</strong></p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:378px">
                            <p>1. หลักสูตรการเพาะเลี้ยงเนื้อเยื่อ</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>1 ธันวาคม 2565</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:130px">
                            <p>09.00 &ndash; 12.00</p>
                                <a href="https://forms.gle/5FGP1Sa8EmnzHzHYA" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="background-color:#bfbfbf; border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>อาคารปฏิบัติการพืชสวน (อาคารเพาะเลี้ยงเนื้อเยื่อ)</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>800</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:378px">
                            <p>2. การเพาะเห็ดเศรษฐกิจในครัวเรือน : การเพาะเชื้อเห็ดฟางไว้ปลูกกินเองอย่างง่าย</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>7 ธันวาคม 2565</p>
                            </td>
                            <td style="background-color:#bfbfbf; border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:130px">
                            <p>&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>13.00 - 16.00</p>
                            <a href="https://forms.gle/HRQRGTHpD6hJhrZZA" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>ห้องดอกสัก ชั้น 1&nbsp; อาคารนวัตกรรม</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>300</p>
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="2" style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:378px">
                            <p>3. การผลิตเมล่อนแตงโมในระบบโรงเรือน&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>2 ธันวาคม 2565</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:130px">
                            <p>09.00 &ndash; 12.00</p>
                            <a href="https://forms.gle/G4UBRiETeNEkWvWW6" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="background-color:#bfbfbf; border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>โรงเรือนผลิตพืชสวน</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>200</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>10 ธันวาคม 2565</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:130px">
                            <p>09.00 &ndash; 12.00</p>
                            <a href="https://forms.gle/G4UBRiETeNEkWvWW6" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="background-color:#bfbfbf; border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>โรงเรือนผลิตพืชสวน</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>200</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:378px">
                            <p>4. การเลี้ยงแมลงทหารเสือ</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>2 ธันวาคม 2565</p>
                            </td>
                            <td style="background-color:#bfbfbf; border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:130px">
                            <p>&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>13.00 - 16.00</p>
                            <a href="https://forms.gle/qixjT2CBJ3E8JZgL9" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>ห้องดอกสัก ชั้น 1&nbsp; อาคารนวัตกรรม</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>300</p>
                            </td>
                        </tr>
                        <tr>
                            <td rowspan="3" style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:378px">
                            <p>5. สวนผักในบ้านสไตล์คนในเมือง : การปลูกและดูแลผักในสวนหลังบ้าน&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>3 ธันวาคม 2565</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:130px">
                            <p>09.00 - 11.00</p>
                            <a href="https://forms.gle/skF1tcadEkWxxAwR7" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="background-color:#bfbfbf; border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>ห้องประชุมสถานีระบบทรัพยากรเกษตร ชั้น 2</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>300</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>5 ธันวาคม 2565</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:130px">
                            <p>09.00 - 11.00</p>
                            <a href="https://forms.gle/skF1tcadEkWxxAwR7" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="background-color:#bfbfbf; border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>ห้องประชุมสถานีระบบทรัพยากรเกษตร ชั้น 2</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>300</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>10 ธันวาคม 2565</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:130px">
                            <p>09.00 - 11.00</p>
                            <a href="https://forms.gle/skF1tcadEkWxxAwR7" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="background-color:#bfbfbf; border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>ห้องประชุมสถานีระบบทรัพยากรเกษตร ชั้น 2</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>300</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:378px">
                            <p>6. การแปรรูปแมลงทหารเสือ สำหรับเลี้ยงสัตว์น้ำ</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>3 ธันวาคม 2565</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:130px">
                            <p>09.00 - 12.00</p>
                            <a href="https://forms.gle/RiXPrkCtrxwShQpV8" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="background-color:#bfbfbf; border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>ห้องดอกสัก ชั้น 1&nbsp; อาคารนวัตกรรม</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>300</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:378px">
                            <p>7. การเลี้ยงปลานิลในระบบไบโอฟลอค (Biofloc)</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>3 ธันวาคม 2565</p>
                            </td>
                            <td style="background-color:#bfbfbf; border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:130px">
                            <p>&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>13.00 &ndash; 16.00</p>
                            <a href="https://forms.gle/nT882vcWBmgbj4Vw7" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>ห้องดอกสัก ชั้น 1&nbsp; อาคารนวัตกรรม</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>200</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:378px">
                            <p>8. การดูแลต้นไม้ให้ออกดอก (ไม้ดอกไม้ประดับ)</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>3 ธันวาคม 2565</p>
                            </td>
                            <td style="background-color:#bfbfbf; border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:130px">
                            <p>&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>13.00 &ndash; 16.00</p>
                            <a href="https://forms.gle/4SNsjsG2g5koSAZm8" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>ห้องเรียน 1 ชั้น 2&nbsp;&nbsp; อาคารนวัตกรรม</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>200</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:378px">
                            <p>9. การเลี้ยงจิ้งหรีดแมลงเศรษฐกิจแหล่งโปรตีนใหม่กับการแปรรูปเชิงพาณิชย์</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>4 ธันวาคม 2565</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:130px">
                            <p>09.00 &ndash; 12.00</p>
                            <a href="https://forms.gle/A9diPhh5YjVJUb6q6" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="background-color:#bfbfbf; border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>ห้องดอกเสี้ยว ชั้น 3&nbsp; อาคารนวัตกรรม</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>350</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:378px">
                            <p>10. ระบบน้ำทางการเกษตร : การวางผัง ออกแบบและติดตั้งระบบน้ำในสวน/โรงเรือน สำหรับการเกษตร</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>4 ธันวาคม 2565</p>
                            </td>
                            <td colspan="2" style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:284px">
                            <p>09.00 &ndash; 16.00</p>
                            <a href="https://forms.gle/R7s9Cgas1jvHM9LS8" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>ห้องดอกสัก ชั้น 1&nbsp; อาคารนวัตกรรม</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>500</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:378px">
                            <p>11. การชงกาแฟและการเท Latte Art นักชงมือใหม่สู่มืออาชีพ : รู้จักกับพื้นฐานการชงกาแฟเบื้องต้น สูตรกาแฟ และเทคนิคการเท&nbsp; Latte Art&nbsp; ให้เป็นเร็ว</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>4 ธันวาคม 2565</p>
                            </td>
                            <td colspan="2" style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:284px">
                            <p>09.00 &ndash; 16.00</p>
                            <a href="https://forms.gle/rXHEN1ca3CzzjdMB9" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>ห้องฝึกอบรม ศูนย์พัฒนากาแฟล้านนาไทย</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>500</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:378px">
                            <p>12. โรคพืชเบื้องต้น/เชื้อรา/เชื้อแบคทีเรีย</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>5 ธันวาคม 2565</p>
                            </td>
                            <td style="background-color:#bfbfbf; border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:130px">
                            <p>&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>13.00 &ndash; 16.00</p>
                            <a href="https://forms.gle/4TyJNEuPKyJvCaPy8" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>ห้องดอกสัก ชั้น 1&nbsp; อาคารนวัตกรรม</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>ฟรี</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:378px">
                            <p>13. นวัตกรรมเกษตรสมัยใหม่&nbsp; (การบริหารพื้นที่/การลดต้นทุน/เอาเทคโนโลยีสมัยใหม่มาใช้ ความปลอดภัย /ผลผลิตต่อไร่/ทำน้อยได้มาก)</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>6 ธันวาคม 2565</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:130px">
                            <p>09.00 &ndash; 12.00</p>
                            <a href="https://forms.gle/d2ChKe2JodwKAxAY7" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="background-color:#bfbfbf; border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>ห้องประชุมสถานีระบบทรัพยากรเกษตร ชั้น 2</p>

                            <p>&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>300</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:378px">
                            <p>14. การทำของหวาน 3 สัมผัส Possct, Panna cotta, Yogurt</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>6 ธันวาคม 2565</p>
                            </td>
                            <td style="background-color:#bfbfbf; border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:130px">
                            <p>&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>13.00 &ndash; 16.00</p>
                            <a href="https://forms.gle/2CTt4gwoBmyNuax99" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>ห้องดอกเสี้ยว ชั้น 3&nbsp; อาคารนวัตกรรม</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>555</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:378px">
                            <p>15. การขยายพันธุ์ไม้ใบ</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>7 ธันวาคม 2565</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:130px">
                            <p>09.00 &ndash; 12.00</p>
                            <a href="https://forms.gle/ec61fQeEq5xczFA36" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="background-color:#bfbfbf; border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>ห้องประชุมสถานีระบบทรัพยากรเกษตร ชั้น 2</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>200</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:378px">
                            <p>16. เปิดโลกของชีส : การทำชีสสด Cottage cheese, Mozzarella Cheese</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>8 ธันวาคม 2565</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:130px">
                            <p>09.00 &ndash; 12.00</p>
                            <a href="https://forms.gle/H3Zb9ENA73AT6AKJ8" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="background-color:#bfbfbf; border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>ห้องดอกเสี้ยว ชั้น 3&nbsp; อาคารนวัตกรรม</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>555</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:378px">
                            <p>17. การเชื่อมโยงตลาดสินค้าเกษตรผ่านแพลตฟอร์มออนไลน์</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>8 ธันวาคม 2565</p>
                            </td>
                            <td style="background-color:#bfbfbf; border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:130px">
                            <p>&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>13.00 &ndash; 16.00</p>
                            <a href="https://forms.gle/v5nLsKGYBmCUgDid7" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>ห้องประชุมสถานีระบบทรัพยากรเกษตร ชั้น 2</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>200</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:378px">
                            <p>18. การผลิตพืชผักตามมาตรฐาน GAP/อินทรีย์</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>9 ธันวาคม 2565</p>
                            </td>
                            <td colspan="2" style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:284px">
                            <p>09.00 &ndash; 16.00</p>
                            <a href="https://forms.gle/NrUzJS1buwjcy2nN8" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>ห้องดอกสัก ชั้น 1&nbsp; อาคารนวัตกรรม</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>ฟรี</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:378px">
                            <p>19. การผลิตปุ๋ยอินทรีย์และปุ๋ยชีวภาพครบวงจร : บรรยายและฝึกปฏิบัติการทำปุ๋ยหมักอย่างง่ายจากวัสดุเศษเหลือทางการเกษตร และวิธีการทำปุ๋ยแบบเม็ด</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>10 ธันวาคม 2565</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:130px">
                            <p>09.00 &ndash; 12.00</p>
                            <a href="https://forms.gle/UgDYdt1EwFKpF4Mv6" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="background-color:#bfbfbf; border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>ห้องดอกสัก ชั้น 1&nbsp; อาคารนวัตกรรม</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>100</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:378px">
                            <p>20. การทำ Souffl&eacute; Pancake และการจัดจาน</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>10 ธันวาคม 2565</p>
                            </td>
                            <td style="background-color:#bfbfbf; border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:130px">
                            <p>&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>13.00 &ndash; 16.00</p>
                            <a href="https://forms.gle/dYkYUaX6o8EB9XCc7" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>ห้องดอกสัก ชั้น 1&nbsp; อาคารนวัตกรรม</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>555</p>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:378px">
                            <p>21. การปลูกเลี้ยงไม้ใบ/เฟิร์น</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:177px">
                            <p>11 ธันวาคม 2565</p>
                            </td>
                            <td style="background-color:#bfbfbf; border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:130px">
                            <p>&nbsp;</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>13.00 &ndash; 16.00</p>
                            <a href="https://forms.gle/zxWFPVJUZ2p1mbeb7" class="btn btn-success" target="_blank">ลงทะเบียน</a>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:201px">
                            <p>ห้องดอกสัก ชั้น 1&nbsp; อาคารนวัตกรรม</p>
                            </td>
                            <td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:1px; vertical-align:top; width:154px">
                            <p>200</p>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <!-- <div class="font_panel6 pull-right"> <i class="fa fa-user-circle-o" aria-hidden="true"></i> : <?php echo $listinfopage['Infopage']['CountRead'] ?> ครั้ง </div> -->
		</div>
	</div>
</div>
 
 
