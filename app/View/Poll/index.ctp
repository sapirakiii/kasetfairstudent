<div class="container" style="padding-top: 0;">
	
	<div class="page-header">
		<h4>กำหนดการฝึกงาน 1 ปีการศึกษา 2564 สำหรับนักศึกษาชั้นปีที่ 1 รหัส 64</h4>
		<h5>ระหว่างวันที่ 6-17 มิถุนายน 2565 ในรูปแบบ ออนไซต์ (on-site)</h5>
		<h5>ณ คณะเกษตรศาสตร์และศูนย์วิจัย และฝึกอบรมการเกษตร แม่เหียะ มหาวิทยาลัยเชียงใหม่</h5>
	</div>	
			
	<div class="table-responsive"> 
		<table class="table table-bordered table-striped"> 
			<thead> 
				<tr> 
					<th class="text-center" width="10%">วันที่</th> 
					<th class="text-center" width="25%">ตารางการฝึกงานนักศึกษาชั้นปีที่ 1 <br>ช่วงฝึกงาน ตั้งแต่เวลา  08.30 - 16.30 น.</th> 
					<th class="text-center" width="20%">ภาควิชา / สาขา / ศูนย์ / บริษัท </th> 
					<th class="text-center" width="15%">แบบประเมิน</th> 
					<th class="text-center" width="15%">ผลการประเมิน</th> 
				</tr> 
			</thead> 
			<tbody> 
				<tr> 
					<td class="text-center">					
						1
					</td> 
					<td>					
						วันจันทร์ที่ 6 มิถุนายน 2565 
					</td> 
					<td>					
						ปฐมนิเทศ + บรรยายพิเศษ

					</td> 
					<td>					
						<a href="<?php echo $this->Html->url(array('action' => 'poll1')); ?>" target="_blank" >
							แบบประเมินการจัดการบรรยายพิเศษ เรื่องค้นพบและเข้าใจตัวเอง : จุดสตาร์ทมุ่งสู่เส้นทางสำเร็จที่ใช่ <br>และ “กำรดูแลสุขภาพเพื่อป้องกันโรคระบาด”
							 
						</a>
	
					</td> 
					<td>					
						<a href="<?php echo $this->Html->url('/files/poll/361/output.asp'); ?>" target="_blank" >
							ผลการประเมิน
							 
						</a>
	
					</td> 
				</tr> 
				<tr> 
					<td class="text-center">					
						2
					</td> 
					<td>					
						วันอังคารที่ 7 มิถุนายน 2565

					</td> 
					<td>					
						สาขาวิชากีฏวิทยา  + สาขาวิชาโรคพืช 
					</td> 
					<td>					
						<a href="<?php echo $this->Html->url('/files/poll/363/index.asp'); ?>" target="_blank" >
						แบบประเมินการฝึกงานของนักศึกษาคณะเกษตรศาสตร์ <br>ฐานการฝึกงาน สาขาวิชากีฏวิทยา 
							 
						</a>
						<br>
						<a href="<?php echo $this->Html->url('/files/poll/364/index.asp'); ?>" target="_blank" >
						แบบประเมินการฝึกงานของนักศึกษาคณะเกษตรศาสตร์ <br>ฐานการฝึกงาน สาขาวิชาโรคพืช
							 
						</a>
					</td> 
					<td>					
						<a href="<?php echo $this->Html->url('/files/poll/363/output.asp'); ?>" target="_blank" >
							ผลการประเมิน
							 
						</a> <br>
						<a href="<?php echo $this->Html->url('/files/poll/364/output.asp'); ?>" target="_blank" >
							ผลการประเมิน
							 
						</a>
					</td> 
				</tr> 
				<tr> 
					<td class="text-center">					
						3
					</td> 
					<td>					
						วันพุธที่ 8 มิถุนายน 2565
					</td> 
					<td>					
						ภาควิชาสัตวศาสตร์และสัตว์น้ำ
					</td> 
					<td>					
						<a href="<?php echo $this->Html->url('/files/poll/366/index.asp'); ?>" target="_blank" >
						แบบประเมินการฝึกงานของนักศึกษาคณะเกษตรศาสตร์ <br>ฐานการฝึกงาน สาขาวิชาสัตวศาสตร์และสัตว์น้ำ
							 
						</a>
					</td> 
					<td>					
						<a href="<?php echo $this->Html->url('/files/poll/366/output.asp'); ?>" target="_blank" >
							ผลการประเมิน
							 
						</a>
						 
					</td> 
				</tr> 
				<tr> 
					<td class="text-center">					
						4
					</td> 
					<td>					
						วันพฤหัสบดีที่ 9 มิถุนายน 2565

					</td> 
					<td>					
						ภาควิชาสัตวศาสตร์และสัตว์น้ำ

					</td> 
					<td>					
						 
					</td> 
				</tr> 
				<tr> 
					<td class="text-center">					
						5
					</td> 
					<td>					
						วันศุกร์ที่ 10 มิถุนายน 2565
					</td> 
					<td>					
						ภาควิชาพัฒนาเศรษฐกิจการเกษตร   
					</td> 
					<td>					
						<a href="<?php echo $this->Html->url('/files/poll/365/index.asp'); ?>" target="_blank" >
						แบบประเมินการฝึกงานของนักศึกษาคณะเกษตรศาสตร์ <br>ฐานการฝึกงาน ภาควิชาพัฒนาเศรษฐกิจการเกษตร
							 
						</a>
					</td> 
					<td>					
						<a href="<?php echo $this->Html->url('/files/poll/365/output.asp'); ?>" target="_blank" >
							ผลการประเมิน
							 
						</a>
						 
					</td> 
				</tr> 
				<tr> 
					<td class="text-center">					
						6
					</td> 
					<td>					
						วันเสาร์ที่ 11 มิถุนายน 2565
					</td> 
					<td>					
						ภาควิชาพัฒนาเศรษฐกิจการเกษตร   
					</td> 
					<td>					
						 
					</td> 
				</tr>  
				<tr> 
					<td class="text-center">					
						7
					</td> 
					<td>					
						วันอาทิตย์ที่ 12 มิถุนายน 2565

					</td> 
					<td>					
						ภาควิชาเกษตรที่สูงและทรัพยากรธรรมชาติ

					</td> 
					<td>					
						<a href="<?php echo $this->Html->url('/files/poll/368/index.asp'); ?>" target="_blank" >
						แบบประเมินการฝึกงานของนักศึกษาคณะเกษตรศาสตร์ <br>ฐานการฝึกงาน ภาควิชาเกษตรที่สูงและทรัพยากรธรรมชาติ
							 
						</a>
					</td> 
					<td>					
						<a href="<?php echo $this->Html->url('/files/poll/368/output.asp'); ?>" target="_blank" >
							ผลการประเมิน
							 
						</a>
						 
					</td> 
				</tr>  
				<tr> 
					<td class="text-center">					
						8
					</td> 
					<td>					
						วันจันทร์ที่ 13 มิถุนายน 2565
					</td> 
					<td>					
						สาขาวิชาพืชสวน
					</td> 
					<td>					
						<a href="<?php echo $this->Html->url('/files/poll/369/index.asp'); ?>" target="_blank" >
						แบบประเมินการฝึกงานของนักศึกษาคณะเกษตรศาสตร์ <br>ฐานการฝึกงาน สาขาวิชาพืชสวน
							 
						</a>
					</td> 
					<td>					
						<a href="<?php echo $this->Html->url('/files/poll/369/output.asp'); ?>" target="_blank" >
							ผลการประเมิน
							 
						</a>
						 
					</td> 
				</tr>  
				<tr> 
					<td class="text-center">					
						9
					</td> 
					<td>					
						วันอังคารที่ 14 มิถุนายน 2565

					</td> 
					<td>					
						ฐานฝึกงานบริษัท ยันม่าร์ เอส.พี. จำกัด

					</td> 
					<td>					
						<a href="<?php echo $this->Html->url('/files/poll/372/index.asp'); ?>" target="_blank" >
						แบบประเมินการฝึกงานของนักศึกษาคณะเกษตรศาสตร์ <br> ฐานฝึกงานบริษัท ยันม่าร์ เอส.พี. จำกัด
							 
						</a>
					</td> 
					<td>					
						<a href="<?php echo $this->Html->url('/files/poll/372/output.asp'); ?>" target="_blank" >
							ผลการประเมิน
							 
						</a>
						 
					</td> 
				</tr>  
				<tr> 
					<td class="text-center">					
						10
					</td> 
					<td>					
						วันพุธที่ 15 มิถุนายน 2565


					</td> 
					<td>					
						ฐานฝึกงานบริษัท ยันม่าร์ เอส.พี. จำกัด

					</td> 
					<td>					
						 
					</td> 
				</tr>  
				<tr> 
					<td class="text-center">					
						11
					</td> 
					<td>					
						วันพฤหัสบดีที่ 16 มิถุนายน 2565
					</td> 
					<td>					
						สาขาวิชาพืชไร่
					</td> 
					<td>					
						<a href="<?php echo $this->Html->url('/files/poll/367/index.asp'); ?>" target="_blank" >
						แบบประเมินการฝึกงานของนักศึกษาคณะเกษตรศาสตร์ <br> ฐานฝึกงาน สาขาวิชาพืชไร่
							 
						</a>
					</td> 
					<td>					
						<a href="<?php echo $this->Html->url('/files/poll/367/output.asp'); ?>" target="_blank" >
							ผลการประเมิน
							 
						</a>
						 
					</td> 
				</tr>  
				<tr> 
					<td class="text-center">					
						12
					</td> 
					<td>					
						วันศุกร์ที่ 17 มิถุนายน 2565

					</td> 
					<td>					
						สาขาวิชาปฐพีศาสตร์ + ศูนย์วิจัยฯ แม่เหียะ

					</td> 
					<td>					
						<a href="<?php echo $this->Html->url('/files/poll/370/index.asp'); ?>" target="_blank" >
						แบบประเมินการฝึกงานของนักศึกษาคณะเกษตรศาสตร์ <br> ฐานฝึกงาน สาขาวิชาปฐพีศาสตร์ + ศูนย์วิจัยฯ แม่เหียะ
							 
						</a>
					</td> 
					<td>					
						<a href="<?php echo $this->Html->url('/files/poll/370/output.asp'); ?>" target="_blank" >
							ผลการประเมิน
							 
						</a>
						 
					</td> 
				</tr>
				<tr> 
					<td class="text-center">					
						13
					</td> 
					<td>					
						วันพุธที่ 29 มิถุนายน 2565

					</td> 
					<td>					
						นำเสนอผลการฝึกงาน (ส่งเป็นคลิปวีดีโอ)

					</td> 
					<td>					
					 <a href="<?php echo $this->Html->url('/files/poll/371/index.asp'); ?>" target="_blank" >
						แบบประเมินการสรุปการฝึกงานของนักศึกษาคณะเกษตรศาสตร์  
							 
						</a>
					</td> 
					<td>					
						<a href="<?php echo $this->Html->url('/files/poll/371/output.asp'); ?>" target="_blank" >
							ผลการประเมิน
							 
						</a>
						 
					</td> 
					 
				</tr>
			</tbody> 
		</table> 
		 
	</div>
	
	 ***  สามารถสอบถามรายละเอียดเพิ่มเติมที่  นางสาวธีรนุช  ภัทรกุล  (พี่ยุ้ย) งานบริการการศึกษาและพัฒนาคุณภาพนักศึกษา    คณะเกษตรศาสตร์  โทรศัพท์  053-944641   
		
	 
	
</div>

