<div class="container" style="padding:15px 0 0; width:60%;">
    <center>
        <h3>แบบประเมินการจัดการบรรยายพิเศษ</h3>
        <p>การฝึกงาน 1 (กระบวนวิชา 400190)</p> 
    </center>

    <span><b>ส่วนที่ 1 ข้อมูลทั่วไปของผู้รับการอบรม</b></span>
    <table class="table">
        <tr>
            <td>เพศ</td>
            <td>
                <?php 
                    echo $this->Form->input('gender', array(
                        'type'=>'radio',
                        'options'=>$genders,
                        'label'=>false,
                        'hiddenField'=>false,
                        'class' => 'form-check-input',
                        'legend'=>false,
                        'style' => 'margin-left: 15px; 
                                    vertical-align:text-bottom;',
                    ));
                ?>
            </td>
        </tr>
        <tr>
            <td>สาขาวิชา</td>
            <td>
                <?php
                    echo $this->Form->input('major', array(
                        'options' => $majors,
                        'class' => 'form-control',
                        'label' => false,
                        'required' => true,
                        'empty' => '--กรุณาเลือก--'
                    ));
                ?>
            </td>
        </tr>
    </table>
    <div style="margin: 30px;"></div>

    <p><b>ส่วนที่ 2</b></p>
    <p><i>หัวข้อ : เรื่องค้นพบและเข้าใจตัวเอง : จุดสตาร์ทมุ่งสู่เส้นทางสำเร็จที่ใช่</i></p>
    <p><u>1. ความคิดเห็นเกี่ยวกับวิทยากร</u></p>
    <table class="table">
        <?php foreach($questions1 as $question){?>
            <tr>
                <td style="width: 55%;"><?php echo $question['Question']['name'] ?></td>
                <td>  
                    <?php 
                        echo $this->Form->input('rate', array(
                            'type'=>'radio',
                            'options'=>$rates,
                            'label'=>false,
                            'hiddenField'=>false,
                            'class' => 'form-check-input',
                            'legend'=>false,
                            'style' => 'margin-left: 10px; 
                                        vertical-align:text-bottom;',
                        ));
                    ?>
                </td>
           
        <?php } ?>
        </tr>
    </table>
    <p><u>2. ความคิดเห็นเกี่ยวกับหัวข้อการบรรยาย</u></p>
    <table class="table">
        <?php foreach($questions2 as $question){?>
            <tr>
                <td style="width: 55%;"><?php echo $question['Question']['name'] ?></td>
                <td>  
                    <?php 
                        echo $this->Form->input('rate', array(
                            'type'=>'radio',
                            'options'=>$rates,
                            'label'=>false,
                            'hiddenField'=>false,
                            'class' => 'form-check-input',
                            'legend'=>false,
                            'style' => 'margin-left: 10px; 
                                        vertical-align:text-bottom;',
                        ));
                    ?>
                </td>
            </tr>
        <?php } ?>
    </table>
    <div style="margin: 30px;"></div>
    
    <p><i>หัวข้อ : การดูแลสุขภาพเพื่อป้องกันโรคระบาด”</i></p>
    <p><u>1. ความคิดเห็นเกี่ยวกับวิทยากร</u></p>
    <table class="table">
        <?php foreach($questions1 as $question){?>
            <tr>
                <td style="width: 55%;"><?php echo $question['Question']['name'] ?></td>
                <td>  
                    <?php 
                        echo $this->Form->input('rate', array(
                            'type'=>'radio',
                            'options'=>$rates,
                            'label'=>false,
                            'hiddenField'=>false,
                            'class' => 'form-check-input',
                            'legend'=>false,
                            'style' => 'margin-left: 10px; 
                                        vertical-align:text-bottom;',
                        ));
                    ?>
                </td>
            </tr>
        <?php } ?>
    </table>
    <p><u>2. ความคิดเห็นเกี่ยวกับหัวข้อการบรรยาย</u></p>
    <table class="table">
        <?php foreach($questions2 as $question){?>
            <tr>
                <td style="width: 55%;"><?php echo $question['Question']['name'] ?></td>
                <td>  
                    <?php 
                        echo $this->Form->input('rate', array(
                            'type'=>'radio',
                            'options'=>$rates,
                            'label'=>false,
                            'hiddenField'=>false,
                            'class' => 'form-check-input',
                            'legend'=>false,
                            'style' => 'margin-left: 10px; 
                                        vertical-align:text-bottom;',
                        ));
                    ?>
                </td>
            </tr>
        <?php } ?>
    </table>
    <div style="margin: 30px;"></div>

    <span><b>ส่วนที่ 3 : ข้อคิดเห็น ข้อเสนอแนะอื่น ๆ (เพิ่มเติม)</b></span>
    <table class="table">
        <tr>
            <td>
                <?php 
                    echo $this->Form->input('description', array(
						'type' => 'textarea',
						'label' => false,
						'class' => 'form-control ckeditor',
						'required' => true,
					)); 
                ?>
            </td>
        </tr>
    </table>
</div>