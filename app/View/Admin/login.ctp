<div class="container" style="padding:15px 0 0;">
	<div class="panel panel-primary" >
		<div class="panel-heading"> 
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center>ผู้ดูแลระบบ</center>
				</h3> 
			</div>
		</div>	
		<div class="panel-body" style="padding-top: 15px; ">
			
			
			<div class="row">
				<div class="col-md-6 col-md-offset-3 well">
					<?php 
					//debug($this->request->data);
					$required = 'กรุณากรอกข้อมูล';
					
					echo $this->Form->create('Admin',array('role' => 'form','data-toggle' => 'validator')); 
					
					?>

						<div class="row">
						  <div class="form-group col-md-12" style="margin-bottom: 0;">
							<label>Username <span style="color: red;">*</span></label>

							<?php echo $this->Form->input('username', array('type' => 'text','data-error' => $required,'label' => false,'class' => 'form-control','required' => true)); ?>
							
							<div class="help-block with-errors"></div>
						  </div>
						  
						</div>
						
						<div class="row">
						  <div class="form-group col-md-12" style="margin-bottom: 0;">
							<label>Password <span style="color: red;">*</span></label>

							<?php echo $this->Form->input('password', array('type' => 'password','data-error' => $required,'label' => false,'class' => 'form-control','required' => true)); ?>
							
							<div class="help-block with-errors"></div>
						  </div>
						  
						</div>
						
						
						<div class="row">
							<div class="col-md-12">
								<input type="submit" value="เข้าสู่ระบบ" class="btn btn-success btn-lg btn-block"> 
							</div>
						</div>
					</form>
				</div>	
			</div>
		</div>

	</div>
	
</div>