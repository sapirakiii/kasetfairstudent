<div class="container" style="padding:15px 0 0;">
	<div class="panel panel-primary" >
		<div class="panel-heading"> 
			<div class="panel-title">
				<h3 style="margin:0px !important;">
					<center>ผู้ดูแลระบบ</center>
				</h3> 
			</div>
		</div>	
		<div class="panel-body" style="padding-top: 15px; ">
			
			
			<div class="row">
				<div class="col-md-6 col-md-offset-3 well">

					<div class="row">
						<div class="col-md-12">
							<a class="btn btn-warning btn-lg btn-block" href="<?php echo $this->Html->url(array('action' => 'logout')); ?>" role="button">ออกจากระบบ</a>	
						</div>
					</div>

				</div>	
			</div>
		</div>

	</div>
	
</div>