<!DOCTYPE html>
<html lang="en">
	<head>
		<meta name="google-site-verification" content="riNYJwobfkZJWGejdSqPjgWadsFT8-RAERhCiQLSbgw" />
		<?php echo $this->Html->charset(); ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta content="Vision :: Go for SMART AGRICULTURE" name="author" />
		<meta content="<?php //echo $keywords; ?>" name="keywords" />
		<meta content="<?php //echo $keywordsTh; ?>" name="keywords" lang="th"/>
		<meta content="<?php //echo $description; ?>" name="description" />
		<meta name="robots" content="noindex">
		<meta name="googlebot" content="noindex">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
		<style>
			@import url('https://fonts.googleapis.com/css2?family=Kanit:wght@200&family=Sarabun:wght@200&display=swap');
			* {font-family: "Sarabun";},
		</style>
		<?php
			// //echo $this->Html->meta('icon');		
			// echo $this->Html->meta('favicon.ico','/img/Logo_ENG.png',array('type' => 'icon'));s
			// //echo $this->Html->css('cake.generic');	
			// echo $this->Html->css('agri');
			// echo $this->Html->css('table');
			// //echo $this->Html->css('profile');
		?>

		<title>
			:: งานเกษตรภาคเหนือ ครั้งที่ 10  :: ฝ่ายอบรมวิชาชีพระยะสั้น / ฝ่ายนันทนาการและบันเทิง
		</title>


	<!-- Bootstrap core CSS -->
	<?php echo $this->Html->css('/assets/bootstrap/dist/css/bootstrap.min'); ?>

    <!-- Select2 CSS -->
    <?php echo $this->Html->css('/assets/select2/dist/css/select2.min'); ?>

	<!-- ckeditor -->
	<?php echo $this->Html->script('ckeditor/ckeditor');?>
	
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <?php echo $this->Html->css('/assets/bootstrap/assets/css/ie10-viewport-bug-workaround'); ?>
    
    <!-- Custom styles for this template -->
    <?php echo $this->Html->css('/assets/bootstrap/sticky-footer-navbar'); ?>

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><?php echo $this->Html->script('/assets/bootstrap/assets/js/ie8-responsive-file-warning'); ?><![endif]-->
    <?php echo $this->Html->script('/assets/bootstrap/assets/js/ie-emulation-modes-warning'); ?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <?php echo $this->Html->script('https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js'); ?>
      <?php echo $this->Html->script('https://oss.maxcdn.com/respond/1.4.2/respond.min.js'); ?>
    <![endif]-->
    
    <?php //echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js'); ?>
    <?php 
      //<script>window.jQuery || document.write('<script src="/assets/bootstrap/assets/js/vendor/jquery.min.js"><\/script>')</script>
      echo $this->Html->script('/assets/bootstrap/assets/js/vendor/jquery.min'); 
    ?>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<?php echo $this->Html->script('/assets/bootstrap/assets/js/ie10-viewport-bug-workaround'); ?>
		
		<?php echo $this->Html->css('/assets/bootstrap-datepicker/css/bootstrap-datepicker3.min'); ?>
		<?php echo $this->Html->script('/assets/bootstrap-datepicker/js/bootstrap-datepicker.min'); ?>
		<?php echo $this->Html->script('/js/dist/clipboard.min'); ?>

		<!-- datatable -->
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
		<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
		<?php echo $this->Html->css('dataTables.bootstrap.min'); ?>
		<?php echo $this->Html->script('jquery.dataTables.min'); ?>
		<?php echo $this->Html->script('dataTables.bootstrap.min'); ?>
		<?php echo $this->Html->css('/assets/font-awesome-4.7.0/css/font-awesome.min'); ?>  
		<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
		
      
    <?php
      echo $this->fetch('meta');
      echo $this->fetch('css');
      echo $this->fetch('script');
    ?>
    <script type="text/javascript">
			$(function(){
				$.fn.datepicker.defaults.format = "dd-MM-yyyy";
				$('.input-group.date').datepicker({});	
			});
				
	</script>
		
	</head>

<body style="margin-bottom: 160px;">
<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-54232567-2', 'auto');
		ga('send', 'pageview');

	</script>
		<!--
	<img src="https://goo.gl/Yl6KNg" class="black-ribbon stick-top stick-left"/>
	-->
	<!-- <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a> -->
	 
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark" aria-label="Tenth navbar example">
		<div class="container-fluid">
		<button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample08">
			<ul class="navbar-nav navbar-right">
			<!-- <li class="nav-item">
				<a class="nav-link active" aria-current="page" href="#">Centered nav only</a>
			</li> --> 
			<li class="nav-item">
				<a class="nav-link active" href="<?php echo $this->Html->url(array('controller' => 'register','action' => 'training_list')); ?>">ลงทะเบียนหลักสูตรการอบรมวิชาชีพระยะสั้น</a>
			</li>
			<li class="nav-item">
				<a class="nav-link active" href="<?php echo $this->Html->url(array('controller' => 'register','action' => 'barista_list')); ?>">ลงทะเบียนแข่งขัน Latte Art Smackdown</a>
			</li>
			 
				<?php  $UserName = $this->Session->read('MisEmployee'); 
						$sSubPre = '';
						if ($UserName['MisSubPrename']['id'] != null) {
						$sSubPre = $sSubPre . $UserName['MisSubPrename']['name_short_th'] . ' ';
						}
						if ($sSubPre == '') {
						$sSubPre = $sSubPre . $UserName['MisPrename']['name_full_th'] . ' ';
						} 
				?>
				<?php  if($UserName != null) { ?>
					<li><font color="white"><?php echo $sSubPre.''. $UserName['MisEmployee']['fname_th'].' '.$UserName['MisEmployee']['lname_th']?></font></li>
				<?php } ?>     
			</ul>
		</div>
		</div>
	</nav>
	 
	
	 <header id="myCarousel" class="carousel slide" style="height: auto;">
      
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		  
			
        </ol>

      
        <div class="carousel-inner">
            <div class="item active">
                <img src="<?php echo $this->Html->url('/img/next33.jpg'); ?>">
            </div> 
			
             
			
        </div>

       
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header> 
	
    <!-- Begin page content -->
    
	<?php 	
	$alert = $this->Session->flash();
	
	if($alert){
		$alertType = $this->Session->read('alertType');
		if(!isset($alertType)){
			$alertType = 'danger';
		}
	?>
		<div class="alert alert-<?php echo $alertType; ?>" role="alert" style="text-align: center;">
			<?php echo $alert; ?>
		</div>
	<?php 
	} 
	?>
	
	<?php echo $this->fetch('content'); ?>
	
	<div class="container" style="<?php echo $this->Html->style(array('height' => 'auto','position' => 'static','margin-top' => '30px'));?>">
		<!-- Footer -->
		<footer >

		</footer>
	</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
	<?php echo $this->Html->script('/assets/bootstrap/dist/js/bootstrap.min'); ?>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<?php echo $this->Html->script('/assets/bootstrap/assets/js/ie10-viewport-bug-workaround'); ?>

	<?php //echo $this->element('sql_dump'); ?>
	
	<!-- Script2 for finding in web-->
	<?php echo $this->Html->script('/assets/select2/dist/js/select2.min'); ?>				
	<?php echo $this->Html->css('https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.css'); ?>
		<?php echo $this->Html->script('https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js'); ?>
		<script type="text/javascript">
			$(document).ready(function() {
			$(".js-example-basic-single").select2();
			});
		</script>


	 
  </body>
</html>
