<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Smart Agriculture towards Sustainable Development :: Faculty of Agriculture</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <!-- Favicons -->
  <?php echo $this->Html->meta('favicon.ico','/img/Logo_ENG.png',array('type' => 'icon')); ?> 
   

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <?php echo $this->Html->css('/assets/Tempo/assets/vendor/bootstrap/css/bootstrap.min.css'); ?>
  <?php echo $this->Html->css('/assets/Tempo/assets/vendor/icofont/icofont.min.css'); ?>
  <?php echo $this->Html->css('/assets/Tempo/assets/vendor/boxicons/css/boxicons.min.css'); ?>
  <?php echo $this->Html->css('/assets/Tempo/assets/vendor/remixicon/remixicon.css'); ?>
  <?php echo $this->Html->css('/assets/Tempo/assets/vendor/venobox/venobox.css'); ?>
  <?php echo $this->Html->css('/assets/Tempo/assets/vendor/owl.carousel/assets/owl.carousel.min.css'); ?>

   
 

  <!-- Template Main CSS File -->   
  <?php echo $this->Html->css('/assets/Tempo/assets/css/style1.css'); ?>
 
  <!-- Modal animate -->   
  <?php echo $this->Html->css('/assets/animatedModal.js-master/css/normalize.min.css'); ?>
  <?php echo $this->Html->css('/assets/animatedModal.js-master/css/animate.min.css'); ?>
 


  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
		<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
		<?php echo $this->Html->script('jquery.dataTables.min'); ?>
		<?php echo $this->Html->script('dataTables.bootstrap.min'); ?>
  <!-- =======================================================
  * Template Name: Tempo - v2.1.0
  * Template URL: https://bootstrapmade.com/tempo-free-onepage-bootstrap-theme/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<style>

  /* ---- reset ---- */ 
 
  canvas{ 
    display: block; 
    vertical-align: bottom; 
  } 
  /* ---- particles.js container ---- */ 
  #particles-js{ 
    position:absolute; 
    width: 100%; 
    height: 100%; 
    position: fixed;
    
  } /* ---- stats.js ---- */ 
  .count-particles{ 
    background: #000022; 
    position: absolute; 
    top: 48px; 
    left: 0; 
    width: 80px; 
    color: #13E8E9; 
    font-size: .8em; 
    text-align: left; 
    text-indent: 4px; 
    line-height: 14px; 
    padding-bottom: 2px; 
    font-family: Helvetica, Arial, sans-serif; 
    font-weight: bold; 
  } 
  .js-count-particles{ 
    font-size: 1.1em; 
  } 
  #stats, .count-particles{ 
    -webkit-user-select: none; 
    margin-top: 5px; 
    margin-left: 5px; 
  } 
  #stats{ 
    border-radius: 3px 3px 0 0; 
    overflow: hidden; 
  } 
  .count-particles{ 
    border-radius: 0 0 3px 3px; 
  }
</style>
<script>
  $("#demo01").animatedModal();
</script>

  <body  >  
   

	  <?php echo $this->fetch('content'); ?> 
     

   
     
		<!-- ======= Footer ======= -->
    <footer id="footer">

      <!-- <div class="footer-top">
        <div class="container">
          <div class="row">

            <div class="col-lg-3 col-md-6 footer-contact">
              <h3>Tempo</h3>
              <p>
                A108 Adam Street <br>
                New York, NY 535022<br>
                United States <br><br>
                <strong>Phone:</strong> +1 5589 55488 55<br>
                <strong>Email:</strong> info@example.com<br>
              </p>
            </div>

            <div class="col-lg-2 col-md-6 footer-links">
              <h4>Useful Links</h4>
              <ul>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Services</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Terms of service</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
              </ul>
            </div>

            <div class="col-lg-3 col-md-6 footer-links">
              <h4>Our Services</h4>
              <ul>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Web Design</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Web Development</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Product Management</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Marketing</a></li>
                <li><i class="bx bx-chevron-right"></i> <a href="#">Graphic Design</a></li>
              </ul>
            </div>

            <div class="col-lg-4 col-md-6 footer-newsletter">
              <h4>Join Our Newsletter</h4>
              <p>Tamen quem nulla quae legam multos aute sint culpa legam noster magna</p>
              <form action="" method="post">
                <input type="email" name="email"><input type="submit" value="Subscribe">
              </form>
            </div>

          </div>
        </div>
      </div> -->
      
      <div class="container d-md-flex py-12">

        <div class="mr-md-auto text-center text-md-left">
        <img class="img-responsive" src="<?php echo $this->Html->url('/img/footer_tcas.png'); ?>" alt="Hero Imgs" title="" width="50%" >
         <div class="copyright font-18">
             Copyright &copy; <strong><span><?php echo date('Y'); ?></span></strong> Faculty of Agriculture, Chiang Mai University, All Rights Reserved.
          </div>
          <div class="credits font-10">
            <!-- All the links in the footer should remain intact. -->
            <!-- You can delete the links only if you purchased the pro version. -->
            <!-- Licensing information: https://bootstrapmade.com/license/ -->
            <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/tempo-free-onepage-bootstrap-theme/ -->
              Faculty of Agriculture 239, Huay Kaew Road, Muang District, Chiang Mai, Thailand, 50200 <br>
              Tel : 0-5394-4009 ,Fax : 0-5394-4666 <br>
              
            <!-- Designed  BootstrapMade  and
            Icons made by  Freepik  -->
          </div>
        </div>
        <!-- <div class="social-links text-center text-md-right pt-3 pt-md-0">
          <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
          <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
          <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
          <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
          <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
        </div> -->
      </div>
    </footer><!-- End Footer -->

<a href="#" class="back-to-top"><i class="ri-arrow-up-line"></i></a>
 
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <?php echo $this->Html->script('/assets/animatedModal.js-master/js/animatedModal.min.js'); ?>
    <script>
        // $("#demo-video").animatedModal({
          //     animatedIn:'lightSpeedIn',
          //     animatedOut:'bounceOutDown',
          //     color:'#F2F2F2',
          //     // Callbacks
          //     beforeOpen: function() {
          //         console.log("The animation was called");
          //     },           
          //     afterOpen: function() {
          //         console.log("The animation is completed");
          //     }, 
          //     beforeClose: function() {
          //         console.log("The animation was called");
          //     }, 
          //     afterClose: function() {
          //         console.log("The animation is completed");
          //     }
        // });

        //demo 01
        $("#demo01").animatedModal({ 
            animatedIn:'zoomIn',
            animatedOut:'bounceOut',           
            color:'#FFFFFF',    
            overflow:'auto',          
        });

        //demo 02
        $("#demo02").animatedModal({ 
            animatedIn:'zoomIn',
            animatedOut:'bounceOut',           
            color:'#FFFFFF',   
                      
        });
         

        //demo 03
        $("#demo03").animatedModal({ 
            animatedIn:'zoomIn',
            animatedOut:'bounceOut',           
            color:'#FFFFFF',             
        });
         

        //demo 04
        // $("#demo04").animatedModal({ 
        //     animatedIn:'zoomIn',
        //     animatedOut:'bounceOut',           
        //     color:'#FFFFFF',             
        // });

         //demo 05
        // $("#demo05").animatedModal({ 
        //     animatedIn:'zoomIn',
        //     animatedOut:'bounceOut',           
        //     color:'#FFFFFF',             
        // });

         //demo 06
        $("#demo06").animatedModal({ 
            animatedIn:'zoomIn',
            animatedOut:'bounceOut',           
            color:'#FFFFFF',             
        });

         //demo 07
        // $("#demo07").animatedModal({ 
        //     animatedIn:'zoomIn',
        //     animatedOut:'bounceOut',           
        //     color:'#FFFFFF',             
        // });

         //demo 08
        //  $("#demo08").animatedModal({ 
        //     animatedIn:'zoomIn',
        //     animatedOut:'bounceOut',           
        //     color:'#FFFFFF',             
        // });

        //demo 09
        // $("#demo09").animatedModal({ 
        //     animatedIn:'zoomIn',
        //     animatedOut:'bounceOut',           
        //     color:'#FFFFFF',             
        // });

        //demo 10
        // $("#demo10").animatedModal({ 
        //     animatedIn:'zoomIn',
        //     animatedOut:'bounceOut',           
        //     color:'#FFFFFF',             
        // });

         //demo 11
         

         //demo 12
        //  $("#demo12").animatedModal({ 
        //     animatedIn:'zoomIn',
        //     animatedOut:'bounceOut',           
        //     color:'#FFFFFF',             
        // });

         //demo 13
       

         

         

        

    </script>


<!-- Vendor JS Files --> 
  <?php echo $this->Html->script('/assets/Tempo/assets/vendor/jquery/jquery.min.js'); ?>
  <?php echo $this->Html->script('/assets/Tempo/assets/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>
  <?php echo $this->Html->script('/assets/Tempo/assets/vendor/jquery.easing/jquery.easing.min.js'); ?>
  <?php echo $this->Html->script('/assets/Tempo/assets/vendor/php-email-form/validate.js'); ?>
  <?php echo $this->Html->script('/assets/Tempo/assets/vendor/isotope-layout/isotope.pkgd.min.js'); ?>
  <?php echo $this->Html->script('/assets/Tempo/assets/vendor/venobox/venobox.min.js'); ?>
  <?php echo $this->Html->script('/assets/Tempo/assets/vendor/owl.carousel/owl.carousel.min.js'); ?>

  <!-- Libraries CSS Files -->
 
   
   
 
  <!-- Template Main Javascript File -->
<?php echo $this->Html->script('/assets/Tempo/assets/js/main.js'); ?>

</body>

</html>
