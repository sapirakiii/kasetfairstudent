<!DOCTYPE html>
<html lang="en">
	<head>
		<?php echo $this->Html->charset(); ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<meta content="Faculty of Agriculture , Chiangmai University" name="author" />
		<meta content="<?php //echo $keywords; ?>" name="keywords" />
		<meta content="<?php //echo $keywordsTh; ?>" name="keywords" lang="th"/>
		<meta content="<?php //echo $description; ?>" name="description" />
		
		<?php
			//echo $this->Html->meta('icon');		
			echo $this->Html->meta('favicon.ico','/img/Logo_ENG.png',array('type' => 'icon'));

			//echo $this->Html->css('cake.generic');	
			echo $this->Html->css('agri');	
		?>

		<title>
			<?php echo /*$title_for_layout.*/'Faculty of Agriculture , Chiangmai University'; ?>
		</title>

		<!-- Bootstrap core CSS -->
		<?php echo $this->Html->css('/assets/bootstrap/dist/css/bootstrap.min'); ?>

		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<?php echo $this->Html->css('/assets/bootstrap/assets/css/ie10-viewport-bug-workaround'); ?>
		
		<!-- Custom styles for this template -->
		<?php echo $this->Html->css('/assets/bootstrap/signin'); ?>

		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><?php echo $this->Html->script('/assets/bootstrap/assets/js/ie8-responsive-file-warning'); ?><![endif]-->
		<?php echo $this->Html->script('/assets/bootstrap/assets/js/ie-emulation-modes-warning'); ?>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<?php echo $this->Html->script('https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js'); ?>
			<?php echo $this->Html->script('https://oss.maxcdn.com/respond/1.4.2/respond.min.js'); ?>
		<![endif]-->
		
		<?php //echo $this->Html->script('https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js'); ?>
		<?php 
			//<script>window.jQuery || document.write('<script src="/assets/bootstrap/assets/js/vendor/jquery.min.js"><\/script>')</script>
			echo $this->Html->script('/assets/bootstrap/assets/js/vendor/jquery.min'); 
		?>
			
		<?php
			echo $this->fetch('meta');
			echo $this->fetch('css');
			echo $this->fetch('script');
		?>
		
		
	</head>

	<body>
	
		
		<!-- Begin page content -->
		
		<?php 	
		//debug($this->Session->flash());
		
		$alert = $this->Session->flash();
		
		if($alert){
		?>
			<div class="alert alert-danger" role="alert" style="text-align: center;">
				<?php echo $alert; ?>
			</div>
		<?php 
		} 
		?>
		
		<?php echo $this->fetch('content'); ?>
		

		


		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		
		<?php echo $this->Html->script('/assets/bootstrap/dist/js/bootstrap.min'); ?>

		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<?php echo $this->Html->script('/assets/bootstrap/assets/js/ie10-viewport-bug-workaround'); ?>

	</body>
</html>
