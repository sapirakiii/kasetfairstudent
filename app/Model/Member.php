<?php
class Member extends AppModel {
	public $useDbConfig = 'kasetfair2017';
	
	public $belongsTo = array(
        'Prefix','Status'
    );
    public $hasMany = array(
    	'MemberDocument'
    );
}