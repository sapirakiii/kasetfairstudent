<?php
class MemberDocument extends AppModel {
	public $useDbConfig = 'kasetfair2017';

	public $belongsTo = array(
        'Member'
    );
}