<?php
class MisEducationMore extends AppModel
{
	public $useDbConfig = 'mis';

	public $belongsTo = array(
		'MisLevelEducational', 'Country'
	);
}
