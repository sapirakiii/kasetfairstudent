<?php
class MisEmployeePosition extends AppModel
{
	public $useDbConfig = 'mis';

	public $belongsTo = array(
		'MisPosition', 'MisPositionAcademic', 'MisEmploymentType', 'MisTypeStaff', 'MisWorkStatus',
		'MisOrganize' => array(
			'foreignKey' => 'mis_organize_id',
			'conditions' => array('MisOrganize.active' => '1')
		)
	);
}
