<?php
class InternshipStudent extends AppModel {
	public $useDbConfig = 'kasetfair2017';
	
	public $belongsTo = array(
        'Student','Internship290Receive','InternshipProject'
    );
}