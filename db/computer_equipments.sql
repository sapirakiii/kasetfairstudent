-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 06, 2022 at 10:51 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kasetfairstudent`
--

-- --------------------------------------------------------

--
-- Table structure for table `computer_equipments`
--

CREATE TABLE `computer_equipments` (
  `id` int(11) NOT NULL,
  `com_type_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `com_use_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `brand` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `room` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `cpu` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `ram` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `HDD` int(11) NOT NULL,
  `SSD` int(11) NOT NULL,
  `code` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `mis_employee_id` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `computer_equipments`
--

INSERT INTO `computer_equipments` (`id`, `com_type_id`, `com_use_id`, `brand`, `room`, `cpu`, `ram`, `HDD`, `SSD`, `code`, `mis_employee_id`, `create_at`) VALUES
(1, '1', '1', 'DEll', 'C137', 'ryzen', '4', 4, 0, 'C137', 1, '2022-10-06 08:43:12'),
(2, '1', '1', 'DEll', 'C137', 'ryzen', '4', 4, 0, 'C137', 1, '2022-10-06 08:44:24');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `computer_equipments`
--
ALTER TABLE `computer_equipments`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `computer_equipments`
--
ALTER TABLE `computer_equipments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
