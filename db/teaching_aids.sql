-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 02, 2022 at 12:38 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kasetfairstudent`
--

-- --------------------------------------------------------

--
-- Table structure for table `teaching_aids`
--

CREATE TABLE `teaching_aids` (
  `teaching_id` int(11) NOT NULL,
  `name` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `create_date` datetime NOT NULL,
  `modify` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `teaching_aids`
--

INSERT INTO `teaching_aids` (`teaching_id`, `name`, `create_date`, `modify`) VALUES
(1, 'โปรเจคเตอร์(Projector)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'เครื่องเสียง(Audio equipment)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'ไมค์(Microphone)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'อุปกรณ์ HDMI to VGA(HDMI to VGA devices)', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'กล้องเว็บแคม(Webcam)', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `teaching_aids`
--
ALTER TABLE `teaching_aids`
  ADD PRIMARY KEY (`teaching_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `teaching_aids`
--
ALTER TABLE `teaching_aids`
  MODIFY `teaching_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
