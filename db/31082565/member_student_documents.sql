-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 30, 2022 at 09:16 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kasetfairstudent`
--

-- --------------------------------------------------------

--
-- Table structure for table `member_student_documents`
--

CREATE TABLE `member_student_documents` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_old` text COLLATE utf8_unicode_ci NOT NULL,
  `student_id` int(11) NOT NULL,
  `post_date` datetime NOT NULL,
  `class` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `member_student_documents`
--

INSERT INTO `member_student_documents` (`id`, `name`, `name_old`, `student_id`, `post_date`, `class`) VALUES
(1, '2022-08-31 021453_การแข่งขันนำเสนอผลงานวิชาการภาคโปสเตอร์.pdf', 'การแข่งขันนำเสนอผลงานวิชาการภาคโปสเตอร์.pdf', 621310183, '2022-08-31 02:14:00', 'file'),
(2, '2022-08-31 021507_การแข่งขันนำเสนอผลงานวิชาการภาคโปสเตอร์.pdf', 'การแข่งขันนำเสนอผลงานวิชาการภาคโปสเตอร์.pdf', 621310183, '2022-08-31 02:15:00', 'file');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `member_student_documents`
--
ALTER TABLE `member_student_documents`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `member_student_documents`
--
ALTER TABLE `member_student_documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
