-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 02, 2022 at 12:37 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kasetfairstudent`
--

-- --------------------------------------------------------

--
-- Table structure for table `equipment_teachings`
--

CREATE TABLE `equipment_teachings` (
  `id` int(11) NOT NULL,
  `computer_equipment_id` int(11) NOT NULL,
  `teaching_id` int(11) NOT NULL,
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `equipment_teachings`
--

INSERT INTO `equipment_teachings` (`id`, `computer_equipment_id`, `teaching_id`, `create_at`) VALUES
(1, 36, 1, '2022-07-25 22:29:00'),
(2, 36, 2, '2022-07-25 22:29:00'),
(3, 36, 3, '2022-07-25 22:29:00'),
(4, 37, 1, '2022-07-25 22:29:06'),
(5, 37, 2, '2022-07-25 22:29:06'),
(6, 37, 3, '2022-07-25 22:29:06'),
(7, 38, 1, '2022-07-25 22:29:58'),
(8, 38, 3, '2022-07-25 22:29:58'),
(9, 38, 5, '2022-07-25 22:29:58'),
(10, 39, 1, '2022-07-26 08:57:21'),
(11, 39, 5, '2022-07-26 08:57:21'),
(12, 40, 1, '2022-07-26 08:58:57'),
(13, 40, 5, '2022-07-26 08:58:57'),
(14, 41, 1, '2022-07-26 09:03:00'),
(15, 41, 5, '2022-07-26 09:03:00'),
(16, 42, 1, '2022-07-26 09:05:35'),
(17, 42, 3, '2022-07-26 09:05:35'),
(18, 44, 1, '2022-07-26 09:55:04'),
(19, 44, 3, '2022-07-26 09:55:04'),
(20, 45, 2, '2022-07-26 09:55:20'),
(21, 45, 4, '2022-07-26 09:55:20'),
(22, 46, 1, '2022-07-26 09:56:30'),
(23, 46, 2, '2022-07-26 09:56:30'),
(24, 46, 3, '2022-07-26 09:56:30'),
(25, 47, 1, '2022-07-26 10:04:58'),
(26, 47, 3, '2022-07-26 10:04:58'),
(27, 48, 2, '2022-07-26 11:59:01'),
(28, 1, 1, '2022-07-26 17:27:15'),
(29, 1, 3, '2022-07-26 17:27:15'),
(30, 2, 1, '2022-08-02 07:15:26'),
(31, 2, 2, '2022-08-02 07:15:26'),
(32, 2, 3, '2022-08-02 07:15:26'),
(33, 3, 1, '2022-08-02 07:17:28'),
(34, 3, 2, '2022-08-02 07:17:28'),
(35, 3, 3, '2022-08-02 07:17:28'),
(36, 4, 1, '2022-08-02 07:19:38'),
(37, 4, 2, '2022-08-02 07:19:38'),
(38, 4, 3, '2022-08-02 07:19:38'),
(39, 5, 1, '2022-08-02 07:21:10'),
(40, 5, 2, '2022-08-02 07:21:10'),
(41, 5, 3, '2022-08-02 07:21:10'),
(42, 6, 1, '2022-08-02 07:23:34'),
(43, 6, 2, '2022-08-02 07:23:34'),
(44, 6, 3, '2022-08-02 07:23:34'),
(45, 7, 1, '2022-08-02 07:24:28'),
(46, 7, 2, '2022-08-02 07:24:28'),
(47, 7, 3, '2022-08-02 07:24:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `equipment_teachings`
--
ALTER TABLE `equipment_teachings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `equipment_teachings`
--
ALTER TABLE `equipment_teachings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
