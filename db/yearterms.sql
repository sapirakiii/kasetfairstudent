-- phpMyAdmin SQL Dump
-- version 2.10.3
-- http://www.phpmyadmin.net
-- 
-- Host: localhost
-- Generation Time: Jul 26, 2024 at 03:42 PM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Database: `educationsystem`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `yearterms`
-- 

CREATE TABLE `yearterms` (
  `id` int(5) NOT NULL auto_increment,
  `year` varchar(5) default NULL,
  `term` int(3) default NULL,
  `trm` varchar(10) default NULL,
  `name` varchar(50) default NULL,
  `postdate` date default NULL,
  `level` int(2) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43 ;

-- 
-- Dumping data for table `yearterms`
-- 

INSERT INTO `yearterms` VALUES (1, '2556', 1, '561', 'ภาคการศึกษาที่ 1 ปีการศึกษา 2556', '2013-08-16', 1);
INSERT INTO `yearterms` VALUES (2, '2556', 2, '562', 'ภาคการศึกษาที่ 2 ปีการศึกษา 2556', NULL, 1);
INSERT INTO `yearterms` VALUES (3, '2556', 3, '563', 'ภาคการศึกษาที่ 3 ปีการศึกษา 2556', NULL, 1);
INSERT INTO `yearterms` VALUES (4, '2557', 1, '571', 'ภาคการศึกษาที่ 1 ปีการศึกษา 2557', NULL, 1);
INSERT INTO `yearterms` VALUES (5, '2557', 2, '572', 'ภาคการศึกษาที่ 2 ปีการศึกษา 2557', NULL, 1);
INSERT INTO `yearterms` VALUES (6, '2557', 3, '573', 'ภาคการศึกษาที่ 3 ปีการศึกษา 2557', NULL, 1);
INSERT INTO `yearterms` VALUES (7, '2558', 1, '581', 'ภาคการศึกษาที่ 1 ปีการศึกษา 2558', NULL, 1);
INSERT INTO `yearterms` VALUES (8, '2558', 2, '582', 'ภาคการศึกษาที่ 2 ปีการศึกษา 2558', NULL, 1);
INSERT INTO `yearterms` VALUES (9, '2558', 3, '583', 'ภาคการศึกษาที่ 3 ปีการศึกษา 2558', NULL, 1);
INSERT INTO `yearterms` VALUES (10, '2559', 1, '591', 'ภาคการศึกษาที่ 1 ปีการศึกษา 2559', NULL, 1);
INSERT INTO `yearterms` VALUES (11, '2559', 2, '592', 'ภาคการศึกษาที่ 2 ปีการศึกษา 2559', NULL, 1);
INSERT INTO `yearterms` VALUES (12, '2559', 3, '593', 'ภาคการศึกษาที่ 3 ปีการศึกษา 2559', NULL, 1);
INSERT INTO `yearterms` VALUES (13, '2560', 1, '601', 'ภาคการศึกษาที่ 1 ปีการศึกษา 2560', '2017-08-16', 1);
INSERT INTO `yearterms` VALUES (14, '2560', 2, '602', 'ภาคการศึกษาที่ 2 ปีการศึกษา 2560', '2018-02-16', 1);
INSERT INTO `yearterms` VALUES (15, '2560', 3, '603', 'ภาคการศึกษาที่ 3 ปีการศึกษา 2560', '2018-07-16', 1);
INSERT INTO `yearterms` VALUES (16, '2561', 1, '611', 'ภาคการศึกษาที่ 1 ปีการศึกษา 2561', '2018-07-29', 1);
INSERT INTO `yearterms` VALUES (17, '2561', 2, '612', 'ภาคการศึกษาที่ 2 ปีการศึกษา 2561', '2019-05-31', 1);
INSERT INTO `yearterms` VALUES (18, '2561', 3, '613', 'ภาคการศึกษาที่ 3 ปีการศึกษา 2561', '2019-07-26', 1);
INSERT INTO `yearterms` VALUES (19, '2562', 1, '621', 'ภาคการศึกษาที่ 1 ปีการศึกษา 2562', '2019-11-14', 1);
INSERT INTO `yearterms` VALUES (20, '2562', 2, '622', 'ภาคการศึกษาที่ 2 ปีการศึกษา 2562', '2020-05-09', 1);
INSERT INTO `yearterms` VALUES (21, '2562', 3, '623', 'ภาคการศึกษาที่ 3 ปีการศึกษา 2562', '2020-06-17', 1);
INSERT INTO `yearterms` VALUES (22, '2563', 1, '631', 'ภาคการศึกษาที่ 1 ปีการศึกษา 2563', '2020-10-06', 1);
INSERT INTO `yearterms` VALUES (23, '2563', 2, '632', 'ภาคการศึกษาที่ 2 ปีการศึกษา 2563', '2021-03-11', 1);
INSERT INTO `yearterms` VALUES (24, '2563', 3, '633', 'ภาคการศึกษาที่ 3 ปีการศึกษา 2563', '2021-05-06', 1);
INSERT INTO `yearterms` VALUES (25, '2564', 1, '641', 'ภาคการศึกษาที่ 1 ปีการศึกษา 2564', '2021-07-07', 1);
INSERT INTO `yearterms` VALUES (26, '2564', 2, '642', 'ภาคการศึกษาที่ 2 ปีการศึกษา 2564', '2022-03-27', 1);
INSERT INTO `yearterms` VALUES (27, '2564', 3, '643', 'ภาคการศึกษาที่ 3 ปีการศึกษา 2564', '2022-03-28', 1);
INSERT INTO `yearterms` VALUES (28, '2565', 1, '651', 'ภาคการศึกษาที่ 1 ปีการศึกษา 2565', '2022-07-20', 1);
INSERT INTO `yearterms` VALUES (29, '2565', 2, '652', 'ภาคการศึกษาที่ 2 ปีการศึกษา 2565', '2022-12-09', 1);
INSERT INTO `yearterms` VALUES (30, '2565', 3, '653', 'ภาคการศึกษาที่ 3 ปีการศึกษา 2565', '2023-05-01', 1);
INSERT INTO `yearterms` VALUES (31, '2566', 1, '661', 'ภาคการศึกษาที่ 1 ปีการศึกษา 2566', '2023-07-06', 1);
INSERT INTO `yearterms` VALUES (32, '2566', 2, '662', 'ภาคการศึกษาที่ 2 ปีการศึกษา 2566', '2023-12-06', 1);
INSERT INTO `yearterms` VALUES (33, '2566', 3, '663', 'ภาคการศึกษาที่ 3 ปีการศึกษา 2566', '2024-05-15', 1);
INSERT INTO `yearterms` VALUES (34, '2567', 1, '671', 'ภาคการศึกษาที่ 1 ปีการศึกษา 2567', '2024-07-05', 1);
INSERT INTO `yearterms` VALUES (35, '2567', 2, '672', 'ภาคการศึกษาที่ 2 ปีการศึกษา 2567', NULL, 2);
INSERT INTO `yearterms` VALUES (36, '2567', 3, '673', 'ภาคการศึกษาที่ 3 ปีการศึกษา 2567', NULL, 2);
INSERT INTO `yearterms` VALUES (37, '2568', 1, '681', 'ภาคการศึกษาที่ 1 ปีการศึกษา 2568', NULL, 2);
INSERT INTO `yearterms` VALUES (38, '2568', 2, '682', 'ภาคการศึกษาที่ 2 ปีการศึกษา 2568', NULL, 2);
INSERT INTO `yearterms` VALUES (39, '2568', 3, '683', 'ภาคการศึกษาที่ 3 ปีการศึกษา 2568', NULL, 2);
INSERT INTO `yearterms` VALUES (40, '2569', 1, '691', 'ภาคการศึกษาที่ 1 ปีการศึกษา 2569', NULL, 2);
INSERT INTO `yearterms` VALUES (41, '2569', 2, '692', 'ภาคการศึกษาที่ 2 ปีการศึกษา 2569', NULL, 2);
INSERT INTO `yearterms` VALUES (42, '2569', 3, '693', 'ภาคการศึกษาที่ 3 ปีการศึกษา 2569', NULL, 2);
