-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 09, 2022 at 12:50 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kasetfairstudent`
--

-- --------------------------------------------------------

--
-- Table structure for table `guides`
--

CREATE TABLE `guides` (
  `id` int(30) NOT NULL,
  `organize` varchar(100) DEFAULT NULL,
  `address` text,
  `prefix_id` int(5) DEFAULT NULL,
  `fname` varchar(100) DEFAULT NULL,
  `lname` varchar(100) DEFAULT NULL,
  `dateguide_id` int(4) DEFAULT NULL,
  `round` int(5) DEFAULT NULL,
  `start` varchar(20) DEFAULT NULL,
  `end` varchar(20) DEFAULT NULL,
  `visit` varchar(3) DEFAULT NULL,
  `prefix_teacher_id` varchar(20) DEFAULT NULL,
  `fname_teacher` varchar(100) DEFAULT NULL,
  `reftel` varchar(20) DEFAULT NULL,
  `refline` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `guides`
--

INSERT INTO `guides` (`id`, `organize`, `address`, `prefix_id`, `fname`, `lname`, `dateguide_id`, `round`, `start`, `end`, `visit`, `prefix_teacher_id`, `fname_teacher`, `reftel`, `refline`, `created`, `modified`) VALUES
(1, 'มช', '<p>เลขที่ ........... ถนน ........... ตำบล/แขวง ........... อำเภอ/เขต ........... จังหวัด ............ รหัสไปรษณีย์ ............โทรศัพท์ ............</p>\r\n', 1, 'sakchai', 'pirak', 2, 2, NULL, NULL, NULL, '1', 'นิค', '0819513293', 'newtron', '2022-09-09 17:41:23', '2022-09-09 17:41:23'),
(2, 'มช', '<p>เลขที่ ........... ถนน ........... ตำบล/แขวง ........... อำเภอ/เขต ........... จังหวัด ............ รหัสไปรษณีย์ ............โทรศัพท์ ............</p>\r\n', 1, 'ปั้ม', 'แคมป์', 2, 2, NULL, NULL, NULL, '1', 'นิค', '0819513293', 'newtron', '2022-09-09 17:41:23', '2022-09-09 17:41:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `guides`
--
ALTER TABLE `guides`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `guides`
--
ALTER TABLE `guides`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
