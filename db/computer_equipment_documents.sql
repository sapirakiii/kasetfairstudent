-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 02, 2022 at 12:37 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kasetfairstudent`
--

-- --------------------------------------------------------

--
-- Table structure for table `computer_equipment_documents`
--

CREATE TABLE `computer_equipment_documents` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name_old` text COLLATE utf8_unicode_ci NOT NULL,
  `computer_equipment_id` int(11) NOT NULL,
  `post_date` datetime NOT NULL,
  `class` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `computer_equipment_documents`
--

INSERT INTO `computer_equipment_documents` (`id`, `name`, `name_old`, `computer_equipment_id`, `post_date`, `class`) VALUES
(1, '2022-08-02 14:17:28_Pou.jpg', 'Pou.jpg', 3, '2022-08-02 14:17:00', 'img'),
(2, '2022-08-02 14:19:38_Pou.jpg', 'Pou.jpg', 4, '2022-08-02 14:19:00', 'img'),
(3, '2022-08-02 142110_Pou.jpg', 'Pou.jpg', 5, '2022-08-02 14:21:00', 'img'),
(4, '2022-08-02 142334_Pou.jpg', 'Pou.jpg', 6, '2022-08-02 14:23:00', 'img'),
(5, '2022-08-02 142428_Pou.jpg', 'Pou.jpg', 7, '2022-08-02 14:24:00', 'img');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `computer_equipment_documents`
--
ALTER TABLE `computer_equipment_documents`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `computer_equipment_documents`
--
ALTER TABLE `computer_equipment_documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
